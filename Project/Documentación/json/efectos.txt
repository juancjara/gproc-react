{
    "efectos": {
        "1.2": [
            {
                "efecto": "El entrenamiento del personal directivo, jerárquico y docente del IFD en gestión administrativa y docente permitirá guiar apropiadamente la elaboración y ejecución de un plan de evaluación de las actividades institucionales, fortaleciendo el clima y cultura organizacional de las carreras de formación docente; logrando la identificación y participación activa del personal, directivo, jerárquico, docente, administrativo y estudiantes; manteniendo la calidad y excelencia educativa."
            }
        ],
        "1.3": [
            {
                "efecto": "La evaluación de resultados de la función docente, dirigida por un personal debidamente capacitado, evidencia la coherencia de la asignación de cargos en el IFD, fortaleciendo los procesos de enseñanza – aprendizaje y permitiendo la aplicación del reglamento y plan de estímulo al personal docente, según su desempeño."
            }
        ],
        "2.1": [
            {
                "efecto": "Los resultados del estudio de la demanda laboral y adaptación de la oferta académica fortalecen los procesos de formación profesional   en el IFD, permitiendo el desarrollo de políticas institucionales  que aseguren la disponibilidad de profesionales cualificados, que exige la sociedad. Así mismo mejoran los procesos de admisión, incrementando el total de postulantes."
            },
            {
                "efecto": "El programa de contextualización del perfil del egresado, asegura el logro óptimo de las competencias profesionales establecidas para las carreras del IFD, respondiendo a los niveles  de exigencia de los avances de la ciencia y la tecnología, dentro de marcos de responsabilidades de la dimensión ética, los avances de los conocimientos y los diversos contextos y características culturales."
            },
            {
                "efecto": "La aplicación oportuna y eficiente de metodologías y contenidos alineados con el perfil profesional y los diseños curriculares de las carreras profesionales de IFD,  garantizan el desarrollo de competencias personales y profesionales, para el óptimo desempeño en el contexto en el que se desenvolverán nuestros egresados."
            }
        ],
        "2.3": [
            {
                "efecto": "Los resultados de la pasantía docente en el desarrollo del cumplimiento de programas y consistencia metodológica de la enseñanza-aprendizaje,   profundizan la aplicabilidad pertinente de los procesos de planificación curricular, como elemento indispensable de la formación profesional docente, desde una concepción cultural, académica y humana;realizando prácticas complementarias a su formación continua."
            },
            {
                "efecto": "Los resultados de la capacitación en planes de mejora en evaluación de los aprendizajes, aseguran la la idoneidad de la formación profesional de los estudiantes de las carreras de formación docente, considerando que la evaluación es  una herramienta útil para el mejoramiento de la calidad educativa, ya que proporciona información que permite establecer fortalezas y debilidades que orientan el diseño de políticas institucionales."
            },
            {
                "efecto": "La implementación de un sistema informático para optimizar el proceso de enseñanza-aprendizaje asegura el fortalecimiento de las competencias profesionales de los estudiantes de las carreras docentes; sustentadas en la necesidad de contar con un sistema de información en tiempo real y con procesos continuos y sistemáticos, que asistan en forma permanente a la toma de decisiones oportunas en el desempeño académico de la población estudiantil."
            },
            {
                "efecto": "El fortalecimiento de las  capacidades y habilidades  del personal a través del entrenamiento del uso correcto del sistema informático, permiten afianzar estratégicamente el mantenimiento y operativización del sistema informático, logrando de esta manera la sostenibilidad de los procesos académicos de la institución."
            }
        ],
        "2.4": [
            {
                "efecto": "La aplicabilidad pertinente de un plan de la eficiencia de la titulación de las carreras profesionales del IFD, aseguran el logro de las metas institucionales, en porcentajes aceptables de finalización los estudios académicos y la titulación correspondiente."
            }
        ],
        "2.6": [
            {
                "efecto": "La pertinencia de la organización y aplicabilidad de las líneas de investigación educativa, acreditan los procesos de publicación de los trabajos de investigación en los diversos medios comunicativos de la comunidad, evidenciando el nivel de desempeño de docentes y estudiantes en el dominio de capacidades investigativas."
            },
            {
                "efecto": "Los resultados del programa estructural de investigación en las carreras profesionales de IFD, permiten consolidar un visión holística de los acontecimientos socioculturales, académicos, etc del contexto educativo, para garantizar la implementación de un currículo contextualizado y flexible, centrado en los procesos que hacen parte de la vida y que dé significado al aprendizaje cotidiano de los estudiantes. "
            },
            {
                "efecto": "El equipamiento bibliográfico, de laboratorio e informático pertinente, optimizan el desarrollo de las actividades investigativas, asegurando el fortalecimiento de las competencias profesionales de los estudiantes de las carreras pedagógicas; de los docentes y de los egresados, promoviendo la disponibilidad de trabajos y/o actividades  relevantes de investigación."
            }
        ],
        "3.1": [
            {
                "efecto": "La implementación de políticas y procedimientos para el desarrollo del personal administrativo del IFD permitirá establecer acciones que propicien el seguimiento del personal asignado para una eficiente imagen del servicio brindado. "
            }
        ],
        "3.3": [
            {
                "efecto": "La oportuna aplicabilidad del plan de prevención de la deserción estudiantil, permiten la sostenibilidad de la permanencia de los estudiantes en los procesos de formación profesional, garantizando el  logro de metas de titulación que promueve la institución."
            }
        ],
        "3.4": [
            {
                "efecto": "El desarrollo del plan de capacitación de mantenimiento preventivo y mejora de la infraestructura, mobiliario y equipos de las carreras profesionales del IFD en la comunidad educativa, aseguran la sostenibilidad de los procesos académicos y administrativos, manteniendo en óptimas condiciones los ambientes de la institución."
            },
            {
                "efecto": "La implementación oportuna de un sistema de mantenimiento y renovación del mobiliario institucional, garantizan la eficiencia de atención del servicio de mobiliario de los estudiantes de las carreras pedagógicas,  mejorando la comodidad y calidez  a los clientes de la institución."
            }
        ],
        "4.1": [
            {
                "efecto": "Los procesos  y resultados del plan de mejora de la imagen institucional del IFD permitirán la actualización permanente de la información sobre la opinión de los usuarios tanto directos como indirectos, extendiéndose la influencia positiva en el entorno."
            }
        ],
        "4.2": [
            {
                "efecto": "La oportuna Implementación de lineamientos y estrategias de sostenibilidad de la proyección social y de alianzas estratégicas de las carreras profesionales del IFD permiten mantener el posicionamiento institucional en los medios sociales, culturales y empresariales."
            }
        ],
        "4.3": [
            {
                "efecto": "La implementación de lineamientos y estrategias de seguimiento de egresados de las carreras profesionales del IFD, aseguran  un pertinente diagnóstico de las necesidades profesionales, permitiendo registrar oportunamente información relevante de la experiencia laboral, actualización y servicio de empleo."
            }
        ]
    },
    "datos": {
        "com": {
            "nombre": "HNA. MONTOYA VARGAS, Angela ",
            "cargo": "DIRECTORA GENERAL",
            "date": "2015-10-26",
            "date_local": "26 de Octubre del 2015"
        },
        "dir": {
            "nombre": "HNA. MONTOYA VARGAS, Angela ",
            "cargo": "DIRECTORA GENERAL",
            "date": "2015-10-26",
            "date_local": "26 de Octubre del 2015"
        }
    }
}