{
    "dimensiones": {
        "1": {
            "objetivo": "Fortalecer la gestión institucional a través del desempeño eficiente de la organización administrativa y docente.",
            "hide": "",
            "nFactores": "2"
        },
        "2": {
            "objetivo": "Fortalecer los procesos académicos a través del diseño y aplicación de estrategias didácticas para la construcción eficiente del aprendizaje mediante el desarrollo de capacidades investigativas que permitan optimizar los procesos de titulación en cumplimento con la oferta y demanda educativa.",
            "hide": "",
            "nFactores": "4"
        },
        "3": {
            "objetivo": "Aplicar estrategias eficientes de los servicios de apoyo para la asistencia permanente de las necesidades de los estudiantes y el óptimo desarrollo de las actividades académicas y administrativas. ",
            "hide": "",
            "nFactores": "3"
        },
        "4": {
            "objetivo": "Optimizar los procesos de imagen institucional, proyección social y satisfacción de egresados, elevando el nivel de resultados e impacto del IFD en el entorno. ",
            "hide": "",
            "nFactores": "3"
        }
    },
    "factores": {
        "1.2": {
            "resultado": " La organización y gestión administrativa garantiza la calidad de la cultura organizacional",
            "hide": ""
        },
        "1.3": {
            "resultado": "Las políticas y procedimientos para la selección, evaluación y promoción de formadores mejoran la gestión docente.",
            "hide": ""
        },
        "2.1": {
            "resultado": " La oferta académica es pertinente a la demanda laboral, permitiendo la mejora de los procesos del diseño curricular.",
            "hide": ""
        },
        "2.3": {
            "resultado": " Los procesos académicos del IFD son ejecutados, evaluados y mejorados, garantizando la  competencia  profesional del estudiante. ",
            "hide": ""
        },
        "2.4": {
            "resultado": "La eficiencia y promoción de la titulación garantiza la oferta educativa de las carreras profesionales del IFD. ",
            "hide": ""
        },
        "2.6": {
            "resultado": "La estructura organizacional de la investigación del IFD promueve la capacitación, asignación de recursos y publicación de las actividades investigativas. ",
            "hide": ""
        },
        "3.1": {
            "resultado": " El IFD dispone de políticas y procedimientos para el desarrollo del personal administrativo, garantizando la satisfacción de los formadores y estudiantes.",
            "hide": ""
        },
        "3.3": {
            "resultado": "El IFD evalúa los lineamientos y estrategias aplicadas en relación a los resultados de la deserción estudiantil y se establecen mejoras. ",
            "hide": ""
        },
        "3.4": {
            "resultado": "El IFD realiza labores de mantenimiento preventivo y correctivo de la infraestructura, equipamiento y tecnología moderna para asegurar la eficiencia de los procesos académicos y administrativos.",
            "hide": ""
        },
        "4.1": {
            "resultado": "El IFD tiene una imagen posicionada en el medio social, cultural y productivo, a nivel local, regional y nacional. ",
            "hide": ""
        },
        "4.2": {
            "resultado": "El IFD evalúa los lineamientos y estrategias aplicadas en relación a la proyección social y establece mejoras. ",
            "hide": ""
        },
        "4.3": {
            "resultado": "El IFD cuenta con mecanismos que estimulan el contacto permanente con los egresados, promoviendo la inserción laboral. ",
            "hide": ""
        }
    },
    "actividades": {
        "1.2": [
            {
                "miactividad": "Entrenamiento del personal directivo, jerárquico y docente del IFD en gestión administrativa y docente.",
                "actor_cargo": "DIRECCIÓN GENERAL",
                "estandares": [
                    "3370",
                    "3372"
                ]
            }
        ],
        "1.3": [
            {
                "miactividad": "Entrenamiento en selección, evaluación y promoción de formadores.",
                "actor_cargo": "JEFATURA DE FORMACIÓN EN SERVICIO",
                "estandares": [
                    "3378"
                ]
            }
        ],
        "2.1": [
            {
                "miactividad": " Consultoría para el estudio de la demanda laboral y adaptación de la oferta académica de las carreras del IFD.",
                "actor_cargo": "COORDINACIÓN DE IMAGEN INSTITUCIONAL",
                "estandares": [
                    "3383"
                ]
            },
            {
                "miactividad": "Capacitación para la elaboración del programa de contextualización del perfil del egresado de las carreras de formación docente.",
                "actor_cargo": "COORDINACIÓN DE IMAGEN INSTITUCIONAL",
                "estandares": [
                    "3385"
                ]
            },
            {
                "miactividad": "Capacitación en la aplicación de metodologías y contenidos alineados con el perfil y los diseños curriculares de las carreras profesionales del IFD.",
                "actor_cargo": "JEFATURA DE ÁREA PEDAGÓGICA",
                "estandares": [
                    "3387"
                ]
            }
        ],
        "2.3": [
            {
                "miactividad": "Pasantía docente para el desarrollo de cumplimiento de programas y consistencia metodológica de la enseñanza – aprendizaje del IFD.  (ALIANZA ESTRATÉGICA – CONVENIO – OTROS) ",
                "actor_cargo": "JEFATURA DE ÁREA PEDAGÓGICA",
                "estandares": [
                    "3395",
                    "3396"
                ]
            },
            {
                "miactividad": "Capacitación para la elaboración de planes de mejora en evaluación del aprendizaje de las carreras profesionales del IFD. (PROCALIDAD).",
                "actor_cargo": "JEFATURA DE ÁREA PEDAGÓGICA",
                "estandares": [
                    "3395",
                    "3396"
                ]
            },
            {
                "miactividad": "Implementación de un sistema informático para optimizar el proceso enseñanza – aprendizaje de las carreras profesionales del IFD. (PROCALIDAD – ADQUISICIÓN) ",
                "actor_cargo": "JEFATURA DE ÁREA PEDAGÓGICA",
                "estandares": [
                    "3396",
                    "3398"
                ]
            },
            {
                "miactividad": "Entrenamiento del personal responsable para el correcto uso del sistema informático de las carreras profesionales del IFD. (PROCALIDAD). \n",
                "actor_cargo": "COORDINACIÓN DE NUEVAS TECNOLOGÍAS",
                "estandares": [
                    "3398"
                ]
            }
        ],
        "2.4": [
            {
                "miactividad": "Capacitación en la implementación eficiente del proceso de titulación de los estudiantes del IFD  (PROCALIDAD). ",
                "actor_cargo": "JEFATURA DE UNIDAD ACADÉMICA",
                "estandares": [
                    "3400"
                ]
            }
        ],
        "2.6": [
            {
                "miactividad": "Capacitación en organización y líneas de investigación educativa para docentes de las áreas de formación especializada y general de las tres carreras profesionales del IFD. (PROCALIDAD).",
                "actor_cargo": "COORDINACIÓN DE INVESTIGACIÓN",
                "estandares": [
                    "3407",
                    "3403"
                ]
            },
            {
                "miactividad": "Desarrollo de un programa estructural de la investigación educativa de las carreras profesionales del IFD. (ISEPCH). ",
                "actor_cargo": "COORDINACIÓN DE INVESTIGACIÓN",
                "estandares": [
                    "3405"
                ]
            },
            {
                "miactividad": "Equipamiento bibliográfico, de laboratorio e informático para el desarrollo de las actividades investigativas  en las tres carreras profesionales de IFD. (PROCALIDAD). ",
                "actor_cargo": "COORDINACIÓN DE INVESTIGACIÓN",
                "estandares": [
                    "3406"
                ]
            }
        ],
        "3.1": [
            {
                "miactividad": "Implementación de  políticas y procedimientos para el desarrollo del personal administrativo. ",
                "actor_cargo": "JEFATURA DE UNIDAD ADMINISTRATIVA.",
                "estandares": [
                    "3408"
                ]
            }
        ],
        "3.3": [
            {
                "miactividad": "Implementación de lineamientos y estrategias de prevención de la deserción estudiantil de las carreras profesionales del IFD. ",
                "actor_cargo": "COORDINACIÓN TUTORÍA Y CONSEJERÍA",
                "estandares": [
                    "3418"
                ]
            }
        ],
        "3.4": [
            {
                "miactividad": "Seguimiento del mantenimiento preventivo y mejora de la infraestructura, mobiliario y equipos de las carreras profesionales del IFD.",
                "actor_cargo": "JEFATURA DE UNIDAD ADMINISTRATIVA.",
                "estandares": [
                    "3421",
                    "3426"
                ]
            },
            {
                "miactividad": "Implementación y operativización permanente de recursos de mantenimiento preventivo y correctivo para optimizar el proceso enseñanza – aprendizaje de las carreras profesionales del IFD. ",
                "actor_cargo": "JEFATURA DE UNIDAD ADMINISTRATIVA.",
                "estandares": [
                    "3421",
                    "3426"
                ]
            }
        ],
        "4.1": [
            {
                "miactividad": "Diseño, ejecución y evaluación del plan de mejora de imagen institucional del IFD. \n ",
                "actor_cargo": "COORDINACIÓN DE IMAGEN INSTITUCIONAL",
                "estandares": [
                    "3428"
                ]
            }
        ],
        "4.2": [
            {
                "miactividad": "Implementación de lineamientos y estrategias de sostenibilidad de la proyección social y de alianzas estratégicas de las carreras profesionales del IFD. ",
                "actor_cargo": "COORDINACIÓN DE IMAGEN INSTITUCIONAL",
                "estandares": [
                    "3430",
                    "3431"
                ]
            }
        ],
        "4.3": [
            {
                "miactividad": "Implementación de lineamientos y estrategias de seguimiento de egresados de las carreras profesionales del IFD. ",
                "actor_cargo": "COORDINACIÓN DE IMAGEN INSTITUCIONAL",
                "estandares": [
                    "3432",
                    "3433",
                    "3434"
                ]
            }
        ]
    },
    "datos": {
        "com": {
            "nombre": "MONTOYA VARGAS, Angela",
            "cargo": "DIRECTORA GENERAL",
            "date": "2015-10-26",
            "date_local": "26 de Octubre del 2015"
        },
        "dir": {
            "nombre": "MONTOYA VARGAS, Angela",
            "cargo": "DIRECTORA GENERAL",
            "date": "2015-10-26",
            "date_local": "26 de Octubre del 2015"
        }
    }
}