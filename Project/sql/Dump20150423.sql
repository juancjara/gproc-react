-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: gproc
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acreditado`
--

DROP TABLE IF EXISTS `acreditado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acreditado` (
  `acre_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`acre_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `fk_acreditado_user1_idx` (`user_id`),
  CONSTRAINT `fk_acreditado_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acreditado`
--

LOCK TABLES `acreditado` WRITE;
/*!40000 ALTER TABLE `acreditado` DISABLE KEYS */;
INSERT INTO `acreditado` VALUES (1,1),(2,2),(3,3),(4,4);
/*!40000 ALTER TABLE `acreditado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acreditado_etapa`
--

DROP TABLE IF EXISTS `acreditado_etapa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acreditado_etapa` (
  `acet_id` int(11) NOT NULL AUTO_INCREMENT,
  `acet_estado` int(11) DEFAULT NULL,
  `acet_finalizado` int(11) DEFAULT NULL,
  `acev_id` int(11) NOT NULL,
  `acet_etapa` int(11) NOT NULL,
  `acet_data` text,
  `acet_datam` text NOT NULL,
  PRIMARY KEY (`acet_id`),
  KEY `fk_etapa_acreditado_evento1_idx` (`acev_id`),
  CONSTRAINT `fk_etapa_acreditado_evento1` FOREIGN KEY (`acev_id`) REFERENCES `acreditado_evento` (`acev_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acreditado_etapa`
--

LOCK TABLES `acreditado_etapa` WRITE;
/*!40000 ALTER TABLE `acreditado_etapa` DISABLE KEYS */;
INSERT INTO `acreditado_etapa` VALUES (3,1,0,1,1,'{\"acet_etapa\":\"1\",\"dimensiones\":{\"1\":{\"objetivo\":\"Capacitar a todos los directores, coordinadores, supervisores, en uso de herramientas TI ligadas al \\u00e1rea de gesti\\u00f3n pedag\\u00f3gica\"},\"2\":{\"objetivo\":\"\"},\"3\":{\"objetivo\":\"\"},\"4\":{\"objetivo\":\"\"}},\"factores\":{\"1.1\":{\"resultado\":\"Autoridades capacitadas\"},\"1.2\":{\"resultado\":\"\"},\"1.3\":{\"resultado\":\"\"},\"1.4\":{\"resultado\":\"\"},\"2.1\":{\"resultado\":\"\"},\"2.5\":{\"resultado\":\"\"},\"2.6\":{\"resultado\":\"\"},\"3.1\":{\"resultado\":\"\"},\"3.2\":{\"resultado\":\"\"},\"3.3\":{\"resultado\":\"\"},\"3.4\":{\"resultado\":\"\"},\"4.1\":{\"resultado\":\"\"},\"4.2\":{\"resultado\":\"\"},\"4.3\":{\"resultado\":\"\"}},\"actividades\":{\"1.1.3\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"1.1.3\",\"porcentaje\":\"20\"},{\"estandar\":\"1.4.2\",\"porcentaje\":\"21\"}],\"costototal\":\"13000\",\"costofec\":\"10000\",\"costopropio\":\"2000\",\"costootros\":\"1000\",\"actor_nombres\":\"David\",\"actor_dni\":\"45454545\",\"actor_telefono\":\"987789789\",\"actor_cargo\":\"Coordinador de Log\\u00edstica\",\"actor_email\":\"david@gmail.com\",\"ejecucion\":\"10\"}],\"1.2.1\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"1.3.2\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"1.3.4\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"1.3.6\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"1.4.2\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"1.4.3\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.1.1\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.1.6\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.1.7\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.5.2\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.6.3\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.6.4\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.6.5\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.1.2\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.1.3\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.2.2\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.2.3\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.3.1\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.3.2\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.4.1\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.4.4\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.4.7\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"4.1.2\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"4.2.2\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"4.3.4\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}]},\"save_only\":\"1\"}',''),(4,NULL,NULL,2,1,'{\"acet_etapa\":\"1\",\"dimensiones\":{\"1\":{\"objetivo\":\"Capacitar\"},\"2\":{\"objetivo\":\"\"},\"3\":{\"objetivo\":\"\"}},\"factores\":{\"1\":{\"resultado\":\"\"},\"2\":{\"resultado\":\"\"},\"3\":{\"resultado\":\"\"},\"4\":{\"resultado\":\"\"},\"5\":{\"resultado\":\"\"},\"6\":{\"resultado\":\"\"},\"7\":{\"resultado\":\"\"},\"9\":{\"resultado\":\"\"}},\"actividades\":{\"5\":[{\"elegible\":\"3\",\"estandares\":[{\"estandar\":\"13\",\"porcentaje\":\"\"}],\"costototal\":\"40000\",\"costofec\":\"30000\",\"costopropio\":\"10000\",\"costootros\":\"0\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"8\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"10\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"13\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"15\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"16\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"17\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"18\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"24\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"25\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"26\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"29\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"31\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"32\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"33\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"35\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"37\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"38\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"40\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"42\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"45\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"46\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"47\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"51\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"56\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"58\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"62\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"63\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"67\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"69\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"77\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"78\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"81\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"84\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"85\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"86\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"87\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"89\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"90\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"96\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"98\":[{\"elegible\":\"0\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}]},\"save_only\":\"1\"}','');
/*!40000 ALTER TABLE `acreditado_etapa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acreditado_evento`
--

DROP TABLE IF EXISTS `acreditado_evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acreditado_evento` (
  `acev_id` int(11) NOT NULL AUTO_INCREMENT,
  `acre_id` int(11) NOT NULL,
  `even_id` int(11) NOT NULL,
  PRIMARY KEY (`acev_id`),
  KEY `fk_acreditado_evento_acreditado1_idx` (`acre_id`),
  KEY `fk_acreditado_evento_evento1_idx` (`even_id`),
  CONSTRAINT `fk_acreditado_evento_acreditado1` FOREIGN KEY (`acre_id`) REFERENCES `acreditado` (`acre_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_acreditado_evento_evento1` FOREIGN KEY (`even_id`) REFERENCES `evento` (`even_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acreditado_evento`
--

LOCK TABLES `acreditado_evento` WRITE;
/*!40000 ALTER TABLE `acreditado_evento` DISABLE KEYS */;
INSERT INTO `acreditado_evento` VALUES (1,3,9),(2,4,9);
/*!40000 ALTER TABLE `acreditado_evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acreditado_ficha`
--

DROP TABLE IF EXISTS `acreditado_ficha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acreditado_ficha` (
  `acfi_id` int(11) NOT NULL AUTO_INCREMENT,
  `acev_id` int(11) NOT NULL,
  `evfi_id` int(11) NOT NULL,
  `acfi_data` text,
  `acfi_estado` enum('pendiente','proceso','cerrado') DEFAULT NULL,
  `adfi_documento` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`acfi_id`),
  KEY `fk_acreditado_ficha_acreditado_evento1_idx` (`acev_id`),
  KEY `fk_acreditado_ficha_evento_ficha1_idx` (`evfi_id`),
  CONSTRAINT `fk_acreditado_ficha_acreditado_evento1` FOREIGN KEY (`acev_id`) REFERENCES `acreditado_evento` (`acev_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_acreditado_ficha_evento_ficha1` FOREIGN KEY (`evfi_id`) REFERENCES `evento_ficha` (`evfi_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acreditado_ficha`
--

LOCK TABLES `acreditado_ficha` WRITE;
/*!40000 ALTER TABLE `acreditado_ficha` DISABLE KEYS */;
/*!40000 ALTER TABLE `acreditado_ficha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acreditado_nivel_etapa`
--

DROP TABLE IF EXISTS `acreditado_nivel_etapa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acreditado_nivel_etapa` (
  `acne_id` int(11) NOT NULL,
  `acne_aprobado` int(11) DEFAULT NULL,
  `acne_comentario` varchar(500) DEFAULT NULL,
  `eval_id` int(11) NOT NULL,
  `acet_id` int(11) NOT NULL,
  PRIMARY KEY (`acne_id`),
  KEY `fk_acreditado_nivel_evaluador1_idx` (`eval_id`),
  KEY `fk_acreditado_nivel_etapa_acreditado_etapa1_idx` (`acet_id`),
  CONSTRAINT `fk_acreditado_nivel_etapa_acreditado_etapa1` FOREIGN KEY (`acet_id`) REFERENCES `acreditado_etapa` (`acet_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_acreditado_nivel_evaluador1` FOREIGN KEY (`eval_id`) REFERENCES `evaluador` (`eval_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acreditado_nivel_etapa`
--

LOCK TABLES `acreditado_nivel_etapa` WRITE;
/*!40000 ALTER TABLE `acreditado_nivel_etapa` DISABLE KEYS */;
/*!40000 ALTER TABLE `acreditado_nivel_etapa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento`
--

DROP TABLE IF EXISTS `documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento` (
  `docu_id` int(11) NOT NULL,
  `docu_nombre` varchar(45) DEFAULT NULL,
  `docu_fecha` datetime DEFAULT NULL,
  `acne_id` int(11) NOT NULL,
  PRIMARY KEY (`docu_id`),
  KEY `fk_documento_acreditado_nivel_etapa1_idx` (`acne_id`),
  CONSTRAINT `fk_documento_acreditado_nivel_etapa1` FOREIGN KEY (`acne_id`) REFERENCES `acreditado_nivel_etapa` (`acne_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento`
--

LOCK TABLES `documento` WRITE;
/*!40000 ALTER TABLE `documento` DISABLE KEYS */;
/*!40000 ALTER TABLE `documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evaluador`
--

DROP TABLE IF EXISTS `evaluador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evaluador` (
  `eval_id` int(11) NOT NULL,
  `eval_nombre` varchar(45) DEFAULT NULL,
  `eval_dni` int(11) DEFAULT NULL,
  `eval_perfil` enum('directorio','normal') DEFAULT NULL,
  PRIMARY KEY (`eval_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evaluador`
--

LOCK TABLES `evaluador` WRITE;
/*!40000 ALTER TABLE `evaluador` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `even_id` int(11) NOT NULL AUTO_INCREMENT,
  `even_descripcion` text,
  `even_url_pdf` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`even_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
INSERT INTO `evento` VALUES (9,'La mejora continua de la calidad de la educación superior es un reto que el Perú afronta para contribuir al incremento de su competitividad. En ese marco, el Proyecto “Mejoramiento de la Calidad de la Educación Superior” – PROCALIDAD propone impulsar los procesos de acreditación de las Instituciones de Educación Superior.\n\nEl Fondo de Estímulo a la Calidad - FEC, es el medio a través del cual PROCALIDAD otorga incentivos financieros a las Instituciones Públicas de Educación Superior (Institutos y Universidades) que ofrecen carreras de formación docente, ciencias de la salud y, ciencia y tecnología (incentivo a las carreras); así como para las instituciones propiamente dichas (incentivo a la institución), que se encuentran realizando esfuerzos por lograr su acreditación o buscar su excelencia.','http://procalidad.gob.pe/documentos/concurso-fec/3convocatoria/Bases-TERCER-CONCURSO-FINANCIAMIENTO-PM-EE-14012015-V2.pdf');
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento_etapa`
--

DROP TABLE IF EXISTS `evento_etapa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento_etapa` (
  `evet_id` int(11) NOT NULL,
  `evet_nombre` varchar(45) DEFAULT NULL,
  `evet_fecha_inicio_vigencia` date DEFAULT NULL,
  `evet_fecha_fin_vigencia` date DEFAULT NULL,
  `evet_fecha_inicio_correccion` date DEFAULT NULL,
  `evet_fecha_fin_correccion` date DEFAULT NULL,
  `even_id` int(11) NOT NULL,
  PRIMARY KEY (`evet_id`),
  KEY `fk_evento_etapa_evento1_idx` (`even_id`),
  CONSTRAINT `fk_evento_etapa_evento1` FOREIGN KEY (`even_id`) REFERENCES `evento` (`even_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento_etapa`
--

LOCK TABLES `evento_etapa` WRITE;
/*!40000 ALTER TABLE `evento_etapa` DISABLE KEYS */;
INSERT INTO `evento_etapa` VALUES (1,'Desarrollo del Proceso de Autoevaluación','2015-04-06','2015-04-10','2015-04-13','2015-04-17',9),(2,'Registro de Expresión de Interés','2015-04-13','2015-04-17','2015-04-20','2015-04-24',9),(3,'Registro de Planes de Mejora','2015-04-20','2015-04-24','2015-04-26','2015-04-30',9),(4,'Registro de Documentación Institucional','2015-05-01','2015-05-04','2015-04-06','2015-04-10',9);
/*!40000 ALTER TABLE `evento_etapa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento_ficha`
--

DROP TABLE IF EXISTS `evento_ficha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento_ficha` (
  `evfi_id` int(11) NOT NULL AUTO_INCREMENT,
  `even_id` int(11) NOT NULL,
  `evfi_nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`evfi_id`),
  KEY `fk_evento_ficha_evento1_idx` (`even_id`),
  CONSTRAINT `fk_evento_ficha_evento1` FOREIGN KEY (`even_id`) REFERENCES `evento` (`even_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento_ficha`
--

LOCK TABLES `evento_ficha` WRITE;
/*!40000 ALTER TABLE `evento_ficha` DISABLE KEYS */;
/*!40000 ALTER TABLE `evento_ficha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento_nivel`
--

DROP TABLE IF EXISTS `evento_nivel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento_nivel` (
  `evni_id` int(11) NOT NULL,
  `evni_nombre` varchar(45) DEFAULT NULL,
  `evni_fecha_inicio_evaluado` datetime DEFAULT NULL,
  `evni_fecha_fin_evaluado` datetime DEFAULT NULL,
  `even_id` int(11) NOT NULL,
  PRIMARY KEY (`evni_id`),
  KEY `fk_evento_nivel_evento1_idx` (`even_id`),
  CONSTRAINT `fk_evento_nivel_evento1` FOREIGN KEY (`even_id`) REFERENCES `evento` (`even_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento_nivel`
--

LOCK TABLES `evento_nivel` WRITE;
/*!40000 ALTER TABLE `evento_nivel` DISABLE KEYS */;
/*!40000 ALTER TABLE `evento_nivel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `financiamiento`
--

DROP TABLE IF EXISTS `financiamiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `financiamiento` (
  `fina_id` int(11) NOT NULL,
  `fina_nombre` varchar(45) DEFAULT NULL,
  `fina_monto_solicitado` int(11) DEFAULT NULL,
  `fina_monto_financiar` int(11) DEFAULT NULL,
  `fina_fecha` varchar(45) DEFAULT NULL,
  `acev_id` int(11) NOT NULL,
  PRIMARY KEY (`fina_id`),
  KEY `fk_financiamiento_acreditado_evento1_idx` (`acev_id`),
  CONSTRAINT `fk_financiamiento_acreditado_evento1` FOREIGN KEY (`acev_id`) REFERENCES `acreditado_evento` (`acev_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `financiamiento`
--

LOCK TABLES `financiamiento` WRITE;
/*!40000 ALTER TABLE `financiamiento` DISABLE KEYS */;
/*!40000 ALTER TABLE `financiamiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) DEFAULT NULL,
  `clave` varchar(45) DEFAULT NULL,
  `perfil` int(11) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='						';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,NULL,1,NULL),(2,NULL,NULL,1,NULL),(3,NULL,NULL,1,NULL),(4,NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-23  9:21:25
