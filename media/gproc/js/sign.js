jQuery(function ($) {
	
	// Placeholder =============================================================
	
	$('select.placeholder').each(togglePlaceholder);
	$('select.placeholder').change(togglePlaceholder);
	
	function togglePlaceholder () {
		var $this = $(this);
		if ($this.val()) $this.removeClass('placeholder');
		else $this.addClass('placeholder');
	}
	
	// Obac and inst ===========================================================
	
	$('#signup-obac').change(function () {
		var $this = $(this),
			$target = $($this.data('target'));
			val = $this.val() || 0,
			tipo = $this.children('[value=' + val + ']').data('tipo');
		
		if ( ! val) return;
		//console.log(tipo);
		
		if (tipo == 1) {
			
			$target.prop('required', false).hide();
			
		} else {
			$target.prop('required', true).show();
		}
		
	});
	
	// Career other ============================================================
	
	$('#carr_id').change(function () {
		var $this = $(this),
			val = $this.val(),
			$carr_nombre = $('#carr_nombre_wrapper');
		
		if (val === '0')
		{
			$carr_nombre.removeClass('hidden').find('input').prop('required', true);
		}
		else
		{
			$carr_nombre.addClass('hidden').find('input').prop('required', false);
		}
	});
	
	// Career Name =============================================================
	
	$('#signup-credit-type').change(toggleCareerName);
	$('#signup-credit-type').each(toggleCareerName);
	//toggleCareerName.call($('#signup-credit-type'));
	
	function toggleCareerName () {
		var $this = $(this), val = $this.val();
		if (val == 'institucion') {
			$('#signup-career-name').prop('required', false).hide();
			//$('#signup-career-type').prop('required', false).hide();
		}
		else if (val == 'carrera') {
			$('#signup-career-name').prop('required', true).show();
			//$('#signup-career-type').prop('required', true).show();
		}
	}
	
	// Committee member add ====================================================
	
	$('#signup-member-add').click(function () {
		var $table = $('.committee'),
			$tbody = $('tbody', $table),
			$row = $('tr:first', $tbody).clone(),
			n = $tbody.children().length;
		
		console.log(n);
		
		$row.find('input[type=radio]').prop('checked', false).val(n);
		$row.find('input[type=text]').val('').prop('required', false);
		$row.appendTo($tbody);
	});
	
	// Committee contact change ================================================
	
	$('.committee').on('change', 'input[type=radio]', changeContact);
	changeContact.call($('.committee input[type=radio]:checked'));
	
	function changeContact () {
		var $tr = $(this).parents('tr');
		$('.committee').find('input[type=text]').prop('required', false);
		$tr.find('input[type=text]').prop('required', true);
	}
	
	// Select ajax =============================================================
	
	$('select.ajax').change(getOptions);
	$('select.ajax').each(getOptions);
	
	function getOptions() {
		var $this = $(this),
			id = $this.val(),
			action = $this.data('action'),
			actions = (typeof action == 'string') ? [action] : action,
			//url = ['/ajax', $action, id].join('/'),
			target = $this.data('target'),
			targets = (typeof target == 'string') ? [target] : target,
			$targets = targets.map(function (a) { return $(a); }),
			i, n = actions.length;
			
		for (i = 0; i < n; i++) {
			url = '/ajax/' + [actions[i],id].join('/');
			
			callAjax(url, $targets[i]);
		}
	}
	
	function callAjax(url, $target) {
		$.get(url).done(function (data) {
			$target.html(data);
			$target.addClass('placeholder');
		});
	}
	
	// Input ajax ==============================================================
	
	$('input.ajax').blur(getResponse);
	//$('input.ajax').each(getResponse);
	
	function getResponse() {
		var $this = $(this),
			$wrapper = $this.parent(),
			val = $this.val(),
			action = $this.data('action'),
			url = [action, val].join('/'),
			alert = ['<p class="text-danger">', '</p>'];
		
		if ( ! val) return;
		
		$.get(url).done(function (data) {
			data = JSON.parse(data);
			
			$wrapper.find('.text-danger').remove();
			
			if (data.status == 'ok') {
				$wrapper.removeClass('has-error');
			} else {
				$wrapper.addClass('has-error');
				$wrapper.append(alert[0] + data.msg + alert[1]);
			}
		});
	}
	
});

//LOGIN 2
//Vermas de login
	$('a#vermas').on('click',function(){
		$('div#login1').css('display','none');
		$('div#login2').css('display','block');
	});
	
//Video de login
	$('a#video-txt').on('click',function(){
		$('div#login3').css('display','block');
		$('div#login1').css('display','none');
		$('div#login2').css('display','none');
	});
	
	$('img#video-img').on('click',function(){
		$('div#login1').css('display','none');
		$('div#login2').css('display','none');
		$('div#login3').css('display','block');
	});
//Volver a login1
	$('a#volver').on('click',function(){
		$('div#login2').css('display','none');
		$('div#login1').css('display','block');
	});
	
	$('a#volver_video').on('click',function(){
		
		$('div#login1').css('display','block');
		$('div#login3').css('display','none');
		$('div#login2').css('display','none');
	});

//img-cuadro1, expandir tipo popup
	$('img#img_table_small').on('click',function(){
		$('div#div_table_big').css('display', 'block');
		$('div#login_container').css('display', 'none');
	});

	$('div#div_table_big').on('click', function(){
		$('div#div_table_big').css('display', 'none');
		$('div#login_container').css('display', 'block');
	});

//tooltip para tabla del login
	$('img#img_table_small').tooltip({'title': 'click para aumentar', 'trigger': 'manual'});
	
	$('img#img_table_small').hover(function(){
		$('img#img_table_small').tooltip('show');
		}, function(){
			$('img#img_table_small').tooltip('hide');
	});
	
	$('div#div_table_big').tooltip({'title': 'click para reducir', 'trigger': 'manual'});
	
	$('div#div_table_big').hover(function(){
		$('div#div_table_big').tooltip('show');
		}, function(){
			$('div#div_table_big').tooltip('hide');
	});

//FIN LOGIN 2