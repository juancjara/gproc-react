$(function () {
	
	$('.reports').each(function () {
		var $this = $(this),
			data = $this.data('json'),
			base = {
				chart: {
					type: 'column',
					margin: [100, 75, 75, 75],
					spacingTop: 0,
					options3d: {
						enabled: true,
						alpha: 15,
						beta: 15,
						depth: 110,
						frame: {
							back: {
								size: 10,
								//color: '#c0c0c0'
							},
							bottom: {
								size: 10,
								//color: '#c0c0c0'
							},
							side: {
								size: 10,
								//color: '#d0d0d0'
							}
						}
					}
				},
				plotOptions: {
					column: {
						depth: 80,
						//stacking: true,
						grouping: false,
						groupZPadding: 80,
						dataLabels: {
							//enabled: true,
							align: 'center',
							verticalAlign: 'middle',
							defer: false
						}
					},
					series: {
						dataLabels: {
							//enabled: true,
							//align: 'center',
							//x: 20,
							//y: -30
						}
					}
				},
			},
			
			object = $.extend({}, base, data);
		
		console.log(object);
		
		$this.highcharts(object);
	})
	
	var data = [{
        label: "Series 0",
        data: 1
    }, {
        label: "Series 1",
        data: 3
    }, {
        label: "Series 2",
        data: 9
    }, {
        label: "Series 3",
        data: 20
    }];

    /*var plotObj = $.plot($("#flot-pie-chart"), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        }
    });*/
	
	$('.flot-chart-content').each(function () {
		var $this = $(this),
			properties = $this.data('properties'),
			data = $this.data('json');
		
		console.log(data);
		console.log(properties);
		
		$.plot($this, data, properties);
	});
	
	//$a = $('.flot-chart-content').data('json');
	//console.log($a);
	
});
