Saes = {};

Saes.scroll = {
	'offset'	: 200 + 52,
	'duration'	: '500',
	'animate'	: function (target) {
		$('html,body').animate({scrollTop:$(target).offset().top - this.offset}, this.duration); // Animate the scroll to this link's href value
	},
	'setData'	: function (data) {
		$('#result').html(data.result);
	}
};

jQuery(function ($) {
	
	// Placeholder =============================================================
	
	$('select.placeholder').change(togglePlaceholder);
	$('select.placeholder').each(togglePlaceholder);
	
	function togglePlaceholder () {
		var $this = $(this);
		if ($this.val()) $this.removeClass('placeholder');
		else $this.addClass('placeholder');
	}
	
	// Select ajax =============================================================
	
	$('select.ajax').change(getOptions);
	$('select.ajax.first').each(getOptions);
	
	function getOptions() {
		var $this = $(this),
			val = $this.val(),
			action = $this.data('action'),
			url = ['/ajax', action, val].join('/'),
			$target = $($this.data('target'));
		
		$.get(url).done(function (data) {
			setOptions(data, $target);
		});
	}
	
	function setOptions(data, $target) {
		$target.html(data);
		$target.addClass('placeholder');

		if ($target.attr('data-selected')) {
			
			$target.find('option[value=' + $target.attr('data-selected') + ']').prop('selected', true);
			//$target.attr('data-selected', '0');

			if ($target.val()) {
				$target.removeClass('placeholder');
			}
			
			if ($target.hasClass('ajax')) {
				//console.log($target);
				$target.each(getOptions);
			}
		}
	}

	// Select ajax multiple ====================================================
	
	$('select.ajax-multiple').change(getOptionsMultiple);
	$('select.ajax-multiple').each(getOptionsMultiple);
	
	function getOptionsMultiple() {
		if ( ! $(this).val()) return;
		
		var $this = $(this),
			$action = $this.data('action'),
			$val = $this.val().join('-'),
			$url = ['/ajax', $action, $val].join('/') + '?multiple=true',
			$target = $($this.data('target'));
		
		$.get($url).done(function (data) {
			$target.html(data);
		});
	}
	
	// nav-navbar-toggle =======================================================
	
	$('.nav-navbar-toggle').click(toggleNavbar);
	
	function toggleNavbar () {
		var $icon = $(this).find('i'),
			$navbar = $('.nav-navbar'),
			$wrapper = $('#page-wrapper'),
			$header = $('.header-fixed');
			
		if ($navbar.hasClass('show')) {
			$icon.removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
			$navbar.removeClass('show');
			$wrapper.removeClass('with-navbar');
			$header.removeClass('with-navbar');
			Saes.scroll.offset = 70 + 52;
		} else {
			$icon.removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
			$navbar.addClass('show');
			$wrapper.addClass('with-navbar');
			$header.addClass('with-navbar');
			Saes.scroll.offset = 200 + 52;
		}
	}
	
	// arrow back ==============================================================
	
	$('.arrow-back').click(function (e) {
		e.preventDefault();
		//window.history.go(-1);
		history.back();
	});
	
	// btn print ===============================================================
	
	$('.btn-print').click(function (e) {
		e.preventDefault();
		window.print();
	});
	
	// btn excel ===============================================================
	
	$('.btn-excel').click(function (e) {
		e.preventDefault();
		var url = location.href + '?ajax=1';
		//console.log(url);
		
		$('.iframe').attr('src', url);
		
	});
	
	$('.btn-pdf').click(function (e) {
		e.preventDefault();
		var url = location.href + '?ajax=1&pdf=1';
		//console.log(url);
		
		$('.iframe').attr('src', url);
		
	});
	
	// Confirm
	
	$('[data-confirm]').click(function () {
		var msg = $(this).data('confirm') || 'Está usted seguro(a)?';
		
		return confirm(msg);
	});
	
	// Start ===================================================================
	
	$('.start-select-all').click(function () {
		var $select = $(this).parent().find('select');
		
		$select.find('option').prop('selected', true);
		if ($select.hasClass('ajax-multiple')) $select.each(getOptionsMultiple);
	});
	
	// Calculator ==============================================================
	
	// - Alternative document
	$('.alt-doc-cb').change(toggleAlternative);
	$('.alt-doc-cb').each(toggleAlternative);
	
	function toggleAlternative () {
		var $this = $(this),
			$altDoc = $this.closest('.row').find('.alt-doc');
		
		if ($this.prop('checked')) {
			$altDoc.removeClass('hidden').find('input').prop('required', true);
		} else {
			$altDoc.addClass('hidden').find('input')./*val('').*/prop('required', false);
		}
	}
	
	// - Accomplishment
	$('.concept-accomp').change(toggleQuality);
	$('.concept-accomp').each(toggleQuality);
	
	function toggleQuality () {
		var $this = $(this),
			$quality = $this.closest('.row').find('.concept-quality');
		
		if ($this.prop('checked')) {
			$quality.prop('required', true).prop('disabled', false);
		} else {
			$quality.val('').addClass('placeholder').prop('required', false).prop('disabled', true);
		}
	}
	
	// - Accomplishment level
	$('.accomplishment-level').change(setConceptRequired);
	$('.accomplishment-level[checked]').each(setConceptRequired);
	
	function setConceptRequired() {
		var $this = $(this),
			$addons = $this.closest('.row').find('.concept-addon'),
			$checks = $this.closest('.row').find('.concept-accomp'),
			condicion = $this.data('condicion').toString(),
			i, n = condicion.length;
		
		//console.log($addons);
		//console.log($checks);
		//console.log(condicion);
		
		$addons.removeClass('con-req-1').addClass('con-req-0');
		$checks.prop('required', false);
		
		for (i = 0; i < n; i++) {
			if (condicion[i] == 1) {
				$addons.eq(i).removeClass('con-req-0').addClass('con-req-1');
				$checks.eq(i).prop('required', true);
			}
		}
	}
	
	// - Form Standard
	$('.form-standard').submit(function (e) {
		e.preventDefault();
		
		var $this = $(this),
			$action = $this.attr('action'),
			$data = $this.serialize(),
			$button = $this.find('.btn-standard-save');
		
		//console.log($data);
		$button.prop('disabled', true);
		
		$.post($action, $data).done(function (data) {
			//console.log(data);
			
			data = JSON.parse(data);
			if (data.status == 'ok') {
				$('#result').html(data.result);
				$button.prop('disabled', false);
			}
		});
	});
	
});

/* =============================================================

    Smooth Scroll 1.1
    Animated scroll to anchor links.

    Script by Charlie Evans.
    http://www.sycha.com/jquery-smooth-scrolling-internal-anchor-links

    Rebounded by Chris Ferdinandi.
    http://gomakethings.com

    Free to use under the MIT License.
    http://gomakethings.com/mit/
    
 * ============================================================= */

(function ($) {
    jQuery(document).ready(function ($) {
        //$(".scroll").click(function(event){ // When a link with the .scroll class is clicked
        $('a[href*=#]').click(function (e){ // When a link with the .scroll class is clicked
			if ($(this).attr('href') == '#') return;
            e.preventDefault(); // Prevent the default action from occurring
			
			Saes.scroll.animate(this.hash);
        });
		
		hash = document.location.hash;
		
		if (hash) {
			Saes.scroll.animate(hash);
		}
		
		$('#criterion-standard option').click(function () {
			var target = $(this).data('target');
			console.log(target);
			
			Saes.scroll.animate(target);
		})
		
    });
})(jQuery);

$(window).on('hashchange', function() {
	
});

// Alerts
window.setTimeout(function() {
	$('.alert-message').fadeTo(250, 0).slideUp(250, function(){
		$(this).remove(); 
	});
}, 3000);
