/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

/*
 * The functions inside this block will get executed AFTER page load, 
 * overwritting previous functionality as event handlers, if they exist
 */
$(function() {
	$('.evaluacion_virtual').iCheck('destroy');
	$('.fichas_estado').iCheck('destroy');
	
	$('.evaluacion_virtual').change(function() { //.on('ifToggled', function (e) {
		var $this = $(this);
		
		if (confirm('Está usted seguro?'))
			$this.closest('form').submit();
		else
			$this.prop('checked', ! $this.prop('checked'));
	});
	
	$('.fichas_estado').change(function() {
		var $this = $(this),
			val = $this.val(),
			checked = $this.prop('checked'),
			$siblings = $('.fichas_estado').slice(1);
			console.log(val, checked);
		
		if (val == 'MPL') {
			$siblings
				.prop('checked', checked)
				.prop('disabled', checked);
		}
		
		if (val == 'MPR') {
			$siblings.slice(1, 2)
				.prop('checked', checked)
				.prop('disabled', checked);
		}
	});
	
});

/*
 * Functions inside this block will get executed inmediatly 
 * (i.e. this callbacks will get registered BEFORE page load)
 */

var ESTADO_OBSERVADO = 40;

(function($) {
	
	$('.estado_nivel').change(onEstadoNivelChange);
	
	function onEstadoNivelChange() {
		
		var $this = $(this),
			val = $this.val(),
			$file = $this.next().find('.btn_file_upload'),
			$input = $file.find('input'),
			$checkboxes = $this.closest('form').find('.fichas_estado_wrapper'),
			$plazo_dias = $this.closest('form').find('.plazo_dias_wrapper');
		
		if (val == ESTADO_OBSERVADO) {
			$file.removeClass('disabled');
			$input.prop('required', true);
			$checkboxes.removeClass('hidden');
			$plazo_dias.removeClass('hidden');
			$plazo_dias.find('input').prop('required', true);
		}
		else {
			$file.addClass('disabled');
			$input.prop('required', false);
			$checkboxes.addClass('hidden');
			$plazo_dias.addClass('hidden');
			$plazo_dias.find('input').prop('required', false);
		}
		
	}
	
})(jQuery);

(function ($) {
	
	$('.evaluacion_virtual').change(function() {//.on('ifToggled', function (e) {
		console.log('foo');
		if (confirm('Está seguro?'))
			$(this).closest('form').submit();
		else ;
			//$(this).iCheck('toggle');
	});
	
})(jQuery);

(function ($) {
	
	$('.ampliar_plazo').click(function (e) {
		e.preventDefault();
		
		$('.ampliar_plazo_wrapper')
			.removeClass('hidden')
			.find('input')
			.prop('required', true)
			.focus();
	});
	
})(jQuery);
