/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

var estandares = {};

jQuery(function ($) {
	$('input[type=radio]').iCheck('destroy');
});
/*Planes Mejora - matriz planificación*/

	/*actividad add_tr_gasto*/
	
(function ($) {

	$('.btn_save').click(function () {
		//alert("guardando");
		var $this = $(this),
			val = $this.val(),
			//$input = $this.siblings('input'),
			$form = $('.ficha');
			
		//console.log(val);
		if (val == 1) {
			$form.submit();
			return;
		}
		$form.submit();
	});
	
	$('.ficha').on('change', '.concept-file', function () {
		var $this = $(this),
			$inputs = $this.parent(),
		
			$concepto = $this.closest('.td-pdf'),
			$upload = $concepto.find('.file_upload'),
			$process = $concepto.find('.file_process'),
			$file_view = $concepto.find('.btn_file_view'),
			$filename = $file_view.siblings('input'),
			
			//$form = $this.closest('.ficha'),
			//$btn_save = $form.find('.btn-save'),
			
			$outside = $('.form_outside'),
			form, xhr;
		
		if ( ! $this.val()) return;
		//console.log($filename);return;
		
		// processing...
		$upload.addClass('hidden');
		$process.removeClass('hidden');
		//$btn_save.prop('disabled', true);
		$file_view.addClass('disabled').tooltip('destroy');
		
		//console.log($file_view);
		//$file_view.removeAttr('disabled').tooltip();return;
		
		// submit
		$inputs.appendTo($outside);
		form = $outside.ajaxSubmit();
		xhr = form.data('jqxhr');
		
		// done
		xhr.done(function (data) {
			console.log('success');
			$filename.val(data);
			console.log(data);
			$file_view.attr('href', '/file/' + data);
			console.log($file_view);
			$this.prop('required', false);
			finish();
			
		}).error(function (x,y,z) {
			console.log('error', x, y, z);
			
			alert('No se pudo subir el archivo. Por favor inténtelo nuevamente');
			finish();
		});
		
		function finish() {
			$this.val('');
			$upload.append($inputs).removeClass('hidden');
			$process.addClass('hidden');

			if ($filename.val()) {
				$file_view.removeClass('disabled').tooltip();
			}
		}
		
	});
})(jQuery);
