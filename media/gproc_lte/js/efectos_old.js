/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

var estandares = {};

jQuery(function ($) {
	$('input[type=radio]').iCheck('destroy');
	$('input[type=checkbox]').iCheck('destroy');
});

(function ($) {

	$('.btn_finish, .btn_save').click(function () {
		var $this = $(this),
			finish = $this.val(),
			$form = $('.ficha');
			//console.log(finish);
		
		$form.data('finish', finish);
		
		if (finish == 0) {
			$form.submit(); // Force no validate
			return false;
		}
		
		var success = 
			check_estandares() && 
			check_totales();
			
		return success;
	});
	

})(jQuery);

/*function justNumbers(e) {
	
	var keynum = window.event ? window.event.keyCode : e.which;
		if ((keynum == 8) || (keynum == 46))
	return true;

	return /\d/.test(String.fromCharCode(keynum));
}*/

var isDirty = false,
	setDirty = function () { console.log('dirty'); isDirty = true; };

(function ($) {
	
	var lastVal,
		//isDirty = false,
		formSubmitting = false;
		//setDirty = function () { isDirty = true; };
	
	$('.ficha').on('focus', 'input, select, textarea', function (e) {
		//console.log(e.target.value);
		lastVal = e.target.value;
	});
	
	$('.ficha').on('blur', 'input, select, textarea', function (e) {
		if (e.target.value != lastVal) setDirty();
	});
	
	//$('.ficha').on('click', '.add_tr, .remove_tr, .add_tr_act, remove_tr_act', setDirty);
	
	$('.ficha').submit(function () {
		formSubmitting = true;
	});
	
	$(window).bind('beforeunload', function() {
		return isDirty && ! formSubmitting ? 'Sus datos aún no han sido guardados y los perderá.' : undefined;
	});
	
})(jQuery);
