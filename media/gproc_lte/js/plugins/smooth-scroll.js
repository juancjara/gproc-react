/* =============================================================

    Smooth Scroll 1.1
    Animated scroll to anchor links.

    Script by Charlie Evans.
    http://www.sycha.com/jquery-smooth-scrolling-internal-anchor-links

    Rebounded by Chris Ferdinandi.
    http://gomakethings.com

    Free to use under the MIT License.
    http://gomakethings.com/mit/
    
 * ============================================================= */

(function ($) {
    jQuery(document).ready(function ($) {
        //$(".scroll").click(function(event){ // When a link with the .scroll class is clicked
        $('a[href*=#]').click(function (e){ // When a link with the .scroll class is clicked
			if ($(this).attr('href') == '#') return;
            e.preventDefault(); // Prevent the default action from occurring
			
			Saes.scroll.animate(this.hash);
        });
		
		hash = document.location.hash;
		
		if (hash) {
			Saes.scroll.animate(hash);
		}
		
		$('#criterion-standard option').click(function () {
			var target = $(this).data('target');
			console.log(target);
			
			Saes.scroll.animate(target);
		})
		
    });
})(jQuery);