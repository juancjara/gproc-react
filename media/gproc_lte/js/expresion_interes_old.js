/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

/*
 * The functions inside this block will get executed AFTER page load, 
 * overwritting previous functionality as event handlers, if they exist
 */
$(function() {
	
});

/*
 * Functions inside this block will get executed inmediatly 
 * (i.e. this callbacks will get registered BEFORE page load)
 */
(function($) {
	
	// FICHA001 ================================================================
	
	// Common
	
	if ($('.ficha').length) {
		
		$('body')
			.css('position', 'relative')
			.attr('data-scroll', 'scroll')
			.scrollspy({
				target: '.sidebar',
				offset: 100,
			});
		
		var institucional = ($('.firmas').children().length == 1),
			universidad = ! $('[name=cod_mod]').length;
		
	}
	
	var add_row = function (id) {
		
		return function () {
			
			var $list = $(this).parent().find('tbody'),
				$item = $list.children().first().clone(),
				number = id || +$list.children().last().data('id') + 1;
			//alert(number);
			$item.attr('data-id', number);
			$item.find('input').each(function () {
				var $this = $(this),
					name = $this.attr('name'),
					pattern = /\[\d+\](?!.*\[\d+\])/, // not followed by... (último)
					text = '[' + number + ']';

				$this.val('').attr('name', name.replace(pattern, text));
				
			});

			$list.append($item);
			$('body').scrollspy('refresh');
		}
	},
	
	remove_row = function () {
		$(this).closest('tr').remove();
		$('body').scrollspy('refresh');
	},
	
	sort = function () {
		var $wrapper = $(this);
		
		$wrapper.children().sort(function (a, b) {
			
			return +$(a).data('id') - +$(b).data('id');
		})
		.appendTo( $wrapper );
	}
	
	$('.ficha').on('click', '.add_tr', add_row());
	
	$('.ficha').on('click', '.remove_tr', remove_row);
	
	// =========================================================================
	
	// Blur carrera nombre
	
	$('.ficha').on('blur', '.carrera_nombre', function () {
		var $this = $(this),
			val = $this.val(),
			$sede = $this.closest('.filial_item'),
			title = $sede.find('.nombre_sede').text(),
			name = title + ': ' + val,
			
			sede = +$sede.data('id'),
			carr = +$this.closest('tr').data('id'),
			number = sede * 100 + carr;
			//alert(number);
		
		// Table carreras
		var $table_carreras = $('.table_carreras tbody');
		
		$table_carreras.each(function () {
			var $tbody = $(this),
				$input = $tbody.find('[data-id="' + (number) + '"] td:first-child input');
			
			$input.val(name);
		});
		
		if ( ! institucional) return;
		
		// Sidebar
		var $li = $('.sidebar_menu').find('[data-id=' + number + ']');
		$li.find('span').html(name);
		
		// Ficha
		var $item = $('.carreras_list[data-id=' + sede + '] > .carreras_item[data-id=' + carr + ']');
		//var $item = $('.carreras_item[data-id=' + number + ']');
		$item.find('.carrera_nombre').val(val);
	});
	
	// Blur carrera area
	
	$('.ficha').on('blur', '.carrera_area', function () {
		var $this = $(this),
			val = $this.val(),
			$sede = $this.closest('.filial_item'),
			
			sede = +$sede.data('id'),
			carr = +$this.closest('tr').data('id');
		
		if ( ! institucional) return;
		
		// Ficha
		var $item = $('.carreras_list[data-id=' + sede + '] > .carreras_item[data-id=' + carr + ']');
		$item.find('.carrera_area').val(val);
	});
	
	var add_carrera = function () {
		var $tbody = $(this).parent().find('tbody'),
			$sede = $tbody.closest('.filial_item'),
			title = $sede.find('.nombre_sede').text(),
			
			sede = +$tbody.closest('.filial_item').data('id'),
			carr = +$tbody.children().last().data('id'), // We don't need to add 1 'cause the new one was already created
			number = sede * 100 + carr;
		
		// Table carreras
		var $table_carreras = $('.table_carreras tbody');
		$table_carreras.each(add_row(number));
		$table_carreras.each(sort);
		
		if ( ! institucional) {
			$('body').scrollspy('refresh');
			return;
		}
		
		// Ficha
		var $list = $('.carreras_list[data-id=' + sede + ']'),
			$item = $('.carreras_list[data-id=0]').children('.carreras_item').first().clone(),
					//$list.children('.carreras_item').first().clone(),
			id = $item.attr('id').replace(/\d+/, number);
			;
			
		$item.attr('data-id', carr);
		$item.attr('id', id);
		
		$item.find('.add_tr').parent().find('tbody tr:not(:first-child)')
			.remove();
		
		$item.find('input').each(function () {
			var $this = $(this),
				name = $this.attr('name'),
				pattern1 = /\[\d+\]/, // will replace only first one
				text1 = '[' + sede + ']',
				pattern2 = /(\[\d\]\[[^\]]+\])\[\d\]/, // Will match everything before second one as $1 and will replace second one
				text2 = '$1[' + carr + ']';
			
			$this.val('').attr('name', name.replace(pattern1, text1).replace(pattern2, text2));
		});
		
		$list.append($item);
		$('body').scrollspy('refresh');
	},
	
	remove_carrera = function () {
		
		var $this = $(this),
			$sede = $this.closest('.filial_item'),
			
			sede = +$sede.data('id'),
			carr = +$this.closest('tr').data('id'),
			number = sede * 100 + carr;
		
		$this.each(remove_row);
		
		// Table carreras
		var $table_carreras = $('.table_carreras tbody');
		$table_carreras.children('[data-id=' + number + ']').remove();
		
		if ( ! institucional) {
			$('body').scrollspy('refresh');
			return;
		}
		
		// Sidebar
		var $li = $('.sidebar_menu').find('[data-id=' + number + ']');
		$li.remove();
		
		// Ficha
		var $item = $('.carreras_list[data-id=' + sede + '] > .carreras_item[data-id=' + carr + ']');
		$item.remove();
		
		$('body').scrollspy('refresh');
	};
	
	// Add carrera
	$('.ficha').on('click', '.add_carrera', add_carrera);
	
	// Remove carrera
	$('.ficha').on('click', '.remove_carrera', function() {
		var $this = $(this);
		
		if ( institucional && ! confirm('Esto eliminará también la ficha asociada a la carrera. Continuar?'))
			return;
		
		$this.each(remove_carrera);
		$('body').scrollspy('refresh');
	});
	
	// Add filial
	$('.add_filial').click(function () {
		var $this = $(this),
			$list = $this.closest('.box').find('.filial_list'),
			$item = $list.children().first().clone(),
			number = +$list.children().last().data('id') + 1,
			title = 'Filial ' + number,
			institucional = ($('.firmas').children().length == 1);
			
		$item.attr('data-id', number);
		$item.find('.box-title').html(title);
		$item.find('tbody tr').slice(1).remove();
		$item.find('input').each(function() {
			var $this = $(this),
				name = $this.attr('name'),
				pattern = /\[\d+\]/, // will replace only first one
				text = '[' + number + ']';
			
			$this.val('').attr('name', name.replace(pattern, text));
		});
		
		$item.find('[type=hidden]').val(title);
		$list.append($item);
		
		// Sedes
		if (institucional) {
			var $sedes_list = $('.sedes_list'),
				$sede_item = $sedes_list.children().first().clone();

			$sede_item.attr('data-id', number);
			$sede_item.children('.carreras_item').remove();
			$sede_item.children('h3').text(title);

			$sedes_list.append($sede_item);
		}
		
		$item.find('.add_carrera').each(add_carrera);
		$('body').scrollspy('refresh');
	});
	
	// Remove filial
	
	$('.filial_list').on('click', '.remove_filial', function () {
		if (institucional && ! confirm('Esto eliminará también las fichas asociadas a las carreras. Continuar?'))
			return;
		
		var $item = $(this).closest('.filial_item'),
			$rows = $item.find('tbody tr'),
			number = +$item.data('id');
		
		$rows.each(remove_carrera);
		
		$item.remove();
		
		// Sedes
		if (institucional)
		{
			var $sede = $('.carreras_list[data-id=' + number + ']');
			$sede.remove();
		}
		
		$('body').scrollspy('refresh');
	});
	
	// =========================================================================
	
	$('.ficha').on('blur', '[pattern]', function () {
		var $this = $(this),
			val = $this.val(),
			regex = new RegExp($this.attr('pattern')),
			required = $this.prop('required'),
			text = 'Sólo se permiten números o los valores N/A o S/D';
		
		$this.tooltip({'title': text, 'trigger': 'manual'});
		
		if ( ! regex.test(val))
		{
			if (val !== '' || required) {
				$this.tooltip('show');
				setTimeout(function () { $this.tooltip('hide'); }, 2000);
			}
		}
	});
	
	
	
	$('.ficha').on('blur', '.calc_total > tr > td', function () {
		var $this = $(this),
			nth_index = $this.index() + 1,
			$tbody = $this.closest('tbody'),
			$row = $this.parent().find('>td>input'),
			$column = $tbody.find('td:nth-child(' + nth_index + ')>input'),
			$last_row = $tbody.children(':last-child').find('>td>input'),
			calc_row = $row.filter('[readonly]:not(.td_per)').length,
			calc_col = $column.filter('[readonly]').length;
		
		if ($this.find('input').prop('readonly'))
			return;
		
		if (calc_row) {
			calc_total($row);
		}
		
		if (calc_col) {
			calc_total($column);
		}
		
		if (calc_row && calc_col) {
			calc_total($last_row);
		}
		
		function calc_total($set) {
			var $last = $set.slice(-1),
				$rest = $set.slice(0,-1);
			
			var	total = $rest.get().reduce(function (p, c) {
					var val = +$(c).val() || 0;
					return p + val;
				}, 0);
			
			$last.val(total);
		}
	});
	
	$('.ficha').on('blur', '.calc_per > tr > td', function () {
		
		var $this = $(this),
			nth_index = $this.index() + 1,
			$tbody = $this.closest('tbody'),
			$columns = $tbody.find('td:nth-child(' + nth_index + ')>input'),
			$columns_per = $tbody.find('td:nth-child(' + (nth_index + 1) + ')>input'),
			total = (+$columns.last().val() || 1);
		
		if ($this.hasClass('td_per')) return;
		//console.log('a');
		//console.log($column);
		
		$columns.each(function (index) {
			var $col = $(this),
				$col_per = $columns_per.eq(index),
				per = +$col.val() / total * 100;
				console.log($col[0], $col_per[0]);
			
			$col_per.val(per.toFixed(2));
		});
		
	});
	
	$('.btn_finish, .btn_save').click(function () {
		
		var $this = $(this),
			finish = $this.val(),
			$form = $('form');
			//console.log(finish);
		
		$form.data('finish', finish);
		
		if (finish == 0) {
			$form.submit(); // Force no validate
			return false;
		}
		
		$('button[type=submit]').first().focus();
		
		return check_totales();
	});
	
	setTimeout(
		function () {
			if ($('.btn_save').length)
			{
				if (confirm('Esta ficha se guardará automáticamente. Continuar?'))
				{
					$('.btn_save').focus();
					$('form').submit();
				}
			}
		}, 1000 * 60 * 15);
	
	function check_totales() {
		var success = true;
		
		$('.cuerpo_academico').each(function () {
			
			//success = false;
			
			var $this = $(this),
				//ded = +$this.find('[data-tipo=ded]').val(),
				//con = +$this.find('[data-tipo=con]').val(),
				cat = +$this.find('[data-tipo=cat]').val(),
				nom = +$this.find('[data-tipo=nom]').val(),
				$dedicacion = $this.find('.table_dedicacion [data-tipo=total]'),
				$condicion = $this.find('.table_condicion [data-tipo=total]');
				//console.log($dedicacion, $condicion);
			
			$dedicacion.each(function () {
				var $ded = $(this),
					pos = $ded.closest('tr').index(),
					$con = $condicion.eq(pos);
					//console.log($ded, pos, $con);
				
				if (+$ded.val() != +$con.val())
				{
					$('html, body').scrollTop($this.offset().top);
					$ded.add($con)
						.addClass('warning').delay(1000).queue(function () {
							$(this).removeClass('warning').dequeue();
						});
					
					alert('Los totales de Dedicación y Condición deben coincidir');
					success = false;
					return false;
				}
			});
			
			if ( ! success) return false;
			
			/*if (ded != con) {
				$('html, body').scrollTop($this.offset().top);
				$this.find('[data-tipo=ded],[data-tipo=con]')
					.addClass('warning').delay(1000).queue(function () {
						$(this).removeClass('warning').dequeue();
					});
				alert('Los totales de Dedicación y Condición deben coincidir');
				return false;
			}*/
			
			if (universidad && cat != nom) {
				$('html, body').scrollTop($this.offset().top);
				$this.find('[data-tipo=cat],[data-tipo=nom]')
					.addClass('warning').delay(1000).queue(function () {
						$(this).removeClass('warning').dequeue();
					});
				alert('Los totales de Nombrados y Categoría Docente deben coincidir');
				success = false;
			}
			
			// @TODO files ??
			if ( ! success) return false;
		});
		
		//alert(success);
		
		return success;
	}
	
}(jQuery));
