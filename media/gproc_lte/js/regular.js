/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

/*
 * The functions inside this block will get executed AFTER page load, 
 * overwritting previous functionality as event handlers, if they exist
 */
$(function() {
	$("input[type='checkbox'], input[type='radio']").iCheck('destroy');
});

/*
 * Functions inside this block will get executed inmediatly 
 * (i.e. this callbacks will get registered BEFORE page load)
 */
(function($) {
	
	// Committee member add ====================================================
	// /profile?change=1
	
	$('#signup-member-add').click(function () {
		var $table = $('.committee'),
			$tbody = $('tbody', $table),
			$row = $('tr:first', $tbody).clone(),
			n = $tbody.children().length;
		
		console.log(n);
		
		$row.find('input[type=radio]').iCheck('uncheck').val(n);
		$row.find('input[type=text]').val('').prop('required', false);
		$row.appendTo($tbody);
	});
	
	// Committee contact change
	
	$('.committee').on('change', 'input[type=radio]', changeContact);
	//changeContact.call($('.committee input[type=radio]:checked'));
	
	function changeContact () {
		var $tr = $(this).parents('tr');
		$('.committee').find('input[type=text]').prop('required', false);
		$tr.find('input[type=text]').prop('required', true);	
	}
	
	// Autoevaluacion evento ===================================================
	// /dashboard/start
	
	$('#auto_even_id').change(toggleTreeEstandares);
	$('#auto_even_id').each(toggleTreeEstandares);
	
	function toggleTreeEstandares() {
		var $this = $(this),
			val = $this.val(),
			$tree = $('#tree-estandares');
		
		if (val === '')
			return;
		
		if (val === '0') {
			$tree.find('select').prop('required', true).prop('disabled', false);
			$tree.find('.start-select-all').prop('disabled', false);
		} else {
			$tree.find('select').prop('required', false).prop('disabled', true);
			$tree.find('.start-select-all').prop('disabled', true);
			$tree.find('select').first().find('option').prop('selected', true);
			$tree.find('select').first().change();
		}
	}
	
	// Select ajax multiple ====================================================
	
	$('select.ajax-multiple').change(getOptionsMultiple);
	$('select.ajax-multiple').each(getOptionsMultiple);
	
	function getOptionsMultiple() {
		if ( ! $(this).val()) return;
		
		var $this = $(this),
			action = $this.data('action'),
			val = $this.val().join('-'),
			url = action + val + '?multiple=true',
			$target = $($this.data('target'));
		
		$.get(url).done(function (data) {
			var val = $('#auto_even_id').val();
			
			$target.html(data);
			console.log(val);
			if (val) {
				$target.find('option').prop('selected', true);
				$target.change();
			}
		});
	}
	
	// Select all
	
	$('.start-select-all').click(function () {
		var $select = $(this).parent().find('select');
		
		$select.find('option').prop('selected', true);
		if ($select.hasClass('ajax-multiple')) $select.each(getOptionsMultiple);
	});
	
	// header text
	setNavbarTextWidth();
	
}(jQuery));

$(window).resize(setNavbarTextWidth);

function setNavbarTextWidth() {
	
	var $text = $('.navbar-text');
		header = $('.header').outerWidth(),
		logo = $('.logo').outerWidth(),
		navbar = $('.navbar-right').outerWidth();
	
	$text.css('width', header - logo - navbar - 15);
}
