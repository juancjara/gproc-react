/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

var medios;

jQuery(function ($) {
	$('input[type=radio]').iCheck('destroy');
	$('input[type=checkbox]').iCheck('destroy');
});

(function ($) {
	$('.table_indicadores').on('click', '.add_tr', function () {

		var $list = $(this).parent().find('tbody'),
			$item = $list.children().first().clone(),
			number = +$list.children().last().data('id') + 1,
			pattern = /\[\d+\](?!.*\[\d+\])/;

		$item.attr('data-id', number);
		
		$item.find('[name]').each(function () {
			var $this = $(this),
				name = $this.attr('name'),
				text = '[' + number + ']';
			$this.val('').data('selected', '').attr('name', name.replace(pattern, text));
		});
		
		$list.append($item);
	});

})(jQuery);

/*indicador add_tr_ind*/
(function ($) {
	
	$('.add_tr_ind').click(function () {

		var $list = $(this).parent().find('.table_indicadores > tbody'),
			$item = $list.children().first().clone(),
			number = +$list.children().last().data('id') + 1,
			pattern = /((?:\[[\.\d]+\].*?){1})(?:\[\d+\])(.*)/; // second
			//pattern = /\[\d+\](?!.*\[\d+\])/; // not followed by... (último)
		$item.find('.dynamic2 > tbody > tr').slice(1).remove();
		$item.attr('data-id', number);
		
		$item.find('.input_otro_medio').addClass('hidden').prop('required', false);

		$item.find('[name]').each(function () {
			var $this = $(this),
				name = $this.attr('name'),
				text = '$1[' + number + ']$2';
				//text = '[' + number + ']';

			$this.val('').data('selected', '').attr('name', name.replace(pattern, text));
		});

		$list.append($item);
	});

})(jQuery);

/*function justNumbers(e) {
	
	var keynum = window.event ? window.event.keyCode : e.which;
		if ((keynum == 8) || (keynum == 46))
	return true;

	return /\d/.test(String.fromCharCode(keynum));
}*/


(function ($) {
	var medios = JSON.parse($('#medios').val());
	//console.log(medios);
	
	if ( ! Object.keys(medios).length) return;

	function get_options(results) {
		var response = results.map(function (item) {
			return item.children ? 
				get_options(item.children) : 
				'<option value="' + item.id + '" title="N° veces seleccionado: ' + (item.count || 0) + '" data-title="' +  item.text + '">(' + (item.count || 0) + ') ' + item.text + '</option>';
		});
		return response.join('');
	}

	function factor_medios(fact_id) {
		
		var temp = medios[fact_id]
			.map(function (medio) {
				return {
					id: medio.conc_id,
					text: 'Estandar ' + medio.esta_codigo + ' - ' + medio.conc_codigo + '. ' + medio.conc_nombre,
					count: medio.count || 0,
				};
			});
		
		return get_options(temp);
	}

	function set_options() {
		var $this = $(this),
			fact_id = $this.data('id'),
			select = '<option value="">[Seleccione Medio de Verificación]</option>',
			otro = '<option value="999">Otro</option>';
		
		$this.html(select + factor_medios(fact_id) + otro);
		$this.val($this.data('selected'));
	}
	
	$('.selectmedios').each(set_options);
	$('.selectmedios').each(update_count);
	
	$('.ficha').on('change', '.selectmedios', update_count);
	//$('.ficha').on('change', '.selectmedios', set_nombre);
	update_result();
	
	/*function set_nombre() {
		var $this = $(this),
			title = $this.find('option:selected').data('title');
		
		console.log($this, title);
		if (title) {
			$this.next().val(title);
		}
	}*/
	
	function update_count() {
		var $this = $(this),
			fact_id = $this.data('id'),
			val = $this.val(),
			old_val = $this.data('selected');
		
		$this.data('selected', val);

		update_medios(fact_id, val, old_val);
		update_result();
		
		var $input_otro_medio = $this.next(),
			OTRO = (val == 999);
		
		$input_otro_medio[OTRO ? 'removeClass' : 'addClass']('hidden').prop('required', OTRO);//.val('');
	}

	function update_medios(fact_id, val, old_val) {
		medios[fact_id]
			.map(function (medio, i) {
				medios[fact_id][i].count = 
					(medio.conc_id == val) ?
						(medio.count || 0) + 1 :
					(medio.conc_id == old_val) ?
						medio.count - 1 :
					medio.count;
			});
		
		$('.selectmedios').each(set_options);
	}
	
	function update_result() {
		var total = 0,
			count = 0,
			per;
		
		Object.keys(medios)
			.map(function (fact_id) {
				medios[fact_id]
					.map(function (medio) {
						total++;
					
						if (medio.count > 0) count++;
					});
			});
		
		per = count / total * 100;
		
		$('.bar_procesados').css('width', per + '%');
		$('.title_resumen').html(count + ' procesados de ' + total + ' medios de verificación por atender (' + per.toFixed(2) + '%)');
		
		return {
			per: per,
			count: count,
			total: total,
		};
	}
	
	$('.dynamic').on('click', '.remove_tr', remove_tr);

	$('.remove_tr_ind').click(function () {
		var $trs = $(this).parent().find('.table_indicadores > tbody > tr'),
			count = $trs.length,
			$last = $trs.last(),
			$btn_remove_tr = $last.find('.remove_tr');
		
		if (count > 1) {
			$last.remove();
			$btn_remove_tr.each(remove_tr);
		}
	});
	
	function remove_tr() {
		var $this = $(this),
			$tr = $this.closest('tr'),
			$select = $tr.find('select'),
			fact_id = $select.data('id'),
			old_val = $select.val();
		
		update_medios(fact_id, null, old_val);
		update_result();
		
		$tr.remove();
	}

	$('.btn_finish, .btn_save').click(function () {
		var $this = $(this),
			finish = $this.val(),
			$form = $('.ficha');
		
		$form.data('finish', finish);
		
		if (finish == 0) {
			$form.submit(); // Force no validate
			return false;
		}
		
		var success = check_medios(); 
			
		return success;
	});
	
	setTimeout(
		function () {
			if ($('.btn_save').length)
			{
				if (confirm('Esta ficha se guardará automáticamente. Continuar?'))
				{
					$('.btn_save').focus();
					$('form').submit();
				}
			}
		}, 1000 * 60 * 15);
	
	$('.btn_finish, .btn_save').prop('disabled', true);
	setTimeout(
		function () {
			$('.btn_finish, .btn_save').prop('disabled', false);
		}, 1000 * 5);
	
	function check_medios() {
		
		var missing = check_result(),
			items = missing
				.map(function (medio) { 
					return 'R.' + medio.fact_codigo + 
						' - Estandar ' + medio.esta_codigo + ' - ' + medio.conc_codigo + '. ' + medio.conc_nombre;
				}).join('\n');
		
		if ( ! missing.length) return true;
		
		if (missing.length > 5) {
			alert('Existen ' + missing.length + ' medios de verificacion que no han sido seleccionados');
		}
		else {
			alert('Los siguientes medios de verificación no han sido seleccionados: \n' + items);
		}
		
		return false;
	}
	
	function check_result() {
		var missing = [];
		
		Object.keys(medios)
			.map(function (fact_id) {
				medios[fact_id]
					.map(function (medio) {
						if (!medio.count || medio.count < 1) {
							missing.push(medio);
						}
					});
			});
		
		return missing;
	}
	
})(jQuery);

var isDirty = false,
	setDirty = function () { console.log('dirty'); isDirty = true; };

(function ($) {
	
	var lastVal,
		//isDirty = false,
		formSubmitting = false;
		//setDirty = function () { isDirty = true; };
	
	$('.ficha').on('focus', 'input, select, textarea', function (e) {
		//console.log(e.target.value);
		lastVal = e.target.value;
	});
	
	$('.ficha').on('blur', 'input, select, textarea', function (e) {
		if (e.target.value != lastVal) setDirty();
	});
	
	$('.ficha').on('click', '.add_tr, .remove_tr, .add_tr_ind, remove_tr_ind', setDirty);
	
	$('.ficha').submit(function () {
		formSubmitting = true;
	});
	
	$(window).bind('beforeunload', function() {
		return isDirty && ! formSubmitting ? 'Sus datos aún no han sido guardados y los perderá.' : undefined;
	});
	
})(jQuery);
