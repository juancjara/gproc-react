/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

/*
 * The functions inside this block will get executed AFTER page load, 
 * overwritting previous functionality as event handlers, if they exist
 */
$(function() {
	
});

/*
 * Functions inside this block will get executed inmediatly 
 * (i.e. this callbacks will get registered BEFORE page load)
 */
(function($) {
	
	
	
}(jQuery));
