/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

var estandares = {};

jQuery(function ($) {
	$('input[type=radio]').iCheck('destroy');
});
/*Planes Mejora - matriz planificación*/

	/*actividad add_tr_gasto*/
	
(function ($) {
	
	$('.remove_tr_gasto').click(function () {
		var trs = $(this).parent().find('.dynamic > tbody > tr').length;
		//alert(trs)
		if (trs > 1)
		{
			$(this).parent().find('.dynamic > tbody ').children().last().remove();
		}
	});

	// review reset
	$('.add_tr_gasto').click(function () {
		var $list = $(this).parent().find('.dynamic > tbody'),
			$item = $list.children().first().clone().change(),
			number = +$list.children().last().data('id') + 1;
			//console.log($item);

		$item.find('.dynamic > tbody > tr').slice(1).remove();
		$item.attr('data-id', number);

		$item.find('[name]').each(function () {
			var $this = $(this),
				name = $this.attr('name'),
				pattern = /\[\d+\](?!.*\[\d+\])/, // not followed by... (último)
				text = '[' + number + ']';

			$this.val('').attr('name', name.replace(pattern, text));
		});
		
		$item.find('.input_gasto, .input_tipo, .input_item')
			.prop('disabled', true)
			.prop('required', false);
		//$tr.find('.input_tipo').addClass('hidden');
		//$tr.find('.input_item').addClass('hidden');
		$item.find('.select_tipo').prop('disabled', false).data('selected', '');
		$item.find('.select_item').prop('disabled', false).data('selected', '');
		$item.find('.input_unidadmedida').prop('readonly', true);
		$item.find('.input_costounitario').prop('readonly', true);

		$list.append($item);
	});

})(jQuery);
	
/*Fin add_tr_gasto y remove_tr_gasto*/

(function ($) {
	
	/*$('.ficha').on('blur', '.costos', calc_costo);
	
	function calc_costo() {
		var $this = $(this),
			$costounitario = $this.closest('tr').find('.input_costounitario').val(),
			$cantidad = $this.closest('tr').find('.input_cantidad').val(),
			$subtotal = $this.closest('tr').find('.input_subtotal');
		
		$subtotal.val($costounitario * $cantidad);
	}*/
	
	/*porcentajes de estándares */
	
	$('.btn_finish, .btn_save').click(function () {
		var $this = $(this),
			finish = $this.val(),
			$form = $('.ficha');
			//console.log(finish);
		
		$form.data('finish', finish);
		
		if (finish == 0) {
			$form.submit(); // Force no validate
			return false;
		}
		
		var success = 
			check_files() &&
			check_totalFEC() && // TEMP
			check_totales() && 
			check_pasantias() && 
			check_recursos() && 
			check_consultorias() && 
			check_obras();
		
		return success;
	});
	
	function check_totalFEC() {
		var totalFECaprobado = +$('.totalFECaprobado').val(),
			totalFEC = +$('[data-tipo=fec][data-grupo=total]').val();
		
		if (totalFECaprobado && totalFEC > totalFECaprobado)
		{
			alert('El total de financiamiento FEC no puede ser mayor que el total aprobado: ' + totalFECaprobado + ' (Actual: ' + totalFEC.toFixed(2) + ')');
			return false;
		}
		
		return true;
	}
	
	setTimeout(
		function () {
			if ($('.btn_save').length)
			{
				if (confirm('Esta ficha se guardará automáticamente. Continuar?'))
				{
					$('.btn_save').focus();
					$('form').submit();
				}
			}
		}, 1000 * 60 * 15);
	
	$('.btn_finish, .btn_save').prop('disabled', true);
	setTimeout(
		function () {
			$('.btn_finish, .btn_save').prop('disabled', false);
		}, 1000 * 5);
	
	function check_files() {
		//return true;
		
		var $tdPdfs = $('td.td-pdf'),
			success = true;
		
		
		$tdPdfs.each(function () {
			var $this = $(this),
				$button = $('button', $this),
				$input = $('input[type=hidden]', $this);
				//console.log($this, $button, $input);
			
			if ( ! $button.hasClass('disabled') && ! $input.val()) {
				//console.log($this);
				//$button.focus();
				$this
					.focus()
					.addClass('danger').delay(1000)
						.queue(function () {
							$(this).removeClass('danger').dequeue();
						});
				alert('Debe subir un archivo de especificación técnica');
				success = false;
				return false;
			}
		});
		
		return success;
	}
	
	function check_totales() {
		var MAX = 95,
			$per = $('.porcentajefec'),
			per = (+$per.val() || 0);
			//console.log(per);
		
		if (per > MAX) {
			$per
				.parent()
				.focus()
				.addClass('danger').delay(1000)
					.queue(function () {
						$(this).removeClass('danger').dequeue();
					});
			alert('El porcentaje del financiamiento FEC no puede ser mayor que ' + MAX + '% (Actual: ' + per.toFixed(2) + '%)');
			return false;
		}
		
		return true;
	}
	
	function check_pasantias() {
		var MAX = 10,
			$pasantias = $('.totalpasantias'),
			$totalFEC = $('[data-tipo=fec][data-grupo=total]'),
			per = +$pasantias.val() / (+$totalFEC.val() || 1) * 100;
		
		//console.log($pasantias, $totalFEC);
		
		if (per > MAX) {
			alert('El porcentaje del financiamiento FEC para Pasantías no puede ser mayor que ' + MAX + '% (Actual: ' + per.toFixed(2) + '%)');
			return false;
		}
		
		return true;
	}
	
	function check_recursos() {
		var MAX = 40,
			$recursos = $('[data-tipo=fec][data-grupo=1]'),
			$totalFEC = $('[data-tipo=fec][data-grupo=total]'),
			per = +$recursos.val() / (+$totalFEC.val() || 1) * 100;
		
		//console.log($consultorias, $totalFEC);
		
		if (per > MAX) {
			alert('El porcentaje del financiamiento FEC para Formación de RRHH no puede ser mayor que ' + MAX + '% (Actual: ' + per.toFixed(2) + '%)');
			return false;
		}
		
		return true;
	}
	
	function check_consultorias() {
		var MAX = 30,
			$consultorias = $('[data-tipo=fec][data-grupo=2]'),
			$totalFEC = $('[data-tipo=fec][data-grupo=total]'),
			per = +$consultorias.val() / (+$totalFEC.val() || 1) * 100;
		
		//console.log($consultorias, $totalFEC);
		
		if (per > MAX) {
			alert('El porcentaje del financiamiento FEC para Consultorías no puede ser mayor que ' + MAX + '% (Actual: ' + per.toFixed(2) + '%)');
			return false;
		}
		
		return true;
	}
	
	function check_obras() {
		var MAX = 60,
			$obras = $('[data-tipo=fec][data-grupo=3]'),
			$totalFEC = $('[data-tipo=fec][data-grupo=total]'),
			per = +$obras.val() / (+$totalFEC.val() || 1) * 100;
		
		//console.log($consultorias, $totalFEC);
		
		if (per > MAX) {
			alert('El porcentaje del financiamiento FEC para Equipamiento y obras menores no puede ser mayor que ' + MAX + '% (Actual: ' + per.toFixed(2) + '%)');
			return false;
		}
		
		return true;
	}
	
})(jQuery);

var isDirty = false,
	setDirty = function () { console.log('dirty'); isDirty = true; };

(function ($) {
	
	var lastVal,
		//isDirty = false,
		formSubmitting = false;
		//setDirty = function () { isDirty = true; };
	
	$('.ficha').on('focus', 'input, select, textarea', function (e) {
		//console.log(e.target.value);
		lastVal = e.target.value;
	});
	
	$('.ficha').on('blur', 'input, select, textarea', function (e) {
		if (e.target.value != lastVal) setDirty();
	});
	
	//$('.ficha').on('click', '.add_tr, .remove_tr, .add_tr_act, remove_tr_act', setDirty);
	
	$('.ficha').submit(function () {
		formSubmitting = true;
	});
	
	$(window).bind('beforeunload', function() {
		return isDirty && ! formSubmitting ? 'Sus datos aún no han sido guardados y los perderá.' : undefined;
	});
	
})(jQuery);
