/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

(function($) {
	"use strict";
	
	// Alerts
	
	setTimeout(function () { $('.alert.fade.in').removeClass('in').parent().hide(); }, 4000);
	
	// Sidebar =================================================================
	
	$('.sidebar-menu li > a').each(function() {
		var item = this;
		
		//if (item.href == window.location.href) {
		if ($(item).attr('href') == window.location.pathname) {
			$(item).parent().addClass('active');
			$(item).parents('.treeview').addClass('active');
		}
	});
	
	// Select ajax =============================================================
	
	$('select.ajax').change(getOptions);
	$('select.ajax').each(getOptions);
	
	function getOptions() {
		
		var $this = $(this),
			val = $this.val(),
			path = $this.data('path'),
			url = path + val,
			$target = $($this.data('target'));

		$.get(url).done(function (data) {
			setOptions(data, $target);
		});
	}
	
	function setOptions(data, $target) {
		$target.html(data);

		if ($target.attr('data-selected')) {
			
			$target
				.find('option[value=' + $target.attr('data-selected') + ']')
				.prop('selected', true);
		}
	}

	
	// Data confirm ============================================================
	
	$('[data-confirm]').click(function () {
		var msg = $(this).data('confirm') || 'Está usted seguro(a)?';
		return confirm(msg);
	});
	
	// Download ficha	
	$('.download_ficha').click(function () {

		var url = $(this).data('url');
		console.log(url);
		$('iframe').attr('src', url);
	});
	
})(jQuery);
