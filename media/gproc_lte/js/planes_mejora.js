/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

(function ($) {
	
	$('.btn_finish, .btn_save').click(function () {
		var $this = $(this),
			finish = $this.val(),
			$form = $('.ficha');
			//console.log(finish);
		
		$form.data('finish', finish);
		
		if (finish == 0) {
			$form.submit(); // Force no validate
			return false;
		}
		
		var success = true; //check_documentos();
			
		return success;
	});
	
	/*function check_documentos() {
		var documentos = $('tbody').children().length,
			subidos = $('input[type=hidden][name=identifier]').length;
		
		if (documentos != subidos) {
			alert('Para cerrar el Plan de Mejora debe finalizar todas las matrices, descargarlas, y subirlas firmadas.');
			
			return false;
		}
		
		return true;
	}*/
	
	$('.ficha').on('change', '.concept-file', function () {
		var $this = $(this),
			$inputs = $this.parent(),
		
			$concepto = $this.closest('.td-pdf'),
			$upload = $concepto.find('.file_upload'),
			$process = $concepto.find('.file_process'),
			$file_view = $concepto.find('.btn_file_view'),
			$filename = $file_view.siblings('input'),
			$form = $('.ficha'),
			
			//$form = $this.closest('.ficha'),
			//$btn_save = $form.find('.btn-save'),
			
			$outside = $('.form_outside'),
			form, xhr;
		
		if ( ! $this.val()) return;
		//console.log($filename);return;
		
		// processing...
		$upload.addClass('hidden');
		$process.removeClass('hidden');
		//$btn_save.prop('disabled', true);
		$file_view.addClass('disabled').tooltip('destroy');
		
		//console.log($file_view);
		//$file_view.removeAttr('disabled').tooltip();return;
		
		// submit
		$inputs.appendTo($outside);
		form = $outside.ajaxSubmit();
		xhr = form.data('jqxhr');
		
		// done
		xhr.done(function (data) {
			console.log('success');
			$filename.val(data);
			console.log(data);
			$file_view.attr('href', '/file/' + data);
			console.log($file_view);
			$this.prop('required', false);
			finish();
			
		}).error(function (x,y,z) {
			console.log('error', x, y, z);
			
			alert('No se pudo subir el archivo. Por favor inténtelo nuevamente');
			finish();
		});
		
		function finish() {
			$this.val('');
			$upload.append($inputs).removeClass('hidden');
			$process.addClass('hidden');

			if ($filename.val()) {
				$file_view.removeClass('disabled').tooltip();
			}
			
			$form.submit();
			
		}
		
	});
	
})(jQuery);
