/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
	
(function ($) {
	
	$('.btn_finish, .btn_save').click(function () {
		var $this = $(this),
			finish = $this.val(),
			$form = $('.ficha');
			//console.log(finish);
		
		$form.data('finish', finish);
		
		if (finish == 0) {
			$form.submit(); // Force no validate
			return false;
		}
		
		var success = check_totales() && check_files();
		
		return success;
	});
	
})(jQuery);

(function ($) {
	
	/*var TH_SIZE= +$('.th_size').val(),
		TH_ACT = +$('.th_act').val(),
		TH_ACT = $('.th-act').outerWidth(),
		TH_PADDING = +$('.th_padding').val();
	
	$('.crono_div').each(function () {
		var $crono = $(this),
			$inicio = $crono.find('.inicio'),
			inicio = +$inicio.val(),
			$duracion = $crono.find('.duracion'),
			duracion = +$duracion.val();
		
		console.log(TH_ACT, TH_SIZE, inicio);
		//console.log($crono);
		$crono.css('left', TH_ACT + TH_SIZE * inicio + TH_PADDING);
		
	});*/
	
})(jQuery);

var TH_SIZE= +$('.th_size').val(),
	TH_PADDING = +$('.th_padding').val(),
	TH_ACT;

function setPosition () {
	var $cronoDiv = $(this),
		$inicio = $cronoDiv.find('.inicio'),
		inicio = +$inicio.val(),
		$duracion = $cronoDiv.find('.duracion'),
		duracion = +$duracion.val();

	$cronoDiv.css('left', TH_ACT + TH_SIZE * inicio + TH_PADDING);
}

function init () {
	TH_ACT = $('.th-act').outerWidth();
	$('.crono_div').each(setPosition);
}

function adjustPosition () {
	setTimeout(init, 250);
}

$(window).resize(adjustPosition);

$(function () {
	
	init();
	
	$("[data-toggle='offcanvas']").click(adjustPosition);
	
	$('.crono_div')
		.draggable({
			containment: 'tr',
			axis: 'x',
			grid: [TH_SIZE, 0],
			/*revert: function () {
				var $cronoDiv = $(this),
					left = $cronoDiv.position().left;
					//console.log(left, TH_ACT, TH_PADDING);
					
				if (left < TH_ACT + TH_PADDING) return true;
			},*/
			scroll: false,
			drag: function (event, ui) {
				if (ui.position.left < TH_ACT + TH_PADDING) return false;
			},
			stop: function (event, ui) {
				var $cronoDiv = $(this),
					$inicio = $cronoDiv.find('.inicio'),
					inicio = +$inicio.val(),
					offset = (ui.position.left - ui.originalPosition.left) / TH_SIZE;
				
				if (ui.position.left < TH_ACT + TH_PADDING) {
					$inicio.val(0);
					return;
				}
				
				$inicio.val(inicio + offset);
			},
		})
		.resizable({
			containment: 'tr',
			grid: [TH_SIZE, 0],
			handles: 'e',
			resize: function (event, ui) {
				ui.element.css('height', 20);
			},
			stop: function (event, ui) {
				console.log(event, ui);
				var $cronoDiv = $(this),
					$duracion = $cronoDiv.find('.duracion'),
					duracion = +$duracion.val(),
					offset = (ui.size.width - ui.originalSize.width) / TH_SIZE;
				
				$duracion.val(duracion + offset);
				$cronoDiv.css('width', TH_SIZE * (duracion + offset) - TH_PADDING * 2);
			}
		});
	
	//$('.crono_div').resizable();
	
	/*$('.crono_div').mousedown(function (e) {
		e.preventDefault();
		console.log('foo');
	});*/
	
});

var isDirty = false,
	setDirty = function () { console.log('dirty'); isDirty = true; };

(function ($) {
	
	var lastVal,
		//isDirty = false,
		formSubmitting = false;
		//setDirty = function () { isDirty = true; };
	
	$('.ficha').on('focus', 'input, select, textarea', function (e) {
		//console.log(e.target.value);
		lastVal = e.target.value;
	});
	
	$('.ficha').on('blur', 'input, select, textarea', function (e) {
		if (e.target.value != lastVal) setDirty();
	});
	
	//$('.ficha').on('click', '.add_tr, .remove_tr, .add_tr_act, remove_tr_act', setDirty);
	
	$('.ficha').submit(function () {
		formSubmitting = true;
	});
	
	$(window).bind('beforeunload', function() {
		return isDirty && ! formSubmitting ? 'Sus datos aún no han sido guardados y los perderá.' : undefined;
	});
	
})(jQuery);
