'use strict';

var TOTAL = 'total';
var SUPER = 'super';
var FEC = 1;
var PROPIO = 2;
var PLAN = 3;

var OTROS = 999;
var GRUPO_RRHH = 1;
var GRUPO_CONSULTORIAS = 2;
var GRUPO_NOFINANCIABLE = 6;

var GASTO_PASANTIAS = 1;

var MONITOREO = 0.015;

var aGrupo = JSON.parse($('.aGrupo').val() || null),
  aGasto = JSON.parse($('.aGasto').val() || null),
  aTipo = JSON.parse($('.aTipo').val() || null),
  aItem = JSON.parse($('.aItem').val() || null);

function getOptions(x, options) {
  var lastGroup, y = [];

  x.forEach(function (row) {
    if (row[options.groupId] !== lastGroup) {
      if (lastGroup) {
        y.push('</optgroup>');
      }

      y.push('<optgroup data-id="' + row[options.groupId] + '" label="' + row[options.groupName] + '">');
      lastGroup = row[options.groupId];
    }

    y.push('<option value="' + row[options.rowId] + '">' + row[options.rowId] + ' - ' + row[options.rowName] + '</option>');
  });

  y.push('</optgroup>');

  return y.join('');
}


function uuid() {
	/*jshint bitwise:false */
	var i, random;
	var uuid = '';

	for (i = 0; i < 32; i++) {
		random = Math.random() * 16 | 0;
		if (i === 8 || i === 12 || i === 16 || i === 20) {
			uuid += '-';
		}
		uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random))
			.toString(16);
	}

	return uuid;
}


// =============================================================================



var TableTotalCell = React.createClass({
  render: function () {
    var showPer = this.props.tipoId == FEC && this.props.grupo != TOTAL;
    var per = (
      <small className="text-primary">
        {this.props.tipoId == FEC && this.props.grupo != TOTAL ? ' (' + this.props.per + '%)' : ''}
      </small>
    );

    return (
      <td className="text-right">
        {/*this.props.tipo == 'fec' && this.props.grupo == GRUPO_NOFINANCIABLE ? '(*) ' : ''*/}
        {this.props.value}
        {showPer ? per : ''}
        {this.props.edit ?
          <input type="hidden"
            name={"totales[" + this.props.tipo + "][" + this.props.grupo + "]"}
            data-tipo={this.props.tipo}
            data-grupo={this.props.grupo}
            value={this.props.value} />
          : ''}
      </td>
    );
  }
});

var TableTotalRow = React.createClass({
  /*getSubactividades: function () {
    var actividades = this.props.actividades;
    var subactividades = [];

    Object.keys(actividades)
      .map(function (key) {
        subactividades = subactividades.concat(actividades[key]);
      });

    return subactividades;
  },
  getTotal: function (grup_id, tipo) {

    var total = this.getSubactividades()
      .filter(function (subactividad) {
        return ((subactividad.grupo == grup_id) || (grup_id == TOTAL)) && ((subactividad.financiamiento == tipo) || (tipo == PLAN));
      })
      .map(function (subactividad) {
        return (subactividad.costounitario * subactividad.cantidad) || 0;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);

    return total;
  },
  getTotalReal: function (grup_id, tipo) {
    return this.getTotal(grup_id, tipo) + (((grup_id == SUPER || grup_id == TOTAL) && tipo != PROPIO) ? this.getOtrosFEC() : 0);
  },
  getOtrosFEC: function () {
    return this.getTotal(TOTAL, FEC) * MONITOREO;
  },
  getPorcentaje: function (tipo) {
    return (this.getTotalReal(TOTAL, tipo) / (this.getTotalReal(TOTAL, PLAN) || 1) * 100);
  },
  getPorcentajeGrupo: function (grup_id, tipo) {
    return (this.getTotalReal(grup_id, tipo) / (this.getTotalReal(TOTAL, tipo) || 1) * 100);
  },*/
  render: function () {
    var cols = this.props.grupos
      .map(function (grupo) {
        return <TableTotalCell
          key={grupo.grup_id}
          edit={this.props.edit}
          value={this.props.getTotalReal(grupo.grup_id, this.props.tipoId).toFixed(2)}
          per={this.props.getPorcentajeGrupo(grupo.grup_id, this.props.tipoId).toFixed(1)}
          grupo={grupo.grup_id}
          tipoId={this.props.tipoId}
          tipo={this.props.tipo} />
      }.bind(this));

    var per = (
      <td className="text-right success" tabIndex={this.props.tipoId}>
        {this.props.getPorcentaje(this.props.tipoId).toFixed(2)}%
        {this.props.edit ?
          <input className={"porcentaje" + this.props.tipo} type="hidden"
            name={"porcentajes[" + this.props.tipo + ']'}
            value={this.props.getPorcentaje(this.props.tipoId).toFixed(2)} />
          : ''}
      </td>
    );

    return (
      <tr>
        <th>Total {this.props.tipo.toUpperCase()}</th>
        {cols}
        {per}
      </tr>
    );
  }
})

var TableTotalHeader = React.createClass({
  render: function () {
    console.log('grupos', this.props.grupos);
    var cols = this.props.grupos
      .map(function (grupo) {
        return <th className="text-center" key={grupo.grup_id}>{grupo.grup_short}</th>
      });

    return (
      <thead>
        <tr className="success">
          <th />
          {cols}
          <th className="text-right">%</th>
        </tr>
      </thead>
    );
  }
});

var TableTotalBody = React.createClass({
  getSubactividades: function () {
    var actividades = this.props.actividades;
    var subactividades = [];

    Object.keys(actividades)
      .map(function (key) {
        subactividades = subactividades.concat(actividades[key]);
      });

    return subactividades;
  },
  getTotal: function (grup_id, tipo) {

    var total = this.getSubactividades()
      .filter(function (subactividad) {
        return ((subactividad.grupo == grup_id) || (grup_id == TOTAL)) && ((subactividad.financiamiento == tipo) || (tipo == PLAN));
      })
      .map(function (subactividad) {
        return (subactividad.costounitario * subactividad.cantidad) || 0;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);

    return total;
  },
  getTotalReal: function (grup_id, tipo) {
    return this.getTotal(grup_id, tipo) + (((grup_id == SUPER || grup_id == TOTAL) && tipo != PROPIO) ? this.getOtrosFEC() : 0);
  },
  getOtrosFEC: function () {
    console.log('otros fect', this.getTotal(TOTAL, FEC), MONITOREO);
    return this.getTotal(TOTAL, FEC) * MONITOREO;
  },
  getPorcentaje: function (tipo) {
    return (this.getTotalReal(TOTAL, tipo) / (this.getTotalReal(TOTAL, PLAN) || 1) * 100);
  },
  getPorcentajeGrupo: function (grup_id, tipo) {
    return (this.getTotalReal(grup_id, tipo) / (this.getTotalReal(TOTAL, tipo) || 1) * 100);
  },
  getTotalPasantias: function () {
    return Object.keys(this.props.actividades)
      .map(function (actividad) {
        return this.props.actividades[actividad]
          .filter(function (subactividad) {
            return subactividad.gasto == GASTO_PASANTIAS && subactividad.financiamiento == FEC;
          })
          .map(function (subactividad) {
            return subactividad.cantidad * subactividad.costounitario;
          })
          .reduce(function (a, b) {
            return a + b;
          }, 0);
      }.bind(this))
      .reduce(function (a, b) {
        return a + b;
      }, 0);
  },
  render: function () {

    var methods = {
      getTotalReal: this.getTotalReal,
      getPorcentajeGrupo: this.getPorcentajeGrupo,
      getPorcentaje: this.getPorcentaje
    };

    var tipos = ['fec', 'propio', 'plan'];

    var rows = tipos
      .map(function (tipo, i) {
        return (
          <TableTotalRow key={tipo} {...this.props} {...methods} tipo={tipo} tipoId={i + 1} />
        );
      }.bind(this));

    return (
      <tbody>
        {rows}
        {<tr>
          <td colSpan="11" className="text-right">
            <input type="hidden" className="totalpasantias" value={this.getTotalPasantias()} />
            <span>Total pasantías FEC: {this.getTotalPasantias()}</span>
            <small className="text-primary">
              ({(this.getTotalPasantias() / (this.getTotalReal(TOTAL, FEC) || 1) * 100).toFixed(1)}%)
            </small>
          </td>
        </tr>}
      </tbody>
    );
  }
});

var TableTotal = React.createClass({
  getInitialState: function () {
    return {};
  },
  reset: function () {
    //console.log('foo');
    this.replaceState({}, function () { $('.tr-actividad').each(updateTotal); });
    //console.log(this.state);
  },
  update: function (factor, actividad, data) {
    //console.log(factor, actividad, data);

    var indentifier = '' + factor + '.' + actividad;

    var object = {};
    object[indentifier] = {$set: data};
    //console.log(object);

    var data = React.addons.update(this.state, object);

    this.setState(data);
    //console.log(object);
    //console.log(this.state);
  },
  render: function () {

    var grupos = aGrupo
      .concat([
        {
          grup_id: SUPER,
          grup_short: 'Supervisión y monitoreo',
        },
        {
          grup_id: TOTAL,
          grup_short: 'Total',
        }
      ]);

    return (
      <table className="table table-bordered table-condensed table-total">
        <TableTotalHeader grupos={grupos} />
        <TableTotalBody grupos={grupos} actividades={this.state} edit={this.props.edit} />
      </table>
    );
  }
});



// =============================================================================



var ControlStatic = React.createClass({
  render: function () {
    var className = 'form-control-static ';

    if ( ! isNaN(this.props.children)) {
      className += 'text-right ';
    }

    className += (this.props.className || '');

    return (
      <p className={className} title={this.props.title}>{this.props.children}</p>
    );
  }
});

var TxtDescripcion = React.createClass({
  title: 'Descripción',
  handleChange: function (event) {
    this.props.onUserChange(this.props.subactividad, {descripcion: event.target.value});
  },
  render: function () {
    return (
      <div>
        <small>{this.title}</small>
        {this.props.edit ?
          <textarea
            className="form-control"
            name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][descripcion]'}
            value={this.props.data.descripcion}
            onChange={this.handleChange}
            placeholder={this.title}
            required />
          :
          <ControlStatic title={this.title}>{this.props.data.descripcion}</ControlStatic> }
      </div>
    );
  }
});

var SelGasto = React.createClass({
  componentDidMount: function () {
    if (this.refs.select) {
      this.refs.select.value = this.props.data.gasto;
    }
  },
  handleChange: function (event) {
    var grup_id = $(event.target).find('option:selected').parent().data('id');

    var unidad = (grup_id == GRUPO_RRHH) ? 'c/u' : '';

    var financiamiento = (event.target.value == OTROS) ? PROPIO : this.props.data.financiamiento;

    this.props.onUserChange(this.props.subactividad,
      {
        grupo: grup_id,
        gasto: event.target.value,
        tipo: '',
        item: '',
        pdf: '',
        unidad: unidad,
        costosRRHH: {},
        cantidadesRRHH: {},
        costounitario: '',
        financiamiento: financiamiento,
      });
  },
  render: function () {
    var gastos = '<option value="">Seleccione gasto</option>' +
      getOptions(aGasto,
        {
          groupId: 'grup_id',
          groupName: 'grup_nombre',
          rowId: 'gael_id',
          rowName: 'gael_nombre',
        });

    var selected = aGasto
      .filter(function (gasto) {
        return gasto.gael_id == this.props.data.gasto;
      }.bind(this))[0] || {};

    return (
      <div>
        <small>Gasto</small>
        {this.props.edit ?
          <div>
            <select
              className="form-control"
              name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][gasto]'}
              ref='select'
              value={this.props.data.gasto}
              onChange={this.handleChange}
              title="Seleccione gasto"
              required
              dangerouslySetInnerHTML={{__html: gastos}} >
            </select>
            <input type="hidden"
              name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][grupo]'}
              value={this.props.data.grupo} />
          </div>
          :
          <ControlStatic title="Gasto">{selected.gael_nombre}</ControlStatic> }
      </div>
    );
  }
});

var InpGasto = React.createClass({
  handleChange: function (event) {
    this.props.onUserChange(this.props.subactividad, {gastoOtro: event.target.value});
  },
  render: function () {
    return this.props.edit ?
      <input
        className="form-control input-otro"
        name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][gastoOtro]'}
        value={this.props.data.gastoOtro}
        onChange={this.handleChange}
        placeholder="Especifique gasto"
        title="Especifique gasto"
        required />
      :
      <ControlStatic>{this.props.data.gastoOtro}</ControlStatic>
  }
});

var Gasto = React.createClass({
  render: function () {
    return (
      <div>
        <SelGasto {...this.props} />
        {this.props.showInpGasto ?
          <InpGasto {...this.props} />
          : ''}
      </div>
    );
  }
});

var SelTipo = React.createClass({
  handleChange: function (event) {
    this.props.onUserChange(this.props.subactividad,
      {
        tipo: event.target.value,
        item: '',
        pdf: '',
        unidad: '',
        costounitario: '',
      });
  },
  render: function () {

    var other = {tiga_id: OTROS, tiga_nombre: 'Otros (especifique)'};

    var options = aTipo
      .filter(function (tipo) {
        return tipo.gael_id == this.props.data.gasto;
      }.bind(this))
      .concat([other])
      .map(function (tipo) {
        return <option key={tipo.tiga_id} value={tipo.tiga_id}>{tipo.tiga_id + ' - ' + tipo.tiga_nombre}</option>;
      });

    var selected = aTipo
      .concat([other])
      .filter(function (tipo) {
        return tipo.tiga_id == this.props.data.tipo;
      }.bind(this))[0] || {};

    return (
      <div>
        <small>Tipo</small>
        {this.props.edit ?
          <select
            className="form-control"
            name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][tipo]'}
            value={this.props.data.tipo}
            onChange={this.handleChange}
            title="Seleccione tipo"
            required >
            <option value="">Seleccione tipo</option>
              {options}
          </select>
          :
          <ControlStatic title="Tipo">{selected.tiga_nombre}</ControlStatic> }
      </div>
    );
  }
});

var InpTipo = React.createClass({
  handleChange: function (event) {
    this.props.onUserChange(this.props.subactividad, {tipoOtro: event.target.value});
  },
  render: function () {
    return this.props.edit ?
      <input
        className="form-control input-otro"
        name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][tipoOtro]'}
        value={this.props.data.tipoOtro}
        onChange={this.handleChange}
        placeholder="Especifique tipo"
        title="Especifique tipo"
        required />
      :
      <ControlStatic>{this.props.data.tipoOtro}</ControlStatic>
  }
});

var Tipo = React.createClass({
  render: function () {
    return (
      <div>
        <SelTipo {...this.props} />
        {this.props.showInpTipo ?
          <InpTipo {...this.props} />
          : ''}
      </div>
    );
  }
});

var TipoRRHH = React.createClass({
  handleChange: function (event) {
    var $target = $(event.target);

    var field = $target.data('fieldname');
    var id = $target.data('id');
    var value = $target.val();

    this.props.handleChangeRRHH(field, id, value);
  },
  /*componentDidMount: function () {
    if (this.props.edit) {
      this.props.onUserChange(this.props.subactividad, {unidad: 'c/u', costounitario: this.props.getCostoUnitario()});
    }
  },*/
  /*componentWillUnmount: function () {
    if (this.props.edit) {
      console.log('unmounting');
      this.props.onUserChange(this.props.subactividad, {unidad: '', costounitario: '', costosRRHH: {}, cantidadesRRHH: {}});
    }
  },*/
  /*componentWillUpdate: function () {
    if (this.props.edit) {
      console.log('updating');
      //console.log('props');
      if (Object.keys(this.props.data.costosRRHH).length)
        this.props.onUserChange(this.props.subactividad, {unidad: '', costounitario: '', costosRRHH: {}, cantidadesRRHH: {}});
    }
  },*/
  getTipos: function () {
    return aTipo
      .filter(function (item) {
        return item.gael_id == this.props.data.gasto;
      }.bind(this));
  },
  /*getCostoUnitario: function () {
    return this
      .getTipos()
        .map(function (item) {
          return +this.refs['tipo' + item.tiga_id].getDOMNode().value || 0;
        }.bind(this))
        .reduce(function (a, b) { return a + b; });
  },*/
  render: function () {

    var inputs = this.getTipos()
      .map(function (item) {
        return (
          <div key={item.tiga_id}>
            {this.props.edit ? '' : <small>{item.tiga_nombre}</small>}
            {this.props.edit ?
              <input
                type="number" min="0"
                _key={item.tiga_id}
                _ref={item.tiga_id}
                className="form-control"
                data-fieldname="costosRRHH"
                data-id={item.tiga_id}
                name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][costosRRHH][' + item.tiga_id + ']'}
                value={(this.props.data.costosRRHH || {})[item.tiga_id]}
                onChange={this.handleChange}
                placeholder={item.tiga_nombre + '(costo)'}
                title={item.tiga_nombre + ' (costo)'}
                required />
            :
            <ControlStatic title={item.tiga_nombre}>{(this.props.data.costosRRHH || {})[item.tiga_id]}</ControlStatic> }
          </div>
        );
      }.bind(this));

    return (
      <div>
        <small>Costos:</small>
        {inputs}
      </div>
    );
  }
});

var SelItem = React.createClass({
  handleChange: function (event) {
    var value = event.target.value;

    var selectedItem = aItem
      .filter(function (item) {
        return item.itga_id == value;
      })[0] || {};

    this.props.onUserChange(this.props.subactividad,
      {
        item: value,
        pdf: selectedItem.itga_path || '',
        unidad: selectedItem.itga_unidad || '',
        costounitario: selectedItem.itga_precio || '',
      });
  },
  render: function () {

    var other = {itga_id: OTROS, itga_nombre: 'Otros (especifique)'};

    var options = aItem
      .filter(function (item) {
        return item.tiga_id == this.props.data.tipo;
      }.bind(this))
      .concat([other])
      .map(function (item) {
        return <option key={item.itga_id} value={item.itga_id}>{item.itga_id + ' - ' + item.itga_nombre}</option>;
      });

    var selected = aItem
      .concat([other])
      .filter(function (item) {
        return item.itga_id == this.props.data.item;
      }.bind(this))[0] || {};

    return (
      <div>
        <small>Item</small>
        {this.props.edit ?
          <select
            className="form-control"
            name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][item]'}
            value={this.props.data.item}
            onChange={this.handleChange}
            title="Seleccione item"
            required >
            <option value="">Seleccione item</option>
            {options}
          </select>
          :
          <ControlStatic title="Item">{selected.itga_nombre}</ControlStatic> }
      </div>
    );
  }
});

var InpItem = React.createClass({
  handleChange: function (event) {
    this.props.onUserChange(this.props.subactividad, {itemOtro: event.target.value});
  },
  render: function () {
    return this.props.edit ?
      <input
        className="form-control input-otro"
        name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][itemOtro]'}
        value={this.props.data.itemOtro}
        onChange={this.handleChange}
        placeholder="Especifique item"
        title="Especifique item"
        required />
      :
      <ControlStatic>{this.props.data.itemOtro}</ControlStatic>
  }
});

var Item = React.createClass({
  render: function () {
    return (
      <div>
        <SelItem {...this.props} />
        {this.props.showInpItem ?
          <InpItem {...this.props} />
          : ''}
      </div>
    );
  }
});

var ItemRRHH = React.createClass({
  handleChange: function (event) {
    var $target = $(event.target);

    var field = $target.data('fieldname');
    var id = $target.data('id');
    var value = $target.val();

    this.props.handleChangeRRHH(field, id, value);
  },
  /*componentDidMount: function () {
    if (this.props.edit) {
      this.props.onUserChange(this.props.subactividad, {unidad: 'c/u', costounitario: this.props.handleChange()});
    }
  },
  componentWillUnmount: function () {
    if (this.props.edit) {
      this.props.onUserChange(this.props.subactividad, {unidad: '', costounitario: ''});
    }
  },*/
  getTipos: function () {
    return aTipo
      .filter(function (item) {
        return item.gael_id == this.props.data.gasto;
      }.bind(this));
  },
  /*getCostoUnitario: function () {
    return this
      .getTipos()
        .map(function (item) {
          return +this.refs['tipo' + item.tiga_id].getDOMNode().value || 0;
        }.bind(this))
        .reduce(function (a, b) { return a + b; });
  },*/
  render: function () {

    var inputs = this.getTipos()
      .map(function (item) {
        return (
          <div key={item.tiga_id}>
            {this.props.edit ? '' : <small>{item.tiga_nombre}</small>}
            {this.props.edit ?
              <input
                type="number" min="0"
                _key={item.tiga_id}
                _ref={item.tiga_id}
                className="form-control"
                data-fieldname="cantidadesRRHH"
                data-id={item.tiga_id}
                name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][cantidadesRRHH][' + item.tiga_id + ']'}
                value={(this.props.data.cantidadesRRHH || {})[item.tiga_id]}
                onChange={this.handleChange}
                placeholder={item.tiga_nombre + ' (cantidad)'}
                title={item.tiga_nombre + ' (cantidad)'}
                required />
            :
            <ControlStatic title={item.tiga_nombre}>{(this.props.data.cantidadesRRHH || {})[item.tiga_id]}</ControlStatic> }
          </div>
        );
      }.bind(this));

    return (
      <div>
        <small>Cantidades:</small>
        {inputs}
      </div>
    );
  }})

/*var BtnProcess = React.createClass({
  getInitialState: function () {
    return {
      process: false,
    };
  },
  render: function () {
    return (
      <div className={"input-group-addon file_process " + (this.state.process ? '' : 'hidden')}>
			   <img src="../../gproc/media/gproc_lte/img/ajax-loader.gif" />
		  </div>
    );
  }
});*/

var InpUpload = React.createClass({
  onFilesAdded: function () {
    this.uploader.start();
  },
  onFileUploaded: function (a, b, c) {
    console.log(a, b, c);

    this.props.onUserChange(this.props.subactividad, {pdf: c.response});
  },
  onError: function (a,b,c) {
    console.log('error',a,b,c);
  },
  activatePlugin: function () {
    this.settings = {
      url: '/presupuesto/upload_especificacion' + window.location.search,
      //url: 'upload.php',
      browse_button: ReactDOM.findDOMNode(this.refs.button),
      multi_selection: false,
      multipart_params: {
        identifier: "A." + this.props.factor + '.' + (this.props.actividad + 1) + '.' + this.props.subactividad,
      },
    };

    this.uploader = new plupload.Uploader(this.settings);
    this.uploader.bind('FilesAdded', this.onFilesAdded);
    this.uploader.bind('FileUploaded', this.onFileUploaded);
    this.uploader.bind('Error', this.onError);
    this.uploader.init();
  },
  removePlugin: function () {
    this.uploader.unbind('FilesAdded', this.onFilesAdded);
    this.uploader.unbind('FileUploaded', this.onFileUploaded);
    this.uploader.unbind('Error', this.onError);
    this.uploader.destroy();
    this.uploader = null;
  },
  componentDidMount: function () {
    if (this.props.enabled) this.activatePlugin();
  },
  componentWillUpdate: function (nextProps) {
    if ( ! this.uploader && nextProps.enabled) this.activatePlugin();

    if (this.uploader && ! nextProps.enabled) this.removePlugin();
  },
  componentWillUnmount: function () {
    console.log('unmount');
  },
  render: function() {
    return (
      <div className="input-group-btn _file_upload">

        <button type="button" className={"btn btn-default btn-flat " + (this.props.enabled ? '' : 'disabled')}
          data-id={"A." + this.props.factor + '.' + (this.props.actividad + 1) + '.' + this.props.subactividad} ref="button"
          title={this.props.enabled ? 'Subir documento de especificación técnica' : false} >
          <i className="fa fa-upload" />
        </button>

      </div>
    );
  }
});

var BtnView = React.createClass({
  getDefaultProps: function () {
    return {
      enabled: false,
    };
  },
  render: function() {
    var enabled = this.props.data.pdf ? true : false;

    return (
      <div className="input-group-btn">
				<a href={"/file/" + this.props.data.pdf}
          className={"btn btn-default btn-flat btn_file_view " + (enabled ? '' : 'disabled')}
          target="_blank" title={enabled ? "Ver documento de especificación técnica" : false} >
					<i className="fa fa-file-text full" />
					<i className="fa fa-file-text-o empty" />
				</a>
        {this.props.edit ?
				  <input type="hidden"
            name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][pdf]'}
            value={this.props.data.pdf} />
          : ''}
			</div>
    );
  }
});

var Pdf = React.createClass({
  render: function () {

    return (
      <div>
        {/*<BtnProcess ref="btnProcess" />*/}
        <small>Especificación</small>
        {this.props.edit ?
          <InpUpload {...this.props} ref="inpUpload" enabled={this.props.shouldInpUpload} />
          : ''}
        <BtnView {...this.props} />
      </div>
    );
  }
});

var InpUnidad = React.createClass({
  handleChange: function (event) {
    this.props.onUserChange(this.props.subactividad, {unidad: event.target.value});
  },
  render: function () {
    return (
      <div>
        <small>Unidad</small>
        {this.props.edit ?
          <input
            className="form-control"
            name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][unidad]'}
            value={this.props.data.unidad}
            onChange={this.handleChange}
            placeholder="Unidad de medida"
            title="Unidad de medida"
            readOnly={this.props.readOnly}
            required={ ! this.props.readOnly} />
          :
          <ControlStatic title="Unidad">{this.props.data.unidad}</ControlStatic> }
      </div>
    );
  }
});

var InpCostoUnitario = React.createClass({
  handleChange: function (event) {
    this.props.onUserChange(this.props.subactividad, {costounitario: event.target.value});
  },
  render: function () {
    return (
      <div>
        <small>C. unitario</small>
        {this.props.edit ?
          <input
            type="number" min="0"
            className="form-control"
            name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][costounitario]'}
            value={this.props.data.costounitario}
            onChange={this.handleChange}
            placeholder="Costo unitario"
            title="Costo unitario"
            readOnly={this.props.readOnly}
            required={ ! this.props.readOnly} />
          :
          <ControlStatic title="C. unitario">{this.props.data.costounitario}</ControlStatic> }
      </div>
    );
  }
});

var InpCantidad = React.createClass({
  handleChange: function (event) {
    this.props.onUserChange(this.props.subactividad, {cantidad: event.target.value});
  },
  render: function () {
    return (
      <div>
        <small>Cantidad</small>
        {this.props.edit ?
          <input
            type="number" min="0"
            className="form-control"
            name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][cantidad]'}
            value={this.props.data.cantidad}
            onChange={this.handleChange}
            placeholder="Cantidad"
            title="Cantidad"
            required />
          :
          <ControlStatic title="Cantidad">{this.props.data.cantidad}</ControlStatic> }
      </div>
    );
  }
});

var InpSubtotal= React.createClass({
  render: function () {
    return (
      <div>
        <small>Subtotal</small>
        {this.props.edit ?
          <input
            className="form-control"
            name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][subtotal]'}
            value={(this.props.data.cantidad * this.props.data.costounitario) || 0}
            placeholder="Subtotal"
            title="Subtotal"
            readOnly />
          :
          <ControlStatic title="Subtotal">{(this.props.data.cantidad * this.props.data.costounitario) || 0}</ControlStatic> }
      </div>
    );
  }
});

var SelFinanciamiento = React.createClass({
  handleChange: function (event) {
    this.props.onUserChange(this.props.subactividad, {financiamiento: event.target.value});
  },
  render: function () {
    var financiamientos = [{id: FEC, title: 'FEC'}, {id: PROPIO, title: 'Propio'}];

    var options = financiamientos.map(function (item) {
      return <option key={item.id} value={item.id}>{item.title}</option>;
    });

    var selected = financiamientos
      .filter(function (item) {
        return item.id == this.props.data.financiamiento;
      }.bind(this))[0] || {};

    return (
      <div>
        <small>Financiamiento</small>
        {this.props.edit ?
          (this.props.selectable ?
            <select
              className="form-control"
              name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][financiamiento]'}
              value={this.props.data.financiamiento}
              onChange={this.handleChange}
              title="Seleccione financiamiento"
              required >
              <option value="">Seleccione financiamiento</option>
              {options}
            </select>
            :
            <div>
              <input className="form-control"
                value="Propio"
                title="Seleccione financiamiento"
                disabled />
              <input type="hidden"
                name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][subactividades][' + this.props.subactividad + '][financiamiento]'}
                value={this.props.data.financiamiento}
                title="Seleccione financiamiento" />
            </div>)
          :
          <ControlStatic title="Financiamiento">{selected.title}</ControlStatic> }
      </div>
    );
  }
});

var BtnEliminarSubactividad = React.createClass({
  onClick: function () {
    this.props.onRemove(this.props.subactividad);
  },
  render: function () {
    return (
      <button type="button"
        title="Eliminar subactividad" onClick={this.onClick} className="btn btn-flat btn-xs btn-danger">
        <i className="fa fa-times"></i></button>
    );
  }
});

var Subactividad = React.createClass({
  handleChangeRRHH: function (field, id, value) {
    var data = {};
    data[field] = (this.props.data[field] || {});

    data[field][id] = value;
    console.log(this.props.data, data);

    this.props.onUserChange(this.props.subactividad, data);

    var costounitario = this.getCostoUnitario();
    console.log(costounitario);

    this.props.onUserChange(this.props.subactividad, {costounitario: costounitario});

  },
  getCostoUnitario: function () {
    var data = this.props.data;
    //console.log(data);
    //return 123;

    var costounitario = Object.keys(data.costosRRHH)
      .map(function (key) {
        return (+data.costosRRHH[key] * +data.cantidadesRRHH[key] || 0);
      })
      .reduce(function (a, b) { return a + b; });

    return costounitario;
  },
  render: function () {
    var isGastoOtro = (this.props.data.gasto == OTROS);

    var isGastoRRHH = (this.props.data.grupo == GRUPO_RRHH);

    var showTipo = (! isGastoOtro);
    var isTipoOtro = (this.props.data.tipo == OTROS);
    var showItem = (showTipo && ! isTipoOtro);
    var isItemOtro = (this.props.data.item == OTROS);
    var shouldInputsReadOnly = (showItem && ! isItemOtro);
    var shouldInpUpload = ! shouldInputsReadOnly;

    return (
      <tr>
        <td className="td-descripcion">
          <TxtDescripcion {...this.props} />
        </td>
        <td className="td-gasto">
          <Gasto {...this.props} showInpGasto={isGastoOtro} />
        </td>
        <td className="td-tipo">
          {showTipo && ! isGastoRRHH ?
            <Tipo {...this.props} showInpTipo={isTipoOtro} />
            : ''}
          {isGastoRRHH ?
            <TipoRRHH {...this.props} handleChangeRRHH={this.handleChangeRRHH} getCostoUnitario={this.getCostoUnitario} _ref="costos" />
            : ''}
        </td>
        <td className="td-item">
          {showItem && ! isGastoRRHH ?
            <Item {...this.props} showInpItem={isItemOtro} />
            : ''}
          {isGastoRRHH ?
            <ItemRRHH {...this.props} handleChangeRRHH={this.handleChangeRRHH} _ref="cantidades" />
            : ''}
        </td>
        <td className="td-pdf" tabIndex="100">
          <Pdf {...this.props} shouldInpUpload={shouldInpUpload || isGastoRRHH} />
        </td>
        <td className="td-unidad">
          <InpUnidad {...this.props} readOnly={shouldInputsReadOnly} />
        </td>
        <td className="td-costounitario">
          <InpCostoUnitario {...this.props} readOnly={shouldInputsReadOnly} shouldCalculate={isGastoRRHH} />
        </td>
        <td className="td-cantidad">
          <InpCantidad {...this.props} />
        </td>
        <td className="td-subtotal">
          <InpSubtotal {...this.props} />
        </td>
        <td className="td-financia">
          <SelFinanciamiento {...this.props} selectable={ ! isGastoOtro} />
        </td>
        <td>
          {this.props.edit ? <BtnEliminarSubactividad {...this.props} /> : '' }
        </td>
      </tr>
    );
  }
});

var ActividadHeader = React.createClass({
  render: function () {
    return (
      <thead>
				<tr className="success">
					<th colSpan="11">
  						A.{this.props.re}.{this.props.actividad + 1}<br />
						{this.props.actividadDescripcion}
					</th>
        </tr>
        <tr>
					<th colSpan="11">
						Subactividades
					</th>
				</tr>
      </thead>
    );
  }
});

var ActividadTotalesHeader = React.createClass({
  render: function () {

    var grupos = aGrupo.concat([{
      grup_id: 'total',
      grup_short: 'Total actividad',
    }]);

    var totalHeaders = grupos.map(function (grupo) {
      return (
        <th key={grupo.grup_id} className="text-center">
          {grupo.grup_short}
        </th>
      );
    }.bind(this));

    return (
      <thead>
        <tr className="success">
          {totalHeaders}
        </tr>
      </thead>
    );
  }
});

var ActividadTotalesBody = React.createClass({
  getTotal: function (grup_id) {

    var total = this.props.subactividades
      .filter(function (subactividad) {
        return ((subactividad.grupo == grup_id) || (grup_id == TOTAL));
      })
      .map(function (subactividad) {
        return (subactividad.costounitario * subactividad.cantidad) || 0;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);

    return total;
  },
  render: function () {
    var grupos = aGrupo.concat([{
      grup_id: TOTAL,
    }]);

    var totalGrupos = grupos
      .map(function (grupo) {
        return (
          <td key={grupo.grup_id}>
            {this.props.edit ?
              <input className="form-control text-center"
                name={'actividades[' + this.props.factor + '][' + this.props.actividad + '][totales][' + grupo.grup_id + ']'}
                value={this.getTotal(grupo.grup_id)} readOnly />
              :
              <ControlStatic className="text-center">{this.getTotal(grupo.grup_id)}</ControlStatic> }
          </td>
        );
      }.bind(this));

    return (
      <tbody>
        <tr className="success">
          {totalGrupos}
        </tr>
      </tbody>
    );
  }
});

var ActividadTotales = React.createClass({
  render: function () {
    return (
      <tfoot>
        <tr className="success">
					<th colSpan="11">
            <strong>Totales por actividad</strong>
            <table className="table table-bordered">
              <ActividadTotalesHeader {...this.props} />
              <ActividadTotalesBody {...this.props} />
            </table>
					</th>
				</tr>
			</tfoot>
    );
  }
});

var BtnAgregar = React.createClass({
  render: function () {
    return (
      <button type="button" className="btn btn-primary btn-flat btn-sm" title="Agregar Subactividad"
        onClick={this.props.onClick}>
				Agregar Subactividad
			</button>
    );
  }
});

var BtnEliminar = React.createClass({
  render: function () {
    return (
      <button type="button" className="btn btn-danger btn-flat btn-sm" title="Eliminar Subactividad"
        onClick={this.props.onClick}>
        Eliminar Subactividad
			</button>
    );
  }
});

var BtnGuardar = React.createClass({
  handleClick: function () {
    $('.btn_save').first().click();
  },
  render: function () {
    return (
      <button type="button" className="btn btn-default btn-flat btn-sm pull-right" title="Guardar" onClick={this.handleClick}>
        Guardar
			</button>
    );
  }
});

var Actividad = React.createClass({
  getInitialState: function () {
    return {
      subactividades: [{uuid: uuid()}],
    };
  },

  addSubactividad: function () {
    var data = React.addons.update(this.state, {subactividades: {$push: [{uuid: uuid()}]}});

    this.setState(data);
    setDirty();
  },

  removeSubactividad: function () {
    if (this.state.subactividades.length < 2) return;

    var data = React.addons.update(this.state, {subactividades: {$splice: [[-1]]}});

    this.setState(data);
    tableTotal.update(this.props.factor, this.props.id, data.subactividades);
    setDirty();
  },

  removeAnySubactividad: function (subactividad) {
    if (this.state.subactividades.length < 2) return;

    console.log(subactividad);
    console.log(this.state);

    var data = React.addons.update(this.state, {subactividades: {$splice: [[subactividad, 1]]}});

    this.setState(data);
    tableTotal.update(this.props.factor, this.props.id, data.subactividades);
    setDirty();
  },

  onUserChange: function (subactividad, data) {
    var object = {};

    object[subactividad] = {$merge: data};

    var data = React.addons.update(this.state, {subactividades: object});

    this.setState(data);
    tableTotal.update(this.props.factor, this.props.id, data.subactividades);
    setDirty();
  },

  componentWillMount: function () {
    var data = this.props.initialData;

    if (data) {

      data.subactividades.map(function (subactividad, j) {
        data.subactividades[j].uuid = uuid();
      });

      this.setState(data);
      tableTotal.update(this.props.factor, this.props.id, data.subactividades);
    }
  },

  render: function () {

    var props = {
      actividad: this.props.id,
      factor: this.props.factor,
      re: this.props.re,
      actividadDescripcion: this.props.descripcion,
      edit: this.props.edit,
    };

    var subactividadList = this.state.subactividades.map(function (subactividad, j) {
      return (
        <Subactividad
          key={subactividad.uuid}
          subactividad={j}
          {...props}
          onUserChange={this.onUserChange}
          onRemove={this.removeAnySubactividad}
          data={subactividad} />);
    }.bind(this));

    return (
      <div className="well-actividad">
        <table className="table _table-bordered table-presupuesto">
          <ActividadHeader {...props} />
          <ActividadTotales {...props} subactividades={this.state.subactividades} />
          <tbody>
            {subactividadList}
          </tbody>
        </table>

        <br />
        {this.props.edit ?
          <div>
            <BtnAgregar {...props} onClick={this.addSubactividad} />
            {/*<BtnEliminar {...props} onClick={this.removeSubactividad} />*/}
            <BtnGuardar />
          </div>
          : ''}

      </div>
    );
  }
});



// =============================================================================
