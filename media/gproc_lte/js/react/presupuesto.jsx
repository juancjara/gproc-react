'use strict';

// TableTotal

var edit = !!$('.btn_save').length;

//if ($('#table-total-wrapper').length)
  var tableTotal = ReactDOM.render(<TableTotal edit={edit} />, document.getElementById('table-total-wrapper'));

// [Actividad]

$('.td-actividades').each(function () {
  var $this = $(this);

  //var edit = !!$('.btn_save').length;

  var { detalles, subactividades, factor, re, ...other } = $this.data();

  var list = detalles.map(function (actividad, i) {

    var props = {
      id: i,
      descripcion: actividad.miactividad,
      factor: factor,
      re: re,
      initialData: (subactividades[i] || null)
    }

    return (
      <Actividad key={i} {...props} edit={edit} />
    );
  });

  var wrapper = (
    <div>
      {list}
    </div>
  );

  ReactDOM.render(wrapper, this);
});
