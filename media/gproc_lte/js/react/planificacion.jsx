'use strict';

// TableTotal

var edit = !!$('.btn_save').length;


//if ($('#table-total-wrapper').length)
  var tableTotal = ReactDOM.render(<TableTotal edit={edit} />, document.getElementById('table-total-wrapper'));


function updateTotal () {
  var $this = $(this),
    subactividades = $this.data('subactividades'),
    factor = $this.data('factor'),
    actividad = $this.data('id'),
    hidden = +$this.attr('data-hidden');
    //console.log(subactividades, factor, actividad, hidden);

  if ( ! hidden)
    tableTotal.update(factor, actividad, subactividades);
}

var table = ReactDOM.render(<TablePlanificacion edit={edit} />, document.getElementById('table-planificacion'));
