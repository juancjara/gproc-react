'use strict';

Object.byString = function(o, s, x) {
    if (o == null) return;
    x = x || '|';
    s = s.replace(/\[(\w+)\]/g, x + '$1'); // convert indexes to properties
    s = s.replace(new RegExp('^' + x), '');           // strip a leading dot
    var a = s.split(x);
    for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in o) {
            o = o[k];
        } else {
            return;
        }
    }
    return o;
}

var dimensiones = JSON.parse($('#dimensiones').val() || null),
  data = JSON.parse($('#data').val() || null),
  //data = null,
  data2 = JSON.parse($('#data2').val() || null),
  data5 = JSON.parse($('#data5').val() || null),
  grupos = JSON.parse($('#grupos').val() || null),
  lista = JSON.parse($('#lista_estandares').val() || null);
  //data['factores'][1]['hide'] = 1;
  //data['actividades'][1][0]['estandares'] = ['1581', '1586'];
  //data2 = null;

//console.log(dimensiones, data, data2, data5, lista);



// =============================================================================

var ThDimension = React.createClass({
  onChange: function (e) {
    this.props.changeData('dimensiones', this.props.dimension.dime_codigo, {$merge: {objetivo: e.target.value}});
  },
  render: function () {
    var dimension = this.props.dimension,
      codigo = dimension.dime_codigo;

    return (
      <th rowSpan={Object.keys(dimension.data).length} className="tobjetivo">
        <small>{dimension.dime}</small><br />
        <div className={this.props.hideDimension ? 'hidden' : ''}>
          <label>
  				  O.E.{this.props.d}
          </label>
          {this.props.edit ?
            <div>
  						<textarea maxLength="500" placeholder="Ingrese Objetivo Específico" className="form-control" rows="15"
  							name={"dimensiones[" + codigo + "][objetivo]"}
                value={Object.byString(this, 'props|data|dimensiones|' + codigo + '|objetivo')}
                onChange={this.onChange}
  							required={!this.props.hideDimension} />
              <input type="hidden" name={"dimensiones[" + codigo + "][hide]"}
    					  value={this.props.hideDimension ? 1 : ''} />
              <input type="hidden" name={"dimensiones[" + codigo + "][nFactores]"}
    					  value={this.props.nFactores} />
            </div>
            : <p className="form-control-static">
								{Object.byString(this, 'props|data|dimensiones|' + codigo + '|objetivo')}</p>
            }
        </div>
      </th>
    );
  }
});

var ThFactor = React.createClass({
  onChange: function (e) {
    this.props.changeData('factores', this.props.factor.fact_codigo, {$merge: {resultado: e.target.value}});
  },
  render: function () {
    var factor = this.props.factor,
      codigo = factor.fact_codigo;

    return (
      <th className="tresultado">
        <small>{factor.fact}</small><br />
        <div className={this.props.hideFactor ? 'hidden' : ''}>
          <label title="Cada resultado debe estar asociado a un FACTOR de la matriz de calidad">
						R.{this.props.f}
					</label>
          {this.props.edit ?
            <textarea maxLength="500" placeholder="Ingrese Resultado o Producto" className="form-control" rows="15"
							name={"factores[" + codigo + "][resultado]"} value={Object.byString(this, 'props|data|factores|' + codigo + '|resultado')}
              onChange={this.onChange}
							required={!this.props.hideFactor} />
            :
						<p className="form-control-static">
							{Object.byString(this, 'props|data|factores|' + codigo + '|resultado')}</p>
            }
        </div>
      </th>
    );
  }
});

var ActividadDescripcion = React.createClass({
  onChange: function (e) {
    this.props.changeDataActividades(this.props.factor.fact_codigo, this.props.a, {$merge: {miactividad: e.target.value}});
  },
  render: function () {
    var codigo = this.props.factor.fact_codigo,
      a = this.props.a;

    return (
      <tr>
        <td colSpan="7" className="_tactividades_inner">
          <span className="strong act_name">A.{this.props.f}.{a + 1}</span>
          {this.props.edit ?
            <textarea className="form-control actividad_input" maxLength="500" rows="5"
              placeholder="Ingrese Descripción de la Actividad a Desarrollar"
              name={"actividades[" + codigo + "][" + a + "][miactividad]"} value={this.props.actividad['miactividad']}
              onChange={this.onChange}
              required={!this.props.hideFactor} />
            :
            <p className="form-control-static">
              {this.props.actividad['miactividad']}</p>
            }
        </td>
      </tr>
    );
  }
});

var ResumenActividadHeader = React.createClass({
  render: function () {
    var rows = grupos.map(function (grupo, i) {
      return (
        <th className="td-total text-right" key={i}>{grupo.grup_short}</th>
      );
    });

    return (
      <tr>
        {rows}
        <th className="tactores_inner"
          title="Actores Institucionales involucrados (responsable y miembros del equipo técnico por actividad)">Actores Involucrados</th>
        <th className="tejecucion_inner">Ejecución (quincenas)</th>
      </tr>
    );
  }
});

var ResumenActividadBody = React.createClass({
  onChange: function (e) {
    this.props.changeDataActividades(this.props.factor.fact_codigo, this.props.a, {$merge: {actor_cargo: e.target.value}});
  },
  render: function () {
    var codigo = this.props.factor.fact_codigo;

    var rows = grupos.map(function (grupo, i) {
      var value = Object.byString(data2, 'actividades|' + codigo + '|' + this.props.a + '|totales|' + grupo.grup_id);

      return (
        <td className="td-total" title={grupo.grup_short} key={i}>
          {this.props.edit ?
  				  <input className="form-control text-right total-act"
  					  value={value}
  					  disabled />
            :
  				  <p className="form-control-static text-right">
  					  {value}</p>
            }
				</td>
      );
    }.bind(this));

    var duracion = Object.byString(data5, 'actividades|' + codigo + '|' + this.props.a + '|duracion');

    return (
      <tr>
        {rows}

        <td className="text-center tactores_inner">
          {this.props.edit ?
  					<input name={"actividades[" + codigo + "][" + this.props.a + "][actor_cargo]"}
  						value={this.props.actividad['actor_cargo']}
              onChange={this.onChange}
  						className="tactorcargo form-control text-center" type="text"
              required={!this.props.hideFactor}
  						placeholder="Cargo que ocupa" />
            :
					  <p className="form-control-static text-center">
						  {this.props.actividad['actor_cargo']}</p>
            }
				</td>

				<td className="text-center tejecucion_inner">
					{this.props.edit ?
  					<input placeholder="Quincenas" className="form-control text-center"
  						value={duracion}
  						disabled />
            :
					  <p className="form-control-static text-center">
						  {duracion}</p>
            }
				</td>

      </tr>
    );
  }
})

var SelectEstandar = React.createClass({
  componentDidMount: function () {
    $(ReactDOM.findDOMNode(this)).each(set_options);
	  $(ReactDOM.findDOMNode(this)).each(update_count);
  },
  componentWillUnmount: function () {
    //console.log('unmounting...');

    update_estandares(null, ReactDOM.findDOMNode(this).value);
    update_result();
  },
  componentWillUpdate: function () {
    //console.log('updating...');
    $(ReactDOM.findDOMNode(this)).val($(ReactDOM.findDOMNode(this)).data('selected'));
    //console.log($(this.getDOMNode()).data('selected'));
    //$(this.getDOMNode()).each(set_options);
    //$(this.getDOMNode()).each(update_count);
  },
  render: function () {
    return (
      <select name={'actividades[' + this.props.factor.fact_codigo + '][' + this.props.a + '][estandares][]'}
        data-selected={this.props.estandar || ''}
        data-id={this.props.factorId}
        data-fact={this.props.factor.fact_codigo}
        className="form-control selectestandar" required={!this.props.hideFactor}>
      </select>
    );
  }
});

var RowActividadEstandar = React.createClass({
  onClick: function () {
    this.props.removeEstandar(this.props.factor.fact_codigo, this.props.a, this.props.index);
  },
  render: function () {
    return (
      <tr>
        {this.props.edit ?
          [<td key={1}>
            <SelectEstandar {...this.props} />
          </td>,
          <td className="td-actions" key={2}><br />
            <button type="button" title="Eliminar estándar" className="btn btn-xs btn-flat btn-danger remove_tr"
              onClick={this.onClick}>
              <i className="fa fa-times"></i></button>
          </td>]
          :
          <td>
            <p className="form-control-static">
              {Object.byString(lista, this.props.estandar + '|esta')}</p>
          </td>
          }
      </tr>
    );
  }
});

var ActividadEstandares = React.createClass({
  onClick: function (e) {
    this.props.addEstandar(this.props.factor.fact_codigo, this.props.a);
  },
  render: function () {
    var rows = (this.props.actividad.estandares || [null]).map(function (estandar, i) {
      return <RowActividadEstandar {...this.props} key={i} index={i} estandar={estandar} />
    }.bind(this));

    return (
      <tr>
				<td colSpan="7">
					<span>Seleccione Estándares con los que se relaciona la actividad:</span>

					<table className="_table dynamic">
						<tbody>
              {rows}
						</tbody>
					</table>

					{this.props.edit ?
					  <button type="button" className="btn btn-xs btn-flat btn-primary add_tr espacio_inf_table" title="Agregar estándar"
              onClick={this.onClick}>
						  <i className="fa fa-plus"></i></button>
					  : '' }

				</td>
			</tr>
    );
  }
});

var BtnEliminarActividad = React.createClass({
  onClick: function () {
    //console.log(this.props);
    this.props.removeAnyActividad(this.props.factor.fact_codigo, this.props.a);
  },
  render: function () {
    return (
      <div className="text-right">
        <button type="button" className="btn btn-flat btn-sm btn-danger"
          onClick={this.onClick}
          title="Eliminar actividad"><i className="fa fa-times"></i></button>
      </div>
    );
  }
});

var RowActividad = React.createClass({
  componentDidMount: function () {
    //tableTotal.update(this.props.factor.fact_codigo, this.props.a, (Object.byString(data2, 'actividades|' + this.props.factor.fact_codigo + '|' + this.props.a + '|subactividades') || []))
  },
  render: function () {
    return (
      <tr className="tr-actividad"
        data-factor={this.props.factor.fact_codigo}
        data-id={this.props.a}
        data-subactividades={JSON.stringify(Object.byString(data2, 'actividades|' + this.props.factor.fact_codigo + '|' + this.props.a + '|subactividades') || [])}
        data-hidden={this.props.hideFactor ? 1 : 0}>
        <td>
          {/*this.props.edit ?
            <BtnEliminarActividad {...this.props} />
            : ''*/}
          <table className="table table-condensed table-bordered">
						<tbody>

              <ActividadDescripcion {...this.props} />
              <ResumenActividadHeader />
              <ResumenActividadBody {...this.props} />
              <ActividadEstandares {...this.props} />

            </tbody>
          </table>
        </td>
      </tr>
    );
  }
});

var TableActividades = React.createClass({
  render: function () {
    var codigo = this.props.factor.fact_codigo,
      actividades = this.props.data.actividades[codigo] || [{}];

    var rows = actividades.map(function (actividad, i) {
      return <RowActividad {...this.props} actividad={actividad} key={i} a={i} />
    }.bind(this));

    return (
      <table className="_table table-condensed table-centered table_actividades">
				<tbody className="calc_total">
          {rows}
        </tbody>
      </table>
    );
  }
});

var TdActividades = React.createClass({
  addActividad: function () {
    this.props.addActividad(this.props.factor.fact_codigo);
  },
  removeActividad: function () {
    this.props.removeActividad(this.props.factor.fact_codigo);
  },
  hideResultado: function () {
    this.props.hideResultado(this.props.factor.fact_codigo);
  },
  restoreResultado: function () {
    this.props.restoreResultado(this.props.factor.fact_codigo);
  },
  save: function () {
    $('.btn_save').first().click();
  },
  render: function () {
    var codigo = this.props.factor.fact_codigo,
      hide = !!Object.byString(this, 'props|data|factores|' + codigo + '|hide');

    var canRemoveResultado = !this.props.factor.data['Obligatorios'];

    return (
      <td className="tladoderecho">
        {this.props.edit ?
					<div>
            <input type="hidden" name={"factores[" + codigo + "][hide]"}
						  value={hide ? 1 : ''} />

            <div className="pull-right">
              {hide ?
                <button type="button" className="btn btn-flat btn-sm btn-success btn_restore_resultado"
                  onClick={this.restoreResultado}>
    							<i className="fa fa-undo"></i> Reestablecer resultado
    						</button>
                : '' }
    					{! hide && canRemoveResultado ?
                <button type="button" className="btn btn-flat btn-sm btn-danger btn_remove_resultado"
                  onClick={this.hideResultado}>
    							<i className="fa fa-times"></i> Eliminar resultado
    						</button>
                : '' }
  					</div>
          </div>
          : '' }

        <div className={hide ? 'hidden' : ''}>
          <TableActividades {...this.props} />

          {this.props.edit ?
            <div>
              <button type="button" className="btn btn-primary btn-sm btn-flat add_tr_act" title="Agregar actividad"
                onClick={this.addActividad}>
  							Agregar Actividad
  						</button>
  						{<button type="button" className="btn btn-danger btn-sm btn-flat remove_tr_act" title="Eliminar actividad"
                onClick={this.removeActividad}>
  							Eliminar Actividad
  						</button>}
  						<button type="button" name="finish" className="btn btn-flat btn-default btn-sm _btn_save pull-right"
  							value="0" onClick={this.save}>
  							Guardar
  						</button>
            </div>
            : '' }
        </div>

      </td>
    );
  }
});

var RowFactor = React.createClass({
  render: function () {
    return (
      <tr>
        {this.props.firstRow ?
          <ThDimension {...this.props} />
          : null }
        <ThFactor {...this.props} />
        <TdActividades {...this.props} />
      </tr>
    );
  }
});

var TablePlanificacionHeader = React.createClass({
  render: function () {
    return (
      <thead>
        <tr className="success text-center" nobr="true">
          <th className="tobjetivo" title="Asociar cada objetivo específico con una DIMENSIÓN de la matriz de calidad">
            Objetivo Específico (O.E.)</th>
          <th className="tresultado" title="Cada resultado debe estar asociado a un FACTOR de la matriz de calidad">
            Resultados o Productos (R.)</th>
          <th className="tladoderecho text-center">
            Actividades (A.)
          </th>
        </tr>
      </thead>
    );
  }
});

var TablePlanificacionBody = React.createClass({
  getInitialState: function () {
    return {
      dimensiones: {},
      factores: {},
      actividades: {}
    };
  },
  componentWillMount: function () {
    if (data) {
      this.setState(data);
    }

  },
  componentDidMount: function () {
    //$('.selectestandar').each(set_options);
	  //$('.selectestandar').each(update_count);
	  update_result();

    tableTotal.reset();
    //$('.tr-actividad').each(updateTotal);
  },
  componentDidUpdate: function () {
    //console.log('upd');
    tableTotal.reset();
    //console.log(tableTotal.state);
    //$('.tr-actividad').each(updateTotal);
  },
  changeData: function (a, b, c) {
    var data = this.state;

    if ( ! (b in data[a])) {
      data[a][b] = {};
    }

    var obj = {};
    obj[a] = {};
    obj[a][b] = c;

    data = React.addons.update(data, obj);
    this.setState(data);
  },
  changeDataActividades: function (a, b, c) {
    var data = this.state;

    if ( ! (a in data.actividades)) {
      data.actividades[a] = [{}];
    }

    var obj = {actividades: {}};
    obj.actividades[a] = {};
    obj.actividades[a][b] = c;

    data = React.addons.update(data, obj);
    this.setState(data);
  },
  addEstandar: function (a, b) {
    var data = this.state;

    if ( ! (a in data.actividades)) {
      data.actividades[a] = [{estandares: [null]}];
    }

    if ( ! data.actividades[a][b]['estandares']) {
      data.actividades[a][b]['estandares'] = [null];
    }

    var obj = {actividades: {}};
    obj.actividades[a] = {};
    obj.actividades[a][b] = {estandares: {$push: [null]}};

    data = React.addons.update(data, obj);
    this.setState(data);
  },
  removeEstandar: function (a, b, c) {
    //console.log(a,b,c);
    var data = this.state;

    var obj = {actividades: {}};
    obj.actividades[a] = {};
    obj.actividades[a][b] = {estandares: {$splice: [[c, 1]]}};
    //console.log(obj);

    data = React.addons.update(data, obj);
    //console.log(data);
    this.setState(data);
  },
  addActividad: function (a) {
    //console.log(a);
    var data = this.state;

    if ( ! (a in data.actividades)) {
      data.actividades[a] = [{estandares: [null]}];
    }

    data.actividades[a].push({estandares: [null]});

    this.setState(data);
  },
  removeActividad: function (a) {
    var data = this.state;

    if ( ! (a in data.actividades)) {
      data.actividades[a] = [{estandares: [null]}];
    }

    if (data.actividades[a].length == 1) return;

    var obj = {actividades: {}};
    obj.actividades[a] = {};
    obj.actividades[a] = {$splice: [[-1, 1]]};

    data = React.addons.update(data, obj);
    this.setState(data);
  },
  removeAnyActividad: function (a, b) {
    console.log(a,b);
    //return;

    var data = this.state;

    if ( ! (a in data.actividades)) {
      data.actividades[a] = [{estandares: [null]}];
    }

    if (data.actividades[a].length == 1) return;

    var obj = {actividades: {}};
    obj.actividades[a] = {};
    obj.actividades[a] = {$splice: [[b, 1]]};

    data = React.addons.update(data, obj);
    this.setState(data);
  },
  hideResultado: function (a) {
    var data = this.state;

    if ( ! (a in data.factores)) {
      data.factores[a] = {};
    }

    data.factores[a]['hide'] = 1;

    this.setState(data);
  },
  restoreResultado: function (a) {
    var data = this.state;

    data.factores[a]['hide'] = 0;

    this.setState(data);
  },
  render: function () {
    var d = 0, f = 0, rows = [];

    Object.keys(dimensiones).forEach(function (dimension) {
      var dimension = dimensiones[dimension];

      var nFactores = Object.keys(dimension.data).filter(function (factor) {
        var fact_codigo = dimension.data[factor].fact_codigo;

        return !Object.byString(this, 'state|factores|' + fact_codigo + '|hide');
      }.bind(this)).length;

      var hideDimension = !nFactores;

      var firstRow = true;

      if ( ! hideDimension) d++;

      Object.keys(dimension.data).forEach(function (factorId) {
        var factor = dimension.data[factorId];
        var fact_codigo = factor.fact_codigo;

        var hideFactor = !!Object.byString(this, 'state|factores|' + fact_codigo + '|hide');

        if ( ! hideFactor) f++;

        var props = {
          dimension: dimension,
          factor: factor,
          d: d,
          f: f,
          hideDimension: hideDimension,
          nFactores: nFactores,
          hideFactor: hideFactor,
          firstRow: firstRow,
          factorId: factorId,
          changeData: this.changeData,
          changeDataActividades: this.changeDataActividades,
          addEstandar: this.addEstandar,
          removeEstandar: this.removeEstandar,
          addActividad: this.addActividad,
          removeActividad: this.removeActividad,
          removeAnyActividad: this.removeAnyActividad,
          hideResultado: this.hideResultado,
          restoreResultado: this.restoreResultado
        };

        rows.push(<RowFactor {...this.props} data={this.state} {...props} key={factorId} />)

        firstRow = false;

      }.bind(this));

    }.bind(this));

    return (
      <tbody>
        {rows}
      </tbody>
    );
  }
});

var TablePlanificacion = React.createClass({
  render: function () {
    return (
      <table border="1" className="table table-bordered table-hover">
        <TablePlanificacionHeader />
        <TablePlanificacionBody {...this.props} />
      </table>
    );
  }
});



// =============================================================================
