<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Servicios extends Bulk {
	
	private $pattern;
	
	public function process($file)
	{
		parent::process($file);
		
		$this->insert_data($this->_data);
		
		return TRUE;
	}
	
	public function insert_data($data)
	{
		$this->clean();
		
		$this->pars_code($data);
	}
	
	protected function pars_code($data)
	{
		$matriz = A::f($data)
			->map(function ($arr) {

				list($code, $parent) = $this->get_code($arr['codigo']);
			
				return $matriz = [
					'name' => $arr['descripcion'],
					'id' => $code,
					'data' => json_encode(['unidad' => $arr['tipo'] , 'costo' => $arr['soles'] ]),
					'parent_id' => $parent.'.'.$arr['parent'],
					'path' => 'catalogo/servicios/'.$arr['path'],	
				];

			})	
			->value();

		foreach ($matriz as $val)
		{
			DB::insert('catalogo', ['id', 'name', 'short_name', 'parent_id', 'data', 'path'])
				->values([$val['id'], $val['name'], $val['name'], $val['parent_id'], $val['data'], $val['path']])
				->execute();
		}
	}
	
	protected function clean()
	{
		DB::query(Database::DELETE, "DELETE FROM catalogo WHERE id like 'SC_.%' and data is not null")
			->execute();
	}
	
	protected function get_code($codigo)
	{
		//codigo
		$pattern = $this->pattern();

		preg_match($pattern, $codigo, $coin);
		unset($coin[0]);
		
		$code = $coin[1] . '.' . $coin[2] . '.' . $coin[3];
		$parent = $coin[1];
		
		return [
			$code,
			$parent,
		];
	}
	
	protected function pattern()
	{
		return $this->pattern = "/(SC.)(\d+)(.\d+)/";
	}
	
}
