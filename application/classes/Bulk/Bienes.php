<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Bienes extends Bulk {
	
	public function process($file)
	{
		parent::process($file);
		
		$this->insert_data($this->_data);
		
		return TRUE;
	}
	
	public function insert_data($data)
	{
		$this->clean();
		
		$this->pars_code($data);
	}
	
	protected function pars_code($data)
	{
		$matriz = A::f($data)
			->map(function ($arr) {
				
				// soles
				$soles = $arr['soles'];
				
				// codigo
				list($code, $parent_id) = $this->get_code($arr['codigo']);
				
				$path = str_replace('\\', '/', 'catalogo/bienes/' . $arr['path']);	

				return $matriz = [
					'name' => $arr['descripcion'],
					'id' => $code,
					'data' => json_encode(['unidad' => $arr['tipo'] , 'costo' => $soles ]),
					'parent_id' => $parent_id,
					'path' => $path,	
				];
			})
			->value();
		
		foreach ($matriz as $val)
		{
			DB::insert('catalogo', ['id', 'name', 'short_name', 'parent_id', 'data', 'path'])
				->values([$val['id'], $val['name'], $val['name'], $val['parent_id'], $val['data'], $val['path']])
				->execute();
		}
	}
	
	protected function clean()
	{
		DB::query(Database::DELETE, "DELETE FROM catalogo WHERE id like 'B1.%' and data is not null")
			->execute();
	}
	
	protected function get_code($codigo)
	{
		$code = '';
				
		$first = substr($codigo, 0, 1);

		if ($first == 1)
		{
			$code = 'B';
		}

		$second = substr($codigo, 1, 1);

		$code .= $second.'.';

		if ($second == 1)
		{
			$ar = [2, 3, 4];
		}

		if ($second == 2)
		{
			$ar = [1, 2];
		}

		$i = 1;
		foreach ($ar as $dig)
		{
			if ($dig == end($ar))
			{
				$parent_id = substr($code, 0, strlen($code) - 1);
				$pars = substr($codigo, $dig, end($ar));
				$code .= $pars;
			}
			else
			{
				$pars = substr($codigo, $dig, $i);
				$code .= $pars. '.';
			}
		}
		
		return [
			$code,
			$parent_id,
		];
	}
}
