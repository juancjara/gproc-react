<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Catalogo extends Bulk {
	
	public function process($file)
	{
		parent::process($file);
		
		$this->insert_data($this->_data); 
		
		return TRUE;
	}
	
	public function insert_data($data)
	{
		$this->clean();
		
		$niveles = [1, 2, 3, 4, 5];
		
		$code_0 = $code_1 = $code_2 = $code_3 = $code_4 = $code_5 = NULL;
		
		foreach ($data as $row)
		{
			foreach ($niveles as $nivel)
			{
				$row_nivel = [
					'code' => Arr::get($row, $nivel.'A'),
					'name' => Arr::get($row, $nivel.'B'),
					'costo' => Arr::get($row, $nivel.'C_costo'),
					'unidad' => Arr::get($row, $nivel.'C_unidad'),
				];
				
				$is_item = ($row_nivel['costo'] OR $row_nivel['unidad']);
				
				$code = 'code_'.$nivel;
				$parent_code = 'code_'.($nivel - 1);
				
				if ($row_nivel['code'] == $$code) continue;
				
				if ($row_nivel['code'] == NULL) break;
				
				$data = $is_item ? json_encode(Arr::extract($row_nivel, ['costo', 'unidad'])) : NULL;
				
				DB::insert('catalogo', ['id', 'name', 'short_name', 'parent_id', 'data'])
					->values([$row_nivel['code'], $row_nivel['name'], $row_nivel['name'], $$parent_code, $data])
					->execute();
				
				$$code = $row_nivel['code'];
			}
			
		}
	}
	
	protected function clean()
	{
		DB::query(Database::DELETE, "TRUNCATE TABLE catalogo")
			->execute();
	}
	
	protected function _get_levels($titles, $levels)
	{
		$arr = array();
		$ar = array();
		for ($i = 0; $i < count($titles); $i++) 
		{
			$ar = array(); 
			if(strrpos($titles[$i],"codigo"))
			{
				$current_level = substr($titles[$i], 0,1);
				//debug($current_level);

				for ($j = $i+1; $j < count($titles); $j++) 
				{
					if(substr($titles[$j], 0,1) == $current_level)
					{
						//debug($titles[$j]);
						
						//$ar  = $titles[$j];
						
						array_push($ar,$titles[$j]);
					}
					
					
				}
				$arr[$titles[$i]] = array_merge(array($titles[$i]),$ar); //[$titles[$i],$ar];
				//debug($ar);
				
			}
			
		}
		return $arr;
		
	}
}



