<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report {
	
	protected $name;
	
	protected $title;
	
	protected $folder = '';
	
	protected $header = array();
	
	protected $params = array();
	
	protected $data;
	
	protected $result;
	
	/**
	 * 
	 * @param  string  $name
	 * @param  array  $params
	 * @return  Report
	 */
	public static function factory($name, $params = NULL)
	{
		$class = 'Report_'.ucfirst(Inflector::camelize($name));
		//debug($class);
		
		if ( ! class_exists($class))
		{
			$class = 'Report';
		}
		
		return new $class($name, $params);
	}
	
	public function __construct($name, $params = NULL)
	{
		$this->name = $name;
		
		if ($params)
		{
			$this->params = $params;
		}
	}
	
	public function set($key, $value)
	{
		$this->params[$key] = $value;
		
		return $this;
	}
	
	public function execute()
	{
		$this->set_data();
		
		$this->set_result();
		
		return $this;
	}
	
	public function get()
	{
		$this->execute();
		
		//$view = Theme_View::factory("reportes/{$this->folder}{$this->name}", $this->get_context());
		$view = $this->get_view();
		
		return $view;
	}
	
	public function get_print()
	{
		return $this->get()
			->set('action', 'print');
	}
	
	public function get_excel()
	{
		return $this->get()
			->set('action', 'excel');
	}
	
	public function get_pdf()
	{
		$this->execute();
		
		//$view = Theme_View::factory("reportes/{$this->folder}{$this->name}", $this->get_context());
		$view = $this->get_view();
		
		return PDF::factory()
			->set('view', $view)
			->get();
	}
	
	public function get_context()
	{
		return array(
			'result' => $this->result,
			'title' => $this->title,
			'header' => $this->get_header(),
			'action' => 'view',
		);
	}
	
	public function get_result()
	{
		return $this->result;
	}
	
	public function get_data($script = NULL)
	{
		$script = $script ?: $this->name;
		
		$raw_query = file_get_contents(APPPATH."scripts/{$this->folder}{$script}.sql");
		//debug2($raw_query);
		
		$query = strtr($raw_query, $this->params);
		
		$data = DB::query(Database::SELECT, $query)
			->execute();
			//debug($data);
		
		return $data;
	}
	
	public function get_name()
	{
		return $this->name;
	}
	
	public function get_title()
	{
		return $this->title;
	}
	
	public function get_params()
	{
		return $this->params;
	}
	
	protected function set_data()
	{
		$this->data = $this->get_data()//Report::get_data($this->name, $this->params)
			->as_array();
	}
	
	protected function set_result()
	{
		$this->result = $this->data;
	}
	
	protected function get_header()
	{
		return Theme_View::factory('reportes/header')
			->set('header', $this->header);
	}
	
	protected function get_view()
	{
		return Theme_View::factory("reportes/{$this->folder}{$this->name}", $this->get_context());
	}
	
}
