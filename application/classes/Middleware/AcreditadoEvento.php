<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEvento extends Middleware {
	
	public function execute()
	{
		$acev_id = $this->controller->request->query('acev_id');
		
		$oAcreditadoEvento = ORM::factory('AcreditadoEvento', $acev_id);
		
		if ( ! $oAcreditadoEvento->loaded())
		{
			throw new Middleware_Exception_ModelNotFound("", [':model' => 'AcreditadoEvento', ':id' => $acev_id]);
		}
		
		$oAcreditado = $oAcreditadoEvento->oAcreditado;
		
		$this->controller->oAcreditado = $oAcreditado = $oAcreditadoEvento->oAcreditado;
		$this->controller->userData = $oAcreditado->get_user_data();
		
		$this->controller->oAcreditadoEvento = $oAcreditadoEvento;
		$this->controller->eventoInfo = $this->controller->userData['acev_list'][$acev_id];
	}
	
}
