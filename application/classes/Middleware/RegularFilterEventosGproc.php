<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_RegularFilterEventosGproc extends Middleware {
	
	public function execute()
	{
		$acev_list = array_filter($this->controller->result['acev_list'], function ($evento)
			{
				return ORM::factory('Evento', $evento['even_id'])->loaded();
			});
		
		if ( ! $acev_list)
		{
			throw new Middleware_Exception_RegularNoEvents;
		}
		
		$this->controller->result['acev_list'] = $acev_list;
		
		//Session::instance()->set('objeto', $this->controller->result);
	}
	
}
