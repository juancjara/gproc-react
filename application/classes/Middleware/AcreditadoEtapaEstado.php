<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEtapaEstado extends Middleware {
	
	public function execute()
	{
		if ( ! $this->controller->oAcreditadoEtapa->is_editable())
		{
			$this->controller->action = 'view';
		}
	}
	
}
