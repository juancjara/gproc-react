<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_RedirectOldEvento extends Middleware {
	
	public function execute()
	{
		$request = $this->controller->request;
		$even_id = $this->controller->oAcreditadoEvento->even_id;
		$eventos = Model_Evento::$current_eventos;
		
		if ($this->controller->current AND 
			! in_array($even_id, $eventos))
		{
			$this->controller->redirect($request->referrer());
			
			/*$controller = $request->controller().'Old';
			$action = $request->action();
			//debug2($controller, $action);

			$url = $request->route()->uri(compact('controller', 'action'));
			
			//debug($url.URL::query());
			$this->controller->redirect($url.URL::query());*/
		}
	}
	
}
