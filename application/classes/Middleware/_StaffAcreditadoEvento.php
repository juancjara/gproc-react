<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_StaffAcreditadoEvento extends Middleware {
	
	public function execute()
	{
		try
		{
			$this->controller->middleware('AcreditadoEvento');
		}
		catch (Middleware_Exception_AcreditadoEvento $e)
		{
			if (ACL::instance()->allowed('check_regular'))
			{
				return $this->get_acreditado_evento_from_backend();
			}
			
			throw $e;
		}
	}
	
	private function get_acreditado_evento_from_backend()
	{
		$oAcreditadoEvento = $this->controller->oAcreditadoEvento;
		
		if ( ! oAcreditadoEvento OR ! $oAcreditadoEvento->loaded())
		{
			throw new Middleware_Exception_AcreditadoEvento();
		}
	}
	
}
