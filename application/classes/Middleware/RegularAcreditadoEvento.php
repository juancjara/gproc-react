<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_RegularAcreditadoEvento extends Middleware {
	
	public function execute()
	{
		// Not Regular
		if ( ! ACL::instance()->check_role(Model_Role::ROLE_REGULAR))
		{
			return;
		}
		
		$acev_id = $this->controller->request->query('acev_id');
		
		$oAcreditadoEvento = $this->controller
			->oAcreditado
				->aAcreditadoEvento
				->where('acev_id', '=', $acev_id)
				->find();
		
		if ( ! $oAcreditadoEvento->loaded())
		{
			throw new Middleware_Exception_ModelNotFound("", [':model' => 'AcreditadoEvento', ':id' => $acev_id]);
		}
		
		$this->controller->oAcreditadoEvento = $oAcreditadoEvento;
		$this->controller->eventoInfo = $this->controller->userData['acev_list'][$acev_id];
	}
	
}
