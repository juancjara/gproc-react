<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_SetEventos extends Middleware {
	
	public function execute()
	{
		$eventos = Session::instance()->get('eventos');
		
		if ( ! $eventos)
		{
			$eventos = Model_Evento::get_from_saes();
			
			Session::instance()->set('eventos', $eventos);
		}
		
		$this->controller->eventos = $eventos;
	}
	
}
