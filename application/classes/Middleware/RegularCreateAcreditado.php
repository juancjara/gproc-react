<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_RegularCreateAcreditado extends Middleware {
	
	public function execute()
	{
		$oAcreditado = ORM::factory('Acreditado', $this->controller->result['acre_id']);
		
		if ( ! $oAcreditado->loaded())
		{
			$oAcreditado = Model_Acreditado::create_acreditado($this->controller->result);
		}
		
		$this->controller->oUser = $oAcreditado->oUser;
		$this->controller->oAcreditado = $oAcreditado;
	}
	
}
