<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_RegularCanAccessEvento extends Middleware {
	
	public function execute()
	{
		$oAutoevaluacion = $this->controller->oAutoevaluacion;
		
		if ( ! $oAutoevaluacion->has_access($this->controller->eventoInfo))
		{
			throw new Middleware_Exception_RegularAutoevaluacionNoAccess;
		}
	}
	
}
