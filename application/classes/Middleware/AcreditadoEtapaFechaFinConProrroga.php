<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEtapaFechaFinConProrroga extends Middleware {
	
	public function execute()
	{
		$oAcreditadoEtapa = $this->controller->oAcreditadoEtapa;
		
		//if ( ! $oAcreditadoEtapa->acet_fecha_fin_observado OR date('Y-m-d') <= $oAcreditadoEtapa->acet_fecha_fin_observado)
		if ( ! $oAcreditadoEtapa->has_prorroga())
		{
			$this->controller->middleware('AcreditadoEtapaFechaFin');
		}
	}
	
}
