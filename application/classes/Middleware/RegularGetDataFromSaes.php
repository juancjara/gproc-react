<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_RegularGetDataFromSaes extends Middleware {
	
	public function execute()
	{
		$request = Arr::extract($this->controller->request->post(), ['username', 'password']);
		
		$result = SaesProxyV2::instance()->post('login', $request);
		//debug($result);
		
		if ( ! $result)
		{
			throw new Middleware_Exception('Error de comunicación con el SAES');
		}
		
		if ($result['status'])
		{
			unset($result['status']);
			return $this->controller->result = $result;
		}
		
		switch ($result['code'])
		{
			// From Saes/Middleware_Exception_AcreditadoLogin
			case 2:
				throw new Middleware_Exception_RegularNoAccess;
			default:
				throw new Middleware_Exception($result['message']);
		}
	}
	
}
