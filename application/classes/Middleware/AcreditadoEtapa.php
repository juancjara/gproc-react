<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEtapa extends Middleware {
	
	public function execute()
	{
		$oAcreditadoEtapa = $this->controller->oAcreditadoEvento->get_acreditado_etapa($this->controller->evet_code);
		
		if ( ! $oAcreditadoEtapa->loaded())
		{
			throw new Middleware_Exception_ModelNotFound("", [':model' => 'AcreditadoEtapa', ':id' => $this->controller->evet_code]);
		}
		
		$this->controller->oAcreditadoEtapa = $oAcreditadoEtapa;
	}
	
}
