<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEtapaFicha extends Middleware {
	
	public function execute()
	{
		$this->controller->oAcreditadoEtapaFicha = 
			$this->controller->oAcreditadoEtapa->get_ficha($this->controller->evef_code);
	}
	
}
