<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_RegularLogin extends Middleware {
	
	public function execute()
	{
		Auth::instance()->regular_login($this->controller->oUser, $this->controller->request->post('remember'));
		
		Session::instance()->set('oAcreditado', $this->controller->oAcreditado);
		Session::instance()->set('objeto', $this->controller->result);
	}
	
}
