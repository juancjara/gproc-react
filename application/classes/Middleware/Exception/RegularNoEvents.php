<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_Exception_RegularNoEvents extends Middleware_Exception {
	
	protected $message = 
		'Usted no ha participado en ningún evento convocatoria FEC
		o aún no dispone de acceso al GPROC.
		Si considera que esto es un error, 
		por favor contáctese con un administrador.';

}
