<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_Exception_Acreditado extends Middleware_Exception {
	
	protected $message = 'Acreditado no encontrado';
	
}
