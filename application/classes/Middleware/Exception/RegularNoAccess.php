<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_Exception_RegularNoAccess extends Middleware_Exception {
	
	protected $message = 'Usuario no registrado en el SAES';
	
}
