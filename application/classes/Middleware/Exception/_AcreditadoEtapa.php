<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_Exception_AcreditadoEtapa extends Middleware_Exception {
	
	protected $message = 'AcreditadoEtapa no encontrado';
	
}
