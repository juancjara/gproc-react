<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_RegularCreateAcreditadoEventos extends Middleware {
	
	public function execute()
	{
		foreach ($this->controller->result['acev_list'] as $evento)
		{
			$this->controller->oAcreditado->get_or_create_acev($evento);
		}
	}
	
}
