<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_RegularAcreditado extends Middleware {
	
	public function execute()
	{
		$oAcreditado = Session::instance()->get('oAcreditado');
		
		if ( ! $oAcreditado OR ! $oAcreditado->loaded())
		{
			throw new Middleware_Exception_ModelNotFound("", [':model' => 'Acreditado', ':id' => 'session']);
		}
		
		$this->controller->oAcreditado = $oAcreditado;
		$this->controller->userData = Session::instance()->get('objeto');	
	}
	
}
