<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEtapaPlanMejoraEvalVirtual extends Middleware {
	
	public function execute()
	{
		$oAcreditadoEvento = $this->controller->oAcreditadoEvento;
		
		if ( ! $oAcreditadoEvento->asistio_eval_virtual())
		{
			$this->controller->request->action('error');
			$this->controller->error_message[] = 
				"Usted no se encuentra habilitado para realizar su Plan de Mejora. "
				."Si considera que esto es un error sírvase ponerse en contacto con un administrador.";
		}
	}
}
