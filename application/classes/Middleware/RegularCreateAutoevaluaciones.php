<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_RegularCreateAutoevaluaciones extends Middleware {
	
	public function execute()
	{
		foreach ($this->controller->result['acev_list'] as $evento)
		{
			$oAutoevaluacion = ORM::factory('Autoevaluacion', $evento['max_auto_id']);
			
			// No existe max_auto_id
			if ( ! $oAutoevaluacion->loaded())
			{
				$oAutoevaluacion->create_from_even($evento);
			}
			// existe max_auto_id
			else
			{
				// pass
			}
		}
	}
	
}
