<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_RegularSetAcreditadoEvento extends Middleware {
	
	public function execute()
	{
		if (ACL::instance()->get_role() === 'regular')
		{
			return;
		}
		
		if ( ! $acev_id = $this->controller->request->query('acev_id'))
		{
			return;
		}
		
		$oAcreditadoEvento = $this->controller->oAcreditado
			->aAcreditadoEvento
			->where('acev_id', '=', $acev_id)
			->find();
		
		if ( ! $oAcreditadoEvento->loaded())
		{
			throw new Middleware_Exception_ModelNotFound("", [':model' => 'AcreditadoEvento', ':id' => $acev_id]);
		}
		
		Session::instance()->set('oAcreditadoEvento', $oAcreditadoEvento);
	}
	
}
