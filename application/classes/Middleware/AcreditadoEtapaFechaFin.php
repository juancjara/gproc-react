<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEtapaFechaFin extends Middleware {
	
	public function execute()
	{
		$oAcreditadoEtapa = $this->controller->oAcreditadoEtapa;
		$oEventoEtapa = $oAcreditadoEtapa->oEventoEtapa;
		
		if (date('Y-m-d') > $oEventoEtapa->evet_fecha_fin)
		{
			$this->controller->action = 'view';
		}
	}
	
}
