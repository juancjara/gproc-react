<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoSetupEtapas extends Middleware {
	
	public function execute()
	{
		$this->controller->oAcreditadoEvento->setup_etapas();
	}
	
}
