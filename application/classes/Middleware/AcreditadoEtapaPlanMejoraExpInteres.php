<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEtapaPlanMejoraExpInteres extends Middleware {
	
	public function execute()
	{
		$oAcreditadoEvento = $this->controller->oAcreditadoEvento;
		$oFichaExpInteres = $oAcreditadoEvento->get_acreditado_etapa('REI');
		
		if ($oFichaExpInteres->acet_estado == Model_AcreditadoEtapa::ESTADO_PENDIENTE)
		{
			$this->controller->request->action('error');
			$this->controller->error_message[] = 
				"Debe terminar el {$oFichaExpInteres->oEventoEtapa->evet_nombre} "
				." para poder acceder a esta etapa";
		}
	}
}
