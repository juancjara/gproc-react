<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_StaffAuth extends Middleware {
	
	public function execute()
	{
		$request = Arr::extract($this->controller->request->post(), ['username', 'password', 'remember']);
		
		if (Auth::instance()->login($request['username'], $request['password'], $request['remember']))
		{
			$this->controller->oUser = Auth::instance()->get_user();
		}
		else
		{
			throw new Middleware_Exception_StaffNoAccess;
		}
	}
	
}
