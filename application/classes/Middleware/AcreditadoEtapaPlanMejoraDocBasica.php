<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEtapaPlanMejoraDocBasica extends Middleware {
	
	public function execute()
	{
		$oAcreditadoEvento = $this->controller->oAcreditadoEvento;
		$oDocumentacionBasica = $oAcreditadoEvento->get_acreditado_etapa('DIB');
		
		if ($oDocumentacionBasica->acet_estado == Model_AcreditadoEtapa::ESTADO_PENDIENTE)
		{
			$this->controller->request->action('error');
			$this->controller->error_message[] = 
				"Debe terminar la {$oDocumentacionBasica->oEventoEtapa->evet_nombre} "
				."para poder acceder a esta etapa";
		}
	}
}
