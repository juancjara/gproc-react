<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEtapaFichaEstado extends Middleware {
	
	public function execute()
	{
		// EtapaEstado editable
		if ($this->controller->action == 'save')
		{
			if ($this->controller->oAcreditadoEtapaFicha->is_atendido())
			{
				return $this->controller->action = 'upload';
			}
		}
		
		if ( ! $this->controller->oAcreditadoEtapaFicha->is_editable())
		{
			$this->controller->action = 'view';
		}
	}
	
}
