<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_EvaluadorEvento extends Middleware {
	
	public function execute()
	{
		$even_id = $this->controller->request->query('even_id');
		
		$oEvento = ORM::factory('Evento', $even_id);
		
		if ( ! $oEvento->loaded())
		{
			throw new Middleware_Exception_ModelNotFound("", [':model' => 'Evento', ':id' => $even_id]);
		}
		
		$this->controller->oEvento = $oEvento;
		$this->controller->eventoInfo = $this->controller->eventos[$oEvento->even_id];
	}
	
}
