<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_Autoevaluacion extends Middleware {
	
	public function execute()
	{
		$oAcreditadoEvento = $this->controller->oAcreditadoEvento;
		
		try
		{
			// throws Middleware_Exception_ModelNotFound
			$oAutoevaluacion = Repository::factory('Autoevaluacion')
				->from_acev($oAcreditadoEvento->acev_id)
				->with_porcentaje()
				->get();
		}
		catch (Middleware_Exception_ModelNotFound $e)
		{
			throw new Middleware_Exception("Autoevaluacion del acev {$oAcreditadoEvento->acev_id} no ha sido cargada");
		}
		
		$this->controller->oAutoevaluacion = $oAutoevaluacion;
		$this->controller->userData['auto_porcentaje'] = $oAutoevaluacion->data['auto_porcentaje'];
	}
	
}
