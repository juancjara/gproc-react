<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEtapaPlanMejoraEstado extends Middleware {
	
	public function execute()
	{
		//$this->controller->middleware('AcreditadoEtapaPlanMejoraExpInteres');
		$this->controller->middleware('AcreditadoEtapaPlanMejoraEvalVirtual');
		//$this->controller->middleware('AcreditadoEtapaPlanMejoraDocBasica');
	}
	
}
