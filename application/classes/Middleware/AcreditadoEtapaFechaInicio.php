<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEtapaFechaInicio extends Middleware {
	
	public function execute()
	{
		$oAcreditadoEtapa = $this->controller->oAcreditadoEtapa;
		$oEventoEtapa = $oAcreditadoEtapa->oEventoEtapa;
		
		if (date('Y-m-d') < $oEventoEtapa->evet_fecha_inicio)
		{
			$this->controller->request->action('error');
			
			$this->controller->error_message[] = "Usted no puede ingresar a esta etapa.".H_EOL
				."Fecha de inicio de la etapa: {$oEventoEtapa->evet_fecha_inicio}".H_EOL
				."Fecha de fin de la etapa: {$oEventoEtapa->evet_fecha_fin}".H_EOL;
		}
	}
	
}
