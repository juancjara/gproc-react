<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoRegularOrEvaluador extends Middleware {
	
	public function execute()
	{
		// Regular
		if (ACL::instance()->check_role(Model_Role::ROLE_REGULAR))
		{
			$this->controller->middleware('RegularAcreditado');
		}
		// Evaluador
		else
		{
			$this->controller->middleware('AcreditadoEvento');
		}
	}
	
}
