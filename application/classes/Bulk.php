<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
abstract class Bulk {
	
	/**
	 * 
	 * @param  type $type
	 * @return  Bulk
	 */
	public static function factory($type)
	{
		$class = 'Bulk_'.ucfirst($type);
		
		return new $class();
	}
	
	protected $_error;
	
	protected $_data;
	
	protected $_levels;
	
	protected $_titles;
	
	protected $_type_catalogo;
	
	public function process($file)
	{
		$this->_check_file($file);
		
		$this->_get_data($file);
	}
	
	public function get_status()
	{
		return $this->_error ? $this->_error : TRUE;
	}
	
	protected function _check_file($file)
	{
		if (! Upload::valid($file) OR
			! Upload::not_empty($file) OR
			! Upload::type($file, array('csv')))
		{
			throw new Exception_Gproc('Archivo inválido.');
		}
	}
	
	protected function count_levels($titles)
	{
		$end = end($titles);
		$count = substr($end, 0,1);
		return $count;
	}
	
	protected function _get_data($file)
	{
		$filename = $file['tmp_name'];
		
		$csv = new parseCSV();
		//$csv->encoding('ISO-8859-1', 'UTF-8');
		$csv->parse($filename);
		
		$this->_data = $csvData = $csv->data;
		
		$this->_titles = $get_titles = $csv->titles;
		//echo '<html><head><meta charset="utf-8"></head><body>';
		//debug($csvData);
	}

}
