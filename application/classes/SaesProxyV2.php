<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 */
class SaesProxyV2 {
	
	/**
	 *
	 * @var  SaesProxy
	 */
	private static $instance;
	
	public static function instance($config = array())
	{
		if ( ! self::$instance)
		{
			SaesProxyV2::$instance = new SaesProxyV2();
		}
		
		return SaesProxyV2::$instance;
	}
	
	private $config = array(
		'token' => 'F5cYRLZF',
		'url' => array(
			SERVER_PROD  => 'http://saes.procalidad.gob.pe/apiV2/',
			SERVER_DEMO  => 'http://saesdemo.procalidad.gob.pe/apiV2/',
			SERVER_DEV   => 'http://saes.dev/apiV2/',
		),
	);
	
	private $token;
	private $url;
	
	public function __construct()
	{
		$this->token = $this->config['token'];
		$this->url = $this->config['url'][Kohana::$server_name];
	}

	public function get($method, $params = array())
	{
		$result = Request::factory($this->_build_url($method))
			->query($params)
			->execute()
			->body();
		
		//echo $result;die();
		//debug($result);
		return json_decode($result, TRUE);
	}
	
	public function post($method, $params = array())
	{
		//debug($params);
		//debug(Request::factory($this->_build_url($method)));
		$result = Request::factory($this->_build_url($method))
			->method('POST')
			->post($params)
			->execute()
			->body();

		//echo $result;die();
		//debug($result);
		return json_decode($result, TRUE);
	}
	
	private function _build_url($method)
	{
		return $this->url.$method.'/'.$this->token;
	}
	
}
