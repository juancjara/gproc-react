<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 */
class SaesProxy {
	
	/**
	 *
	 * @var  SaesProxy
	 */
	private static $instance;
	
	public static function instance($config = array())
	{
		if ( ! self::$instance)
		{
			SaesProxy::$instance = new SaesProxy();
		}
		
		return SaesProxy::$instance;
	}
	
	private $config = array(
		'token' => 'F5cYRLZF',
		'url' => array(
			SERVER_PROD  => 'http://saes.procalidad.gob.pe/api/',
			SERVER_DEMO  => 'http://saesdemo.procalidad.gob.pe/api/',
			SERVER_DEV   => 'http://saes.dev/api/',
		),
	);
	
	private $token;
	private $url;
	
	public function __construct()
	{
		$this->token = $this->config['token'];
		$this->url = $this->config['url'][Kohana::$server_name];
	}

	public function get($method, $params = array())
	{
		$result = Request::factory($this->_build_url($method))
			->query($params)
			->execute();
		
		//echo $result;die();
		//debug($result);
		return json_decode($result, TRUE);
	}
	
	public function post($method, $params = array())
	{
		//debug($params);
		//debug(Request::factory($this->_build_url($method)));
		$result = Request::factory($this->_build_url($method))
			->method('POST')
			->post($params)
			->execute();
		//echo $result;die();
		//debug($result);
		return json_decode($result, TRUE);
	}
	
	private function _build_url($method)
	{
		return $this->url.$method.'/'.$this->token;
	}
	
	private function _post($method, $params)
	{
		/*$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1) ;
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		
		$result = curl_exec($ch);
		
		curl_close($ch);
		
		return $result;*/
	}
	
}
