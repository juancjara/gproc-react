<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Auth_Gproc extends Auth_ORM {

	/**
	 * Checks if a session is active.
	 *
	 * @param   mixed    $role Role name string, role ORM object, or array with role names
	 * @return  boolean
	 */
	public function logged_in($role = NULL)
	{
		if ($role)
		{
			throw new Exception_Saes('Auth operation not supported anymore');
		}
		
		// Get the user from the session
		$user = $this->get_user();

		if ( ! $user)
			return FALSE;
		
		return TRUE;
	}
	
	public function is_super()
	{
		$user = $this->get_user();
		return (bool) ($user->id == 0);
	}
	
	/**
	 * Logs a user in.
	 *
	 * @param   string   $username
	 * @param   string   $password
	 * @param   boolean  $remember  enable autologin
	 * @return  boolean
	 */
	protected function _login($username, $password, $remember)
	{
		$user = ORM::factory('User');
		$user->where($user->unique_key($username), '=', $username)->find();

		if (is_string($password))
		{
			// Create a hashed password
			$password = $this->hash($password);
		}

		if ($user->estado == Model_User::ESTADO_ACTIVO AND $user->clave === $password)
		{	
			$this->remember($user, $remember);

			// Finish the login
			$this->complete_login($user);

			return TRUE;
		}
		
		return FALSE;
	}
	
	public function regular_login($user, $remember)
	{
		$this->remember($user, $remember);

		// Finish the login
		$this->complete_login($user);
		
		return TRUE;
	}
	
	private function remember($user, $remember)
	{
		if ($remember === TRUE)
		{
			// Token data
			$data = array(
				'user_id'    => $user->pk(),
				'expires'    => time() + $this->_config['lifetime'],
				'user_agent' => sha1(Request::$user_agent),
			);

			// Create a new autologin token
			$token = ORM::factory('User_Token')
						->values($data)
						->create();

			// Set the autologin cookie
			Cookie::set('authautologin', $token->token, $this->_config['lifetime']);
		}
	}
	
} // End Auth ORM
