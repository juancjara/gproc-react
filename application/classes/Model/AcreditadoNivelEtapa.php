<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AcreditadoNivelEtapa extends Model_Gproc
{
	const ESTADO_PENDIENTE = 10;
	const ESTADO_APROBADO  = 30;
	const ESTADO_OBSERVADO = 40;
	
	const ESTADO_CANCELADO = 50;
	
	public static $estados = array(
		self::ESTADO_PENDIENTE => 'Pendiente',
		self::ESTADO_APROBADO  => 'Aprobado',
		self::ESTADO_OBSERVADO => 'Observado',
		self::ESTADO_CANCELADO => 'Cancelado',
	);
	
	protected $_table_name = 'acreditado_nivel_etapa';
	
	protected $_primary_key = 'acne_id';
	
	protected $_belongs_to = array(
		'oAcreditadoEtapa' => array(
			'model' => 'AcreditadoEtapa',
			'foreign_key' => 'acet_id',
		),
		'oEventoNivel' => array(
			'model' => 'EventoNivel',
			'foreign_key' => 'evni_id',
		),
		/*'oEvaluador' => array(
			'model' => 'Evaluador',
			'foreign_key' => 'eval_id',
		),*/
	);
		
	protected $_has_many = array(
		'aAcreditadoNivelEtapaEstado' => array(
			'model' => 'AcreditadoNivelEtapaEstado',
			'foreign_key' => 'acne_id',
		),
	);
	
	public function estado()
	{
		return Arr::path(self::$estados, $this->acne_estado, '-');
	}
	
	public function acciones()
	{
		$acciones = array(
			self::ESTADO_APROBADO => 'Aprobar',
			self::ESTADO_OBSERVADO => 'Observar',
			//self::ESTADO_CANCELADO => 'Cancelar',
		);
		
		//unset($acciones[$this->acne_estado]);
		
		return $acciones;
	}
	
	public function is_evaluable()
	{
		return ($this->acne_estado != self::ESTADO_APROBADO) AND
			$this->oAcreditadoEtapa->is_evaluable();
	}
	
	public function set_estado($data)
	{
		$estado = $data['estado'];
		
		if ($this->acne_estado == $estado AND $estado != self::ESTADO_OBSERVADO)
		{
			throw new Exception_Gproc('Ya se encuentra en el estado seleccionado');
		}
		
		$oAcreditadoEtapa = $this->oAcreditadoEtapa;
		
		$oAcreditadoNivelEtapaEstado = ORM::factory('AcreditadoNivelEtapaEstado')
			->set('acne_id', $this->pk())
			->set('acne_estado', $estado)
			->set('anee_fecha', date('Y-m-d H:i:s'));
		
		$evet_code = $oAcreditadoEtapa->oEventoEtapa->evet_code;
		$evni_code = $this->oEventoNivel->evni_code;
		$identifier = "{$evet_code}-{$evni_code}-".date('YmdHis');
		
		$file_saved = $oAcreditadoNivelEtapaEstado
			->set_path($oAcreditadoEtapa->acev_id)
			->save_file('anee_path', $identifier, $_FILES['file']);
		
		if ($estado == self::ESTADO_OBSERVADO AND ! $file_saved)
		{
			throw new Exception_Gproc('No se pudo actualizar el estado');
		}
		
		$oAcreditadoNivelEtapaEstado->save();

		$this->set('acne_estado', $estado)
			->save();
		
		$oAcreditadoEtapa->update_estado($data);
	}
	
	public function class_estado()
	{
		$classes = array(
			self::ESTADO_PENDIENTE => 'default',
			self::ESTADO_APROBADO => 'success',
			self::ESTADO_OBSERVADO => 'danger',
			self::ESTADO_CANCELADO => 'default',
		);
		
		return Arr::get($classes, $this->acne_estado);
	}
	
	public function is_observado()
	{
		return $this->acne_estado == self::ESTADO_OBSERVADO;
	}
	
	public function get_documento_observacion()
	{
		return $this
			->aAcreditadoNivelEtapaEstado
			->order_by('anee_id', 'DESC')
			->find()
			->anee_path;
	}
	
}
