<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Evaluador extends Model_Gproc {
	
	protected $_table_name = 'evaluador';
	
	protected $_primary_key = 'eval_id';
	
	protected $_belongs_to = [
		'oUser' => [
			'model' => 'User',
			'foreign_key' => 'user_id',
		],
		'oEvento' => [
			'model' => 'Evento',
			'foreign_key' => 'even_id',
		],
	];
	
	protected $_has_many = array(
		/*'aAcreditadoNivelEtapa' => array(
			'model' => 'AcreditadoNivelEtapa',
			'foreign_key' => 'eval_id',
		),*/
	);	
	
	public function get_eventos()
	{
		return $this->oUser->get_eventos();
	}
	
	/**
	 * 
	 * @return  Database_Result
	 */
	public function get_acreditados()
	{
		return $this->oEvento->aAcreditadoEvento->find_all();
	}
	
	public function has_access()
	{
		//debug($this);
		return $this->user_id == 1;
	}
}