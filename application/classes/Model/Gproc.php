<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Gproc extends ORM
{
	protected $_upload = 'uploads/';
	
	public function save_file($column, $filename, $file, $params = NULL)
	{
		if ( ! $this->_check_file($file, $params))
			return FALSE;
		
		$dir = $this->_upload;
		$full_dir = APPPATH.$dir;
		
		if ( ! is_dir($full_dir))
		{
			mkdir($full_dir);
		}
		
		$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
		$filename = URL::title($filename, '-', TRUE).'.'.$ext;
		
        if ( ! Upload::save($file, $filename, $full_dir))
        {
            return FALSE;
        }
 
		$this->{$column} = $dir.$filename;
		return TRUE;
	}
	
	protected function _check_file($file, array $params = NULL)
	{
		if (! Upload::valid($file) OR
			! Upload::not_empty($file))
			return FALSE;
		
		if (Upload::type($file, array('exe', 'php')))
			return FALSE;
		
		if ($params !== NULL AND ! Upload::type($file, $params))
			return FALSE;
		
		return TRUE;
	}
	
	public function set_path($acev_id)
	{
		$this->_upload .= "{$acev_id}/";
		
		return $this;
	}
	
	//public function find_and_delete()
	public function delete_if_exists()
	{
		//$this->find();
		
		if ($this->loaded())
		{
			$this->delete();
		}
		
		return $this;
	}
	
	public function find_or_throw()
	{
		$this->find();
		
		if ( ! $this->loaded())
		{
			throw new Middleware_Exception_ModelNotFound('Model not found');
		}
		
		return $this;
	}
	
	public function find_or_create($values)
	{
		$this->find();
		
		if ( ! $this->loaded())
		{
			return $this
				->values($values)
				->create();
		}
		
		return $this;
	}
	
	public function union($table, $all = TRUE)
	{
		// Add pending database call which is executed after query type is determined
		$this->_db_pending[] = array(
			'name' => 'union',
			'args' => array($table, $all),
		);

		return $this;
	}
	
}
