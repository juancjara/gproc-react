<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Documento extends Model_Gproc
{
	
	protected $_table_name = 'documento';
	
	protected $_primary_key = 'docu_id';
	
	protected $_belongs_to = array(
		'oAcreditadoNivelEtapa' => array(
			'model' => 'AcreditadoNivelEtapa',
			'foreign_key' => 'acne_id',
		),
	);
}