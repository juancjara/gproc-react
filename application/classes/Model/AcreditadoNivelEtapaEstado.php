<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AcreditadoNivelEtapaEstado extends Model_Gproc
{
	protected $_table_name = 'acreditado_nivel_etapa_estado';
	
	protected $_primary_key = 'anee_id';
	
	protected $_belongs_to = array(
		'oAcreditadoNivelEtapa' => array(
			'model' => 'AcreditadoNivelEtapa',
			'foreign_key' => 'acne_id',
		),
		/*'oEvaluador' => array(
			'model' => 'Evaluador',
			'foreign_key' => 'eval_id',
		),*/
	);
	
	public function short_estado()
	{
		return Arr::path(Model_AcreditadoNivelEtapa::$estados, $this->acne_estado)[0];
	}
	
}
