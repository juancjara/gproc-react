<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Nodo extends Model_Gproc {
	
	public static $initial_status = [
		'status' => 'STORED',
		'hasChanged' => FALSE,
		'errors' => [],
	];
	
	protected $_table_name = 'nodo';
	
	protected $_primary_key = 'id';
	
	protected $_belongs_to = array(
		'oAcreditadoEtapaFicha' => array(
			'model' => 'AcreditadoEtapaFicha',
			'foreign_key' => 'acef_id',
		),
		'oNodo' => array(
			'model' => 'Nodo',
			'foreign_key' => 'parent_id',
		),
	);
	
	protected $_has_many = array(
		'aNodo' => array(
			'model' => 'Nodo',
			'foreign_key' => 'parent_id',
		),
	);
	
	public function decode()
	{
		return [
			'data' => json_decode($this->data, TRUE),
			'parent_id' => $this->parent_id,
			'dbId' => $this->id,
			'status' => self::$initial_status,
		];
	}
	
}
