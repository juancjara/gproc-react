<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_EventoNivel extends Model_Gproc
{
	protected $_table_name = 'evento_nivel';
	
	protected $_primary_key = 'evni_id';
	
	protected $_belongs_to = array(
		'oEvento' => array(
			'model' => 'Evento',
			'foreign_key' => 'even_id',
		),
	);
	
}