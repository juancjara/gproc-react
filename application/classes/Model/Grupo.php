<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Grupo extends Model_Gproc {
	
	protected $_table_name = 'grupo';
	
	protected $_primary_key = 'grup_id';
	
	protected $_belongs_to = array(
		'oEventoEtapa' => array(
			'model' => 'EventoEtapa',
			'foreign_key' => 'evet_id',
		),
	);
}
