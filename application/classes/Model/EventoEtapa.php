<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_EventoEtapa extends Model_Gproc
{
	const EVENTO_PLANTILLA_ETAPA_REI = 2;
	
	public static $niveles = array(
		'AUT' => [],
		'REI' => ['EDF'],
		'DIB' => ['EDF'],
		'RPM' => ['EFF', 'EVP', 'ECT', 'EDR'],
		'DIC' => ['EDF'],
	);
	
	protected $_table_name = 'evento_etapa';
	
	protected $_primary_key = 'evet_id';
	
	protected $_belongs_to = array(
		'oEvento' => array(
			'model' => 'Evento',
			'foreign_key' => 'even_id',
		),
	);
	
	protected $_has_many = array(
		'aAcreditadoEtapa' => array(
			'model' => 'AcreditadoEtapa',
			'foreign_key' => 'evet_id',
			'far_key' => 'acet_id',
		),
		'aEventoEtapaFicha' => array(
			'model' => 'EventoEtapaFicha',
			'foreign_key' => 'evet_id',
			'far_key' => 'evef_id',
		),
		'aGrupo' => array(
			'model' => 'Grupo',
			'foreign_key' => 'evet_id',
			'far_key' => 'grup_id',
		),
	);
	
	public function get_documentacion_basica()
	{
		return DB::select('docu_nombre', 'docu_id')
			->from('documentacion')
			->where('evet_id', '=', $this->evet_id)
			->execute()
			->as_array();
	}
	
	public function get_documentacion_complementaria()
	{
		return DB::select('docu_nombre', 'docu_id')
			->from('documentacion')
			->where('evet_id', '=', $this->evet_id)
			->execute()
			->as_array();
	}
	
	public function nivel_exists($oEventoNivel)
	{
		$niveles = self::$niveles;
		
		$etapa = $this->evet_code;
		
		$nivel = $oEventoNivel->evni_code;
		
		return in_array($nivel, $niveles[$etapa]);
	}
	
	public static function get_eventos_gproc()
	{
		$eventos = DB::select( 'evento.*')
			->from('evento_etapa')
			->join('evento')
				->using('even_id')
			//->where('evet_id', '=', $this->evet_id)
			->execute()
			->as_array();
		//debug($eventos);
		
		return $eventos;
	}
	
	public static function get_etapas_gproc($even_id = NULL)
	{ 
//		return DB::select('et.*')
//			->select(DB::expr("concat(evet_id, '. ', evet_nombre) etapa_title"))
//			->from(DB::expr("evento_etapa et"))
//			->join(DB::expr("evento ev"))
//				->using('even_id')
//			->where('et.even_id', '=', $even_id)
//			->order_by(DB::expr('abs(et.evet_orden)'))
//			->execute()
//			->as_array('evet_id', 'etapa_title');
		
		return DB::select('e.*')
			->select(DB::expr("concat(evet_id, '. ', evet_nombre) etapa_title"))
			->from(DB::expr("evento_etapa e"))
			->where('e.even_id', '=', $even_id)
			->order_by('evet_orden')
			->execute()
			->as_array('evet_id', 'etapa_title');
	}
	
	public static function get_plantilla_etapa()
	{
		$aEtapas = ORM::factory('eventoEtapa')->where('even_id', ' = ' , '0' )->as_array('evet_id');
		return $aEtapas;
	}
	
}
