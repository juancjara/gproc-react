<?php 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Catalogo
 *
 * @author COMPONENTE2
 */
class Model_Catalogo extends Model_Gproc{
	
	const FOLDER_TEMPLATE = "/media/adminlte/template/";
	const TEMPLATE_CATALOGO = "template_catalogo.csv";
	
	protected $_table_name = 'catalogo';

	protected $_primary_key = 'id';
	
	protected $_has_many = array(
		'aItem' => array(
			'model' => 'Catalogo',
			'foreign_key' => 'parent_id',
		),
	);
	
	//const RRHH = 'R';
	const SERVICIOS = 'S';
	const BIENES = 'B';
	const CATALOGO = 'CC';
	
	public static $tipo_catalogo = [
		//self::RRHH => 'RRHH',
		self::SERVICIOS => 'SERVICIOS',
		self::BIENES => 'BIENES',
		self::CATALOGO => 'CATALOGO',
	];
	
	protected $_belongs_to = array(
		'oParent' => array(
			'model' => 'Catalogo',
			'foreign_key' => 'parent_id',
		),
	);
	
	public function is_leaf()
	{
		return ! $this->n_children;
	}
	
	public function not_like_SP_R_B2()
	{
		return substr($this->id, 0, 2) !== 'SP' AND
			substr($this->id, 0, 1) !== 'R' AND
			substr($this->id, 0, 2) !== 'B2';
	}
	
	public function can_have_path()
	{
		return $this->is_leaf() AND $this->not_like_SP_R_B2();
	}
	
}
