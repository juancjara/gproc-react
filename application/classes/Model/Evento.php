<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Evento extends Model_Gproc {
	
	public static $current_eventos = [18, 19];
	
	const EVENTO_PLANTILLA = 0;
	
	//public static $eventos;
	
	public function labels()
	{
		return array(
			'tiin_id' => 'T. Institución',
			'even_id' => 'Evento',
			'even_url_pdf' => 'URL PDF',
			'even_descripcion' => 'Introducción',
		);
	}
	
	public static $tipo_financiamiento = array(
		'' => '-',
		1 => 'FEC',
		2 => 'Propio',
	);
	
	protected $_table_name = 'evento';
	
	protected $_primary_key = 'even_id';
	
	protected $_has_many = array(
		'aEventoFicha' => array(
			'model' => 'EventoFicha',
			'foreign_key' => 'even_id',
		),
		'aAcreditadoEvento' => array(
			'model' => 'AcreditadoEvento',
			'foreign_key' => 'even_id',
		),
		'aEventoEtapa' => array(
			'model' => 'EventoEtapa',
			'foreign_key' => 'even_id',
		),
		'aEventoNivel' => array(
			'model' => 'EventoNivel',
			'foreign_key' => 'even_id',
		),
		'aEvaluador' => array(
			'model' => 'Evaluador',
			'foreign_key' => 'even_id',
		),
	);
	
	public function get_grupos()
	{
		return DB::select()
			->from('grupo');
	}
	
	public function get_gastos()
	{
		return $this->get_grupos()
			->join('gasto_elegible')
			->using('grup_id');
	}
	
	public function get_tipos()
	{
		return $this->get_gastos()
			->join('tipo_gasto')
			->using('gael_id');
	}
	
	public function get_items()
	{
		return $this->get_tipos()
			->join('item_gasto')
			->using('tiga_id');
	}
	
	public static function get_etapas($even_id)
	{
		return DB::select('et.*')
			->select(DB::expr("concat(evet_id, '. ', evet_nombre) etapa_title"))
			->from(DB::expr("evento_etapa et"))
			->where('et.even_id', '=', $even_id)
			->order_by('et.evet_orden')
			->execute()
			->as_array('evet_id', 'etapa_title');
	}
	
	public static function get_from_saes()
	{
		$eventos = SaesProxyV2::instance()->get('getEventosFEC');
		unset($eventos['status']);
		
		return $eventos;
	}
	
	public static function get_names($field = 'even_name',  $filter = FALSE)
	{
		$eventos = Session::instance()->get('eventos');
		
		$filtered = array_filter(
			$eventos, 
			function ($evento) use ($filter)
			{
				return (! $filter OR ORM::factory('Evento', $evento['even_id'])->loaded());
			}
		);
		
		return Arr::array_to_assoc($filtered, 'even_id', $field);
	}
	
	public function create_from_saes($evento)
	{
		$this->values($evento, ['tiin_id'])
			->set('even_id', $evento['even_id'])
			->create();
		
		$this->create_evento_etapa($evento);
		$this->create_evento_nivel($evento);
	}
	
	public function create_evento_etapa($evento)
	{
		$query = "INSERT INTO evento_etapa
			(even_id,evet_nombre,evet_fecha_inicio,evet_fecha_fin,evet_path,evet_code,evet_orden)
			SELECT {$evento['even_id']}, evet_nombre,evet_fecha_inicio,evet_fecha_fin,evet_path,evet_code,evet_orden
			FROM evento_etapa WHERE even_id = 0";
		
		$result = DB::query(Database::INSERT, $query)
			->execute();
		
		$this->create_evento_etapa_ficha($result);
	}
	
	public function create_evento_etapa_ficha($result)
	{
		$delta = $result[0] - Model_EventoEtapa::EVENTO_PLANTILLA_ETAPA_REI;
		
		$query2 = "INSERT INTO evento_etapa_ficha
			(evef_code,evef_nombre,evef_path,evet_id)
			SELECT evef_code,evef_nombre,evef_path, evet_id + {$delta} +1
			FROM evento_etapa_ficha WHERE evet_id <= 5";

		$result2 = DB::query(Database::INSERT, $query2)
			->execute();
	}
	
	public function create_evento_nivel($evento)
	{
		$query3 = "INSERT INTO evento_nivel
			(evni_id,evni_code,evni_nombre,evni_fecha_inicio_evaluado,evni_fecha_fin_evaluado,even_id)
			SELECT null, evni_code,evni_nombre,evni_fecha_inicio_evaluado,evni_fecha_fin_evaluado,'{$evento['even_id']}'
			FROM evento_nivel WHERE even_id = 0";

		$result3 = DB::query(Database::INSERT, $query3)
			->execute();	
	}
	
}
