<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AcreditadoEtapaFicha extends Model_Gproc
{
	const ESTADO_PENDIENTE = 10;
	const ESTADO_ATENDIDO = 20;
	const ESTADO_APROBADO  = 30;
	const ESTADO_OBSERVADO = 40;
	
	public static $estados = [
		self::ESTADO_PENDIENTE => 'Pendiente',
		self::ESTADO_ATENDIDO => 'Atendido',
		self::ESTADO_APROBADO  => 'Aprobado',
		self::ESTADO_OBSERVADO => 'Observado',
	];
	
	const RRHH = 1;
	const SERVICIOS = 2;
	const BIENES = 3;
	
	public static $tipo_catalogo = [
		self::RRHH => 'R',
		self::SERVICIOS => 'S',
		self::BIENES => 'B',
	];
	
	const DOCUMENTO_POR_SUBIR = 1;
	const DOCUMENTO_SUBIDO = 0;
	
	protected $_table_name = 'acreditado_etapa_ficha';
	
	protected $_primary_key = 'acef_id';
	
	protected $_belongs_to = array(
		'oAcreditadoEtapa' => array(
			'model' => 'AcreditadoEtapa',
			'foreign_key' => 'acet_id',
		),
		'oEventoEtapaFicha' => array(
			'model' => 'EventoEtapaFicha',
			'foreign_key' => 'evef_id',
		),
	);
	
	protected $_has_many = array(
		'aNodo' => array(
			'model' => 'Nodo',
			'foreign_key' => 'acef_id',
		),
	);
	
	public function get_path()
	{
		return $this->acef_path ?
			'/file/'.$this->acef_path :
			"{$this->oEventoEtapaFicha->evef_path}/pdf?acev_id={$this->oAcreditadoEtapa->acev_id}";
	}
	
	public function estado()
	{
		return Arr::path(self::$estados, $this->acef_estado);
	}
	
	public function class_estado()
	{
		$classes = array(
			self::ESTADO_PENDIENTE => 'default',
			self::ESTADO_ATENDIDO => 'success',
			self::ESTADO_APROBADO => 'success',
			self::ESTADO_OBSERVADO => 'danger',
		);
		
		return Arr::get($classes, $this->acef_estado);
	}
	
	public static $finalizado = array(
		self::ESTADO_ATENDIDO,
		self::ESTADO_APROBADO,
	);
	
	public function is_finalizado()
	{
		return in_array($this->acef_estado, self::$finalizado);
	}
	
	public function is_atendido()
	{
		return $this->acef_estado == self::ESTADO_ATENDIDO;
	}
	
	public function is_pendiente()
	{
		return $this->acef_estado == self::ESTADO_PENDIENTE;
	}
	
	public static $editable = array(
		self::ESTADO_PENDIENTE,
		self::ESTADO_OBSERVADO,
	);
	
	public function is_editable()
	{
		//debug($this->acef_estado);
		return in_array($this->acef_estado, self::$editable);
	}
	
	public function show_documento()
	{
		return (bool) $this->acef_path ;
			//AND ! $this->acef_por_subir;
	}
	
	public function documento_por_subir()
	{
		return $this->acef_por_subir == self::DOCUMENTO_POR_SUBIR;
	}
	
	public function get_nodo($title, $values = [])
	{
		$values += [
			'title' => $title,
			'acef_id' => $this->acef_id,
		];
		
		return $this
			->aNodo
			->where('title', '=', $title)
			->find_or_create($values);
			//->decode();
	}
	
	public function get_nodo_by_parent($parent_id, $values = [])
	{
		$values += [
			'parent_id' => $parent_id,
			'acef_id' => $this->acef_id,
		];
		
		return $this
			->aNodo
			->where('parent_id', '=', $parent_id)
			->find_or_create($values);
	}
	
	public function close()
	{
		$this
			->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO)
			->set('acef_por_subir', Model_AcreditadoEtapaFicha::DOCUMENTO_POR_SUBIR)
			->set('acef_path', NULL)
			->save();
		
		//$this->oAcreditadoEtapa->open_related_fichas($this->evef_code);
	}
	
}
