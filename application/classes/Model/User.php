<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_User extends Model_Auth_User { //ORM {

	const ESTADO_ACTIVO		= 1;
	const ESTADO_INACTIVO   = 0;
	
	public static $estados = array(
		self::ESTADO_ACTIVO		=> 'Activo',
		self::ESTADO_INACTIVO   => 'Inactivo',
	);
	
	protected $_table_name = 'user';
	
	protected $_primary_key = 'id';
	
	protected $_has_many = array(
		'aEvento' => array(
			'model' => 'Evento',
			'foreign_key' => 'user_id',
			'through' => 'evaluador',
			'far_key' => 'even_id',
		),
	);
	
	public function get_eventos()
	{
		if ($this->role_id == Model_Role::ROLE_ADMIN)
		{
			return ORM::factory('Evento')
				->where('even_id', '<>', Model_Evento::EVENTO_PLANTILLA)
				->find_all(); 
		}
		else
		{
			return $this->aEvento->find_all();
		}
	}
	
	public function unique_key($value)
	{
		return 'usuario';
	}
	
	public function filters()
	{
		return array(
			'clave' => array(
				array(array(Auth::instance(), 'hash'))
			)
		);
	}
	
	public function rules()
	{
		return array();
	}
	
	public function update_user($values, $expected = NULL)
	{
		if (empty($values['clave']))
		{
			unset($values['clave'], $values['clave_confirm']);
		}

		// Validation for passwords
		$extra_validation = Model_User::get_password_validation($values);

		return $this->values($values, $expected)->update($extra_validation);
	}
	
	public function update(\Validation $validation = NULL)
	{
		$values = $this->original_values();
		
		if ($this->clave == '')
		{
			$this->clave = $values['clave'];
		}
		
		parent::update($validation);
	}
	
} // End User Model
