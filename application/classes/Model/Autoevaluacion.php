<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Autoevaluacion extends Model_Gproc {
	
	protected $_table_name = 'autoevaluacion';
	
	protected $_primary_key = 'auto_id';
	
	protected $_belongs_to = array(
		'oAcreditadoEvento' => array(
			'model' => 'AcreditadoEvento',
			'foreign_key' => 'acev_id',
		),
	);
	
	public function create_from_even($evento)
	{
		//debug($evento);
		// check for old auto and delete
		$oldAuto = ORM::factory('Autoevaluacion')
			->where('acev_id', '=', $evento['acev_id'])
			->find()
			->delete_if_exists();
		
		// get from SAES
		$result = SaesProxyV2::instance()
			->get('getAutoDataFromAPI', ['auto_id' => $evento['max_auto_id']]);
		
		$values = [
			'auto_resumen' => json_encode($result['auto_resumen']),
			'auto_estandares' => json_encode($result['auto_estandares']),
			'auto_fuentes' => json_encode($result['auto_fuentes']),
		];
		
		try
		{
			$this
				->values($values)
				->set('acev_id', $evento['acev_id'])
				->set('auto_id', $evento['max_auto_id'])
				->create();
		}
		catch (Exception $e)
		{
			debug($e);
		}
	}
	
	public function has_access($evento)
	{
		//debug($this->data);
		return ($this->data['auto_porcentaje'] >= $evento['even_per_minimo']);
	}
	
	public function get_data()
	{
		return json_decode($this->auto_estandares, TRUE);
	}
	
	public function get_dimensiones()
	{
		return A::f($this->get_data())
			->map(function ($row) {
				return [
					'display' => $row['dime'],
					'dime_codigo' => $row['dime_codigo'],
				];
			})
			->to_dict('dime_codigo')
			->value();
	}
	
	public function get_factores()
	{
		return A::f($this->get_data())
			->map(function ($row) {
				return [
					'display' => $row['fact'],
					'dime_codigo' => $row['dime_codigo'],
					'fact_codigo' => $row['fact_codigo'],
				];
			})
			->to_dict('fact_codigo')
			->value();
	}
	
	public function get_estandares()
	{
		return A::f($this->get_data())
			->map(function ($row) {
				return [
					'display' => $row['esta'],
					'esta_id' => $row['esta_id'],
					'tipo' => $row['tipo'],
					'fact_codigo' => $row['fact_codigo'],
				];
			})
			->value();
	}
	
	public function get_fuentes()
	{
		return A::f(json_decode($this->auto_fuentes, TRUE))
			->map(function ($row) {
				//debug($row);
				return 
					Arr::extract(
						$row,
						[
							'esta_id', 
							'esta_codigo', 
							'fact_codigo',
							'conc_id',
							'code_cumple', 
							'code_calidad'
						]
					) + [
						'display' => "{$row['esta']} - {$row['conc_codigo']}.{$row['concepto']}",
					];
			})
			->value();
	}
	
}
