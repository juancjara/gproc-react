<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AcreditadoEtapa extends Model_Gproc
{
	const ESTADO_PENDIENTE = 10;
	const ESTADO_ATENDIDO  = 20;
	const ESTADO_APROBADO  = 30;
	const ESTADO_OBSERVADO = 40;
	const ESTADO_CANCELADO = 50;
	
	public static $estados = array(
		self::ESTADO_PENDIENTE => 'Pendiente',
		self::ESTADO_ATENDIDO  => 'Atendido',
		self::ESTADO_APROBADO  => 'Aprobado',
		self::ESTADO_OBSERVADO => 'Observado',
		self::ESTADO_CANCELADO => 'Cancelado',
	);
	
	public static $classes = array(
		self::ESTADO_PENDIENTE => 'default',
		self::ESTADO_ATENDIDO => 'success',
		self::ESTADO_APROBADO => 'success',
		self::ESTADO_OBSERVADO => 'danger',
		self::ESTADO_CANCELADO => 'default',
	);
	
	protected $_table_name = 'acreditado_etapa';
	
	protected $_primary_key = 'acet_id';
	
	protected $_belongs_to = array(
		'oAcreditadoEvento' => array(
			'model' => 'AcreditadoEvento',
			'foreign_key' => 'acev_id',
		),
		'oEventoEtapa' => array(
			'model' => 'EventoEtapa',
			'foreign_key' => 'evet_id',
		),
	);
	
	protected $_has_many = array(
		'aAcreditadoNivelEtapa' => array(
			'model' => 'AcreditadoNivelEtapa',
			'foreign_key' => 'acet_id',
		),
		'aAcreditadoEtapaFicha' => array(
			'model' => 'AcreditadoEtapaFicha',
			'foreign_key' => 'acet_id',
		),
	);
	
	public function estado()
	{
		return Arr::path(self::$estados, $this->acet_estado, '-');
	}
	
	public function upload_documentacion($identifier, $file)
	{
		if ( ! $this->_check_file($file))
			return FALSE;
		
		$dir = "{$this->_upload}{$this->acev_id}/";
		$full_dir = APPPATH.$dir;
		
		if ( ! is_dir($full_dir))
		{
			mkdir($full_dir);
		}
		
		$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
		$filename = "{$this->acet_id}-{$identifier}-".date('YmdHis').".{$ext}";
		
        if ( ! Upload::save($file, $filename, $full_dir))
        {
            return FALSE;
        }
		return $dir.$filename;
	}
	
	public function get_or_create_ficha($oEventoEtapaFicha)
	{	
		if ( ! $oEventoEtapaFicha->loaded())
		{
			throw new Exception_Gproc('Model not found');
		}
		
		$params = [
			'acet_id' => $this->pk(),
			'evef_id' => $oEventoEtapaFicha->pk(),
		];
		
		$oAcreditadoEtapaFicha = ORM::factory('AcreditadoEtapaFicha', $params);
		
		if ( ! $oAcreditadoEtapaFicha->loaded())
		{
			$oAcreditadoEtapaFicha
				->values($params)
				->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_PENDIENTE)
				->create();
		}
		//debug($oAcreditadoEtapaFicha);
		return $oAcreditadoEtapaFicha;
	}
	
	/**
	 * 
	 * @param  string  $evef_code  Codigo de la ficha
	 * @return  Model_EventoEtapaFicha
	 */
	public function get_ficha($evef_code)
	{
		$oEventoEtapaFicha = $this->oEventoEtapa
			->aEventoEtapaFicha
			->where('evef_code', '=', $evef_code)
			->find();
		
		return $this->get_or_create_ficha($oEventoEtapaFicha);
	}
	
	/**
	 * 
	 * @return  Model_AcreditadoEtapa
	 */
	public function setup_fichas()
	{
		$aEventoEtapaFicha = $this->oEventoEtapa->aEventoEtapaFicha->find_all();
		
		foreach ($aEventoEtapaFicha as $oEventoEtapaFicha)
		{
			$this->get_or_create_ficha($oEventoEtapaFicha);
		}
		
		return $this;
	}
	
	public function get_fichas()
	{
		return $this->aAcreditadoEtapaFicha
			->with('oEventoEtapaFicha')
			->order_by('evef_id')
			->find_all();
	}
	
	public function all_fichas_atendidas()
	{
		return ($this->aAcreditadoEtapaFicha->count_all() == 
			$this->aAcreditadoEtapaFicha
				->where('acef_estado', '=', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO)
				->count_all());
	}
	
	/**
	 * 
	 * @param  Model_EventoNivel $oEventoNivel
	 * @return  Model_AcreditadoNivelEtapa
	 */
	public function get_or_create_acreditado_nivel_etapa($oEventoNivel)
	{
		/*if ( ! $this->nivel_accesible($oEventoNivel))
		{
			return FALSE;
		}*/
		
		$params = [
			'acet_id' => $this->pk(),
			'evni_id' => $oEventoNivel->pk(),
		];
		
		$oAcreditadoNivelEtapa = ORM::factory('AcreditadoNivelEtapa', $params);
		
		if (/*$this->oEventoEtapa->evet_code != 'AUT' AND*/ ! $oAcreditadoNivelEtapa->loaded())
		{
			$oAcreditadoNivelEtapa
				->values($params)
				->set('acne_estado', Model_AcreditadoNivelEtapa::ESTADO_PENDIENTE)
				->create();
		}
		
		return $oAcreditadoNivelEtapa;
	}
	
	public function acciones()
	{
		$acciones = array(
			self::ESTADO_APROBADO  => 'Aprobar',
			self::ESTADO_OBSERVADO => 'Observar',
			self::ESTADO_CANCELADO => 'Cancelar',
		);
		
		unset($acciones[$this->acet_estado]);
		
		return $acciones;
	}
	
	public static $editable = array(
		self::ESTADO_PENDIENTE,
		self::ESTADO_OBSERVADO,
	);
	
	public function is_editable()
	{
		return in_array($this->acet_estado, self::$editable);
	}
	
	public static $evaluable = array(
		self::ESTADO_ATENDIDO,
		//self::ESTADO_OBSERVADO,
	);
	
	public function is_evaluable()
	{
		return in_array($this->acet_estado, self::$evaluable);
	}
	
	public function is_pendiente()
	{
		return $this->acet_estado == self::ESTADO_PENDIENTE;
	}
	
	public function is_observado()
	{
		return $this->acet_estado == self::ESTADO_OBSERVADO;
	}
	
	public static $finalizado = array(
		self::ESTADO_ATENDIDO,
		self::ESTADO_APROBADO,
	);
	
	public function is_finalizado()
	{
		return in_array($this->acet_estado, self::$finalizado);
	}
	
	public function update_estado($data)
	{
		list($estado, $fichas) = array_values($data);
		
		if ($estado == self::ESTADO_APROBADO)
		{
			$this->setAprobado($data);
			
			$fichas = TRUE;
		}
		elseif ($estado == self::ESTADO_OBSERVADO)
		{
			$this->setObservado($data);
			
			$fichas = $this->get_fichas_to_open($fichas);
		}
		else
		{
			die('Not supported');
		}
		
		$this->update_fichas_estado($estado, $fichas);
	}
	
	public function setAprobado($data)
	{
		$any_no_aprobados = $this->aAcreditadoNivelEtapa
			->where('acne_estado', '<>', self::ESTADO_APROBADO)
			->count_all();
		
		// RPM && set APROBADO && all_niveles_aprobados
		if ($this->oEventoEtapa->evet_code == 'RPM' AND
			$any_no_aprobados)
		{
			return;
		}
		
		$this->set('acet_estado', self::ESTADO_APROBADO)
			->save();
	}
	
	public function setObservado($data)
	{
		$plazo_dias = +$data['plazo_dias_observado'];
		
		$this
			->set('acet_fecha_fin_observado', date('Y-m-d', strtotime('+'.(+$plazo_dias).' day' , strtotime ( date('Y-m-d') ))))
			->set('acet_estado', self::ESTADO_OBSERVADO)
			->save();
	}
	
	public $should_open = [
		'MPL' => ['MPR', 'MCR', 'MME', 'MEE'],
		'MPR' => ['MCR'],
		'MCR' => [],
		'MME' => [],
		'MEE' => [],
	];
	
	public function get_fichas_to_open($fichas)
	{
		$fichas = $fichas ?: [];
		
		if ($this->oEventoEtapa->evet_code != 'RPM')
		{
			return TRUE;
		}
		
		return A::f($fichas)
			->map(function ($ficha) {
				return array_merge($this->should_open[$ficha], $ficha);
			})
			->union()
			->value();
	}
	
	public function update_fichas_estado($estado, $fichas)
	{
		foreach ($this->aAcreditadoEtapaFicha->find_all() as $oAcreditadoEtapaFicha)
		{
			$evef_code = $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_code;
			
			// Todas las fichas, o fichas en la lista
			if ($fichas === TRUE OR
				in_array($evef_code, $fichas))
			{
				$oAcreditadoEtapaFicha
					->set('acef_estado', $estado)
					->save();
			}
		}
	}
	
	public function open_related_fichas($evef_code)
	{
		$codes = $this->should_open[$evef_code];
		
		foreach ($codes as $code)
		{
			$this->get_ficha($code)
				->set('acef_estado', $this->acet_estado)
				->save();
		}
	}
	
	public function get_documento_observacion()
	{
		return $this->aAcreditadoNivelEtapa->find()
			->aAcreditadoNivelEtapaEstado
			->order_by('anee_id', 'DESC')
			->find()
			->anee_path;
	}
	
	public function class_estado()
	{
		return Arr::get(self::$classes, $this->acet_estado);
	}
	
	/**
	 * Se establece acef_por_subir a DOCUMENTO_POR_SUBIR
	 * @param type $codes
	 */
	public function set_por_subir($codes)
	{
		if ( ! is_array($codes))
		{
			$codes = [$codes];
		}
		
		foreach ($codes as $code)
		{
			$this->get_ficha($code)
				->set('acef_por_subir', Model_AcreditadoEtapaFicha::DOCUMENTO_POR_SUBIR)
				->set('acef_path', NULL)
				->save();
		}
	}
	

	/**
	 * Verifica si hay documentos por subir en plan de mejora
	 * @return  boolean
	 */

	//VERIFICA LOS DOCUMENTOS SUBIDOS EN PLAN DE MEJORA

	public function all_documentos_subidos()
	{
		return ! $this->aAcreditadoEtapaFicha
			->where('acef_path', 'IS', NULL)
			->count_all();
	}
	
	public function has_prorroga()
	{
		return $this->acet_fecha_fin_observado AND 
			date('Y-m-d') <= $this->acet_fecha_fin_observado;
	}
	
}
