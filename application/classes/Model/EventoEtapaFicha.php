<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_EventoEtapaFicha extends Model_Gproc
{
	protected $_table_name = 'evento_etapa_ficha';
	
	protected $_primary_key = 'evef_id';
	
	protected $_belongs_to = array(
		'oEventoEtapa' => array(
			'model' => 'EventoEtapa',
			'foreign_key' => 'evet_id',
		),
	);
	
	protected $_has_many = array(
		'aAcreditadoEtapaFicha' => array(
			'model' => 'AcreditadoEtapaFicha',
			'foreign_key' => 'evet_id',
			'far_key' => 'acef_id',
		),
	);
}
