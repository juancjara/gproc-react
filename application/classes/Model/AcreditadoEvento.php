<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AcreditadoEvento extends Model_Gproc
{
	protected $_table_name = 'acreditado_evento';

	protected $_primary_key = 'acev_id';

	protected $_belongs_to = array(
		'oAcreditado' => array(
			'model' => 'Acreditado',
			'foreign_key' => 'acre_id',
		),
		'oEvento' => array(
			'model' => 'Evento',
			'foreign_key' => 'even_id',
		),
	);
	
	protected $_has_one = array(
		'oFinanciamiento' => array(
			'model' => 'Financiamiento',
			'foreign_key' => 'fina_id',
		),
	);
	
	protected $_has_many = array(
		'aAcreditadoEtapa' => array(
			'model' => 'AcreditadoEtapa',
			'foreign_key' => 'acev_id',
		),
		/*'aEventoEtapa' => array(
			'model' => 'EventoEtapa',
			'foreign_key' => 'acev_id',
			'through' => 'acreditado_etapa',
			'far_key' => 'evet_id',
		),*/
	);
	
	public function get_or_create_etapa($oEventoEtapa)
	{
		if ( ! $oEventoEtapa->loaded())
		{
			throw new Exception_Gproc('Model not found');
		}
		
		$params = [
			'acev_id' => $this->pk(),
			'evet_id' => $oEventoEtapa->evet_id,
		];
		
		$oAcreditadoEtapa = ORM::factory('AcreditadoEtapa', $params);
		
		if ( ! $oAcreditadoEtapa->loaded())
		{
			$oAcreditadoEtapa
				->values($params)
				->set('acet_estado', ($oEventoEtapa->evet_code == 'AUT') ? 
					Model_AcreditadoEtapa::ESTADO_ATENDIDO : 
					Model_AcreditadoEtapa::ESTADO_PENDIENTE)
				->create();
		}
		
		return $oAcreditadoEtapa;
	}
	
	/**
	 * 
	 * @param  styring  $evet_code
	 * @return  Model_AcreditadoEtapa
	 */
	public function get_acreditado_etapa($evet_code)
	{
		return ORM::factory('AcreditadoEtapa')
			->with('oEventoEtapa')
			->where('evet_code', '=', $evet_code)
			->where('acev_id', '=', $this->pk())
			->find();
	}
	
	public function setup_etapas()
	{
		foreach ($this->oEvento->aEventoEtapa->find_all() as $oEventoEtapa)
		{
			$this->get_or_create_etapa($oEventoEtapa);
		}
		
		return $this;
	}
	
	public function asistio_eval_virtual()
	{
		return (bool) $this->acev_eval_virtual;
	}
	
	public function autoevaluacion()
	{
		return $this->aAutoevaluacion->find();
	}
	
	/*public function has_access($evento)
	{
		//debug($this->data);
		//return ($evento['auto_porcentaje'] >= $evento['even_per_minimo']);
		return FALSE;
	}*/
	
}
