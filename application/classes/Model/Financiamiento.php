<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Financiamiento extends Model_Gproc
{
	protected $_table_name = 'financiamiento';
	
	protected $_primary_key = 'fina_id' ;
	
	protected $_has_one = array(
		'oAcreditadoEvento' => array(
			'model' => 'AcreditadoEvento',
			'foreign_key' => 'acev_id',
		),
	);
	
}