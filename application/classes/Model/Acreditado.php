<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Acreditado extends Model_Gproc
{
	const TIPO_UNIVERSIDAD = 1;
	const TIPO_INSTITUTO = 3;
	
	public static $tipos = array(
		self::TIPO_UNIVERSIDAD => 'Universidad',
		self::TIPO_INSTITUTO => 'Instituto',
	);
	
	protected $_table_name = 'acreditado';

	protected $_primary_key = 'acre_id';
	
	protected $_belongs_to = array(
		'oUser' => array(
			'model' => 'User',
			'foreign_key' => 'user_id',
		),
	);
	
	protected $_has_many = array(
		'aEvento' => array(
			'model' => 'Evento',
			'foreign_key' => 'acre_id',
			'through' => 'acreditado_evento',
			'far_key' => 'even_id',
		),
		'aAcreditadoEvento' => array(
			'model' => 'AcreditadoEvento',
			'foreign_key' => 'acre_id',
		),
	);
	
	public static function create_acreditado($objeto)
	{
		$oUser = ORM::factory('User')
			->set('role_id', Model_Role::ROLE_REGULAR)
			->create();
		
		$oAcreditado = ORM::factory('Acreditado')
			->set('acre_id', $objeto['acre_id'])
			->set('user_id', $oUser->id)
			->set('inst_id', $objeto['inst_id'])
			->create();
		
		return $oAcreditado;
	}
	
	public function get_or_create_acev($evento)
	{
		$oAcreditadoEvento = $this->aAcreditadoEvento
			->where('even_id', '=', $evento['even_id'])
			->find();
			
		if ( ! $oAcreditadoEvento->loaded())
		{
			$oAcreditadoEvento
				->values($evento, ['acev_id', 'even_id'])
				->set('acre_id', $this->acre_id)
				->create();
		}
		
		return $oAcreditadoEvento;
	}
	
	public function get_user_data()
	{
		$acre_id = $this->acre_id;
		
		$objetos = Session::instance()->get('objetos', []);
		$objeto = Arr::get($objetos, $acre_id);
		
		if ( ! $objeto)
		{
			$objetos[$acre_id] = SaesProxyV2::instance()->get('getUserData', ['acre_id' => $acre_id]);
			//debug($objetos[$acre_id]);
			
			Session::instance()->set('objetos', $objetos);
		}
		
		return $objetos[$acre_id];
	}
	
}
