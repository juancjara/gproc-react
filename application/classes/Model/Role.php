<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Role extends Model_Auth_Role {
	
	const ROLE_REGULAR		= 1;
	const ROLE_EVALUADOR	= 2;
	const ROLE_ADMIN		= 3;
	const ROLE_SUPERVISOR	= 4;
	
	public static $roles = array(
		self::ROLE_REGULAR		=> 'Regular',
		self::ROLE_EVALUADOR	=> 'Evaluador',
		self::ROLE_ADMIN		=> 'Administrador',
		self::ROLE_SUPERVISOR	=> 'Supervisor',
	);
	
	/*public static $roles = array(
		self::ROLE_REGULAR		=> 'regular',
		self::ROLE_EVALUADOR	=> 'evaluador',
		self::ROLE_ADMIN		=> 'admin',
		self::ROLE_SUPERVISOR	=> 'supervisor',
	);*/
	
}

