<?php defined('SYSPATH') OR die('No direct script access.');

abstract class Database_Result extends Kohana_Database_Result {
	
	public function as_json_array($column = NULL)
	{
		return array_map(function ($model) { return $model->as_array(); }, $this->as_array($column));
	}
	
	/*public function as_object_array($column)
	{
		return array_map(function ($model) use ($column) { return $model->as_array(); }, $this->as_array($column));
	}*/
}
