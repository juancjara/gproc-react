<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Repository_AcreditadoEvento extends Repository {
	
	public function from_even($even_id)
	{
		$this->value
			->where('even_id', '=', $even_id);
		
		return $this;
	}
	
	public function filter_with_etapas()
	{
		$this->value
			->join('acreditado_etapa')->using('acev_id')
			->group_by('acev_id');
		
		return $this;
	}
	
	public function filter_from($comites)
	{
		$aAcreditadoEvento = $this->value
			->find_all()
			->as_array();
		
		return array_filter($aAcreditadoEvento, function ($oAcreditadoEvento) use ($comites)
			{
				return isset($comites[$oAcreditadoEvento->acev_id]);
			});
	}
	
	public function from_list($acev_list)
	{
		$this->value
			->where('acev_id', 'in', array_keys($acev_list));
		
		return $this;
	}
	
	/*public function from_acev_data($acev)
	{
		$oAcreditadoEvento = $this->value
			//->select(DB::expr("'' data"))
			->where('acev_id', '=', $acev['acev_id'])
			->find_or_throw();
		
		$this->value->clear()->where('even_id', '=', $oAcreditadoEvento->even_id);
		
		$oAcreditadoEvento->data = $acev;
		
		return $oAcreditadoEvento;
	}*/
	
}
