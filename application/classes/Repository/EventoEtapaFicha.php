<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Repository_EventoEtapaFicha extends Repository {
	
	public function from_even($even_id)
	{
		$this->value
			->with('oEventoEtapa')
			->where('even_id', '=', $even_id)
			->order_by('evet_orden');
		
		return $this;
	}
	
	public function from_code($evet_code)
	{
		$this->value
			->where('evet_code', '=', $evet_code);
		
		return $this;
	}
	
}
