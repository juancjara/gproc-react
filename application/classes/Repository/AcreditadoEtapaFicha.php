<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Repository_AcreditadoEtapaFicha extends Repository {
	
	public function from_even($even_id)
	{
		$this->value
			->with('oAcreditadoEtapa:oEventoEtapa')
			->with('oEventoEtapaFicha')
			->select('evef_code')
			->select('acev_id')
			->where('even_id', '=', $even_id)
			->order_by('acev_id');
		
		return $this;
	}
	
	public function from_code($evet_code)
	{
		$this->value
			->where('evet_code', '=', $evet_code);
		
		return $this;
	}
	
	public function from($oAcreditadoEvento)
	{
		$this->value
			->select('evet_code')
			->select(DB::expr('coalesce(evef_code, oEventoEtapaFicha.evef_id) evef_code'))
			->with('oEventoEtapaFicha:oEventoEtapa')
			->join('acreditado_etapa')
			->using('acet_id')
			->where('acev_id', '=', $oAcreditadoEvento->acev_id)
			->order_by('acev_id')
			->order_by('evef_id');
		
		return $this;
	}
	
	public function order_by_evet_evef()
	{
		$aAcreditadoEtapaFicha = $this->value
			->find_all()
			->as_array();
		
		return array_reduce($aAcreditadoEtapaFicha, function ($carry, $oAcreditadoEtapaFicha) 
			{
				$evet_code = $oAcreditadoEtapaFicha->evet_code;
				$evef_code = $oAcreditadoEtapaFicha->evef_code;
				
				Arr::set_path($carry, "{$evet_code}.{$evef_code}", $oAcreditadoEtapaFicha);
				
				return $carry;
			}
			, []);
	}
	
	//public function order_for($aAcreditadoEvento)
	public function order_by_acev_evef()
	{
		$aAcreditadoEtapaFicha = $this->value
			->find_all()
			->as_array();
		
		return array_reduce($aAcreditadoEtapaFicha, function ($carry, $oAcreditadoEtapaFicha)
			{
				$acev_id = $oAcreditadoEtapaFicha->acev_id;
				$evef_code = $oAcreditadoEtapaFicha->evef_code;
				
				Arr::set_path($carry, "{$acev_id}.{$evef_code}", $oAcreditadoEtapaFicha);
				
				return $carry;
			}
			, []);
	}
	
}
