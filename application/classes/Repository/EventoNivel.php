<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Repository_EventoNivel extends Repository {
	
	public function from($even_id)
	{
		$this->value
			->where('even_id', '=', $even_id);
		
		return $this;
	}
	
}
