<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Repository_Catalogo extends Repository {
	
	public function level_1()
	{
		$this->value
			->where('parent_id', 'is', NULL)
			->order_by(DB::expr("
				case id
				when 'R' then 1
				when 'S' then 2
				when 'B' then 3
				end
			"));
		
		return $this;
	}
	
	public function level_2()
	{
		$this->value
			->union(DB::select(DB::expr("'RP' id, 'Pasantías' name, 'Pasantías' short_name, 'R' parent_id, null data, null path")))
			->union(DB::select(DB::expr("'RC' id, 'Capacitación' name, 'Capacitación' short_name, 'R' parent_id, null data, null path")))
			->join(['catalogo', 'p'])->on('catalogo.parent_id', '=', 'p.id')
			->where('p.parent_id', 'is', NULL)
			->where('catalogo.id', 'not in', ['RV', 'RPA', 'RPT', 'RPL']);
		
		return $this;
	}
	
	public function get_catalogo_presupuesto($tiin_id)
	{
		//debug($tiin_id);
		$showing = $tiin_id == 1 ? 'U' : 'I';
		$not_showing = $tiin_id == 1 ? 'I' : 'U';
		
		$query = "if(c.id = 'SC{$showing}', 'SC', c.id) id
			, c.name
			, c.short_name
			, c.parent_id
			, c.data
			#, if(c.id = 'R', 'RP,RC', group_concat(cd.id)) children 
			, case c.id
				when 'R' then 'RP,RC,SP'
				when 'SP' then 'RPA,RPT,SPL'
				else group_concat(cd.id)
				end 
				children
			, c.path
			from catalogo as c
			LEFT JOIN  catalogo as cd ON c.id = cd.parent_id 
			WHERE c.id <> 'RV'
			AND c.id not like 'SC{$not_showing}%'
			AND c.id <> 'SC'
			GROUP BY c.id
			UNION
			select 'RP','Pasantías','Pasantías','R',null,'RPT,RPA',null
			UNION
			select 'RC','Capacitación','Capacitación','R',null,'RPT,RPA,RPL',null
			ORDER BY case id
				when 'R' then 1
				when 'S' then 2
				when 'B' then 3
				end
			;";
		
		return DB::select(DB::expr($query))
			->execute()
			->as_array();
	}
	
	public function get_viaticos()
	{
		$query = "c.*
			, group_concat(cd.id) children 
			from catalogo as c
			LEFT JOIN  catalogo as cd ON c.id = cd.parent_id 
			WHERE c.id like 'RV%'
			GROUP BY c.id";
		
		return DB::select(DB::expr($query))
			->execute()
			->as_array();
	}
	
	public function get_query()
	{
		$this->value = DB::select(DB::expr('SQL_CALC_FOUND_ROWS c.*'))
			->from(DB::expr('catalogo c'))
			->as_object('Model_Catalogo');
		
		return $this;
	}
	
	public function with_children()
	{
		$this->value
			->join(DB::expr('catalogo p'), 'left')->on('c.id', '=', 'p.parent_id')
			->select(DB::expr('group_concat(p.id) children'))
			->select(DB::expr('count(p.id) n_children'))
			->group_by('c.id');
		
		return $this;
	}
	
}
