<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Repository_AcreditadoEtapa extends Repository {
	
	public function from_even($even_id)
	{
		$this->value
			->with('oEventoEtapa')
			->select('evet_code')
			->where('even_id', '=', $even_id)
			->order_by('acev_id')
			->order_by('evet_orden');
		
		return $this;
	}
	
	public function from_code($evet_code)
	{
		$this->value
			->where('evet_code', '=', $evet_code);
		
		return $this;
	}
	
	public function from($oAcreditadoEvento)
	{
		$this->value
			->with('oEventoEtapa')
			->select('evet_code')
			->where('acev_id', '=', $oAcreditadoEvento->acev_id)
			->order_by('evet_orden');
		
		return $this;
	}
	
	//public function order_for($aAcreditadoEvento)
	public function order_by_acev_evet()
	{
		$aAcreditadoEtapa = $this->value
			->find_all()
			->as_array();
		
		return array_reduce($aAcreditadoEtapa, function ($carry, $oAcreditadoEtapa) 
			{
				$acev_id = $oAcreditadoEtapa->acev_id;
				$evet_code = $oAcreditadoEtapa->evet_code;
				
				Arr::set_path($carry, "{$acev_id}.{$evet_code}", $oAcreditadoEtapa);
				
				return $carry;
			}
		, []);
	}
	
	public function get_totales(/*$even_id*/)
	{
		$this->value
			->select(DB::expr('count(acev_id) count_all'))
			->select(DB::expr('sum(if(acet_estado = 10, 1, 0)) count_10'))
			->select(DB::expr('sum(if(acet_estado = 20, 1, 0)) count_20'))
			->select(DB::expr('sum(if(acet_estado = 30, 1, 0)) count_30'))
			->select(DB::expr('sum(if(acet_estado = 40, 1, 0)) count_40'))
			->select(DB::expr('sum(if(acet_estado = 50, 1, 0)) count_50'))
			->group_by('evet_id');
		
		return $this->value
			->find_all()
			->as_json_array('evet_code');
	}
}
