<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Repository_Autoevaluacion extends Repository {
	
	public function from_list($eventos)
	{
		$this->value
			->where('acev_id', 'in', array_keys($eventos));
		
		return $this;
	}
	
	public function from_acev($acev_id)
	{
		$this->value
			->where('acev_id', '=', $acev_id);
		
		return $this;
	}
	
	public function with_porcentaje()
	{
		$this->value
			->select(DB::expr("'' data"));
		
		return $this;
	}
	
	public function get()
	{
		$oAutoevaluacion = $this->value->find_or_throw();
		
		$oAutoevaluacion->data = json_decode($oAutoevaluacion->auto_resumen, TRUE);
		
		return $oAutoevaluacion;
	}
	
	public function get_all()
	{
		$aAutoevaluacion = $this->value
			->find_all()
			->as_array('acev_id');
		
		foreach ($aAutoevaluacion as $oAutoevaluacion)
		{
			$oAutoevaluacion->data = json_decode($oAutoevaluacion->auto_resumen, TRUE);
		}
		
		return $aAutoevaluacion;
	}
	
}
