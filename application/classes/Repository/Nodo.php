<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Repository_Nodo extends Repository {
	
	public function from($oAcreditadoEtapaFicha)
	{
		$this->value
			->where('nodo.acef_id', '=', $oAcreditadoEtapaFicha->acef_id);
		
		return $this;
	}
	
	public function get_all()
	{
		return $this->value
			->order_by('nodo.title')
			->find_all()
			->as_array('title');
	}
	
	public function decode()
	{
		$this->value = A::f($this->value->find_all()->as_array())
			->map(function ($oNodo) {
				return $oNodo->decode();
			})
			->value();
		
		return $this;
	}
	
	public function get_institucional($controller)
	{
		$inst_id = $controller->oAcreditado->inst_id;
		$even_id = $controller->oAcreditadoEvento->even_id;
		$acev_id = $controller->oAcreditadoEvento->acev_id;
		
		return $this->value
			->with('oAcreditadoEtapaFicha:oAcreditadoEtapa:oAcreditadoEvento:oAcreditado')
			->with('oAcreditadoEtapaFicha:oEventoEtapaFicha')
			->where('title', '=', 'ins')
			->where('evef_code', '=', $controller->evef_code)
			->where('acef_estado', 'in', Model_AcreditadoEtapaFicha::$finalizado)
			->where('oAcreditadoEtapaFicha:oAcreditadoEtapa:oAcreditadoEvento.even_id', '=', $even_id)
			->where('oAcreditadoEtapaFicha:oAcreditadoEtapa:oAcreditadoEvento.acev_id', '<>', $acev_id)
			->where('inst_id', '=', $inst_id)
			->find_all()
			->current();
		
		//profile();
		//debug($aNodo);
		
		// First
		//$oNodo = $aNodo->current();
		//debug($oNodo);
		//return $oNodo;
	}
	
	public function get($title)
	{
		return $this->value
			->where('nodo.title', '=', $title)
			->find();
	}
	
	public function get_dimensiones()
	{
		$this->value
			->where('nodo.title', 'like', 'dim%');
		
		return $this;
	}
	
	public function get_factores()
	{
		$this->value
			->where('nodo.title', 'like', 'fac%');
		
		return $this;
	}
	
	public function get_actividades()
	{
		$this->value
			->where('nodo.title', 'like', 'act%')
			->order_by('nodo.parent_id')
			->order_by('nodo.orden');
		
		return $this;
	}
	
	public function get_subactividades()
	{
		$this->value
			->where('nodo.title', 'like', 'sub%')
			->order_by('nodo.parent_id')
			->order_by('nodo.orden');
		
		return $this;
	}
	
	public function filter_fac()
	{
		$this->value
			->where(DB::expr("coalesce(nodo.data, '')"), 'not like', '%"hide":true%');
		
		return $this;
	}
	
	public function filter_act()
	{
		$this->value
			->join(DB::expr('nodo fac'))->on('nodo.parent_id', '=', 'fac.id')
			->where(DB::expr("coalesce(fac.data, '')"), 'not like', '%"hide":true%');
		
		return $this;
	}
	
	public function filter_sub()
	{
		$this->value
			->join(DB::expr('nodo act'))->on('nodo.parent_id', '=', 'act.id')
			->join(DB::expr('nodo fac'))->on('act.parent_id', '=', 'fac.id')
			->where(DB::expr("coalesce(fac.data, '')"), 'not like', '%"hide":true%');
		
		return $this;
	}
	
	public function get_act_totales($sub)
	{
		$act = $this->value->find_all();
		
		$sub_grouped = A::f($sub)
			->map(function ($oNodo) { 
				return $oNodo->decode();
			})
			->group_by(function ($nodo) { 
				return $nodo['parent_id']; 
			})
			->value();
		//debug2($sub_grouped);
		
		return A::f($act->as_array())
			->map(function ($oNodo) {
				return $oNodo->decode();
			})
			->map(function ($nodo) use ($sub_grouped) {
				$subactividades = Arr::get($sub_grouped, $nodo['dbId'], []);
				
				$nodo['detalleTotales'] = $this->get_totales($subactividades);
				
				return $nodo;
			})
			->value();
	}
	
	private $totales = [
		'RP' => '0.00',
		'RC' => '0.00',
		'SC' => '0.00',
		'SP' => '0.00',
		'B1' => '0.00',
		'B2' => '0.00',
		'R' => '0.00',
		'S' => '0.00',
		'B' => '0.00',
		'T' => '0.00',
	];
	
	private $financiamiento = [
		'fec',
		'propio',
		'plan',
	];
	
	public function get_struct_totales()
	{
		$totales = $this->totales;
		
		$data = [
			'fec' => $totales,
			'propio' => $totales,
			'plan' => $totales,
			'row_per_fec' => [],
		];
		
		return $data;
	}
	
	private function parse_nodo($nodo)
	{
		return [
			'nivel1' => Arr::path($nodo, 'data.niveles.0'),
			'nivel2' => Arr::path($nodo, 'data.niveles.1'),
			'financiamiento' => Arr::path($nodo, 'data.financiamiento'),
			'total' => Arr::path($nodo, 'data.total'),
			'parent_id' => $nodo['parent_id'],
		];
	}
	
	public function get_plan_totales($subactividades)
	{
		$subactividades = A::f($subactividades)
			->map(function ($oNodo) {
				return $oNodo->decode();
			})
			->value();
		
		$data = $this->get_totales($subactividades);
		
		foreach ($this->totales as $key => $val)
		{
			$data['row_per_fec'][$key] = 
				numberformat(
					$data['fec'][$key] / 
					(convert_numeric($data['fec']['T']) ?: 1) * 
					100, 2
				);
		}
		
		foreach ($this->financiamiento as $fin)
		{
			$data['per_'.$fin] =
				numberformat(
					$data[$fin]['T'] / 
					(convert_numeric($data['plan']['T']) ?: 1) *
					100, 2
				);
		}
		
		return $data;
	}
	
	private function get_totales($subactividades)
	{
		$totales = $this->get_struct_totales();
		
		$subactividades = A::f($subactividades)
			->map(function ($nodo) {
				return $this->parse_nodo($nodo);
			})
			->value();
		
		foreach ($subactividades as $sub)
		{
			extract($sub);

			if ($nivel1 AND $nivel2 AND $financiamiento)
			{
				$totales[$financiamiento][$nivel1] += $total;
				$totales['plan'][$nivel1] += $total;
				
				$totales[$financiamiento][$nivel2] += $total;
				$totales['plan'][$nivel2] += $total;
				
				$totales[$financiamiento]['T'] += $total;
				$totales['plan']['T'] += $total;
			}
		}
		
		foreach (array_keys($this->totales) as $tot)
		{
			foreach ($this->financiamiento as $fin)
			{
				$totales[$fin][$tot] = numberformat($totales[$fin][$tot], 2);
			}
		}
		
		return $totales;
	}
	
	public function get_indicadores()
	{
		$this->value
			->where('nodo.title', 'like', 'ind%');

		return $this;
	}
	
}
