<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Repository_EventoEtapa extends Repository {
	
	public function from_even($even_id)
	{
		$this->value
			->where('even_id', '=', $even_id)
			->order_by('evet_orden');
		
		return $this;
	}
	
}
