<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Totales extends Report_Planes {
	
	protected $title = 'Totales por grupos';
	
	protected function set_result()
	{
		$aAcreditadoEtapaFicha = $this->data;//get_fichas();
		//echo Debug::dump($aAcreditadoEtapaFicha, 5000);
		//die();
		//debug($aAcreditadoEtapaFicha);
		
		foreach ($this->comites as $key => $data)
		{
			$acev_id = $data['acev_id'];
			
			$oAcreditadoEtapaFicha = Arr::get($aAcreditadoEtapaFicha, $acev_id);
			
			//if (in_array($acev_id, array_keys($aAcreditadoEtapa)))
			if ($oAcreditadoEtapaFicha)
			{
				$data = json_decode($oAcreditadoEtapaFicha->acef_data, TRUE);
				//debug($data);
				
				$per = $data['totales']['fec'][3] / $data['totales']['fec']['total'];
				
				if ($per > 0.6)
				{
					unset($this->comites[$key]);
					continue;
				}
				
				$this->comites[$key]['totales'] = $data['totales'];
			}
			else
			{
				unset($this->comites[$key]);
			}
		}
		
		$this->result = $this->comites;
	}
	
	private function get_grupos()
	{
		$grupos = ORM::factory('Evento', $this->id)
			->get_grupos()
			->execute()
			->as_array('grup_id');
		
		$grupos['super'] = ['grup_short' => 'Supervisión'];
		$grupos['total'] = ['grup_short' => 'Total'];
		//debug($grupos);
		
		return $grupos;
	}
	
	public function get_context()
	{
		return parent::get_context() + [
			'grupos' => $this->get_grupos(),
		];
	}
}
