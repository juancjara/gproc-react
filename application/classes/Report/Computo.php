<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Computo extends Report_Planes {
	
	protected $title = 'Hardware y Software';
	
	protected function set_result()
	{
		$aAcreditadoEtapaFicha = $this->data;//get_fichas();
		//echo Debug::dump($aAcreditadoEtapaFicha, 5000);
		//die();
		//debug($aAcreditadoEtapaFicha);
		
		$gastos = [11, 12];
		
		$items = ORM::factory('Evento', $this->id)
			->get_items()
			->select('*', DB::expr('0 total'), DB::expr('0 cantidad'))
			->where('gael_id', 'in', $gastos)
			->execute()
			->as_array('itga_id');
		
		$items['H'] = [
			'gael_nombre' => 'Hardware',
			'itga_nombre' => 'Otros',
			'cantidad' => 0,
			'total' => 0,
		];
		$items['S'] = [
			'gael_nombre' => 'Software',
			'itga_nombre' => 'Otros',
			'cantidad' => 0,
			'total' => 0,
		];
		
		//debug($items);
		
		foreach ($aAcreditadoEtapaFicha as $oAcreditadoEtapaFicha)
		{
			$data = json_decode($oAcreditadoEtapaFicha->acef_data, TRUE);
			//debug2($data);
			
			$per = $data['totales']['fec'][3] / $data['totales']['fec']['total'];
			
			//debug2($data['totales'], numberformat($per, 2));
			
			if ($per > 0.6)
			{
				continue;
			}
			
			foreach ($data['actividades'] as $actividades)
			{
				//debug2($actividades);
				
				foreach ($actividades as $actividad)
				{
					foreach ($actividad['subactividades'] as $subactividad)
					{
						//debug($subactividad);
						
						$gasto = $subactividad['gasto'];
						$financiamiento = $subactividad['financiamiento'];
						
						if (in_array($gasto, $gastos) AND $financiamiento == 1)
						{
							$item = Arr::get($subactividad, 'item', 999);
							
							if ($item == 999)
							{
								$item = $gasto == 11 ? 'H' : 'S';
							}
							
							$subtotal = Arr::get($subactividad, 'subtotal');
							$cantidad = Arr::get($subactividad, 'cantidad');
							
							//$items
							//debug2($item);
							//$items[]
							$items[$item]['total'] += $subtotal;
							$items[$item]['cantidad'] += $cantidad;
							$items[$item]['costounitario'] = Arr::get($subactividad, 'costounitario');
						}
					}
				}
			}
		}
		
		$this->result = array_values($items);
	}
	
}
