<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Planes extends Report {
	
	protected $id;
	
	protected $evento;
	
	protected $comites;
	
	private $ids = [540, 809, 810];
	
	public function get_data($script = NULL)
	{
		$data = $this->get_fichas();
		
		return $data;
	}
	
	protected function get_fichas()
	{
		$status = Model_AcreditadoEtapa::ESTADO_ATENDIDO;
		$ids = '('.implode(',', $this->ids).')';
		
		return ORM::factory('AcreditadoEtapaFicha')
			->with('oEventoEtapaFicha')
			->with('oAcreditadoEtapa:oEventoEtapa')
			->with('oAcreditadoEvento')
			->select('acev_id')
			->where('oAcreditadoEtapa:oEventoEtapa.even_id', '=', $this->id)
			->where('evet_code', '=', 'RPM')
			->where('evef_code', '=', 'MPR')
			//->where('acet_estado', '=', Model_AcreditadoEtapa::ESTADO_ATENDIDO)
			->where(DB::expr(''), '', DB::expr("(acet_estado = {$status} or acev_id in {$ids})"))
			->find_all();
		
	}
	
	protected function set_data()
	{
		$this->id = $this->params['id'];
		
		$this->data = $this->get_data()//Report::get_data($this->name, $this->params)
			->as_array('acev_id');
		
		$query = SaesProxy::instance()
			->get('getComites', array('even_id' => $this->id));
			//debug($query);
			
		$this->evento = $query['evento'];
		$this->comites = $query['users'];
	}
	
	protected function set_result()
	{
		$this->result = $this->data;
	}
	
	public function get_context()
	{
		return array(
			'result' => $this->result,
			'title' => $this->title,
			'evento' => $this->evento,
			//'header' => $this->get_header(),
			'action' => 'view',
			'name' => $this->name,
		);
	}
	
}
