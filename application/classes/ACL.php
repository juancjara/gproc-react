<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class ACL extends Kohana_ACL {
	
	const ROLE_SUPER = 'super';
	
	public function __construct($init = FALSE)
	{
		parent::__construct($init);
		
		$this->register();
	}
	
	private function register()
	{
		//if (Auth::instance()->get_user()->id == 1) $this->_auth->set_role('super');
		//if ($role = Request::$current->query('role')) 
			//$this->_auth->set_role($role);
		
		$this->add_resources();
		
		$this->add_roles();
		
		$this->add_rules();
	}
	
	private function add_resources()
	{
		$this->add_controllers('regular', TRUE);
		$this->add_controllers('evaluador', TRUE);
		$this->add_controllers('administrador', TRUE);
		//debug($this);
	}
	
	public function add_controllers($resource, $add_prefix = FALSE)
	{
		$prefix = $add_prefix ? $resource.'.' : NULL;
		
		$this->add_resource($resource);
		
		$controllers = Kohana::list_files('classes/Controller/'.ucfirst($resource), array(APPPATH));
		
		foreach ($controllers as $controller => $path)
		{
			$controller = lcfirst(basename($controller, EXT));
			//debug2($controller);
			
			$this->add_resource($prefix.$controller, $resource);
		}
		
		//debug($this);
	}
	
	private function add_roles()
	{
		$this->add_role('regular');
		$this->add_role('evaluador');
		$this->add_role('administrador');
		$this->add_role('supervisor');
	}
	
	private function add_rules()
	{
		//debug($this);/admin	
		
		// Regular
		//debug(Auth::instance()->get_user());
		$this->allow('regular', 'regular');
		
		// Supervisor
		$this->allow('supervisor', 'evaluador');
		
		// Evaluador
		$this->allow('evaluador', 'regular');
		$this->allow('evaluador', 'evaluador');
		
		// Admin
		$this->allow('administrador');
		
//		$this->allow('admin','admin');
		
		//$a = $this->is_allowed('admin', 'regular');
		//debug($a);
	}
	
	public function get_sidebar()
	{
		$items = Kohana::$config->load('sidebar')->as_array();
		//debug2($items);
		
		$sidebar = Arr::filter_recursive($items, array($this, 'check_resource'));
		//debug($sidebar);
		
		return $sidebar;
	}
	
	public function check_resource($path)
	{
		//debug2($path);
		
		$item = explode('/', $path);
		//debug($item);
		
		$resource = $item[0];
		$privilege = Arr::get($item, 1);
		
		//return $this->is_allowed('admin', $resource, $privilege) ? $path : FALSE;
		return $this->check($resource, $privilege) ? $path : FALSE;
	}
	
	public function get_role()
	{
		return $this->_auth->get_role_id();
	}
	
	public function check_role($role)
	{
		//return $this->get_role() === $role;
		return (int) $this->get_role_id() === $role;
	}
	
	public function get_role_id()
	{
		return $this->_auth->get_user()->role_id;
	}
	
}
