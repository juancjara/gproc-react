<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Welcome extends Controller_Template {
	
	use Middleware_Trait;
	
	public $template = 'welcome/template';

	protected $_config;
	
	/**
	 *
	 * @var  Model_User
	 */
	public $oUser;
	
	/**
	 *
	 * @var  Model_Acreditado
	 */
	public $oAcreditado;
	
	public $result;
	
	public function before()
	{
		$this->_check_ajax();
		
		parent::before();
		
		$this->_set_controller_globals();
		$this->_set_view_globals();
		$this->_set_template_blocks();
	}
	
	protected function apply_middlewares()
	{
		try
		{
			$this->login_regular_or_staff();
		}
		catch (Middleware_Exception $e)
		{
			Session::instance()->set('error', $e->getMessage());
			$this->redirect('/'.URL::query());
		}
	}
	
	private function login_regular_or_staff()
	{
		try
		{
			$this->set_regular_middlewares();
		}
		catch (Middleware_Exception_RegularNoAccess $e)
		{
			// tries auth staff
			// sets oUser
			// throws NoAccess
			$this->middleware('StaffAuth');
		}
	}
	
	private function set_regular_middlewares()
	{
		// gets result from saes
		// sets result
		// throws RegularNoAccess
		$this->middleware('RegularGetDataFromSaes');
		
		// gets result from controller
		// filters events that don't exist in GPROC
		// throws NoEvents
		$this->middleware('RegularFilterEventosGproc');
		
		// checks porcentajes from result
		// throws NoEventsPassMinimum
		//$this->middleware('RegularCheckPorcentajes');
		
		$this->middleware('RegularCreateAcreditado');
		$this->middleware('RegularLogin');
		$this->middleware('RegularCreateAcreditadoEventos');
		$this->middleware('RegularCreateAutoevaluaciones');
	}
	
	public function action_index()
	{
		$this->_check_login();
		
		if ($this->request->method() == 'POST')
		{
			$this->apply_middlewares();
			
			$this->_check_login();
			exit;
		}
		
		$view = View::factory('welcome/login');
		$view->error = Session::instance()->get_once('error');
		
		$this->template->content = $view;
	}
	
	public function action_salir()
	{
		Auth::instance()->logout(TRUE);
		$this->redirect('/');
	}
	
	protected function _check_ajax()
	{
		if ($this->request->is_ajax())
		{
			$this->auto_render = FALSE;
		}
	}
	
	protected function _set_controller_globals()
	{
		$this->oUser = Auth::instance()->get_user();
		$this->_config = Kohana::$config->load('general');
	}
	
	protected function _set_view_globals()
	{
		//debug($this->_config);
		View::bind_global('site_title', $this->_config->site_title);
	}

	protected function _set_template_blocks()
	{
		if ($this->auto_render)
		{
			$this->template->styles = View::factory('welcome/styles');
			$this->template->scripts = View::factory('welcome/scripts');
			$this->template->footer = View::factory('welcome/footer');
		}
	}

	protected function _check_login()
	{
		$auth = Auth::instance();
		
		if (Session::instance()->get('oAcreditado'))
		{
			if ($close = $this->request->query('close'))
			{
				die('<script>window.close();</script>');
			}
			
			$this->redirect('/eventos');
		}
		
		if ($oUser = $auth->get_user())
		{
			$role = ACL::instance()->get_role_id();
			
			if (in_array($role, [Model_Role::ROLE_ADMIN]))
			{
				$this->redirect('/admin');
			}
			elseif (in_array($role, [Model_Role::ROLE_EVALUADOR, Model_Role::ROLE_SUPERVISOR]))
			{
				$this->redirect('/evaluador');
			}
			
			throw new Kohana_Exception('No role');
		}
	}

	public static function redirect($uri = '', $code = 302)
	{
		if ($redirect = Request::$current->query('r'))
		{
			$uri = $redirect;
		}
		
		parent::redirect($uri, $code);
	}
	
} // End Welcome
