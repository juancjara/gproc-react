<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Admin_Test extends Controller_Admin {
	
	
	
	public function action_index()
	{
		$query = "select c.*
			, if(c.id = 'R', 'RPP,RPC', group_concat(cd.id)) children 
			from catalogo as c
			LEFT JOIN  catalogo as cd ON c.id = cd.parent_id 
			WHERE c.id <> 'RV'
			GROUP BY c.id
			UNION
			select 'RPP','Pasantías','Pasantías','R',null,'RPT,RPA'
			UNION
			select 'RPC','Capacitación','Capacitación','R',null,'RPT,RPA'
		;";
		
		$result = DB::query(Database::SELECT, $query)
			->execute()->as_array();
		
		echo json_encode($result);die();
	}
	
	
	
}