<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Admin_Utils extends Controller_Admin {
	
	private $title = 'Utils';
	
	public function action_index()
	{
		$methods = get_class_methods($this);
		$actions = array_filter($methods, function ($m) { 
			return preg_match('/^action/', $m); 
		});
		
		debug($actions);
	}
	
	public function action_freeze()
	{
		$aAcreditadoEtapaFicha = ORM::factory('AcreditadoEtapaFicha')
			->with('oEventoEtapaFicha:oEventoEtapa')
			->with('oAcreditadoEtapa:oAcreditadoEvento')
			->select('oAcreditadoEtapa.acev_id')
			->where('evet_code', '=', 'REI')
			->where('evef_code', '=', 'FEI')
			->where('acef_estado', 'in', Model_AcreditadoEtapaFicha::$finalizado) // 92 >> 83
			->find_all();
		
		//debug($aAcreditadoEtapaFicha);
		
		$limit = 5;
		$count = 0;
		foreach ($aAcreditadoEtapaFicha as $oAcreditadoEtapaFicha)
		{
			//debug($oAcreditadoEtapaFicha);
			
			//$data = json_decode($oAcreditadoEtapaFicha->acef_data, TRUE);
			//debug($data);
			//debug($oAcreditadoEtapaFicha->oEventoEtapaFicha);
			$url = URL::base()
				.'expresionInteres'
				.URL::query(['acev_id' => $oAcreditadoEtapaFicha->acev_id]);
			
			//debug2($url);
			
			//$url = '/admin/utils';
			
			//$url = URL::base(TRUE);
			//$opts = array('http' => array('header'=> 'Cookie: ' . $_SERVER['HTTP_COOKIE']."\r\n"));
			//$context = stream_context_create($opts);
			//$content = file_get_contents($url, FALSE, $context);
			
			//$content = file_get_contents($url);
			
			$content = Request::factory($url)
				->execute();
			
			debug2($content);
			
			debug(Theme::instance()
				->template
				->cont);
				
			//debug($this->auto_render);
			echo $content;
			die();
			
			
			
			break;
		}
		
	}
	
	/*protected function _set_template_blocks()
	{
		parent::_set_template_blocks();
		
		if ($this->auto_render)
		{
			$theme = Theme::instance();
			$theme->css('page', 'evaluador', '/gproc_lte/css/evaluador.css');
			$theme->js('page', 'evaluador', '/gproc_lte/js/evaluador.js');
			
			$theme->template->header = Theme_View::factory('evaluador/template/header');
			$theme->template->sidebar = Theme_View::factory('admin/template/sidebar');
		}
	}*/
	
}
