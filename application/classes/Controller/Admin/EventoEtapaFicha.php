<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Admin_EventoEtapaFicha extends Controller_Admin {

	private $title = 'Fichas';
	
	public function before()
	{
		parent::before();
		
		if ( ! ACL::instance()->allowed('admin.fichas'))
		{
			$this->redirect($this->request->referrer());
		}
	}
	
	public function action_index()
	{
		
		$labels = array(
			'tiin_id' => 'Tipo Institución',
			'even_id' => 'Evento',
			'evet_nombre' => 'Etapa',
			'evef_nombre' => 'Ficha',
			'evef_code' => 'Código',
			'evef_path' => 'Path',
		);

		$query = DB::select(DB::expr('SQL_CALC_FOUND_ROWS *'))
			->from('evento_etapa_ficha')
			->join('evento_etapa')
				->using('evet_id')
			->join('evento')
				->using('even_id')
			->where('even_id', '<>', Model_Evento::EVENTO_PLANTILLA);
		
		$evento_etapas = $this->get_evento_etapas();
		
		$table = Table::factory('EventoEtapaFicha')
			->query($query)
			->labels($labels)
			->options('even_id',  Model_Evento::get_names('even_nombre'))
			->options('tiin_id', Model_Acreditado::$tipos)
			->columns(array_keys($labels))
			->filter('tiin_id')
			->filter('even_id', NULL, Model_Evento::get_names('even_name', TRUE))
			->filter('evet_id', NULL, $evento_etapas)
			->search('evef_nombre', 'Ficha')
			->search('evet_nombre', 'Etapa')
			->bulk_actions('new')
			->actions(array('edit', 'delete'))
			->build();
		
		$subtitle = 'Lista';

		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	private function get_evento_etapas()
	{
		$query = ORM::factory('EventoEtapa')
			->select(DB::expr("concat(even_id, ' - ', evet_nombre) evet_name"))
			->where('even_id', '<>', Model_Evento::EVENTO_PLANTILLA)
			->order_by('even_id')
			->order_by('evet_id');
		
		if ($even_id = $this->request->query('filter__even_id'))
		{
			$query->where('even_id', '=', $even_id);
		}
		
		return $query
			->find_all()
			->as_array('evet_id', 'evet_name');

	}
	
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin/eventos', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/eventos');
	}
	
	public function action_new()
	{
		$this->action_edit();
	}
	
	public function action_edit()
	{
		$id = $this->request->param('id');
		
		$labels = array(
			'even_id' => 'Evento',
			'evet_id' => 'Etapa',
			'evef_nombre' => 'Ficha',
			'evef_code' => 'Código',
			'evef_path' => 'Path',

		);
		
		$query = DB::select(DB::expr('SQL_CALC_FOUND_ROWS *'))
			->from('evento_etapa_ficha')
			->join('evento_etapa')
				->using('evet_id')
			->join('evento')
				->using('even_id')
			->where('evef_id', '=',$id);
		
		$id_evento = $query->execute()->get('even_id');
		
		$evet_id = DB::select('evet_id')
			->from('evento_etapa')
			->where('even_id', '=', $id_evento)
			->execute()
			->get('evet_id');

		$aEtapas = Model_Evento::get_etapas($id_evento);

		$form = AdminForm::factory('EventoEtapaFicha')
			->labels($labels)
			->query($query)
			->options('even_id' , $this->_get_query())
			->options('evet_id', $aEtapas)
			->custom('even_id', array(
				'class' => 'ajax',
				'data-path' => '/ajax/get_even_etapa/',
				'data-target' => '#evet_id',
			))
			->custom('evet_id', array(
				'data-selected' => $this->request->post('evet_id') ?: $evet_id ,
			))
			->columns(array_keys($labels))
			->readonly(array(
				'tiin_id',
				'evef_code',
				'evef_path',
			))
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'New';
	
		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
		
	public function action_delete()
	{
		$id = $this->request->param('id');
		$oEventoEtapaFicha = ORM::factory('EventoEtapaFicha', $id);
		$oEventoEtapaFicha->delete();
		$this->redirect('/admin/EventoEtapaFicha');
	}
	
	protected function _get_query()
	{
		return Model_Evento::get_names('even_name');
	}
	
	
}