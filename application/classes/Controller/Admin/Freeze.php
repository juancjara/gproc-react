<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Admin_Freeze extends Controller_Admin {
	
	const MAX = 60;
	
	private $title = 'Utils';
	
	public $auto_render = FALSE;
	
	private $eventos = [10, 11];
	
	private $i = 0;
	
	public function action_index()
	{
		set_time_limit(0);
		
		$this->get_even_dirs();
		
		echo Form::open()
			.Form::submit(NULL, NULL)
			.Form::close();
		
		$log = LogTable::factory(['border' => 1]);
		
		$this->process_acevs($log);
		
		$log->stop();
	}
	
	private function process_acevs($log)
	{
		$aAcreditadoEtapaFicha = $this->getAcreditadoEtapaFicha();
		
		//$i = 0;
		foreach ($aAcreditadoEtapaFicha as $oAcreditadoEtapaFicha)
		{
			$even_id = $oAcreditadoEtapaFicha->even_id;
			$acev_id = $oAcreditadoEtapaFicha->acev_id;
			$evef_code = $oAcreditadoEtapaFicha->evef_code;
			
			$log->log(
				compact('even_id', 'acev_id') + 
				[
					'acet_id' => $oAcreditadoEtapaFicha->acet_id,
					'acef_id' => $oAcreditadoEtapaFicha->acev_id,
					'evet_code' => $oAcreditadoEtapaFicha->evet_code
				] + 
				compact('evef_code')
			);
			
			//$i += 
			$this->process_urls($log, compact('even_id', 'acev_id', 'evef_code'));
			
			//if ($i > 8) break;
		}
	}
	
	private function process_urls($log, $vars)
	{
		//debug2($vars);
		$acev_id = $vars['acev_id'];
		$acev_dir = $this->get_acev_dir($vars);
		
		$urls = $this->get_urls($vars['evef_code']);
		//debug($urls);
		
		//$i = 0;
		foreach ($urls as $base_url)
		{
			$file = $acev_dir.$this->get_filename($base_url);
			//debug($file);

			$log->partial(compact('file'));
			
			if (file_exists($file))
			{
				$log->log(['X' => 'exists']);
				continue;
			}
			
			$url = $base_url.URL::query(compact('acev_id'));
			//debug($url);
			//debug2($url);
			//continue;
			
			$log->partial(compact('url'));
			
			if ($this->can_process())
			{
				$this->process_request($log, $vars + compact('url', 'file'));
			}
			else
			{
				$log->log(['X' => 'waiting...']);
			}
		}
	}
	
	private function process_request($log, $vars)
	{
		//debug2($vars);
		Theme::init('adminlte');
		
		$response = Request::factory($vars['url'])
			//->query('acev_id', $vars['acev_id'])
			->execute();
		
		//echo $log->log()->show();die();
		//debug($response);
		//echo $response; die();
		
		if ($response->status() !== 200)
		{
			//echo $response; die();
			$log->log(['X' => 'ERROR: '.Session::instance()->get_once('error')]);
			
			return;
		}
		
		file_put_contents($vars['file'], $response->body());
		$log->log(['X' => 'OK']);

		$this->i++;
	}
	
	private function can_process()
	{
		return $this->request->method() === 'POST' AND 
			$this->i < self::MAX;
	}
	
	private function get_acev_dir($vars)
	{
		$dirs = $this->get_even_dirs();
		
		$even_dir = $dirs[$vars['even_id']];
		
		$acev_dir = "{$even_dir}{$vars['acev_id']}/";
		if ( ! is_dir($acev_dir))
		{
			mkdir($acev_dir);
		}
		return $acev_dir;
	}
			
	private function get_even_dirs()
	{
		$base_dir = APPPATH.'freeze/';
		if ( ! is_dir($base_dir))
		{
			mkdir($base_dir);
		}
		
		$dirs = [];
		foreach ($this->eventos as $even_id)
		{
			$even_dir = "{$base_dir}{$even_id}/";
			if ( ! is_dir($even_dir))
			{
				mkdir($even_dir);
			}
			
			$dirs[$even_id] = $even_dir;
		}
		
		return $dirs;
	}
	
	private function get_urls($evef_code = NULL)
	{
		$urls = [
			'FEI' => [
				//'/expresionInteresOld',
				'/expresionInteresOld/pdf',
			],
			'MPL' => [
				//'/planificacion',
				'/planificacion/pdf',
			],
			'MPR' => [
				//'/presupuesto',
				'/presupuesto/pdf',
				'/presupuesto/excel',
			],
			'MCR' => [
				//'/cronograma',
				'/cronograma/pdf',
			],
			'MME' => [
				//'/monitoreo',
				'/monitoreo/pdf',
			],
			'MEE' => [
				//'/efectos',
				'/efectos/pdf',
			],
		];
		
		return $evef_code ? $urls[$evef_code] : $urls;
	}
	
	private function getAcreditadoEtapaFicha()
	{
		$aAcreditadoEtapaFicha = ORM::factory('AcreditadoEtapaFicha')
			->with('oEventoEtapaFicha:oEventoEtapa')
			->with('oAcreditadoEtapa:oAcreditadoEvento')
			->select('oAcreditadoEtapa.acev_id')
			->select('oAcreditadoEtapa:oAcreditadoEvento.even_id')
			->select('evet_code', 'evef_code')
			->select('acet_estado', 'acef_estado')
			
			//->where('evet_code', '=', 'REI')
			->where('evef_code', 'in', array_keys($this->get_urls()))
			->where('oAcreditadoEtapa:oAcreditadoEvento.even_id', 'in', $this->eventos)
			//->where('acef_estado', 'in', Model_AcreditadoEtapaFicha::$finalizado) // 92 >> 83
			//->where('acet_estado', 'in', Model_AcreditadoEtapa::$finalizado)
			->find_all();
		
		debug2($aAcreditadoEtapaFicha->count());
		
		return $aAcreditadoEtapaFicha;
	}
	
	private function get_filename($url)
	{
		//debug2($url);
		$arr = array_values(array_filter(explode('/', $url)));
		$arr[1] = Arr::get($arr, 1, 'html');
		$arr[1] = $arr[1] == 'excel' ? 'xls' : $arr[1];
		
		return implode('.', $arr);
	}
	
}
