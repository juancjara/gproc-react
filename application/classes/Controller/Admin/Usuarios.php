<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Admin_Usuarios extends Controller_Admin {

	private $title = 'Usuarios';
	
	
	public function before()
	{
		parent::before();

		// @TEMP
		//debug(ACL::instance());
		if ( ! ACL::instance()->allowed('admin.usuarios'))
		{
			$this->redirect($this->request->referrer());
		}
	}

	public function action_index()
	{
		$estados = Model_User::$estados;
		$roles = Model_Role::$roles;
		
		unset($roles[1]);

		$labels = array(
			'usuario' => 'Usuario',
			'role_id' => 'Rol',
			'estado' => 'Estado',
		);

		$query = $this->_get_query()
			->where('u.role_id', '<>', Model_Role::ROLE_REGULAR); // We cannot edit a regular user here.

		$table = Table::factory('User')
			->query($query)
			->labels($labels)
			->columns(array_keys($labels))
			->options('role_id', $roles)
			->options('estado', $estados)
			->filter('role_id', NULL, $roles)//array_intersect_key(Model_User::$perfiles, array_flip($roles)))
			->filter('estado')
			->search('usuario', 'usuario')
			->bulk_actions(array('new'))
			->actions(array('edit', 'delete'))
			->build();

		$subtitle = 'Lista';

		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);

		Theme::instance()
			->template
			->content = $view;

		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}

	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin/usuarios', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/usuarios');
	}

	public function action_new()
	{
		$this->action_edit();
	}

	public function action_edit()
	{
		$estados = Model_User::$estados;
//		$roles = array_shift(Model_Role::$roles);
		$roles = Model_Role::$roles;
		unset($roles[1]);
		
		$id = $this->request->param('id');

		$labels = array(
			'usuario' => 'Usuario',
			'password' => 'Contraseña',
			'password_confirm' => 'Confirmar password',
			'role_id' => 'Rol',
			'estado' => 'Estado',
			'even_id' => 'Eventos'
		);

		$query = $this->_get_query()
			->where('u.role_id', '<>', Model_Role::ROLE_REGULAR)
			->where('u.id', '=', $id);
		
		$extra = Model_User::get_password_validation($this->request->post());

		$helptexts = array();
		
		if ($this->request->action() == 'new')
		{
			$extra->rule('password', 'not_empty');
		}
		else
		{
	
			$helptexts += array(
				'password_confirm' => 'Ingrese password 2 veces sólo si desea cambiarlo',
			);
		}
		
		if($this->request->post('password') == "")
		{
			
		}
		
		$form = AdminForm::factory('User')
			->query($query)
			->columns(array_keys($labels))
			->labels($labels)
			->types(array(
				'password' => 'password',
				'password_confirm' => 'password',
			))
			->helptexts($helptexts)
			->options('estado', $estados)
			->options('role_id', $roles)
			->options('even_id', Model_Evento::get_names('even_name' , TRUE))
//			->options('clave',Auth::instance()->hash($clave))
			->multiple('even_id', array(
				'alias' => 'aEvento',
				'column_name' => 'even_id',
			))
			->extra_validation($extra)
			->build();
		
		$subtitle = $id ? 'Editar #' . $id : 'New';

		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
			
		Theme::instance()
			->template
			->content = $view;

		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}

	public function action_delete()
	{
		$id = $this->request->param('id');
		$oUser = ORM::factory('User', $id);
		$oUser->set('estado', 0)->save();
		$this->redirect('/admin/usuarios');
	}

	protected function _get_query()
	{
		return DB::select(DB::expr('SQL_CALC_FOUND_ROWS u.*'))
				->from(DB::expr('user u'))
				->as_object('Model_User');
	}

}
