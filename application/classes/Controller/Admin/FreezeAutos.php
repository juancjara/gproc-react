<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Admin_FreezeAutos extends Controller_Admin {
	
	const MAX = 150;
	
	private $title = 'Utils';
	
	public function action_index()
	{
		set_time_limit(0);
		
		$aAcreditado = ORM::factory('Acreditado')
			//->where('acre_id', '=', 19544)
			->find_all();
		
		echo Form::open()
			.Form::submit(NULL, NULL)
			.Form::close();
		
		debug2($aAcreditado->count());
		
		$log = LogTable::factory(['border' => 1]);
		
		$i = 0;
		foreach ($aAcreditado as $oAcreditado)
		{
			$userData = SaesProxyV2::instance()->get('getUserData', ['acre_id' => $oAcreditado->acre_id]);
			
			$log->log([
				'acre' => $oAcreditado->acre_id,
				'status?' => $userData['status'] ? 1 : 0,
			]);
			
			if ($userData['status'] == FALSE) continue;
			
			// inst_id
			if ($this->request->method() === Request::POST)
			{
				$oAcreditado
					->set('inst_id', $userData['inst_id'])
					->save();
			}
			
			foreach ($userData['acev_list'] as $evento)
			{
				$oAcreditadoEvento = ORM::factory('AcreditadoEvento', $evento['acev_id']);
				
				$log->log(['acev?' => $oAcreditadoEvento->loaded() ? 1 : 0]);
				
				if ( ! $oAcreditadoEvento->loaded()) continue;
				
				$oAutoevaluacion = ORM::factory('Autoevaluacion', $evento['max_auto_id']);
				
				$log->partial([
					'acev' => $evento['acev_id'],
					'max_auto_id' => $evento['max_auto_id'],
					'loaded?' => $oAutoevaluacion->loaded() ? 1 : 0,
					'action' => 'waiting...',
				]);
				
				//$log->log()->stop();
				
				// No existe max_auto_id
				if ( ! $oAutoevaluacion->loaded())
				{
					//debug('x');
					if ($i++ < self::MAX AND $this->request->method() == 'POST')
					{
						//die('xxx');
						$oAutoevaluacion->create_from_even($evento);
						$log->partial(['action' => '**just created**']);
					}
				}
				else
				{
					$log->partial(['action' => 'already created']);
				}
				
				$log->log();
			}
		}
		
		$log->stop();
		die();
		
	}
	
	public function action_update()
	{
		set_time_limit(0);
		
		$aAutoevaluacion = ORM::factory('Autoevaluacion')
			->with('oAcreditadoEvento')
			->where('even_id', 'in', [18, 19])
			->find_all();
		
		echo Form::open()
			.Form::submit(NULL, NULL)
			.Form::close();
		
		debug2($aAutoevaluacion->count());
		
		$log = LogTable::factory(['border' => 1]);
		
		$i = 0;
		foreach ($aAutoevaluacion as $oAutoevaluacion)
		{
			$log->partial([
				'auto_id' => $oAutoevaluacion->auto_id,
				'auto_data' => substr($oAutoevaluacion->auto_estandares, 0, 10),
				'auto_data_f' => substr($oAutoevaluacion->auto_fuentes, 0, 10),
			]);
			
			$old_length = strlen($oAutoevaluacion->auto_estandares);
			$log->partial(['old_length' => $old_length]);
			
			$old_length_f = strlen($oAutoevaluacion->auto_fuentes);
			$log->partial(['old_length_f' => $old_length_f]);
			
			// No existe max_auto_id
			//if ( ! $oAutoevaluacion->loaded())
			//debug('x');
			if ($i++ < self::MAX)
			{
				$result = SaesProxyV2::instance()
					->get('getAutoDataFromAPI', ['auto_id' => $oAutoevaluacion->auto_id]);

				debug2($result);

				$values = [
					//'auto_resumen' => json_encode($result['auto_resumen']),
					'auto_estandares' => json_encode($result['auto_estandares']),
					'auto_fuentes' => json_encode($result['auto_fuentes']),
				];
				
				$new_length = strlen($values['auto_estandares']);
				$new_length_f = strlen($values['auto_fuentes']);
				
				$log->partial([
					'new_length' => $new_length,
					'status' => $new_length > $old_length ? '>' : ($new_length == $old_length ? '=' : 'MINOR!!!'),
					'new_length_f' => $new_length_f,
					'status_f' => $new_length_f > $old_length_f ? '>' : ($new_length_f == $old_length_f ? '=' : 'MINOR!!!'),
				]);
				
				if ($this->request->method() == 'POST')
				{
					try
					{
						$oAutoevaluacion
							->values($values)
							->update();
						
						$log->partial(['done' => 'done']);
					}
					catch (Exception $e)
					{
						debug($e);
					}
				}
			}
			
			$log->log();
		}
		
		$log->stop();
		die();
		
	}
	
}
