<?php

defined('SYSPATH') or die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Admin_Rol extends Controller_Admin {

	private $title = 'Roles';

	public function before()
	{
		parent::before();

		// @TEMP
		//debug(ACL::instance());
		if (!ACL::instance()->allowed('admin.rol'))
		{
			$this->redirect($this->request->referrer());
		}
	}

	public function action_index()
	{
		$estados = Model_Rol::$estados;

		$labels = array(
			'name' => 'Rol',
			'description' => 'Descripción',
			'estado' => 'Estado',
		);

		$table = Table::factory('Rol')
			->labels($labels)
			->columns(array_keys($labels))
			->filter('estado', NULL, $estados)
			->options('estado', $estados)
			->search('name', 'Rol')
			->build();

		$subtitle = 'Lista';

		$view = Theme_View::factory('admin/rol/index')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);

		Theme::instance()
			->template
			->content = $view;

		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}

	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin/rol', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/rol');
	}

	public function action_new()
	{
		$this->action_edit();
	}

	public function action_edit()
	{
		$estados = Model_Rol::$estados;

		$id = $this->request->param('id');

		$labels = array(
			'name' => 'Rol',
			'description' => 'Descripción',
			'estado' => 'Estado',
		);

		$form = AdminForm::factory('Rol')
			->columns(array_keys($labels))
			->labels($labels)
			->build();

		$subtitle = $id ? 'Editar #' . $id : 'New';

		$view = Theme_View::factory('admin/template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
			
		Theme::instance()
			->template
			->content = $view;

		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}

	public function action_delete()
	{
		$id = $this->request->param('id');
		$oRol = ORM::factory('Rol', $id);
		$oRol->set('estado', 0)->save();
		$this->redirect('/admin/rol');
	}
	
}
