<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Admin_Catalogo extends Controller_Admin {
	
	private $title = 'Catálogo';
	
	/**
	 *
	 * @var  Model_Catalogo
	 */
	private $oParent;
	
	public static $btn_next = array(
		'item' => array(
			'btn_class' => 'btn btn-sm btn-flat btn-facebook',
			'icon_class' => 'fa fa-hand-o-right',
			'data-title' => 'Siguiente',
		));
	
	public function action_index()
	{
		$this->action_item();
	}
	
	public function action_item()
	{
		$id = $this->request->param('id');
		
		$oParent = ORM::factory('Catalogo', $id);
		
		$labels = array(
			'id' => 'ID',
			'name' => 'Tipo',
 		);
		
		// All items from parent $id, with their children
		$query = Repository::factory('Catalogo')
			->get_query()
			->with_children()
			->where('c.parent_id', '=', $id);
		
		// Get first item
		$oItem = $query->execute()->current();
		//debug($oItem);
		
		$table = Table::factory('Catalogo')
			->query($query)
			->labels($labels)
			->columns(array_keys($labels));
		
		// Parent exists
		if ($oParent->loaded())
		{
			$table->bulk_actions([
				'back' => [
					'path' => '/admin/catalogo/item/'.$oParent->parent_id,
					'btn_class' => 'btn btn-flat btn-default',
					'icon_class' => 'fa fa-arrow-left',
					'data-title' => 'Anterior',
				]]);
		}
		
		// First item exists and it has children
		if ($oItem AND ! $oItem->is_leaf())
		{
			$table->actions(self::$btn_next);
		}
		
		if ($oItem AND $oItem->can_have_path())
		{
			$table
				->columns('path')
				->labels('path', 'Path');
		}
		
		//else
		{
			$table->actions(array('edit', 'delete'));
		}
		
		$table = $table
			->bulk_actions(array('new' => array('path' => '/admin/catalogo/new/')))
			->build();	
		
		$subtitle = $oParent->loaded() ? 'Items de: '.$oParent->name : '';

		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);

		Theme::instance()
			->template
			->content = $view;
	
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin/catalogo', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/catalogo');
	}
	
	public function action_new()
	{
		$this->action_edit();
	}
	
	public function action_edit()
	{
		$id = $this->request->param('id');
		
		$labels = array(
			'id' => 'ID',
			'name' => 'Nombre',
			'short_name' => 'Nombre corto',		
 		);
		 
		//$oParent = ORM::factory('Catalogo', $id);
		
		// Get item with its children
		$query = Repository::factory('Catalogo')
			->get_query()
			->with_children()
			->where('c.id', '=', $id);
		
		$aParent = Repository::factory('Catalogo')
			->get_query()
			->with_children()
			->select(DB::expr("concat(c.id, '. ', c.name) alias"))
			//->having('n_children', '>', 0)
			->execute()
			->as_array('id', 'alias');
		//debug($aParent);
		
		// Get THE item
		$oItem = $query->execute()->current();
		
		// EDIT && is_leaf
		if ($oItem AND ! $oItem->n_children)
		{
			$data = json_decode($oItem->data, TRUE);
			$costo = $data['costo'];
			$unidad = $data['unidad'];
			
			$query->select(DB::expr("'$costo' costo"))
				->select(DB::expr("'$unidad' unidad"));
		}
		
		$form = AdminForm::factory('Catalogo')
			->query($query)
			->labels(['parent_id' => 'Parent'] + $labels)
			->options('parent_id', $aParent)
			->required(['id', 'name']);
		
		// EDIT
		if ($oItem)
		{
			$form
				->readonly('parent_id');
		}
		
		// NEW || is_leaf
		if ( ! $oItem OR $oItem->is_leaf())
		{
			$form->labels('costo', 'Costo')
				->labels('unidad', 'Unidad')
				->callback('new', 'after', [$this, 'set_data'])
				->callback('edit', 'after', [$this, 'set_data']);
		}
		
		// NEW || (is_leaf && id not like ['SP%', 'R%', 'B2%'])
		if ( ! $oItem OR $oItem->can_have_path())
		{
			$form->labels('path', 'Ruta archivo');
		}
		
		$form = $form->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'New';
	
		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	public function set_data($oItem)
	{
		$costo = $this->request->post('costo');
		$unidad = $this->request->post('unidad');
		
		$data = ($costo OR $unidad) ? 
			json_encode(compact('costo', 'unidad')) :
			NULL;
		
		return $oItem
			->set('data', $data)
			->save();
	}
	
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oItem = ORM::factory('Catalogo', $id)
			->delete();
		
		Session::instance()->set('info', 'Item borrado');
		
		$this->redirect($this->request->referrer());
	}
	
}
