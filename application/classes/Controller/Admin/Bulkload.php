<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Admin_Bulkload extends Controller_Admin {

	protected $theme = 'adminlte';
	
	protected $title = 'Carga masiva';
	
	protected $_header_title = 'Admin';
	
	public function action_index()
	{
		$this->action_table();
	}
	
	public function action_table()
	{
		if ($this->request->method() == 'POST')
		{
			$type = $this->request->post('type_catalogo');
			$status = $this->evaluated($type, $_FILES['file']);	
			
			if ($status)
			{
				Session::instance()->set('info', 'Data cargada correctamente');
			}
			else
			{
				Session::instance()->set('error', 'Some error');
			}
		}
	
		$view = Theme_View::factory('admin/bulkload/table');

		Theme::instance()
			->template
			->content = $view;
	}
	
	public function evaluated($type, $file)
	{
		switch($type)
		{
			case (Model_Catalogo::BIENES) :
				
				Bulk::factory('Bienes')
					->process($file);
			
			break;
		
			case (Model_Catalogo::SERVICIOS) :
			
				Bulk::factory('Servicios')
					->process($file);
				
			break;
		
			default:
			
				Bulk::factory('Catalogo')
					->process($file);
		}
		
		return TRUE;	
	}

}
