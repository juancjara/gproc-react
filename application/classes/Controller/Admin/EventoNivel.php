<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Admin_EventoNivel extends Controller_Admin {

	private $title = 'Niveles';
	
	public function before()
	{
		parent::before();
		
		// @TEMP
		//debug(ACL::instance());
		if ( ! ACL::instance()->allowed('admin.niveles'))
		{
			$this->redirect($this->request->referrer());
		}
	}
	
	public function action_index()
	{
		$labels = array(
			'tiin_id' => 'Tipo Institución',//tiin_id
			'even_id' => 'Evento', //evento
			'evni_nombre' => 'Nivel',
			'evni_fecha_inicio_evaluado' => 'Inicio',
			'evni_fecha_fin_evaluado' => 'Fin',
		);
		
		$query = DB::select(DB::expr('SQL_CALC_FOUND_ROWS *'))
			->from('evento_nivel')
			->join('evento')
				->using('even_id')
			->where('even_id', '<>', 0);

		$table = Table::factory('EventoNivel')
			->query($query)
			->labels($labels)
			->options('even_id',  Model_Evento::get_names('even_nombre'))
			->options('tiin_id', Model_Acreditado::$tipos)
			->search('evni_nombre','Nivel')
			->columns(array_keys($labels))
			->filter('tiin_id', NULL, array_intersect_key(Model_Acreditado::$tipos, array_flip(Model_Acreditado::$tipos)))
			->filter('even_id', NULL, Model_Evento::get_names('even_name',TRUE))
			->bulk_actions('new')
			->actions(array('edit', 'delete'))
			->build();
		
		$subtitle = 'Lista';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin/niveles', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/niveles');
	}
	
	public function action_new()
	{
		$this->action_edit();
	}
	
//	public static function eventos()
//	{
//		$eventos_saes = Session::instance()->get('eventos');
//
//		foreach ($eventos_saes as $key => $value)
//		{
//			$evento_saes[$value['even_id']] = $value['even_nombre'];
//		}
//		
//		$eventos =  ORM::factory('evento')->find_all()->as_array();
//		foreach ($eventos as $key => $value)
//		{
//			foreach ($eventos_saes as $key2 => $value2)
//			{
//				if($value->even_id == $value2['even_id'])
//				{
//					//debug2($value2);
//					if($value->even_id != 0)
//					{
//						$eventos_gproc[$value->even_id] = $value2['even_nombre']." - ".Model_Acreditado::$tipos[$value2['tiin_id']];
//					}
//				}
//			}
//		}
//		//debug($eventos_gproc);
//		
//		return $eventos_gproc;
//	}
	
	public function action_edit()
	{
		$id = $this->request->param('id');
		//debug($this->eventos());
		
		$labels = array(
			//'tiin_id' => 'Tipo Institución',//tiin_id
			'even_id' => 'Evento', //evento
			'evni_nombre' => 'Nivel',
			'evni_fecha_inicio_evaluado' => 'Fecha Inicio',
			'evni_fecha_fin_evaluado' => 'Fecha Fin',
			//'even_nombre' => 'Evento',
		);
		//debug($id);
		$query = DB::select(DB::expr('SQL_CALC_FOUND_ROWS *'))
			->from('evento_nivel')
			->join('evento')
				->using('even_id')
			->where('evni_id', '=', $id);
		
		
		
		$form = AdminForm::factory('EventoNivel')
			->query($query)
			->columns(array_keys($labels))
			->labels($labels)
			->options('even_id',  Model_Evento::get_names())
			->options('tiin_id', Model_Acreditado::$tipos)
			->build();
		
		$query2 = DB::select(DB::expr('SQL_CALC_FOUND_ROWS *'))
			->from('evento_nivel')
			->join('evento')
				->using('even_id');
				
		if($id)
		{

			//unset($labels['tiin_id']);
			$form = AdminForm::factory('EventoNivel')
				->query($query)
				->options('even_id',  Model_Evento::get_names())
				->columns(array_keys($labels))
				->labels($labels)
				->options('tiin_id', Model_Acreditado::$tipos)
				->readonly(array(
					'even_id',
				))
				->required(array(
					'even_id',
					'evni_nombre',
					'evni_fecha_inicio_evaluado',
					'evni_fecha_fin_evaluado',
				))
				->custom(array('tiin_id'), 'disabled')
				->build();
		}
		else
		{
			
			$form = AdminForm::factory('EventoNivel')
				->query($query2)
				->options('even_id',  Model_Evento::get_names())
				->columns(array_keys($labels))
				->labels($labels)
				->options('tiin_id', Model_Acreditado::$tipos)
				->options('even_id', Model_Evento::get_names())
				->required(array(
					'even_id',
					'evni_nombre',
					'evni_fecha_inicio_evaluado',
					'evni_fecha_fin_evaluado',
				))
				->build();
		}

		$subtitle = $id ? 'Editar #'.$id : 'New';

		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
		
	public function action_delete()
	{
		$id = $this->request->param('id');
		$oUser = ORM::factory('User',$id);
		$oUser->set('estado', 0)->save();
		$this->redirect('/admin/usuarios');
	}
}