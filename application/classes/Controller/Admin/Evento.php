<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Admin_Evento extends Controller_Admin {
	
	private $title = 'Eventos';
	
	public function action_index()
	{
		//$eventos = Arr::array_to_assoc($this->eventos, ['even_id', 'even_name']);
		//debug($eventos);
		
		$eventos = Model_Evento::get_names('even_nombre');
		
		$labels = ORM::factory('Evento')->labels();
		
		array_pop($labels);

		$query = $this->_get_query()
			->where('even_id', '<>', Model_Evento::EVENTO_PLANTILLA)
			->order_by('tiin_id')
			->order_by('even_id');
		
		$table = Table::factory('Evento')
			->query($query)
			->labels($labels)
			->options('even_id', $eventos)
			->options('tiin_id', Model_Acreditado::$tipos)
			->columns(array_keys($labels))
			->bulk_actions(array('sincronizar' => array(
				'btn_class' => 'btn btn-flat btn-primary',
				'icon_class' => 'fa fa-refresh',
				'title' => 'Sincronizar',
			)))
			->actions('edit')
			->filter('tiin_id')
			->build();
		
		$subtitle = 'Lista';

		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin/eventos', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/eventos');
	}
	
	public function action_new()
	{
		$this->action_edit();
	}
	
	public function action_sincronizar()
	{
		//Model_Evento::set_eventos();
		//$eventos = Model_Evento::get_all();
		
		$eventos = Model_Evento::get_from_saes();
		
		foreach ($eventos as $evento)
		{
			$oEvento = ORM::factory('Evento', $evento['even_id']);
			
			/*if ( ! $evento['even_fec'])
			{
				continue;
			}*/
			
			if ( $oEvento->loaded())
			{
				continue;
			}
			
			$oEvento->create_from_saes($evento);
		}
		
		$this->redirect($this->request->referrer());
	}
	
	public function action_edit()
	{
		$id = $this->request->param('id');

		$labels = ORM::factory('Evento')->labels();
		
		$query = $this->_get_query()
			->where('e.even_id', '=', $id);
		
		$form = AdminForm::factory('Evento')
			->labels($labels)
			->query($query)
			->options('even_id', Model_Evento::get_names('even_name'))
			->options('tiin_id', Model_Acreditado::$tipos)
			->types('even_descripcion', 'textarea')
			->columns(array_keys($labels))
			->readonly(['even_id', 'tiin_id'])
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'New';
	
		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
		
	public function action_delete()
	{
		$id = $this->request->param('id');
		$oEvento = ORM::factory('Evento',$id);
		$oEvento->set('estado', 0)->save();
		$this->redirect('/admin/eventos');
	}
	
	protected function _get_query()
	{
		return DB::select(DB::expr('SQL_CALC_FOUND_ROWS e.*'))
			->from(DB::expr('evento e'))
			->as_object('Model_Evento');
	}
	
}
