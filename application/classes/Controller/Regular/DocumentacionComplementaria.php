<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_DocumentacionComplementaria extends Controller_Regular_DocumentacionBasica {
	
	public $evet_code = 'DIC';
	
	protected $filename = 'documentacion_basica';
	
	protected $url = 'documentacionComplementaria';
	
	protected function check_estado()
	{
		parent::check_estado();
		
		$oFichaExpInteres = $this->oAcreditadoEvento->get_acreditado_etapa('REI');
		$oDocumentacionBasica = $this->oAcreditadoEvento->get_acreditado_etapa('DIB');
		
		/*if ($oFichaExpInteres->acet_estado == Model_AcreditadoEtapa::ESTADO_PENDIENTE)
		{
			$this->request->action('error');
			$this->error_message[] = "Debe terminar la {$oFichaExpInteres->oEventoEtapa->evet_nombre} para poder acceder a esta etapa";
		}
		
		if ($oDocumentacionBasica->acet_estado == Model_AcreditadoEtapa::ESTADO_PENDIENTE)
		{
			$this->request->action('error');
			$this->error_message[] = "Debe terminar la {$oDocumentacionBasica->oEventoEtapa->evet_nombre} para poder acceder a esta etapa";
		}*/
	}
	
}
