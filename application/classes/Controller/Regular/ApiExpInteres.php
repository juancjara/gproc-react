<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_ApiExpInteres extends Controller_Regular_ApiFicha {
	
	public $evet_code = 'REI';
	
	public $evef_code = 'FEI';
	
	/**
	 * 
	 */
	public function action_storeNodo()
	{
		//debug($this->request);
		//debug($this->request->body());
		$this->should_be_post();
		//sleep(10);
		
		$data = json_decode($this->request->body(), TRUE);

		$response = $this->save_nodo($data);
		
		$this->send_response($response);
	}
	
	/**
	 * 
	 */
	public function action_addCarrera()
	{
		$this->should_be_post();
		
		$data = json_decode($this->request->body(), TRUE);
		
		$response = array_map(function ($nodo)
			{
				return $this->save_nodo($nodo);
			}
			, $data);
		
		$this->send_response(['data' => $response]);
	}
	
	/**
	 * 
	 */
	public function action_removeCarrera()
	{
		$this->should_be_post();
		
		$data = json_decode($this->request->body(), TRUE);
		
		$response = $this->save_nodo($data['ins']);
		
		$this->remove_nodos([$data['carreraId']]);
		
		$this->send_response($response);
	}
	
	public function action_removeSede()
	{
		$this->should_be_post();
		
		$data = json_decode($this->request->body(), TRUE);
		
		$response = $this->save_nodo($data['ins']);
		
		$this->remove_nodos($data['ids']);
		
		$this->send_response($response);
	}
	
	private function save_nodo($data)
	{
		$oNodo = $this->oAcreditadoEtapaFicha
			->aNodo
			->where('id', '=', $data['dbId'])
			->find();
		
		// create or update
		$oNodo
			->values([
				'acef_id' => $this->oAcreditadoEtapaFicha->acef_id,
				'title' => implode('', $data['path']),
				'data' => json_encode($data['data']),
			])->save();

		return [
			'dbId' => $oNodo->id,
		];
	}
	
	private function remove_nodos($ids)
	{
		foreach ($ids as $id)
		{
			$this->oAcreditadoEtapaFicha
				->aNodo
				->where('id', '=', $id)
				->find()
				->delete();
		}
	}
	
}
