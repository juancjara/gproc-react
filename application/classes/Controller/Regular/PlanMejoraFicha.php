<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_PlanMejoraFicha extends Controller_Regular_PlanMejora {

	public $evef_code;
	
	/**
	 *
	 * @var  Model_AcreditadoEtapaFicha
	 */
	public $oAcreditadoEtapaFicha;
	
	protected function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('AcreditadoEtapaFicha');
		$this->middleware('AcreditadoEtapaFichaEstado');
	}
	
	protected function get_dimensiones($result)
	{
		$indicadores = [
			'dime_id' => ['dime', 'dime_codigo'],
			'fact_id' => ['fact', 'crit', 'fact_codigo'],
			'tipo' => [],
			'esta_id' => [],
		];
		
		$dimensiones = TreePowered::factory($result, $indicadores)->get(); //result por dimension - factor - estandar
		ksort($dimensiones);
		//debug($dimensiones);
		
		// Eliminamos factores y dimensiones sin estándares obligatorios
		// En todas las universidades, y en los institutos que estén en iniciación al cambio
		
		//$data = Arr::get($result, 0);
		//debug($data);
		
		//if (Arr::get($data, 'moda_id') == 1 OR Arr::get($data, 'tiin_id') == 1)
		{
			foreach ($dimensiones as $d => $dimension)
			{
				ksort($dimension['data']);
				
				$dimensiones[$d] = $dimension;
				/*foreach ($dimension['data'] as $f => $factor)
				{
					if ( ! isset($factor['data']['Obligatorios']))
					{
						//debug($dimension);
						//debug($factor);
						unset($dimensiones[$d]['data'][$f]);
						//debug($dimensiones);

						if ( ! count($dimensiones[$d]['data']))
						{
							unset($dimensiones[$d]);
						}
					}
				}*/
			}
		}
		
		return $dimensiones;
	}
	
}
