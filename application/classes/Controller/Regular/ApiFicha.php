<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_ApiFicha extends Controller_Regular_Api {
	
	public $evet_code;
	
	/**
	 *
	 * @var  Model_AcreditadoEtapa
	 */
	public $oAcreditadoEtapa;
	
	public $error_message = [];
	
	public $evef_code;
	
	/**
	 *
	 * @var  Model_AcreditadoEtapaFicha
	 */
	public $oAcreditadoEtapaFicha;
	
	public $action = 'save';
	
	protected function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('RegularAcreditadoEvento');
		
		$this->middleware('Autoevaluacion');
		$this->middleware('RegularCanAccessEvento');
		
		$this->middleware('AcreditadoSetupEtapas');
		$this->middleware('AcreditadoEtapa');
		$this->middleware('AcreditadoEtapaEstado');
		$this->middleware('AcreditadoEtapaFechaInicio');
		$this->middleware('AcreditadoEtapaFechaFinConProrroga');
		
		$this->middleware('AcreditadoEtapaFicha');
		$this->middleware('AcreditadoEtapaFichaEstado');
	}
	
	protected function handle_middleware_exceptions()
	{
		parent::handle_middleware_exceptions();
		
		if (count($this->error_message))
		{
			$this->send_error(new Middleware_Exception(array_shift($this->error_message)));
		}
		
		if ($this->action != 'save')
		{
			$this->send_error(new Middleware_Exception('La ficha no es editable'));
		}
	}
}
