<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_Etapas extends Controller_Regular {
	
	/**
	 *
	 * @var  Model_AcreditadoEvento
	 */
	public $oAcreditadoEvento;
	
	public $eventoInfo;
	
	/**
	 *
	 * @var  Model_Autoevaluacion
	 */
	public $oAutoevaluacion;
	
	protected function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('RegularAcreditadoEvento');
		
		$this->middleware('Autoevaluacion');
		$this->middleware('RegularCanAccessEvento');
		
		$this->middleware('AcreditadoSetupEtapas');
	}
	
	public function action_index()
	{
		$objeto = $this->userData + $this->eventoInfo;
		
		$aAcreditadoEtapa = Repository::factory('AcreditadoEtapa')
			->from($this->oAcreditadoEvento)
			->find_all();
		
		$fichas = Repository::factory('AcreditadoEtapaFicha')
			->from($this->oAcreditadoEvento)
			->order_by_evet_evef();
		
		$oEvento = $this->oAcreditadoEvento->oEvento;
		
		$view = Theme_View::factory('regular/etapas/list', compact('objeto', 'aAcreditadoEtapa', 'fichas', 'oEvento'));
		
		Theme::instance()
			->template
			->content = $view;
	}
	
}
