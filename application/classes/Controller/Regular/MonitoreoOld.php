<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_MonitoreoOld extends Controller_Regular_PlanMejoraFicha {
	
	public $current = FALSE;
	
	public $evef_code = 'MME';
	
	public function action_index()
	{
		$this->save();
		
		$filename = 'monitoreo_old';
		
		$view = Ficha::factory($filename)
			->set($this->get_vars())
			->get();

		Theme::instance()
			->template
			->content = $view;
	}
	
	public function save()
	{
		if ( ! $this->check_post()) return;
		
		//if ($this->request->method() == 'POST')
		{	
			$post = $this->request->post();
			$finish = Arr::get($post, 'finish');
			unset($post['finish']);
			
			if ($finish)
			{
				$this->oAcreditadoEtapaFicha
					->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO);
				
				$this->oAcreditadoEtapa->set_por_subir(['MME']);
			}
			
			$this->oAcreditadoEtapaFicha
				->set('acef_data', json_encode($post))
				->save();
			
			Session::instance()->set('info', 'Ficha guardada');
			
			$this->redirect('/monitoreoOld');
		}
	}
	
	public function action_pdf()
	{
		$filename = 'monitoreo_old';
		
		$view = Ficha::factory($filename)
			->set($this->get_vars())
			->get_pdf();
		
		$this->send_file($view, "{$filename}.pdf");
	}
	
	private function get_vars()
	{
		$action = $this->action;
		
		$objeto = $this->userData + $this->eventoInfo;
		
		$auto_id = $objeto['max_auto_id'];
		
		/*$result = SaesProxy::instance()
			->get('getAutoevaluacion', ['auto_id' => $auto_id]);*/
		
		$result = json_decode($this->oAutoevaluacion->auto_estandares, TRUE);
		
		$dimensiones = $this->get_dimensiones($result); //result por dimension - factor - estandar
		
		$data = json_decode($this->oAcreditadoEtapa->get_ficha('MPL')->acef_data, true);
		$data3 = json_decode($this->oAcreditadoEtapaFicha->acef_data, true);
		//debug($data3);
		
		//list($object, $factores_medios) = $this->get_object_and_factores($data, $auto_id);
		$medios = $this->get_medios($data, $auto_id);
		
		$oAcreditadoEtapaFicha = $this->oAcreditadoEtapaFicha;
		
		unset($result, $auto_id);
		
		return get_defined_vars();
	}
	
	private function get_medios($data, $auto_id)
	{
		/*$result = SaesProxy::instance()
			->get('getMedios', array('auto_id' => $auto_id));*/
		
		$result = json_decode($this->oAutoevaluacion->auto_fuentes, TRUE);
		//debug($result);
		
		$estandares = TreeArray::factory($result, ['esta_id', 'code_id'])->get();
		//debug($estandares);
		
		$medios = [];
		//debug(Arr::path($data, 'actividades', []));
		
		foreach (Arr::path($data, 'actividades', []) as $fact_codigo => $actividades)
		{
			if (Arr::path($data, "factores|{$fact_codigo}|hide", NULL, '|')) continue;
			
			$medios[$fact_codigo] = [];
			
			$lista_estandares = [];
			
			foreach ($actividades as $actividad => $detalles)
			{
				foreach (array_filter($detalles['estandares']) as $esta_id)
				{
					if ( ! in_array($esta_id, $lista_estandares))
					{
						$medios[$fact_codigo] = array_merge($medios[$fact_codigo], array_values($estandares[$esta_id]));
					}
					
					$lista_estandares[] = $esta_id;
				}
			}
			
			//debug($medios[$fact_codigo]);
		}
		
		//debug($medios);
		return $medios;
	}
	
	
	private function get_object_and_factores($data, $auto_id)
	{
		/*$result = SaesProxy::instance()
			->get('getAutoevaluacion', ['auto_id' => $objeto['max_auto_id']]);*/
		
		$result = json_decode($this->oAutoevaluacion->auto_estandares, TRUE);
		//debug($result);
		
		//$estandares = TreeArray::factory($result, ['esta_id', 'code_id'])->get();
		//debug($estandares);
		
		foreach (Arr::path($data, 'actividades', []) as $fact_codigo => $actividades)
		{
			$estandares_seleccionados = [];
			$medios_seleccionados = [];

			foreach ($actividades as $actividad => $estandares_sel)
			{
				foreach ($estandares_sel['estandares'] as $esta_id)
				{
					if ($esta_id != '')
					{
						foreach ($result as $key => $value)
						{
							if ($value['esta_id'] ==  $esta_id)
							{
								//$medios_seleccionados[] = "Estándar ".$value['dime_codigo'].".".$value['fact_codigo'].".".$value['esta_codigo']." - ".$value['concepto'];
								$medios_seleccionados[] = "Estándar ".$value['esta_codigo']." - ".$value['conc_codigo'].'. '.$value['concepto'];
							}
						}
						//$medios_seleccionados[] = "Otro Medio";
						$estandares_seleccionados[] = $medios_seleccionados;
					}
				}
			}

			$medios_seleccionados[999] = "Otro Medio de Verificación";
			$factores_medios[$fact_codigo] = $medios_seleccionados;
		}

		foreach ($factores_medios as $factor => $estandares)
		{
			$fact = [];

			$esta = array_map(
				function ($key) use ($estandares) { 
					return ['id'=> $key, 'text'=> $estandares[$key], 'count' => 0]; 
				}, array_keys($estandares));

			$fact[] = ['text' => $factor,  'children' => $esta];
			$object[] = ['text' => $factor,  'children' => $fact];
		}
		
		//debug2($object);
		//debug($factores_medios);
		
		return [$object, $factores_medios];
	}
	
}
