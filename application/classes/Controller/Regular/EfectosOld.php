<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_EfectosOld extends Controller_Regular_PlanMejoraFicha {
	
	public $current = FALSE;
	
	public $evef_code = 'MEE';
	
	public function action_index()
	{
		$this->save();
		
		$filename = 'efectos_old';
		
		$view = Ficha::factory($filename)
			->set($this->get_vars())
			->get();

		Theme::instance()
			->template
			->content = $view;
	}
	
	public function save()
	{
		if ( ! $this->check_post()) return;
		
		//if ($this->request->method() == 'POST')
		{
			$post = $this->request->post();
			$finish = Arr::get($post, 'finish');
			unset($post['finish']);
			
			if ($finish)
			{
				$this->oAcreditadoEtapaFicha
					->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO);
				
				$this->oAcreditadoEtapa->set_por_subir(['MEE']);
			}
			
			$this->oAcreditadoEtapaFicha
				->set('acef_data', json_encode($post))
				->save();
			
			Session::instance()->set('info', 'Ficha guardada');
			
			$this->redirect('/efectosOld');
		}
	}
	
	public function action_pdf()
	{
		$filename = 'efectos_old';
		
		$view = Ficha::factory($filename)
			->set($this->get_vars())
			->get_pdf();

		$this->send_file($view, "{$filename}.pdf");
	}
	
	public function get_vars()
	{
		$action = $this->action;
		
		$objeto = $this->userData + $this->eventoInfo;
		
		$auto_id = $objeto['max_auto_id'];
		
		/*$result = SaesProxy::instance()
			->get('getAutoevaluacion', ['auto_id' => $auto_id]);*/
		
		$result = json_decode($this->oAutoevaluacion->auto_estandares, TRUE);

		$dimensiones = $this->get_dimensiones($result); //result por dimension - factor - estandar

		$data = json_decode($this->oAcreditadoEtapa->get_ficha('MPL')->acef_data, true);
		$data4 = json_decode($this->oAcreditadoEtapaFicha->acef_data, true);
		
		$oAcreditadoEtapaFicha = $this->oAcreditadoEtapaFicha;
		
		return get_defined_vars();
	}
	
}
