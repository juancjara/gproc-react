<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
abstract class Controller_Regular_Api extends Controller {
	
	use Middleware_TraitAjax;
	
	/**
	 *
	 * @var  Model_User
	 */
	public $oUser;
	
	/**
	 *
	 * @var  Model_Acreditado
	 */
	public $oAcreditado;
	
	/**
	 * 
	 * @var  Model_AcreditadoEvento
	 */
	public $oAcreditadoEvento;
	
	public function before()
	{
		$this->handle_middleware_exceptions();
	}
	
	protected function set_middlewares()
	{
		$this->middleware('Auth');
		
		$this->middleware('ACL');
		$this->middleware('AcreditadoRegularOrEvaluador');
		//$this->middleware('RegularAcreditado');
	}
	
	protected function send_response($data = [])
	{
		$response = $data + [
			'status' => 'OK',
		];
		
		$this->response
			->headers('Access-Control-Allow-Origin', 'http://localhost:3000')
			->headers('Access-Control-Allow-Headers', 'Content-Type')
			->headers('Content-Type', 'application/json')
			->body(json_encode($response));
	}
	
	protected function send_error($e, $code = 500)
	{
		$response = [
			'statusText' => 'ERROR',
			'message' => $e->getMessage(),
		];
		
		$this->response
			->status($code)
			->headers('Access-Control-Allow-Origin', 'http://localhost:3000')
			->headers('Access-Control-Allow-Headers', 'Content-Type')
			->headers('Content-Type', 'application/json')
			->body(json_encode($response))
			->send_headers();
		
		echo $this->response->body();
		exit;
	}
	
	protected function should_be_post()
	{
		try
		{
			$this->middleware('Post');
		}
		catch (Middleware_Exception_Post $e)
		{
			$this->send_error($e);
		}
	}
	
}
