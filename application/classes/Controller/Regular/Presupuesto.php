<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_Presupuesto extends Controller_Regular_PlanMejoraFicha {
	
	public $evef_code = 'MPR';
	
	// @TEMP
	/*public $skipped = [
		//'AcreditadoEtapaFichaEstado',
		'AcreditadoEtapaPlanMejoraEstado',
		'AcreditadoEtapaEstado',
		'AcreditadoEtapaFechaInicio',
		'AcreditadoEtapaFechaFinConProrroga',
	];
	
	public function middleware($name)
	{
		if (in_array($name, $this->skipped))
		{
			return;
		}
		
		parent::middleware($name);
	}*/
	
	public function action_index()
	{
		$context = $this->get_context();
		
		$view_context = [
			'objeto' => $this->userData + $this->eventoInfo,
			'oAcreditadoEtapaFicha' => $this->oAcreditadoEtapaFicha,
			'action' => $this->action,
		];
		
		$view = Ficha::factory('presupuesto')
			->set([
				'context' => json_encode($context),
				'data' => json_encode($this->get_data()),
			])
			->set($view_context)
			->get();
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_reedit()
	{
		if ($this->action !== 'upload')
		{
			throw new Exception_Gproc('Ficha no setteada para subir');
		}
		
		$this->oAcreditadoEtapaFicha
			->set('acef_estado', $this->oAcreditadoEtapa->acet_estado)
			->save();
		
		$this->oAcreditadoEtapa->open_related_fichas($this->evef_code);
		
		$this->redirect('/presupuesto'.URL::query());
	}
	
	public function action_finish()
	{
		if ($this->action !== 'save')
		{
			throw new Exception_Gproc('Ficha no pendiente');
		}
		
		if ($this->request->method() === Request::POST)
		{
			if ($this->oAcreditadoEtapa->get_ficha('MPL')->is_atendido())
			{
				$this->oAcreditadoEtapaFicha->close();

				$this->oAcreditadoEtapa->set_por_subir(['MPL']);
			}
			else
			{
				Session::instance()->set('error', 'Debe finalizar la Matriz de Planificación');
			}
		}
		
		$this->redirect('/presupuesto'.URL::query());
	}
	
	public function action_pdf()
	{
		set_time_limit(0);
		
		$context = $this->get_context();
		
		$view_context = [
			'objeto' => $this->userData + $this->eventoInfo,
			'oAcreditadoEtapaFicha' => $this->oAcreditadoEtapaFicha,
			'action' => $this->action,
		];
		$filename = 'presupuesto';
		
		$header = ($this->oAcreditadoEtapaFicha->is_editable() OR $this->request->query('finish') != 1);
		
		$view = Ficha::factory($filename)
			->set($this->get_pdf_data($context))
			->set($view_context)
			->get_pdf($header);

		$this->send_file($view, "{$filename}.pdf");
	}
	
	public function action_excel()
	{
		
	}
	
	private function get_context()
	{
		$dimensiones = $this->oAutoevaluacion
			->get_dimensiones();
		
		$factores = $this->oAutoevaluacion
			->get_factores();
		
		$dim = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapa->get_ficha('MPL'))
			->get_dimensiones()
			->find_all()
			->as_json_array();
		
		$fac = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapa->get_ficha('MPL'))
			->get_factores()
			->filter_fac()
			->find_all()
			->as_json_array();
		
		$objetivos = A::f($dim)
			->map(function ($dimension) use ($dimensiones) {
				$dime_codigo = substr($dimension['title'], 3);
				return
					[
						'data' => json_decode($dimension['data'], TRUE),
					]
					+ $dimensiones[$dime_codigo]
					+ $dimension;
			})
			->value();
		
		$resultados = A::f($fac)
			->map(function ($factor) use ($factores) {
				$fact_codigo = substr($factor['title'], 3);
				return 
					[
						'data' => json_decode($factor['data'], TRUE),
					]
					+ $factores[$fact_codigo]
					+ $factor;
			})
			->value();
		
		$catalogo = Repository::factory('Catalogo')
			->get_catalogo_presupuesto($this->userData['tiin_id']);
		
		$viaticos = Repository::factory('Catalogo')
			->get_viaticos();
		
		$catalogo_titles = Repository::factory('Catalogo')
			->level_1()
			->find_all()
			->as_json_array();
		
		$catalogo_subtitles = Repository::factory('Catalogo')
			->level_2()
			->find_all()
			->as_json_array();
		
		$context = [
			'editable' => ($this->action === 'save'),
			'date' => date('Y-m-d'),
			'date_local' => dateformat(date('d \d\e F \d\e\l Y')),
		];
		
		return compact('catalogo_titles', 'catalogo_subtitles', 'catalogo', 'viaticos') + 
			['dimensiones' => $objetivos] +
			['factores' => $resultados] +
			['act' => $this->get_actividades()] +
			$context;
	}
	
	private function get_actividades()
	{
		$act = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapa->get_ficha('MPL'))
			->get_actividades()
			->filter_act()
			->select(DB::expr('nodo.id dbId'))
			->find_all()
			->as_json_array();
		
		return A::f($act)
			->map(function ($actividad) {
				return [
					'data' => json_decode($actividad['data'], TRUE),
				] + $actividad;
			})
			->value();
	}
	
	private function get_data()
	{
		$aNodo = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapaFicha)
			->get_subactividades()
			->filter_sub()
			->find_all()
			->as_array();
		
		$sub = A::f($aNodo)
			->map(function ($oNodo) {
				return $oNodo->decode();
			})
			->value();
		
		return [
			'fic' => $this->oAcreditadoEtapaFicha->get_nodo('fic')->decode(),
			'sub' => $sub,
		];
	}
	
	private function get_pdf_data($context)
	{
		$catalogo_dic = A::f($context['catalogo'])
			->concat([['id' => 'otro' ,
				'name' => 'Otro']])
			->to_dict('id')
			->value();
		$data = $this->get_data();
		
		$subactividades = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapaFicha)
			->get_subactividades()
			->filter_sub()
			->find_all()
			->as_array();
		
		$act = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapa->get_ficha('MPL'))
			->get_actividades()
			->filter_act()
			->get_act_totales($subactividades);
		
		$fac = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapa->get_ficha('MPL'))
			->get_factores()
			->filter_fac()
			->decode()
			->value();
		
		$dim = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapa->get_ficha('MPL'))
			->get_dimensiones()
			->decode()
			->value();
		
		$sub_grouped = A::f($subactividades)
			->map(function ($oNodo) {
				return $oNodo->decode();
			})
			->group_by(function ($sub) {
				return $sub['parent_id'];
			})
			->value();
		
		$actividades_with_sub = A::f($act)
			->map(function ($act) use ($sub_grouped) {
				return $act + [ 'subactividades' => Arr::get($sub_grouped, $act['dbId'], []) ];
			})
			->value();
			
		$actividades = A::f($actividades_with_sub)
			->group_by(function ($act) {
				return $act['parent_id'];
			})
			->value();
			
		$total_otros_fec = A::f($actividades_with_sub)
			->pluck('subactividades')
			->flatten()
			->filter(function($sub) {
				return Arr::path($sub, 'data.todo_seleccionado') AND
					strtolower(end($sub['data']['niveles'])) == 'otro' AND
					strtolower($sub['data']['financiamiento']) == 'fec';
			})
			->reduce(function($acc, $sub) {
				return numberformat($acc + $sub['data']['total'], 2);
			}, '0.00')
			->value();
		
		$resultados = A::f($fac)
			->map(function ($fac) use ($actividades) {
				return $fac + [ 'actividades' => Arr::get($actividades, $fac['dbId'], []) ];
			})
			->group_by(function ($fac) {
				return $fac['parent_id'];
			})
			->value();
		
		$objetivos = A::f($dim)
			->map(function ($dim) use ($resultados) {
				return $dim + [ 'resultados' => Arr::get($resultados, $dim['dbId'], []) ];
			})
			->filter(function ($dim) {
				return count($dim['resultados']);
			})
			->value();
		
		$totales = Repository::factory('Nodo')
			->get_plan_totales($subactividades);
		
		$otros_fec = numberformat(( $total_otros_fec / ((float)$totales['fec']['T'] ?:  1)) * 100, 2);
		$text_otros_fec = 'Otros FEC: S/.' . number_format($total_otros_fec, 2 , '.', ',') . ' (' . $otros_fec .  '%)';

		return compact('objetivos', 'data', 'totales', 'context', 'catalogo_dic', 'text_otros_fec');
	}
	
	public function after()
	{
		Theme::instance()
			->js('page', 'babel-polyfill', "/../frontend-tests/fichas/node_modules/babel-polyfill/dist/polyfill.min.js")
			->js('page', 'plupload', '/gproc_lte/js/plugins/plupload/plupload.full.min.js')
			->js('page', 'ficha_react', "/../frontend-tests/fichas/dist/presupuesto.js?v=3.0.1");
		
		parent::after();
	}
	
}
