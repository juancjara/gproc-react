<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_Eventos extends Controller_Regular {
	
	public function action_index()
	{
		$eventos = $this->userData['acev_list'];
		
		$aAcreditadoEvento = Repository::factory('AcreditadoEvento')
			->from_list($eventos)
			->find_all();
			//->with_autoevaluacion();
		
		$autoevaluaciones = Repository::factory('Autoevaluacion')
			->from_list($eventos)
			->with_porcentaje()
			->get_all();
		
		if (count($aAcreditadoEvento) !== count($autoevaluaciones))
		{
			$error = 'Error al obtener autoevaluaciones. Por favor ingrese nuevamente';
			Session::instance()->destroy();
			
			throw new Exception_Gproc($error);
		}
		
		$view = Theme_View::factory('regular/eventos/list')
			->set(compact('aAcreditadoEvento', 'eventos', 'autoevaluaciones'));
		
		Theme::instance()
			->template
			->content = $view;
	}
	
}
