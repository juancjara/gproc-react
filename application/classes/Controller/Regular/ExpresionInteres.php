<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_ExpresionInteres extends Controller_Regular_EventoEtapa {
	
	public $evet_code = 'REI';
	
	public $evef_code = 'FEI';
	
	/**
	 *
	 * @var  Model_AcreditadoEtapaFicha
	 */
	public $oAcreditadoEtapaFicha;
	
	protected function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('AcreditadoEtapaFicha');
		$this->middleware('AcreditadoEtapaFichaEstado');
	}
	
	public function action_index()
	{
		$context = $this->get_context();
		
		$view_context = [
			'objeto' => $this->userData + $this->eventoInfo,
			'oAcreditadoEtapaFicha' => $this->oAcreditadoEtapaFicha,
			'action' => $this->action,
			'institucional' => $context['institucional'],
		];
		
		$view = Ficha::factory('expresion_interes')
			->set([
				'context' => addslashes(json_encode($context)),
				'data' => addslashes(json_encode($this->get_data())),
			])
			->set($view_context)
			->get();
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_pdf()
	{
		set_time_limit(0);
		
		$view_context = [
			'objeto' => $this->userData + $this->eventoInfo,
			'oAcreditadoEtapaFicha' => $this->oAcreditadoEtapaFicha,
			'action' => $this->action,
		];
		
		$filename = 'expresion_interes';
		
		$header = $this->oAcreditadoEtapaFicha->is_editable() ? TRUE : FALSE;
		
		$view = Ficha::factory($filename)
			->set([
				'context' => $this->get_context(),
				'data' => $this->get_data(),
			])
			->set($view_context)
			->get_pdf($header);
		
		$this->send_file($view, "{$filename}.pdf");
	}
	
	public function action_data()
	{
		debug($this->get_data());
	}
	
	public function action_context()
	{
		debug($this->get_context());
	}
	
	public function action_upload()
	{
		if ($this->action !== 'save')
		{
			throw new Exception_Gproc('Ficha no pendiente');
		}
		
		if ($this->request->method() === Request::POST)
		{
			$this->oAcreditadoEtapaFicha
				->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO)
				//->set('acef_por_subir', Model_AcreditadoEtapaFicha::DOCUMENTO_POR_SUBIR)
				->save();
		}
		
		$this->redirect('/expresionInteres'.URL::query());
	}
	
	public function action_reedit()
	{
		if ($this->action !== 'upload')
		{
			throw new Exception_Gproc('Ficha no setteada para subir');
		}
		
		$this->oAcreditadoEtapaFicha
			->set('acef_estado', $this->oAcreditadoEtapa->acet_estado)
			->save();
		
		$this->redirect('/expresionInteres'.URL::query());
	}
	
	public function action_finish()
	{
		if ($this->action !== 'upload')
		{
			throw new Exception_Gproc('Ficha no setteada para subir');
		}
		
		if ($this->request->method() === Request::POST)
		{
			$identifier = "EI";
			
			$filename = $this->oAcreditadoEtapa->upload_documentacion($identifier, $_FILES['file']);
				
			if ($filename)
			{
				$this->oAcreditadoEtapaFicha
					->set('acef_path', $filename)
					->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO)
					->save();
				
				$this->oAcreditadoEtapa
					->set('acet_estado', Model_AcreditadoEtapa::ESTADO_ATENDIDO)
					->save();
				
				Session::instance()->set('info', 'Ficha guardada');
			}
			else
			{
				Session::instance()->set('error', 'No se pudo guardar el documento. Inténtelo nuevamente');
			}
		}
		
		$this->redirect('/expresionInteres'.URL::query());
	}
	
	private function get_context()
	{
		$saes_vars = SaesProxyV2::instance()
			->get('getExpIntData', array('acev_id' => $this->oAcreditadoEvento->pk()));
		unset($saes_vars['status']);
		//debug($saes_vars);
		
		$objeto = $this->userData + $this->eventoInfo;
		//debug($objeto);
		
		$objeto_context = Arr::extract($objeto, ['moda_nombre', 'inst_nombre', 'inst_gestion', 'carr_nombre']);
		
		$context = [
			'editable' => ($this->action === 'save'),
			'date' => date('Y-m-d'),
			'date_local' => dateformat(date('d \d\e F \d\e\l Y')),
		];
		
		return $context + $saes_vars + $objeto_context;
	}
	
	private $base_nodos = ['fic', 'ins'];
	
	private function get_data()
	{
		$aNodo = A::f($this->get_nodos_with_institucional())
			->map_dict(function ($oNodo) { return $oNodo->decode(); })
			->value();
		
		// init data
		$data = $this->build_data($aNodo);
		
		return $data ?: null;
	}
	
	private function get_nodos_with_institucional()
	{
		$aNodo = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapaFicha)
			->get_all();
		
		// Set nodo institucional
		if ( ! Arr::get($aNodo, 'ins'))
		{
			if ($nodoIns = Repository::factory('Nodo')->get_institucional($this))
			{
				$aNodo['ins'] = $nodoIns;
			}
		}
		
		return $aNodo;
	}
	
	private function build_data($aNodo)
	{
		$data = [];
		foreach ($aNodo as $key => $oNodo)
		{
			// ignore base nodos
			if (in_array($key, $this->base_nodos))
			{
				$data[$key] = $oNodo;
			}
			else
			{
				$data['car'][] = $oNodo;
			}
		}
		//debug($data);
		
		return $data;
	}
	
	public function after()
	{
		Theme::instance()
			->js('page', 'babel-polyfill', "/../frontend-tests/fichas/node_modules/babel-polyfill/dist/polyfill.min.js")
			->js('page', 'ficha', "/../frontend-tests/fichas/dist/expresionInteres.js?v=3.0.1");
		
		parent::after();
	}
	
}
