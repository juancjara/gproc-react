<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_CerrarPlanMejora extends Controller_Regular_PlanMejora {
	
	//public $current = FALSE;
	
	/**
	 *
	 * @var  Database_Result
	 */
	//private $aAcreditadoEtapaFicha;
	
	protected function set_middlewares()
	{
		parent::set_middlewares();
		
		//$this->middleware('AcreditadoEventoPMClose');
	}
	
	public function action_documentos()
	{
		$action = $this->action;
		
		//$objeto = Session::instance()->get('objeto');
		$objeto = $this->userData + $this->eventoInfo;
		
		$aAcreditadoEtapaFicha = $this->aAcreditadoEtapaFicha = $this->oAcreditadoEtapa
			->setup_fichas()
			->get_fichas();
			//debug($aAcreditadoEtapaFicha);
		
		$oAcreditadoEtapa = $this->oAcreditadoEtapa;
		
		$all_atendidas = $this->oAcreditadoEtapa->all_fichas_atendidas();
		
		$this->save();
		
		$view = Theme_View::factory('fichas/cerrar_plan_mejora')
			->set(get_defined_vars());
		
		Theme::instance()
			->css('page', 'ficha', "/gproc_lte/css/planes_mejora.css")
			->js('page', 'ficha', "/gproc_lte/js/planes_mejora.js")
			->template
			->content = $view;
	}
	
	public function save()
	{
		if ( ! $this->check_post()) return;
		
		if ( ! $this->oAcreditadoEtapa->is_editable())
		{
			Session::instance()->set('error', 'El Plan de Mejora ya ha sido cerrado');
			$this->redirect($this->request->referrer());
		}
		
		//if ($this->request->method() == 'POST')
		{
			//debug($this->request->post());
			$finish = $this->request->post('finish');
			
			$documentos = $this->request->post('documentos');
			//debug2($documentos);
			
			foreach ($this->aAcreditadoEtapaFicha as $oAcreditadoEtapaFicha)
			{
				//echo H_EOL.$oAcreditadoEtapaFicha->acef_id;
				if ($acef_path = Arr::get($documentos, $oAcreditadoEtapaFicha->acef_id))
				{
					//echo ' '.$acef_path;
					//continue;
					
					if ($finish)
					{
						$oAcreditadoEtapaFicha
							->set('acef_por_subir', Model_AcreditadoEtapaFicha::DOCUMENTO_SUBIDO);
					}
					
					$oAcreditadoEtapaFicha
						->set('acef_path', $acef_path)
						//->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO)
						//->set('acef_por_subir', Model_AcreditadoEtapaFicha::DOCUMENTO_SUBIDO)
						->save();
				}
			}
			//die();
			
			if ($finish)
			{
				if ($this->oAcreditadoEtapa->all_documentos_subidos())
				{
					//die('a');
					$this->oAcreditadoEtapa
						->set('acet_estado', Model_AcreditadoEtapa::ESTADO_ATENDIDO)
						->save();
					
					Session::instance()->set('info', 'Plan de Mejora actualizado');
				}
				else
				{
					//die('b');
					Session::instance()->set('error', 'Debe subir todos los documentos para finalizar');
				}
			}
			else
			{
				Session::instance()->set('info', 'Plan de Mejora actualizado');
			}
			
			$this->redirect($this->request->referrer());
		}
	}
	
	public function action_uploadDocumentacion()
	{
		$this->upload_documentacion();
	}
	
}
