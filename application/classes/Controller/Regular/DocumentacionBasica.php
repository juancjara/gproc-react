<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_DocumentacionBasica extends Controller_Regular_EventoEtapa {
	
	public $evet_code = 'DIB';
	
	protected $filename = 'documentacion_basica';
	
	protected $url = 'documentacionBasica';
	
	/**
	 *
	 * @var  Database_Result
	 */
	protected $aDocumentacion;
	
	public function before()
	{
		parent::before();
		
		$this->aDocumentacion = $this->oAcreditadoEtapa
			->setup_fichas()
			->get_fichas();
	}
	
	public function action_index()
	{
		$action = $this->action;
		
		$this->save();
		
		$objeto = $this->userData + $this->eventoInfo;
		
		$aDocumentacion =  $this->aDocumentacion;
		
		$filename = $this->filename;
		
		$view = Ficha::factory($filename)
			->set(compact('action', 'objeto', 'aDocumentacion'))
			->set('evet_code', $this->evet_code)
			->set('url', $this->url)
			->set('oAcreditadoEtapa', $this->oAcreditadoEtapa)
			->get();

		Theme::instance()
			->template
			->content = $view;
	}
	
	public function save()
	{
		if ( ! $this->check_post()) return;
		
		//if ($this->request->method() == 'POST')
		{
			$finish = $this->request->post('finish');
			
			$documentos = $this->request->post('documentos');
			
			foreach ($this->aDocumentacion as $oDocumentacion)
			{
				if ($acef_path = $documentos[$oDocumentacion->acef_id])
				{
					$oDocumentacion
						->set('acef_path', $acef_path)
						->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO)
						->save();
				}
			}
			
			if ($finish)
			{
				$this->oAcreditadoEtapa
					->set('acet_estado', Model_AcreditadoEtapa::ESTADO_ATENDIDO)
					->save();
			}
			
			Session::instance()->set('info', 'Ficha guardada');
			
			$this->redirect($this->url);
		}
	}
	
	public function action_uploadDocumentacion()
	{
		$this->upload_documentacion();
	}
	
}
