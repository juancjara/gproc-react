<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_Efectos extends Controller_Regular_PlanMejoraFicha {
	
	public $evef_code = 'MEE';
	
	// @TEMP
	/*public $skipped = [
		'AcreditadoEtapaFichaEstado',
		'AcreditadoEtapaPlanMejoraEstado',
		'AcreditadoEtapaEstado',
		'AcreditadoEtapaFechaInicio',
		'AcreditadoEtapaFechaFinConProrroga',
	];
	
	public function middleware($name)
	{
		if (in_array($name, $this->skipped))
		{
			return;
		}
		
		parent::middleware($name);
	}*/
	
	public function action_index()
	{
		//Session::instance()->set('info', 'Todavía no puede ingresar a esta ficha');
		//$this->redirect($this->request->referrer());
		
		$context = $this->get_context();
		
		$view_context = [
			'objeto' => $this->userData + $this->eventoInfo,
			'oAcreditadoEtapaFicha' => $this->oAcreditadoEtapaFicha,
			'action' => $this->action,
		];
		
		$view = Ficha::factory('efectos')
			->set([
				'context' => json_encode($context),
				'data' => json_encode($this->get_data($context)),
			])
			->set($view_context)
			->get();
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_reedit()
	{
		if ($this->action !== 'upload')
		{
			throw new Exception_Gproc('Ficha no setteada para subir');
		}
		
		$this->oAcreditadoEtapaFicha
			->set('acef_estado', $this->oAcreditadoEtapa->acet_estado)
			->save();
		
		$this->redirect('/efectos'.URL::query());
	}
	
	public function action_finish()
	{
		if ($this->action !== 'save')
		{
			throw new Exception_Gproc('Ficha no pendiente');
		}
		
		if ($this->request->method() === Request::POST)
		{
			$this->oAcreditadoEtapaFicha->close();
			
			/*$this->oAcreditadoEtapaFicha
				->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO)
				->save();

			$this->oAcreditadoEtapa->set_por_subir(['MEE']);*/
		}
		
		$this->redirect('/efectos'.URL::query());
	}
	
	public function action_pdf()
	{
		set_time_limit(0);
		
		$context = $this->get_context();
		
		$view_context = [
			'objeto' => $this->userData + $this->eventoInfo,
			'oAcreditadoEtapaFicha' => $this->oAcreditadoEtapaFicha,
			'action' => $this->action,
		];
		$filename = 'efectos';
		
		$header = ($this->oAcreditadoEtapaFicha->is_editable() OR $this->request->query('finish') != 1);
		
		$view = Ficha::factory($filename)
			->set($this->get_pdf_data($context))
			->set($view_context)
			->get_pdf($header);

		$this->send_file($view, "{$filename}.pdf");
	}
	
	private function get_context()
	{
		$dimensiones = $this->oAutoevaluacion
			->get_dimensiones();
		
		$factores = $this->oAutoevaluacion
			->get_factores();
		
		$dim = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapa->get_ficha('MPL'))
			->get_dimensiones()
			->find_all()
			->as_json_array();
		
		$fac = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapa->get_ficha('MPL'))
			->get_factores()
			->filter_fac()
			->find_all()
			->as_json_array();
		
		$objetivos = A::f($dim)
			->map(function ($dimension) use ($dimensiones) {
				$dime_codigo = substr($dimension['title'], 3);
				return
					[
						'data' => json_decode($dimension['data'], TRUE),
					]
					+ $dimensiones[$dime_codigo]
					+ $dimension;
			})
			->value();
		
		$resultados = A::f($fac)
			->map(function ($factor) use ($factores) {
				$fact_codigo = substr($factor['title'], 3);
				return 
					[
						'data' => json_decode($factor['data'], TRUE),
					]
					+ $factores[$fact_codigo]
					+ $factor;
			})
			->value();
		
		
		$data = [
			'dimensiones' => $objetivos,
			'factores' => $resultados,
		];
		
		$context = [
			'editable' => ($this->action === 'save'),
			'date' => date('Y-m-d'),
			'date_local' => dateformat(date('d \d\e F \d\e\l Y')),
		];
		
		return $data + $context;
	}
	
	public function get_pdf_data($context)
	{
		$data = $this->get_data($context);
		
		$efe_grouped = A::f($data['efe'])
			->group_by(function ($efe){
				return $efe['parent_id'];
			})
			->value();
		
		$fact_grouped = A::f($context['factores'])
			->map(function ($fac) use ($efe_grouped) {
				return $fac + ['efectos' => Arr::get($efe_grouped, $fac['id'], [])];
			})
			->group_by(function($fac){
				return $fac['parent_id'];
			})
			->value();

		$dimensiones = A::f($context['dimensiones'])
		->map(function ($dim) use ($fact_grouped) { 
			return $dim + ['resultados' => Arr::get($fact_grouped, $dim['id'], [])];
		})
		->filter(function($dim){
			return count($dim['resultados']);
		})
		->value();
		
		return compact('dimensiones','data');
	}
	
	private function get_data($context)
	{
		$efe = A::f($context['factores'])
			->map(function ($fac) {
				return $this->oAcreditadoEtapaFicha->get_nodo_by_parent($fac['id'],
					[
						'title' => 'efe',
						'parent_id' => $fac['id'],
					]);
			})
			->map(function ($oNodo) {
				return $oNodo->decode();
			})
			->value();
		
		return [
			'efe' => $efe,
			'fic' => $this->oAcreditadoEtapaFicha->get_nodo('fic')->decode(),
		];
	}
	
	public function after()
	{
		Theme::instance()
			->js('page', 'babel-polyfill', "/../frontend-tests/fichas/node_modules/babel-polyfill/dist/polyfill.min.js")
			->js('page', 'ficha_react', "/../frontend-tests/fichas/dist/efectos.js?v=3.0.1");
		
		parent::after();
	}
	
}
