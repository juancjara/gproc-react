<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_Planificacion extends Controller_Regular_PlanMejoraFicha {
	
	public $evef_code = 'MPL';
	
	// @TEMP
	/*public $skipped = [
		//'AcreditadoEtapaFichaEstado',
		'AcreditadoEtapaPlanMejoraEstado',
		'AcreditadoEtapaEstado',
		'AcreditadoEtapaFechaInicio',
		'AcreditadoEtapaFechaFinConProrroga',
	];
	
	public function middleware($name)
	{
		if (in_array($name, $this->skipped))
		{
			return;
		}
		
		parent::middleware($name);
	}*/
	
	public function action_index()
	{
		$context = $this->get_context();
		
		$view_context = [
			'objeto' => $this->userData + $this->eventoInfo,
			'oAcreditadoEtapaFicha' => $this->oAcreditadoEtapaFicha,
			'action' => $this->action,
		];
		
		$view = Ficha::factory('planificacion')
			->set([
				'context' => json_encode($context),
				'data' => json_encode($this->get_data($context)),
			])
			->set($view_context)
			->get();
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_reedit()
	{
		if ($this->action !== 'upload')
		{
			throw new Exception_Gproc('Ficha no setteada para subir');
		}
		
		$this->oAcreditadoEtapaFicha
			->set('acef_estado', $this->oAcreditadoEtapa->acet_estado)
			->save();
		
		$this->oAcreditadoEtapa->open_related_fichas($this->evef_code);
		
		$this->redirect('/planificacion'.URL::query());
	}
	
	public function action_finish()
	{
		if ($this->action !== 'save')
		{
			throw new Exception_Gproc('Ficha no pendiente');
		}
		
		if ($this->request->method() === Request::POST)
		{
			$this->oAcreditadoEtapaFicha->close();
			
			/*$this->oAcreditadoEtapaFicha
				->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO)
				->save();*/

			//$this->oAcreditadoEtapa->set_por_subir(['MPL', 'MPR', 'MCR', 'MME', 'MEE']);
		}
		
		$this->redirect('/planificacion'.URL::query());
	}
	
	public function action_pdf()
	{
		set_time_limit(0);
		
		$context = $this->get_context();
		
		$view_context = [
			'objeto' => $this->userData + $this->eventoInfo,
			'oAcreditadoEtapaFicha' => $this->oAcreditadoEtapaFicha,
			'action' => $this->action,
		];
		$filename = 'planificacion';
		
		$header = ($this->oAcreditadoEtapaFicha->is_editable() OR $this->request->query('finish') != 1);
		
		//debug($this->get_pdf_data($context));
		
		$view = Ficha::factory($filename)
			->set($this->get_pdf_data($context))
			->set($view_context)
			->get_pdf($header);
		
		$this->send_file($view, "{$filename}.pdf");
	}
	
	public function action_data()
	{
		debug($this->get_data($this->get_context()));
	}
	
	public function action_context()
	{
		debug($this->get_context());
	}
	
	private function get_context()
	{
		$dimensiones = $this->oAutoevaluacion
			->get_dimensiones();
		
		$factores = $this->oAutoevaluacion
			->get_factores();
		
		$estandares = $this->oAutoevaluacion
			->get_estandares();
		
		$matriz = [
			'estandares' => $estandares,
			'factores' => array_values($factores),
			'dimensiones' => array_values($dimensiones),
		];
		//debug($matriz);
		
		$catalogo_titles = Repository::factory('Catalogo')
			->level_1()
			->find_all()
			->as_json_array();
		
		$catalogo_subtitles = Repository::factory('Catalogo')
			->level_2()
			->find_all()
			->as_json_array();
		
		$subactividades = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapa->get_ficha('MPR'))
			->get_subactividades()
			->filter_sub()
			->find_all()
			->as_array();
		
		$context = [
			'canEdit' => ($this->action === 'save'),
			'canAddOrRemove' => ($this->action === 'save'),
			'date' => date('Y-m-d'),
			'date_local' => dateformat(date('d \d\e F \d\e\l Y')),
		];
		
		return compact('catalogo_titles', 'catalogo_subtitles', 'subactividades') + 
			$matriz +
			$context;
	}
	
	private function get_data($context)
	{
		$dim = A::f($context['dimensiones'])
			->map(function ($dimension) {
				return $this->oAcreditadoEtapaFicha->get_nodo('dim'.$dimension['dime_codigo']);
			})
			->to_dict(function ($oNodo) {
				return substr($oNodo->title, 3);
			})
			->map_dict(function ($oNodo) {
				return $oNodo->decode();
			})
			->value();
		
		$fac = A::f($context['factores'])
			->map(function ($factor) use ($dim) {
				return $this->oAcreditadoEtapaFicha->get_nodo(
					'fac'.$factor['fact_codigo'],
					['parent_id' => $dim[$factor['dime_codigo']]['dbId']]
				);
			})
			->map(function ($oNodo) {
				return $oNodo->decode();
			})
			->value();
		
		return [
			'fic' => $this->oAcreditadoEtapaFicha->get_nodo('fic')->decode(),
			'dim' => array_values($dim),
			'fac' => $fac,
			'act' => $this->get_nodos_actividades($context),
		];
	}
	
	private function get_nodos_actividades($context)
	{
		$aNodo = Repository::factory('Nodo')
			->from($this->oAcreditadoEtapaFicha)
			->get_actividades()
			->get_act_totales($context['subactividades']);
			//->find_all();
		
		//debug($aNodo);
		return $aNodo;
	}
	
	private function get_pdf_data($context)
	{
		$data = $this->get_data($context);
		
		$estandares = Arr::array_to_assoc($context['estandares'], 'esta_id');
		
		$actividades = A::f($data['act'])
			->group_by(function ($act) {
				return $act['parent_id'];
			})
			->value();
			
		$resultados = A::f($data['fac'])
			->map(function ($fac, $i) use ($context) {
				return $fac + [ 'factor' => $context['factores'][$i] ];
			})
			->filter(function ($fac) {
				return Arr::path($fac, 'data.hide') == FALSE;
			})
			->map(function ($fac) use ($actividades) {
				return $fac + [ 'actividades' => Arr::get($actividades, $fac['dbId'], []) ];
			})
			->group_by(function ($fac) {
				return $fac['parent_id'];
			})
			->value();
		
		$objetivos = A::f($data['dim'])
			->map(function ($dim, $i) use ($context) {
				return $dim + [ 'dimension' => $context['dimensiones'][$i] ];
			})
			->map(function ($dim) use ($resultados) {
				return $dim + [ 'resultados' => Arr::get($resultados, $dim['dbId'], []) ];
			})
			->filter(function ($dim) {
				return count($dim['resultados']);
			})
			->value();
		
		$totales = Repository::factory('Nodo')
			->get_plan_totales($context['subactividades']);
		//debug($totales);
		
		return compact('objetivos', 'estandares', 'data', 'totales', 'context');
	}
	
	public function after()
	{
		Theme::instance()
			->js('page', 'babel-polyfill', "/../frontend-tests/fichas/node_modules/babel-polyfill/dist/polyfill.min.js")
			->js('page', 'ficha_react', "/../frontend-tests/fichas/dist/planificacion.js?v=3.0.1");
		
		parent::after();
	}
	
}
