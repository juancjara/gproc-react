<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_Cronograma extends Controller_Regular_PlanMejoraFicha {
	
	public $evef_code = 'MCR';
	
	// @TEMP
	/*public $skipped = [
		'AcreditadoEtapaFichaEstado',
		'AcreditadoEtapaPlanMejoraEstado',
		'AcreditadoEtapaEstado',
		'AcreditadoEtapaFechaInicio',
		'AcreditadoEtapaFechaFinConProrroga',
	];
	
	public function middleware($name)
	{
		if (in_array($name, $this->skipped))
		{
			return;
		}
		
		parent::middleware($name);
	}*/
	
	public function action_index()
	{
		//Session::instance()->set('info', 'Todavía no puede ingresar a esta ficha');
		//$this->redirect($this->request->referrer());
		
		$context = $this->get_context();
		//debug($context);
		
		$view_context = [
			'objeto' => $this->userData + $this->eventoInfo,
			'oAcreditadoEtapaFicha' => $this->oAcreditadoEtapaFicha,
			'action' => $this->action,
		];
		
		$view = Ficha::factory('cronograma')
			->set([
				'context' => json_encode($context),
				'data' => json_encode($this->get_data($context)),
			])
			->set($view_context)
			->get();
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_reedit()
	{
		if ($this->action !== 'upload')
		{
			throw new Exception_Gproc('Ficha no setteada para subir');
		}
		
		$this->oAcreditadoEtapaFicha
			->set('acef_estado', $this->oAcreditadoEtapa->acet_estado)
			->save();
		
		$this->redirect('/cronograma'.URL::query());
	}
	
	public function action_finish()
	{
		if ($this->action !== 'save')
		{
			throw new Exception_Gproc('Ficha no pendiente');
		}
		
		if ($this->request->method() === Request::POST)
		{
			if ($this->oAcreditadoEtapa->get_ficha('MPR')->is_atendido())
			{
				$this->oAcreditadoEtapaFicha->close();
			}
			else
			{
				Session::instance()->set('error', 'Debe finalizar la Matriz de Presupuesto');
			}
		}
		
		$this->redirect('/cronograma'.URL::query());
	}
	
	public function action_pdf()
	{
		set_time_limit(0);
		
		$context = $this->get_context();
		
		$view_context = [
			'objeto' => $this->userData + $this->eventoInfo,
			'oAcreditadoEtapaFicha' => $this->oAcreditadoEtapaFicha,
			'action' => $this->action,
		];
		$filename = 'cronograma';
		
		$header = ($this->oAcreditadoEtapaFicha->is_editable() OR $this->request->query('finish') != 1);
		
		$view = Ficha::factory($filename)
			->set($this->get_pdf_data($context))
			->set($view_context)
			->get_pdf($header);

		$this->send_file($view, "{$filename}.pdf");
	}
	
	private function get_context()
	{
		$tiin_id = $this->oAcreditadoEvento->oEvento->tiin_id;
		
		$max_quincenas = ($tiin_id == Model_Acreditado::TIPO_UNIVERSIDAD) ? 18 : 12;
		
		$sub = A::f(Repository::factory('Nodo')
			->from($this->oAcreditadoEtapa->get_ficha('MPR'))
			->get_subactividades()
			->filter_sub()
			->decode()
			->value())
			->map(function ($sub){
				$sub['id'] = $sub['dbId'];
				return $sub;
			})
			->value();
		
		$act = A::f(Repository::factory('Nodo')
			->from($this->oAcreditadoEtapa->get_ficha('MPL'))
			->get_actividades()
			->filter_act()
			->decode()
			->value())
			->map(function ($act){
				$act['id'] = $act['dbId'];
				return $act;
			})
			->value();
		
		$context = [
			'subActividades' => $sub,
			'actividades' => $act,
			'editable' => ($this->action === 'save'),
			'date' => date('Y-m-d'),
			'date_local' => dateformat(date('d \d\e F \d\e\l Y')),
			'quincenas' => $max_quincenas,
		];
		
		return $context;
	}
	
	private function get_pdf_data($context)
	{
		$data = $this->get_data($context);
		
		$cro_grouped = A::f($data['cro'])
			->group_by(function ($cro) {
				return $cro['parent_id'];
			})
			->value();
	
		$subactividades = A::f($context['subActividades'])
			->map(function ($sub) use ($cro_grouped) {
				return $sub + ['cronograma' => Arr::get($cro_grouped, $sub['id'], [])];
			})
			->group_by(function($sub){
				return $sub['parent_id'];
			})
			->value();

		$act_grouped = A::f($context['actividades'])
			->map(function ($act) use ($subactividades) {
				return $act + ['subactividades' => Arr::get($subactividades, $act['id'], [])];
			})
			/*->filter(function ($act){
				return count($act['subactividades']);
			})*/
			->group_by(function ($act) {
				return $act['parent_id'];
			})
			->value();
			//debug($act_grouped);
		
		return compact('act_grouped', 'context', 'data');
	}
	
	private function get_data($context)
	{
		$cro = A::f($context['subActividades'])
			->map(function ($sub) {
				return $this->oAcreditadoEtapaFicha->get_nodo_by_parent($sub['id'],
					[
						'title' => 'Cro',
						'parent_id' => $sub['id'],
					]);
			})
			->map(function ($oNodo) {
				return $oNodo->decode();
			})
			->value();
			//debug($ind);
		
		return [
			'cro' => $cro,
			'fic' => $this->oAcreditadoEtapaFicha->get_nodo('fic')->decode(),
		];
	}
	
	public function after()
	{
		Theme::instance()
			->css('default', 'jquery-ui', '/gproc_lte/js/plugins/jquery-ui/jquery-ui.min.css')
			->js('page', 'babel-polyfill', "/../frontend-tests/fichas/node_modules/babel-polyfill/dist/polyfill.min.js")
			->js('page', 'ficha_react', "/../frontend-tests/fichas/dist/cronograma.js?v=3.0.1");
		
		parent::after();
	}
	
}
