<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_PlanificacionOld extends Controller_Regular_PlanMejoraFicha {
	
	public $current = FALSE;
	
	public $evef_code = 'MPL';
	
	public function action_index()
	{
		$this->save();
		
		$filename = 'planificacion_old';

		$view = Ficha::factory($filename)
			->set($this->get_vars())
			->get();

		Theme::instance()
			->template
			->content = $view;
	}
	
	public function save()
	{
		if ( ! $this->check_post()) return;
		
		if ( ! $this->request->post('dimensiones'))
		{
			Session::instance()
				->set('error', 'Ha ocurrido una excepción al procesar su información. Por favor contáctese con un administrador');
			
			$this->redirect('/planificacion');
		}
		
		//if ($this->request->method() == 'POST')
		{
			//debug($this->request->post());
			$post = $this->request->post();
			$finish = Arr::get($post, 'finish');
			unset($post['finish']);
			
			// Ficha Presupuesto
			
			$oFichaPresupuesto = $this->oAcreditadoEtapa->get_ficha('MPR');
			
			$data2 = json_decode($oFichaPresupuesto->acef_data, TRUE);
			$data2['totales'] = $post['totales'];
			$data2['porcentajes'] = $post['porcentajes'];
			
			unset($post['totales'], $post['porcentajes']);
			
			if ($finish)
			{
				$this->oAcreditadoEtapaFicha
					//->set('acef_por_subir', Model_AcreditadoEtapaFicha::DOCUMENTO_POR_SUBIR)
					->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO);
				
				$this->oAcreditadoEtapa->set_por_subir(['MPL', 'MPR', 'MCR', 'MME', 'MEE']);
			}
			
			$this->oAcreditadoEtapaFicha
				->set('acef_data', json_encode($post))
				->save();
			
			$oFichaPresupuesto
				->set('acef_data', json_encode($data2))
				->save();
			
			Session::instance()->set('info', 'Ficha guardada');
			
			$this->redirect('/planificacion');
		}
	}
	
	public function action_pdf()
	{
		$filename = 'planificacion_old';
		
		$view = Ficha::factory($filename)
			->set($this->get_vars())
			->get_pdf();

		$this->send_file($view, "{$filename}.pdf");
	}
	
	private function get_vars()
	{
		$action = $this->action;
	
		$objeto = $this->userData + $this->eventoInfo;
		
		$aGrupo = $this->oAcreditadoEvento->oEvento
			->get_grupos()
			->execute()
			->as_array();
		
		$grupos_header = array_merge($aGrupo, 
			[
				['grup_id' => 'super', 'grup_short' => 'Supervisión y monitoreo'],
				['grup_id' => 'total', 'grup_short' => 'Total'],
			]);
		
		$grupos = array_merge($aGrupo, 
			[
				['grup_id' => 'total', 'grup_short' => 'Total actividad'],
			]);
		
		/*$result = SaesProxy::instance()
			->get('getAutoevaluacion', ['auto_id' => $objeto['max_auto_id']]);*/
		
		$result = json_decode($this->oAutoevaluacion->auto_estandares, TRUE);
		
		$dimensiones = $this->get_dimensiones($result);
		//debug($dimensiones);
		
		$grupos_estandares = $this->get_grupos_estandares($result);
		
		$lista_estandares = TreeArray::factory($result, ['esta_id'])->get();
		//debug($lista_estandares);
		
		//debug($this->oAcreditadoEtapaFicha);
		$data = json_decode($this->oAcreditadoEtapaFicha->acef_data, TRUE);
		$data2 = json_decode($this->oAcreditadoEtapa->get_ficha('MPR')->acef_data, TRUE);
		$data5 = json_decode($this->oAcreditadoEtapa->get_ficha('MCR')->acef_data, TRUE);
		//debug($data);
		
		$oAcreditadoEtapaFicha = $this->oAcreditadoEtapaFicha;
		
		return get_defined_vars();
	}
	
	private function get_grupos_estandares($result)
	{
		// Estandares agrupados en opcionales y obligatorios
		$opcionales_obligatorios = TreeArray::factory($result, ['tipo', 'fact_id', 'esta_id'])->get(); //estandares agrupados en opcionales y obligatorios
		
		// Modificación para llenar select estándares
		$grupos_estandares = [];
		foreach ($opcionales_obligatorios as $type => $factor)
		{
			$fact = [];
			foreach ($factor as $fact_id => $estandar)
			{
				$esta = array_map(function ($key) use ($estandar) { 
					return ['id'=> $key, 'text' => $estandar[$key]['esta'], 'count' => 0]; }, array_keys($estandar));
				
				$fact[] = ['text' => $fact_id, 'children' => $esta];
			}
			
			$grupos_estandares[] = ['text' => $type, 'children' => $fact];
		}
		
		return $grupos_estandares;
	}
	
	protected function _set_template_blocks()
	{
		parent::_set_template_blocks();
		
		if ($this->auto_render)
		{
			$theme = Theme::instance();
			
			$theme->js('default', 'shim', '/gproc_lte/js/plugins/react/thirdparty/es5-shim.min.js');
			$theme->js('default', 'sham', '/gproc_lte/js/plugins/react/thirdparty/es5-sham.min.js');
			$theme->js('default', 'react', '/gproc_lte/js/plugins/react/build/react-with-addons.js');
			$theme->js('default', 'reactdom', '/gproc_lte/js/plugins/react/build/react-dom.js');
			$theme->js('default', 'jsx', '/gproc_lte/js/plugins/react/build/JSXTransformer.js');
			$theme->extra('<script type="text/jsx;harmony=true" src="/media/gproc_lte/js/react/components.jsx"></script>');
			$theme->extra('<script type="text/jsx;harmony=true" src="/media/gproc_lte/js/react/extra.jsx"></script>');
			$theme->extra('<script type="text/jsx;harmony=true" src="/media/gproc_lte/js/react/planificacion.jsx"></script>');
		}
	}
	
}
