<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_PresupuestoOld extends Controller_Regular_PlanMejoraFicha {
	
	public $current = FALSE;
	
	public $evef_code = 'MPR';
	
	// @TEMP
	/*public $skipped = [
		'AcreditadoEtapaFichaEstado',
		'AcreditadoEtapaPlanMejoraEstado',
		'AcreditadoEtapaEstado',
		'AcreditadoEtapaFechaInicio',
		'AcreditadoEtapaFechaFinConProrroga',
	];
	
	public function middleware($name)
	{
		if (in_array($name, $this->skipped))
		{
			return;
		}
		
		parent::middleware($name);
	}*/
	
	public function action_index()
	{
		$this->save();

		$filename = 'presupuesto_old';
		
		$view = Ficha::factory($filename)
			->set($this->get_vars())
			->set('totalFEC', $this->getTotalFEC()) // TEMP
			->get();
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	// TEMP
	private function getTotalFEC()
	{
		//$totales = [];
		
		$ids = ['10', '11'];
		
		foreach ($ids as $id)
		{
			$filename = APPPATH."RPMtotalesFEC_{$id}.txt";
			
			if (file_exists($filename))
			{
				$data = json_decode(file_get_contents($filename), TRUE);
				
				//$totales += $data;
				if ($totalFEC = Arr::get($data, $this->oAcreditadoEtapa->acev_id))
				{
					return $totalFEC;
				}
			}
		}
		
		return NULL;
	}
	
	public function save()
	{
		if ( ! $this->check_post()) return;
		
		if ( ! $this->request->post('actividades'))
		{
			Session::instance()
				->set('error', 'Ha ocurrido una excepción al procesar su información. Por favor contáctese con un administrador');
			
			$this->redirect('/presupuestoOld');
		}
		
		//if ($this->request->method() == 'POST')
		{
			$post = $this->request->post();
			//debug($post);
			$finish = Arr::get($post, 'finish');
			unset($post['finish']);
			
			if ($finish)
			{
				$this->oAcreditadoEtapaFicha
					->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO);
				
				$this->oAcreditadoEtapa->set_por_subir(['MPL', 'MPR']);
			}
			
			$this->oAcreditadoEtapaFicha
				->set('acef_data', json_encode($post))
				->save();
			
			Session::instance()->set('info', 'Ficha guardada');
			
			$this->redirect('/presupuestoOld');
		}
	}
	
	public function action_pdf()
	{
		$filename = 'presupuesto_old';
		
		$view = Ficha::factory($filename)
			->set($this->get_vars())
			->get_pdf();

		$this->send_file($view, "{$filename}.pdf");
	}
	
	public function action_excel()
	{
		$filename = 'presupuesto_old';
		
		$view = Ficha::factory($filename)
			->set($this->get_vars())
			->get_excel();

		$this->send_file($view, "{$filename}.xls");
	}
	
	private function get_vars()
	{
		$action = $this->action;
		//debug($action);
		$objeto = $this->userData + $this->eventoInfo;
		
		$oEvento = $this->oAcreditadoEvento->oEvento;
		
		// =====================================================================
		
		$aGrupo = $oEvento
			->get_grupos()
			->execute()
			->as_array();
		
		$aGasto = $oEvento
			->get_gastos()
			->execute()
			->as_array();
		
		$aTipo = $oEvento
			->get_tipos()
			->execute()
			->as_array();
		//debug($aTipo);
		
		$aItem = $oEvento
			->get_items()
			->execute()
			->as_array();
		
		// =====================================================================
		
		$grupos_header = array_merge($aGrupo, 
			[
				['grup_id' => 'super', 'grup_short' => 'Supervisión y monitoreo'],
				['grup_id' => 'total', 'grup_short' => 'Total'],
			]);
		
		$grupos = array_merge($aGrupo, 
			[
				['grup_id' => 'total', 'grup_short' => 'Total actividad'],
			]);
		
		$gastos = $oEvento
			->get_gastos()
			->execute()
			->as_array('gael_id', 'gael_nombre');
		
		$tipos = $oEvento
			->get_tipos()
			->execute()
			->as_array('tiga_id', 'tiga_nombre') + [999 => 'Otros'];
		
		$items = $oEvento
			->get_items()
			->execute()
			->as_array('itga_id', 'itga_nombre') + [999 => 'Otros'];
		
		/*$get_tipos = function($gael_id) use ($aTipo) {
			return array_filter($aTipo, 
				function ($tipo) use ($gael_id) {
					return $tipo['gael_id'] == $gael_id;
				});
		};*/
		
		// =====================================================================
		
		$financiamiento = $oEvento::$tipo_financiamiento;
		
		/*$result = SaesProxy::instance()
			->get('getAutoevaluacion', ['auto_id' => $objeto['max_auto_id']]);*/
		
		$result = json_decode($this->oAutoevaluacion->auto_estandares, TRUE);
		
		$dimensiones = $this->get_dimensiones($result);
		
		$data = json_decode($this->oAcreditadoEtapa->get_ficha('MPL')->acef_data, true);
		$data2 = json_decode($this->oAcreditadoEtapaFicha->acef_data, true);
		
		$oAcreditadoEtapaFicha = $this->oAcreditadoEtapaFicha;
		
		return get_defined_vars(); //+ ['get_tipos' => $get_tipos];
	}
	
	public function action_upload_especificacion()
	{
		//$this->oAcreditado;
		//$this->oAutoevaluacion;
		$this->auto_render = FALSE;
		
		$identifier = $this->request->post('identifier');
		
		if ($this->request->post_max_size_exceeded())
		{
			throw new Exception_Gproc('File too big');
		}
		
		$filename = $this->oAcreditadoEtapa
			->upload_documentacion($identifier, $_FILES['file']);
		
		if ($filename === FALSE)
		{
			throw new Exception_Gproc('File could not be saved');
		}
		
		$this->response->body($filename);
	}
	
	protected function _set_template_blocks()
	{
		parent::_set_template_blocks();
		
		if ($this->auto_render)
		{
			$theme = Theme::instance();
			
			$theme->js('default', 'plupload', '/gproc_lte/js/plugins/plupload/plupload.full.min.js');
			$theme->js('default', 'shim', '/gproc_lte/js/plugins/react/thirdparty/es5-shim.min.js');
			$theme->js('default', 'sham', '/gproc_lte/js/plugins/react/thirdparty/es5-sham.min.js');
			$theme->js('default', 'react', '/gproc_lte/js/plugins/react/build/react-with-addons.min.js');
			$theme->js('default', 'reactdom', '/gproc_lte/js/plugins/react/build/react-dom.min.js');
			$theme->js('default', 'jsx', '/gproc_lte/js/plugins/react/build/JSXTransformer.js');
			$theme->extra('<script type="text/jsx;harmony=true" src="/media/gproc_lte/js/react/components.jsx"></script>');
			$theme->extra('<script type="text/jsx;harmony=true" src="/media/gproc_lte/js/react/presupuesto.jsx"></script>');
		}
	}
	
}
