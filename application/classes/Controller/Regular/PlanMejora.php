<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_PlanMejora extends Controller_Regular_EventoEtapa {
	
	public $evet_code = 'RPM';
	
	public $error_message = [];
	
	protected function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('AcreditadoEtapaPlanMejoraEstado');
	}
	
}
