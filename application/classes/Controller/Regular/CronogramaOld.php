<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_CronogramaOld extends Controller_Regular_PlanMejoraFicha {
	
	public $current = FALSE;
	
	public $evef_code = 'MCR';
	
	// @TEMP
	/*public $skipped = [
		'AcreditadoEtapaFichaEstado',
		'AcreditadoEtapaPlanMejoraEstado',
		'AcreditadoEtapaEstado',
		'AcreditadoEtapaFechaInicio',
		'AcreditadoEtapaFechaFinConProrroga',
	];
	
	public function middleware($name)
	{
		if (in_array($name, $this->skipped))
		{
			return;
		}
		
		parent::middleware($name);
	}*/
	
	public function action_index()
	{
		$this->save();
		
		$filename = 'cronograma_old';
		
		$view = Ficha::factory($filename)
			->set($this->get_vars())
			->get();

		Theme::instance()
			->template
			->content = $view;
	}
	
	public function save()
	{
		if ( ! $this->check_post()) return;
		
		//if ($this->request->method() == 'POST')
		{
			$post = $this->request->post();
			$finish = Arr::get($post, 'finish');
			unset($post['finish']);
			
			if ($finish)
			{
				$this->oAcreditadoEtapaFicha
					->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO);
				
				$this->oAcreditadoEtapa->set_por_subir(['MPL', 'MCR']);
			}
			
			$this->oAcreditadoEtapaFicha
				->set('acef_data', json_encode($post))
				->save();
			
			Session::instance()->set('info', 'Ficha guardada');
			
			$this->redirect('/cronogramaOld');
		}
	}
	
	public function action_pdf()
	{
		$filename = 'cronograma_old';
		
		$view = Ficha::factory($filename)
			->set($this->get_vars())
			->get_pdf();

		$this->send_file($view, "{$filename}.pdf");
	}
	
	public function get_vars()
	{
		$action = $this->action;
		
		$objeto = $this->userData + $this->eventoInfo;
		
		$max_quincenas = ($objeto['tiin_id'] == Model_Acreditado::TIPO_UNIVERSIDAD) ? 18 : 12;
		
		$data = json_decode($this->oAcreditadoEtapa->get_ficha('MPL')->acef_data, true);
		$data5 = json_decode($this->oAcreditadoEtapaFicha->acef_data, true);
		
		$oAcreditadoEtapaFicha = $this->oAcreditadoEtapaFicha;
		
		return get_defined_vars();
	}
	
	protected function _set_template_blocks()
	{
		parent::_set_template_blocks();
		
		if ($this->auto_render)
		{
			$theme = Theme::instance();
			
			$theme->css('default', 'jquery-ui', '/gproc_lte/js/plugins/jquery-ui/jquery-ui.min.css');
		}
	}
	
}
