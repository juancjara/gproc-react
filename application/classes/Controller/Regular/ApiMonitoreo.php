<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_ApiMonitoreo extends Controller_Regular_ApiFicha {
	
	public $evet_code = 'RPM';
	
	public $evef_code = 'MME';
	
	// @TEMP
	public $skipped = [
		'AcreditadoEtapaFichaEstado',
		'AcreditadoEtapaPlanMejoraEstado',
		'AcreditadoEtapaEstado',
		'AcreditadoEtapaFechaInicio',
		'AcreditadoEtapaFechaFinConProrroga',
	];
	
	public function middleware($name)
	{
		if (in_array($name, $this->skipped))
		{
			return;
		}
		
		parent::middleware($name);
	}
	
	/**
	 * 
	 */
	public function action_storeNodos()
	{
		$this->should_be_post();
		//sleep(10);
		
		$nodos = json_decode($this->request->body(), TRUE);
		
		$response = [];
		foreach ($nodos as $nodo)
		{
			$response[] = $this->save_nodo($nodo);
		}
		
		$this->send_response(['ids' => $response]);
	}
	
	/**
	 * 
	 */
	public function action_addIndicador()
	{
		$this->should_be_post();
		//sleep(10);
		
		$nodo = json_decode($this->request->body(), TRUE);
		
		$response = $this->save_nodo($nodo, TRUE);
		
		$this->send_response($response);
	}
	
	/**
	 * 
	 */
	public function action_removeIndicador()
	{
		$this->should_be_post();
		
		$data = json_decode($this->request->body(), TRUE);
		
		$this->remove_nodos([$data['dbId']]);
		
		$this->send_response();
	}
	
	private function save_nodo($data, $orden = FALSE)
	{
		$oNodo = $this->oAcreditadoEtapaFicha
			->aNodo
			->where('id', '=', $data['dbId'])
			->find();
		
		$values = [
			'acef_id' => $this->oAcreditadoEtapaFicha->acef_id,
			'title' => implode('', $data['path']),
			'data' => json_encode($data['data']),
			'parent_id' => Arr::get($data, 'parent_id'),
		];
		
		/*if ($orden)
		{
			$values += ['orden' => Arr::get($data, 'orden')];
		}*/
		
		// create or update
		$oNodo->values($values)->save();
		
		return [
			'dbId' => $oNodo->id,
		];
	}
	
	private function remove_nodos($ids)
	{
		foreach ($ids as $id)
		{
			$this->oAcreditadoEtapaFicha
				->aNodo
				->where('id', '=', $id)
				->find()
				->delete_if_exists();
				//->delete();
		}
	}
	
}
