<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Regular_ExpresionInteresOld extends Controller_Regular_EventoEtapa {
	
	public $current = FALSE;
	
	public $evet_code = 'REI';
	
	public $evef_code = 'FEI';
	
	/**
	 *
	 * @var  Model_AcreditadoEtapaFicha
	 */
	public $oAcreditadoEtapaFicha;
	
	// @TEMP
	// Since it's for even 10, 11 we allow access to autos < min
	public $skipped = [
		'RegularCanAccessEvento',
	];
	
	public function middleware($name)
	{
		if (in_array($name, $this->skipped))
		{
			return;
		}
		
		parent::middleware($name);
	}
	
	public function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('AcreditadoEtapaFicha');
		$this->middleware('AcreditadoEtapaFichaEstado');
	}
	
	public function action_index()
	{
		$saes_vars = SaesProxyV2::instance()
			->get('getExpIntData', array('acev_id' => $this->oAcreditadoEvento->pk()));

		$action = $this->action;
		//debug($action);
		
		//call_user_method($action, $this);
		
		//$data = json_decode($this->oAcreditadoEtapaFicha->acef_data, TRUE);
		//debug($data);
		$view = Ficha::factory('expresion_interes_old')
			->set($saes_vars)
			->set($this->get_vars())
			//->set('action', $action)
			->get();
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	private function view()
	{
		//
	}
	
	/*private function save()
	{
		if ( ! $this->check_post()) return;
		
		//if ($this->request->method() == HTTP_Request::POST)
		{
			$post = $this->request->post();
			$finish = Arr::get($post, 'finish');
			unset($post['finish']);
			
			$this->oAcreditadoEtapaFicha
				->set('acef_data', json_encode($post))
				->save();
			
			Session::instance()->set('info', 'Ficha guardada');
			
			if ($finish)
			{
				$this->redirect($this->request->uri().URL::query(['action' => 'finish']));
			}
		}
	}*/
	
	/*private function finish()
	{
		if ( ! $this->check_post()) return;
		
		//$oAcreditadoEtapa = $this->oAcreditadoEtapa;
		
		//if ($this->request->method() == 'POST')
		{
			//debug2($this->request->post());
			//debug($_FILES);
			
			$identifier = "EI";
			
			$filename = $this->oAcreditadoEtapa->upload_documentacion($identifier, $_FILES['file']);
				
			if ($filename)
			{
				$this->oAcreditadoEtapaFicha
					->set('acef_path', $filename)
					->set('acef_estado', Model_AcreditadoEtapaFicha::ESTADO_ATENDIDO)
					->save();
				
				$this->oAcreditadoEtapa
					->set('acet_estado', Model_AcreditadoEtapa::ESTADO_ATENDIDO)
					->save();
				
				Session::instance()->set('info', 'Ficha guardada');
				
				$this->redirect('/expresionInteres');
			}

			Session::instance()->set('error', 'No se pudo guardar el documento. Inténtelo nuevamente');
		}
	}*/
	
	public function action_pdf()
	{
		set_time_limit(0);
		
		$vars = SaesProxyV2::instance()
			->get('getExpIntData', array('acev_id' => $this->oAcreditadoEvento->pk()));

		//$data = json_decode($this->oAcreditadoEtapaFicha->acef_data, TRUE);
		
		$filename = 'expresion_interes_old';
		
		$view = Ficha::factory($filename)
			->set($vars)
			->set($this->get_vars())
			->get_pdf();
		
		$this->send_file($view, "{$filename}.pdf");
	}
	
	private function get_vars()
	{
		$action = $this->action;
		//debug($action);
		
		//$objeto = Session::instance()->get('objeto');
		$objeto = $this->userData + $this->eventoInfo;
		
		$data = json_decode($this->oAcreditadoEtapaFicha->acef_data, TRUE);
		
		$oAcreditadoEtapaFicha = $this->oAcreditadoEtapaFicha;
		
		return get_defined_vars();
	}
	
}
