<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
abstract class Controller_Regular_EventoEtapa extends Controller_Regular {
	
	public $current = TRUE;
	
	public $evet_code;
	
	/**
	 *
	 * @var  Model_AcreditadoEvento
	 */
	public $oAcreditadoEvento;
	
	public $eventoInfo;
	
	/**
	 *
	 * @var  Model_Autoevaluacion
	 */
	public $oAutoevaluacion;
	
	/**
	 *
	 * @var  Model_AcreditadoEtapa
	 */
	public $oAcreditadoEtapa;
	
	public $action = 'save';
	
	protected function _check_initial()
	{
		if ( ! $this->request->is_initial())
		{
			$_GET = $this->request->query();
		}
	}
	
	protected function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('RegularAcreditadoEvento');
		$this->middleware('RedirectOldEvento');
		
		$this->middleware('Autoevaluacion');
		$this->middleware('RegularCanAccessEvento');
		
		$this->middleware('AcreditadoSetupEtapas');
		$this->middleware('AcreditadoEtapa');
		$this->middleware('AcreditadoEtapaEstado');
		$this->middleware('AcreditadoEtapaFechaInicio');
		$this->middleware('AcreditadoEtapaFechaFinConProrroga');
	}
	
	public function action_error()
	{
		$objeto = $this->userData + $this->eventoInfo;
		
		$oEventoEtapa = $this->oAcreditadoEtapa->oEventoEtapa;
		
		$errors = $this->error_message;
		
		$view = Theme_View::factory('regular/etapas/noaccess', get_defined_vars());

		Theme::instance()
			->template
			->content = $view;
	}
	
	protected function check_post()
	{
		if ($this->request->method() != HTTP_Request::POST)
			return FALSE;
		
		if ($this->oAcreditadoEvento->acev_id != $this->request->query('acev_id'))
			return FALSE;
		
		return TRUE;
	}
	
	protected function send_file($view, $filename)
	{
		$this->response
			->body($view);
		
		if ($this->request->is_initial())
		{
			$this->response
				->send_file(TRUE, $filename, array('inline' => (bool) $this->request->query('view')));
		}
		
		$this->auto_render = FALSE;
	}
	
	protected function upload_documentacion()
	{
		$identifier = $this->request->post('identifier');
		
		if ($this->request->post_max_size_exceeded())
		{
			throw new Exception_Gproc('File too big');
		}
		
		$filename = $this->oAcreditadoEtapa
			->upload_documentacion($identifier, $_FILES['file']);
		
		if ($filename === FALSE)
		{
			throw new Exception_Gproc('File could not be saved');
		}
		
		$this->response->body($filename);
	}
	
}
