<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_File extends Controller {

	public function before()
	{
		parent::before();

		$this->_check_authentication();
	}

	protected function _check_authentication()
	{
		if ( ! Auth::instance()->logged_in())
		{
			die('Please login');
		}
	}
	
	public function action_get()
	{
		
		$file = $this->request->param('id');
		
		$filename = APPPATH.$file;
		//debug($filename);
		if ( ! file_exists($filename))
		{
			die('File does not exist');
		}
		
		//$this->response->headers('Content-Type', File::mime($filename));
		//$this->response->body(file_get_contents($filename));
		
		//Response::factory()
			//->send_file($filename);
		
		Response::factory()
			->send_file($filename, NULL, array('inline' => (bool) $this->request->query('view')));
	}
	
} // End Controller_Template
