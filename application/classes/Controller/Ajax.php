<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Ajax extends Controller {

	public function action_getInstitution()
	{
		$val = $this->request->param('id');
		
		$query = ORM::factory('Institucion')
			//->add_name()
			->where('inst_estado', '=', 'activo')
			->where('tiin_id', '=', $val);
		
		$result = $query
			->order_by('name')
			->find_all()
			->as_array('inst_id', 'name');
		
		$html = $this->_build($result, 'Seleccione institución');
		
		$this->response->body($html);
	}
	
	public function action_getObac()
	{
		$inst_id = $this->request->param('id');
		
		$aObjeto = Model_ObjetoAcreditacion::get_obac_from_inst($inst_id, FALSE);
		
		$html = $this->_build_obac($aObjeto, 'Seleccione objeto de acreditación');
		
		$this->response->body($html);
	}
	
	public function action_getCareerObac()
	{
		$id = $this->request->param('id');
		
		try
		{
			list($inst_id, $obac_id) = explode('-', $id);
			
			$aCarrera = Model_Carrera::get_from_inst_and_obac($inst_id, $obac_id);
			//debug('foo');
		}
		catch(Exception $e)
		{
			$aCarrera = array();
		}
		
		$html = $this->_build($aCarrera, 'Seleccione carrera');
		
		$this->response->body($html);
	}
	
	public function action_getCareerByTipo()
	{
		$val = $this->request->param('id');
		
		$tiin_id = ORM::factory('Institucion', $val)->tiin_id;
		
		$carreras = DB::select('carr_nombre')
			->from('carrera')
			->where('inst_id', '=', $val);
		
		$result = ORM::factory('Carrera')
			->join('institucion')
				->using('inst_id')
			->where('carr_estado', '=', 'activo')
			->where('tiin_id', '=', $tiin_id)
			->where('carr_nombre', 'not in', $carreras)
			->order_by('carr_nombre')
			->group_by('carr_nombre')
			->find_all()
			->as_array('carr_id', 'carr_nombre');
		
		$html = $this->_build($result, 'Seleccione carrera');
		
		$html .= HTML::tag('option', 'Otra...', array('value' => '0'));
		
		$this->response->body($html);
	}
	
	public function action_getCareer()
	{
		$val = $this->request->param('id');
		
		$result = ORM::factory('Carrera')
			//->join('institucion_carrera')
				//->on('carrera.carr_id', '=', 'institucion_carrera.carr_id')
			->where('carr_estado', '=', 'activo')
			->where('inst_id', '=', $val)
			->order_by('carr_nombre')
			->find_all()
			->as_array('carr_id', 'carr_nombre');
		
		$html = $this->_build($result, 'Seleccione carrera');
		
		$this->response->body($html);
	}
	
	public function action_getCareerType()
	{
		$val = $this->request->param('id');
		
		$result = ORM::factory('ObjetoAcreditacion')
			->where('obac_estado', '=', 'activo')
			->where('tiin_id', '=', $val)
			->find_all()
			->as_array('obac_id', 'obac_nombre');
		
		$html = $this->_build($result, 'Tipo de Institución/Carrera');
		
		$this->response->body($html);
	}
	
	public function action_getDimension()
	{
		$val = $this->request->param('id');
		$val = explode('-', $val);
		
		$result = ORM::factory('Dimension')
			->where('dime_estado', '=', 'activo')
			->where('obac_id', 'IN', $val)
			->find_all();
			//->as_array('fact_id', 'fact_titulo');
		
		$html = $this->_build_title($result);
		
		$this->response->body($html);
	}
	
	public function action_getFactor()
	{
		$val = $this->request->param('id');
		$val = explode('-', $val);
		
		$result = ORM::factory('Factor')
			->where('fact_estado', '=', 'activo')
			->where('dime_id', 'IN', $val)
			->find_all();
			//->as_array('fact_id', 'fact_titulo');
		
		$html = $this->_build_title($result);
		
		$this->response->body($html);
	}
	
	public function action_getCriterion()
	{
		$val = $this->request->param('id');
		$val = explode('-', $val);
		
		$result = ORM::factory('Criterio')
			->where('crit_estado', '=', 'activo')
			->where('fact_id', 'IN', $val)
			->find_all();
			//->as_array('crit_id', 'crit_titulo');
		
		$html = $this->_build_title($result);
		
		$this->response->body($html);
	}
	
	public function action_getStandard()
	{
		$val = $this->request->param('id');
		$val = explode('-', $val);
		
		$result = ORM::factory('Estandar')
			->where('esta_estado', '=', 'activo')
			->where('crit_id', 'IN', $val)
			->find_all();
			//->as_array('esta_id', 'esta_titulo');
		
		$html = $this->_build_title($result);
		
		$this->response->body($html);
	}
	
	public function action_getStandardByModaId()
	{
		$oAcreditado = Session::instance()->get('oAcreditado');
	
		$moda_id = $oAcreditado ? $oAcreditado->moda_id : NULL;
		
		$val = $this->request->param('id');
		$val = explode('-', $val);
		
		$result = ORM::factory('Estandar')
			->where('esta_estado', '=', Model_Saes::STATUS_ACTIVO)
			->where('crit_id', 'IN', $val)
			->where('moda_id', '<=', $moda_id)
			->find_all();
			//->as_array('esta_id', 'esta_titulo');
		
		$html = $this->_build_title($result);
		
		$this->response->body($html);
	}
	
	public function action_getConcept()
	{
		$val = $this->request->param('id');
		$val = explode('-', $val);
		
		$result = ORM::factory('Concepto')
			->where('conc_estado', '=', 'activo')
			->where('esta_id', 'IN', $val)
			->find_all();
			//->as_array('esta_id', 'esta_titulo');
		
		$html = View::factory('admin/cumplimiento/concepts')
			->set('aConcepto', $result);
		
		$this->response->body($html);
	}
	
	public function action_get_obac_dimensiones()
	{
		$obac_id = $this->request->param('id');
		
		$aDimension = Model_ObjetoAcreditacion::get_dimensiones($obac_id);
		
		$html = $this->_build($aDimension, '--');
		
		$this->response->body($html);
	}
	
	public function action_get_obac_factores()
	{
		$obac_id = $this->request->param('id');
		
		$aFactor = Model_ObjetoAcreditacion::get_factores($obac_id);
		
		$html = $this->_build($aFactor, '--');
		
		$this->response->body($html);
	}
	
	public function action_get_obac_criterios()
	{
		$obac_id = $this->request->param('id');
		
		$aCriterio = Model_ObjetoAcreditacion::get_criterios($obac_id);
		
		$html = $this->_build($aCriterio, '--');
		
		$this->response->body($html);
	}
	
	public function action_get_obac_estandares()
	{
		$obac_id = $this->request->param('id');
		
		$aEstandar = Model_ObjetoAcreditacion::get_estandares($obac_id);
		
		$html = $this->_build($aEstandar, '--');
		
		$this->response->body($html);
	}
	
	protected function action_get_even_etapa()
	{
		$even_id = $this->request->param('id');
		
		$aEtapa = Model_EventoEtapa::get_etapas_gproc($even_id);
		
		$html = $this->_build($aEtapa, '--');

		$this->response->body($html);
	}
	
	protected function _build($result, $default = NULL)
	{
		$html = $this->request->query('multiple') ? '' : HTML::default_option($default);
		
		foreach ($result as $key => $val)
		{
			$html .= HTML::tag('option', $val, array('value' => $key));
		}
		return $html;
	}
	
	protected function _build_obac($aObjeto, $default = NULL)
	{
		$html = HTML::default_option($default);
		
		foreach ($aObjeto as $oObjeto)
		{
			$html .= HTML::tag('option', $oObjeto->obac_nombre, 
				array('value' => $oObjeto->inst_obac, 'data-tipo' => $oObjeto->obac_tipo_acreditacion));
		}
		return $html;
	}
	
	protected function _build_title($result)
	{
		$html = $this->request->query('multiple') ? '' : HTML::default_option();
		
		foreach ($result as $object)
		{
			$html .= HTML::tag('option', $object->title(), array('value' => $object->pk(), 'title' => $object->titulo()));
		}
		return $html;
	}
	
	
} // End Controller_Template
