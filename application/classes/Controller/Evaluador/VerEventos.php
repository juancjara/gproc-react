<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Evaluador_VerEventos extends Controller_Evaluador {
	
	public function action_index()
	{
		$aEvento = $this->oUser->get_eventos();
		//debug($aEvento);
		
		$view = Theme_View::factory('evaluador/vereventos/index', compact('aEvento'))
			->set('eventos', $this->eventos);
		
		Theme::instance()
			->template
			->content = $view;
		
		Breadcrumb::add('Ver eventos');
	}
	
}