<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Evaluador_VerComites extends Controller_Evaluador {
	
	/**
	 *
	 * @var  Model_Evento
	 */
	public $oEvento;
	
	public $eventoInfo;
	
	protected function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('EvaluadorEvento');
	}
	
	public function action_index()
	{
		$even_id = $this->oEvento->even_id;
		
		$comites = SaesProxyV2::instance()
			->get('getComites', array('even_id' => $even_id));
		//debug($comites);
		
		$aAcreditadoEvento = Repository::factory('AcreditadoEvento')
			->from_even($even_id)
			->filter_with_etapas()
			->filter_from($comites);
		//debug($aAcreditadoEvento);
		
		$aEventoEtapa = Repository::factory('EventoEtapa')
			->from_even($even_id)
			->find_all();
		
		$etapas = Repository::factory('AcreditadoEtapa')
			->from_even($even_id)
			->order_by_acev_evet();	
			//->order_for($aAcreditadoEvento);
		
		$totales = Repository::factory('AcreditadoEtapa')
			->from_even($even_id)
			->get_totales();
		//debug($totales);
		
		$view = Theme_View::factory('evaluador/vercomites/index')
			->set(compact('aEventoEtapa', 'aAcreditadoEvento', 'comites', 'etapas', 'totales'))
			->set('evento', $this->eventoInfo);
		
		Theme::instance()
			->template
			->content = $view;
		
		Breadcrumb::add('Ver eventos', '/evaluador/verEventos');
		Breadcrumb::add($this->get_evento_nombre($this->eventoInfo));
	}
	
	public function get_report($even_id)
	{
		$arr = [];
		foreach (Model_AcreditadoEtapa::$estados as $index => $val)
		{
			$and = "AND acet.acet_estado = {$index}";
			
			$query = "SELECT count(*) FROM acreditado_evento acev
			LEFT JOIN acreditado_etapa acet ON acev.acev_id = acet.acev_id WHERE acev.even_id = 10 {$and}";
			//debug($query);
			
			$result = DB::query(Database::SELECT, $query)
			->execute()->as_array();
			
			array_push($arr,$result);
			//debug($result);
			//debug($arr);
		}
		
		//debug($arr);
			
	}
	
	public function action_setTotal()
	{
		$id = $this->request->param('id');
		
		$status = Model_AcreditadoEtapa::ESTADO_ATENDIDO;
		$ids = '('.implode(',', [581]).')';
		
		$aAcreditadoEtapaFicha = ORM::factory('AcreditadoEtapaFicha')
			->with('oEventoEtapaFicha')
			->with('oAcreditadoEtapa:oEventoEtapa')
			->with('oAcreditadoEvento')
			->select('acev_id')
			->where('oAcreditadoEtapa:oEventoEtapa.even_id', '=', $id)
			->where('evet_code', '=', 'RPM')
			->where('evef_code', '=', 'MPR')
			//->where('acet_estado', '=', Model_AcreditadoEtapa::ESTADO_ATENDIDO)
			->where(DB::expr(''), '', DB::expr("(acet_estado = {$status} or acev_id in {$ids})"))
			->find_all();
		
		$totales = [];
		
		foreach ($aAcreditadoEtapaFicha as $i => $oAcreditadoEtapaFicha)
		{
			$data = json_decode($oAcreditadoEtapaFicha->acef_data, TRUE);
			
			$totalFEC = Arr::path($data, 'totales.fec.total');
			
			echo H_EOL.($i + 1).'. '.$oAcreditadoEtapaFicha->acev_id.' - '.$totalFEC;
			
			$totales[$oAcreditadoEtapaFicha->acev_id] = $totalFEC;
		}
		
		echo Form::open().Form::submit(NULL, 'Save').Form::close();
		
		if ($this->request->method() == 'POST')
		{
			$filename = APPPATH."RPMtotalesFEC_{$id}.txt";
			file_put_contents($filename, json_encode($totales));
			
			debug('Log creado: '.$filename);
		}
		
		die();
	}
	
	private function get_evento_nombre($evento)
	{
		return Text::limit_words($evento['even_nombre'], 2)." ({$evento['tiin_nombre']})";
	}
	
}
