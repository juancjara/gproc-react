<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Evaluador_ReportesFec extends Controller_Evaluador {
	
	public function action_index()
	{
		$eventos = [18, 19];
		
		$acreditados_evento = A::f($eventos)
			->map(function ($evento) {
				return $this->get_acreditados($evento);
			})
			->value();
			//debug($acreditados_evento);
		
		$view = Theme_View::factory('evaluador/reportesfec/index')
			->set(compact('acreditados_evento'));
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_pdf()
	{
		$eventos = [18, 19];
		
		$acreditados_evento = A::f($eventos)
			->map(function ($evento) {
				return $this->get_acreditados($evento);
			})
			->value();
			//debug($acreditados_evento);
		
		$view = Theme_View::factory('evaluador/reportesfec/index')
			->set(compact('acreditados_evento'));
		
		$pdf = PDF::factory()
			->set('view', $view)
			->get();
		
		$this->response
			->body($pdf);
		
		$this->response
			->send_file(TRUE, 'reporteFec.pdf', array('inline' => (bool) $this->request->query('view')));
	}
	
	public function action_otros()
	{
		$eventos = [18, 19];
		
		$acreditados_evento = A::f($eventos)
			->map(function ($evento) {
				return $this->get_acreditados($evento);
			})
			->value();
			//debug($acreditados_evento);
		
		$view = Theme_View::factory('evaluador/reportesfec/otros')
			->set(compact('acreditados_evento'));
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_otrosPdf()
	{
		$eventos = [18, 19];
		
		$acreditados_evento = A::f($eventos)
			->map(function ($evento) {
				return $this->get_acreditados($evento);
			})
			->value();
			//debug($acreditados_evento);
		
		$view = Theme_View::factory('evaluador/reportesfec/otros')
			->set(compact('acreditados_evento'));
		
		$pdf = PDF::factory()
			->set('view', $view)
			->get();
		
		$this->response
			->body($pdf);
		
		$this->response
			->send_file(TRUE, 'reporteFecOtros.pdf', array('inline' => (bool) $this->request->query('view')));
	}
	
	private function get_acreditados($evento)
	{
		$comites = A::f(SaesProxyV2::instance()
			->get('getComites', ['even_id' => $evento]))
			->slice(1)
			->to_dict('acev_id')
			->value();
		
		$aFicha = $this->get_fichas($evento);
		//debug($fichas);
		
		$fichas = A::f($aFicha)
			->map(function ($oFicha) use ($comites) {
				
				$subactividades = $this->get_subactividades($oFicha);
				
				return [
					'comite' => $comites[$oFicha->acev_id],
					'totales' => $this->get_total($subactividades),
					'otros' => $this->get_total_otros($subactividades),
				];
			})
			->value();
			//debug($fichas);
		
		$totales = //Repository::factory('Nodo')
			//->get_summary_totales($fichas);
			$this->sum_totales($fichas);
		
		$otros = //Repository::factory('Nodo')
			//->get_summary_otros($fichas);
			$this->sum_otros($fichas);
		
		return compact('fichas', 'totales', 'otros');
	}
	
	private function get_fichas($evento)
	{
		return  ORM::factory('AcreditadoEtapaFicha')
			->with('oEventoEtapaFicha')
			->with('oAcreditadoEtapa:oAcreditadoEvento')
			->select('oAcreditadoEtapa.acev_id')
			->where('even_id', '=', $evento)
			->where('evef_code', '=', 'MPR')
			
			->join('nodo')->on('acreditadoetapaficha.acef_id', '=', 'nodo.acef_id')
			->group_by('acreditadoetapaficha.acef_id')
			
			->find_all()
			->as_array();
	}
	
	private function get_subactividades($oFicha)
	{
		return Repository::factory('Nodo')
			->from($oFicha)
			->get_subactividades()
			->filter_sub()
			->find_all()
			->as_array();
	}
	
	private function get_total($subactividades)
	{
		return Repository::factory('Nodo')
			->get_plan_totales($subactividades);
	}
	
	private function get_total_otros($subactividades)
	{
		$subactividades = A::f($subactividades)
			->filter(function ($oNodo) {
				$sub = $oNodo->decode();
				
				//debug2($sub['data']);
				return Arr::path($sub, 'data.todo_seleccionado') AND
					strtolower(end($sub['data']['niveles'])) == 'otro' AND
					strtolower($sub['data']['financiamiento']) == 'fec';
			})
			->value();
			//debug($subactividades);
		
		return Repository::factory('Nodo')
			->get_plan_totales($subactividades);
	}
	
	private function sum_totales($fichas, $col = 'totales')
	{
		$struct = Repository::factory('Nodo')
			->get_struct_totales();
			//debug($struct);
		
		foreach ($fichas as $ficha)
		{
			//debug($ficha);
			foreach ($struct as $fin => $totales)
			{
				foreach (array_keys($totales) as $total)
				{
					$struct[$fin][$total] += $ficha[$col][$fin][$total];
				}
			}
		}
		
		return $struct;
	}
	
	private function sum_otros($fichas)
	{
		return $this->sum_totales($fichas, 'otros');
	}
	
}
