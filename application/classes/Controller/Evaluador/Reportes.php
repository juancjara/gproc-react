<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Evaluador_Reportes extends Controller_Evaluador {
	
	private $id;
	
	private $evento;
	
	private $comites;
	
	private $ids = [540, 809, 810];
	
	public function before()
	{
		parent::before();
		
		$this->id = $this->request->param('id');
		
		$query = SaesProxy::instance()
			->get('getComites', array('even_id' => $this->id));
			//debug($query);
			
		$this->evento = $query['evento'];
		$this->comites = $query['users'];
	}
	
	public function after()
	{
		//debug($this->evento);
		
		Theme::instance()
			->template
			->content
			->evento = $this->evento;
		
		parent::after();
	}
	
	public function action_evento()
	{
		//debug($this->evento);
		
		$reportes = ['computo', 'totales', 'items', 'detalle'];
		
		$view = Theme_View::factory('evaluador/reportes/index', compact('reportes'));
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_ver()
	{
		$tipo = $this->request->query('tipo');
		
		$view = Report::factory($tipo, ['id' => $this->id])
			->get();
		
		Theme::instance()
			->template
			->content = $view;
		
		//echo $x; die();
		//debug($x);
	}
	
	public function action_excel()
	{
		$tipo = $this->request->query('tipo');
		
		$theme = Theme::instance();
		
		$theme->css(array());
		
		$theme->template = Theme_View::factory('reportes/template');
		$theme->template->content = $view = Report::factory($tipo, ['id' => $this->id])->get_excel();
		
		Response::factory()
			->body($theme->template)
			->send_file(TRUE, "$tipo.xls");
	}
	
	public function action_computo()
	{
		$aAcreditadoEtapaFicha = $this->get_fichas();
		//echo Debug::dump($aAcreditadoEtapaFicha, 5000);
		//die();
		//debug($aAcreditadoEtapaFicha);
		
		$gastos = [11, 12];
		
		$items = ORM::factory('Evento', $this->id)
			->get_items()
			->select('*', DB::expr('0 total'), DB::expr('0 cantidad'))
			->where('gael_id', 'in', $gastos)
			->execute()
			->as_array('itga_id');
		
		$items['H'] = [
			'gael_nombre' => 'Hardware',
			'itga_nombre' => 'Otros',
			'cantidad' => 0,
			'total' => 0,
		];
		$items['S'] = [
			'gael_nombre' => 'Software',
			'itga_nombre' => 'Otros',
			'cantidad' => 0,
			'total' => 0,
		];
		
		//debug($items);
		
		foreach ($aAcreditadoEtapaFicha as $oAcreditadoEtapaFicha)
		{
			$data = json_decode($oAcreditadoEtapaFicha->acef_data, TRUE);
			//debug2($data);
			
			$per = $data['totales']['fec'][3] / $data['totales']['fec']['total'];
			
			//debug2($data['totales'], numberformat($per, 2));
			
			if ($per > 0.6)
			{
				continue;
			}
			
			foreach ($data['actividades'] as $actividades)
			{
				//debug2($actividades);
				
				foreach ($actividades as $actividad)
				{
					foreach ($actividad['subactividades'] as $subactividad)
					{
						//debug($subactividad);
						
						$gasto = $subactividad['gasto'];
						$financiamiento = $subactividad['financiamiento'];
						
						if (in_array($gasto, $gastos) AND $financiamiento == 1)
						{
							$item = Arr::get($subactividad, 'item', 999);
							
							if ($item == 999)
							{
								$item = $gasto == 11 ? 'H' : 'S';
							}
							
							$subtotal = Arr::get($subactividad, 'subtotal');
							$cantidad = Arr::get($subactividad, 'cantidad');
							
							//$items
							//debug2($item);
							//$items[]
							$items[$item]['total'] += $subtotal;
							$items[$item]['cantidad'] += $cantidad;
							$items[$item]['costounitario'] = Arr::get($subactividad, 'costounitario');
						}
					}
				}
			}
		}
		
		//debug($subactividades);
		//debug($items);
		
		$view = Theme_View::factory('evaluador/reportes/evento')
			->set('evento', $this->evento)
			->set('items', array_values($items));
		
		Theme::instance()
			->template
			->content = $view;
		
		Breadcrumb::add('Ver eventos', '/evaluador/verEventos');
		Breadcrumb::add($this->get_evento_nombre($this->evento));
	}
	
	public function action_totales()
	{
		$aAcreditadoEtapaFicha = $this->get_fichas();
		$comites = $this->comites;
		//echo Debug::dump($aAcreditadoEtapaFicha, 5000);
		//die();
		//debug($aAcreditadoEtapaFicha);
		
		//debug($this->comites);
		
		$grupos = ORM::factory('Evento', $this->id)
			->get_grupos()
			->execute()
			->as_array('grup_id');
		
		$grupos['super'] = ['grup_short' => 'Supervisión'];
		$grupos['total'] = ['grup_short' => 'Total'];
		//debug($grupos);
		
		foreach ($comites as $key => $data)
		{
			$acev_id = $data['acev_id'];
			
			$oAcreditadoEtapaFicha = Arr::get($aAcreditadoEtapaFicha, $acev_id);
			
			//if (in_array($acev_id, array_keys($aAcreditadoEtapa)))
			if ($oAcreditadoEtapaFicha)
			{
				$data = json_decode($oAcreditadoEtapaFicha->acef_data, TRUE);
				//debug($data);
				
				$per = $data['totales']['fec'][3] / $data['totales']['fec']['total'];
				
				if ($per > 0.6)
				{
					unset($comites[$key]);
					continue;
				}
				
				$comites[$key]['totales'] = $data['totales'];
			}
			else
			{
				unset($comites[$key]);
			}
		}
		
		//debug($subactividades);
		//debug($items);
		
		$view = Theme_View::factory('evaluador/reportes/totales', compact('grupos'))
			->set('comites', array_values($comites))
			->set('evento', $this->evento);
		
		Theme::instance()
			->template
			->content = $view;
		
		Breadcrumb::add('Ver eventos', '/evaluador/verEventos');
		Breadcrumb::add($this->get_evento_nombre($this->evento));
	}
	
	public function action_items()
	{
		$aAcreditadoEtapaFicha = $this->get_fichas();
		//echo Debug::dump($aAcreditadoEtapaFicha, 5000);
		//die();
		//debug($aAcreditadoEtapaFicha);
		
		$oEvento = ORM::factory('Evento', $this->id);
		
		$grupos = $oEvento
			->get_grupos()
			->execute()
			->as_array('grup_id', 'grup_nombre');
		
		$gastos = $oEvento
			->get_gastos()
			->execute()
			->as_array('gael_id', 'gael_nombre');
			//debug($gastos);
		
		$tipos = $oEvento
			->get_tipos()
			->execute()
			->as_array('tiga_id', 'tiga_nombre');
		
		$items = ORM::factory('Evento', $this->id)
			->get_items()
			->select('*', DB::expr('0 total'), DB::expr('0 cantidad'))
			->execute()
			->as_array('itga_id');
		
		foreach ($aAcreditadoEtapaFicha as $oAcreditadoEtapaFicha)
		{
			$data = json_decode($oAcreditadoEtapaFicha->acef_data, TRUE);
			//debug2($data);
			
			$per = $data['totales']['fec'][3] / $data['totales']['fec']['total'];
			
			//debug2($data['totales'], numberformat($per, 2));
			
			if ($per > 0.6)
			{
				continue;
			}
			
			foreach ($data['actividades'] as $actividades)
			{
				//debug2($actividades);
				
				foreach ($actividades as $actividad)
				{
					foreach ($actividad['subactividades'] as $subactividad)
					{
						//debug($subactividad);
						
						$gasto = $subactividad['gasto'];
						$financiamiento = $subactividad['financiamiento'];
						
						if ($financiamiento == 1)
						{
							$item = Arr::get($subactividad, 'item');
							$tipo = Arr::get($subactividad, 'tipo');
							$gasto = Arr::get($subactividad, 'gasto');
							$grupo = Arr::get($subactividad, 'grupo');
							$subtotal = Arr::get($subactividad, 'subtotal');
							$cantidad = Arr::get($subactividad, 'cantidad');
							
							// Solo se da en acev 616, ver archivo de incidencias
							if ( ! $gasto)
							{
								continue;
								//debug2($oAcreditadoEtapaFicha->oAcreditadoEtapa->acev_id);
								//debug2($data);
							}
							
							if ($item == 999)
							{
								$items[] = [
									'grup_id' => Arr::get($grupos, $grupo, $grupo),
									'tiga_id' =>  Arr::get($tipos, $tipo, 'Otros'),
									'gael_id' => Arr::get($gastos, $gasto, $gasto),
									'itga_nombre' => Arr::get($subactividad, 'itemOtro'),
									'total' => $subtotal,
									'costounitario' => Arr::get($subactividad, 'costounitario'),
									'cantidad' => Arr::get($subactividad, 'cantidad'),
								];
							}
							elseif ($item)
							{
								$items[$item]['total'] += $subtotal;
								$items[$item]['cantidad'] += $cantidad;
								$items[$item]['costounitario'] = Arr::get($subactividad, 'costounitario');
							}
							elseif ($tipo == 999)
							{
								//debug2($subactividad);
								$items[] = [
									'grup_id' => Arr::get($grupos, $grupo, $grupo),
									'tiga_id' => Arr::get($tipos, $tipo, 'Otros'),
									'gael_id' =>  Arr::get($gastos, $gasto, $gasto),
									'itga_nombre' => Arr::get($subactividad, 'tipoOtro'),
									'total' => $subtotal,
									'costounitario' => Arr::get($subactividad, 'costounitario'),
									'cantidad' => Arr::get($subactividad, 'cantidad'),
								];
							}
							elseif ($grupo == 1)
							{
								$items[] = [
									'grup_id' => Arr::get($grupos, $grupo, $grupo),
									'tiga_id' => Arr::get($tipos, $tipo, '--'),
									'gael_id' =>  Arr::get($gastos, $gasto, $gasto),
									'itga_nombre' => Arr::get($subactividad, '--'),
									'total' => $subtotal,
									'costounitario' => Arr::get($subactividad, 'costounitario'),
									'cantidad' => Arr::get($subactividad, 'cantidad'),
								];
							}
						}
					}
				}
			}
		}
		
		//debug($subactividades);
		//debug($items);
		
		$view = Theme_View::factory('evaluador/reportes/items')
			->set('evento', $this->evento)
			->set('items', array_values($items));
		
		Theme::instance()
			->template
			->content = $view;
		
		Breadcrumb::add('Ver eventos', '/evaluador/verEventos');
		Breadcrumb::add($this->get_evento_nombre($this->evento));
	}
	
	public function action_detalles()
	{
		$aAcreditadoEtapaFicha = $this->get_fichas();
		$comites = $this->comites;
		//echo Debug::dump($aAcreditadoEtapaFicha, 5000);
		//die();
		//debug($aAcreditadoEtapaFicha);
		
		//debug($this->comites);
		
		$oEvento = ORM::factory('Evento', $this->id);
		
		$grupos = $oEvento
			->get_grupos()
			->execute()
			->as_array('grup_id', 'grup_nombre');
		
		$gastos = $oEvento
			->get_gastos()
			->execute()
			->as_array('gael_id', 'gael_nombre');
			//debug($gastos);
		
		$tipos = $oEvento
			->get_tipos()
			->execute()
			->as_array('tiga_id', 'tiga_nombre');
		
		$items = ORM::factory('Evento', $this->id)
			->get_items()
			->select('*', DB::expr('0 total'), DB::expr('0 cantidad'))
			->execute()
			->as_array('itga_id');
		
		$grupos['super'] = ['grup_short' => 'Supervisión'];
		$grupos['total'] = ['grup_short' => 'Total'];
		//debug($grupos);
		
		foreach ($comites as $key => $data)
		{
			$acev_id = $data['acev_id'];
			
			$oAcreditadoEtapaFicha = Arr::get($aAcreditadoEtapaFicha, $acev_id);
			
			//if (in_array($acev_id, array_keys($aAcreditadoEtapa)))
			if ($oAcreditadoEtapaFicha)
			{
				$data = json_decode($oAcreditadoEtapaFicha->acef_data, TRUE);
				//debug($data);
				
				$per = $data['totales']['fec'][3] / $data['totales']['fec']['total'];

				//debug2($data['totales'], numberformat($per, 2));

				if ($per > 0.6)
				{
					unset($comites[$key]);
					continue;
				}
				
				$comites[$key]['totales'] = $data['totales'];
				$comites[$key]['items'] = $items;
				
				foreach ($data['actividades'] as $actividades)
				{
					//debug2($actividades);

					foreach ($actividades as $actividad)
					{
						foreach ($actividad['subactividades'] as $subactividad)
						{
							//debug($subactividad);

							$gasto = $subactividad['gasto'];
							$financiamiento = $subactividad['financiamiento'];

							if ($financiamiento == 1)
							{
								$item = Arr::get($subactividad, 'item');
								$tipo = Arr::get($subactividad, 'tipo');
								$gasto = Arr::get($subactividad, 'gasto');
								$grupo = Arr::get($subactividad, 'grupo');
								$subtotal = Arr::get($subactividad, 'subtotal');
								$cantidad = Arr::get($subactividad, 'cantidad');

								// Solo se da en acev 616, ver archivo de incidencias
								if ( ! $gasto)
								{
									continue;
									//debug2($oAcreditadoEtapaFicha->oAcreditadoEtapa->acev_id);
									//debug2($data);
								}

								if ($item == 999)
								{
									$comites[$key]['items'][] = [
										'grup_id' => Arr::get($grupos, $grupo, $grupo),
										'tiga_id' =>  Arr::get($tipos, $tipo, 'Otros'),
										'gael_id' => Arr::get($gastos, $gasto, $gasto),
										'itga_nombre' => Arr::get($subactividad, 'itemOtro'),
										'total' => $subtotal,
										'costounitario' => Arr::get($subactividad, 'costounitario'),
										'cantidad' => Arr::get($subactividad, 'cantidad'),
									];
								}
								elseif ($item)
								{
									$comites[$key]['items'][$item]['total'] += $subtotal;
									$comites[$key]['items'][$item]['cantidad'] += $cantidad;
									$comites[$key]['items'][$item]['costounitario'] = Arr::get($subactividad, 'costounitario');
								}
								elseif ($tipo == 999)
								{
									//debug2($subactividad);
									$comites[$key]['items'][] = [
										'grup_id' => Arr::get($grupos, $grupo, $grupo),
										'tiga_id' => Arr::get($tipos, $tipo, 'Otros'),
										'gael_id' =>  Arr::get($gastos, $gasto, $gasto),
										'itga_nombre' => Arr::get($subactividad, 'tipoOtro'),
										'total' => $subtotal,
										'costounitario' => Arr::get($subactividad, 'costounitario'),
										'cantidad' => Arr::get($subactividad, 'cantidad'),
									];
								}
								elseif ($grupo == 1)
								{
									$comites[$key]['items'][] = [
										'grup_id' => Arr::get($grupos, $grupo, $grupo),
										'tiga_id' => Arr::get($tipos, $tipo, '--'),
										'gael_id' =>  Arr::get($gastos, $gasto, $gasto),
										'itga_nombre' => Arr::get($subactividad, '--'),
										'total' => $subtotal,
										'costounitario' => Arr::get($subactividad, 'costounitario'),
										'cantidad' => Arr::get($subactividad, 'cantidad'),
									];
								}
								else
								{
									die('wtf');
								}
							}
						}
					}
				}
				
				//debug($comites[$key]);
			}
			else
			{
				unset($comites[$key]);
			}
		}
		
		//debug($subactividades);
		//debug($items);
		
		$view = Theme_View::factory('evaluador/reportes/detalles', compact('grupos'))
			->set('comites', array_values($comites))
			->set('evento', $this->evento);
		
		Theme::instance()
			->template
			->content = $view;
		
		Breadcrumb::add('Ver eventos', '/evaluador/verEventos');
		Breadcrumb::add($this->get_evento_nombre($this->evento));
	}
	
	private function get_evento_nombre($evento)
	{
		return Text::limit_words($evento['even_nombre'], 2)." ({$evento['tiin_nombre']})";
	}
	
	private function get_fichas()
	{
		$status = Model_AcreditadoEtapa::ESTADO_ATENDIDO;
		$ids = '('.implode(',', $this->ids).')';
		
		return ORM::factory('AcreditadoEtapaFicha')
			->with('oEventoEtapaFicha')
			->with('oAcreditadoEtapa:oEventoEtapa')
			->with('oAcreditadoEvento')
			->select('acev_id')
			->where('oAcreditadoEtapa:oEventoEtapa.even_id', '=', $this->id)
			->where('evet_code', '=', 'RPM')
			->where('evef_code', '=', 'MPR')
			//->where('acet_estado', '=', Model_AcreditadoEtapa::ESTADO_ATENDIDO)
			->where(DB::expr(''), '', DB::expr("(acet_estado = {$status} or acev_id in {$ids})"))
			->find_all()
			->as_array('acev_id');
	}
	
}
