<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Evaluador_Seguimiento extends Controller_Evaluador {
	
	/**
	 *
	 * @var  Model_Evento
	 */
	public $oEvento;
	
	public $eventoInfo;
	
	protected function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('EvaluadorEvento');
	}
	
	public function action_index()
	{
		$even_id = $this->oEvento->even_id;
		
		$comites = SaesProxyV2::instance()
			->get('getComites', array('even_id' => $even_id));
		
		$aAcreditadoEvento = Repository::factory('AcreditadoEvento')
			->from_even($even_id)
			->filter_with_etapas()
			->filter_from($comites);
		
		$aEventoEtapaFicha = Repository::factory('EventoEtapaFicha')
			->from_even($even_id)
			->from_code('RPM')
			->find_all();
		
		$fichas = Repository::factory('AcreditadoEtapaFicha')
			->from_even($even_id)
			->from_code('RPM')
			->order_by_acev_evef();
			//->order_for($aAcreditadoEvento);
		
		$aAcreditadoEtapa = Repository::factory('AcreditadoEtapa')
			->from_even($even_id)
			->from_code('RPM')
			->find_all()
			->as_array('acev_id');
		
		$view = Theme_View::factory('evaluador/seguimiento/index')
			->set(compact('aEventoEtapaFicha', 'aAcreditadoEvento', 'comites', 'fichas', 'aAcreditadoEtapa'))
			->set('evento', $this->eventoInfo);
		
		Theme::instance()
			->template
			->content = $view;
		
		Breadcrumb::add('Ver eventos', '/evaluador/verEventos');
		Breadcrumb::add($this->get_evento_nombre($this->eventoInfo));
	}
	
	private function get_evento_nombre($evento)
	{
		return Text::limit_words($evento['even_nombre'], 2)." ({$evento['tiin_nombre']})";
	}
	
}
