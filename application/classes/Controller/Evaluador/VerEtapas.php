<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Evaluador_VerEtapas extends Controller_Evaluador {
	
	/**
	 *
	 * @var  Model_Acreditado
	 */
	public $oAcreditado;
	
	public $userData;
	
	/**
	 * 
	 * @return  Model_AcreditadoEvento
	 */
	public $oAcreditadoEvento;
	
	public $eventoInfo;
	
	protected function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('AcreditadoEvento');
		$this->middleware('Autoevaluacion');
	}
	
	public function action_index()
	{
		$objeto = $this->userData + $this->eventoInfo;
		
		$aAcreditadoEtapa = Repository::factory('AcreditadoEtapa')
			->from($this->oAcreditadoEvento)
			->find_all();
		
		$fichas = Repository::factory('AcreditadoEtapaFicha')
			->from($this->oAcreditadoEvento)
			->order_by_evet_evef();
		
		$aEventoNivel = Repository::factory('EventoNivel')
			->from($this->oAcreditadoEvento->even_id)
			->find_all();
		
		$view = Theme_View::factory('evaluador/veretapas/index')
			->set(compact('aAcreditadoEtapa', 'fichas', 'objeto', 'aEventoNivel'));
		
		Theme::instance()
			->template
			->content = $view;
		
		Breadcrumb::add('Ver eventos', '/evaluador/verEventos');
		Breadcrumb::add(
			$this->get_evento_nombre($objeto), 
			'/evaluador/verComites'.URL::query(['even_id' => $objeto['even_id']])
		);
		Breadcrumb::add('Ver etapas');
	}
	
	public function action_cambiarEstadoNivel()
	{
		$acne_id = $this->request->post('acne_id');
		
		$data = Arr::extract(
			$this->request->post(), 
			['estado', 'fichas', 'plazo_dias_observado']
		);
		//debug($plazo_dias);
		
		$oAcreditadoNivelEtapa = ORM::factory('AcreditadoNivelEtapa', $acne_id);
		//debug($oAcreditadoNivelEtapa);
		
		if ( ! $oAcreditadoNivelEtapa->loaded())
		{
			throw new Exception_Gproc('Model not found');
		}
		
		try
		{
			$oAcreditadoNivelEtapa->set_estado($data);
			Session::instance()->set('info', 'Se actualizó el estado');
		}
		catch (Exception_Gproc $e)
		{
			Session::instance()->set('error', $e->getMessage());
		}
		
		$this->redirect('/evaluador/verEtapas'.URL::query(['acev_id' => $this->oAcreditadoEvento->acev_id]));
	}
	
	public function action_ampliarPlazo()
	{
		if ($this->request->method() == 'POST')
		{
			$acet_id = $this->request->post('acet_id');
			$plazo_dias = +$this->request->post('plazo_dias') ?: 0;
			
			$oAcreditadoEtapa = ORM::factory('AcreditadoEtapa', $acet_id);
			
			if ( ! $oAcreditadoEtapa->loaded())
			{
				throw new Exception_Gproc('Model not found');
			}
			
			if ($oAcreditadoEtapa->is_observado())
			{
				$oAcreditadoEtapa
					->set('acet_fecha_fin_observado', date('Y-m-d', strtotime('+'.$plazo_dias.' day' , strtotime ( date('Y-m-d') ))))
					->save();
			}
			else
			{
				Session::instance()->set('error', 'AcreditadoEtapa no observado');
			}
			
			$this->redirect($this->request->referrer());
		}
	}
	
	public function action_setAsistencia()
	{
		//debug($this->request->post());
		//debug($this->oAcreditadoEvento);
		
		if ($this->request->method() == 'POST')
		{
			$this->oAcreditadoEvento
				->set('acev_eval_virtual', $this->request->post('asistio') ?: 0)
				->save();
			
			Session::instance()->set('info', 'Registro guardado');
		}
		
		$this->redirect($this->request->referrer());
	}
	
	private function get_evento_nombre($objeto)
	{
		//debug($objeto);
		return Text::limit_words($objeto['even_nombre'], 2)." ({$objeto['tiin_nombre']})";
	}
}
