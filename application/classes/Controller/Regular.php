<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
abstract class Controller_Regular extends Controller_Gproc {
	
	/**
	 *
	 * @var  Model_Acreditado
	 */
	public $oAcreditado;
	
	public $userData;
	
	protected function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('ACL');
		$this->middleware('AcreditadoRegularOrEvaluador');
	}
	
	protected function _set_view_globals()
	{
		parent::_set_view_globals();
		
		View::set_global('oAcreditado', $this->oAcreditado);
		View::set_global('objeto', $this->userData);
	}
	
	protected function _set_template_blocks()
	{
		parent::_set_template_blocks();
		
		if ($this->auto_render)
		{
			Theme::instance()
				->template
				->header
				->objeto = $this->userData; //Session::instance()->get('objeto');
		}
	}
	
}