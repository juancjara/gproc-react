<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
abstract class Controller_Gproc extends Theme_Controller {
	
	use Middleware_Trait;
	
	protected $theme = 'adminlte';
	
	/**
	 *
	 * @var  Model_User
	 */
	public $oUser;
	
	public function before()
	{
		$this->_check_ajax();
		$this->_check_initial();
		
		$this->handle_middleware_exceptions();
		
		parent::before();
	
		$this->_set_view_globals();
		$this->_set_template_blocks();
		
		return;
	}
	
	protected function set_middlewares()
	{
		$this->middleware('Auth');
	}
	
	protected function _check_ajax()
	{
		if ($this->request->is_ajax())
		{
			$this->auto_render = FALSE;
			
			// @experimental
			Theme::init($this->theme);
		}
	}
	
	protected function _check_initial()
	{
		if ( ! $this->request->is_initial())
		{
			$this->auto_render = FALSE;
		}
	}
	
	protected function _set_view_globals()
	{
		View::set_global('auth', Auth::instance());
		View::set_global('welcome', 'Sistema de Gestión de Convocatorias');
		
		if ($this->auto_render)
		{
			View::set_global('theme', Theme::instance());
		}
	}
	
	protected function _set_template_blocks()
	{
		if ($this->auto_render)
		{
			$theme = Theme::instance();
			
			$theme->js('default', 'form', '/gproc_lte/js/plugins/jquery.form.js');
			
			$theme->css('page', 'regular', '/gproc_lte/css/regular.css');
			$theme->js('page', 'regular', '/gproc_lte/js/regular.js');
			
			$theme->template->header = Theme_View::factory('regular/template/header');
			$theme->template->sidebar = Theme_View::factory('regular/template/sidebar');
			$theme->template->footer = Theme_View::factory('template/footer');
		}
	}
	
	public function after()
	{
		parent::after();
		
		$this->response->headers('Expires', 0);
	}
	
}
