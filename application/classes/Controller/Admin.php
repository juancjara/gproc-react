<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
abstract class Controller_Admin extends Controller_Gproc {
	
	public $eventos;
	
	protected function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('ACL');
		$this->middleware('SetEventos');
	}
	
	protected function _set_template_blocks()
	{
		parent::_set_template_blocks();
		
		if ($this->auto_render)
		{
			$theme = Theme::instance();
			$theme->css('page', 'evaluador', '/gproc_lte/css/evaluador.css');
			$theme->js('page', 'evaluador', '/gproc_lte/js/evaluador.js');
			
			$theme->template->header = Theme_View::factory('evaluador/template/header');
			$theme->template->sidebar = Theme_View::factory('admin/template/sidebar');
		}
	}
	
}
