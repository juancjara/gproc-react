<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class PDF extends Common_PDF {
	
	protected $author = 'GPROC - Procalidad';
	
	protected $title = 'GPROC - Procalidad';
	
	public function header($header)
	{
		if ($header === TRUE)
		{
			$this->set_handler('TCPDF_Header');
		}
		
		return $this;
	}
	
}
