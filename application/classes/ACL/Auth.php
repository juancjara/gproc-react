<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class ACL_Auth extends Kohana_ACL_Auth {
	
	protected $_role;
	
	public function __construct()
	{
		$this->_config 	= Kohana::$config->load('acl');
		$this->_auth 	= Auth::instance();
		$this->_user 	= $this->_auth->get_user($this->_config['guest_role']);
		$this->_role 	= $this->_config['guest_role'];
		
		if (isset($this->_user->role_id))
		{
			$id = $this->_user->role_id;

			$this->_role = lcfirst(Model_Role::$roles[$id]);
		}
		
		if ($this->_auth->is_super())
		{
			$this->_role = ACL::ROLE_SUPER;
		}
	}
	
	// @temp
	/*public function set_role($role)
	{
		$this->_role = $role;
		debug($role);
	}*/
	
}