<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Ficha {
	
	protected $name;
	
	protected $_data;
	
	/**
	 * 
	 * @return  Ficha
	 */
	public static function factory($name)
	{
		return new Ficha($name);
	}
	
	public function __construct($name)
	{
		$this->name = $name;
	}
	
	public function get_data()
	{
		return $this->_data;
	}
	
	public function set($key, $value = NULL)
	{
		if (is_array($key))
		{
			foreach ($key as $name => $value)
			{
				$this->_data[$name] = $value;
			}
		}
		else
		{
			$this->_data[$key] = $value;
		}

		return $this;
	}
	
	public function get($action = NULL)
	{
		$this->set_assets();
		
		$ficha = Theme_View::factory('fichas/'.$this->name, $this->_data);
		
		return $ficha;
	}
	
	public function get_pdf($header = FALSE)
	{
		$ficha = Theme_View::factory('fichas/pdf/'.$this->name, $this->_data);
		//echo $ficha; die();
		
		return PDF::factory()
			->set('view', $ficha)
			->header($header)
			->get($header);
	}
	
	public function get_excel()
	{
		$ficha = Theme_View::factory('fichas/excel/'.$this->name, $this->_data)
			->set('title', '');
		//echo $ficha; die();
		
		Theme::instance()->css([]);
		
		return Theme_View::factory('reportes/template')
			->set('content', $ficha);
	}
	
	private function set_assets()
	{
		//debug($this->name);
		Theme::instance()
			->css('page', 'ficha', "/gproc_lte/css/{$this->name}.css")
			->js('page', 'ficha', "/gproc_lte/js/{$this->name}.js");
			//debug(Theme::instance());
	}
	
}
