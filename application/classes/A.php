<?php defined('SYSPATH') or die('No direct script access.');

class A {
	
	private $value;
	
	public static function f($array)
	{
		return new A($array);
	}
	
	public function __construct($array)
	{
		$this->value = $array;
	}
	
	public function value()
	{
		return $this->value;
	}
	
	public function map_dict($fn)
	{
		$this->value = array_map($fn, $this->value);
		
		return $this;
	}
	
	public function map($fn)
	{
		if (Arr::is_assoc($this->value))
		{
			$this->map_dict($fn);
		}
		else
		{
			$this->value = array_map($fn, $this->value, array_keys($this->value));
		}
		
		return $this;
	}
	
	public function filter($fn)
	{
		$this->value = array_values(array_filter($this->value, $fn));
		
		return $this;
	}
	
	public function reduce($fn, $initial = NULL)
	{
		$this->value = array_reduce($this->value, $fn, $initial);
		
		return $this;
	}
	
	public function group_by($fn)
	{
		$this->value = Arr::group_by($this->value, $fn);
		
		return $this;
	}
	
	public function concat($arr)
	{
		$this->value = array_merge($this->value, $arr);
		
		return $this;
	}
	
	public function to_dict($key)
	{
		if (is_string($key))
		{
			$this->value = Arr::array_to_assoc($this->value, $key);
		}
		else
		{
			$fn = $key;
			$this->value = array_reduce($this->value, 
				function ($acc, $el) use ($fn) { 
					$acc[$fn($el)] = $el;
					
					return $acc;
				}, 
				[]);
		}
		
		return $this;
	}
	
	public function pluck($key)
	{
		$this->value = array_map(
			function($el) use ($key) {
				return $el[$key];
			}, 
			$this->value);
		
		return $this;
	}
	
	public function union()
	{
		$this->value = 
			array_unique(
				array_reduce($this->value, 
					function ($carry, $item) {
						return array_merge($carry, $item);
					}, 
					[]
				)
			);

		return $this;
	}
	
	public function flatten()
	{
		$this->value = array_reduce($this->value, 
			function ($acc, $el) {
				return $acc + $el;
			},
			[]
		);
		
		return $this;
	}
	
	public function slice($offset, $length = NULL, $preserve_keys = FALSE)
	{
		$this->value = array_slice($this->value, $offset, $length, $preserve_keys);
		
		return $this;
	}
	
}
