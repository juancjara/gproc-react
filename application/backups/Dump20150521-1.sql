-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: gprocdemo.procalidad.gob.pe    Database: pro_gprocdemo
-- ------------------------------------------------------
-- Server version	5.5.41-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acreditado`
--

DROP TABLE IF EXISTS `acreditado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acreditado` (
  `acre_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`acre_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `fk_acreditado_user1_idx` (`user_id`),
  CONSTRAINT `fk_acreditado_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acreditado`
--

LOCK TABLES `acreditado` WRITE;
/*!40000 ALTER TABLE `acreditado` DISABLE KEYS */;
INSERT INTO `acreditado` VALUES (1,1),(2,2),(3,3),(4,4),(18651,5);
/*!40000 ALTER TABLE `acreditado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acreditado_etapa`
--

DROP TABLE IF EXISTS `acreditado_etapa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acreditado_etapa` (
  `acet_id` int(11) NOT NULL AUTO_INCREMENT,
  `acet_estado` int(11) DEFAULT NULL,
  `acet_finalizado` int(11) DEFAULT NULL,
  `acev_id` int(11) NOT NULL,
  `acet_data` text,
  `acet_data2` text NOT NULL,
  `evet_id` int(11) NOT NULL,
  `acet_path` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`acet_id`),
  KEY `fk_etapa_acreditado_evento1_idx` (`acev_id`),
  KEY `fk_acreditado_etapa_evento_etapa1_idx` (`evet_id`),
  CONSTRAINT `fk_acreditado_etapa_evento_etapa1` FOREIGN KEY (`evet_id`) REFERENCES `evento_etapa` (`evet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_etapa_acreditado_evento1` FOREIGN KEY (`acev_id`) REFERENCES `acreditado_evento` (`acev_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acreditado_etapa`
--

LOCK TABLES `acreditado_etapa` WRITE;
/*!40000 ALTER TABLE `acreditado_etapa` DISABLE KEYS */;
INSERT INTO `acreditado_etapa` VALUES (3,10,0,1,'{\"acet_etapa\":\"1\",\"dimensiones\":{\"1\":{\"objetivo\":\"Capacitar a todos los directores, coordinadores, supervisores, en uso de herramientas TI ligadas al \\u00e1rea de gesti\\u00f3n pedag\\u00f3gica\"},\"2\":{\"objetivo\":\"\"},\"3\":{\"objetivo\":\"\"},\"4\":{\"objetivo\":\"\"}},\"factores\":{\"1.1\":{\"resultado\":\"Autoridades capacitadas\"},\"1.2\":{\"resultado\":\"\"},\"1.3\":{\"resultado\":\"\"},\"1.4\":{\"resultado\":\"\"},\"2.1\":{\"resultado\":\"\"},\"2.5\":{\"resultado\":\"\"},\"2.6\":{\"resultado\":\"\"},\"3.1\":{\"resultado\":\"\"},\"3.2\":{\"resultado\":\"\"},\"3.3\":{\"resultado\":\"\"},\"3.4\":{\"resultado\":\"\"},\"4.1\":{\"resultado\":\"\"},\"4.2\":{\"resultado\":\"\"},\"4.3\":{\"resultado\":\"\"}},\"actividades\":{\"1.1.3\":[{\"elegible\":\"4\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"2.4.2\",\"porcentaje\":\"20\"},{\"estandar\":\"2.4.2\",\"porcentaje\":\"50\"}],\"costototal\":\"300\",\"costofec\":\"0\",\"costopropio\":\"200\",\"costootros\":\"100\",\"actor_nombres\":\"David\",\"actor_dni\":\"05454545\",\"actor_telefono\":\"987987987\",\"actor_cargo\":\"Coordinador de Log\\u00edstica\",\"actor_email\":\"david@gmail.com\",\"ejecucion\":\"10\"}],\"1.2.1\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"0\",\"costofec\":\"0\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"1.3.2\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"1.3.4\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"1.3.6\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"1.4.2\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"1.4.3\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.1.1\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.1.6\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.1.7\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.5.2\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.6.3\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.6.4\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"2.6.5\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.1.2\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.1.3\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.2.2\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.2.3\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.3.1\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.3.2\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.4.1\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.4.4\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"3.4.7\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"4.1.2\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"4.2.2\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"4.3.4\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}]},\"save_only\":\"1\"}','',0,NULL),(4,10,NULL,4,'{\"acet_etapa\":\"1\",\"dimensiones\":{\"1\":{\"objetivo\":\"Capacitar\"},\"2\":{\"objetivo\":\"\"},\"3\":{\"objetivo\":\"\"}},\"factores\":{\"1\":{\"resultado\":\"\"},\"2\":{\"resultado\":\"\"},\"3\":{\"resultado\":\"\"},\"4\":{\"resultado\":\"\"},\"5\":{\"resultado\":\"\"},\"6\":{\"resultado\":\"\"},\"7\":{\"resultado\":\"\"},\"9\":{\"resultado\":\"\"}},\"actividades\":{\"5\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"10\",\"pasantia_alojamiento\":\"20\",\"pasantia_alimentacion\":\"30\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"28\",\"porcentaje\":\"\"}],\"costototal\":\"10060\",\"costofec\":\"60\",\"costopropio\":\"10000\",\"costootros\":\"0\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"8\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"10\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"13\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"15\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"16\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"17\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"18\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"24\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"25\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"26\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"29\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"31\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"32\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"33\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"35\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"37\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"38\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"40\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"42\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"45\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"46\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"47\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"51\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"56\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"58\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"62\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"63\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"67\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"69\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"77\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"78\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"81\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"84\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"85\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"86\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"87\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"89\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"90\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\",\"estandares\":[{\"estandar\":\"\",\"porcentaje\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_nombres\":\"\",\"actor_dni\":\"\",\"actor_telefono\":\"\",\"actor_cargo\":\"\",\"actor_email\":\"\",\"ejecucion\":\"\"}],\"96\":[{\"elegible\":\"0\",\"nofinanciable\":\"\",\"pasantia_pasaje\":\"\",\"pasantia_alojamiento\":\"\",\"pasantia_alimentacion\":\"\",\"capacitacion_pasaje\":\"\",\"capacitacion_alojamiento\":\"\",\"capacitacion_alimentacion\":\"\",\"capacitacion_formacion\":\"\",\"taller_pasaje\":\"\",\"taller_alojamiento\":\"\",\"taller_alimentacion\":\"\",\"taller_costo\":\"\"}]}}','',0,NULL),(5,10,NULL,318,'{\"save_only\":\"1\",\"acet_etapa\":\"1\",\"dimensiones\":{\"1\":{\"objetivo\":\"\"},\"2\":{\"objetivo\":\"\"},\"3\":{\"objetivo\":\"\"}},\"factores\":{\"1\":{\"resultado\":\"\"},\"2\":{\"resultado\":\"\"},\"3\":{\"resultado\":\"\"},\"4\":{\"resultado\":\"\"},\"5\":{\"resultado\":\"\"},\"6\":{\"resultado\":\"\"},\"7\":{\"resultado\":\"\"},\"8\":{\"resultado\":\"\"},\"9\":{\"resultado\":\"\"}},\"actividades\":{\"1\":[{\"miactividad\":\"\",\"estandares\":[{\"estandar\":\"1590\"},{\"estandar\":\"1581\"}],\"costototal\":\"\",\"costofec\":\"\",\"costopropio\":\"\",\"radiocosto\":\"PROPIO\",\"costootros\":\"\",\"actor_cargo\":\"\",\"ejecucion\":\"\"},{\"miactividad\":\"\",\"estandares\":[{\"estandar\":\"1657\"}],\"costototal\":\"\",\"costofec\":\"\",\"radiocosto\":\"FEC\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_cargo\":\"\",\"ejecucion\":\"\"}],\"2\":[{\"miactividad\":\"\",\"estandares\":[{\"estandar\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"radiocosto\":\"FEC\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_cargo\":\"actor3\",\"ejecucion\":\"\"}],\"3\":[{\"miactividad\":\"\",\"estandares\":[{\"estandar\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"radiocosto\":\"FEC\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_cargo\":\"\",\"ejecucion\":\"\"}],\"4\":[{\"miactividad\":\"\",\"estandares\":[{\"estandar\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"radiocosto\":\"FEC\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_cargo\":\"\",\"ejecucion\":\"\"}],\"5\":[{\"miactividad\":\"\",\"estandares\":[{\"estandar\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"radiocosto\":\"FEC\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_cargo\":\"\",\"ejecucion\":\"\"}],\"6\":[{\"miactividad\":\"\",\"estandares\":[{\"estandar\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"radiocosto\":\"FEC\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_cargo\":\"\",\"ejecucion\":\"\"}],\"7\":[{\"miactividad\":\"\",\"estandares\":[{\"estandar\":\"\"}],\"costototal\":\"\",\"costofec\":\"\",\"radiocosto\":\"FEC\",\"costopropio\":\"\",\"costootros\":\"\",\"actor_cargo\":\"\",\"ejecucion\":\"\"}],\"8\":[{\"miactividad\":\"\",\"estandares\":[{\"estandar\":\"\"},{\"estandar\":\"\"}],\"costototal\":\"0\",\"costofec\":\"\",\"costopropio\":\"\",\"radiocosto\":\"PROPIO\",\"costootros\":\"\",\"actor_cargo\":\"\",\"ejecucion\":\"\"}],\"9\":[{\"miactividad\":\"\",\"estandares\":[{\"estandar\":\"\"}],\"costototal\":\"50\",\"costofec\":\"\",\"costopropio\":\"20\",\"radiocosto\":\"PROPIO\",\"costootros\":\"30\",\"actor_cargo\":\"\",\"ejecucion\":\"\"}]}}','',7,NULL),(6,20,NULL,318,'{\"tifi_id\":\"2\",\"comite\":[{\"pers_nombres\":\"a\",\"pers_apellidos\":\"b\",\"pers_cargo\":\"c\",\"pers_correo\":\"d@e\",\"pers_telefono\":\"f\"}],\"ruc\":\"\",\"sedes\":[{\"nombre_sede\":\"Sede principal\",\"direc\":\"g\",\"dist\":\"h\",\"prov\":\"i\",\"region\":\"j\",\"carreras\":[{\"nombre\":\"k\",\"area\":\"l\",\"comite\":[{\"pers_nombres\":\"Carlos\",\"pers_apellidos\":\"Flores\",\"pers_cargo\":\"Presidente de la comisi\\u00f3n\",\"pers_correo\":\"comision@universidad.com\",\"pers_telefono\":\"987654321\"}],\"estudiantes\":{\"2014\":{\"pa\":{\"M\":\"\",\"F\":\"\"},\"ms\":{\"M\":\"\",\"F\":\"\"},\"eg\":{\"M\":\"\",\"F\":\"\"},\"ti\":{\"M\":\"\",\"F\":\"\"}},\"2013\":{\"pa\":{\"M\":\"\",\"F\":\"\"},\"ms\":{\"M\":\"\",\"F\":\"\"},\"eg\":{\"M\":\"\",\"F\":\"\"},\"ti\":{\"M\":\"\",\"F\":\"\"}},\"2012\":{\"pa\":{\"M\":\"\",\"F\":\"\"},\"ms\":{\"M\":\"\",\"F\":\"\"},\"eg\":{\"M\":\"\",\"F\":\"\"},\"ti\":{\"M\":\"\",\"F\":\"\"}},\"2011\":{\"pa\":{\"M\":\"\",\"F\":\"\"},\"ms\":{\"M\":\"\",\"F\":\"\"},\"eg\":{\"M\":\"\",\"F\":\"\"},\"ti\":{\"M\":\"\",\"F\":\"\"}},\"2010\":{\"pa\":{\"M\":\"\",\"F\":\"\"},\"ms\":{\"M\":\"\",\"F\":\"\"},\"eg\":{\"M\":\"\",\"F\":\"\"},\"ti\":{\"M\":\"\",\"F\":\"\"}}},\"grados_dedicacion\":{\"pt\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"},\"pe\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"},\"bc\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"},\"lt\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"},\"mm\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"},\"dr\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"},\"tt\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"}},\"grados_condicion\":{\"pt\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"},\"pe\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"},\"bc\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"},\"lt\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"},\"mm\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"},\"dr\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"},\"tt\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"}},\"docentes_categoria\":{\"pr\":{\"tt\":\"\"},\"as\":{\"tt\":\"\"},\"au\":{\"tt\":\"\"},\"tt\":{\"tt\":\"\"}},\"inves\":{\"2014\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2013\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2012\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2011\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2010\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"}},\"publi\":{\"2014\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2013\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2012\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2011\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2010\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"}},\"paten\":{\"2014\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2013\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2012\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2011\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2010\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"}},\"actividades\":{\"2014\":\"\",\"2013\":\"\",\"2012\":\"\",\"2011\":\"\",\"2010\":\"\"},\"servicios\":{\"2014\":\"\",\"2013\":\"\",\"2012\":\"\",\"2011\":\"\",\"2010\":\"\"},\"vinculos\":[{\"ic\":\"\",\"dv\":\"\"}],\"origen_fondos\":{\"pao\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"},\"rdr\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"},\"itd\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"},\"otr\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"},\"tot\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"}},\"destino_fondos\":{\"s1\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"},\"s2\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"},\"bc\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"},\"ob\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"},\"ou\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"},\"tt\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"}}}]}],\"grados_dedicacion\":{\"pt\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"},\"pe\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"},\"bc\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"},\"lt\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"},\"mm\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"},\"dr\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"},\"tt\":{\"ex\":\"\",\"tc\":\"\",\"tp\":\"\",\"tt\":\"\"}},\"grados_condicion\":{\"pt\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"},\"pe\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"},\"bc\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"},\"lt\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"},\"mm\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"},\"dr\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"},\"tt\":{\"no\":\"\",\"co\":\"\",\"tt\":\"\"}},\"docentes_categoria\":{\"pr\":{\"tt\":\"\"},\"as\":{\"tt\":\"\"},\"au\":{\"tt\":\"\"},\"tt\":{\"tt\":\"\"}},\"p_admision\":[{\"nombre\":\"Sede principal: k\",\"x2014\":\"\",\"x2013\":\"\",\"x2012\":\"\",\"x2011\":\"\",\"x2010\":\"\"}],\"m_semestre\":[{\"nombre\":\"Sede principal: k\",\"x2014\":\"\",\"x2013\":\"\",\"x2012\":\"\",\"x2011\":\"\",\"x2010\":\"\"}],\"egresados\":[{\"nombre\":\"Sede principal: k\",\"x2014\":\"\",\"x2013\":\"\",\"x2012\":\"\",\"x2011\":\"\",\"x2010\":\"\"}],\"titulados\":[{\"nombre\":\"Sede principal: k\",\"x2014\":\"\",\"x2013\":\"\",\"x2012\":\"\",\"x2011\":\"\",\"x2010\":\"\"}],\"carrera_area\":\"m\",\"datos\":{\"inst\":{\"nombre\":\"n\",\"cargo\":\"o\",\"direccion\":\"\",\"fijo\":\"\",\"celular\":\"\",\"email\":\"\",\"emailalt\":\"\",\"date\":\"2015-04-30\",\"date_local\":\"30 de Abril del 2015\"},\"carr\":{\"nombre\":\"p\",\"cargo\":\"q\",\"direccion\":\"\",\"fijo\":\"\",\"celular\":\"\",\"email\":\"\",\"emailalt\":\"\",\"date\":\"2015-04-30\",\"date_local\":\"30 de Abril del 2015\"}}}','',6,'uploads/6-expresioninsteres.pdf'),(7,10,NULL,377,'{\"tifi_id\":\"2\",\"comite\":[{\"pers_nombres\":\"Juan \",\"pers_apellidos\":\"Perez\",\"pers_cargo\":\"Director\",\"pers_correo\":\"jperez@universidad.com\",\"pers_telefono\":\"987654321\"},{\"pers_nombres\":\"Perico\",\"pers_apellidos\":\"Palotes\",\"pers_cargo\":\"Presidente de Comisi\\u00f3n\",\"pers_correo\":\"ppalotes@universidad.com\",\"pers_telefono\":\"12345678\"}],\"ruc\":\"2052368964\",\"sedes\":[{\"nombre_sede\":\"Sede principal\",\"direc\":\"Av. Siempre Viva #742\",\"dist\":\"Palpa\",\"prov\":\"Ica\",\"region\":\"Ica\",\"carreras\":[{\"nombre\":\"Ingenier\\u00eda Industrial\",\"area\":\"Ingenier\\u00edas\",\"comite\":[{\"pers_nombres\":\"Carlos\",\"pers_apellidos\":\"Flores\",\"pers_cargo\":\"Presidente de la comisi\\u00f3n\",\"pers_correo\":\"comision@universidad.com\",\"pers_telefono\":\"987654321\"}],\"estudiantes\":{\"2014\":{\"pa\":{\"M\":\"200\",\"F\":\"280\"},\"ms\":{\"M\":\"100\",\"F\":\"80\"},\"eg\":{\"M\":\"50\",\"F\":\"40\"},\"ti\":{\"M\":\"30\",\"F\":\"20\"}},\"2013\":{\"pa\":{\"M\":\"100\",\"F\":\"100\"},\"ms\":{\"M\":\"80\",\"F\":\"80\"},\"eg\":{\"M\":\"60\",\"F\":\"60\"},\"ti\":{\"M\":\"30\",\"F\":\"30\"}},\"2012\":{\"pa\":{\"M\":\"150\",\"F\":\"150\"},\"ms\":{\"M\":\"100\",\"F\":\"100\"},\"eg\":{\"M\":\"70\",\"F\":\"70\"},\"ti\":{\"M\":\"40\",\"F\":\"40\"}},\"2011\":{\"pa\":{\"M\":\"90\",\"F\":\"90\"},\"ms\":{\"M\":\"80\",\"F\":\"80\"},\"eg\":{\"M\":\"60\",\"F\":\"60\"},\"ti\":{\"M\":\"40\",\"F\":\"40\"}},\"2010\":{\"pa\":{\"M\":\"80\",\"F\":\"90\"},\"ms\":{\"M\":\"50\",\"F\":\"40\"},\"eg\":{\"M\":\"30\",\"F\":\"30\"},\"ti\":{\"M\":\"20\",\"F\":\"20\"}}},\"grados_dedicacion\":{\"pt\":{\"ex\":\"5\",\"tc\":\"5\",\"tp\":\"5\",\"tt\":\"15\"},\"pe\":{\"ex\":\"15\",\"tc\":\"10\",\"tp\":\"10\",\"tt\":\"35\"},\"bc\":{\"ex\":\"3\",\"tc\":\"15\",\"tp\":\"20\",\"tt\":\"38\"},\"lt\":{\"ex\":\"5\",\"tc\":\"10\",\"tp\":\"15\",\"tt\":\"30\"},\"mm\":{\"ex\":\"3\",\"tc\":\"4\",\"tp\":\"4\",\"tt\":\"11\"},\"dr\":{\"ex\":\"0\",\"tc\":\"1\",\"tp\":\"2\",\"tt\":\"3\"},\"tt\":{\"ex\":\"31\",\"tc\":\"45\",\"tp\":\"56\",\"tt\":\"132\"}},\"grados_condicion\":{\"pt\":{\"no\":\"10\",\"co\":\"5\",\"tt\":\"15\"},\"pe\":{\"no\":\"20\",\"co\":\"15\",\"tt\":\"35\"},\"bc\":{\"no\":\"20\",\"co\":\"18\",\"tt\":\"38\"},\"lt\":{\"no\":\"20\",\"co\":\"10\",\"tt\":\"30\"},\"mm\":{\"no\":\"5\",\"co\":\"6\",\"tt\":\"11\"},\"dr\":{\"no\":\"1\",\"co\":\"2\",\"tt\":\"3\"},\"tt\":{\"no\":\"76\",\"co\":\"56\",\"tt\":\"132\"}},\"docentes_categoria\":{\"pr\":{\"tt\":\"60\"},\"as\":{\"tt\":\"30\"},\"au\":{\"tt\":\"42\"},\"tt\":{\"tt\":\"132\"}},\"inves\":{\"2014\":{\"ti\":\"Investigaci\\u00f3n I\",\"ac\":\"Ciencias\",\"in\":\"Pedro D\\u00edaz\"},\"2013\":{\"ti\":\"Investigaci\\u00f3n II\",\"ac\":\"Ciencias\",\"in\":\"Juan Alc\\u00e1ntara\"},\"2012\":{\"ti\":\"Investigaci\\u00f3n III\",\"ac\":\"Enfermer\\u00eda\",\"in\":\"Jos\\u00e9 Jos\\u00e9\"},\"2011\":{\"ti\":\"Investigaci\\u00f3n IV\",\"ac\":\"-\",\"in\":\"-\"},\"2010\":{\"ti\":\"Investigaci\\u00f3n V\",\"ac\":\"-\",\"in\":\"-\"}},\"publi\":{\"2014\":{\"ti\":\"T\\u00edtulo I  \",\"ac\":\"Ingenier\\u00eda\",\"in\":\"Pedro D\\u00edaz\"},\"2013\":{\"ti\":\"T\\u00edtulo II\",\"ac\":\"Ingenier\\u00eda\",\"in\":\"Juan Alc\\u00e1ntara\"},\"2012\":{\"ti\":\"T\\u00edtulo III\",\"ac\":\"Ingenier\\u00eda\",\"in\":\"Juan Alc\\u00e1ntara\"},\"2011\":{\"ti\":\"T\\u00edtulo IV\",\"ac\":\"Ingenier\\u00eda\",\"in\":\"Juan Alc\\u00e1ntara\"},\"2010\":{\"ti\":\"T\\u00edtulo V\",\"ac\":\"Ingenier\\u00eda\",\"in\":\"Pedro D\\u00edaz\"}},\"paten\":{\"2014\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2013\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2012\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2011\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"},\"2010\":{\"ti\":\"\",\"ac\":\"\",\"in\":\"\"}},\"actividades\":{\"2014\":\"-\",\"2013\":\"-\",\"2012\":\"-\",\"2011\":\"-\",\"2010\":\"-\"},\"servicios\":{\"2014\":\"Renovaci\\u00f3n de equipos de laboratorio\",\"2013\":\"\",\"2012\":\"\",\"2011\":\"\",\"2010\":\"\"},\"vinculos\":[{\"ic\":\"\",\"dv\":\"\"}],\"origen_fondos\":{\"pao\":{\"2014n\":\"7000000\",\"2014p\":\"12\",\"2013n\":\"7000000\",\"2013p\":\"14\"},\"rdr\":{\"2014n\":\"0\",\"2014p\":\"0\",\"2013n\":\"0\",\"2013p\":\"0\"},\"itd\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"},\"otr\":{\"2014n\":\"\",\"2014p\":\"\",\"2013n\":\"\",\"2013p\":\"\"},\"tot\":{\"2014n\":\"7000000\",\"2014p\":\"12\",\"2013n\":\"7000000\",\"2013p\":\"14\"}},\"destino_fondos\":{\"s1\":{\"2014n\":\"4000\",\"2014p\":\"\",\"2013n\":\"3000\",\"2013p\":\"\"},\"s2\":{\"2014n\":\"2000\",\"2014p\":\"\",\"2013n\":\"1800\",\"2013p\":\"\"},\"bc\":{\"2014n\":\"100000\",\"2014p\":\"\",\"2013n\":\"900000\",\"2013p\":\"\"},\"ob\":{\"2014n\":\"500000\",\"2014p\":\"\",\"2013n\":\"250000\",\"2013p\":\"\"},\"ou\":{\"2014n\":\"6000\",\"2014p\":\"\",\"2013n\":\"6000\",\"2013p\":\"\"},\"tt\":{\"2014n\":\"612000\",\"2014p\":\"0\",\"2013n\":\"1160800\",\"2013p\":\"\"}}},{\"nombre\":\"Ingenier\\u00eda Mec\\u00e1nica\",\"area\":\"Ingenier\\u00edas\"},{\"nombre\":\"Medicina\",\"area\":\"Medicina\"},{\"nombre\":\"Enfermer\\u00eda\",\"area\":\"Ciencias de la Salud\"}]}],\"grados_dedicacion\":{\"pt\":{\"ex\":\"5\",\"tc\":\"5\",\"tp\":\"15\",\"tt\":\"25\"},\"pe\":{\"ex\":\"2\",\"tc\":\"4\",\"tp\":\"4\",\"tt\":\"10\"},\"bc\":{\"ex\":\"3\",\"tc\":\"3\",\"tp\":\"1\",\"tt\":\"7\"},\"lt\":{\"ex\":\"10\",\"tc\":\"10\",\"tp\":\"1\",\"tt\":\"21\"},\"mm\":{\"ex\":\"2\",\"tc\":\"2\",\"tp\":\"2\",\"tt\":\"6\"},\"dr\":{\"ex\":\"1\",\"tc\":\"1\",\"tp\":\"1\",\"tt\":\"3\"},\"tt\":{\"ex\":\"23\",\"tc\":\"25\",\"tp\":\"24\",\"tt\":\"72\"}},\"grados_condicion\":{\"pt\":{\"no\":\"10\",\"co\":\"22\",\"tt\":\"32\"},\"pe\":{\"no\":\"5\",\"co\":\"6\",\"tt\":\"11\"},\"bc\":{\"no\":\"4\",\"co\":\"1\",\"tt\":\"5\"},\"lt\":{\"no\":\"2\",\"co\":\"2\",\"tt\":\"4\"},\"mm\":{\"no\":\"4\",\"co\":\"5\",\"tt\":\"9\"},\"dr\":{\"no\":\"6\",\"co\":\"8\",\"tt\":\"14\"},\"tt\":{\"no\":\"31\",\"co\":\"44\",\"tt\":\"75\"}},\"docentes_categoria\":{\"pr\":{\"tt\":\"5\"},\"as\":{\"tt\":\"5\"},\"au\":{\"tt\":\"5\"},\"tt\":{\"tt\":\"15\"}},\"p_admision\":[{\"nombre\":\"Sede principal: Ingenier\\u00eda Industrial\",\"x2014\":\"100\",\"x2013\":\"90\",\"x2012\":\"80\",\"x2011\":\"75\",\"x2010\":\"89\"},{\"nombre\":\"Sede principal: Ingenier\\u00eda Mec\\u00e1nica\",\"x2014\":\"50\",\"x2013\":\"40\",\"x2012\":\"60\",\"x2011\":\"90\",\"x2010\":\"70\"},{\"nombre\":\"Sede principal: Medicina\",\"x2014\":\"30\",\"x2013\":\"40\",\"x2012\":\"25\",\"x2011\":\"35\",\"x2010\":\"64\"},{\"nombre\":\"Sede principal: Enfermer\\u00eda\",\"x2014\":\"90\",\"x2013\":\"80\",\"x2012\":\"70\",\"x2011\":\"85\",\"x2010\":\"76\"}],\"m_semestre\":[{\"nombre\":\"Sede principal: Ingenier\\u00eda Industrial\",\"x2014\":\"70\",\"x2013\":\"80\",\"x2012\":\"70\",\"x2011\":\"65\",\"x2010\":\"77\"},{\"nombre\":\"Sede principal: Ingenier\\u00eda Mec\\u00e1nica\",\"x2014\":\"40\",\"x2013\":\"30\",\"x2012\":\"50\",\"x2011\":\"70\",\"x2010\":\"50\"},{\"nombre\":\"Sede principal: Medicina\",\"x2014\":\"25\",\"x2013\":\"35\",\"x2012\":\"20\",\"x2011\":\"30\",\"x2010\":\"60\"},{\"nombre\":\"Sede principal: Enfermer\\u00eda\",\"x2014\":\"80\",\"x2013\":\"70\",\"x2012\":\"65\",\"x2011\":\"80\",\"x2010\":\"74\"}],\"egresados\":[{\"nombre\":\"Sede principal: Ingenier\\u00eda Industrial\",\"x2014\":\"30\",\"x2013\":\"30\",\"x2012\":\"30\",\"x2011\":\"30\",\"x2010\":\"30\"},{\"nombre\":\"Sede principal: Ingenier\\u00eda Mec\\u00e1nica\",\"x2014\":\"25\",\"x2013\":\"25\",\"x2012\":\"25\",\"x2011\":\"25\",\"x2010\":\"25\"},{\"nombre\":\"Sede principal: Medicina\",\"x2014\":\"19\",\"x2013\":\"19\",\"x2012\":\"10\",\"x2011\":\"10\",\"x2010\":\"24\"},{\"nombre\":\"Sede principal: Enfermer\\u00eda\",\"x2014\":\"50\",\"x2013\":\"50\",\"x2012\":\"50\",\"x2011\":\"50\",\"x2010\":\"50\"}],\"titulados\":[{\"nombre\":\"Sede principal: Ingenier\\u00eda Industrial\",\"x2014\":\"11\",\"x2013\":\"11\",\"x2012\":\"11\",\"x2011\":\"11\",\"x2010\":\"11\"},{\"nombre\":\"Sede principal: Ingenier\\u00eda Mec\\u00e1nica\",\"x2014\":\"22\",\"x2013\":\"22\",\"x2012\":\"22\",\"x2011\":\"22\",\"x2010\":\"22\"},{\"nombre\":\"Sede principal: Medicina\",\"x2014\":\"9\",\"x2013\":\"9\",\"x2012\":\"9\",\"x2011\":\"9\",\"x2010\":\"9\"},{\"nombre\":\"Sede principal: Enfermer\\u00eda\",\"x2014\":\"30\",\"x2013\":\"30\",\"x2012\":\"30\",\"x2011\":\"30\",\"x2010\":\"30\"}],\"carrera_area\":\"EDUCACI\\u00d3N\",\"datos\":{\"inst\":{\"nombre\":\"Dr. Diego D\\u00edaz\",\"cargo\":\"Rector\",\"direccion\":\"Av. Siempre Viva #437\",\"fijo\":\"235-6231\",\"celular\":\"945178243\",\"email\":\"ddiaz@universidad.com\",\"emailalt\":\"ddiaz2512@gmail.com\",\"date\":\"2015-05-21\",\"date_local\":\"21 de Mayo del 2015\"},\"carr\":{\"nombre\":\"Xavi Hern\\u00e1ndez\",\"cargo\":\"Decano\",\"direccion\":\"Av. Cattar 485\",\"fijo\":\"452-6531\",\"celular\":\"968472123\",\"email\":\"xavi@universidad.com\",\"emailalt\":\"\",\"date\":\"2015-05-21\",\"date_local\":\"21 de Mayo del 2015\"}},\"save_only\":\"1\"}','',6,NULL),(8,10,NULL,377,'{\"acet_etapa\":\"1\",\"dimensiones\":{\"1\":{\"objetivo\":\"qwqwqwqw\"},\"2\":{\"objetivo\":\"qwqwq\"},\"3\":{\"objetivo\":\"qwqwqw\"}},\"factores\":{\"1\":{\"resultado\":\"qwqwqw\"},\"2\":{\"resultado\":\"qwqwqwqw\"},\"5\":{\"resultado\":\"qwqwqw\"},\"6\":{\"resultado\":\"qwqwqwqwqwqwqw\"},\"7\":{\"resultado\":\"qwqwqwqw\"},\"8\":{\"resultado\":\"qwqwqwqw\"},\"9\":{\"resultado\":\"qwqwqwqw\"}},\"actividades\":{\"1\":[{\"miactividad\":\"Factor 1 actividad 1 .\",\"costototal\":\"100\",\"costofec\":\"100\",\"radiocosto\":\"FEC\",\"actor_cargo\":\"wwewewe\",\"ejecucion\":\"1\",\"estandares\":[\"1586\",\"1594\"]}],\"2\":[{\"miactividad\":\"Factor 2 actividad 1\",\"costototal\":\"100\",\"costofec\":\"100\",\"radiocosto\":\"FEC\",\"actor_cargo\":\"wwewe\",\"ejecucion\":\"11\",\"estandares\":[\"1597\",\"1613\"]}],\"5\":[{\"miactividad\":\"actividad tercera\",\"costototal\":\"100\",\"costofec\":\"100\",\"radiocosto\":\"FEC\",\"actor_cargo\":\"wewewewew\",\"ejecucion\":\"1\",\"estandares\":[\"1649\",\"1655\",\"1656\",\"1657\",\"1658\",\"1661\"]}],\"6\":[{\"miactividad\":\"actividad cuarta\",\"costototal\":\"100\",\"costofec\":\"100\",\"radiocosto\":\"FEC\",\"actor_cargo\":\"\",\"ejecucion\":\"\",\"estandares\":[\"1664\",\"1665\"]}],\"7\":[{\"miactividad\":\"actividad 5\",\"costototal\":\"100\",\"costofec\":\"100\",\"radiocosto\":\"FEC\",\"actor_cargo\":\"\",\"ejecucion\":\"\",\"estandares\":[\"1667\",\"1669\",\"1670\"]}],\"8\":[{\"miactividad\":\"actividad 6\",\"costototal\":\"100\",\"costofec\":\"100\",\"radiocosto\":\"FEC\",\"actor_cargo\":\"\",\"ejecucion\":\"\",\"estandares\":[\"1673\",\"1674\"]}],\"9\":[{\"miactividad\":\"actividad 7\",\"costototal\":\"100\",\"costofec\":\"100\",\"radiocosto\":\"FEC\",\"actor_cargo\":\"\",\"ejecucion\":\"\",\"estandares\":[\"1677\"]}]}}','{\"save_only\":\"1\",\"acet_etapa\":\"1\",\"dimensiones\":{\"1\":{\"objetivo\":\"qwqwqwqw\"},\"2\":{\"objetivo\":\"qwqwq\"},\"3\":{\"objetivo\":\"qwqwqw\"}},\"factores\":{\"1\":{\"resultado\":\"qwqwqw\"},\"2\":{\"resultado\":\"qwqwqwqw\"},\"5\":{\"resultado\":\"qwqwqw\"},\"6\":{\"resultado\":\"qwqwqwqwqwqwqw\"},\"7\":{\"resultado\":\"qwqwqwqw\"},\"8\":{\"resultado\":\"qwqwqwqw\"},\"9\":{\"resultado\":\"qwqwqwqw\"}},\"actividades\":{\"1\":[{\"miactividad\":\"Factor 1 actividad 1 .\",\"gastos\":[{\"gasto\":\"11\",\"tipo\":\"12\",\"item\":\"2\",\"unidad\":\"decena\",\"costounitario\":\"2800\",\"cantidad\":\"15\",\"subtotal\":\"15000\"},{\"gasto\":\"12\",\"tipo\":\"999\",\"tipogasto\":\"S.O. Linux vxxx\",\"unidad\":\"\",\"costounitario\":\"\",\"cantidad\":\"\",\"subtotal\":\"\"}]}],\"2\":[{\"miactividad\":\"Factor 2 actividad 1\",\"gastos\":[{\"gasto\":\"\",\"tipo\":\"\",\"item\":\"\",\"unidad\":\"\",\"costounitario\":\"\",\"cantidad\":\"\",\"subtotal\":\"\"}]}],\"5\":[{\"miactividad\":\"actividad tercera\",\"gastos\":[{\"gasto\":\"\",\"tipo\":\"\",\"item\":\"\",\"unidad\":\"\",\"costounitario\":\"\",\"cantidad\":\"\",\"subtotal\":\"\"}]}],\"6\":[{\"miactividad\":\"actividad cuarta\",\"gastos\":[{\"gasto\":\"\",\"tipo\":\"\",\"item\":\"\",\"unidad\":\"\",\"costounitario\":\"\",\"cantidad\":\"\",\"subtotal\":\"\"}]}],\"7\":[{\"miactividad\":\"actividad 5\",\"gastos\":[{\"gasto\":\"\",\"tipo\":\"\",\"item\":\"\",\"unidad\":\"\",\"costounitario\":\"\",\"cantidad\":\"\",\"subtotal\":\"\"}]}],\"8\":[{\"miactividad\":\"actividad 6\",\"gastos\":[{\"gasto\":\"\",\"tipo\":\"\",\"item\":\"\",\"unidad\":\"\",\"costounitario\":\"\",\"cantidad\":\"\",\"subtotal\":\"\"}]}],\"9\":[{\"miactividad\":\"actividad 7\",\"gastos\":[{\"gasto\":\"13\",\"tipo\":\"\",\"item\":\"\",\"unidad\":\"\",\"costounitario\":\"\",\"cantidad\":\"\",\"subtotal\":\"\"}]}]}}',7,NULL);
/*!40000 ALTER TABLE `acreditado_etapa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acreditado_evento`
--

DROP TABLE IF EXISTS `acreditado_evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acreditado_evento` (
  `acev_id` int(11) NOT NULL AUTO_INCREMENT,
  `acre_id` int(11) NOT NULL,
  `even_id` int(11) NOT NULL,
  PRIMARY KEY (`acev_id`),
  KEY `fk_acreditado_evento_acreditado1_idx` (`acre_id`),
  KEY `fk_acreditado_evento_evento1_idx` (`even_id`),
  CONSTRAINT `fk_acreditado_evento_acreditado1` FOREIGN KEY (`acre_id`) REFERENCES `acreditado` (`acre_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_acreditado_evento_evento1` FOREIGN KEY (`even_id`) REFERENCES `evento` (`even_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=378 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acreditado_evento`
--

LOCK TABLES `acreditado_evento` WRITE;
/*!40000 ALTER TABLE `acreditado_evento` DISABLE KEYS */;
INSERT INTO `acreditado_evento` VALUES (1,3,9),(2,4,9),(4,18651,8),(318,18651,8),(377,18651,6);
/*!40000 ALTER TABLE `acreditado_evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acreditado_nivel_etapa`
--

DROP TABLE IF EXISTS `acreditado_nivel_etapa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acreditado_nivel_etapa` (
  `acne_id` int(11) NOT NULL,
  `acne_aprobado` int(11) DEFAULT NULL,
  `acne_comentario` varchar(500) DEFAULT NULL,
  `eval_id` int(11) NOT NULL,
  `acet_id` int(11) NOT NULL,
  PRIMARY KEY (`acne_id`),
  KEY `fk_acreditado_nivel_evaluador1_idx` (`eval_id`),
  KEY `fk_acreditado_nivel_etapa_acreditado_etapa1_idx` (`acet_id`),
  CONSTRAINT `fk_acreditado_nivel_etapa_acreditado_etapa1` FOREIGN KEY (`acet_id`) REFERENCES `acreditado_etapa` (`acet_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_acreditado_nivel_evaluador1` FOREIGN KEY (`eval_id`) REFERENCES `evaluador` (`eval_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acreditado_nivel_etapa`
--

LOCK TABLES `acreditado_nivel_etapa` WRITE;
/*!40000 ALTER TABLE `acreditado_nivel_etapa` DISABLE KEYS */;
/*!40000 ALTER TABLE `acreditado_nivel_etapa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento`
--

DROP TABLE IF EXISTS `documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento` (
  `docu_id` int(11) NOT NULL,
  `docu_nombre` varchar(45) DEFAULT NULL,
  `docu_fecha` datetime DEFAULT NULL,
  `acne_id` int(11) NOT NULL,
  PRIMARY KEY (`docu_id`),
  KEY `fk_documento_acreditado_nivel_etapa1_idx` (`acne_id`),
  CONSTRAINT `fk_documento_acreditado_nivel_etapa1` FOREIGN KEY (`acne_id`) REFERENCES `acreditado_nivel_etapa` (`acne_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento`
--

LOCK TABLES `documento` WRITE;
/*!40000 ALTER TABLE `documento` DISABLE KEYS */;
/*!40000 ALTER TABLE `documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evaluador`
--

DROP TABLE IF EXISTS `evaluador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evaluador` (
  `eval_id` int(11) NOT NULL,
  `eval_nombre` varchar(45) DEFAULT NULL,
  `eval_dni` int(11) DEFAULT NULL,
  `eval_perfil` enum('directorio','normal') DEFAULT NULL,
  PRIMARY KEY (`eval_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evaluador`
--

LOCK TABLES `evaluador` WRITE;
/*!40000 ALTER TABLE `evaluador` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `even_id` int(11) NOT NULL AUTO_INCREMENT,
  `even_url_pdf` text,
  `even_descripcion` text,
  PRIMARY KEY (`even_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
INSERT INTO `evento` VALUES (1,NULL,NULL),(2,NULL,NULL),(4,NULL,NULL),(5,NULL,NULL),(6,'www.procalidad.gob.pe/documentos/concurso-fec/3convocatoria/Bases-TERCER-CONCURSO-FINANCIAMIENTO-PM-EE-14012015-V2.pdf','La mejora continua de la calidad de la educación superior es un reto que el Perú afronta para contribuir al incremento de su competitividad. En ese marco, el Proyecto “Mejoramiento de la Calidad de la Educación Superior” – PROCALIDAD propone impulsar los procesos de acreditación de las Instituciones de Educación Superior.  El Fondo de Estímulo a la Calidad - FEC, es el medio a través del cual PROCALIDAD otorga incentivos financieros a las Instituciones Públicas de Educación Superior (Institutos y Universidades) que ofrecen carreras de formación docente, ciencias de la salud y, ciencia y tecnología (incentivo a las carreras); así como para las instituciones propiamente dichas (incentivo a la institución), que se encuentran realizando esfuerzos por lograr su acreditación o buscar su excelencia. '),(7,NULL,NULL),(8,'www.procalidad.gob.pe/documentos/concurso-fec/3convocatoria/Bases-TERCER-CONCURSO-FINANCIAMIENTO-PM-EE-14012015-V2.pdf','La mejora continua de la calidad de la educación superior es un reto que el Perú afronta para contribuir al incremento de su competitividad. En ese marco, el Proyecto “Mejoramiento de la Calidad de la Educación Superior” – PROCALIDAD propone impulsar los procesos de acreditación de las Instituciones de Educación Superior.  El Fondo de Estímulo a la Calidad - FEC, es el medio a través del cual PROCALIDAD otorga incentivos financieros a las Instituciones Públicas de Educación Superior (Institutos y Universidades) que ofrecen carreras de formación docente, ciencias de la salud y, ciencia y tecnología (incentivo a las carreras); así como para las instituciones propiamente dichas (incentivo a la institución), que se encuentran realizando esfuerzos por lograr su acreditación o buscar su excelencia. '),(9,'www.procalidad.gob.pe/documentos/concurso-fec/3convocatoria/Bases-TERCER-CONCURSO-FINANCIAMIENTO-PM-EE-14012015-V2.pdf','La mejora continua de la calidad de la educación superior es un reto que el Perú afronta para contribuir al incremento de su competitividad. En ese marco, el Proyecto “Mejoramiento de la Calidad de la Educación Superior” – PROCALIDAD propone impulsar los procesos de acreditación de las Instituciones de Educación Superior.El Fondo de Estímulo a la Calidad - FEC, es el medio a través del cual PROCALIDAD otorga incentivos financieros a las Instituciones Públicas de Educación Superior (Institutos y Universidades) que ofrecen carreras de formación docente, ciencias de la salud y, ciencia y tecnología (incentivo a las carreras); así como para las instituciones propiamente dichas (incentivo a la institución), que se encuentran realizando esfuerzos por lograr su acreditación o buscar su excelencia. ');
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento_etapa`
--

DROP TABLE IF EXISTS `evento_etapa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento_etapa` (
  `evet_id` int(11) NOT NULL,
  `evet_nombre` varchar(45) DEFAULT NULL,
  `evet_fecha_inicio_vigencia` date DEFAULT NULL,
  `evet_fecha_fin_vigencia` date DEFAULT NULL,
  `evet_fecha_inicio_correccion` date DEFAULT NULL,
  `evet_fecha_fin_correccion` date DEFAULT NULL,
  `even_id` int(11) NOT NULL,
  PRIMARY KEY (`evet_id`),
  KEY `fk_evento_etapa_evento1_idx` (`even_id`),
  CONSTRAINT `fk_evento_etapa_evento1` FOREIGN KEY (`even_id`) REFERENCES `evento` (`even_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento_etapa`
--

LOCK TABLES `evento_etapa` WRITE;
/*!40000 ALTER TABLE `evento_etapa` DISABLE KEYS */;
INSERT INTO `evento_etapa` VALUES (1,'Desarrollo del Proceso de Autoevaluación','2015-04-06','2015-04-10','2015-04-13','2015-04-17',9),(2,'Registro de Expresión de Interés','2015-04-13','2015-04-17','2015-04-20','2015-04-24',9),(3,'Registro de Planes de Mejora','2015-04-20','2015-04-24','2015-04-26','2015-04-30',9),(4,'Registro de Documentación Institucional','2015-05-01','2015-05-04','2015-04-06','2015-04-10',9),(5,'Desarrollo del Proceso de Autoevaluación','2015-05-06','2015-05-10','2015-04-13','2015-04-17',6),(6,'Registro de Expresión de Interés','2015-05-13','2015-05-17','2015-04-20','2015-04-24',6),(7,'Registro de Planes de Mejora','2015-05-27','2015-06-01','2015-04-26','2015-04-30',6),(8,'Documentación Institucional Complementaria','2015-06-02','2015-06-09','2015-05-06','2015-05-10',6),(9,'Documentación Institucional Básica','2015-05-20','2015-05-24',NULL,NULL,6);
/*!40000 ALTER TABLE `evento_etapa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento_nivel`
--

DROP TABLE IF EXISTS `evento_nivel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento_nivel` (
  `evni_id` int(11) NOT NULL,
  `evni_nombre` varchar(45) DEFAULT NULL,
  `evni_fecha_inicio_evaluado` datetime DEFAULT NULL,
  `evni_fecha_fin_evaluado` datetime DEFAULT NULL,
  `even_id` int(11) NOT NULL,
  PRIMARY KEY (`evni_id`),
  KEY `fk_evento_nivel_evento1_idx` (`even_id`),
  CONSTRAINT `fk_evento_nivel_evento1` FOREIGN KEY (`even_id`) REFERENCES `evento` (`even_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento_nivel`
--

LOCK TABLES `evento_nivel` WRITE;
/*!40000 ALTER TABLE `evento_nivel` DISABLE KEYS */;
/*!40000 ALTER TABLE `evento_nivel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `financiamiento`
--

DROP TABLE IF EXISTS `financiamiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `financiamiento` (
  `fina_id` int(11) NOT NULL,
  `fina_nombre` varchar(45) DEFAULT NULL,
  `fina_monto_solicitado` int(11) DEFAULT NULL,
  `fina_monto_financiar` int(11) DEFAULT NULL,
  `fina_fecha` varchar(45) DEFAULT NULL,
  `acev_id` int(11) NOT NULL,
  PRIMARY KEY (`fina_id`),
  KEY `fk_financiamiento_acreditado_evento1_idx` (`acev_id`),
  CONSTRAINT `fk_financiamiento_acreditado_evento1` FOREIGN KEY (`acev_id`) REFERENCES `acreditado_evento` (`acev_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `financiamiento`
--

LOCK TABLES `financiamiento` WRITE;
/*!40000 ALTER TABLE `financiamiento` DISABLE KEYS */;
/*!40000 ALTER TABLE `financiamiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gasto_elegible`
--

DROP TABLE IF EXISTS `gasto_elegible`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gasto_elegible` (
  `gael_id` int(11) NOT NULL AUTO_INCREMENT,
  `gael_nombre` varchar(200) DEFAULT NULL,
  `grup_id` int(11) NOT NULL,
  PRIMARY KEY (`gael_id`),
  KEY `fk_gasto_elegible_grupo1_idx` (`grup_id`),
  CONSTRAINT `fk_gasto_elegible_grupo1` FOREIGN KEY (`grup_id`) REFERENCES `grupo` (`grup_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gasto_elegible`
--

LOCK TABLES `gasto_elegible` WRITE;
/*!40000 ALTER TABLE `gasto_elegible` DISABLE KEYS */;
INSERT INTO `gasto_elegible` VALUES (1,'Pasantía',1),(2,'Capacitación Docente',1),(3,'Entrenamiento / Taller',1),(4,'Estudios de reforma académica (identificación y diseño organizativo)',2),(5,'Modificaciones curriculares',2),(6,'Planes de mejora de procesos o estrategias pedagógicas',2),(7,'Capacitación \"in house\" uso de equipos educativos y operatividad de bibliotecas',2),(8,'Pago de docentes visitantes para mejora de estándares y criterios de evaluación académica',2),(9,'Maquinaria y Equipo',3),(10,'Mobiliario y enseres',3),(11,'Hardware',3),(12,'Software',3),(13,'Otros bienes',3),(14,'Instalación de equipos en edificios existentes',4),(15,'Mejora en infraestructura educativa o intervenciones menores',4),(16,'Bienes y Servicios para el desarrollo de procesos académicos y de gestión',5),(17,'Bienes y servicios para la estandarización, normalización y certificación de procesos aplicables a los PMI',5),(18,'Reparación de equipos',3),(999,'No Financiable por FEC',6);
/*!40000 ALTER TABLE `gasto_elegible` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo` (
  `grup_id` int(11) NOT NULL AUTO_INCREMENT,
  `grup_nombre` varchar(250) DEFAULT NULL,
  `evet_id` int(11) NOT NULL,
  PRIMARY KEY (`grup_id`),
  KEY `fk_grupo_evento_etapa1_idx` (`evet_id`),
  CONSTRAINT `fk_grupo_evento_etapa1` FOREIGN KEY (`evet_id`) REFERENCES `evento_etapa` (`evet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
INSERT INTO `grupo` VALUES (1,'Estímulos para la formación de recursos humanos',7),(2,'Servicios de Consultoría para la mejora o impulso a reformas académicas y de gestión',7),(3,'Equipamiento',7),(4,'Obras menores',7),(5,'Fortalecimiento Institucional',7),(6,'Otros',7);
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_gasto`
--

DROP TABLE IF EXISTS `item_gasto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_gasto` (
  `itga_id` int(11) NOT NULL AUTO_INCREMENT,
  `itga_nombre` varchar(200) DEFAULT NULL,
  `itga_descripcion` text,
  `itga_precio` int(11) DEFAULT NULL,
  `itga_moneda` varchar(45) DEFAULT NULL,
  `itga_path` varchar(250) DEFAULT NULL,
  `itga_estado` int(11) DEFAULT NULL,
  `itga_fecha_creacion` date DEFAULT NULL,
  `itga_vigencia` varchar(45) DEFAULT NULL,
  `tiga_id` int(11) NOT NULL,
  `prov_id` int(11) NOT NULL,
  `itga_unidad` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`itga_id`),
  KEY `fk_item_gasto_tipo_gasto1_idx` (`tiga_id`),
  KEY `fk_item_gasto_proveedor1_idx` (`prov_id`),
  CONSTRAINT `fk_item_gasto_proveedor1` FOREIGN KEY (`prov_id`) REFERENCES `proveedor` (`prov_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_gasto_tipo_gasto1` FOREIGN KEY (`tiga_id`) REFERENCES `tipo_gasto` (`tiga_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_gasto`
--

LOCK TABLES `item_gasto` WRITE;
/*!40000 ALTER TABLE `item_gasto` DISABLE KEYS */;
INSERT INTO `item_gasto` VALUES (2,'Core I7 - 4ta generación - 8GB RAM - 1GB Video','Computadora de escritorio, con procesador I7 de cuarta generación, con 1Gb de memoria RAM, 8 GBs de video, con monitor Samsung LED de 21\"',2800,'nuevos soles','FICHA001.pdf',1,NULL,'2 meses',12,1,'unidad'),(3,'Core I5 - 3ra generación - 4GB RAM - 500MB Video','Computadora de escritorio, con procesador I5 de tercera generación, con 4Gb de memoria RAM, 500 Mb de video, con monitor Samsung LED de 19\"',2100,NULL,'corei5.pdf',1,NULL,'3 meses',12,1,'unidad'),(4,'AMD A6 - 4Gb RAM - 1 Gb Video','Computadora de escritorio, con procesador AMD A6, con 4 Gb de memoria RAM, 1 Gb de video, con monitor Samsung LED de 22\"',2600,NULL,'amda6.pdf',1,NULL,'1 mes',12,1,'unidad'),(5,'Impresora Epson L 800','Impresora Epson L800, 37 páginas por minuto, USB 2.0, color negro',1500,NULL,'epson.pdf',1,NULL,'2 meses',13,1,'unidad'),(6,'Impresora Brother Inalámbrica','Impresora Brother Inalámbrica,  Láser Color  HL - 4570, 30 páginas por minuto',2000,NULL,'brother.pdf',1,NULL,'3 meses',13,1,'unidad'),(7,'Impresora Samsung Multifuncional','Impresora Samsung Multifuncional, Láser CLX-3185W - Negro, a colores y ambos lados.',1200,NULL,'samsung.pdf',1,NULL,'2 meses',13,1,'unidad'),(8,'Escáner fotográfico HP G3110','Escáner fotográfico HP G3110, óptica hasta 4800 ppp, conectividad USB 2.0',600,NULL,'hp.pdf',1,NULL,'4 meses',14,1,'unidad'),(9,'Escáner Brother color ADS - 2000 - negro','Escáner Brother color ADS - 2000 - negro',900,NULL,'escaner.pdf',1,NULL,'2 meses',14,1,'unidad'),(10,'Escáner Konica Minolta','Escáner Konica Minolta Bizhub 552 negro, Resolución de Scanner Máximo 600x600 dpi',2200,NULL,'konica.pdf',1,NULL,'1 mes',14,1,'unidad'),(11,'Escritorio 3 cajones','Escritorio Paris blanco 3 cajones 130x62x74 cm  Tvilum',300,NULL,'escritorio.pdf',1,NULL,'4 meses',31,1,'unidad'),(12,'Estante','Estante 3 repisas 2 puertas marrón  Moduart',400,NULL,'estante.pdf',1,NULL,'4 meses',31,1,'unidad'),(13,'Sillón','Sillón gerencial marrón  Asenti',350,NULL,'sillon.pdf',1,NULL,'4 meses',31,1,'unidad'),(14,'Pizarra interactiva','Display Interactivo CTOUCH de 84 pulgadas',4000,NULL,'pizarra.pdf',1,NULL,'2 meses',32,1,'unidad'),(15,'Carpeta Personal','Carpeta para Estudiante con Tablero de Melamina su asiento de Melamina en Tubo Redondo de Calidad',1500,NULL,'carpeta.pdf',1,NULL,'6 meses',32,1,'decena'),(16,'Pizarra Acrílica','Pizarra Acrilica 2.50 X 1.25 Mts con Parantes',300,NULL,'acrilica.pdf',1,NULL,'6 meses',32,1,'unidad'),(17,'Centrifuga','Centrifuga Digital 12 Tubos 80-2c Max.',2000,NULL,'centrifuga.pdf',1,NULL,'3 meses',33,1,'unidad'),(18,'Microscopio','Microscopio Binocular Novex 86.025-led',3000,NULL,'microscopio.pdf',1,NULL,'3 meses',33,1,'unidad'),(19,'Tubos de ensayo','Tubos De Ensayo Vidrio X 12unidades C/corcho + Pergaminos',500,NULL,'tubo.pdf',1,NULL,'2 meses',33,1,'docena');
/*!40000 ALTER TABLE `item_gasto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `prov_id` int(11) NOT NULL AUTO_INCREMENT,
  `prov_nombre` varchar(250) DEFAULT NULL,
  `prov_razon_social` varchar(250) DEFAULT NULL,
  `prov_ruc` varchar(11) DEFAULT NULL,
  `prov_direccion` varchar(250) DEFAULT NULL,
  `prov_rubro` varchar(250) DEFAULT NULL,
  `prov_contacto` varchar(250) DEFAULT NULL,
  `prov_celular` varchar(9) DEFAULT NULL,
  `prov_antiguedad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`prov_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (1,'ITG','ITG Solutions','20202020202','Av. Garcilazo de la Vega 1231 - Lima Cercado','TI','Ángel Robles','987987987','4 años');
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_gasto`
--

DROP TABLE IF EXISTS `tipo_gasto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_gasto` (
  `tiga_id` int(11) NOT NULL AUTO_INCREMENT,
  `tiga_nombre` varchar(200) DEFAULT NULL,
  `gael_id` int(11) NOT NULL,
  PRIMARY KEY (`tiga_id`),
  KEY `fk_tipo_gasto_gasto_elegible1_idx` (`gael_id`),
  CONSTRAINT `fk_tipo_gasto_gasto_elegible1` FOREIGN KEY (`gael_id`) REFERENCES `gasto_elegible` (`gael_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_gasto`
--

LOCK TABLES `tipo_gasto` WRITE;
/*!40000 ALTER TABLE `tipo_gasto` DISABLE KEYS */;
INSERT INTO `tipo_gasto` VALUES (1,'Pasajes',1),(2,'Alojamiento (max. 5 días)',1),(3,'Alimentación (max. 5 días)',1),(4,'Pasajes',2),(5,'Alojamiento (max. 5 días)',2),(6,'Alimentación (max. 5 días)',2),(7,'Costos de formación (Inscripción)',2),(8,'Pasajes',3),(9,'Alojamiento (max. 5 días)',3),(10,'Alimentación (max. 5 días)',3),(11,'Costo de taller',3),(12,'Computadora(PC)',11),(13,'Impresora',11),(14,'Escáner',11),(31,'Oficina',9),(32,'Aula',9),(33,'Laboratorio',9);
/*!40000 ALTER TABLE `tipo_gasto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) DEFAULT NULL,
  `clave` varchar(45) DEFAULT NULL,
  `perfil` int(11) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='						';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,NULL,1,NULL),(2,NULL,NULL,1,NULL),(3,NULL,NULL,1,NULL),(4,NULL,NULL,1,NULL),(5,NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-21 11:46:31
