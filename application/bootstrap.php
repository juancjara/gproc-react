<?php defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------

// Load the core Kohana class
require SYSPATH.'classes/Kohana/Core'.EXT;

if (is_file(APPPATH.'classes/Kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/Kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/Kohana'.EXT;
}

/**
 * Set the default time zone.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/timezones
 */
date_default_timezone_set('America/Lima');

/**
 * Set the default locale.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/function.setlocale
 */
setlocale(LC_ALL, 'es_PE.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @link http://kohanaframework.org/guide/using.autoloading
 * @link http://www.php.net/manual/function.spl-autoload-register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Optionally, you can enable a compatibility auto-loader for use with
 * older modules that have not been updated for PSR-0.
 *
 * It is recommended to not enable this unless absolutely necessary.
 */
//spl_autoload_register(array('Kohana', 'auto_load_lowercase'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @link http://www.php.net/manual/function.spl-autoload-call
 * @link http://www.php.net/manual/var.configuration#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

/**
 * Set the mb_substitute_character to "none"
 *
 * @link http://www.php.net/manual/function.mb-substitute-character.php
 */
mb_substitute_character('none');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('en-us');

if (isset($_SERVER['SERVER_PROTOCOL']))
{
	// Replace the default protocol.
	HTTP::$protocol = $_SERVER['SERVER_PROTOCOL'];
}

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV']))
{
	Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - integer  cache_life  lifetime, in seconds, of items cached              60
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 * - boolean  expose      set the X-Powered-By header                        FALSE
 */
Kohana::init(array(
	'base_url'   => '/',
	'index_file' => FALSE,
	'caching'    => Kohana::$environment === Kohana::PRODUCTION,
	'profile'    => Kohana::$environment !== Kohana::PRODUCTION,
	'errors'     => TRUE,
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH.'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */

Kohana::modules(array(
	'auth'       => MODPATH.'auth',       // Basic authentication
	// 'cache'      => MODPATH.'cache',      // Caching with multiple backends
	// 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
	'database'   => MODPATH.'database',   // Database access
	// 'image'      => MODPATH.'image',      // Image manipulation
	// 'minion'     => MODPATH.'minion',     // CLI Tasks
	'orm'        => MODPATH.'orm',        // Object Relationship Mapping
	// 'unittest'   => MODPATH.'unittest',   // Unit testing
	'userguide'  => MODPATH.'userguide',  // User guide and API documentation
	'admin'      => MODPATH.'admin',      // Admin
	//'email'      => MODPATH.'email',      // Email
	'kohana-acl'      => MODPATH.'kohana-acl',      // kohana-acl
	'common'     => MODPATH.'common',     // Common library
	));

/**
 * Cookie Salt
 * @see  http://kohanaframework.org/3.3/guide/kohana/cookies
 * 
 * If you have not defined a cookie salt in your Cookie class then
 * uncomment the line below and define a preferrably long salt.
 */
// Cookie::$salt = NULL;
 Cookie::$salt = 'gproc';

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
/*Route::set('default', '(<controller>(/<action>(/<id>)))')
	->defaults(array(
		'controller' => 'welcome',
		'action'     => 'index',
	));

*/
//AGREGADO PARA AJAX FICHAS
Route::set('foo', '(<controller>(/<action>(/<id>)))', array(
		'controller' => '(welcome|file|ajax|api)',
))
	->defaults(array(
		'controller' => 'Welcome',
));

Route::set('files', 'file/<id>', array(
		'id' => '.*',
	))->defaults(array(
		'controller' => 'File',
		'action'     => 'get',
	));

Route::set('backend', 'admin(/<controller>(/<action>(/<id>)))', array('id' => '.+'))
	->defaults(array(
		'directory'	 => 'Admin',
		'controller' => 'Usuarios',
		'action'     => 'index',
	));

Route::set('evaluador', 'evaluador(/<controller>(/<action>(/<id>)))')
	->defaults(array(
		'directory'	 => 'Evaluador',
		'controller' => 'verEventos',
		'action'     => 'index',
	));

Route::set('welcome', '(welcome(/<action>))')
	->defaults(array(
		'controller' => 'Welcome',
		'action'     => 'index',
	));

// @TEMP
Route::set('node', 'node(/<action>(/<id>))')
	->defaults(array(
		'controller' => 'Node',
		'action'     => 'index',
	));

Route::set('default', '<controller>(/<action>(/<id>))')
	->defaults(array(
		'directory'  => 'Regular',
		'controller' => 'Etapas',
		'action'     => 'index',
	));


// @TEST



// Attach Config Database
//Kohana::$config->attach(new Config_Database);






// EOL's
define('M_EOL', "\r\n");
define('H_EOL', "<br>");

// Debug
function debug($var, $die = TRUE)
{
	echo Debug::vars($var);
	if ($die) die();
}

// Debug2
function debug2()
{
	foreach (func_get_args() as $arg)
	{
		if ($arg === 'die') continue;
		echo Debug::vars($arg);
	}
	
	if ($arg === 'die') die();
}

// Profile
function profile()
{
	echo View::factory('profiler/stats');
	die();
}

// Number format
function numberformat($number, $decimals = 0, $dec = '.', $tho = '')
{
	return is_numeric($number) ? number_format($number, $decimals, $dec, $tho) : $number;
}

function numbercommaformat($number, $decimals = 2, $dec = '.', $tho = ',')
{
	return is_numeric($number) ? number_format($number, $decimals, $dec, $tho) : $number;
}

//Convert numeric
function convert_numeric($string)
{
	return is_string($string) ? floatval($string) : $string;
}

// Replace string single quotes
function replace($string)
{
	return str_replace("'", '`', $string);
}

function dateformat($date)
{
	return strtr($date, array(
		'January'   => 'Enero',
		'February'  => 'Febrero',
		'March'     => 'Marzo',
		'April'     => 'Abril',
		'May'       => 'Mayo',
		'June'      => 'Junio',
		'July'      => 'Julio',
		'August'    => 'Agosto',
		'September' => 'Septiembre',
		'October'   => 'Octubre',
		'November'  => 'Noviembre',
		'December'  => 'Diciembre',
		'Jan' => 'Ene',
		'Apr' => 'Abr',
		'Aug' => 'Ago',
		'Dec' => 'Dic',
	));
}

function partial_function()
{
	$applied_args = func_get_args();

	return function() use ($applied_args)
		{
			return call_user_func_array('call_user_func', array_merge($applied_args, func_get_args()));
		};
}

//Kohana::$environment = Kohana::PRODUCTION;

if (Kohana::$environment <= Kohana::STAGING)
{
	Kohana_Exception::$error_view = 'template/errors';
}

Kohana::$server_name = $_SERVER['SERVER_NAME'];

define('SERVER_PROD', 'gproc.procalidad.gob.pe');
define('SERVER_DEMO', 'gprocdemo.procalidad.gob.pe');
define('SERVER_DEV',  'gproc.dev');

define('GPROC_BUILD', 'v2.1.8991');

define('MEDIA_PATH', '/media');
