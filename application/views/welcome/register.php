<form class="form-sign signup" role="form" method="post" enctype="multipart/form-data">
	
	<h3 class="heading">
		Sistema de Autoevaluación de la Educación Superior - 
		<a href="#">SAES</a>
	</h3>
	
	<img src="/media/gproc/img/logo.jpg" class="logo img-responsive" alt="<?= __($site_title) ?>">
	
	<?php if ($error): ?>
		<div class="alert alert-danger"><?= $error ?></div>
	<?php endif ?>
	<hr>
	
	<fieldset>
		<legend><?= __('Acreditación') ?></legend>
		
		<div class="row">
			<div class="col-sm-6 form-group">
				<select name="tiin_id" class="form-control control-sign placeholder ajax" required
						data-action="getInstitution" data-target="#signup-institution">
					<?= HTML::default_option('Seleccione tipo de institución') ?>
					<?php foreach ($aTipoInstitucion as $key => $val): ?>
						<option value="<?= $key ?>"><?= $val ?></option>
					<?php endforeach ?>
				</select>
			</div>
			<div class="col-sm-6 form-group">
				<select name="inst_id" class="form-control control-sign placeholder ajax" required id="signup-institution" 
						data-action="getObac" data-target="#signup-obac">
					<?= HTML::default_option('Seleccione institución') ?>
				</select>
			</div>
			<div class="col-sm-6 form-group">
				<select name="obac_id" class="form-control control-sign placeholder ajax" required id="signup-obac" 
						data-action="getCareerObac" data-target="#signup-career">
					<?= HTML::default_option('Seleccione objeto de acreditación') ?>
				</select>
			</div>
			<div class="col-sm-6 form-group">
				<select name="carr_id" class="form-control control-sign placeholder" required id="signup-career">
					<?= HTML::default_option('Seleccione carrera') ?>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 form-group">
				<select name="moda_id" class="form-control control-sign placeholder" required>
					<?= HTML::default_option('Seleccione el estado de acreditación') ?>
					<?php foreach ($aModalidad as $key => $val): ?>
						<option value="<?= $key ?>"><?= $val ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
		
		<!--p class="help-block">
			Si no encuentra su carrera registrada, puede solicitarla <a href="/welcome/carrera">aquí</a>
		</p-->
		
	</fieldset>
	
	<fieldset>
		<legend><?= __('Comité de Calidad') ?></legend>
		<div class="table-responsive">
		<table class="table committee">
			<thead>
				<tr>
					<th><?= __('Contacto') ?></th>
					<th><?= __('Nombres') ?></th>
					<th><?= __('Apellidos') ?></th>
					<th><?= __('Cargo') ?></th>
					<th><?= __('Correo') ?></th>
					<th><?= __('Teléfono') ?></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><div class="radio text-center"><input name="contact" value="0" type="radio" required></div></td>
					<td><input type="text" name="first_names[]" class="form-control"></td>
					<td><input type="text" name="last_names[]" class="form-control"></td>
					<td><input type="text" name="positions[]" class="form-control"></td>
					<td><input type="text" name="emails[]" class="form-control"></td>
					<td><input type="text" name="phones[]" class="form-control"></td>
				</tr>
			</tbody>
		</table>
		</div>
		
		<button class="btn btn-default pull-right" type="button" title="<?= __('Agregar miembro') ?>" id="signup-member-add">
			<span class="glyphicon glyphicon-plus"></span>
		</button>
		
		<div class="clearfix"></div><br>
		
		<div class="form-group">
			<label for="file">Adjuntar Documento que autoriza la Creación del Comité de Autoevaluación/Calidad</label>
			<input name="file" type="file" id="file" class="form-control" required>
		</div>
		
	</fieldset>
	
	<fieldset>
		<legend><?= __('Autenticación') ?></legend>
		
		<div class="row">
			<div class="col-sm-6 form-group">
				<input name="username" type="text" class="form-control control-sign ajax" placeholder="<?= __('Elija un Usuario') ?>" required data-action="/welcome/uniqueUser">
			</div>
			<div class="col-sm-6 form-group">
				<input name="password" type="password" class="form-control control-sign" placeholder="<?= __('Elija una contraseña') ?>" required>
			</div>
		</div>
	</fieldset>
	
	<button class="btn btn-lg btn-primary btn-block" type="submit"><?= __('Registrarse') ?></button><br>
	
	<div class="form-group actions text-center">
		<a class="btn btn-link" href="/welcome"><?= __('Ingresar') ?></a>
	</div>
</form>
