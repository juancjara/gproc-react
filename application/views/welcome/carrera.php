<form class="form-sign signup" role="form" method="post">
	
	<h3 class="heading">
		Sistema de Autoevaluación de la Educación Superior - 
		<a href="#">SAES</a>
	</h3>
	
	<img src="/media/saes/img/logo.jpg" class="logo img-responsive" alt="<?= __($site_title) ?>">
	
	<hr>
	
	<fieldset>
		<legend><?= __('Solicitar nueva carrera') ?></legend>
		
		<div class="row">
			<div class="col-sm-12 form-group">
				<select name="inst_id" class="form-control control-sign placeholder ajax" 
						data-action="getCareerByTipo" data-target="#carr_id" required>
					<?= HTML::default_option('Seleccione institución') ?>
					<?php foreach ($aInstitucion as $key => $val): ?>
						<option value="<?= $key ?>"><?= $val ?></option>
					<?php endforeach ?>
				</select>
			</div>
			<div class="col-sm-6 form-group">
				<select name="carr_id" class="form-control control-sign placeholder" id="carr_id" required>
					<?= HTML::default_option('Seleccione carrera') ?>
				</select>
			</div>
			<div class="col-sm-6 form-group hidden" id="carr_nombre_wrapper">
				<input name="carr_nombre" class="form-control control-sign" placeholder="Nombre de la carrera">
			</div>
		</div>
		
	</fieldset>
	
	<button class="btn btn-lg btn-primary" type="submit"><?= __('Solicitar') ?></button>
	<a class="btn btn-link" href="/welcome/register"><?= __('Volver') ?></a><br><br>
	
</form>
