<form class="form-sign signin" role="form" method="post" enctype="multipart/form-data" _action="//saes.dev/api/login/F5cYRLZF">
	
	<h3 class="heading">
		Sistema de Gestión de Convocatorias - 
		<a href="">GPROC</a>
	</h3>
	
	<img src="/media/gproc/img/logo.jpg" class="logo img-responsive" alt="<?= __($site_title) ?>">
	
	<?php if ($error): ?>
		<div class="alert alert-danger"><?= $error ?></div>
	<?php endif ?>
	<hr>
	
	<div class="form-group">
		<input name="username" type="text" class="form-control control-sign input-flat" placeholder="<?= __('Usuario') ?>" required autofocus>
	</div>
	
	<div class="form-group">
		<input name="password" type="password" class="form-control control-sign input-flat" placeholder="<?= __('Contraseña') ?>" required>
	</div>
	
	<a class="_btn _btn-link _btn-md pull-right btn-forgot" href="/welcome/forgot"><?= __('Olvidé mi contraseña') ?></a>
	
	<div class="form-group checkbox">
		<label>
			<input name="remember" type="checkbox" value="1"> <?= __('Recordarme') ?>
		</label>
	</div>
	
	<button class="btn btn-lg btn-default btn-block btn-flat" type="submit"><?= __('Ingresar') ?></button><br>

	<!--div class="form-group actions text-center">
		<a class="btn btn-link" href="http://saesdemo.procalidad.gob.pe/file/uploads/Manual de Usuario SAES.pdf" target="_blank"><?= __('Manual de uso') ?></a> |
		<a class="btn btn-link" href="http://saesdemo.procalidad.gob.pe/welcome/forgot"><?= __('Olvidé mi contraseña') ?></a>
	</div-->
</form>
