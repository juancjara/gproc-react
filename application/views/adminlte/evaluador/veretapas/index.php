<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h3 class="text-center">
			 Etapas de <?= $objeto['even_nombre'] ?>
		</h3>
		<h4 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle']." de ".$objeto['inst_region'] ?>
		</h4>
	</section>
	<!-- Main content -->
	<section class="content">
		
		<?= Breadcrumb::build() ?>
		
		<div class="box">
			<div class="box-body box-responsive">
				<table class="table table-hover ">
					<thead>
						<tr class="active">
							<th style="width: 10%;">Etapa</th>
							<th style="width: 10%;">Vigencia</th>
							<th style="width: 10%;">Estado</th>
							
							<?php foreach ($aEventoNivel as $oEventoNivel): ?>
							<th style="width: 14%;"><?= $oEventoNivel->evni_nombre ?></th>
							<?php endforeach ?>
							
							<th></th>
						</tr>
					</thead>
					<tbody>
						
						<?php // Etapas ?>
						<?php foreach ($aAcreditadoEtapa as $oAcreditadoEtapa): ?>
							<?php $oEventoEtapa = $oAcreditadoEtapa->oEventoEtapa ?>
							<tr>
								
								<td>
									<?= $oEventoEtapa->evet_nombre ?><br>
									
									<?php if ($oAcreditadoEtapa->acet_estado != Model_AcreditadoEtapa::ESTADO_PENDIENTE): ?>
										
										<?php foreach (Arr::get($fichas, $oAcreditadoEtapa->evet_code, []) as $oAcreditadoEtapaFicha): ?>

											<a class="btn btn-flat btn-sm btn-default" target="_blank" 
												href="<?= $oAcreditadoEtapaFicha->get_path() ?>" 
												title="<?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>">
												<i class="fa fa-file-text-o"></i>
											</a>
											
										<?php endforeach ?>
										
										<?php if ($oEventoEtapa->evet_code == 'RPM' AND ACL::instance()->allowed('regular')): ?>
											<hr style="margin: 5px 0;">
											<a class="btn btn-flat btn-sm btn-default" target="_blank" 
												href="/presupuesto?acev_id=<?= $objeto['acev_id'] ?>" 
												title="Ver detalle de Presupuesto">
												<i class="fa fa-file-text"></i>
											</a>
											<a class="btn btn-flat btn-sm btn-default" target="_blank" 
												href="/presupuesto/excel?acev_id=<?= $objeto['acev_id'] ?>" 
												title="Descargar Presupuesto como excel">
												<i class="fa fa-file-text"></i>
											</a>
										<?php endif ?>
										
									<?php endif ?>
								</td>
								
								<td>
									<?= date('d/m', strtotime($oEventoEtapa->evet_fecha_inicio)) ?> - <?= date('d/m', strtotime($oEventoEtapa->evet_fecha_fin)) ?>
								</td>
								<td>
									<label class="label label-<?= $oAcreditadoEtapa->class_estado() ?>">
										<?= $oAcreditadoEtapa->estado() ?>
										<?= ($oEventoEtapa->evet_code == 'AUT') ? $objeto['auto_porcentaje'].'%' : NULL ?>
									</label>
								</td>
								
								<?php // Niveles ?>
								<?php foreach ($aEventoNivel as $oEventoNivel): ?>
								<td>
									<?php // Etapa Nivel existe y AcreditadoEtapa != pendiente ?>
									<?php if ($oEventoEtapa->nivel_exists($oEventoNivel) AND ! $oAcreditadoEtapa->is_pendiente()): ?>
										
										<?php $oAcreditadoNivelEtapa = 
											$oAcreditadoEtapa->get_or_create_acreditado_nivel_etapa($oEventoNivel) ?>
										
										<span _class="label label-default">
											Estado: <strong><?= $oAcreditadoNivelEtapa->estado() ?></strong>
										</span>
										
										<?= ''//($oAcreditadoNivelEtapa->acne_estado == Model_AcreditadoNivelEtapa::ESTADO_OBSERVADO) ?
											//"(".date('d/m', strtotime($oAcreditadoEtapa->acet_fecha_fin_observado)).")" : NULL
										?>
										
										<?php if ($oAcreditadoNivelEtapa->is_observado()): ?>
											
											<?= "(".date('d/m', strtotime($oAcreditadoEtapa->acet_fecha_fin_observado)).")" ?>
											<a href="#" title="Ampliar plazo" class="ampliar_plazo"><i class="fa fa-pencil"></i></a>
											
											<form class="ampliar_plazo_wrapper hidden" 
												method="post"
												action="<?= '/evaluador/verEtapas/ampliarPlazo'
												.URL::query(['acev_id' => $oAcreditadoEtapa->acev_id]) ?>" >
												<span title="(a partir de hoy)">Plazo en días: </span>
												<input name="plazo_dias" style="width: 30%;" />
												<input type="hidden" name="acet_id" value="<?= $oAcreditadoEtapa->acet_id ?>" />
											</form>
											
										<?php endif ?>
										
										<table class="table table-condensed table-bordered">
											<tbody>
												<?php foreach ($oAcreditadoNivelEtapa->aAcreditadoNivelEtapaEstado->find_all() as 
													$oAcreditadoNivelEtapaEstado): ?>
													<tr>
														<td>
															<?= date('d/m', strtotime($oAcreditadoNivelEtapaEstado->anee_fecha)) ?> 
															(<?= $oAcreditadoNivelEtapaEstado->short_estado() ?>)

															<?php if ($oAcreditadoNivelEtapaEstado->anee_path): ?>
															<a class="btn btn-flat btn-xs btn-default pull-right" 
																title="Ver documento correspondiente" 
																href="/file/<?= $oAcreditadoNivelEtapaEstado->anee_path ?>">
																<i class="fa fa-file-text"></i>
															</a>
															<?php endif ?>

														</td>
													</tr>
												<?php endforeach ?>
											</tbody>
										</table>

										<?php if ($oAcreditadoNivelEtapa->is_evaluable()): ?>

											<span>Cambiar estado:</span>
											<form action="<?= '/evaluador/verEtapas/cambiarEstadoNivel'
												.URL::query(['acev_id' => $oAcreditadoEtapa->acev_id]) ?>" 
												class="_input-group" method="post" enctype="multipart/form-data">
												
												<div class="input-group">
													<select class="form-control input-sm estado_nivel" title="Cambiar estado" name="estado" required>
														<option value="">--</option>

														<?php foreach ($oAcreditadoNivelEtapa->acciones() as $key => $val): ?>
														<option value="<?= $key ?>"><?= $val ?></option>
														<?php endforeach ?>

													</select>

													<span class="input-group-btn">
														<span class="btn btn-default btn-flat btn-sm btn-file btn_file_upload disabled" 
															data-title="Subir documento correspondiente" data-toggle="tooltip">
															<i class="fa fa-upload"></i>
															<input class="concept-file" name="file" type="file" _required />
														</span>
													</span>
													<span class="input-group-btn">
														<button class="btn btn-flat btn-sm btn-default"
															title="Cambiar estado"><i class="fa fa-check"></i></button>
													</span>
													<input type="hidden" name="acne_id" value="<?= $oAcreditadoNivelEtapa->acne_id ?>" />
												</div>
												
												<?php if ($oEventoNivel->evni_code == 'EFF'): ?>
													<!--div class="checkbox">
														<input type="checkbox" id="foo" checked>
														<label for="foo">Aplica a todas las fichas</label>
													</div-->
													
													<div class="fichas_estado_wrapper hidden">
														<?php foreach (Arr::get($fichas, $oAcreditadoEtapa->evet_code, []) as $oAcreditadoEtapaFicha): ?>
															
															<div class="checkbox">
																<label>
																	<input type="checkbox" class="fichas_estado"
																		name="fichas[]" value="<?= $oAcreditadoEtapaFicha->evef_code ?>" 
																		id="ficha_<?= $oAcreditadoEtapaFicha->evef_code ?>" checked 
																		<?= $oAcreditadoEtapaFicha->evef_code != 'MPL' ? 'disabled' : NULL ?> />
																	<?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_path ?>
																</label>
															</div>
															
														<?php endforeach ?>
													</div>
													
												<?php endif ?>
												
												<div class="plazo_dias_wrapper hidden">
													Plazo en días: <input name="plazo_dias_observado" style="width: 30%;" type="text">
												</div>
												
											</form>
											
										<?php endif ?>
										
									<?php endif ?>
								</td>
								<?php endforeach ?>
								
								<td class="td-actions">
									<?php /*if ($oEventoEtapa->evet_code != 'AUT' AND $oAcreditadoEtapa->is_evaluable()): ?>
									<?php foreach ($oAcreditadoEtapa->acciones() as $key => $val): ?>
									
									<form action="<?= '/evaluador/verEtapas/cambiarEstadoEtapa'.URL::query(['acev_id' => $oAcreditadoEtapa->acev_id]) ?>"
										method="post">
										<button class="btn btn-flat btn-sm btn-success" title="<?= $val ?>">
											<i class="fa fa-adjust"></i></button>
										<input type="hidden" name="acet_id" value="<?= $oAcreditadoEtapa->acet_id ?>" />
										<input type="hidden" name="estado" value="<?= $key ?>" />
									</form>
									
									<?php endforeach ?>
									<?php endif*/ ?>
								</td>
								
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</section><!-- /.content -->
</aside><!-- /.right-side -->
