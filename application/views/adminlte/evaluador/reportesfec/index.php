<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h3 class="text-center">
				Detalle de los resultados de 6ta Convocatoria
			</h3>
			
			<?php //debug($acreditados_evento) ?>
			<h4 class="text-center">
				<strong>Planes de Mejora a financiar: S/.
					<?= numbercommaformat(
						Arr::path($acreditados_evento[0], 'totales.plan.T') + 
						Arr::path($acreditados_evento[1], 'totales.plan.T') 
					) ?>
				</strong>
			</h4>
			<h5 class="text-center">	
				<?php foreach ($acreditados_evento as $i => $evento): ?>
				<strong>Planes de Mejora de <?= ($i == 0) ? 'Universidades' : 'Institutos' ?>: S/.
					<?= numbercommaformat(Arr::path($evento, 'totales.plan.T')) ?>
				</strong><br><br>
				<?php endforeach ?>
			</h5>

		</section>
		<!-- Main content -->
		<section class="content">
			
			<?= Breadcrumb::build() ?>
			
			<?php foreach ($acreditados_evento as $index => $evento): ?>
			<div class="box">
				<div class="box-body">
					<ul class="list-unstyled">
						<table class="table" border="1">
							<thead>
								<tr>
									<th class="text-center" style="width:55%" colspan="2">Comité de Calidad participante</th>
									<th class="text-center" style="width:45%" colspan="3">Valoración del Plan de Mejora</th>
								</tr>
								<tr>
									<th class="text-center" style="width:5%">N°</th>
									<th class="text-center" style="width:50%">Institución</th>
									<th class="text-center" style="width:15%">FEC (S/.)</th>
									<th class="text-center" style="width:15%">Propio (S/.)</th>
									<th class="text-center" style="width:15%">Plan (S/.)</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($evento['fichas'] as $i => $acreditado): ?>
								<tr>
									<?php //debug($acreditado) ?>
									
									<td class="text-center" style="width:5%"><?= $i + 1 ?></td>

									<td style="width:50%">
										<?= $acreditado['comite']['inst_name'] ?><br>
										<small><?= $acreditado['comite']['carr_nombre'] ?></small>
									</td>
									<td class="text-center" style="width:15%">
										<?= numbercommaformat($acreditado['totales']['fec']['T']) ?><br>
										<small>(<?= $acreditado['totales']['per_fec'] ?>%)</small>
									</td>
									<td class="text-center" style="width:15%">
										<?= numbercommaformat($acreditado['totales']['propio']['T']) ?><br>
										<small>(<?= $acreditado['totales']['per_propio'] ?>%)</small>
									</td>
									<td class="text-center" style="width:15%">
										<?= numbercommaformat($acreditado['totales']['plan']['T']) ?><br>
										<small>(100%)</small>
									</td>
								</tr>
								<?php endforeach ?>
								<tr nobr="true">
									<td>	
										<strong>Total <?= ($index == 0) ? 'Universidades:' : 'Institutos:'?></strong>
									</td>
									<td class="text-right"><strong>
										<?= numbercommaformat(Arr::path($evento, 'totales.fec.T')) ?><br>
									</strong></td>
									<td class="text-right"><strong>
										<?= numbercommaformat(Arr::path($evento, 'totales.propio.T')) ?><br>
									</strong></td>
									<td class="text-right"><strong>
										<?= numbercommaformat(Arr::path($evento, 'totales.plan.T')) ?><br>
									</strong></td>
								</tr>

							</tbody>
						</table>
					</ul>
				</div>
			</div>
			<?php endforeach ?>
			
		</section><!-- /.content -->

</aside><!-- /.right-side -->

<style>
	.text-center {
		text-align: center;
	}
	.text-right {
		text-align: right;
	}
</style>
