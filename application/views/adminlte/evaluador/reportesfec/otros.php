<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h3 class="text-center">
				Detalle de OTRAS actividades financiadas por el FEC - 6ta Convocatoria
			</h3>
			
			<?php //debug($acreditados_evento) ?>
			<h4 class="text-center">
				<strong>Total en Planes de Mejora a financiar: S/.
					<?= numbercommaformat(
						Arr::path($acreditados_evento[0], 'totales.plan.T') + 
						Arr::path($acreditados_evento[1], 'totales.plan.T') 
					) ?>
				</strong>
			</h4>
			<h5 class="text-center">	
				<strong>Total en otros: S/.
					<?= numbercommaformat(
						Arr::path($acreditados_evento[0], 'otros.plan.T') +
						Arr::path($acreditados_evento[1], 'otros.plan.T')
					) ?>
				</strong><br><br>
			</h5>

		</section>
		<!-- Main content -->
		<section class="content">
			
			<?= Breadcrumb::build() ?>
			
			<?php foreach ($acreditados_evento as $index => $evento): ?>
			<div class="box">
				<div class="box-body">
					<ul class="list-unstyled">
						<table class="table" border="1">
							<thead>
								<tr>
									<th class="text-center" colspan="2">Comité de Calidad participante</th>
									<th class="text-center" colspan="6">Actividades financiadas por el FEC</th>
								</tr>
								<tr>
									<th rowspan="2">N°</th>
									<th rowspan="2">
										<?= $index == 0 ? 'Universidades' : 'Institutos' ?><br>
										Total FEC S/. <?= numbercommaformat($evento['totales']['fec']['T']) ?><br>
										Total Otros S/. <?= numbercommaformat($evento['otros']['fec']['T']) ?>
									</th>
									<th class="text-right" colspan="3">RRHH (S/.)</th>
									<th class="text-right" colspan="1">Servicios (S/.)</th>
									<th class="text-right" colspan="2">Bienes (S/.)</th>
								</tr>
								<tr>
									<th class="text-right">Pasantías (S/.)</th>
									<th class="text-right">Capacitación (S/.)</th>
									<th class="text-right">Prof. visitantes (S/.)</th>
									<th class="text-right">Consultorías (S/.)</th>
									<th class="text-right">Equipamiento (S/.)</th>
									<th class="text-right">Obras menores (S/.)</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($evento['fichas'] as $i => $acreditado): ?>
								<tr nobr="true">
									<?php //debug($acreditado) ?>
									
									<td><?= $i + 1 ?></td>

									<td>
										<?= $acreditado['comite']['inst_name'] ?><br>
										<small><?= $acreditado['comite']['carr_nombre'] ?></small>
									</td>
									
									<td class="text-right">
										<?= numbercommaformat($acreditado['totales']['fec']['RP']) ?>
									</td>
									<td class="text-right">
										<?= numbercommaformat($acreditado['totales']['fec']['RC']) ?>
									</td>
									<td class="text-right">
										<?= numbercommaformat($acreditado['totales']['fec']['SP']) ?>
									</td>
									<td class="text-right">
										<?= numbercommaformat($acreditado['totales']['fec']['SC']) ?>
									</td>
									<td class="text-right">
										<?= numbercommaformat($acreditado['totales']['fec']['B1']) ?>
									</td>
									<td class="text-right">
										<?= numbercommaformat($acreditado['totales']['fec']['B2']) ?>
									</td>
								</tr>
								<?php endforeach ?>
								<tr>
									<td>
										<strong>Total <?= ($index == 0) ? 'Universidades:' : 'Institutos:'?></strong>
									</td>
									<td class="text-right"><strong>
										<?= numbercommaformat(Arr::path($evento, 'totales.fec.RP')) ?><br>
									</strong></td>
									<td class="text-right"><strong>
										<?= numbercommaformat(Arr::path($evento, 'totales.fec.RC')) ?><br>
									</strong></td>
									<td class="text-right"><strong>
										<?= numbercommaformat(Arr::path($evento, 'totales.fec.SC')) ?><br>
									</strong></td>
									<td class="text-right"><strong>
										<?= numbercommaformat(Arr::path($evento, 'totales.fec.SP')) ?><br>
									</strong></td>
									<td class="text-right"><strong>
										<?= numbercommaformat(Arr::path($evento, 'totales.fec.B1')) ?><br>
									</strong></td>
									<td class="text-right"><strong>
										<?= numbercommaformat(Arr::path($evento, 'totales.fec.B2')) ?><br>
									</strong></td>
								</tr>

							</tbody>
						</table>
					</ul>
				</div>
			</div>
			<?php endforeach ?>
			
		</section><!-- /.content -->

</aside><!-- /.right-side -->

<style>
	.text-center {
		text-align: center;
	}
	.text-right {
		text-align: right;
	}
</style>
