<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h3 class="text-center">
				 Seguimiento de Planes de Mejora de <?= $evento['even_nombre'] ?>
			</h3>
			<h4 class="text-center">
				<a href="<?= '/evaluador/verComites'
					.URL::query(['even_id' => $evento['even_id']]) ?>">
					Ver todas las etapas</a>
			</h4>
		</section>
		<!-- Main content -->
		<section class="content">
			
			<?= Breadcrumb::build() ?>
			
			<div class="box">
				<div class="box-body box-responsive">
					<table class="table table-hover ">
						<thead>
							<tr>
								<th style="width: 5%;">N</th>
								<th style="width: 10%;">Tipo</th>
								<th style="width: 35%;">Comité de calidad</th>
								<?php foreach ($aEventoEtapaFicha as $oEventoEtapaFicha): ?>
								<th style="width: 8%;"><?= $oEventoEtapaFicha->evef_nombre ?></th>
								<?php endforeach ?>
								<th class="success" style="width: 10%;">Plan de Mejora</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($aAcreditadoEvento as $index => $oAcreditadoEvento): ?>
								<?php $comite = $comites[$oAcreditadoEvento->acev_id] ?>
								<tr>
									<td>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $oAcreditadoEvento->acev_id]) ?>">
											<?= $index + 1 ?></a>
									</td>
									<td>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $oAcreditadoEvento->acev_id]) ?>">
											<?= $comite['objeto'] ?></a>
									</td>
									<td>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $oAcreditadoEvento->acev_id]) ?>">
											<?= $comite['inst_name'] ?><br>
											<?= $comite['carr_nombre'] ?>
											<?= $comite['carr_nombre'] ? '<br>' : NULL ?>
											
											<small class="text-primary"><?= $comite['inst_gestion'] ?></small>
											-
											<small class="text-primary"><?= $comite['moda_nombre'] ?></small>

											<label class="label label-default"><?= ($comite['acre_test'] == '1') ? 'Test' : NULL ?></label>
										</a>
									</td>
									
									<?php foreach ($aEventoEtapaFicha as $oEventoEtapaFicha): ?>
									<td>
										<?php $oAcreditadoEtapaFicha = Arr::path($fichas, "{$oAcreditadoEvento->acev_id}.{$oEventoEtapaFicha->evef_code}") ?>
										<?php //debug($oAcreditadoEtapaFicha) ?>
										
										<?php if ($oAcreditadoEtapaFicha): ?>
										<a class="table-link" target="_blank" 
											href="<?= $oEventoEtapaFicha->evef_path.'/pdf?acev_id='.$oAcreditadoEvento->acev_id ?>">
											<label class="label label-<?= $oAcreditadoEtapaFicha->class_estado() ?>">
												<?= $oAcreditadoEtapaFicha->estado() ?></label>
										</a>
										<?php else: ?>
											No revisado
										<?php endif ?>
									</td>
									<?php endforeach ?>
									
									<td class="success">
										<a class="table-link" target="_blank" 
											href="/presupuesto?acev_id=<?= $oAcreditadoEvento->acev_id ?>">
											<label class="label label-<?= $aAcreditadoEtapa[$oAcreditadoEvento->acev_id]->class_estado() ?>">
												<?= $aAcreditadoEtapa[$oAcreditadoEvento->acev_id]->estado() ?>
											</label>
										</a>
									</td>
									
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</section><!-- /.content -->

</aside><!-- /.right-side -->
