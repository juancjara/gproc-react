<aside class="left-side sidebar-offcanvas">                
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<img src="/media/gproc_lte/img/logo.png" class="img-responsive img-logo">
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->

		<ul class="sidebar-menu">
			<li>
				<a href="/evaluador/verEventos">
					<i class="fa fa-dashboard"></i> 
					<span>Ver eventos</span>
				</a>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-suitcase"></i>
					<span>Reportes</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li><a href="/evaluador/reportesFec"><i class="fa fa-angle-double-right"></i>Reporte Fec</a></li>
					<li><a href="/evaluador/reportesFec/pdf" target="_blank"><i class="fa fa-angle-double-right"></i>Reporte Fec (PDF)</a></li>
					<li><a href="/evaluador/reportesFec/otros"><i class="fa fa-angle-double-right"></i>Reporte Fec (Otros)</a></li>
					<li><a href="/evaluador/reportesFec/otrosPdf" target="_blank"><i class="fa fa-angle-double-right"></i>Reporte Fec (Otros) (PDF)</a></li>
				</ul>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>
