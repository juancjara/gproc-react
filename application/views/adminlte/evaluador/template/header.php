<header class="header">
	<a href="/" class="logo">
		<!-- Add the class icon to your logo image or logo icon to add the margining -->
		<strong>GProC</strong>
		<!--img src="/media/saes_lte/img/logo-white.png" class="img-responsive"-->
	</a>
	
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<p class="navbar-text navbar-left hidden-xs color-white" title="<?= $welcome ?>"><?= $welcome ?></p>

		<div class="navbar-right ubicacion-menu-usuario" >
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i>
						<span><?= $auth->get_user()->usuario ?> <i class="caret"></i></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header bg-light-blue">
							<!--img src="/media/adminlte/img/avatar3.png" class="img-circle" alt="User Image" /-->
							<p>
								Fecha: <?= date('d/m/Y') ?>
							</p>
							<!--p>
								Evento: <?php //= $objeto['even_nombre'] ?>
							</p-->
						</li>

						<li class="user-footer">
							<div class="pull-right">
								<a href="/welcome/salir" class="btn btn-default btn-flat">Salir</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
