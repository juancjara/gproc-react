<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h3 class="text-center">
				Reportes de <?= $evento['even_nombre'] ?>
			</h3>
			<!--h4 class="text-center">
				<a href="<?= '/evaluador/verComites'
					.URL::query(['even_id' => $evento['even_id']]) ?>">Ver todas las etapas</a>
			</h4-->
		</section>
		<!-- Main content -->
		<section class="content">
			
			<?= Breadcrumb::build() ?>
			
			<div class="box">
				<div class="box-body">
					<ul class="list-unstyled">
						<?php foreach ($reportes as $reporte): ?>
						<li>
							<a href="/evaluador/reportes/ver/10?tipo=<?= $reporte ?>"><?= $reporte ?></a>
						</li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
		</section><!-- /.content -->

</aside><!-- /.right-side -->
