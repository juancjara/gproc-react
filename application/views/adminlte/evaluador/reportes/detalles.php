<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h3 class="text-center">
				Ejecución de <?= $evento['even_nombre'] ?> (Hardware y Software)
			</h3>
			<h4 class="text-center">
				<a href="<?= '/evaluador/verComites'
					.URL::query(['even_id' => $evento['even_id']]) ?>">
					Ver todas las etapas</a>
			</h4>
		</section>
		<!-- Main content -->
		<section class="content">
			
			<?= Breadcrumb::build() ?>
			
			<div class="box">
				<div class="box-body box-responsive">
					<table class="table table-hover table_comites" border="1">
						<thead>
							<tr>
								<th style="width: 5%;">N</th>
								<th style="width: 10%;">Tipo</th>
								<th style="width: 25%;">Comité de calidad</th>
								<th style="width: 60%;">Items</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($comites as $index => $comite): ?>
								<tr>
									<td>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $comite['acev_id']]) ?>">
											<?= $index + 1 ?></a>
									</td>
									<td>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $comite['acev_id']]) ?>">
											<?= $comite['objeto'] ?></a>
									</td>
									<td>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $comite['acev_id']]) ?>">
											<?= $comite['inst_name'] ?><br>
											<?= $comite['carr_nombre'] ?>
											<?= $comite['carr_nombre'] ? '<br>' : NULL ?>
											
											<small class="text-primary"><?= $comite['inst_gestion'] ?></small>
											-
											<small class="text-primary"><?= $comite['moda_nombre'] ?></small>

											<label class="label label-default"><?= ($comite['acre_test'] == '1') ? 'Test' : NULL ?></label>
										</a>
									</td>
									
									<td>
										<?php //debug($comite['items']) ?>
										
										<table class="table table-hover" border="1">
											<thead>
												<tr>
													<th style="width: 5%;">N°</th>
													<th style="width: 10%;">Grupo</th>
													<th style="width: 10%;">Gasto</th>
													<th style="width: 10%;">Tipo</th>
													<th style="width: 50%;">Item</th>
													<th style="width: 5%;">Costo U.</th>
													<th style="width: 5%;">Cantidad</th>
													<th style="width: 5%;">Total</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 0 ?>
												<?php foreach ($comite['items'] as $index => $item): ?>

													<?php if (Arr::path($item, 'itga_id') AND $item['total'] == 0) continue ?>
													
													<?php //debug($item) ?>
													<?php //if ( ! in_array(Arr::path($item, 'gael_id'), [11, 12, 'Software', 'Hardware'])) continue ?>
													
													<tr>
														<td>
															<?= ++$i ?>
														</td>
														<td>
															<?= Arr::get($item, 'grup_nombre', $item['grup_id']) ?>
														</td>
														<td>
															<?= Arr::get($item, 'gael_nombre', $item['gael_id']) ?>
														</td>
														<td>
															<?= Arr::get($item, 'tiga_nombre', $item['tiga_id']) ?>
														</td>
														<td>
															<?= $item['itga_nombre'] ?>
														</td>
														<td>
															<?= $item['costounitario'] ?>
														</td>
														<td>
															<?= $item['cantidad'] ?>
														</td>
														<td>
															<?= $item['total'] ?>
														</td>
													</tr>
												<?php endforeach ?>

											</tbody>
										</table>
										
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</section><!-- /.content -->

</aside><!-- /.right-side -->

<script>
	setTimeout(function () {
		$('.table_comites > tbody > tr').each(function () {
			var $this = $(this);
			if ( ! $this.find('table > tbody > tr').length) $this.remove();
		});
	}, 2000);
</script>
