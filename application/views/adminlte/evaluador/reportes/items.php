<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h3 class="text-center">
				Ejecución de <?= $evento['even_nombre'] ?> (Hardware y Software)
			</h3>
			<h4 class="text-center">
				<a href="<?= '/evaluador/verComites'
					.URL::query(['even_id' => $evento['even_id']]) ?>">
					Ver todas las etapas</a>
			</h4>
		</section>
		<!-- Main content -->
		<section class="content">
			
			<?= Breadcrumb::build() ?>
			
			<div class="box">
				<div class="box-body box-responsive">
					<table class="table table-hover" border="1">
						<thead>
							<tr>
								<th style="width: 5%;">N°</th>
								<th style="width: 10%;">Grupo</th>
								<th style="width: 10%;">Gasto</th>
								<th style="width: 10%;">Tipo</th>
								<th style="width: 60%;">Item</th>
								<th style="width: 5%;">Costo U.</th>
								<th style="width: 5%;">Cantidad</th>
								<th style="width: 5%;">Total</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 0 ?>
							<?php foreach ($items as $index => $item): ?>
								
								<?php if (Arr::path($item, 'itga_id') AND $item['total'] == 0) continue ?>
								
								<tr>
									<td>
										<?= ++$i ?>
									</td>
									<td>
										<?= Arr::get($item, 'grup_nombre', $item['grup_id']) ?>
									</td>
									<td>
										<?= Arr::get($item, 'gael_nombre', $item['gael_id']) ?>
									</td>
									<td>
										<?= Arr::get($item, 'tiga_nombre', $item['tiga_id']) ?>
									</td>
									<td>
										<?= $item['itga_nombre'] ?>
									</td>
									<td>
										<?= $item['costounitario'] ?>
									</td>
									<td>
										<?= $item['cantidad'] ?>
									</td>
									<td>
										<?= $item['total'] ?>
									</td>
								</tr>
							<?php endforeach ?>
							
						</tbody>
					</table>
				</div>
			</div>
		</section><!-- /.content -->

</aside><!-- /.right-side -->
