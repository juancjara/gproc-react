<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h3 class="text-center">
				 Comités de Calidad de <?= $evento['even_nombre'] ?>
			</h3>
			<h4 class="text-center">
				<a href="<?= '/evaluador/seguimiento'
					.URL::query(['even_id' => $evento['even_id']]) ?>">
					Ver seguimiento de Planes de Mejora</a>
			</h4>
		</section>
		<!-- Main content -->
		<section class="content">
			
			<?= Breadcrumb::build() ?>
			
			<div class="box">
				<div class="box-body box-responsive">
					<table class="table table-hover ">
						<thead>
							<tr>
								<th style="width: 5%;">N</th>
								<th style="width: 10%;">Tipo</th>
								<th style="width: 30%;">Comité de calidad</th>
								<?php foreach ($aEventoEtapa as $oEventoEtapa): ?>
								<th style="width: 10%;"><?= $oEventoEtapa->evet_nombre ?></th>
								<?php endforeach ?>
								<th style="width: 5%;">Habilitado - Planes de Mejora</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($aAcreditadoEvento as $index => $oAcreditadoEvento): ?>
								<?php $comite = $comites[$oAcreditadoEvento->acev_id] ?>
								<tr>
									<td>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $oAcreditadoEvento->acev_id]) ?>">
											<?= $index + 1 ?></a>
									</td>
									<td>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $oAcreditadoEvento->acev_id]) ?>">
											<?= $comite['objeto'] ?></a>
									</td>
									<td>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $oAcreditadoEvento->acev_id]) ?>">
											<?= $comite['inst_name'] ?><br>
											<?= $comite['carr_nombre'] ?>
											<?= $comite['carr_nombre'] ? '<br>' : NULL ?>
											
											<small class="text-primary"><?= $comite['inst_gestion'] ?></small>
											-
											<small class="text-primary"><?= $comite['moda_nombre'] ?></small>

											<label class="label label-default"><?= ($comite['acre_test'] == '1') ? 'Test' : NULL ?></label>
										</a>
									</td>
									
									<?php foreach ($aEventoEtapa as $oEventoEtapa): ?>
									<td>
										<?php $oAcreditadoEtapa = Arr::path($etapas, "{$oAcreditadoEvento->acev_id}.{$oEventoEtapa->evet_code}") ?>
										<?php //debug($oAcreditadoEtapa) ?>
										
										<?php if ($oAcreditadoEtapa): ?>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $oAcreditadoEvento->acev_id]) ?>">
											<label class="label label-<?= $oAcreditadoEtapa->class_estado() ?>"><?= $oAcreditadoEtapa->estado() ?></label>
										</a>
										<?php endif ?>
									</td>
									<?php endforeach ?>
									
									<td class="text-center">
										<?php //debug($oAcreditadoEtapa) ?>
										<?php $oAcreditadoEvento = $oAcreditadoEtapa->oAcreditadoEvento ?>
										
										<?php if ($oAcreditadoEvento->get_acreditado_etapa('RPM')->is_pendiente()): ?>
										<form method="post" 
											action="<?= '/evaluador/verEtapas/setAsistencia'
											.URL::query(['acev_id' => $oAcreditadoEvento->acev_id]) ?>">
											<input type="checkbox" name="asistio" 
												value="1" class="evaluacion_virtual" 
												data-confirm <?= $oAcreditadoEvento->acev_eval_virtual ? 'checked' : NULL ?> />
										</form>
										<?php else: ?>
										<?= $oAcreditadoEvento->asistio_eval_virtual() ? 'Sí' : 'No' ?>
										<?php endif ?>

									</td>
									
								</tr>
							<?php endforeach ?>
						
							<tr>
								<th class="text-center" colspan="3"></th>
								<?php foreach ($aEventoEtapa as $oEventoEtapa): ?>
									<th><?= $oEventoEtapa->evet_nombre ?></th>
								<?php endforeach ?>
								<th></th>
							</tr>
							
							<?php foreach ([10, 20, 30, 40, 50] as $estado): ?>
							<tr>
								<td class="text-center" colspan="3">
									<label class="label label-<?= Model_AcreditadoEtapa::$classes[$estado] ?>">
										<?= Model_AcreditadoEtapa::$estados[$estado] ?>
									</label>
								</td>
								
								<?php foreach ($aEventoEtapa as $oEventoEtapa): ?>
									<td><?= $totales[$oEventoEtapa->evet_code]['count_'.$estado] ?></td>
								<?php endforeach ?>
								
								<td></td>
							</tr>
							<?php endforeach ?>
							
							<tr>
								<th class="text-center" colspan="3">TOTALES</th>
								<?php foreach ($aEventoEtapa as $oEventoEtapa): ?>
									<th><?= $totales[$oEventoEtapa->evet_code]['count_all'] ?></th>
								<?php endforeach ?>
								<th></th>
							</tr>
						</tbody>
					</table>	
				</div>
			</div>
		</section><!-- /.content -->

</aside><!-- /.right-side -->
