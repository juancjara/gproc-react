<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h3 class="text-center">
				 Eventos
			</h3>
		</section>
		<!-- Main content -->
		<section class="content">
			
			<?= Breadcrumb::build() ?>
			
			<div class="box">
				<div class="box-body box-responsive">
					<table class="table table-hover ">
						<thead>
							<tr>
								<th>N</th>
								<th>Tipo</th>
								<th>Nombre</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($aEvento as $index => $oEvento): ?>
								<?php $evento = $eventos[$oEvento->even_id] ?>
								<tr>
									<td>
										<a class="table-link" href="<?= '/evaluador/verComites'
											.URL::query(['even_id' => $evento['even_id']]) ?>">
											<?= $index + 1 ?></a>
									</td>
									<td>
										<a class="table-link" href="<?= '/evaluador/verComites'
											.URL::query(['even_id' => $evento['even_id']]) ?>">
											<?= $evento['tiin_nombre'] ?></a>
									</td>
									<td>
										<a href="<?= '/evaluador/verComites'
											.URL::query(['even_id' => $evento['even_id']]) ?>">
											<?= $evento['even_nombre'] ?></a>
									</td>
									<td>
										<a class="btn btn-flat btn-sm btn-default"
											href="<?= '/evaluador/verComites'
											.URL::query(['even_id' => $evento['even_id']]) ?>">
											Seguimiento Etapas</a>
										<a class="btn btn-flat btn-sm btn-default"
											href="<?= '/evaluador/verComites'
											.URL::query(['even_id' => $evento['even_id']]) ?>">
											Seguimiento Planes de Mejora</a>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</section><!-- /.content -->

</aside><!-- /.right-side -->

<style>
a:link, a:hover, a:visited, a:active {
	color: #000; 
}
</style>
