<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h3 class="text-center">
				Ejecución de <?= $evento['even_nombre'] ?> - <?= $title ?>
			</h3>
			<h4 class="text-center">
				<a href="<?= '/evaluador/verComites'
					.URL::query(['even_id' => $evento['even_id']]) ?>">
					Ver todas las etapas</a>
			</h4>
		</section>
		<!-- Main content -->
		<section class="content">
			
			<?php //= Breadcrumb::build() ?>
			
			<div class="text-right">
				<a class="btn btn-flat btn-default" 
					href="/evaluador/reportes/excel/<?= $evento['even_id'] ?>?tipo=<?= $name ?>" target="_blank">
					<i class="fa fa-file-text-o"></i> Excel</a>
			</div><br>
			
			<div class="box">
				<div class="box-body box-responsive">
					<table class="table table-hover" border="1">
						<thead>
							<tr>
								<th style="width: 5%;">N</th>
								<th style="width: 10%;">Tipo</th>
								<th style="width: 45%;">Comité de calidad</th>
								<?php foreach ($grupos as $grupo): ?>
								<th style="width: 5%;"><?= $grupo['grup_short'] ?> - FEC</th>
								<?php endforeach ?>
								<th style="width: 5%;">Total - Propio</th>
								<th style="width: 5%;">Total Plan</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($result as $index => $comite): ?>
								<tr>
									<td>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $comite['acev_id']]) ?>">
											<?= $index + 1 ?></a>
									</td>
									<td>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $comite['acev_id']]) ?>">
											<?= $comite['objeto'] ?></a>
									</td>
									<td>
										<a class="table-link" 
											href="<?= '/evaluador/verEtapas'
											.URL::query(['acev_id' => $comite['acev_id']]) ?>">
											<?= $comite['inst_name'] ?><br>
											<?= $comite['carr_nombre'] ?>
											<?= $comite['carr_nombre'] ? '<br>' : NULL ?>
											
											<small class="text-primary"><?= $comite['inst_gestion'] ?></small>
											-
											<small class="text-primary"><?= $comite['moda_nombre'] ?></small>

											<label class="label label-default"><?= ($comite['acre_test'] == '1') ? 'Test' : NULL ?></label>
										</a>
									</td>
									
									<?php foreach ($grupos as $id => $grupo): ?>
									<td>
										<?= $comite['totales']['fec'][$id] ?>
									</td>
									<?php endforeach ?>
									
									<td>
										<?= $comite['totales']['propio']['total'] ?>
									</td>
									<td>
										<?= $comite['totales']['plan']['total'] ?>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</section><!-- /.content -->

</aside><!-- /.right-side -->
