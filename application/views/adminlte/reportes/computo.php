<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h3 class="text-center">
				Ejecución de <?= $evento['even_nombre'] ?> - <?= $title ?>
			</h3>
			<h4 class="text-center">
				<a href="<?= '/evaluador/verComites'
					.URL::query(['even_id' => $evento['even_id']]) ?>">
					Ver todas las etapas</a>
			</h4>
		</section>
		<!-- Main content -->
		<section class="content">
			
			<?php //= Breadcrumb::build() ?>
			
			<div class="text-right">
				<a class="btn btn-flat btn-default" 
					href="/evaluador/reportes/excel/<?= $evento['even_id'] ?>?tipo=<?= $name ?>" target="_blank">
					<i class="fa fa-file-text-o"></i> Excel</a>
			</div><br>
			
			<div class="box">
				<div class="box-body box-responsive">
					<table class="table table-hover" border="1">
						<thead>
							<tr>
								<th style="width: 5%;">N°</th>
								<th style="width: 20%;">Tipo</th>
								<th style="width: 60%;">Item</th>
								<th style="width: 5%;">Costo U.</th>
								<th style="width: 5%;">Cantidad</th>
								<th style="width: 5%;">Total</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 0 ?>
							<?php foreach ($result as $index => $item): ?>
								
								<?php if ($item['total'] == 0) continue ?>
								
								<tr>
									<td>
										<?= ++$i ?>
									</td>
									<td>
										<?= Arr::get($item, 'gael_nombre', 'Otros') ?>
									</td>
									<td>
										<?= $item['itga_nombre'] ?>
									</td>
									<td>
										<?= Arr::get($item, 'itga_nombre') == 'Otros' ? '-' : $item['costounitario'] ?>
									</td>
									<td>
										<?= Arr::get($item, 'itga_nombre') == 'Otros' ? '-' : $item['cantidad'] ?>
									</td>
									<td>
										<?= $item['total'] ?>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</section><!-- /.content -->

</aside><!-- /.right-side -->
