<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?= $content->title ?></title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<?= $theme->get_css() ?>
		
	</head>
	<body class="skin-blue">
		<!-- header logo: style can be found in header.less -->

		<div class="wrapper row-offcanvas row-offcanvas-left">
			
			<h5>PROCALIDAD - GPROC | Reporte al <?= date('d/m/Y') ?></h5>
			
			<?= $content ?>
			
		</div>
		
		<script>
			//window.print();
		</script>
		
	</body>
</html>
