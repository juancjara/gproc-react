<?php $pattern = "^(\d+|N\/A|S\/D)$" ?>
<?php //$action = 'upload' ?>
<?php //$action = 'save' ?>
<?php $edit = ($action == 'save') ?>

<div class="temp_data">
<?php //$edit = TRUE ?>
<?php //$institucional = TRUE ?>
<?php //$universidad = FALSE ?>
</div>

<script>
	window.__INITIAL_STATE__ = <?= $data ?> || undefined;
	window.__SERVER_RENDER__ = false;
	window.__CONTEXT__ = <?= $context ?>;
</script>

<aside class="right-side _strech">
	
	<section class="content-header">
		
		<?php if ($edit): ?>
		<div id="root-progress"></div>
		<?php endif ?>
		
		<h4 class="text-center">
			<?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
			<label class="label label-sm label-<?= $oAcreditadoEtapaFicha->class_estado() ?>">
				<?= $oAcreditadoEtapaFicha->estado() ?></label>
		</h4>
		<h5 class="text-center">
			<strong><?= $objeto['inst_nombre'] ?></strong>
			
		<?php if ($objeto['carr_id']): ?>
		</h5>
		<h5 class="text-center">
			<strong><?= $objeto['carr_nombre'] ?></strong>
		<?php endif ?>
			
			<?= ' | '.$objeto['moda_nombre'] ?>
			<?php //= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
	</section>
	
	<!-- Main content -->
	<section class="content">
		
		<?php if ($action == 'view' AND $oAcreditadoEtapaFicha->acef_path): ?>
			<div class="well system-status">
				<a class="btn btn-flat btn-github" 
					href="file/<?= $oAcreditadoEtapaFicha->acef_path ?>">
					<i class="fa fa-file-text"></i> Descargar PDF firmado
				</a>
			</div>
		<?php endif ?>
		
		<?php if ($action == 'upload'): ?>
			<div class="alert alert-info _alert-dismissable">
				<i class="fa fa-info"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				Para finalizar y subir los documentos, debe terminar todas las fichas del Plan de Mejora.
				También puede volver a abrir esta ficha si desea editarla.
			</div>
			
			<div class="well system-status">
				<button class="btn btn-flat btn-github download_ficha" 
					data-url="<?= '/presupuesto/pdf'.URL::query() ?>"
					type="button" 
					title="Descargar vista previa">
					<i class="fa fa-download"></i> PDF
				</button>
				
				<?php //if (ACL::instance()->check_role(Model_Role::ROLE_ADMIN)): ?>
				<div class="pull-right">
					<a href="<?= '/presupuesto/reedit'.URL::query() ?>" 
						class="btn btn-flat btn-warning">
						<!--small class="label label-danger"
							title="Admin">
							<i class="fa fa-lock"></i>
						</small-->
						Volver a editar
					</a>
				</div>
				<?php //endif ?>
			</div>
			
		<?php endif ?>
		
		<div id="root">
			<?php //$html ?>
		</div>

		<hr>

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
