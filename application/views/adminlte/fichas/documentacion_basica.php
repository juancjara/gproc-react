<?php $edit = ($action == 'save') ?>

<aside class="right-side">
	
	<section class="content-header">
		<h5 class="text-center">
			<?php echo
				"Bienvenidos, Sres. del Comité de Calidad de ". $objeto['subtitle'];
			?>
		</h5>
		<h4 class="text-center">
			<?= $oAcreditadoEtapa->oEventoEtapa->evet_nombre ?>
			<label class="label label-sm label-<?= $oAcreditadoEtapa->class_estado() ?>">
				<?= $oAcreditadoEtapa->estado() ?></label>
		</h4>
	</section>
	
	<section class="content">
		<div class="div-contenido">
			<form class="form-horizontal ficha" method="post" action="<?= URL::query(['acev_id' => $objeto['acev_id']]) ?>">
				<hr>
				<table  class="table table-bordered">
					<thead>
						<tr class="success" nobr="true">
							<th class="text-center tdocumento-nombre">
								<label title="Nombre del documento institucional">
									Nombre del documento institucional</label></th>
							<th class="text-center tdocumento-pdf">
								<label title="Subir archivo correspondiente">
									<?= $edit ? 'Subir' : 'Ver' ?> archivo correspondiente</label></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($aDocumentacion as $key => $oDocumentacion): ?>
						<?php //debug($oDocumentacion) ?>
						<tr>
							<td class="tdocumento-nombre"><?= ($key + 1).". ".$oDocumentacion->oEventoEtapaFicha->evef_nombre ?></td>
							<td class="td-pdf tdocumento-pdf">
								
								<?php if ($edit) : ?>
								<div class="input-group-btn file_upload">
									<span class="btn btn-default btn-flat btn-file btn_file_upload span_input_pdf" 
										data-title="Subir documento">
										<i class="fa fa-upload"></i>
										<input class="concept-file" name="file" type="file" 
											<?= $oDocumentacion->acef_path ? NULL : 'required' ?> />
										<input type="hidden" 
											name="identifier" 
											value="<?= "{$evet_code}-{$oDocumentacion->acef_id}" ?>" />
									</span>
								</div>

								<div class="input-group-addon file_process hidden">
									<img src="/media/gproc_lte/img/ajax-loader.gif">
								</div>
								<?php endif ?>

								<div class="input-group-btn">
									
									<a href="/file/<?= $oDocumentacion->acef_path ?>" 
										class="btn btn-default btn-flat btn_file_view <?= $oDocumentacion->acef_path ? 
											NULL : 'disabled' ?>" target="_blank" 
										data-title="Ver documento" 
										<?= $oDocumentacion->acef_path ? 'data-toggle="tooltip"' : NULL ?> >
										<i class="fa fa-file-text full"></i>
										<i class="fa fa-file-text-o empty"></i>
									</a>
									
									<input type="hidden" 
										name="<?= "documentos[{$oDocumentacion->acef_id}" ?>]" 
										value="<?= $oDocumentacion->acef_path ?>">
								</div>
							</td>

						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				
				<?php if ($edit): ?>
				<div class="_actions">
					<button type="button" name="finish" class="btn btn-flat btn-default btn_save" value="0">
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1">
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/<?= $url ?>" class="btn btn-flat btn-danger" > Cancelar</a>
					</div>
				</div>
				<?php endif ?>
				
			</form>
		</div>
	</section>
</aside>

<form class="form_outside" method="post" 
	action="/<?= $url ?>/uploadDocumentacion<?= URL::query(['acev_id' => $objeto['acev_id']]) ?>" 
	enctype="multipart/form-data"></form>
