<?php $edit = ($action == 'save') ?>

<aside class="right-side">
	
	<section class="content-header">
		
		<h4 class="text-center">
			Documentos del Plan de Mejora
			<label class="label label-sm label-<?= $oAcreditadoEtapa->class_estado() ?>">
				<?= $oAcreditadoEtapa->estado() ?></label>
		</h4>
		<h5 class="text-center">
			<strong><?= $objeto['inst_nombre'] ?></strong>
			
		<?php if ($objeto['carr_id']): ?>
		</h5>
		<h5 class="text-center">
			<strong><?= $objeto['carr_nombre'] ?></strong>
		<?php endif ?>
			
			<?= ' | '.$objeto['moda_nombre'] ?>
			<?php //= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
	</section>
	
	<section class="content">
		<div class="row"><div class="col-lg-8 col-lg-offset-2">
			
			<?php if ($edit): ?>
			<div class="callout callout-danger">
				<p>Para cerrar su Plan de Mejora debe finalizar cada matriz, descargarla <i class="fa fa-download"></i>, 
					firmarla y subirla <i class="fa fa-upload"></i> escaneada.
				</p>
			</div>
			<?php endif ?>
			
			<form class="form-horizontal ficha" method="post" action="<?= URL::query(['acev_id' => $objeto['acev_id']]) ?>">
				<hr>
				<table  class="table table-bordered">
					<thead>
						<tr class="success" nobr="true">
							<th class="text-center tdocumento-nombre">
								Matrices del Plan de Mejora<br>
								<!--small>Finalice cada matriz, descárguela, fírmela, escanéala, y súbala</small-->
							</th>
							<th class="text-center testado">Estado</th>
							<th class="text-center tdocumento-pdf">
								<label title="Subir archivo correspondiente">
									Documento firmado</label></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($aAcreditadoEtapaFicha as $oAcreditadoEtapaFicha): ?>
						<tr>
							<td>
								<?php $query = ['action' => 'finish'] ?>

								<a class="btn btn-flat btn-xs btn-github 
									<?= ($all_atendidas AND $oAcreditadoEtapaFicha->acef_por_subir) ? NULL : 'disabled' ?>" 
									target="_blank" 
									href="<?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_path ?>/pdf<?= URL::query() . '&' .  $query['action'] . '=1' ?>"
									data-toggle="tooltip" title="Descargar matriz">
									<i class="fa fa-download"></i>
									PDF</a>
								<a href="<?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_path.URL::query() ?>" target="_blank">
									<?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?></a>
							</td>
							<td>
								<label class="label label-<?= $oAcreditadoEtapaFicha->class_estado() ?>">
									<?= $oAcreditadoEtapaFicha->estado() ?></label>
							</td>
							
							<td class="td-pdf tdocumento-pdf">
								
								<?php if ($edit): ?>
									
									<?php if ($all_atendidas AND $oAcreditadoEtapaFicha->acef_por_subir): ?>
										<div class="input-group-btn file_upload">
											<span class="btn btn-default btn-flat btn-file btn_file_upload span_input_pdf" 
												data-toggle="tooltip" data-title="Subir matriz firmada">
												<i class="fa fa-upload"></i>
												<input class="concept-file" name="file" type="file" 
													<?= $oAcreditadoEtapaFicha->acef_path ? NULL : 'required' ?> />
												<input type="hidden" name="identifier" 
													value="<?= "{$oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_code}-{$oAcreditadoEtapaFicha->acef_id}" ?>" />
											</span>
										</div>

										<div class="input-group-addon file_process hidden">
											<img src="/media/gproc_lte/img/ajax-loader.gif">
										</div>
									<?php elseif ( ! $all_atendidas): ?>
										<div class="input-group-btn">
											<div class="btn">
												<i class="fa fa-exclamation-triangle text-yellow" data-toggle="tooltip"
													title="Debe finalizar todas las matrices"></i>
											</div>
										</div>
									<?php else: ?>
										<div class="input-group-btn">
											<div class="btn btn-flat">
												<i class="fa fa-check text-success" data-toggle="tooltip"
													title="Matriz ya fue subida"></i>
											</div>
										</div>
										<input type="hidden" name="identifier" 
											value="<?= "{$oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_code}-{$oAcreditadoEtapaFicha->acef_id}" ?>" />
									<?php endif ?>
									
								<?php endif ?>

								<div class="input-group-btn">
									
									<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
										class="btn btn-default btn-flat btn_file_view 
											<?= $oAcreditadoEtapaFicha->show_documento() ? NULL : 'disabled' ?> ?>" 
										target="_blank" data-title="Ver documento" 
										<?= $oAcreditadoEtapaFicha->show_documento() ? 'data-toggle="tooltip"' : NULL ?> >
										<i class="fa fa-file-text full"></i>
										<i class="fa fa-file-text-o empty"></i>
									</a>
									
									<?php if ($oAcreditadoEtapaFicha->acef_por_subir): ?>
									<input type="hidden" 
										name="<?= "documentos[{$oAcreditadoEtapaFicha->acef_id}" ?>]" 
										value="<?= $oAcreditadoEtapaFicha->acef_path ?>">
									<?php endif ?>
									
								</div>
							</td>
							
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				
				<?php if ($edit AND $all_atendidas): ?>
				<div class="_actions">
					<button type="button" name="finish" class="btn btn-flat btn-default btn_save" value="0">
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1">
						Cerrar Plan de Mejora</button>
					<div class="pull-right">
						<a href="/planesMejora/documentos" class="btn btn-flat btn-danger"> Cancelar</a>
					</div>
				</div>
				<?php else: ?>
				<big>
					<label class="label label-<?= $oAcreditadoEtapa->class_estado() ?>">
						Plan de Mejora <?= $oAcreditadoEtapa->estado() ?></label></big>
				<?php endif ?>
				
			</form>
		</div></div>
	</section>
</aside>

<form class="form_outside" method="post" action="/cerrarPlanMejora/uploadDocumentacion<?= URL::query() ?>" enctype="multipart/form-data"></form>
