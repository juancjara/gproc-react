<?php /*debug($action);*/ $edit = ($action == 'save') ;
	//debug($edit); ?>
<?php
	//$edit = FALSE;
?>

<aside class="right-side">
	
	<section class="content-header">
		
		<?php if ($edit): ?>
		<div class="pull-right">
			<h4 class="text-right title_resumen"></h4>
			<div class="progress xs progress-striped active bar_progress" _data-toggle="tooltip" 
				title="" data-placement="bottom">

				<div class="progress-bar progress-bar-success bar_procesados" role="progressbar" 
					data-toggle="tooltip" title="" _data-placement="bottom">
				</div>
			</div>
			<div class="text-right total_result"></div>
		</div>
		<?php endif ?>
		
		<h5 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h4 class="text-center">
			Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
			<label class="label label-sm label-<?= $oAcreditadoEtapaFicha->class_estado() ?>">
				<?= $oAcreditadoEtapaFicha->estado() ?></label>
		</h4>

		<h5 class="text-center">
			<?= ($oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_estado == Model_AcreditadoEtapa::ESTADO_OBSERVADO AND $oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_fecha_fin_observado) ? 
				"Levantar observación antes de: ".date('d/m', strtotime($oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_fecha_fin_observado)) : NULL 
			?>
		</h5>
		
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
		
	</section>
	
	<section class="content">
	
	
		<form class="form-horizontal ficha" method="post" action="<?= URL::query(['acev_id' => $objeto['acev_id']]) ?>">
			
			<div class="_actions">
				
				<?php if ($edit): ?>
					
					<button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/planificacion/pdf<?= URL::query() ?>"
						title="Descargar ficha" data-toggle="tooltip">
						<i class="fa fa-download"></i> PDF
						</button>
					<button type="button" name="finish" class="btn btn-flat btn-success btn_save" value="0" disabled>
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1" disabled>
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/planificacion" class="btn btn-flat btn-danger">Cancelar</a>
					</div>
					
				<?php elseif ($oAcreditadoEtapaFicha->acef_path): ?>
					
					<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
						target="_blank" class="btn btn-flat btn-github">
						<i class="fa fa-file-text"></i>
						Descargar documento firmado</a>
					
				<?php endif ?>
				
			</div>
			
			<hr>

			<?php if ($edit): ?>
			<div class="callout callout-warning">
				<p>Recuerde que puede guardar su ficha sin terminar. El sistema autoguardará cada 15 min.</p>
			</div>
			<?php endif ?>
			
			<div class="pull-right table-totales" id="table-total-wrapper"></div>
			
			<div id="table-planificacion"></div>
			
			<?php //debug($grupos) ?>
			<table border="1" class="table table-bordered">
				<thead>
					<tr class="success text-center" nobr="true">
						<th class="tobjetivo" title="Asociar cada objetivo específico con una DIMENSIÓN de la matriz de calidad">
							Objetivo Específico (O.E.)</th>
						<th class="tresultado" title="Cada resultado debe estar asociado a un FACTOR de la matriz de calidad">
							Resultados o Productos (R.)</th>
						<th class="tladoderecho text-center">
							Actividades (A.)
						</th>
					</tr>
				</thead>
				<tbody>

					<?php // Dimensiones (objetivos) ?>
					<?php $oe = 0; ?>
					<?php $re = 0; ?>
					<?php //debug($dimensiones) ?>
					<?php //debug($data) ?>
					<?php /*foreach ($dimensiones as $val1 => $row1): ?>
						<tr>
							<th rowspan="<?= count($row1['data']) ?>" class="tobjetivo">
								<small><?= $row1['dime'] ?></small><br>
								O.E.<?= ++$oe ?>
								
								<?php if ($edit): ?>
								<textarea maxlength="" placeholder="Ingrese Objetivo Específico" class="form-control" rows="15" 
									name="dimensiones[<?= $row1['dime_codigo'] ?>][objetivo]" 
									required><?= Arr::path($data, "dimensiones|{$row1['dime_codigo']}|objetivo", NULL, '|') ?></textarea>
								<?php else: ?>
								<p class="form-control-static">
									<?= Arr::path($data, "dimensiones|{$row1['dime_codigo']}|objetivo", NULL, '|') ?></p>
								<?php endif ?>
								
							</th>

							<?php //debug2 ($data);
								end($row1['data']);
								$last1 = key($row1['data']);
							?>

						<?php // Factores (resultados) ?>
						<?php foreach ($row1['data'] as $val2 => $row2): ?>

							<?php
								//debug2($row2);
								$rows = count($row2['data']);
								end($row2['data']);
								$last2 = key($row2['data']);
							?>
							<th class="tresultado">
								
								<?php $hide = FALSE //Arr::path($data, "factores|{$row2['fact_codigo']}|hide") ?>
								
								<div class="<?= $hide ? 'hidden' : NULL ?>">
									<label title="Cada resultado debe estar asociado a un FACTOR de la matriz de calidad">
										<small>
											<?= $row2['fact'] ?>
										</small>
										<br>
										R.<?= ++$re ?>
									</label>

									<?php if ($edit): ?>
									<textarea maxlength="500" placeholder="Ingrese Resultado o Producto" class="form-control" rows="15" 
										name="factores[<?= $row2['fact_codigo'] ?>][resultado]" 
										required><?= Arr::path($data, "factores|{$row2['fact_codigo']}|resultado", NULL, '|') ?></textarea>
									<?php else: ?>
									<p class="form-control-static">
										<?= Arr::path($data, "factores|{$row2['fact_codigo']}|resultado", NULL, '|') ?></p>
									<?php endif ?>
								</div>
								
							</th>

							<?php // Estandares  ?>
							<td class="estandares_data tladoderecho" colspan="7">
								
								<?php if ($edit): ?>
								<input type="hidden" name="factores[<?= $row2['fact_codigo'] ?>][hide]" 
									value="<?= $hide ?>" />
								
								<div class="pull-right">
									<button type="button" 
										class="btn btn-flat btn-sm btn-danger btn_remove_resultado <?= $hide ? 'hidden' : NULL ?>">
										<i class="fa fa-times"></i> Eliminar resultado
									</button>
									
									<button type="button" 
										class="btn btn-flat btn-sm btn-success btn_restore_resultado <?= $hide ? NULL : 'hidden' ?>">
										<i class="fa fa-undo"></i> Reestablecer resultado
									</button>
								</div>
								<?php endif ?>
								
								<div class="wrapper_actividades <?= $hide ? 'hidden' : NULL ?>">
									<table class="_table table-condensed table-centered table_actividades">

										<tbody class="calc_total">

										<?php // actividades ?>
										<?php foreach (Arr::path($data, "actividades|{$row2['fact_codigo']}", 
											array(0 => NULL), "|") as $i => $actividad): ?>

											<tr class="tr-actividad" 
												data-subactividades='<?= json_encode(Arr::path($data2, "actividades|{$row2['fact_codigo']}|{$i}|subactividades", array(), "|")) ?>' 
												data-factor="<?= $row2['fact_codigo'] ?>"
												data-id="<?= $i ?>">
												<td>
													<?php $j = $i + 1 ?>

													<table class="table table-condensed table-bordered">
														<tbody>
															<tr>
																<td colspan="7" class="_tactividades_inner">
																	<span class="strong act_name"><?= "A.{$re}.{$j}" ?></span>

																	<?php if ($edit): ?>
																	<textarea class="form-control actividad_input" maxlength="500" 
																		placeholder="Ingrese Descripción de la Actividad a Desarrollar" 
																		class="form-control" rows="5" 
																		name="<?= "actividades[{$row2['fact_codigo']}][$i][miactividad]" ?>" 
																		required><?= Arr::path($actividad, 'miactividad'); ?></textarea>
																	<?php else: ?>
																	<p class="form-control-static">
																		<?= Arr::path($actividad, 'miactividad'); ?></p>
																	<?php endif ?>

																</td>
															</tr>
															<tr>
																<?php foreach ($grupos as $grupo): ?>
																	<th class="td-total text-right"><?= $grupo['grup_short'] ?></th>
																<?php endforeach ?>

																<th class="tactores_inner" 
																	title="Actores Institucionales involucrados (responsable y miembros del equipo técnico por actividad)">Actores Involucrados</th>
																<th class="tejecucion_inner">Ejecución (quincenas)</th>															
															</tr>
															<tr>
																<?php foreach ($grupos as $grupo): ?>
																<td class="td-total" title="<?= $grupo['grup_short'] ?>">

																	<?php if ($edit): ?>
																	<input class="form-control text-right total-act" name=""
																		value="<?= Arr::path($data2, 
																			"actividades|{$row2['fact_codigo']}|{$i}|totales|{$grupo['grup_id']}", 
																			NULL, '|') ?>" 
																		disabled>
																	<?php else: ?>
																	<p class="form-control-static text-right">
																		<?= Arr::path($data2, 
																			"actividades|{$row2['fact_codigo']}|{$i}|totales|{$grupo['grup_id']}", 
																			NULL, '|') ?></p>
																	<?php endif ?>

																</td>
																<?php endforeach ?>

																<td class="text-center tactores_inner">

																	<?php if ($edit): ?>
																	<input name="<?= "actividades[{$row2['fact_codigo']}][$i][actor_cargo]" ?>" 
																		value="<?= Arr::path($actividad, 'actor_cargo'); ?>" 
																		class="tactorcargo form-control text-center" required type="text" 
																		placeholder="Cargo que ocupa">
																	<?php else: ?>
																	<p class="form-control-static text-center">
																		<?= Arr::path($actividad, 'actor_cargo'); ?></p>
																	<?php endif ?>

																</td>
																<td class="text-center tejecucion_inner">

																	<?php if ($edit): ?>
																	<input placeholder="Quincenas" class="form-control text-center" 
																		value="<?= Arr::path($data5, 
																			"actividad|{$row2['fact_codigo']}|{$i}|duracion", NULL, '|'); ?>" 
																		disabled />
																	<?php else: ?>
																	<p class="form-control-static text-center">
																		<?= Arr::path($data5, 
																			"actividades|{$row2['fact_codigo']}|{$i}|duracion", NULL, '|'); ?></p>
																	<?php endif ?>

																</td>
															</tr>
															<tr>
																<td colspan="7">
																	Seleccione Estándares con los que se relaciona la actividad:

																	<table class="_table dynamic">
																		<tbody>

																			<?php foreach (Arr::path($actividad, 'estandares',  
																				array(0 => NULL), "|") as $j => $estandar): ?>
																				<tr data-id="<?= $j ?>">

																					<?php if ($edit): ?>
																					<td>
																						<select name="<?= "actividades[{$row2['fact_codigo']}][$i][estandares][]" ?>" 
																							data-selected="<?= $estandar ?>" 
																							data-id="<?= $val2 ?>" 
																							data-fact="<?= $row2['fact'] ?>" 
																							class="form-control selectestandar" required>
																						</select>
																					</td>
																					<td class="td-actions"><br>
																						<button type="button" title="Eliminar estándar" class="btn btn-xs btn-flat btn-danger remove_tr">
																							<i class="fa fa-times"></i>
																						</button>
																					</td>
																					<?php else: ?>
																					<td>
																						<p class="form-control-static">
																							<?= $lista_estandares[$estandar]['esta'] ?></p>
																					</td>
																					<?php endif ?>

																				</tr>
																			<?php endforeach ?>

																		</tbody>
																	</table>

																	<?php if ($edit): ?>
																	<button type="button" class="btn btn-xs btn-flat btn-primary add_tr espacio_inf_table" title="Agregar estándar">
																		<i class="fa fa-plus"></i>
																	</button>
																	<?php endif ?>

																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>

										<?php endforeach ?>

										</tbody>
									</table>

									<?php if ($edit): ?>
									<button type="button" class="btn btn-primary btn-sm btn-flat add_tr_act" title="Agregar actividad">
										Agregar Actividad
									</button>
									<button type="button" class="btn btn-danger btn-sm btn-flat remove_tr_act" title="Eliminar actividad">
										Eliminar Actividad
									</button>
									<button type="button" name="finish" class="btn btn-flat btn-default btn-sm btn_save pull-right _guardaract" 
										value="0">
										Guardar
									</button>
									<?php endif ?>
								</div>
								
							</td>

						</tr>

						<?php if ($val2 != $last1): ?> <tr> <?php endif ?>

						<?php endforeach ?>

					<?php endforeach*/ ?>

				</tbody>
			</table>
			
			<?= Theme_View::factory('fichas/firmas', ['data' => $data] + compact('edit')) ?>
			
			<div class="_actions">
				
				<?php if ($edit): ?>
					
					<!--button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/planificacion/pdf<?= URL::query() ?>"
						title="Descargar ficha" data-toggle="tooltip">
						<i class="fa fa-download"></i> PDF
						</button-->
					<button type="button" name="finish" class="btn btn-flat btn-success btn_save" value="0" disabled>
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1" disabled>
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/planificacion" class="btn btn-flat btn-danger">Cancelar</a>
					</div>
					
				<?php elseif ($oAcreditadoEtapaFicha->acef_path): ?>
					
					<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
						target="_blank" class="btn btn-flat btn-github">
						<i class="fa fa-file-text"></i>
						Descargar documento firmado</a>
					
				<?php endif ?>
				
			</div>
			
		</form>
	</section>
</aside>

<input type="hidden" id="estandares" value='<?= json_encode($grupos_estandares) ?>' />
<input type="hidden" class="aGrupo" value='<?= json_encode($aGrupo) ?>' />
<input type="hidden" id="data" value='<?= replace(json_encode($data)) ?>' />
<input type="hidden" id="data2" value='<?= replace(json_encode($data2)) ?>' />
<input type="hidden" id="data5" value='<?= replace(json_encode($data5)) ?>' />
<input type="hidden" id="dimensiones" value='<?= json_encode($dimensiones) ?>' />
<input type="hidden" id="grupos" value='<?= json_encode($grupos) ?>' />
<input type="hidden" id="lista_estandares" value='<?= json_encode($lista_estandares) ?>' />

<iframe class="iframe"></iframe>
