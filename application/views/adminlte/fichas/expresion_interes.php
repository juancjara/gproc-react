<?php $pattern = "^(\d+|N\/A|S\/D)$" ?>
<?php //$action = 'upload' ?>
<?php //$action = 'save' ?>
<?php $edit = ($action == 'save') ?>

<div class="temp_data">
<?php //$edit = TRUE ?>
<?php //$institucional = TRUE ?>
<?php //$universidad = FALSE ?>
</div>

<script>
	window.__INITIAL_STATE__ = JSON.parse('<?= $data ?>') || undefined;
	window.__SERVER_RENDER__ = false;
	window.__CONTEXT__ = JSON.parse('<?= $context ?>');
</script>

<aside class="right-side _strech">
	
	<section class="content-header">
		<h4 class="text-center">
			<?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
			<label class="label label-sm label-<?= $oAcreditadoEtapaFicha->class_estado() ?>">
				<?= $oAcreditadoEtapaFicha->estado() ?></label>
		</h4>
		<h5 class="text-center">
			<strong><?= $objeto['inst_nombre'] ?></strong>
			
		<?php if ($objeto['carr_id']): ?>
		</h5>
		<h5 class="text-center">
			<strong><?= $objeto['carr_nombre'] ?></strong>
		<?php endif ?>
			
			<?= ' | '.$objeto['moda_nombre'] ?>
			<?php //= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
	</section>
	
	<!-- Main content -->
	<section class="content">
		
		<?php if ($action == 'view' AND $oAcreditadoEtapaFicha->acef_path): ?>
			<div class="well system-status">
				<a class="btn btn-flat btn-github" 
					href="file/<?= $oAcreditadoEtapaFicha->acef_path ?>">
					<i class="fa fa-file-text"></i> Descargar PDF firmado
				</a>
			</div>
		<?php endif ?>
		
		<?php if ($edit): ?>
			<div class="alert alert-info alert-dismissable ficha-info">
				<i class="fa fa-info"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<ul>
					<li>
						Esta ficha consta de <?= $institucional ? 2 : 3 ?> secciones
						(
							<span class="label label-default">Datos del responsable</span>
							<span class="label label-default">Datos institucionales</span>
							<?php if ( ! $institucional): ?>
								<span class="label label-default">Datos de la carrera</span>
							<?php endif ?>
						)
					</li>
					<?php if ($institucional): ?>
					<!--li>
						Se crearán tantas secciones
						<small class="label label-warning">Datos institucionales</small>
					</li-->
					<?php endif ?>
					<li>
						Cada sección cuenta con un estado
						(
							<label class="badge bg-green" title="Guardado"><i class="fa fa-save"></i></label> 
							Guardado,
							<label class="badge bg-yellow" title="Pendiente"><i class="fa fa-exclamation"></i></label> 
							Pendiente,
							<label class="badge bg-yellow" title="Guardando..."><i class="fa fa-spin fa-spinner"></i></label>
							Guardando...<!--,
							<label class="badge bg-red" title="Error al guardar"><i class="fa fa-exclamation-triangle"></i></label>
							Error al guardar-->
						), siendo GUARDADO el estado con el que usted debe cerrar su ficha
					</li>
					<li>
						El estado de cada una de las secciones determina el estado de la ficha
					</li>
				</ul>
			</div>
			<h5>Si no se cuenta con la información solicitada, especificar en el campo correspondiente: 
				No aplicable por no corresponder (N/A) y/o Sin Dato Actualizado (S/D)
			</h5>
		<?php endif ?>
		
		<?php if ($action == 'upload'): ?>
			<div class="alert alert-info _alert-dismissable">
				<i class="fa fa-info"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				Para finalizar, deberá seguir los siguientes pasos:
				<ol>
					<li>Descargar esta ficha</li>
					<li>Imprimirla</li>
					<li>Firmarla donde corresponde</li>
					<li>Escanearla</li>
					<li>Subirla.</li>
				</ol>
			</div>
			
			<div class="well system-status">
				<form 
					action="<?= '/expresionInteres/finish'.URL::query() ?>" 
					method="post" 
					enctype="multipart/form-data">

					<h4>Subir documento</h4><hr />
					<div class="form-group">
						<input type="file" name="file" class="form-control" required>
					</div><hr />
					
					<button class="btn btn-flat btn-github download_ficha" 
						data-url="<?= '/expresionInteres/pdf'.URL::query() ?>"
						type="button" 
						title="Descargar PDF final">
						<i class="fa fa-download"></i> PDF
					</button>
					
					<button type="submit" name="action" value="finish" 
						class="btn btn-flat btn-primary">Terminar
					</button>
					
					<?php if (ACL::instance()->check_role(Model_Role::ROLE_ADMIN)): ?>
					<div class="pull-right">
						<a href="<?= '/expresionInteres/reedit'.URL::query() ?>" 
							class="btn btn-flat btn-warning">
							<small class="label label-danger"
								title="Admin">
								<i class="fa fa-lock"></i>
							</small>
							Volver a editar
						</a>
					</div>
					<?php endif ?>
				</form>
			</div>
			
		<?php endif ?>

		<div id="root">
			<?php //$html ?>
		</div>

		<hr>

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
