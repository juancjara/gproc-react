<?php $edit = ($action == 'save') ?>
<?php
	$TH_SIZE = 70;
	$TH_ACT = 60;
	$PADDING = 8;
	//$edit = FALSE;
?>

<aside class="right-side">
	<section class="content-header">
		<h5 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h4 class="text-center">
			Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
			<label class="label label-sm label-<?= $oAcreditadoEtapaFicha->class_estado() ?>">
				<?= $oAcreditadoEtapaFicha->estado() ?></label>
		</h4>
		
		<h5 class="text-center">
			<?= ($oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_estado == Model_AcreditadoEtapa::ESTADO_OBSERVADO && $oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_fecha_fin_observado) ? 
				"Levantar observación antes de: ".date('d/m', strtotime($oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_fecha_fin_observado)) : NULL 
			?>
		</h5>
		
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
	</section>
	
	<section class="content">
		<form class="form-horizontal ficha" method="post" action="<?= URL::query(['acev_id' => $objeto['acev_id']]) ?>">
			
			<div class="_actions">
				
				<?php if ($edit): ?>
					
					<!--button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/cronogramaOld/pdf<?= URL::query() ?>"
						title="Descargar ficha" data-toggle="tooltip">
						<i class="fa fa-download"></i> PDF
						</button-->
					<button type="button" name="finish" class="btn btn-flat btn-success btn_save" value="0">
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1">
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/cronogramaOld" class="btn btn-flat btn-danger">Cancelar</a>
					</div>
					
				<?php elseif ($oAcreditadoEtapaFicha->acef_path): ?>
					
					<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
						target="_blank" class="btn btn-flat btn-github">
						<i class="fa fa-file-text"></i>
						Descargar documento firmado</a>
					
				<?php endif ?>
				
			</div>
			
			<hr>
			
			<div class="callout callout-warning">
				<p>Con el cursor sobre la actividad puede mover <i class="fa fa-arrows"></i> la quincena de inicio de cada 
					actividad y/o ajustar <i class="fa fa-arrows-h"></i> la duración.</p>
			</div>
			
			<div class="crono_wrapper">
				<table border="1" class="table table-bordered crono_table" style="min-width: <?= $TH_SIZE * $max_quincenas + $TH_ACT ?>px;">
					<thead>
						<tr class="success" nobr="true">

							<th class="th-act" _style="width: <?= $TH_ACT ?>px;"></th>
							<?php for ($i = 0; $i < $max_quincenas; $i++): ?>
							<th style="width: <?= $TH_SIZE ?>px;">Q<?= $i + 1 ?></th>
							<?php endfor ?>

						</tr>
					</thead>
					<tbody>

						<?php // factores ?>
						<?php $re = 0 ?>
						<?php foreach (Arr::path($data, 'actividades', array()) as $val1 => $row1): ?>
							
							<?php if (Arr::path($data, "factores|{$val1}|hide", NULL, '|')) continue ?>
							
							<?php $re++ ?>
							
							<?php // actividades ?>
							<?php foreach ($row1 as $val2 => $row2): ?>
								<?php $act = $val2 + 1 ?>

								<tr>
									<th title="<?= htmlentities($row2['miactividad']) ?>"><?= "A.{$re}.{$act}" ?></th>

									<?php // quincenas ?>
									<?php for ($i = 0; $i < $max_quincenas; $i++): ?>

										<?php $inicio = Arr::path($data5, "actividades|{$val1}|{$val2}|inicio", 0, '|') ?>
										<?php $duracion = Arr::path($data5, "actividades|{$val1}|{$val2}|duracion", 1, '|') ?>

										<?php if ($edit): ?>
										<td class="crono_td">
											
											<?php if ($i == 0): ?>
											<div class="crono_div" 
												style="width: <?= $TH_SIZE * $duracion - $PADDING * 2 ?>px;">

												<input type="hidden" name="<?= "actividades[{$val1}][{$val2}][inicio]" ?>" 
													value="<?= $inicio ?>" class="inicio" />
												<input type="hidden" name="<?= "actividades[{$val1}][{$val2}][duracion]" ?>" 
													value="<?= $duracion ?>" class="duracion" />

											</div>
											<?php endif ?>
										</td>
										<?php else: ?>
										<td class="crono_td <?= ($inicio <= $i AND $i < $inicio + $duracion) ? 'success' : NULL ?>"
											title="<?= "A.{$re}.{$act}" ?>. Inicio: Q<?= $inicio + 1 ?>, Duración: <?= 
												$duracion ?> quincenas"></td>
										<?php endif ?>

									<?php endfor ?>

								</tr>

							<?php endforeach ?>

						<?php endforeach ?>

					</tbody>
				</table>
			</div>
			
			<?= Theme_View::factory('fichas/firmas', ['data' => $data5] + compact('edit')) ?>

			<div class="_actions">
				
				<?php if ($edit): ?>
					
					<!--button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/cronogramaOld/pdf<?= URL::query() ?>"
						title="Descargar ficha" data-toggle="tooltip">
						<i class="fa fa-download"></i> PDF
						</button-->
					<button type="button" name="finish" class="btn btn-flat btn-success btn_save" value="0">
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1">
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/cronogramaOld" class="btn btn-flat btn-danger">Cancelar</a>
					</div>
					
				<?php elseif ($oAcreditadoEtapaFicha->acef_path): ?>
					
					<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
						target="_blank" class="btn btn-flat btn-github">
						<i class="fa fa-file-text"></i>
						Descargar documento firmado</a>
					
				<?php endif ?>
				
			</div>
			
		</form>
	</section>
</aside>

<input type="hidden" class="max_quincenas" value="<?= $max_quincenas ?>" />
<input type="hidden" class="th_size" value="<?= $TH_SIZE ?>" />
<input type="hidden" class="th_act" value="<?= $TH_ACT ?>" />
<input type="hidden" class="th_padding" value="<?= $PADDING ?>" />

<iframe class="iframe"></iframe>
