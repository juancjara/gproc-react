<?php /*debug($action);*/ $edit = ($action == 'save') ?>
<?php
	//$edit = FALSE;
?>

<aside class="right-side">
	
	<section class="content-header">
		<h5 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h4 class="text-center">
			Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
			<label class="label label-sm label-<?= $oAcreditadoEtapaFicha->class_estado() ?>">
				<?= $oAcreditadoEtapaFicha->estado() ?></label>
		</h4>
		
		<h5 class="text-center">
			<?= ($oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_estado == Model_AcreditadoEtapa::ESTADO_OBSERVADO && $oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_fecha_fin_observado) ? 
				"Levantar observación antes de: ".date('d/m', strtotime($oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_fecha_fin_observado)) : NULL 
			?>
		</h5>
		
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
	</section>
	
	<section class="content">
		<form class="form-horizontal ficha" method="post" action="<?= URL::query(['acev_id' => $objeto['acev_id']]) ?>">
			
			<div class="_actions">
				
				<?php if ($edit): ?>
					
					<button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/presupuestoOld/pdf<?= URL::query() ?>"
						title="Descargar ficha" data-toggle="tooltip">
						<i class="fa fa-download"></i> PDF
						</button>
					<button type="button" name="finish" class="btn btn-flat btn-success btn_save" value="0">
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1">
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/presupuestoOld" class="btn btn-flat btn-danger">Cancelar</a>
					</div>
					
				<?php elseif ($oAcreditadoEtapaFicha->acef_path): ?>
					
					<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
						target="_blank" class="btn btn-flat btn-github">
						<i class="fa fa-file-text"></i>
						Descargar documento firmado</a>
					
				<?php endif ?>
				
			</div>
			
			<hr>
			
			<?php if ($edit): ?>
			<div class="callout callout-warning">
				<p>Recuerde que puede guardar su ficha sin terminar. El sistema autoguardará cada 15 min.</p>
			</div>
			<?php endif ?>
			
			<?php if ($totalFEC): ?>
			<table class="table table-bordered table-maximo-fec">
				<tbody>
					<tr>
						<td colspan="8" class="text-right">
							Total financiamiento FEC aprobado: <?= $totalFEC ?><br>
							Monto máximo pasantías: <?= number_format($totalFEC * 0.1, 2, '.', '') ?> 
							<small class="text-primary">(10%)</small></td>
						</td>
					</tr>
					<tr>
						<th class="success">Montos máximos aprobados</th>
						<td class="text-right"><?= number_format($totalFEC * 0.4, 2, '.', '') ?> 
							<small class="text-primary">(40%)</small></td>
						<td class="text-right"><?= number_format($totalFEC * 0.3, 2, '.', '') ?> 
							<small class="text-primary">(30%)</small></td>
						<td class="text-right"><?= number_format($totalFEC * 0.6, 2, '.', '') ?> 
							<small class="text-primary">(60%)</small></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<?php endif ?>
			
			<div id="table-total-wrapper" class="clearfix"></div>
			
			<table border="1" class="table table-bordered">
				<thead>
					<tr class="success" nobr="true">
						<th class="text-center th-objetivo">
							<label title="Objetivo específico, asociado con una DIMENSIÓN de la matriz de Calidad">
								O.E.</label></th>
						<th class="text-center th-resultado">
							<label title="Resultado o Producto, asociado a un FACTOR de la matriz de calidad">
								R.</label></th>
						<th class="text-center th-actividades">
							Subactividades
						</th>
					</tr>
				</thead>
				<tbody>
					<?php // Dimensiones (objetivos) ?>
					<?php $oe = 0; ?>
					<?php $re = 0; ?>
					<?php foreach ($dimensiones as $val1 => $row1): ?>
						
						<?php if (Arr::path($data, "dimensiones|{$row1['dime_codigo']}|hide", NULL, '|')) continue ?>
						<?php $nFactores = Arr::path($data, "dimensiones|{$row1['dime_codigo']}|nFactores", count($row1['data']), '|') ?>
						
						<tr>
							<th rowspan="<?= $nFactores ?>" class="th-objetivo text-center">
								<!--small><?= $row1['dime'] ?></small><br-->
								<label title="<?= htmlentities(Arr::path($data, "dimensiones|{$row1['dime_codigo']}|objetivo", NULL, '|')) ?>">
									O.E.<?= ++$oe ?></label>
							</th>
							
							<?php
								//debug2 ($data);
								//end($row1['data']);
								//$last1 = key($row1['data']);
								foreach ($row1['data'] as $val2 => $row2)
									if ( ! Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|'))
										$last1 = $val2;
							?>
							
						<?php // Factores (resultados) ?>
						<?php foreach ($row1['data'] as $val2 => $row2): ?>
							
							<?php if (Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|')) continue ?>
							
							<?php
								//$rows = count($row2['data']);
								//end($row2['data']);
								//$last2 = key($row2['data']);
							?>
							
							<th class="th-resultado text-center">
								<label title="<?= htmlentities(Arr::path($data, "factores|{$row2['fact_codigo']}|resultado", NULL, '|')) ?>">
									R.<?= ++$re ?>
								</label>
							</th>
							
							<?php // Actividades  ?>
							<td class="estandares_data td-actividades" 
								data-re="<?= $re ?>" 
								data-detalles='<?= replace(json_encode(Arr::path($data, "actividades|".$row2['fact_codigo'], array(), "|"))) ?>' 
								data-subactividades='<?= replace(json_encode(Arr::path($data2, "actividades|{$row2['fact_codigo']}", array(), "|"))) ?>' 
								data-factor="<?= $row2['fact_codigo'] ?>" 
								data-titulo="<?= $row2['fact'] ?>">
								
							</td>
						</tr>
						
						<?php if ($val2 != $last1 ): ?> <tr> <?php endif ?>
						
						<?php endforeach ?>
					
					<?php endforeach ?>
					
				</tbody>
			</table>
			
			<?= Theme_View::factory('fichas/firmas', ['data' => $data2] + compact('edit')) ?>
			
			<div class="_actions">
				
				<?php if ($edit): ?>
					
					<!--button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/presupuestoOld/pdf<?= URL::query() ?>"
						title="Descargar ficha" data-toggle="tooltip">
						<i class="fa fa-download"></i> PDF
						</button-->
					<button type="button" name="finish" class="btn btn-flat btn-success btn_save" value="0">
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1">
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/presupuestoOld" class="btn btn-flat btn-danger">Cancelar</a>
					</div>
					
				<?php elseif ($oAcreditadoEtapaFicha->acef_path): ?>
					
					<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
						target="_blank" class="btn btn-flat btn-github">
						<i class="fa fa-file-text"></i>
						Descargar documento firmado</a>
					
				<?php endif ?>
				
			</div>
			
		</form>
	</section>
</aside>

<input type="hidden" class="aGrupo" value='<?= json_encode($aGrupo) ?>' />
<input type="hidden" class="aGasto" value='<?= json_encode($aGasto) ?>' />
<input type="hidden" class="aTipo" value='<?= json_encode($aTipo) ?>' />
<input type="hidden" class="aItem" value='<?= json_encode($aItem) ?>' />

<!-- TEMP -->
<input type="hidden" class="totalFECaprobado" value="<?= $totalFEC ?>" />

<iframe class="iframe"></iframe>
