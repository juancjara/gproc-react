<?php $edit = ($action == 'save') ?>

<aside class="right-side">
	<section class="content-header">
		
		<?php if ($edit): ?>
		<div class="pull-right">
			<h4 class="text-right title_resumen">
			</h4>
			<div class="progress xs progress-striped active bar_progress" _data-toggle="tooltip" 
				title="" data-placement="bottom">

				<div class="progress-bar progress-bar-success bar_procesados" role="progressbar" 
					data-toggle="tooltip" title="" _data-placement="bottom">
				</div>
			</div>
			<div class="text-right total_result"></div>
		</div>
		<?php endif ?>

		<h5 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h4 class="text-center">
			Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
			<label class="label label-sm label-<?= $oAcreditadoEtapaFicha->class_estado() ?>">
				<?= $oAcreditadoEtapaFicha->estado() ?></label>
		</h4>
		
		<h5 class="text-center">
			<?= ($oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_estado == Model_AcreditadoEtapa::ESTADO_OBSERVADO && $oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_fecha_fin_observado) ? 
				"Levantar observación antes de: ".date('d/m', strtotime($oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_fecha_fin_observado)) : NULL 
			?>
		</h5>
		
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>

	</section>
	
	<section class="content">
		<form class="form-horizontal ficha" method="post" action="<?= URL::query(['acev_id' => $objeto['acev_id']]) ?>">

			<div class="_actions">
				
				<?php if ($edit): ?>
					
					<!--button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/monitoreoOld/pdf<?= URL::query() ?>"
						title="Descargar ficha" data-toggle="tooltip">
						<i class="fa fa-download"></i> PDF
						</button-->
					<button type="button" name="finish" class="btn btn-flat btn-success btn_save" value="0">
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1">
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/monitoreoOld" class="btn btn-flat btn-danger">Cancelar</a>
					</div>
					
				<?php elseif ($oAcreditadoEtapaFicha->acef_path): ?>
					
					<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
						target="_blank" class="btn btn-flat btn-github">
						<i class="fa fa-file-text"></i>
						Descargar documento firmado</a>
					
				<?php endif ?>
				
			</div>
			
			<hr>

			<table border="1" class="table table-bordered">
				<thead>
					<tr class="success" nobr="true">
						<th class="text-center tobjetivo"><label title="Asociar cada objetivo específico con una DIMENSIÓN de la matriz de Calidad">O.E.</label></th>
						<th class="text-center tladoderecho">
							<table>
								<thead>
									<tr>
										<th class="text-center tindicadores" style="border-right: 1px solid #ddd">Indicadores<br><br></th>
										<th class="text-center tmedios" title="Se recomienda asociarlos a los medios de verificación planteados en la matriz de calidad. Se pueden incluir medios de verificación adicionales">Medios de Verificación</th>
									</tr>
								</thead>
							</table>
						</th>
					</tr>
				</thead>
				<tbody>

					<?php // Dimensiones (objetivos) ?>
					<?php $oe = 0 ?>
					<?php $re = 0 ?>
					<?php foreach ($dimensiones as $val1 => $row1): ?>
						
						<?php if (Arr::path($data, "dimensiones|{$row1['dime_codigo']}|hide", NULL, '|')) continue ?>
						<?php $nFactores = Arr::path($data, "dimensiones|{$row1['dime_codigo']}|nFactores", count($row1['data']), '|') ?>
						
						<tr>
							<th rowspan="<?= $nFactores ?>" class="tobjetivo">
								<label title="<?= Arr::path($data, "dimensiones|{$row1['dime_codigo']}|objetivo", NULL, '|') ?>"
									>O.E.<?= ++$oe ?>
								</label>
							</th>

							<?php
								//debug2 ($data);
								//end($row1['data']);
								//$last1 = key($row1['data']);
								foreach ($row1['data'] as $val2 => $row2)
									if ( ! Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|'))
										$last1 = $val2;
							?>

						<?php // Factores (resultados) ?>
						<?php foreach ($row1['data'] as $val2 => $row2): ?>
							
							<?php if (Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|')) continue ?>
							
							<td>
								<table>
									<tbody>
										<tr>
											<td class="tresultado">
												<label title="Factor: <?= $row2['fact']?>">
													R.<?= ++$re ?>
												</label>
												<span>
													<?= Arr::path($data, "factores|{$row2['fact_codigo']}|resultado", NULL, '|') ?>
												</span>
											</td>
										</tr>
									<!--/thead>
									<tbody-->
										<tr>
											<td class="ladoderecho">
											<?php // Estandares  ?>
												<table class="table table-condensed table-bordered table_indicadores dynamic">
													<tbody>
														<?php foreach (Arr::path($data3, "indicadores|{$row2['fact_codigo']}", 
															array(0 => NULL), "|") as $i => $indicador): ?>

															<tr data-id="<?= $i ?>">
																<td class="tindicadores">
																	<?php if ($edit): ?>
																		<textarea class="form-control actividad_input" maxlength="500"
																			placeholder="Ingrese Indicador"
																			class="form-control" rows="7"
																			name="<?= "indicadores[{$row2['fact_codigo']}][$i][indicador]" ?>"
																			required><?= Arr::path($indicador, 'indicador'); ?></textarea>
																	<?php else: ?>
																		<p class="form-control-static">
																			<?= Arr::path($indicador, 'indicador'); ?>
																		</p>
																	<?php endif ?>
																</td>
																<td colspan="2" class="tmedios">
																	<?php if ($edit): ?>
																		Seleccione Medios de Verificación que se relacionan con el Resultado:
																	<?php endif ?>
																	<table class="dynamic2">
																		<tbody>
																			<?php foreach (Arr::path($indicador, 'medios',  
																				array(0 => NULL), "|") as $j => $estandar): ?>
																				<tr data-id="<?= $j ?>">
																					<td>
																						<?php if ($edit): ?>
																							<select name="<?= "indicadores[{$row2['fact_codigo']}][$i][medios][$j][medio]" ?>" 
																								data-selected="<?= Arr::path($estandar, 'medio'); ?>" 
																								data-id="<?= $row2['fact_codigo'] ?>" 
																								class="form-control selectmedios" required>
																							</select>
																							<input class="form-control input_otro_medio hidden" placeholder="Ingrese Otro Medio de Verificación" 
																								name="<?= "indicadores[{$row2['fact_codigo']}][$i][medios][$j][medio_nombre]" ?>" 
																								value="<?= Arr::path($estandar, 'medio_nombre'); ?>" type="text">
																						<?php else: ?>
																							<?php foreach (Arr::path($medios, $row2['fact_codigo'], []) as $medio): ?>
																								<?php if ($medio['conc_id'] == Arr::path($estandar, 'medio')): ?>
																								<p class="form-control-static"><?= "Estandar {$medio['esta_codigo']} - {$medio['conc_codigo']}. {$medio['conc_nombre']}" ?></p>
																								<?php endif ?>
																							<?php endforeach ?>

																							<?php if (Arr::path($estandar, 'medio') == 999): ?>
																							<p class="form-control-static">
																								Otro: <?= Arr::path($estandar, 'medio_nombre'); ?>
																							</p>
																							<?php endif ?>
																						<?php endif ?>

																					</td>
																					<td class="td-actions"><br>
																						<?php if ($edit): ?>
																						<button type="button" title="Eliminar estándar" class="btn btn-xs btn-flat btn-danger remove_tr">
																							<i class="fa fa-times"></i>
																						</button>
																						<?php endif ?>
																					</td>
																				</tr>
																			<?php endforeach ?>
																		</tbody>
																	</table>

																	<?php if ($edit): ?>
																	<button type="button" class="btn btn-xs btn-flat btn-primary add_tr espacio_inf_table" title="Agregar estándar">
																		<i class="fa fa-plus"></i>
																	</button>
																	<?php endif ?>

																</td>
															</tr>

														<?php endforeach ?>
													</tbody>
												</table><br>

												<?php if ($edit): ?>
												<button type="button" class="btn btn-primary btn-sm btn-flat add_tr_ind" title="Agregar Indicador">
													Agregar Indicador
												</button>
												<button type="button" class="btn btn-danger btn-sm btn-flat remove_tr_ind" title="Eliminar Indicador">
													Eliminar Indicador
												</button>
												<button type="button" name="finish" class="btn btn-flat btn-default btn_save pull-right" value="0">
													Guardar</button>
												<?php endif ?>

											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						
						<?php if ($val2 != $last1): ?> <tr> <?php endif ?>
					
						<?php endforeach ?>
					<?php endforeach ?>
				
				</tbody>
			</table>
			
			<?= Theme_View::factory('fichas/firmas', ['data' => $data3] + compact('edit')) ?>

			<div class="_actions">
				
				<?php if ($edit): ?>
					
					<!--button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/monitoreoOld/pdf<?= URL::query() ?>"
						title="Descargar ficha" data-toggle="tooltip">
						<i class="fa fa-download"></i> PDF
						</button-->
					<button type="button" name="finish" class="btn btn-flat btn-success btn_save" value="0">
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1">
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/monitoreoOld" class="btn btn-flat btn-danger">Cancelar</a>
					</div>
					
				<?php elseif ($oAcreditadoEtapaFicha->acef_path): ?>
					
					<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
						target="_blank" class="btn btn-flat btn-github">
						<i class="fa fa-file-text"></i>
						Descargar documento firmado</a>
					
				<?php endif ?>
				
			</div>

		</form>
	</section>
</aside>

<input type="hidden" id="medios" value='<?= json_encode($medios) ?>'>
<input type="hidden" id="estandares" value='<?php //= json_encode($object) ?>'>
<!--input type="hidden" id="medios" value='<?php //= json_encode($factores_medios) ?>'-->

<iframe class="iframe"></iframe>
