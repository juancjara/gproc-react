<?php $edit = ($action == 'save') ?>

<aside class="right-side">
	<section class="content-header">
		<h5 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h4 class="text-center">
			Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
			<label class="label label-sm label-<?= $oAcreditadoEtapaFicha->class_estado() ?>">
				<?= $oAcreditadoEtapaFicha->estado() ?></label>
		</h4>
		
		<h5 class="text-center">
			<?= ($oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_estado == Model_AcreditadoEtapa::ESTADO_OBSERVADO && $oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_fecha_fin_observado) ? 
				"Levantar observación antes de: ".date('d/m', strtotime($oAcreditadoEtapaFicha->oAcreditadoEtapa->acet_fecha_fin_observado)) : NULL 
			?>
		</h5>
		
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
	</section>
	
	<section class="content">
		<form class="form-horizontal ficha" method="post" action="<?= URL::query(['acev_id' => $objeto['acev_id']]) ?>">
			
			<div class="_actions">
				
				<?php if ($edit): ?>
					
					<!--button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/efectosOld/pdf<?= URL::query() ?>"
						title="Descargar ficha" data-toggle="tooltip">
						<i class="fa fa-download"></i> PDF
						</button-->
					<button type="button" name="finish" class="btn btn-flat btn-success btn_save" value="0">
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1">
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/efectosOld" class="btn btn-flat btn-danger">Cancelar</a>
					</div>
					
				<?php elseif ($oAcreditadoEtapaFicha->acef_path): ?>
					
					<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
						target="_blank" class="btn btn-flat btn-github">
						<i class="fa fa-file-text"></i>
						Descargar documento firmado</a>
					
				<?php endif ?>
				
			</div>
			
			<hr>
			
			<table class="table table-bordered">
				<thead>
					<tr class="success text-center" >
						<th class="tobjetivo" title="Asociar cada objetivo específico con una DIMENSIÓN de la matriz de Calidad">
							O.E.</th>
						<th class="tresultado" title="Cada resultado debe estar asociado a un FACTOR de la matriz de calidad">
							R.</th>
						<th class="t-ladoderecho">
							<table class="table-condensed table-efectos">
								<thead>
									<tr>
										<th class=" success t-actividades text-center" title="Se incluirán una o más actividades por cada estándar que se quiera lograr. Es posible que una actividad contribuya al logro de más de un estándar"> 
											Actividades (A.)</th>
										<th	class=" success t-efectos text-center" title="Se debe explicar como se espera que la ejecución de cada actividad incida en la mejora de la calidad de la educación">
											Efecto Esperado</th>
									</tr>
								</thead>
							</table>
						</th>
						
					</tr>
				</thead>
				<tbody>

					<?php // Dimensiones (objetivos) ?>
					<?php $oe = 0 ?>
					<?php $re = 0 ?>
					<?php foreach ($dimensiones as $val1 => $row1): ?>
						
						<?php if (Arr::path($data, "dimensiones|{$row1['dime_codigo']}|hide", NULL, '|')) continue ?>
						<?php $nFactores = Arr::path($data, "dimensiones|{$row1['dime_codigo']}|nFactores", count($row1['data']), '|') ?>
						
						<tr>
							<th rowspan="<?= $nFactores ?>" class="tobjetivo">
								<label title="<?= Arr::path($data, "dimensiones|{$row1['dime_codigo']}|objetivo", NULL, '|') ?>">
									O.E.<?= ++$oe ?></label>
							</th>
							
							<?php
								//debug2 ($data);
								//end($row1['data']);
								//$last1 = key($row1['data']);
								foreach ($row1['data'] as $val2 => $row2)
									if ( ! Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|'))
										$last1 = $val2;
							?>
							
						<?php // Factores (resultados) ?>
						<?php foreach ($row1['data'] as $val2 => $row2): ?>
							
							<?php if (Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|')) continue ?>
							
							<?php
								//$rows = count($row2['data']);
								//end($row2['data']);
								//$last2 = key($row2['data']);
							?>
							
							<th class="tresultado">
								<label title="<?= Arr::path($data, "factores|{$row2['fact_codigo']}|resultado", NULL, '|') ?>">
									R.<?= ++$re ?>
								</label>
							</th>

							<?php // Estandares  ?>
							<td class="t-ladoderecho">
								<table  class="table table-bordered table_actividades">
									<tbody>
									<?php // actividades ?>
									<?php foreach (Arr::path($data, "actividades|{$row2['fact_codigo']}", 
										array(0 => NULL), "|") as $i => $actividad): ?>
										<tr data-id="<?= $i ?>">
											<td class="t-actividades">
												<?php $j = $i + 1 ?>
												<strong><?= "A.{$re}.{$j}" ?></strong><br>
												<?= Arr::path($actividad, 'miactividad'); ?>
											</td>
											<td class="t-efectos">
												<?php if ($edit): ?>
												<textarea class="form-control" maxlength="1500" placeholder="Ingrese Efecto Esperado" rows="4" required
													name="<?= "efectos[{$row2['fact_codigo']}][$i][efecto]" ?>" 
													><?= Arr::path($data4, "efectos|{$row2['fact_codigo']}|$i|efecto", NULL, "|") ?></textarea>
												<?php else: ?>
												<p class="form-control-static">
													<?= Arr::path($data4, "efectos|{$row2['fact_codigo']}|$i|efecto", NULL, "|") ?>
												</p>
												<?php endif ?>
											</td>
										</tr>
									<?php endforeach ?>
									</tbody>
								</table>
								
								<?php if ($edit): ?>
								<button type="button" name="next" class="btn btn-flat btn-default btn_save pull-right" value="0">
									Guardar
								</button>
								<?php endif ?>
								
							</td>
						</tr>
						<?php if ($val2 != $last1): ?> <tr> <?php endif ?>
						<?php endforeach ?>
					<?php endforeach ?>
				</tbody>
			</table>
			
			<?= Theme_View::factory('fichas/firmas', ['data' => $data4] + compact('edit')) ?>
			
			<div class="_actions">
				
				<?php if ($edit): ?>
					
					<!--button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/efectosOld/pdf<?= URL::query() ?>"
						title="Descargar ficha" data-toggle="tooltip">
						<i class="fa fa-download"></i> PDF
						</button-->
					<button type="button" name="finish" class="btn btn-flat btn-success btn_save" value="0">
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1">
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/efectosOld" class="btn btn-flat btn-danger">Cancelar</a>
					</div>
					
				<?php elseif ($oAcreditadoEtapaFicha->acef_path): ?>
					
					<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
						target="_blank" class="btn btn-flat btn-github">
						<i class="fa fa-file-text"></i>
						Descargar documento firmado</a>
					
				<?php endif ?>
				
			</div>
			
		</form>
	</section>
</aside>

<iframe class="iframe"></iframe>
