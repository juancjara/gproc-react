<?php $pattern = "^(\d+|N\/A|S\/D)$" ?>
<?php $edit = ($action == 'save') ?>

<div class="temp_data">
<?php //$edit = TRUE ?>
<?php //$institucional = TRUE ?>
<?php //$universidad = FALSE ?>
</div>


<aside class="right-side _strech">
	
	<section class="content-header">
		<h5 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
		<h4 class="text-center">
			<?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
			<label class="label label-sm label-<?= $oAcreditadoEtapaFicha->class_estado() ?>">
				<?= $oAcreditadoEtapaFicha->estado() ?></label>
		</h4>
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
	</section>
	
	<!-- Main content -->
	<section class="content">
		
		<form class="form-horizontal ficha" method="post" 
			action="<?= URL::query(['action' => $action, 'acev_id' => $objeto['acev_id']]) ?>" enctype="multipart/form-data">
			
			<div class="_actions">
				
				<?php if ($edit): ?>
					
					<button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/expresionInteresOld/pdf<?= URL::query() ?>"
						title="Descargar ficha" data-toggle="tooltip">
						<i class="fa fa-download"></i> PDF
						</button>
					<button type="button" name="finish" class="btn btn-flat btn-success btn_save" value="0"
						title="" data-toggle="tooltip">
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1"
						title="" data-toggle="tooltip">
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/expresionInteresOld" class="btn btn-flat btn-danger">Cancelar</a>
					</div>

				<?php elseif ($action == 'finish'): ?>
					
					<button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/expresionInteresOld/pdf<?= URL::query() ?>">
						<i class="fa fa-download"></i>
						Descargar ficha</button>
					
				<?php endif ?>
				
				<?php if ($action == 'view' AND $oAcreditadoEtapaFicha->acef_path): ?>
					
					<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
						target="_blank" class="btn btn-flat btn-github">
						<i class="fa fa-file-text"></i>
						Descargar documento firmado</a>
					
				<?php endif ?>
				
			</div>
			
			<hr>

			<?php if ($action == 'finish'): ?>
			<div class="alert alert-info _alert-dismissable">
				<i class="fa fa-info"></i>
				<!--button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button-->
				<!--h3>Ficha guardada!</h3-->
				Para finalizar, deberá seguir los siguientes pasos:
				<ol>
					<li>Descargar esta ficha</li>
					<li>Imprimirla</li>
					<li>Firmarla donde corresponde</li>
					<li>Escanearla</li>
					<li>Subirla.</li>
				</ol>
			</div>

			<hr>

			<div class="box">
				<div class="box-header">
					<h4 class="box-title">Subir documento</h4>
				</div>
				<div class="box-body">
					<div class="form-group">
						<input type="file" name="file" class="form-control" required>
					</div>
				</div>
			</div>
			
			<div>
				<!--button type="button" class="btn btn-flat btn-success download_ficha" 
					data-url="/expresionInteresOld/pdf">Descargar ficha</button-->
				<button type="submit" name="action" value="finish" class="btn btn-flat btn-primary">Terminar</button>
				<div class="pull-right">
					<a href="/expresionInteresOld" class="btn btn-flat btn-warning">Volver</a>
				</div>
			</div>

			<hr>
			<?php endif ?>

			<!--h4>Ficha de Inscripción y manifestación de interés - 
				Convocatoria de Planes de Mejora y Evaluaciones Externas para Instituciones Públicas de Educación Superior</h4>

			<h4><small>PROCALIDAD - IC / CHE - PMI / PMC / EE - 003-2015</small></h4-->
			
			<?php if ($edit): ?>
			<div class="callout callout-warning">
				<p>Recuerde que puede guardar su ficha sin terminar. El sistema autoguardará cada 15 min.</p>
			</div>
			<?php endif ?>
			
			<h5>Si no se cuenta con la información solicitada, especificar en el campo correspondiente: 
				No aplicable por no corresponder (N/A) y/o Sin Dato Actualizado (S/D)</h5>

			<div class="box">
				<div class="box-header">
					<h4 class="box-title"><span class="number">0.</span>
						Modalidad y Categoría de Financiamiento - Miembros del Comité de Calidad Institucional</h4>
				</div>

				<div class="box-body">
					<div class="form-group">
						<label class="col-xs-3 col-sm-2 control-label">Modalidad</label>
						<div class="col-xs-9">

							<p class="form-control-static">
							<?= $objeto['moda_nombre'] ?>
							</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 col-sm-2 control-label">Categoría de Financiamiento *</label>
						<div class="col-xs-9">

							<?php if ($edit): ?>
								<?php foreach ($oTipoFinanciamiento as $key => $val): ?>
									<div class="radio">
										<label>
											<input name="tifi_id" value="<?= $key ?>" type="radio"
												<?= (Arr::path($data, 'tifi_id') == $key) ? 'checked' : NULL ?> required ><?= $val ?>
										</label>
									</div>
								<?php endforeach ?>
							<?php else: ?>
								<p class="form-control-static"><?= Arr::get($oTipoFinanciamiento, Arr::path($data, 'tifi_id')) ?></p>
							<?php endif ?>

						</div>
					</div>
				</div>

				<div class="box-body box-responsive">
					<?php if ( ! $universidad): ?>
					<h5>Para el caso de los ISE, sólo pueden presentar Planes de Mejora para el conjunto de las carreras pedagógicas 
						o para cada una de las carreras tecnológicas.</h5>
					<?php endif ?>

					<legend>Miembros del Comité de Calidad Institucional</legend>

					<?php $cols = array(
						'pers_nombres' => 'Nombres *',
						'pers_apellidos' => 'Apellidos *',
						'pers_cargo' => 'Cargo *',
						'pers_correo' => 'Correo *',
						'pers_telefono' => 'Teléfono *',
					) ?>

					<table class="table table-condensed">
						<thead>
							<tr>
								<?php foreach ($cols as $col): ?>
								<th><?= $col ?></th>
								<?php endforeach ?>

								<?php if ($edit): ?>
								<th class="th-actions"></th>
								<?php endif ?>

							</tr>
						</thead>
						<tbody>
							<?php $comite = Arr::path($data, 'comite', array(0 => NULL)) ?>

							<?php $personas = ($institucional AND $edit) ? $aPersona : $comite ?>
								<?php //($institucional ? $aPersona : array(0 => NULL)) ?>

							<?php foreach ($personas as $i => $persona): ?>
								<tr data-id="<?= $i ?>">
									<?php foreach ($cols as $col => $nombre): ?>
									<td>

										<?php if ($edit AND ! $institucional): ?>
											<input type="<?= ($col == 'pers_correo') ? 'email' : 'text' ?>" name="<?= "comite[$i][$col]" ?>" 
												value="<?= Arr::path($persona, $col) ?>" 
												class="form-control" required>
										<?php else: ?>
											<?= Arr::path($persona, $col) ?>

											<?php if ($edit): ?>
											<input name="<?= "comite[$i][$col]" ?>" value="<?= Arr::path($persona, $col) ?>" 
												type="hidden" class="form-control">
											<?php endif ?>

										<?php endif ?>

									</td>
									<?php endforeach ?>

									<?php if ($edit AND ! $institucional): ?>
									<td class="td-actions">
										<button type="button" class="btn btn-xs btn-flat btn-danger remove_tr">
											<i class="fa fa-times"></i></button>
									</td>
									<?php endif ?>

								</tr>
							<?php endforeach ?>
						</tbody>
					</table>

					<?php if ($edit AND ! $institucional): ?>
					<button type="button" class="btn btn-xs btn-flat btn-primary add_tr _add_comite" title="Agregar miembro">
						<i class="fa fa-plus"></i></button>
					<?php endif ?>

				</div>
			</div>

			<h4 class="text-center" id="institucional">Sección A - Datos Institucionales</h4>
			<h4><small>Esta Sección es aplicable para todas y cada una de las categorías de Planes de Mejora</small></h4>

			<div class="box">
				<div class="box-header">
					<h4 class="box-title">1. Institución</h4>
				</div>

				<div class="box-body">
					<div class="form-group">
						<label class="col-xs-3 col-sm-2 control-label">Nombre de la Institución</label>
						<div class="col-xs-9">
							<?php //debug($objeto) ?>
							<p class="form-control-static">
							<?= $objeto['inst_nombre'] ?>
							</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 col-sm-2 control-label">RUC</label>
						<div class="col-xs-9 col-sm-3">
							<?php if ($edit): ?>
							<input name="ruc" value="<?= Arr::path($data, 'ruc') ?>" class="form-control" required>
							<?php else: ?>
							<p class="form-control-static"><?= Arr::path($data, 'ruc') ?></p>
							<?php endif ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 col-sm-2 control-label">Tipo de Gestión</label>
						<div class="col-xs-9">
							<p class="form-control-static"><?= $objeto['inst_gestion'] ?></p>
						</div>
					</div>

					<?php if ( ! $universidad): ?>
					<div class="form-group">
						<label class="col-xs-3 col-sm-2 control-label">Tipo</label>
						<div class="col-xs-9">
							<p class="form-control-static"><?= $objeto['inst_subtipo'] ?: '-' ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 col-sm-2 control-label">Código Modular *<!-- (sólo institutos)--></label>
						<div class="col-xs-9 col-sm-3">
							<?php if ($edit): ?>
							<input name="cod_mod" value="<?= Arr::path($data, 'cod_mod') ?>" class="form-control" required>
							<?php else: ?>
							<p class="form-control-static"><?= Arr::path($data, 'cod_mod', '-') ?></p>
							<?php endif ?>
						</div>
					</div>
					<?php endif ?>
				</div>
			</div>

			<div class="box">
				<div class="box-header">
					<h4 class="box-title">2. Sedes de la Institución y Carreras por área de conocimiento</h4>

					<?php if ($edit): ?>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-flat btn-primary btn-md add_filial" title="Agregar filial">
							<i class="fa fa-plus"></i> Agregar filial</button>
					</div>
					<?php endif ?>
				</div>

				<div class="box-body">

					<div class="row filial_list">

						<?php foreach (Arr::path($data, 'sedes', array(0 => array('nombre_sede' => 'Sede principal'))) as $i => $sede): ?>
						<div class="col-sm-6 col-lg-4 filial_item" data-id="<?= $i ?>">

							<?php //$nombre_sede = ($i == 0) ? 'Sede principal' : 'Filial '.$i ?>

							<?php if ($edit): ?>
							<input type="hidden" name="<?= "sedes[$i][nombre_sede]" ?>" value="<?= Arr::path($sede, 'nombre_sede') ?>">
							<?php endif ?>

							<div class="box">
								<div class="box-header">
									<h4 class="box-title nombre_sede"><?= Arr::path($sede, 'nombre_sede') ?></h4>

									<?php if ($edit): ?>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-flat btn-danger btn-xs remove_filial" title="Remover filial">
											<i class="fa fa-times"></i></button>
									</div>
									<?php endif ?>
								</div>
								<div class="box-body">

									<?php $cols = array(
										'direc' => 'Dirección *',
										'dist' => 'Distrito *',
										'prov' => 'Provincia *',
										'region' => 'Región *',
									) ?>

									<?php foreach ($cols as $col => $nombre): ?>
									<div class="form-group">
										<label class="col-xs-4 control-label"><?= $nombre ?></label>
										<div class="col-xs-8">
											<?php if ($edit): ?>
											<input name="<?= "sedes[$i][$col]"?>" value="<?= Arr::path($sede, $col) ?>" 
												class="form-control" required>
											<?php else: ?>
											<p class="form-control-static"><?= Arr::path($sede, $col) ?></p>
											<?php endif ?>
										</div>
									</div>
									<?php endforeach ?>

									<legend>Carreras</legend>
									<table class="table table-condensed">
										<thead>
											<tr>
												<th>Nombre *</th>
												<th>Área académica *</th>

												<?php if ($edit): ?>
												<th class="th-actions"></th>
												<?php endif ?>

											</tr>
										</thead>
										<tbody>
											<?php foreach (Arr::path($data, "sedes.$i.carreras", array(0 => NULL)) as $j => $carrera): ?>
											<tr data-id="<?= $j ?>">
												<td>
													<?php if ($edit): ?>
													<input name="<?= "sedes[$i][carreras][$j][nombre]" ?>" 
														value="<?= Arr::path($carrera, 'nombre') ?>"
														class="form-control carrera_nombre" required>
													<?php else: ?>
													<?= Arr::path($carrera, 'nombre') ?>
													<?php endif ?>
												</td>
												<td>
													<?php if ($edit): ?>
													<input name="<?= "sedes[$i][carreras][$j][area]" ?>" 
														value="<?= Arr::path($carrera, 'area') ?>"
														class="form-control carrera_area" required>
													<?php else: ?>
													<?= Arr::path($carrera, 'area') ?>
													<?php endif ?>
												</td>

												<?php if ($edit): ?>
												<td class="td-actions">
													<button type="button" class="btn btn-xs btn-flat btn-danger _remove_tr remove_carrera">
														<i class="fa fa-times"></i></button>
												</td>
												<?php endif ?>

											</tr>
											<?php endforeach ?>
										</tbody>
									</table>

									<?php if ($edit): ?>
									<button type="button" class="btn btn-sm btn-flat btn-primary add_tr add_carrera" title="Agregar carrera">
										<i class="fa fa-plus"></i> Agregar carrera</button>
									<?php endif ?>

								</div>
							</div>
						</div>
						<?php endforeach ?>

					</div>

				</div>

			</div>

			<div class="box <?= $edit ? 'cuerpo_academico' : NULL ?>">
				<div class="box-header">
					<h4 class="box-title"><span class="number">3.</span>
						Cuerpo académico de la Institución del año en curso (detallar en números) 
						<small>(registrar el mayor grado y/o título alcanzado por cada docente)</small></h4>
				</div>

				<div class="box-body">

					<div class="row"><div class="col-lg-6">

						<?php $rows = array(
							'pt' => 'Profesional Técnico',
							'pe' => 'Profesional con mención especializada',
							'bc' => 'Bachiller',
							'lt' => 'Licenciado / Titulado',
							'mm' => 'Magister / Master',
							'dr' => 'Doctor',
							'tt' => 'Total',
						) ?>

						<?php $cols = array(
							'ex' => 'Exclusiva',
							'tc' => 'Tiempo Completo',
							'tp' => 'Tiempo Parcial',
							'tt' => 'Total',
						) ?>

						<table class="table table-bordered table-condensed table-centered table_dedicacion">
							<thead>
								<tr>
									<th rowspan="2" class="col-xs-4">Grados y Títulos</th>
									<th colspan="4">Dedicación</th>
								</tr>
								<tr>
									<?php foreach ($cols as $col): ?>
									<th><?= $col ?></th>
									<?php endforeach ?>
								</tr>
							</thead>
							<tbody class="calc_total">
								<?php foreach ($rows as $i => $row): ?>
								<tr>
									<th><?= $row ?></th>

									<?php foreach ($cols as $j => $col): ?>
									<td>
										<?php if ($edit): ?>
										<input type="text" name="<?= "grados_dedicacion[$i][$j]" ?>" class="form-control" 
											value="<?= Arr::path($data, "grados_dedicacion.$i.$j") ?>" 
											pattern="<?= $pattern ?>" required 
											<?php //= ($i == 'tt' AND $j == 'tt') ? 'data-tipo="ded"' : NULL ?> 
											<?= ($j == 'tt') ? 'data-tipo="total"' : NULL ?> 
											<?= ($i == 'tt' OR $j == 'tt') ? 'readonly tabindex="-1"' : NULL ?> >
										<?php else: ?>
										<p class="form-control-static text-center"><?= Arr::path($data, "grados_dedicacion.$i.$j") ?></p>
										<?php endif ?>
									</td>
									<?php endforeach ?>

								</tr>
								<?php endforeach ?>
							</tbody>
						</table>

					</div><div class="col-sm-6">

						<?php $cols = array(
							'no' => 'Nombrado',
							'co' => 'Contratado',
							'tt' => 'Total',
						) ?>

						<table class="table table-bordered table-condensed table-centered table_condicion">
							<thead>
								<tr>
									<th rowspan="2" class="col-xs-4">Grados y Títulos</th>
									<th colspan="3">Condición</th>
								</tr>
								<tr>
									<?php foreach ($cols as $col): ?>
									<th><?= $col ?></th>
									<?php endforeach ?>
								</tr>
							</thead>
							<tbody class="calc_total">
								<?php foreach ($rows as $i => $row): ?>
								<tr>
									<th><?= $row ?></th>

									<?php foreach ($cols as $j => $col): ?>
									<td>
										<?php if ($edit): ?>
										<input type="text" name="<?= "grados_condicion[$i][$j]" ?>" class="form-control" 
											value="<?= Arr::path($data, "grados_condicion.$i.$j") ?>" 
											pattern="<?= $pattern ?>" required 
											<?php //= ($i == 'tt' AND $j == 'tt') ? 'data-tipo="con"' : NULL ?> 
											<?= ($j == 'tt') ? 'data-tipo="total"' : NULL ?> 
											<?= ($i == 'tt' AND $j == 'no') ? 'data-tipo="nom"' : NULL ?> 
											<?= ($i == 'tt' OR $j == 'tt') ? 'readonly tabindex="-1"' : NULL ?> >
										<?php else: ?>
										<p class="form-control-static text-center"><?= Arr::path($data, "grados_condicion.$i.$j") ?></p>
										<?php endif ?>
									</td>
									<?php endforeach ?>

								</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div></div><br>

					<?php if ($universidad): ?>
					<div class="row"><div class="col-sm-4">

						<?php $rows = array(
							'pr' => 'Principal',
							'as' => 'Asociado',
							'au' => 'Auxiliar',
							'tt' => 'Total',
						) ?>

						<?php $cols = array(
							'tt' => 'Total',
						) ?>

						<table class="table table-bordered table-condensed table-centered">
							<thead>
								<tr>
									<th class="col-xs-6">Categoría Docente <!--small>sólo universidades)</small--></th>
									<?php foreach ($cols as $col): ?>
									<th><?= $col ?></th>
									<?php endforeach ?>
								</tr>
							</thead>
							<tbody class="calc_total">
								<?php foreach ($rows as $i => $row): ?>
								<tr>
									<th><?= $row ?></th>

									<?php foreach ($cols as $j => $col): ?>
									<td>
										<?php if ($edit): ?>
										<input type="text" name="<?= "docentes_categoria[$i][$j]" ?>" class="form-control" 
											pattern="<?= $pattern ?>" required 
											value="<?= Arr::path($data, "docentes_categoria.$i.$j") ?>" 
											<?= ($i == 'tt') ? 'data-tipo="cat"' : NULL ?> 
											<?= ($i == 'tt') ? 'readonly tabindex="-1"' : NULL ?> >
										<?php else: ?>
										<p class="form-control-static text-center"><?= Arr::path($data, "docentes_categoria.$i.$j") ?></p>
										<?php endif ?>
									</td>
									<?php endforeach ?>

								</tr>
								<?php endforeach ?>
							</tbody>
						</table>

					</div></div>
					<?php endif ?>

				</div>

			</div>

			<div class="box">
				<div class="box-header">
					<h4 class="box-title"><span class="number">4.</span>
						Total de estudiantes de la Institución por Carrera (detallar en números)</h4>
				</div>

				<div class="box-body">

					<?php $tables = array(
						'p_admision' => 'Postulantes x Admisión',
						'm_semestre' => 'Matrículados x Semestre',
						'egresados' => 'Egresados',
						'titulados' => 'Titulados',
					) ?>

					<?php $cols = array(
						'nombre' => 'FOO',
						'x2014' => '2014',
						'x2013' => '2013',
						'x2012' => '2012',
						'x2011' => '2011',
						'x2010' => '2010',
					) ?>

					<div class="row estudiantes_list">
						<?php foreach ($tables as $table => $title): ?>
						<div class="col-sm-6">
							<table class="table table-bordered table-condensed table-centered table_carreras">
								<thead>
									<tr>
										<th rowspan="2" class="col-xs-4">Carrera</th>
										<th colspan="5"><?= $title ?></th>
									</tr>
									<tr>
										<?php foreach (array_slice($cols, 1) as $col): ?>
										<th><?= $col ?></th>
										<?php endforeach ?>
									</tr>
								</thead>
								<tbody>
									<?php foreach (Arr::path($data, $table, array('0' => NULL)) as $i => $row): ?>
									<tr data-id="<?= $i ?>">

										<?php foreach ($cols as $j => $col): ?>
										<td>
											<?php if ($edit): ?>
											<input type="text" 
												<?php if ($j == 'nombre'): ?>
												readonly tabindex="-1" 
												<?php else: ?>
												pattern="<?= $pattern ?>" required 
												<?php endif ?>
												name="<?= "{$table}[$i][$j]" ?>" class="form-control" 
												value="<?= Arr::path($row, $j) ?>">
											<?php else: ?>
											<p class="form-control-static <?= ($j == 'nombre') ? NULL : 'text-center' ?>">
												<?= Arr::path($row, $j) ?></p>
											<?php endif ?>
										</td>
										<?php endforeach ?>

									</tr>
									<?php endforeach ?>
								</tbody>
							</table><br>

							<?php /*if ($edit): ?>
							<button type="button" class="btn btn-xs btn-flat btn-primary add_tr _add_carrera_estudiantes" title="Agregar carrera">
								<i class="fa fa-plus"></i></button><hr>
							<?php endif*/ ?>

						</div>
						<?php endforeach ?>
					</div>

				</div>
			</div>

			<h4 class="text-center">
				Sección B - Presentación de la<?= $institucional ? 's' : NULL ?> Carrera<?= $institucional ? 's' : NULL ?></h4>

			<?php if ($institucional): ?>
			<h4><small>En el caso de los Planes de Mejora Insitucional, 
				se registrará la información correspondiente por cada una de las Carreras ofrecidas</small></h4>
			<?php endif ?>

			<div class="sedes_list">

				<?php foreach (Arr::path($data, 'sedes', array(0 => array('nombre_sede' => 'Sede principal'))) as $h => $sede): ?>
				<div class="well carreras_list" data-id="<?= $h ?>">

					<?php if ($institucional): ?>
					<h3><?= Arr::path($sede, 'nombre_sede') ?></h3>
					<?php endif ?>

					<?php foreach (Arr::path($sede, 'carreras', array(0 => NULL)) as $i => $carrera): ?>
					<div class="well carreras_item" data-id="<?= $i ?>" id="carreras_item_<?= $h * 100 + $i ?>">

						<?php /*if ($edit): ?>
						<div class="text-right <?= $i ? NULL : '_hidden' ?>">
							<button type="button" class="btn btn-flat btn-danger btn-sm remove_carrera_ficha" title="Remover ficha de carrera">
								<i class="fa fa-times"></i></button>
						</div><br>
						<?php endif*/ ?>

						<div class="box">
							<div class="box-header">
								<h4 class="box-title"><span class="number">1.</span>
									Carrera / Área Académica</h4>
							</div>

							<div class="box-body">
								<div class="form-group">
									<label class="col-xs-3 col-sm-2 control-label">Carrera *</label>
									<div class="col-xs-9 col-sm-3">
										<?php if ($institucional): ?>

											<?php if ($edit): ?>
											<input name="<?= "sedes[$h][carreras][$i][nombre]" ?>" value="<?= Arr::path($carrera, 'nombre')?>" 
												readonly tabindex="-1" 
												class="form-control carrera_nombre" required>
											<?php else: ?>
											<p class="form-control-static"><?= Arr::path($carrera, 'nombre')?></p>
											<?php endif ?>

										<?php else: ?>

											<p class="form-control-static"><?= $objeto['carr_nombre'] ?></p>

										<?php endif ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 col-sm-2 control-label">Área Académica *</label>
									<div class="col-xs-9 col-sm-3">
										<?php if ($institucional): ?>

											<?php if ($edit): ?>
											<input name="<?= "sedes[$h][carreras][$i][area]" ?>" value="<?= Arr::path($carrera, 'area')?>" 
												class="form-control carrera_area" readonly tabindex="-1">
											<?php else: ?>
											<p class="form-control-static"><?= Arr::path($carrera, 'area')?></p>
											<?php endif ?>

										<?php else: ?>

											<?php if ($edit): ?>
											<input name="<?= "carrera_area" ?>" value="<?= Arr::path($data, 'carrera_area')?>" 
												class="form-control" required>
											<?php else: ?>
											<p class="form-control-static"><?= Arr::path($data, 'carrera_area')?></p>
											<?php endif ?>

										<?php endif ?>
									</div>
								</div>
							</div>
						</div>

						<div class="box">
							<div class="box-header">
								<h4 class="box-title"><span class="number">2.</span>
									Miembros del Comité de Calidad de la Carrera</h4>
							</div>

							<div class="box-body">

								<?php $cols = array(
									'pers_nombres' => 'Nombres *',
									'pers_apellidos' => 'Apellidos *',
									'pers_cargo' => 'Cargo *',
									'pers_correo' => 'Correo *',
									'pers_telefono' => 'Teléfono *',
								) ?>

								<table class="table table-condensed">
									<thead>
										<tr>
											<?php foreach ($cols as $col): ?>
											<th><?= $col ?></th>
											<?php endforeach ?>

											<?php if ($edit): ?>
											<th class="th-actions"></th>
											<?php endif ?>
										</tr>
									</thead>
									<tbody>
										<?php $comite = Arr::path($carrera, 'comite', array(0 => NULL)) ?>

										<?php $personas = (! $institucional AND $edit) ? $aPersona : $comite ?>
											<?php //($institucional ? array(0 => NULL) : $aPersona) ?>

										<?php foreach ($personas as $j => $persona): ?>
											<tr data-id="<?= $j ?>">
												<?php foreach ($cols as $col => $nombre): ?>
												<td>

													<?php if ($edit AND $institucional): ?>
														<input type="<?= ($col == 'pers_correo') ? 'email' : 'text' ?>" 
															name="<?= "sedes[$h][carreras][$i][comite][$j][$col]" ?>" 
															value="<?= Arr::path($persona, $col) ?>" 
															class="form-control" required>
													<?php else: ?>
														<?= Arr::path($persona, $col) ?>

														<?php if ($edit): ?>
														<input name="<?= "sedes[$h][carreras][$i][comite][$j][$col]" ?>" 
															value="<?= Arr::path($persona, $col) ?>" 
															type="hidden" class="form-control">
														<?php endif ?>

													<?php endif ?>

												</td>
												<?php endforeach ?>

												<?php if ($edit AND $institucional): ?>
												<td class="td-actions">
													<button type="button" class="btn btn-xs btn-flat btn-danger remove_tr">
														<i class="fa fa-times"></i></button>
												</td>
												<?php endif ?>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>

								<?php if ($edit AND $institucional): ?>
								<button type="button" class="btn btn-xs btn-flat btn-primary add_tr _add_comite" title="Agregar miembro">
									<i class="fa fa-plus"></i></button>
								<?php endif ?>

							</div>
						</div>

						<div class="box">
							<div class="box-header">
								<h4 class="box-title"><span class="number">3.</span>
									Estudiantes de la Carrera (consignar en números)</h4>
							</div>

							<div class="box-body">

								<?php $rows = array(
									'2014',
									'2013',
									'2012',
									'2011',
									'2010',
								) ?>

								<?php $cols = array(
									'pa' => 'Postulantes x Admisión',
									'ms' => 'Matriculados x Semestre',
									'eg' => 'Egresados',
									'ti' => 'Titulados',
								) ?>

								<table class="table table-bordered table-condensed table-centered">
									<thead>
										<tr>
											<th rowspan="2" class="col-xs-2">Año</th>
											<?php foreach ($cols as $col): ?>
											<th colspan="2"><?= $col ?></th>
											<?php endforeach ?>
										</tr>
										<tr>
											<?php foreach ($cols as $col): ?>
											<th>M</th>
											<th>F</th>
											<?php endforeach ?>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($rows as $row): ?>
										<tr>
											<th><?= $row ?></th>

											<?php foreach ($cols as $k => $col): ?>
												<?php foreach (array('M', 'F') as $gender): ?>
												<td>
													<?php if ($edit): ?>
													<input type="text" name="<?= "sedes[$h][carreras][$i][estudiantes][$row][$k][$gender]" ?>" 
														class="form-control" pattern="<?= $pattern ?>" required 
														value="<?= Arr::path($carrera, "estudiantes.$row.$k.$gender") ?>">
													<?php else: ?>
													<p class="form-control-static text-center"><?= Arr::path($carrera, "estudiantes.$row.$k.$gender") ?></p>
													<?php endif ?>
												</td>
												<?php endforeach ?>
											<?php endforeach ?>

										</tr>
										<?php endforeach ?>
									</tbody>
								</table>

							</div>
						</div>

						<div class="box <?= $edit ? 'cuerpo_academico' : NULL ?>">
							<div class="box-header">
								<h4 class="box-title"><span class="number">4.</span>
									Cuerpo académico de la Carrera del año en curso 
									<small>(consignar en números; registrar el mayor grado y/o título alcanzado por cada docente)</small></h4>
							</div>

							<div class="box-body">

								<div class="row"><div class="col-lg-6">

									<?php $rows = array(
										'pt' => 'Profesional Técnico',
										'pe' => 'Profesional con mención especializada',
										'bc' => 'Bachiller',
										'lt' => 'Licenciado / Titulado',
										'mm' => 'Magister / Master',
										'dr' => 'Doctor',
										'tt' => 'Total',
									) ?>

									<?php $cols = array(
										'ex' => 'Exclusiva',
										'tc' => 'Tiempo Completo',
										'tp' => 'Tiempo Parcial',
										'tt' => 'Total',
									) ?>

									<table class="table table-bordered table-condensed table-centered table_dedicacion">
										<thead>
											<tr>
												<th rowspan="2" class="col-xs-4">Grados y Títulos</th>
												<th colspan="4">Dedicación</th>
											</tr>
											<tr>
												<?php foreach ($cols as $col): ?>
												<th><?= $col ?></th>
												<?php endforeach ?>
											</tr>
										</thead>
										<tbody class="calc_total">
											<?php foreach ($rows as $j => $row): ?>
											<tr>
												<th><?= $row ?></th>

												<?php foreach ($cols as $k => $col): ?>
												<td>
													<?php if ($edit): ?>
													<input type="text" name="<?= "sedes[$h][carreras][$i][grados_dedicacion][$j][$k]" ?>" 
														class="form-control" pattern="<?= $pattern ?>" required 
														value="<?= Arr::path($carrera, "grados_dedicacion.$j.$k") ?>" 
														<?php //= ($j == 'tt' AND $k == 'tt') ? 'data-tipo="ded"' : NULL ?> 
														<?= ($k == 'tt') ? 'data-tipo="total"' : NULL ?> 
														<?= ($j == 'tt' OR $k == 'tt') ? 'readonly tabindex="-1"' : NULL ?> >
													<?php else: ?>
													<p class="form-control-static text-center">
														<?= Arr::path($carrera, "grados_dedicacion.$j.$k") ?></p>
													<?php endif ?>
												</td>
												<?php endforeach ?>

											</tr>
											<?php endforeach ?>
										</tbody>
									</table>

								</div><div class="col-sm-6">

									<?php $cols = array(
										'no' => 'Nombrado',
										'co' => 'Contratado',
										'tt' => 'Total',
									) ?>

									<table class="table table-bordered table-condensed table-centered table_condicion">
										<thead>
											<tr>
												<th rowspan="2" class="col-xs-4">Grados y Títulos</th>
												<th colspan="3">Condición</th>
											</tr>
											<tr>
												<?php foreach ($cols as $col): ?>
												<th><?= $col ?></th>
												<?php endforeach ?>
											</tr>
										</thead>
										<tbody class="calc_total">
											<?php foreach ($rows as $j => $row): ?>
											<tr>
												<th><?= $row ?></th>

												<?php foreach ($cols as $k => $col): ?>
												<td>
													<?php if ($edit): ?>
													<input type="text" name="<?= "sedes[$h][carreras][$i][grados_condicion][$j][$k]" ?>" 
														class="form-control" pattern="<?= $pattern ?>" required 
														value="<?= Arr::path($carrera, "grados_condicion.$j.$k") ?>" 
														<?php //= ($j == 'tt' AND $k == 'tt') ? 'data-tipo="con"' : NULL ?> 
														<?= ($k == 'tt') ? 'data-tipo="total"' : NULL ?> 
														<?= ($j == 'tt' AND $k == 'no') ? 'data-tipo="nom"' : NULL ?> 
														<?= ($j == 'tt' OR $k == 'tt') ? 'readonly tabindex="-1"' : NULL ?> >
													<?php else: ?>
													<p class="form-control-static text-center">
														<?= Arr::path($carrera, "grados_condicion.$j.$k") ?></p>
													<?php endif ?>
												</td>
												<?php endforeach ?>

											</tr>
											<?php endforeach ?>
										</tbody>
									</table>
								</div></div><br>

								<?php if ($universidad): ?>
								<div class="row"><div class="col-sm-4">

									<?php $rows = array(
										'pr' => 'Principal',
										'as' => 'Asociado',
										'au' => 'Auxiliar',
										'tt' => 'Total',
									) ?>

									<?php $cols = array(
										'tt' => 'Total',
									) ?>

									<table class="table table-bordered table-condensed table-centered">
										<thead>
											<tr>
												<th class="col-xs-6">Categoría Docente <!--small>(sólo universidades)</small--></th>
												<?php foreach ($cols as $col): ?>
												<th><?= $col ?></th>
												<?php endforeach ?>
											</tr>
										</thead>
										<tbody class="calc_total">
											<?php foreach ($rows as $j => $row): ?>
											<tr>
												<th><?= $row ?></th>

												<?php foreach ($cols as $k => $col): ?>
												<td>
													<?php if ($edit): ?>
													<input type="text" name="<?= "sedes[$h][carreras][$i][docentes_categoria][$j][$k]" ?>" 
														class="form-control" pattern="<?= $pattern ?>" required 
														value="<?= Arr::path($carrera, "docentes_categoria.$j.$k") ?>" 
														<?= ($j == 'tt') ? 'data-tipo="cat"' : NULL ?> 
														<?= ($j == 'tt') ? 'readonly tabindex="-1"' : NULL ?> >
													<?php else: ?>
													<p class="form-control-static text-center">
														<?= Arr::path($carrera, "docentes_categoria.$j.$k") ?></p>
													<?php endif ?>
												</td>
												<?php endforeach ?>

											</tr>
											<?php endforeach ?>
										</tbody>
									</table>

								</div></div>
								<?php endif ?>

							</div>
						</div>

						<div class="box">
							<div class="box-header">
								<h4 class="box-title"><span class="number">5.</span>
									Investigaciones concluidas, publicaciones y patentes de la Carrera
									<small>En caso de tener más de una por año, separar con punto y coma (;) en cada campo</small>
								</h4>
							</div>

							<div class="box-body">

								<?php $tables = array(
									'inves' => 'Investigaciones',
									'publi' => 'Publicaciones',
									'paten' => 'Patentes',
								) ?>

								<?php $rows = array(
									'2014',
									'2013',
									'2012',
									'2011',
									'2010',
								) ?>

								<?php $cols = array(
									'inves' => array(
										'ti' => 'Título',
										'ac' => 'Área de conocimiento',
										'in' => 'Investigador(es)',
									),
									'publi' => array(
										'ti' => 'Título',
										'ac' => 'Área de conocimiento',
										'in' => 'Autor(es)',
									),
									'paten' => array(
										'ti' => 'Nombre y N° de Registro',
										'ac' => 'Modalidad',
										'in' => 'Institución que la otorga',
									),
								) ?>

								<div class="row">

									<?php foreach ($tables as $table => $nombre): ?>
									<div class="col-md-6">
										<table class="table table-bordered table-condensed table-centered">
											<thead>
												<tr>
													<th rowspan="2" class="col-xs-2">Año</th>
													<th colspan="3"><?= $nombre ?></th>
												</tr>
												<tr>
													<?php foreach ($cols[$table] as $col): ?>
													<th><?= $col ?></th>
													<?php endforeach ?>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($rows as $row): ?>
												<tr>
													<th><?= $row ?></th>

													<?php foreach ($cols[$table] as $k => $col): ?>
													<td>
														<?php if ($edit): ?>
														<input name="<?= "sedes[$h][carreras][$i][$table][$row][$k]" ?>" 
															class="form-control" 
															value="<?= Arr::path($carrera, "$table.$row.$k") ?>" required>
														<?php else: ?>
														<p class="form-control-static text-center">
															<?= Arr::path($carrera, "$table.$row.$k") ?></p>
														<?php endif ?>
													</td>
													<?php endforeach ?>

												</tr>
												<?php endforeach ?>
											</tbody>
										</table><br>
									</div>
									<?php endforeach ?>

								</div>

							</div>
						</div>

						<div class="row">

							<div class="col-sm-6">

								<div class="box">
									<div class="box-header">
										<h4 class="box-title"><span class="number">6.</span>
											Actividades de Extensión y Responsabilidad Social de la Carrera<br>
											<small>En caso de tener más de una por año, separar con punto y coma (;) en cada campo</small>
										</h4>
									</div>

									<div class="box-body">

										<?php $rows = array(
											'2014',
											'2013',
											'2012',
											'2011',
											'2010',
										) ?>

										<table class="table _table-bordered table-condensed table-centered">
											<thead>
												<tr>
													<th class="col-xs-2"></th>
													<th>Nombre de la actividad</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($rows as $row): ?>
												<tr>
													<th><?= $row ?></th>

													<td>
														<?php if ($edit): ?>
														<input name="<?= "sedes[$h][carreras][$i][actividades][$row]" ?>" 
															class="form-control" 
															value="<?= Arr::path($carrera, "actividades.$row") ?>" required>
														<?php else: ?>
														<p class="form-control-static text-center">
															<?= Arr::path($carrera, "actividades.$row") ?></p>
														<?php endif ?>
													</td>

												</tr>
												<?php endforeach ?>
											</tbody>
										</table>

									</div>
								</div>

							</div>
							<div class="col-sm-6">

								<div class="box">
									<div class="box-header">
										<h4 class="box-title"><span class="number">7.</span>
											Servicios de Bienestar de la Carrera<br>
											<small>En caso de tener más de una por año, separar con punto y coma (;) en cada campo</small>
										</h4>
									</div>

									<div class="box-body">

										<table class="table _table-bordered table-condensed table-centered">
											<thead>
												<tr>
													<th class="col-xs-2"></th>
													<th>Nombre del servicio</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($rows as $row): ?>
												<tr>
													<th><?= $row ?></th>

													<td>
														<?php if ($edit): ?>
														<input name="<?= "sedes[$h][carreras][$i][servicios][$row]" ?>" 
															class="form-control" 
															value="<?= Arr::path($carrera, "servicios.$row") ?>" required>
														<?php else: ?>
														<p class="form-control-static text-center">
															<?= Arr::path($carrera, "servicios.$row") ?></p>
														<?php endif ?>
													</td>

												</tr>
												<?php endforeach ?>
											</tbody>
										</table>

									</div>
								</div>

							</div>
						</div>

						<div class="box">
							<div class="box-header">
								<h4 class="box-title"><span class="number">8.</span>
									Vínculos interinstitucionales vigentes y activos involucrados con la Carrera</h4>
							</div>

							<div class="box-body">

								<div class="row">
									<div class="col-sm-6">

										<?php $cols = array(
											'ic' => 'Institución contraparte',
											'dv' => 'Detalle del vínculo',
										) ?>

										<table class="table table-condensed">
											<thead>
												<tr>
													<?php foreach ($cols as $col): ?>
													<th><?= $col ?></th>
													<?php endforeach ?>

													<?php if ($edit): ?>
													<th class="th-actions"></th>
													<?php endif ?>
												</tr>
											</thead>
											<tbody>

												<?php foreach (Arr::path($carrera, 'vinculos', array(0 => NULL)) as $j => $vinculo): ?>
												<tr data-id="<?= $j ?>">
													<?php foreach ($cols as $k => $col): ?>
													<td>
														<?php if ($edit): ?>
														<input name="<?= "sedes[$h][carreras][$i][vinculos][$j][$k]" ?>" 
															class="form-control" 
															value="<?= Arr::path($vinculo, $k) ?>" required>
														<?php else: ?>
														<p class="form-control-static text-center"><?= Arr::path($vinculo, $k) ?></p>
														<?php endif ?>
													</td>
													<?php endforeach ?>

													<?php if ($edit): ?>
													<td class="td-actions">
														<button type="button" class="btn btn-xs btn-flat btn-danger remove_tr">
															<i class="fa fa-times"></i></button>
													</td>
													<?php endif ?>

												</tr>
												<?php endforeach ?>

											</tbody>
										</table>

										<?php if ($edit): ?>
										<button type="button" class="btn btn-xs btn-flat btn-primary add_tr _add_vinculo" title="Agregar vínculo">
											<i class="fa fa-plus"></i></button>
										<?php endif ?>

									</div>
								</div>

							</div>
						</div>

						<div class="row">

							<div class="col-sm-6">

								<div class="box">
									<div class="box-header">
										<h4 class="box-title"><span class="number">9.</span>
											Financiamiento de la Carrera <small>(miles de nuevos soles)</small></h4>
									</div>

									<div class="box-body">

										<?php $rows = array(
											'pao' => 'Presupuesto Anual ordinario (Tesoro Público)',
											'rdr' => 'Recursos Directamente Recaudados',
											'itd' => 'Ingresos por transferencias y donaciones',
											'otr' => 'Otros (especificar)',
											'tot' => 'TOTAL',
										) ?>

										<?php $cols = array(
											'2014n' => '2014',
											'2014p' => '%',
											'2013n' => '2013',
											'2013p' => '%',
										) ?>

										<table class="table table-condensed _table-bordered table-centered">
											<thead>
												<tr>
													<th>Origen de los fondos</th>

													<?php foreach ($cols as $col): ?>
													<th><?= $col ?></th>
													<?php endforeach ?>
												</tr>
											</thead>
											<tbody class="calc_total calc_per">

												<?php foreach ($rows as $j => $row): ?>
												<tr>
													<th><?= $row ?></th>

													<?php foreach ($cols as $k => $col): ?>
													<td class="<?= ($col == '%') ? 'td_per' : NULL ?>">
														<?php if ($edit): ?>
														<input type="text" name="<?= "sedes[$h][carreras][$i][origen_fondos][$j][$k]" ?>" 
															class="form-control <?= ($col == '%') ? 'td_per' : NULL ?>" 
															pattern="<?= $pattern ?>" required 
															value="<?= Arr::path($carrera, "origen_fondos.$j.$k") ?>" 
															<?= ($j == 'tot' OR $col == '%') ? 'readonly tabindex="-1"' : NULL ?> >
														<?php else: ?>
														<p class="form-control-static text-center">
															<?= Arr::path($carrera, "origen_fondos.$j.$k") ?></p>
														<?php endif ?>
													</td>
													<?php endforeach ?>
												</tr>
												<?php endforeach ?>

											</tbody>
										</table>

									</div>
								</div>

							</div>

							<div class="col-sm-6">

								<div class="box">
									<div class="box-header">
										<h4 class="box-title"><span class="number">10.</span>
											Utilización de fondos en la Carrera <small>(miles de nuevos soles)</small></h4>
									</div>

									<div class="box-body">

										<?php $rows = array(
											's1' => 'Salarios profesores/investigadores',
											's2' => 'Salarios personal técnico y administrativo',
											'bc' => 'Bienes de capital',
											'ob' => 'Obras',
											'ou' => 'Otros usos',
											'tt' => 'TOTAL',
										) ?>

										<?php $cols = array(
											'2014n' => '2014',
											'2014p' => '%',
											'2013n' => '2013',
											'2013p' => '%',
										) ?>

										<table class="table table-condensed _table-bordered table-centered">
											<thead>
												<tr>
													<th>Utilización de los fondos</th>

													<?php foreach ($cols as $col): ?>
													<th><?= $col ?></th>
													<?php endforeach ?>
												</tr>
											</thead>
											<tbody class="calc_total calc_per">

												<?php foreach ($rows as $j => $row): ?>
												<tr>
													<th><?= $row ?></th>

													<?php foreach ($cols as $k => $col): ?>
													<td class="<?= ($col == '%') ? 'td_per' : NULL ?>">
														<?php if ($edit): ?>
														<input type="text" name="<?= "sedes[$h][carreras][$i][destino_fondos][$j][$k]" ?>" 
															class="form-control <?= ($col == '%') ? 'td_per' : NULL ?>" 
															pattern="<?= $pattern ?>" required 
															value="<?= Arr::path($carrera, "destino_fondos.$j.$k") ?>" 
															<?= ($j == 'tt' OR $col == '%') ? 'readonly tabindex="-1"' : NULL ?> >
														<?php else: ?>
														<p class="form-control-static text-center">
															<?= Arr::path($carrera, "destino_fondos.$j.$k") ?></p>
														<?php endif ?>
													</td>
													<?php endforeach ?>
												</tr>
												<?php endforeach ?>

											</tbody>
										</table>

									</div>
								</div>

							</div>

						</div>

					</div>

					<?php if ( ! $institucional) break; ?>
					<?php endforeach ?>

				</div>

				<?php if ( ! $institucional) break; ?>
				<?php endforeach ?>

			</div>

			<?php /*if ($edit AND $institucional): ?>
			<div class="text-right">
				<button type="button" class="btn btn-flat btn-primary add_carrera_ficha" title="Agregar ficha de carrera">
					<i class="fa fa-plus"></i> Agregar Sección de Carrera</button>
			</div><br>
			<?php endif*/ ?>

			<hr>
			<div class="help-block">
				Manifesto el interés de participar en la presente Convocatoria. Los datos consignados en las Secciones A y B, 
				tienen carácter de Declaración Jurada, la cual suscribo en mi condición de representante 
				autorizado para firmar el Convenio correspondiente. Para el caso de los Planes de Mejora Institucional, 
				consignar exclusivamente datos y firmas de la autoridad institucional.
			</div>

			<div class="firmas">
				<?php $types = $institucional ? ['inst'] : ['inst', 'carr'] ?>
				
				<?php $titles = [
					'inst' => 'Datos y firma de la autoridad Institucional',
					'carr' => 'Datos y firma de la autoridad de la Carrera',
				] ?>
				
				<?php foreach ($types as $type): ?>
				<div class="box">
					<div class="box-header">
						<h4 class="box-title">
							<?= $titles[$type] ?></h4>
					</div>

					<div class="box-body">
						<div class="form-group">
							<label class="col-xs-3 col-sm-2 control-label">Apellidos y nombres *</label>
							<div class="col-xs-9 col-sm-6">
								<?php if ($edit): ?>
								<input name="<?= "datos[$type][nombre]" ?>" value="<?= Arr::path($data, "datos.$type.nombre") ?>" 
									class="form-control" required>
								<?php else: ?>
								<p class="form-control-static"><?= Arr::path($data, "datos.$type.nombre") ?></p>
								<?php endif ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-3 col-sm-2 control-label">Cargo *</label>
							<div class="col-xs-9 col-sm-6">
								<?php if ($edit): ?>
								<input name="<?= "datos[$type][cargo]" ?>" value="<?= Arr::path($data, "datos.$type.cargo") ?>" 
									class="form-control" required>
								<?php else: ?>
								<p class="form-control-static"><?= Arr::path($data, "datos.$type.cargo") ?></p>
								<?php endif ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-3 col-sm-2 control-label">Dirección institucional</label>
							<div class="col-xs-9 col-sm-6">
								<?php if ($edit): ?>
								<input name="<?= "datos[$type][direccion]" ?>" value="<?= Arr::path($data, "datos.$type.direccion") ?>" 
									class="form-control" required>
								<?php else: ?>
								<p class="form-control-static"><?= Arr::path($data, "datos.$type.direccion") ?></p>
								<?php endif ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-3 col-sm-2 control-label">Teléfonos</label>
							<div class="col-xs-4 col-sm-3">
								<?php if ($edit): ?>
								<input name="<?= "datos[$type][fijo]" ?>" value="<?= Arr::path($data, "datos.$type.fijo") ?>" 
									placeholder="Fijo" class="form-control">
								<?php else: ?>
								<p class="form-control-static">Fijo: <?= Arr::path($data, "datos.$type.fijo") ?></p>
								<?php endif ?>
							</div>
							<div class="col-xs-4 col-sm-3">
								<?php if ($edit): ?>
								<input name="<?= "datos[$type][celular]" ?>" value="<?= Arr::path($data, "datos.$type.celular") ?>" 
									placeholder="Celular" class="form-control">
								<?php else: ?>
								<p class="form-control-static">Celular: <?= Arr::path($data, "datos.$type.celular") ?></p>
								<?php endif ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-3 col-sm-2 control-label">E-mail</label>
							<div class="col-xs-4 col-sm-3">
								<?php if ($edit): ?>
								<input type="email" name="<?= "datos[$type][email]" ?>" value="<?= Arr::path($data, "datos.$type.email") ?>" 
									placeholder="Institucional" class="form-control" required>
								<?php else: ?>
								<p class="form-control-static">Institucional: <?= Arr::path($data, "datos.$type.email") ?></p>
								<?php endif ?>
							</div>
							<div class="col-xs-4 col-sm-3">
								<?php if ($edit): ?>
								<input type="email" name="<?= "datos[$type][emailalt]" ?>" value="<?= Arr::path($data, "datos.$type.emailalt") ?>" 
									placeholder="Alternativo" class="form-control" required>
								<?php else: ?>
								<p class="form-control-static">Alternativo: <?= Arr::path($data, "datos.$type.emailalt") ?></p>
								<?php endif ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-3 col-sm-2 control-label">Fecha y firma</label>
							<div class="col-xs-4 col-sm-3">
								<br><br><br>
								__________________________
							</div>
							<div class="col-xs-5 col-sm-3">
								<br><br><br>
								<?php if ($edit): ?>
								<input type="hidden" name="<?= "datos[$type][date]" ?>" value="<?= date('Y-m-d') ?>" 
									class="form-control">
								<input type="hidden" name="<?= "datos[$type][date_local]" ?>" value="<?= dateformat(date('d \d\e F \d\e\l Y')) ?>" 
									class="form-control">
								<p class="form-control-static text-right"><?= dateformat(date('d \d\e F \d\e\l Y')) ?></p>
								<?php else: ?>
								<p class="form-control-static text-right"><?= Arr::path($data, "datos.$type.date_local") ?></p>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach ?>
			</div>

			<hr>
			<div id="bottom"></div>

			<div class="_actions">
				
				<?php if ($edit): ?>
					
					<button type="button" class="btn btn-flat btn-github download_ficha" 
						data-url="/expresionInteresOld/pdf<?= URL::query() ?>"
						title="Descargar ficha" data-toggle="tooltip">
						<i class="fa fa-download"></i> PDF
						</button>
					<button type="button" name="finish" class="btn btn-flat btn-success btn_save" value="0"
						title="" data-toggle="tooltip">
						Guardar sin terminar</button>
					<button type="submit" name="finish" class="btn btn-flat btn-primary btn_finish" value="1"
						title="" data-toggle="tooltip">
						Finalizar ficha</button>
					<div class="pull-right">
						<a href="/expresionInteresOld" class="btn btn-flat btn-danger">Cancelar</a>
					</div>

				<?php elseif ($action == 'finish'): ?>
					
					<button type="button" class="btn btn-flat bg-olive download_ficha" 
						data-url="/expresionInteresOld/pdf<?= URL::query() ?>">
						<i class="fa fa-download"></i>
						Descargar ficha</button>
					
				<?php endif ?>
				
				<?php if ($action == 'view'): ?>
					
					<a href="/file/<?= $oAcreditadoEtapaFicha->acef_path ?>" 
						target="_blank" class="btn btn-flat btn-github">
						<i class="fa fa-file-text"></i>
						Descargar documento firmado</a>
					
				<?php endif ?>
				
			</div>
			
		</form>

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
