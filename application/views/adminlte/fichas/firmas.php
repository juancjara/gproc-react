<div class="firmas">
	<?php $types = ['com', 'dir'] ?>

	<?php $titles = [
		'com' => 'Nombre y firma del Presidente del Comité de Calidad',
		'dir' => 'Nombre y firma del Director de la Escuela Universitaria o Director General del Instituto',
	] ?>

	<?php foreach ($types as $type): ?>
	<div class="box">
		<div class="box-header">
			<h4 class="box-title">
				<?= $titles[$type] ?></h4>
		</div>

		<div class="box-body">
			<div class="form-group">
				<label class="col-xs-3 col-sm-2 control-label">Apellidos y nombres *</label>
				<div class="col-xs-9 col-sm-6">
					<?php if ($edit): ?>
					<input name="<?= "datos[$type][nombre]" ?>" value="<?= Arr::path($data, "datos.$type.nombre") ?>" 
						class="form-control" required>
					<?php else: ?>
					<p class="form-control-static"><?= Arr::path($data, "datos.$type.nombre") ?></p>
					<?php endif ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-3 col-sm-2 control-label">Cargo *</label>
				<div class="col-xs-9 col-sm-6">
					<?php if ($edit): ?>
					<input name="<?= "datos[$type][cargo]" ?>" value="<?= Arr::path($data, "datos.$type.cargo") ?>" 
						class="form-control" required>
					<?php else: ?>
					<p class="form-control-static"><?= Arr::path($data, "datos.$type.cargo") ?></p>
					<?php endif ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-3 col-sm-2 control-label">Fecha y firma</label>
				<div class="col-xs-4 col-sm-3">
					<br><br><br>
					__________________________
				</div>
				<div class="col-xs-5 col-sm-3">
					<br><br><br>
					<?php if ($edit): ?>
					<input type="hidden" name="<?= "datos[$type][date]" ?>" value="<?= date('Y-m-d') ?>" 
						class="form-control">
					<input type="hidden" name="<?= "datos[$type][date_local]" ?>" value="<?= dateformat(date('d \d\e F \d\e\l Y')) ?>" 
						class="form-control">
					<p class="form-control-static text-right"><?= dateformat(date('d \d\e F \d\e\l Y')) ?></p>
					<?php else: ?>
					<p class="form-control-static text-right"><?= Arr::path($data, "datos.$type.date_local") ?></p>
					<?php endif ?>
				</div>
			</div>
		</div>
	</div>
	<?php endforeach ?>
</div>
