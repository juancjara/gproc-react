<div>
	<?php $types = ['com', 'dir'] ?>

	<?php $titles = [
		'com' => 'Nombre y firma del Presidente del Comité de Calidad',
		'dir' => 'Nombre y firma del Director de la Escuela Universitaria o Director General del Instituto',
	] ?>
</div>

<?php foreach ($types as $type): ?>
<table nobr="true"><tbody><tr><td>
	<h3 class="box-title">
		<?= $titles[$type] ?></h3>

	<table _nobr="true" border="1" class="firmas">
		<tbody>
			<tr>
				<th>Apellidos y nombres</th>
				<td colspan="2"><?= Arr::path($data, "datos.$type.nombre") ?></td>
			</tr>
			<tr>
				<th>Cargo</th>
				<td colspan="2"><?= Arr::path($data, "datos.$type.cargo") ?></td>
			</tr>
			<tr>
				<th>Fecha y firma</th>
				<td class="text-center">
					<div></div><div></div>
					__________________________
					<div></div>
				</td>
				<td class="text-center">
					<div></div><div></div>
					<?= Arr::path($data, "datos.$type.date_local") ?>
					<div></div>
				</td>
			</tr>
		</tbody>
	</table>
</td></tr></tbody></table><div></div>

<?php endforeach ?>

<style>
	.firmas {
		padding: 2px;
	}
</style>
