<aside class="right-side">
	
	<section class="content-header">
		<h5 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h4 class="text-center">
			Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
		</h4>
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
		
		<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado()/* OR $action != 'finish'*/): ?>
		<h3 class="text-center">(RESULTADO PARCIAL)</h3>
		<?php endif ?>
		
	</section>
	
	<section class="content">
		
		<div class="pull-right table-totales" id="table-total-wrapper">
			<table class="table" border="1" cellpadding="2">
				<thead>
					<tr class="success">
						<th></th>
						<?php foreach ($grupos_header as $grupo): ?>
						<th class="text-right"><?= ($grupo['grup_id'] == 'total') ? 'Total' : $grupo['grup_short'] ?></th>
						<?php endforeach ?>
						<th class="text-right">%</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach(['fec', 'propio', 'plan'] as $tipo): ?>
					<tr>
						<th>Total <?= strtoupper($tipo) ?></th>
						<?php foreach ($grupos_header as $grupo): ?>
						<td class="text-right">
							<?= Arr::path($data2, "totales.$tipo.{$grupo['grup_id']}") ?></td>
						<?php endforeach ?>
						<td class="text-right success"><?= Arr::path($data2, "porcentajes.$tipo") ?></td>
					</tr>
					<?php endforeach ?>
					<!--tr>
						<td colspan="9" class="text-right">
							(*) Corresponde a la Actividad de supervisión y monitoreo (1.5% del financiamiento FEC)</td>
					</tr-->
				</tbody>
			</table>
		</div>

		<table border="1" class="table table-bordered" cellpadding="5">
			<thead>
				<tr class="success" nobr="true">
					<th class="text-center th-objetivo">
						<label title="Objetivo específico, asociado con una DIMENSIÓN de la matriz de Calidad">
							O.E.</label></th>
					<th class="text-center th-resultado">
						<label title="Resultado o Producto, asociado a un FACTOR de la matriz de calidad">
							R.</label></th>
					<td class="text-center th-actividades">
						<span class="text-center">Subactividades</span>
					</td>
				</tr>
			</thead>
			<tbody>
				<?php // Dimensiones (objetivos) ?>
				<?php $oe = 0; ?>
				<?php $re = 0; ?>
				<?php foreach ($dimensiones as $val1 => $row1): ?>
					
					<?php if (Arr::path($data, "dimensiones|{$row1['dime_codigo']}|hide", NULL, '|')) continue ?>
					<?php $nFactores = Arr::path($data, "dimensiones|{$row1['dime_codigo']}|nFactores", count($row1['data']), '|') ?>
					
					<tr>
						<th rowspan="<?= $nFactores ?>" class="th-objetivo text-center">
							<!--small><?= $row1['dime'] ?></small><br-->
							<label title="<?= ''//Arr::path($data, "dimensiones|{$row1['dime_codigo']}|objetivo", NULL, '|') ?>">
								O.E.<?= ++$oe ?></label>
						</th>
						
						<?php
							//debug2 ($data);
							//end($row1['data']);
							//$last1 = key($row1['data']);
							foreach ($row1['data'] as $val2 => $row2)
								if ( ! Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|'))
									$last1 = $val2;
						?>
						
					<?php // Factores (resultados) ?>
					<?php foreach ($row1['data'] as $val2 => $row2): ?>
						
						<?php if (Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|')) continue ?>
						
						<?php
							//$rows = count($row2['data']);
							//end($row2['data']);
							//$last2 = key($row2['data']);
						?>
						
						<th class="th-resultado text-center">
							<label title="<?= ''//Arr::path($data, "factores|{$row2['fact_codigo']}|resultado", NULL, '|') ?>">
								R.<?= ++$re ?>
							</label>
						</th>

						<?php // Actividades  ?>
						<td class="estandares_data td-actividades"
							data-detalles='<?= ''//replace(json_encode(Arr::path($data, "actividades|".$row2['fact_codigo'], array(), "|"))) ?>' 
							data-subactividades='<?= ''//replace(json_encode(Arr::path($data2, "actividades|{$row2['fact_codigo']}", array(), "|"))) ?>' 
							data-factor="<?= $row2['fact_codigo'] ?>" 
							data-titulo="<?= $row2['fact'] ?>">
							
							<?php foreach (Arr::path($data2, "actividades|{$row2['fact_codigo']}", array(), "|") as $i => $actividad): ?>
							
							<table border="1" cellpadding="2" nobr="true" class="table-actividad">
								<thead>
									<tr>
										<th colspan="10">
											<strong>A.<?= $re ?>.<?= $i + 1 ?></strong><br>
											<?= Arr::path($data, "actividades|{$row2['fact_codigo']}|{$i}|miactividad", NULL, '|') ?>
										</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach (Arr::path($actividad, 'subactividades', array()) as $j => $subactividad): ?>
									<tr class="tr-subactividad">
										<td class="td-descripcion">
											<strong>Descripción</strong><br>
											<?= Arr::path($subactividad, 'descripcion') ?>
										</td>
										<td class="td-gasto">
											<strong>Gasto elegible</strong><br>
											<span><?= Arr::path($gastos, Arr::path($subactividad, 'gasto')) ?></span>
											<em><?= Arr::path($subactividad, 'gastoOtro') ?></em>
										</td>
										<td class="td-tipo">
											
											<?php if (Arr::path($subactividad, 'grupo') != 1): ?>
												
												<strong>Tipo de gasto</strong><br>
												<span><?= Arr::path($tipos, Arr::path($subactividad, 'tipo')) ?></span>
												<em><?= Arr::path($subactividad, 'tipoOtro') ?></em>
												
											<?php else: ?>
												
												<strong>Costos:</strong><br>
												<?php foreach (Arr::path($subactividad, 'costosRRHH', array()) as $key => $val): ?>
													<em><?= Arr::path($tipos, $key) ?>: </em>
													<span><?= $val ?></span><br>
												<?php endforeach ?>
												
											<?php endif ?>
											
										</td>
										<td class="td-item">
											
											<?php if (Arr::path($subactividad, 'grupo') != 1): ?>
												
												<strong>Tipo de item</strong><br>
												<span><?= Arr::path($items, Arr::path($subactividad, 'item')) ?></span>
												<em><?= Arr::path($subactividad, 'itemOtro') ?></em>
												
											<?php else: ?>
												
												<strong>Cantidades:</strong><br>
												<?php foreach (Arr::path($subactividad, 'cantidadesRRHH', array()) as $key => $val): ?>
													<em><?= Arr::path($tipos, $key) ?>: </em>
													<span><?= $val ?></span><br>
												<?php endforeach ?>
												
											<?php endif ?>
											
										</td>
										<td class="td-unidad">
											<strong>Unidad de medida</strong><br>
											<span><?= Arr::path($subactividad, 'unidad') ?></span>
										</td>
										<td class="td-costounitario">
											<strong>Costo unitario</strong><br>
											<span><?= Arr::path($subactividad, 'costounitario') ?></span>
										</td>
										<td class="td-cantidad">
											<strong>Cantidad</strong><br>
											<span><?= Arr::path($subactividad, 'cantidad') ?></span>
										</td>
										<td class="td-subtotal">
											<strong>Subtotal</strong><br>
											<span><?= Arr::path($subactividad, 'subtotal') ?></span>
										</td>
										<td class="td-financia">
											<strong>Financiamiento</strong><br>
											<span><?= $financiamiento[Arr::path($subactividad, 'financiamiento', '')] ?></span>
										</td>
									</tr>
									<?php endforeach ?>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="9">
											<strong>TOTALES POR ACTIVIDAD</strong><br>
											<table border="1" cellpadding="0">
												<tbody>
													<tr class="success text-center">
														<?php foreach ($grupos as $grupo): ?>
														<td><?= $grupo['grup_short'] ?></td>
														<?php endforeach ?>
													</tr>
													<tr class="success text-center">
														<?php foreach ($grupos as $grupo): ?>
														<td><?= Arr::path($actividad, "totales.{$grupo['grup_id']}") ?></td>
														<?php endforeach ?>
													</tr>
												</tbody>
											</table>
										</th>
									</tr>
								</tfoot>
							</table>
							
							<?php endforeach ?>
							
						</td>
					</tr>

					<?php if ($val2 != $last1 ): ?> <tr> <?php endif ?>

					<?php endforeach ?>

				<?php endforeach ?>

			</tbody>
		</table>
		
		<?= Theme_View::factory('fichas/pdf/firmas', ['data' => $data2]) ?>
		
		<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado() OR $action != 'finish'): ?>
		<h3 class="text-center">(RESULTADO PARCIAL)</h3>
		<?php endif ?>

	</section>
</aside>

<?php //die() ?>

<style>
	.success {
		background-color: #ddd;
	}
	.text-center {
		text-align: center;
	}
	.text-right {
		text-align: right;
	}
	table {
		width: 100%;
	}
	.table-actividad {
		margin-bottom: 20px;
	}
	.th-objetivo {
		width: 8%;
	}
	.th-resultado {
		width: 8%;
	}
	.th-actividades,
	.td-actividades {
		width: 84%;
	}
	
	.tr-subactividad {
		font-size: 9px;
	}
	
	.td-descripcion {
		width: 32%;
	}
	.td-gasto, 
	.td-tipo, 
	.td-item {
		width: 10%;
	}
	.td-unidad, 
	.td-costounitario, 
	.td-cantidad, 
	.td-subtotal{
		width: 8%;
	}
	.td-financia {
		width: 6%;
	}
	
</style>
