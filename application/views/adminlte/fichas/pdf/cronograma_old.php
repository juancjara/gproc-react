<?php
	$TH_SIZE = 70;
	$TH_ACT = 60;
	$PADDING = 8;
?>

<aside class="right-side">
	<section class="content-header">
		<h5 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h4 class="text-center">
			Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
		</h4>
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
		
		<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado()/* OR $action != 'finish'*/): ?>
		<h3 class="text-center">(RESULTADO PARCIAL)</h3>
		<?php endif ?>
		
	</section>
	
	<section class="content">
		
		<div class="crono_wrapper">
			<table border="1" class="table table-bordered crono_table" style="min-width: <?= $TH_SIZE * $max_quincenas + $TH_ACT ?>px;">
				<thead>
					<tr class="success" nobr="true">

						<th class="th-act" _style="width: <?= $TH_ACT ?>px;"></th>
						<?php for ($i = 0; $i < $max_quincenas; $i++): ?>
						<th class="crono_td">Q<?= $i + 1 ?></th>
						<?php endfor ?>

					</tr>
				</thead>
				<tbody>

					<?php // factores ?>
					<?php $re = 0 ?>
					<?php foreach (Arr::path($data, 'actividades', array()) as $val1 => $row1): ?>
						
						<?php if (Arr::path($data, "factores|{$val1}|hide", NULL, '|')) continue ?>
						
						<?php $re++ ?>

						<?php // actividades ?>
						<?php foreach ($row1 as $val2 => $row2): ?>
							<?php $act = $val2 + 1 ?>

							<tr>
								<th class="th-act" title="<?= ''//$row2['miactividad'] ?>">
									<strong><?= "A.{$re}.{$act}" ?></strong>
									<?php $inicio = Arr::path($data5, "actividades|{$val1}|{$val2}|inicio", 0, '|') ?>
									<?php $duracion = Arr::path($data5, "actividades|{$val1}|{$val2}|duracion", 1, '|') ?>
								
									<br>Inicio: Q<?= $inicio + 1 ?>
									<br>Duración: <?= $duracion ?>
								</th>
								
								<?php // quincenas ?>
								<?php for ($i = 0; $i < $max_quincenas; $i++): ?>

									<td class="crono_td <?= ($inicio <= $i AND $i < $inicio + $duracion) ? 'success' : NULL ?>"
										title="<?= "A.{$re}.{$act}" ?>. Inicio: Q<?= $inicio + 1 ?>, Duración: <?= 
											$duracion ?> quincenas"></td>

								<?php endfor ?>

							</tr>

						<?php endforeach ?>

					<?php endforeach ?>

				</tbody>
			</table>
		</div>
		
		<?= Theme_View::factory('fichas/pdf/firmas', ['data' => $data5]) ?>
		
		<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado() OR $action != 'finish'): ?>
		<h3 class="text-center">(RESULTADO PARCIAL)</h3>
		<?php endif ?>
		
	</section>
</aside>

<?php //die() ?>

<style>
	.success {
		background-color: #ddd;
	}
	.text-center {
		text-align: center;
	}
	.text-right {
		text-align: right;
	}
	.th-act {
		width: 10%;
	}
	.crono-td {
		width: <?= 90 / $max_quincenas * 100 ?>%;
	}
</style>
