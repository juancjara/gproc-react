<aside class="right-side">
	
	<section class="content-header">
		<h5 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h4 class="text-center">
			Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
		</h4>
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
		
		<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado()/* OR $action != 'finish'*/): ?>
		<h3 class="text-center">(RESULTADO PARCIAL)</h3>
		<?php endif ?>
		
	</section>
	
	<section class="content">
		
		<div class="pull-right table-totales" id="table-total-wrapper">
			<table class="table" border="1" cellpadding="2">
				<thead>
					<tr class="success">
						<th></th>
						<?php foreach ($grupos_header as $grupo): ?>
						<th class="text-right"><?= ($grupo['grup_id'] == 'total') ? 'Total' : $grupo['grup_short'] ?></th>
						<?php endforeach ?>
						<th class="text-right">%</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach(['fec', 'propio', 'plan'] as $tipo): ?>
					<tr>
						<th>Total <?= strtoupper($tipo) ?></th>
						<?php foreach ($grupos_header as $grupo): ?>
						<td class="text-right">
							<?= Arr::path($data2, "totales.$tipo.{$grupo['grup_id']}") ?></td>
						<?php endforeach ?>
						<td class="text-right success"><?= Arr::path($data2, "porcentajes.$tipo") ?></td>
					</tr>
					<?php endforeach ?>
					<!--tr>
						<td colspan="10" class="text-right">
							(*) Corresponde a la Actividad de supervisión y monitoreo (1.5% del financiamiento FEC)</td>
					</tr-->
				</tbody>
			</table>
		</div>

		<?php //debug($grupos) ?>
		<table border="1" class="table table-bordered" cellpadding="5">
			<thead>

				<tr class="success text-center">
					<th class="tobjetivo" title="Asociar cada objetivo específico con una DIMENSIÓN de la matriz de calidad">
						Objetivo Específico (O.E.)</th>
					<th class="tresultado" title="Cada resultado debe estar asociado a un FACTOR de la matriz de calidad">
						Resultados o Productos (R.)</th>
					<th class="tladoderecho">
						Actividades (A.)
					</th>
				</tr>
			</thead>
			<tbody>

				<?php // Dimensiones (objetivos) ?>
				<?php $oe = 0; ?>
				<?php $re = 0; ?>
				<?php foreach ($dimensiones as $val1 => $row1): ?>
					
					<?php if (Arr::path($data, "dimensiones|{$row1['dime_codigo']}|hide", NULL, '|')) continue ?>
					<?php $nFactores = Arr::path($data, "dimensiones|{$row1['dime_codigo']}|nFactores", count($row1['data']), '|') ?>
					
					<tr>
						<th rowspan="<?= $nFactores ?>" class="tobjetivo">
							<strong><?= $row1['dime'] ?></strong><br>
							O.E.<?= ++$oe ?>

							<p class="form-control-static">
								<?= Arr::path($data, "dimensiones|{$row1['dime_codigo']}|objetivo", NULL, '|') ?></p>

						</th>

						<?php
							//debug2 ($data);
							//end($row1['data']);
							//$last1 = key($row1['data']);
							foreach ($row1['data'] as $val2 => $row2)
								if ( ! Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|'))
									$last1 = $val2;
						?>

					<?php // Factores (resultados) ?>
					<?php foreach ($row1['data'] as $val2 => $row2): ?>
						
						<?php if (Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|')) continue ?>

						<?php
							//debug2($row2);
							//$rows = count($row2['data']);
							//end($row2['data']);
							//$last2 = key($row2['data']);
						?>
						
						<th class="tresultado">
							<label title="Cada resultado debe estar asociado a un FACTOR de la matriz de calidad"><strong><?= $row2['fact'] ?>
								</strong>
								<br>
								R.<?= ++$re ?>
							</label>

							<p class="form-control-static">
								<?= Arr::path($data, "factores|{$row2['fact_codigo']}|resultado", NULL, '|') ?></p>

						</th>

						<?php // Estandares  ?>
						<td class="estandares_data tladoderecho" _colspan="7">

							<table class="_table table-condensed table-centered table_actividades">

								<tbody class="calc_total">

								<?php // actividades ?>
								<?php foreach (Arr::path($data, "actividades|{$row2['fact_codigo']}", 
									array(0 => NULL), "|") as $i => $actividad): ?>

									<tr class="tr-actividad" 
										data-subactividades='<?= ''//replace(json_encode(Arr::path($data2, "actividades|{$row2['fact_codigo']}|{$i}|subactividades", array(), "|"))) ?>' 
										data-factor="<?= $row2['fact_codigo'] ?>"
										data-id="<?= $i ?>">
										<td>
											<?php $j = $i + 1 ?>

											<table class="table table-condensed table-bordered" nobr="true" border="1" cellpadding="2">
												<tbody>
													<tr>
														<td colspan="7" class="_tactividades_inner">
															<strong><?= "A.{$re}.{$j}" ?></strong>
															<br>
															<span class="form-control-static"><?= Arr::path($actividad, 'miactividad'); ?></span>

														</td>
													</tr>
													<tr>
														<?php foreach ($grupos as $grupo): ?>
														<th class="td-total text-center small"><?= $grupo['grup_short'] ?></th>
														<?php endforeach ?>
														
														<th class="tactores_inner text-center small" 
															title="Actores Institucionales involucrados (responsable y miembros del equipo técnico por actividad)">Actores Involucrados</th>
														<th class="tejecucion_inner text-center small">Ejecución (quincenas)</th>
													</tr>
													<tr>
														<?php foreach ($grupos as $grupo): ?>
														<td class="td-total" title="<?= $grupo['grup_short'] ?>">

															<p class="form-control-static text-right small" _style="font-size: 8px">
																<?= Arr::path($data2, 
																	"actividades|{$row2['fact_codigo']}|{$i}|totales|{$grupo['grup_id']}", 
																	NULL, '|') ?></p>

														</td>
														<?php endforeach ?>

														<td class="text-center tactores_inner">

															<p class="form-control-static text-center small">
																<?= Arr::path($actividad, 'actor_cargo'); ?></p>

														</td>
														<td class="text-center tejecucion_inner">

															<p class="form-control-static text-center small">
																<?= Arr::path($data5, 
																	"actividades|{$row2['fact_codigo']}|{$i}|duracion", NULL, '|'); ?></p>

														</td>
													</tr>
													<tr>
														<td colspan="7">
															<strong>Estándares con los que se relaciona la actividad:</strong><br>

															<table class="_table dynamic">
																<tbody>

																	<?php foreach (Arr::path($actividad, 'estandares',  
																		array(0 => NULL), "|") as $j => $estandar): ?>
																		<tr data-id="<?= $j ?>">

																			<td>
																				<p class="form-control-static">
																					<?= Arr::path($lista_estandares, "{$estandar}.esta") ?></p>
																			</td>

																		</tr>
																	<?php endforeach ?>

																</tbody>
															</table>

														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>

								<?php endforeach ?>

								</tbody>
							</table>

						</td>

					</tr>

					<?php if ($val2 != $last1): ?> <tr> <?php endif ?>

					<?php endforeach ?>

				<?php endforeach ?>

			</tbody>
		</table>
		
		<?= Theme_View::factory('fichas/pdf/firmas', ['data' => $data]) ?>
		
		<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado() OR $action != 'finish'): ?>
		<h3 class="text-center">(RESULTADO PARCIAL)</h3>
		<?php endif ?>

	</section>
</aside>

<style>
	.success {
		background-color: #ddd;
	}
	table {
		width: 100%;
	}
	.tobjetivo {
		width: 20%;
	}
	.tresultado {
		width: 20%;
	}
	.tladoderecho {
		width: 60%;
	}
	.text-center {
		text-align: center;
	}
	.text-right {
		text-align: right;
	}
	.td-total {
		width: 14%;
	}
	.tactores_inner {
		width: 20%;
	}
	.tejecucion_inner {
		width: 10%;
	}
	.small {
		font-size: 8px;
	}
</style>
