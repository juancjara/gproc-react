<section class="content-header">
	<h5 class="text-center">
		<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
	</h5>
	<h4 class="text-center">
		Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
	</h4>
	<h5 class="text-center">
		<?= $objeto['even_nombre'] ?>
	</h5>

	<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado()/* OR $action != 'finish'*/): ?>
	<h3 class="text-center">(RESULTADO PARCIAL)</h3>
	<?php endif ?>
	
</section>

<hr>
<table border="1" class="table">
	<thead>
		<tr class="text-center success" >
			<th class="tobjetivo">O.E.</th>
			<th class="tresultado" >R.</th>
			<th class="thactividad text-center">Actividades (A.)</th>
			<th	class="thefecto text-center" >Efectos Esperados</th>
		</tr>
	</thead>
	<tbody>
		<?php // Dimensiones (objetivos) ?>
		<?php $oe = 0 ?>
		<?php $re = 0 ?>
		<?php foreach ($dimensiones as $val1 => $row1): ?>
			
			<?php if (Arr::path($data, "dimensiones|{$row1['dime_codigo']}|hide", NULL, '|')) continue ?>
			<?php $nFactores = Arr::path($data, "dimensiones|{$row1['dime_codigo']}|nFactores", count($row1['data']), '|') ?>
			
			<tr>
				<th rowspan="<?= $nFactores ?>" class="tobjetivo">
					<p>O.E.<?= ++$oe ?></p>
				</th>
				
				<?php
					//debug2 ($data);
					//end($row1['data']);
					//$last1 = key($row1['data']);
					foreach ($row1['data'] as $val2 => $row2)
						if ( ! Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|'))
							$last1 = $val2;
				?>
				
			<?php // Factores (resultados) ?>
			<?php foreach ($row1['data'] as $val2 => $row2): ?>
				
				<?php if (Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|')) continue ?>
				
				<?php
					//$rows = count($row2['data']);
					//end($row2['data']);
					//$last2 = key($row2['data']);
				?>
				
				<th class="tresultado">
					<p>R.<?= ++$re ?></p>
				</th>

				<?php // Estandares  ?>
				<td class="tladoderecho">
					<table border="1" class="table">
						<tbody>
							
							<?php // actividades ?>
							<?php foreach (Arr::path($data, "actividades|{$row2['fact_codigo']}", 
								array(0 => NULL), "|") as $i => $actividad): ?>
								<tr data-id="<?= $i ?>">
									<td class="tactividad">
									<?php $j = $i + 1 ?>
										<strong><?= "A.{$re}.{$j}" ?></strong><br>
										<?= Arr::path($actividad, 'miactividad'); ?>
									</td>
									<td class="tefecto">
										<p><?= Arr::path($data4, "efectos|{$row2['fact_codigo']}|$i|efecto", NULL, "|") ?></p>
									</td>
								</tr>
							<?php endforeach ?>
							
						</tbody>
					</table>
				</td>
			</tr>
			
			<?php if ($val2 != $last1): ?> <tr> <?php endif ?>
			
			<?php endforeach ?>
		<?php endforeach ?>
	</tbody>
</table>

<?= Theme_View::factory('fichas/pdf/firmas', ['data' => $data4]) ?>

<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado() OR $action != 'finish'): ?>
<h3 class="text-center">(RESULTADO PARCIAL)</h3>
<?php endif ?>

<style>
	.success {
		background-color: #ddd;
	}
	.text-center {
		text-align: center;
	}
	.table {
		padding: 5px;
	}
	.tobjetivo {
		width: 6%;
	}
	.tresultado {
		width: 6%;
	}
	.tladoderecho {
		width: 88%;
	}
	.tactividad{
		width: 50%;
		text-align: justify;
	}
	.tefecto{
		width: 50%;
		text-align: justify;
	}
	.thactividad{
		width: 44%;
	}
	.thefecto{
		width: 44%;
	}
	.table-centered > thead > tr > th {
		text-align: center;
	}
	.table-centered > tbody > tr > td {
		padding: 0;
	}
</style>
