<?php //$context['universidad'] = FALSE ?>
<?php //$context['institucional'] = TRUE ?>

	<section class="content-header">
		
		<h4 class="text-center">
			<?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
			<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado()): ?>
				<strong class="text-center">(VISTA PREVIA)</strong>
			<?php endif ?>
		</h4>
		<h5 class="text-center">
			<strong><?= $objeto['inst_nombre'] ?></strong>
			
		<?php if ($objeto['carr_id']): ?>
		</h5>
		<h5 class="text-center">
			<strong><?= $objeto['carr_nombre'] ?></strong>
		<?php endif ?>
			
			<?= ' | '.$objeto['moda_nombre'] ?>
			<?php //= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
		
	</section>

	<h3 class="box-title"><span class="number">1.</span>
		Modalidad y Categoría de Financiamiento - Miembros del Comité de Calidad Institucional</h3>
	
	<h4>Miembros del Comité de Calidad Institucional</h4>

	<?php $cols = array(
		'pers_nombres' => 'Nombres',
		'pers_apellidos' => 'Apellidos',
		'pers_cargo' => 'Cargo',
		'pers_correo' => 'Correo',
		'pers_telefono' => 'Teléfono',
	) ?>

	<table nobr="true" border="1" _class="table">
		<thead>
			<tr>
				<?php foreach ($cols as $col): ?>
				<th><?= $col ?></th>
				<?php endforeach ?>
			</tr>
		</thead>
		<tbody>
			<?php $comite = Arr::path($data, 'ins.data.comite') ?>

			<?php $personas = $comite ?:
				($context['institucional'] ? $context['aPersona'] : array(0 => NULL)) ?>

			<?php foreach ($personas as $i => $persona): ?>
				<tr data-id="<?= $i ?>">
					<?php foreach ($cols as $col => $nombre): ?>
					<td>
						<?= Arr::path($persona, $col) ?>
					</td>
					<?php endforeach ?>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	
	<h2 class="text-center">Sección A - Datos Institucionales<br>
		<small>Esta Sección es aplicable para todas y cada una de las categorías de Planes de Mejora</small></h2>
	
		<h3 class="box-title">2. Institución</h3>

		<table nobr="true" border="1" _class="table">
			<tbody>
				<tr>
					<th>Nombre de la Institución</th>
					<td><?= $objeto['inst_nombre'] ?></td>
				</tr>
				<tr>
					<th>RUC</th>
					<td><?= Arr::path($data, 'ins.data.ruc') ?></td>
				</tr>
				<tr>
					<th>Tipo de Gestión</th>
					<td><?= $objeto['inst_gestion'] ?></td>
				</tr>
				
				<?php if ( ! $context['universidad']): ?>
				<tr>
					<th>Tipo</th>
					<td><?= $objeto['inst_subtipo'] ?: '-' ?></td>
				</tr>
				<tr>
					<th>Código Modular</th>
					<td><?= Arr::path($data, 'ins.data.cod_mod', '-') ?></td>
				</tr>
				<?php endif ?>
				
			</tbody>
		</table>
	
		<h3 class="box-title">3. Sedes de la Institución y Carreras por área de conocimiento</h3>

		<?php foreach (Arr::path($data, 'ins.data.sedes', array(0 => NULL)) as $i => $sede): ?>
		<table _nobr="true" _border="1" class="table">
			<thead>
				<tr>
					<th colspan="2" class="text-center"><?= Arr::path($sede, 'nombre_sede') ?><hr></th>
				</tr>
			</thead>
			<tbody>

				<?php $cols = array(
					'direc' => 'Dirección',
					'dist' => 'Distrito',
					'prov' => 'Provincia',
					'region' => 'Región',
				) ?>

				<?php foreach ($cols as $col => $nombre): ?>
				<tr>
					<th><?= $nombre ?></th>
					<td><?= Arr::path($sede, $col) ?></td>
				</tr>
				<?php endforeach ?>

				<tr>
					<th colspan="2" class="text-center">Carreras<hr></th>
				</tr>
				<tr>
					<th>Nombre</th>
					<th>Área académica</th>
				</tr>

				<?php foreach (Arr::path($data, "ins.data.sedes.$i.carreras", array(0 => NULL)) as $j => $carrera): ?>
				<tr data-id="<?= $j ?>">
					<td>
						<?= Arr::path($carrera, 'nombre') ?>
					</td>
					<td>
						<?= Arr::path($carrera, 'area') ?>
					</td>
				</tr>
				<?php endforeach ?>


			</tbody>
		</table>
		<div></div>
		<?php endforeach ?>
		
		<table nobr="true"><tbody><tr><td>
			<h3 class="box-title"><span class="number">4.</span>
				Cuerpo académico de la Institución del año en curso (detallar en números) <br>
				<small>(registrar el mayor grado y/o título alcanzado por cada docente)</small></h3>

			<?php $rows = array(
				'pt' => 'Profesional Técnico',
				'pe' => 'Profesional con mención especializada',
				'bc' => 'Bachiller',
				'lt' => 'Licenciado / Titulado',
				'mm' => 'Magister / Master',
				'dr' => 'Doctor',
				'tt' => 'Total',
			) ?>

			<?php $cols = array(
				'ex' => 'Exclusiva',
				'tc' => 'Tiempo Completo',
				'tp' => 'Tiempo Parcial',
				'tt' => 'Total',
			) ?>

			<table _nobr="true" border="1">
				<thead>
					<tr>
						<th rowspan="2" class="col-xs-4">Grados y Títulos</th>
						<th colspan="4" class="text-center">Dedicación</th>
					</tr>
					<tr>
						<?php foreach ($cols as $col): ?>
						<th class="text-center"><?= $col ?></th>
						<?php endforeach ?>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($rows as $i => $row): ?>
					<tr>
						<th><?= $row ?></th>

						<?php foreach ($cols as $j => $col): ?>
						<td>
							<p class="form-control-static text-center"><?= Arr::path($data, "ins.data.cuerpo_academico.grados_dedicacion.$i.$j") ?></p>
						</td>
						<?php endforeach ?>

					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<div></div>

			<?php $cols = array(
				'no' => 'Nombrado',
				'co' => 'Contratado',
				'tt' => 'Total',
			) ?>

			<table _nobr="true" border="1">
				<thead>
					<tr>
						<th rowspan="2" class="col-xs-4">Grados y Títulos</th>
						<th colspan="3" class="text-center">Condición</th>
					</tr>
					<tr>
						<?php foreach ($cols as $col): ?>
						<th class="text-center"><?= $col ?></th>
						<?php endforeach ?>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($rows as $i => $row): ?>
					<tr>
						<th><?= $row ?></th>

						<?php foreach ($cols as $j => $col): ?>
						<td>
							<p class="form-control-static text-center"><?= Arr::path($data, "ins.data.cuerpo_academico.grados_condicion.$i.$j") ?></p>
						</td>
						<?php endforeach ?>

					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<div></div>

			<?php if ($context['universidad']): ?>

				<?php $rows = array(
					'pr' => 'Principal',
					'as' => 'Asociado',
					'au' => 'Auxiliar',
					'tt' => 'Total',
				) ?>

				<?php $cols = array(
					'tt' => 'Total',
				) ?>

				<table _nobr="true" border="1">
					<thead>
						<tr>
							<th class="col-xs-6">Categoría Docente <small>(sólo universidades)</small></th>
							<?php foreach ($cols as $col): ?>
							<th class="text-center"><?= $col ?></th>
							<?php endforeach ?>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($rows as $i => $row): ?>
						<tr>
							<th><?= $row ?></th>

							<?php foreach ($cols as $j => $col): ?>
							<td>
								<p class="form-control-static text-center"><?= Arr::path($data, "ins.data.cuerpo_academico.docentes_categoria.$i.$j") ?></p>
							</td>
							<?php endforeach ?>

						</tr>
						<?php endforeach ?>
					</tbody>
				</table>

			<?php endif ?>
		</td></tr></tbody></table><div></div>
		
		<table nobr="true"><tbody><tr><td>
			<h3 class="box-title"><span class="number">5.</span>
				Total de estudiantes de la Institución por Carrera (detallar en números)</h3>

			<?php $tables = array(
				'p_admision' => 'Postulantes x Admisión',
				'm_semestre' => 'Matrículados x Semestre',
				'egresados' => 'Egresados',
				'titulados' => 'Titulados',
			) ?>

			<?php $cols = array(
				'nombre' => 'FOO',
				'x2015' => array('M', 'F'),
				'x2014' => array('M', 'F'),
				'x2013' => array('M', 'F'),
				'x2012' => array('M', 'F'),
				'x2011' => array('M', 'F'),
			) ?>
			

			<?php foreach ($tables as $table => $title): ?>
			<table _nobr="true" border="1">
				<thead>
					<tr>
						<th rowspan="3" class="col-xs-4">Carrera</th>
						<th colspan="10" class="text-center"><?= $title ?></th>
					</tr>
					<tr>
						<?php foreach (array_slice($cols, 1) as $col => $col_value): ?>
							<th colspan="2" class="text-center"><?= substr($col,1) ?></th>
						<?php endforeach ?>
					</tr>
					<tr>
						<?php foreach (array_slice($cols, 1) as $col => $col_value): ?>
							<?php foreach ($col_value as $genero): ?>
								<th class="text-center"><?= $genero ?></th>
							<?php endforeach ?>
						<?php endforeach ?>
					</tr>
				</thead>
				<tbody>
					<?php foreach (Arr::path($data, 'ins.data.total_estudiantes.'.$table, array(0 => NULL)) as $i => $row): ?>
					<tr data-id="<?= $i ?>">
						<?php foreach ($cols as $key => $col): ?>
							<?php if(sizeof($col) > 1) :?>
								<td class="text-center"><?= Arr::path($row, $key.'.m') ?></td>
								<td class="text-center"><?= Arr::path($row, $key.'.f') ?></td>
							<?php else: ?>
								<td>
									<p class="form-control-static text-center ">
										<?= Arr::path($row, 'nombre') ?></p>
								</td>
							<?php endif ?>
						<?php endforeach ?>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<div></div>
			<?php endforeach ?>
		</td></tr></tbody></table><div></div>
	
	<h2 class="text-center">Sección B - Presentación de la<?= $context['institucional'] ? 's' : NULL ?> Carrera<?= $context['institucional'] ? 's' : NULL ?>
		<?php if ($context['institucional']): ?>
		<br><small>En el caso de los Planes de Mejora Insitucional, 
			se registrará la información correspondiente por cada una de las Carreras ofrecidas</small>
		<?php endif ?>
	</h2>
	
	<?php foreach (Arr::path($data, 'ins.data.sedes', array(0 => NULL)) as $h => $sede): ?>
		
		<?php if ($context['institucional']): ?>
		<h2>
			<?= Arr::path($sede, 'nombre_sede') ?>
		</h2>
		<?php endif ?>
		
		<?php foreach (Arr::path($sede, 'carreras', array(0 => NULL)) as $i => $carrera): ?>
			<table nobr="true"><tbody><tr><td>
				<h3 class="box-title"><span class="number">1.</span>
					Carrera / Área Académica</h3>

				<table _nobr="true" border="1" _class="table">
					<tbody>
						<tr>
							<th>Carrera</th>
							<td>
								<?php if ($context['institucional']): ?>
								<?= Arr::path($carrera, 'nombre') ?>
								<?php else: ?>
								<?= $objeto['carr_nombre'] ?>
								<?php endif ?>
							</td>
						</tr>
						<tr>
							<th>Área Académica</th>
							<td>
								<?php if ($context['institucional']): ?>
								<?= Arr::path($carrera, 'area') ?>
								<?php else: ?>
								<?= Arr::path($data, "car.$i.data.area") ?>
								<?php endif ?>
							</td>
						</tr>
					</tbody>
				</table>
			</td></tr></tbody></table><div></div>

			<table nobr="true"><tbody><tr><td>
				<h3 class="box-title"><span class="number">2.</span>
					Miembros del Comité de Calidad de la Carrera</h3>

				<?php $cols = array(
					'pers_nombres' => 'Nombres',
					'pers_apellidos' => 'Apellidos',
					'pers_cargo' => 'Cargo',
					'pers_correo' => 'Correo',
					'pers_telefono' => 'Teléfono',
				) ?>

				<table _nobr="true" border="1" _class="table">
					<thead>
						<tr>
							<?php foreach ($cols as $col): ?>
							<th><?= $col ?></th>
							<?php endforeach ?>
						</tr>
					</thead>
					<tbody>
						<?php $comite = Arr::path($data, "car.$i.data.comite") ?>

						<?php $personas = $comite ?:
							($context['institucional'] ? array(0 => NULL) : $context['aPersona']) ?>

						<?php foreach ($personas as $j => $persona): ?>
							<tr data-id="<?= $j ?>">
								<?php foreach ($cols as $col => $nombre): ?>
								<td>
									<?= Arr::path($persona, $col) ?>
								</td>
								<?php endforeach ?>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</td></tr></tbody></table><div></div>

			<table nobr="true"><tbody><tr><td>
				<h3 class="box-title"><span class="number">3.</span>
					Estudiantes de la Carrera (consignar en números)</h3>

				<?php $rows = array(
					'2015',
					'2014',
					'2013',
					'2012',
					'2011',
				) ?>

				<?php $cols = array(
					'pa' => 'Postulantes x Admisión',
					'ms' => 'Matriculados x Semestre',
					'eg' => 'Egresados',
					'ti' => 'Titulados',
				) ?>

				<table _nobr="true" border="1">
					<thead>
						<tr>
							<th rowspan="2" class="col-xs-2">Año</th>
							<?php foreach ($cols as $col): ?>
							<th colspan="2" class="text-center"><?= $col ?></th>
							<?php endforeach ?>
						</tr>
						<tr>
							<?php foreach ($cols as $col): ?>
							<th class="text-center">M</th>
							<th class="text-center">F</th>
							<?php endforeach ?>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($rows as $row): ?>
						<tr>
							<th><?= $row ?></th>

							<?php foreach ($cols as $k => $col): ?>
								<?php foreach (array('m', 'f') as $gender): ?>
								<td>
									<p class="form-control-static text-center"><?= Arr::path($data, "car.$i.data.estudiantes.$row.$k.$gender") ?></p>
								</td>
								<?php endforeach ?>
							<?php endforeach ?>

						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</td></tr></tbody></table><div></div>

			<table nobr="true"><tbody><tr><td>
				<h3 class="box-title"><span class="number">4.</span>
					Cuerpo académico de la Carrera del año en curso (consignar en números)
					<small>(registrar el mayor grado y/o título alcanzado por cada docente)</small></h3>

				<?php $rows = array(
					'pt' => 'Profesional Técnico',
					'pe' => 'Profesional con mención especializada',
					'bc' => 'Bachiller',
					'lt' => 'Licenciado / Titulado',
					'mm' => 'Magister / Master',
					'dr' => 'Doctor',
					'tt' => 'Total',
				) ?>

				<?php $cols = array(
					'ex' => 'Exclusiva',
					'tc' => 'Tiempo Completo',
					'tp' => 'Tiempo Parcial',
					'tt' => 'Total',
				) ?>

				<table _nobr="true" border="1">
					<thead>
						<tr>
							<th rowspan="2" class="col-xs-4">Grados y Títulos</th>
							<th colspan="4" class="text-center">Dedicación</th>
						</tr>
						<tr>
							<?php foreach ($cols as $col): ?>
							<th class="text-center"><?= $col ?></th>
							<?php endforeach ?>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($rows as $j => $row): ?>
						<tr>
							<th><?= $row ?></th>

							<?php foreach ($cols as $k => $col): ?>
							<td>
								<p class="form-control-static text-center"><?= Arr::path($data, "car.$i.data.cuerpo_academico.grados_dedicacion.$j.$k") ?></p>
							</td>
							<?php endforeach ?>

						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				<div></div>

				<?php $cols = array(
					'no' => 'Nombrado',
					'co' => 'Contratado',
					'tt' => 'Total',
				) ?>

				<table _nobr="true" border="1">
					<thead>
						<tr>
							<th rowspan="2" class="col-xs-4">Grados y Títulos</th>
							<th colspan="3" class="text-center">Condición</th>
						</tr>
						<tr>
							<?php foreach ($cols as $col): ?>
							<th class="text-center"><?= $col ?></th>
							<?php endforeach ?>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($rows as $j => $row): ?>
						<tr>
							<th><?= $row ?></th>

							<?php foreach ($cols as $k => $col): ?>
							<td>
								<p class="form-control-static text-center"><?= Arr::path($data, "car.$i.data.cuerpo_academico.grados_condicion.$j.$k") ?></p>
							</td>
							<?php endforeach ?>

						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				<div></div>

				<?php if ($context['universidad']): ?>

					<?php $rows = array(
						'pr' => 'Principal',
						'as' => 'Asociado',
						'au' => 'Auxiliar',
						'tt' => 'Total',
					) ?>

					<?php $cols = array(
						'tt' => 'Total',
					) ?>

					<table _nobr="true" border="1">
						<thead>
							<tr>
								<th class="col-xs-6">Categoría Docente <small>(sólo universidades)</small></th>
								<?php foreach ($cols as $col): ?>
								<th class="text-center"><?= $col ?></th>
								<?php endforeach ?>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($rows as $j => $row): ?>
							<tr>
								<th><?= $row ?></th>

								<?php foreach ($cols as $k => $col): ?>
								<td>
									<p class="form-control-static text-center"><?= Arr::path($data, "car.$i.data.cuerpo_academico.docentes_categoria.$j.$k") ?></p>
								</td>
								<?php endforeach ?>

							</tr>
							<?php endforeach ?>
						</tbody>
					</table>

				<?php endif ?>
			</td></tr></tbody></table><div></div>

			<table nobr="true"><tbody><tr><td>
				<h3 class="box-title"><span class="number">5.</span>
					Investigaciones concluidas, publicaciones y patentes de la Carrera</h3>

				<?php $tables = array(
					'inves' => 'Investigaciones',
					'publi' => 'Publicaciones',
					'paten' => 'Patentes',
				) ?>

				<?php $rows = array(
					'2015',
					'2014',
					'2013',
					'2012',
					'2011',
				) ?>

				<?php $cols = array(
					'inves' => array(
						'ti' => 'Título',
						'ac' => 'Área de conocimiento',
						'in' => 'Investigador(es)',
					),
					'publi' => array(
						'ti' => 'Título',
						'ac' => 'Área de conocimiento',
						'in' => 'Autor(es)',
					),
					'paten' => array(
						'ti' => 'Nombre y N° de Registro',
						'ac' => 'Modalidad',
						'in' => 'Institución que la otorga',
					),
				) ?>


				<?php foreach ($tables as $table => $nombre): ?>
				<table _nobr="true" border="1">
					<thead>
						<tr>
							<th rowspan="2" class="col-xs-2">Año</th>
							<th colspan="3" class="text-center"><?= $nombre ?></th>
						</tr>
						<tr>
							<?php foreach ($cols[$table] as $col): ?>
							<th><?= $col ?></th>
							<?php endforeach ?>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($rows as $row): ?>
						<tr>
							<th><?= $row ?></th>

							<?php foreach ($cols[$table] as $k => $col): ?>
							<td>
								<p class="form-control-static text-center"><?= Arr::path($data, "car.$i.data.$table.$row.$k") ?></p>
							</td>
							<?php endforeach ?>

						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				<div></div>
				<?php endforeach ?>
			</td></tr></tbody></table>
			
			<table nobr="true"><tbody><tr><td>
				<h3 class="box-title"><span class="number">6.</span>
					Actividades de Extensión y Responsabilidad Social de la Carrera</h3>

				<?php $rows = array(
					'2015',
					'2014',
					'2013',
					'2012',
					'2011',
				) ?>

				<table _nobr="true" border="1">
					<thead>
						<tr>
							<th class="col-xs-2"></th>
							<th>Nombre de la actividad</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($rows as $row): ?>
						<tr>
							<th><?= $row ?></th>

							<td>
								<p class="form-control-static text-center"><?= Arr::path($data, "car.$i.data.actividades.$row") ?></p>
							</td>

						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</td></tr></tbody></table><div></div>

			<table nobr="true"><tbody><tr><td>
				<h3 class="box-title"><span class="number">7.</span>
					Servicios de Bienestar de la Carrera</h3>

				<table _nobr="true" border="1">
					<thead>
						<tr>
							<th class="col-xs-2"></th>
							<th>Nombre del servicio</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($rows as $row): ?>
						<tr>
							<th><?= $row ?></th>

							<td>
								<p class="form-control-static text-center"><?= Arr::path($data, "car.$i.data.servicios.$row") ?></p>
							</td>

						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</td></tr></tbody></table><div></div>
			
			<table nobr="true"><tbody><tr><td>
				<h3 class="box-title"><span class="number">8.</span>
					Vínculos interinstitucionales vigentes y activos involucrados con la Carrera</h3>

				<?php $cols = array(
					'ic' => 'Institución contraparte',
					'dv' => 'Detalle del vínculo',
				) ?>

				<table _nobr="true" border="1">
					<thead>
						<tr>
							<?php foreach ($cols as $col): ?>
							<th><?= $col ?></th>
							<?php endforeach ?>
						</tr>
					</thead>
					<tbody>

						<?php foreach (Arr::path($data, 'car.'.$i.'.data.vinculos', array(0 => NULL)) as $j => $vinculo): ?>
						<tr data-id="<?= $j ?>">
							<?php foreach ($cols as $k => $col): ?>
							<td>
								<p class="form-control-static text-center"><?= Arr::path($vinculo, $k) ?></p>
							</td>
							<?php endforeach ?>
						</tr>
						<?php endforeach ?>

					</tbody>
				</table>
			</td></tr></tbody></table><div></div>

			<table nobr="true"><tbody><tr><td>
				<h3 class="box-title"><span class="number">9.</span>
					Financiamiento de la Carrera <small>(miles de nuevos soles)</small></h3>

				<?php $rows = array(
					'pao' => 'Presupuesto Anual ordinario (Tesoro Público)',
					'rdr' => 'Recursos Directamente Recaudados',
					'itd' => 'Ingresos por transferencias y donaciones',
					'otr' => 'Otros (especificar)',
					'tt' => 'TOTAL',
				) ?>

				<?php $cols = array(
					'2015n' => '2015',
					'2015p' => '%',
					'2014n' => '2014',
					'2014p' => '%',
				) ?>

				<table _nobr="true" border="1">
					<thead>
						<tr>
							<th>Origen de los fondos</th>

							<?php foreach ($cols as $col): ?>
							<th><?= $col ?></th>
							<?php endforeach ?>
						</tr>
					</thead>
					<tbody>

						<?php foreach ($rows as $j => $row): ?>
						<tr>
							<th><?= $row ?></th>

							<?php foreach ($cols as $k => $col): ?>
							<td>
								<p class="form-control-static text-center"><?= Arr::path($data, "car.$i.data.origen_fondos.$j.$k") ?></p>
							</td>
							<?php endforeach ?>
						</tr>
						<?php endforeach ?>

					</tbody>
				</table>
			</td></tr></tbody></table><div></div>

			<table nobr="true"><tbody><tr><td>
				<h3 class="box-title"><span class="number">10.</span>
					Utilización de fondos en la Carrera <small>(miles de nuevos soles)</small></h3>

				<?php $rows = array(
					's1' => 'Salarios profesores/investigadores',
					's2' => 'Salarios personal técnico y administrativo',
					'bc' => 'Bienes de capital',
					'ob' => 'Obras',
					'ou' => 'Otros usos',
					'tt' => 'TOTAL',
				) ?>

				<?php $cols = array(
					'2015n' => '2015',
					'2015p' => '%',
					'2014n' => '2014',
					'2014p' => '%',
				) ?>

				<table _nobr="true" border="1">
					<thead>
						<tr>
							<th>Utilización de los fondos</th>

							<?php foreach ($cols as $col): ?>
							<th><?= $col ?></th>
							<?php endforeach ?>
						</tr>
					</thead>
					<tbody>

						<?php foreach ($rows as $j => $row): ?>
						<tr>
							<th><?= $row ?></th>

							<?php foreach ($cols as $k => $col): ?>
							<td>
								<p class="form-control-static text-center"><?= Arr::path($data, "car.$i.data.destino_fondos.$j.$k") ?></p>
							</td>
							<?php endforeach ?>
						</tr>
						<?php endforeach ?>

					</tbody>
				</table>
			</td></tr></tbody></table><div></div>
			<hr />
			<div></div>
			
		<?php if ( ! $context['institucional']) break; ?>
		<?php endforeach ?>
		
	<?php if ( ! $context['institucional']) break; ?>
	<?php endforeach ?>
	
	<div class="help-block">
		Manifesto el interés de participar en la presente Convocatoria. Los datos consignados en las Secciones A y B, 
		tienen carácter de Declaración Jurada, la cual suscribo en mi condición de representante 
		autorizado para firmar el Convenio correspondiente. Para el caso de los Planes de Mejora Institucional, 
		consignar exclusivamente datos y firmas de la autoridad institucional.
	</div>
	
	<?php $types = $context['institucional'] ? ['inst'] : ['inst', 'carr'] ?>
	
	<?php $titles = [
		'inst' => 'Datos y firma de la autoridad Institucional',
		'carr' => 'Datos y firma de la autoridad de la Carrera',
	] ?>
	
	<?php foreach ($types as $type): ?>
	<table nobr="true"><tbody><tr><td>
		<h3 class="box-title">
			<?= $titles[$type] ?></h3>

		<table _nobr="true" border="1">
			<tbody>
				<tr>
					<th>Apellidos y nombres</th>
					<td colspan="2"><?= Arr::path($data, "fic.data.datos.$type.nombre") ?></td>
				</tr>
				<tr>
					<th>Cargo</th>
					<td colspan="2"><?= Arr::path($data, "fic.data.datos.$type.cargo") ?></td>
				</tr>
				<tr>
					<th>Dirección institucional</th>
					<td colspan="2"><?= Arr::path($data, "fic.data.datos.$type.direccion") ?></td>
				</tr>
				<tr>
					<th>Teléfonos</th>
					<td>Fijo: <?= Arr::path($data, "fic.data.datos.$type.fijo") ?></td>
					<td>Celular: <?= Arr::path($data, "fic.data.datos.$type.celular") ?></td>
				</tr>
				<tr>
					<th>E-mail</th>
					<td>Institucional: <?= Arr::path($data, "fic.data.datos.$type.email") ?></td>
					<td>Alternativo: <?= Arr::path($data, "fic.data.datos.$type.emailalt") ?></td>
				</tr>
				<tr>
					<th>Fecha y firma</th>
					<td class="text-center">
						<div></div><div></div>
						__________________________
						<div></div>
					</td>
					<td class="text-center">
						<div></div><div></div>
						<?= Arr::path($data, "fic.data.datos.$type.date_local") ?>
						<div></div>
					</td>
				</tr>
			</tbody>
		</table>
	</td></tr></tbody></table><div></div>
	
	<?php endforeach ?>
	
	<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado() AND $action != 'finish'): ?>
	<h3 class="text-center">(RESULTADO PARCIAL)</h3>
	<?php endif ?>
	
<style>
	.text-center {
		text-align: center;
	}
	.box-title {
		padding: 10px;
	}
	/*.table {
		border: 1px solid black;
	}*/
	th {
		font-weight: bold;
	}
	.col-xs-3 {
		float: left;
	}
	.col-xs-9 {
		float: right;
	}
	
</style>