<section class="content-header">
	<h5 class="text-center">
		<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
	</h5>
	<h4 class="text-center">
		Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
	</h4>
	<h5 class="text-center">
		<?= $objeto['even_nombre'] ?>
	</h5>

	<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado()/* OR $action != 'finish'*/): ?>
	<h3 class="text-center">(RESULTADO PARCIAL)</h3>
	<?php endif ?>
	
</section>

<table border="1" class="table">
	<thead>
		<tr class="success" nobr="true">
			<th class="text-center tobjetivo">O.E.</th>
			<th class="text-center thindicadores" >Indicadores</th>
			<th class="text-center thmedios">Medios de Verificación</th>
		</tr>
	</thead>
	<tbody>
		<?php // Dimensiones (objetivos) ?>
		<?php $oe = 0 ?>
		<?php $re = 0 ?>
		<?php foreach ($dimensiones as $val1 => $row1): ?>
			
			<?php if (Arr::path($data, "dimensiones|{$row1['dime_codigo']}|hide", NULL, '|')) continue ?>
			<?php $nFactores = Arr::path($data, "dimensiones|{$row1['dime_codigo']}|nFactores", count($row1['data']), '|') ?>
			
			<tr>
				<th rowspan="<?= $nFactores ?>" class="tobjetivo">
					<p>O.E.<?= ++$oe ?></p>
				</th>
				
				<?php
					//debug2 ($data);
					//end($row1['data']);
					//$last1 = key($row1['data']);
					foreach ($row1['data'] as $val2 => $row2)
						if ( ! Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|'))
							$last1 = $val2;
				?>
				
			<?php // Factores (resultados) ?>
			<?php foreach ($row1['data'] as $val2 => $row2): ?>
				
				<?php if (Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|')) continue ?>
				
				<th colspan="2" class="tladoderecho">
					<table >
						<thead>
							<tr>
								<th class="tresultado">
									<strong>R.<?= ++$re ?></strong>
									<?= Arr::path($data, "factores|{$row2['fact_codigo']}|resultado", NULL, '|') ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td >
								<?php // Estandares  ?>
									<table border="1" class="table">
										<tbody>
										<?php foreach (Arr::path($data3, "indicadores|{$row2['fact_codigo']}", 
											array(0 => NULL), "|") as $i => $indicador): ?>
											<tr data-id="<?= $i ?>" >
												<td class="tindicadores">
													<p><?= Arr::path($indicador, 'indicador'); ?></p>
												</td>
												<td class="tmedios">
													<table class="">
														<tbody>
														<?php foreach (Arr::path($indicador, 'medios',  
															array(0 => NULL)) as $j => $estandar): ?>
															<tr data-id="<?= $j ?>" >
																<td>
																<?php /*foreach ($factores_medios as $key => $value): ?>
																	<?php foreach ($value as $nro => $medio): ?>
																		<?php if ($nro == Arr::path($estandar, 'medio') && Arr::path($indicador, 'indicador') ) echo "<p>".$medio."</p>" ?>
																	<?php endforeach ?>
																<?php endforeach*/ ?>
																	<?php foreach (Arr::path($medios, $row2['fact_codigo'], []) as $medio): ?>
																		<?php if ($medio['conc_id'] == Arr::path($estandar, 'medio')): ?>
																		<p class="form-control-static"><?= "Estandar {$medio['esta_codigo']} - {$medio['conc_codigo']}. {$medio['conc_nombre']}" ?></p>
																		<?php endif ?>
																	<?php endforeach ?>

																	<?php if (Arr::path($estandar, 'medio') == 999): ?>
																	<p class="form-control-static">
																		Otro: <?= Arr::path($estandar, 'medio_nombre'); ?>
																	</p>
																	<?php endif ?>
																</td>
															</tr>
														<?php endforeach ?>
														</tbody>
													</table>
												</td>
											</tr>
										<?php endforeach ?>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</th>
			</tr>

		<?php if ($val2 != $last1): ?> <tr> <?php endif ?>

		<?php endforeach ?>

	<?php endforeach ?>

	</tbody>
</table>

<?= Theme_View::factory('fichas/pdf/firmas', ['data' => $data3]) ?>

<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado() OR $action != 'finish'): ?>
<h3 class="text-center">(RESULTADO PARCIAL)</h3>
<?php endif ?>

<style>
	.success {
		background-color: #ddd;
	}
	.text-center {
		text-align: center;
	}
	.table {
		/*border: 1px solid black;*/
		padding: 5px;
	}
	.tobjetivo {
		width: 6%;
	}
	.tresultado {
		font-weight:normal;
		text-align: justify;
	}
	.tladoderecho {
		width: 94%;
	}
	.tindicadores{
		width: 30%;
		font-weight:normal;
		text-align: justify;
	}
	.thindicadores{
		width: 28.2%;
	}
	.tmedios {
		width: 70%;
		font-weight:normal;
	}
	.thmedios {
		width: 65.8%;
	}
	.table-centered > thead > tr > th {
		text-align: center;
	}
	.table-centered > tbody > tr > td {
		padding: 0;
	}
</style>
