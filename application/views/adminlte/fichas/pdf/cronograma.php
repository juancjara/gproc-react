<style>
	.success {
		background-color: #ddd;
	}
	.text-center {
		text-align: center;
	}
	.text-right {
		text-align: right;
	}
	.th-act {
		width: 10%;
	}
	.full-width
	{
		width:100%;
	}
	.bg-cro{
		background-color:#ccc;
	}
</style>
<aside class="right-side">
	<section class="content-header">
		<h5 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h4 class="text-center">
			Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
		</h4>
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
		
		<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado()/* OR $action != 'finish'*/): ?>
		<h3 class="text-center">(RESULTADO PARCIAL)</h3>
		<?php endif ?>
		
	</section>
	
	<section class="content">
		<div class="crono_wrapper">
			<table border="1" class="table table-bordered crono_table full-width" >
				<thead>
					<tr class="success text-center">
						<th style="width: 8%;" title="Asociar cada objetivo específico con una DIMENSIÓN de la matriz de calidad">
							Act</th>
						<th style="width: 8%;" title="Cada resultado debe estar asociado a un FACTOR de la matriz de calidad">
							Sub</th>
						
						<?php for ($i = 1; $i <= $context['quincenas']; $i++): ?>
						<th style="width: <?= 84 / $context['quincenas'] ?>%;">
							<?= 'Q' . $i ?>
						</th>
						<?php endfor ?>
						
					</tr>
					
				</thead>
				<tbody><?php //debug($actividades) ?>
					<?php $r = 1 ?>
					<?php foreach ($act_grouped as $actividades): ?>
						
						<?php foreach ($actividades as $a => $act): ?>
							
							<?php if ($act['subactividades']): ?>
								<?php foreach ($act['subactividades'] as $i => $val): ?>
									<tr>
										<?php if ($i == 0): ?>
											<td style="width: 8%;"
												rowspan="<?= count($act['subactividades']) ?>">
												<?= "A.{$r}.".($a + 1) ?><br>
												<?php //= htmlentities($act['data']['miactividad']) ?>
											</td>
										<?php endif ?>

										<td style="width: 8%;">
											<?= "Sub.{$r}.".($a + 1).".".($i + 1) ?><br />
											<?php //= htmlentities($val['data']['descripcion']) ?>
										</td>

										<?php for ($c = 0; $c < $context['quincenas']; $c++): ?>
											<?php $rango = 
												$val['cronograma'][0]['data']['duration'] + 
												$val['cronograma'][0]['data']['start'];
												
												$is_active = ($val['cronograma'][0]['data']['start'] <= $c AND $c < $rango);
											?>
											<td style="width: <?= 84 / $context['quincenas'] ?>%;"
												class="<?= $is_active ? 'bg-cro' : '' ?>"></td>
										<?php endfor ?>
									</tr>
								<?php endforeach ?>
							<?php else: ?>
								
								<tr>
									<td>
										<?= "A.{$r}.".($a + 1) ?><br>
										<?= htmlentities($act['data']['miactividad']) ?>
									</td>
									<td>Sin subactividades</td>
									<td colspan="<?= $context['quincenas'] ?>"></td>
								</tr>
								
							<?php endif ?>

						<?php endforeach ?>
						
						<?php $r++ ?>
					<?php endforeach ?>
				</tbody>
			</table>
			
			<h5 class="text-left">
				Nombre y firma del Presidente del Comité de Calidad
			</h5>
			<table border="1" class="table table-bordered full-width" cellpadding="5">
				<tbody>
					<tr>
						<th>Apellidos y Nombres</th>
						<th colspan="2"><?= Arr::path($data, 'fic.data.firmas.com.nombre') ?></th>
					</tr>
					<tr>
						<td>Cargo</td>
						<td colspan="2"><?= Arr::path($data, 'fic.data.firmas.com.cargo') ?></td>
					</tr>
					<tr>
						<td>Fecha y Firma</td>
						<td><p class="text-center">______________________</p></td>
						<td><?= Arr::path($data, 'fic.data.firmas.com.date_local') ?></td>
					</tr>
				</tbody>
			</table>
		
			<h5 class="text-left">
				Nombre y firma del Director de la Escuela Universitaria o Director General del Instituto
			</h5>
			<table border="1" class="table table-bordered full-width" cellpadding="5">
				<tbody>
					<tr>
						<th>Apellidos y Nombres</th>
						<th colspan="2"><?= Arr::path($data, 'fic.data.firmas.dir.nombre') ?></th>
					</tr>
					<tr>
						<td>Cargo</td>
						<td colspan="2"><?= Arr::path($data, 'fic.data.firmas.dir.cargo') ?></td>
					</tr>
					<tr>
						<td>Fecha y Firma</td>
						<td><p class="text-center">______________________</p></td>
						<td><?= Arr::path($data, 'fic.data.firmas.dir.date_local') ?></td>
					</tr>
				</tbody>
			</table>
		</div>

	</section>
</aside>

