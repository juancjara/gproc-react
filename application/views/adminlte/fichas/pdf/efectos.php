<style>
	.success {
		background-color: #ddd;
	}
	.text-center {
		text-align: center;
	}
	.table {
		padding: 5px;
	}
	.tobjetivo {
		width: 6%;
	}
	.tresultado {
		width: 6%;
	}
	.tladoderecho {
		width: 88%;
	}
	.tactividad{
		width: 50%;
		text-align: justify;
	}
	.tefecto{
		width: 50%;
		text-align: justify;
	}
	.thactividad{
		width: 44%;
	}
	.thefecto{
		width: 44%;
	}
	.table-centered > thead > tr > th {
		text-align: center;
	}
	.table-centered > tbody > tr > td {
		padding: 0;
	}
</style>

<section class="content-header">
	<h5 class="text-center">
		<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
	</h5>
	<h4 class="text-center">
		Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
	</h4>
	<h5 class="text-center">
		<?= $objeto['even_nombre'] ?>
	</h5>

	<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado()/* OR $action != 'finish'*/): ?>
	<h3 class="text-center">(RESULTADO PARCIAL)</h3>
	<?php endif ?>
	
</section>

<table style="width:100%" border="1" class="table">
	<thead>
		<tr class="text-center success" >
			<th style="width:20%">O.E.</th>
			<th style="width:20%" >R.</th>
			<th	style="width:60%" class="text-center" >Efectos Esperados</th>
		</tr>
	</thead>
	<tbody>
		<?php $d = $r = 1 ?>
		<?php foreach ($dimensiones as $dim): ?>

			<?php foreach ($dim['resultados'] as $i => $res): ?>
				<tr>
					<?php if ($i == 0): ?>
						<td style="width:20%" rowspan="<?= count($dim['resultados']) ?>" >
							<br><?= 'O.E.'.$d . '<br>'
                                    . Arr::path($dim['data'], 'dime_codigo', '') . ' ' .
                                    Arr::path($dim['data'], 'objetivo', '') ?>
						</td>
					<?php endif ?>	
					<td style="width:20%">
						<br><?= 'R.' . $r . '<br>' . Arr::path($res['data'], 'fact_codigo', '') . ' '.
                                Arr::path($res['data'], 'resultado', '') ?>
						<br><br>
					</td>
					<?php foreach ($res['efectos'] as $efe): ?>
					<td style="width:60%">
						<?= Arr::path($efe, 'data.descripcion') ?>
					</td>
					<?php endforeach ?>
				</tr>	
			<?php endforeach ?>
				
		<?php endforeach ?>	
	</tbody>
</table>

<h5 class="text-left">
	Nombre y firma del Presidente del Comité de Calidad
</h5>
<table style="width:100%" border="1" class="table table-bordered" cellpadding="5">
	<tbody>
		<tr>
			<th>Apellidos y Nombres</th>
			<th colspan="2"><?= Arr::path($data, 'fic.data.firmas.com.nombre') ?></th>
		</tr>
		<tr>
			<td>Cargo</td>
			<td colspan="2"><?= Arr::path($data, 'fic.data.firmas.com.cargo') ?></td>
		</tr>
		<tr>
			<td>Fecha y Firma</td>
			<td><p class="text-center">______________________</p></td>
			<td><?= Arr::path($data, 'fic.data.firmas.com.date_local') ?></td>
		</tr>
	</tbody>
</table>
		
<h5 class="text-left">
	Nombre y firma del Director de la Escuela Universitaria o Director General del Instituto
</h5>
<table style="width:100%" border="1" class="table table-bordered" cellpadding="5">
	<tbody>
		<tr>
			<th>Apellidos y Nombres</th>
			<th colspan="2"><?= Arr::path($data, 'fic.data.firmas.dir.nombre') ?></th>
		</tr>
		<tr>
			<td>Cargo</td>
			<td colspan="2"><?= Arr::path($data, 'fic.data.firmas.dir.cargo') ?></td>
		</tr>
		<tr>
			<td>Fecha y Firma</td>
			<td><p class="text-center">______________________</p></td>
			<td><?= Arr::path($data, 'fic.data.firmas.dir.date_local') ?></td>
		</tr>
	</tbody>
</table>
