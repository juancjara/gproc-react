<style type="text/css">
	.align-top td{
		vertical-align:top;
	}	
</style>
<aside class="right-side">
	
	<section class="content-header">
		<h5 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h4 class="text-center">
			Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
		</h4>
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
		
		<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado()/* OR $action != 'finish'*/): ?>
		<h3 class="text-center">(RESULTADO PARCIAL)</h3>
		<?php endif ?>
		
	</section>
	
	<section class="content">
		<?php //debug($totales) ?>
		<div class="pull-right table-totales" id="table-total-wrapper">
			<table class="table" border="1" cellpadding="2">
				<thead>
					<tr class="success">
						<th rowspan="2">Participación</th>
						<?php $vladi = ['R' => 4, 'S' => 2, 'B' => 3];  ?>
						<?php foreach ($context['catalogo_titles'] as $index => $titles): ?>
							<th colspan="<?= $vladi[$titles['id']];  ?>">
							<?= $titles['name'] ?>
							</th>
						<?php endforeach ?>
						
						<th colspan="2">Total Plan</th>
					</tr>	

					<tr class="success">
						<?php foreach ($context['catalogo_titles'] as $titles): ?>
							
							<?php foreach ($context['catalogo_subtitles'] as $subtitles): ?>
								<?php if ($titles['id'] == $subtitles['parent_id']): ?>
									<th><?= $subtitles['name'] ?></th>
								<?php endif ?>
							<?php endforeach ?>	
							
							<th>Total <?= $titles['name'] ?></th>
							
						<?php endforeach ?>
						
						<th>Total</th>
						<th>%</th>
					</tr>
				</thead>
				
				<tbody>
					<?php foreach (['fec', 'propio', 'plan'] as $fin): ?>
						<tr style="font-size:9px" class="text-center">
							<th>Total <?= strtoupper($fin) ?></th>
							
							<?php foreach ($context['catalogo_titles'] as $titles): ?>
								
								<?php foreach ($context['catalogo_subtitles'] as $subtitles): ?>
									<?php if ($titles['id'] == $subtitles['parent_id']): ?>
										<td>
											<?= numberformat($totales[$fin][$subtitles['id']], 2, '.', ',') ?> <br>
											<?php if ($fin == 'fec'): ?>
												<small class="text-primary">
													(<?= $totales['row_per_'.$fin][$titles['id']] ?> %)
												</small>
											<?php endif ?>
										</td>
									<?php endif ?>
								<?php endforeach ?>
								
								<th>
									<?= numberformat($totales[$fin][$titles['id']], 2, '.', ',') ?><br>
									<?php if ($fin == 'fec'): ?> 
										<small class="text-primary">
											(<?= $totales['row_per_'.$fin][$titles['id']] ?> %)
										</small>
									<?php endif ?>
								</th>
							<?php endforeach ?>
							
							<th><?= numberformat($totales[$fin]['T'], 2, '.', ',') ?></th>
							<th><?= $totales['per_' . $fin] ?></th>
						</tr>
					<?php endforeach ?>
						<tr>
							<td colspan='6' style="width:50%"><strong>Tipo de Cambio: 3.46</strong></td>
							<td colspan='6' style="width:50%" class="text-right"><strong><?= $text_otros_fec ?></strong></td>
						</tr>
				</tbody>
			</table>
		</div>
		<br>
		<table border="1" class="table table-bordered" cellpadding="5">
			<thead>

				<tr class="success text-center">
					<th style="width:15%" title="Asociar cada objetivo específico con una DIMENSIÓN de la matriz de calidad">
						Objetivo Específico (O.E.)</th>
					<th style="width:15%" title="Cada resultado debe estar asociado a un FACTOR de la matriz de calidad">
						Resultados o Productos (R.)</th>
					<th style="width:70%">
						Actividades (A.)
					</th>
				</tr>
			</thead>	
			<tbody>
				<?php $o = $r = 1 ?>
				<?php foreach ($objetivos as $objetivo): ?>

					<?php foreach ($objetivo['resultados'] as $i => $resultado):?>
					<tr>
						<?php if ($i == 0): ?><?php //debug($objetivo)?>
							<td rowspan="<?= count($objetivo['resultados']) ?>" style="width:15%">
								<br><?= 'O.E.'.$o . '<br>' .  
									Arr::path($objetivo['data'], 'dime_codigo', '') . ' ' . 
									Arr::path($objetivo['data'], 'objetivo', '') ?></td>
						<?php endif ?>
						
						<td style="width:15%"><?= 'R.'.$r . '<br>'.$resultado['data']['fact_codigo'] . ' ' .  $resultado['data']['resultado'] ?></td>

						<td style="width:70%">
							<?php foreach ($resultado['actividades'] as $i => $actividad): ?>
							
							<strong><?= 'A.'.$r.'. '.++$i ?></strong><br>
								<?= htmlentities($actividad['data']['miactividad']) ?><br>
							
								<?php foreach ($actividad['subactividades'] as $s => $subactividad): ?>

								<table border="1" class="table table-bordered align-top" cellpadding="5">
									<thead>
										<tr>	
											<th colspan='5' style="text-align:left;"><?= 'Sub.'.$r.'.'.$i.'.'.++$s ?></th>	
										</tr>
											
									</thead>
									<tbody>
										<tr>
											<td style="width:35%">
												<strong>Descripción</strong><br>
												<p><?= htmlentities($subactividad['data']['descripcion'])  ?></p>
											</td>
											<td style="width:16.25%">
												<strong>Catálogo</strong><br>
												<?php foreach ($subactividad['data']['niveles'] as $nivel): ?>
													<?= $nivel ? $catalogo_dic[$nivel]['name'] : '--' ?><br>
												<?php endforeach ?>
												
												<?php if (end($subactividad['data']['niveles']) == 'otro'): ?>
													<?= $subactividad['data']['otro'] ?: '--' ?>
												<?php endif ?>	
											</td>
											<td style="width:16.25%"><?php //debug($subactividad) ?>
												<?php if ($subactividad['data']['todo_seleccionado']): ?>
													<?php if ($subactividad['data']['niveles'][0] == Model_AcreditadoEtapaFicha::$tipo_catalogo[Model_AcreditadoEtapaFicha::RRHH]): ?>
														<strong>Pasajes</strong><br><?= $subactividad['data']['pasajes'] ?: '--'  ?><br>
														<strong>Viáticos</strong><br><?= $subactividad['data']['viaticos'] ?: '--'  ?><br>
														<strong>N° Días</strong><br><?= $subactividad['data']['num_dias'] ?: '--' ?><br>
														
														<?php if($subactividad['data']['niveles'][1] == Model_AcreditadoEtapaFicha::$tipo_catalogo[Model_AcreditadoEtapaFicha::RRHH].'C'): ?>
															<strong>Inscripción</strong><br><?= $subactividad['data']['inscripcion'] ?: '--'  ?>
														<?php endif ?>
													<?php endif ?>
													<?php $pv = $subactividad['data']['niveles'][1] == Model_AcreditadoEtapaFicha::$tipo_catalogo[Model_AcreditadoEtapaFicha::SERVICIOS].'P'; ?>
													<?php if ($subactividad['data']['niveles'][0] == Model_AcreditadoEtapaFicha::$tipo_catalogo[Model_AcreditadoEtapaFicha::SERVICIOS]): ?>
														<?php if($pv): ?>
															<strong>Pasajes: </strong><br><?= $subactividad['data']['pasajes'] ?: '--'  ?><br>
														<?php endif ?>	
															<strong><?= $pv ? 'Viáticos' : 'Costo' ?></strong><br><?= $subactividad['data']['costo'] ?: '--'  ?><br>
														<?php if($pv): ?>
															<strong>N° Días</strong><br><?= $subactividad['data']['num_dias'] ?: '--'  ?>
														<?php endif ?>
													<?php endif ?>
													
													<?php if ($subactividad['data']['niveles'][0] == Model_AcreditadoEtapaFicha::$tipo_catalogo[Model_AcreditadoEtapaFicha::BIENES]): ?>
														<strong>Costo</strong><br><?= $subactividad['data']['costo'] ?: '--'  ?>
													<?php endif ?>
												<?php endif ?>		
											</td>
											<td style="width:16.25%">
												<?php if ($subactividad['data']['todo_seleccionado']): ?>
													<strong>CU</strong><br><?= $subactividad['data']['costo_unitario'] ?: '--' ?><br>
													<strong>Cantidad</strong><br><?= $subactividad['data']['cantidad'] ?: '--' ?><br>
													<strong>Unidad</strong><br><?= $subactividad['data']['unidad'] ?: '--' ?><br>
													<strong>TOTAL</strong><br><?= $subactividad['data']['total'] ?: '--' ?><br>
												<?php endif ?>	
											</td>
											<td style="width:16.25%">
												<?php if ($subactividad['data']['todo_seleccionado']): ?>
												<strong>Tipo de Financiamiento</strong><br><?= strtoupper($subactividad['data']['financiamiento']) ?: '--' ?><br>
												<?php endif ?>	
											</td>

										</tr>
									</tbody>
								</table>
								
								<?php endforeach ?>
								
								<table border="1" class="table table-bordered" cellpadding="5">

									<thead>
										<tr class="success text-center">
											<?php foreach ($context['catalogo_titles'] as $title): ?>
												<th class="success text-center"><?= $title['name'] ?></th>
											<?php endforeach ?>
											<th>Total <br> Actividad</th>
											<th>Responsable de <br> Actividad</th>	
										</tr>
									</thead>

									<tbody>	
										<tr>
											<td><?= $actividad['detalleTotales']['plan']['R'] ?></td>
											<td><?= $actividad['detalleTotales']['plan']['S'] ?></td>
											<td><?= $actividad['detalleTotales']['plan']['B'] ?></td>
											<td><?= $actividad['detalleTotales']['plan']['T'] ?></td>
											<td><?= $actividad['data']['actor_cargo'] ?></td>
										</tr>
									</tbody>

								</table>
							<?php endforeach ?>
						</td>
					</tr>
					<?php $r++ ?>
					<?php endforeach ?>
					<?php $o++ ?>
				<?php endforeach ?>		
			</tbody>
		</table>
		
		<h5 class="text-left">
			Nombre y firma del Presidente del Comité de Calidad
		</h5>
		<table border="1" class="table table-bordered" cellpadding="5">
			<tbody>
				<tr>
					<th>Apellidos y Nombres</th>
					<th colspan="2"><?= Arr::path($data, 'fic.data.firmas.com.nombre') ?></th>
				</tr>
				<tr>
					<td>Cargo</td>
					<td colspan="2"><?= Arr::path($data, 'fic.data.firmas.com.cargo') ?></td>
				</tr>
				<tr>
					<td>Fecha y Firma</td>
					<td><p class="text-center">______________________</p></td>
					<td><?= Arr::path($data, 'fic.data.firmas.com.date_local') ?></td>
				</tr>
			</tbody>
		</table>
		
		<h5 class="text-left">
			Nombre y firma del Director de la Escuela Universitaria o Director General del Instituto
		</h5>
		<table border="1" class="table table-bordered" cellpadding="5">
			<tbody>
				<tr>
					<th>Apellidos y Nombres</th>
					<th colspan="2"><?= Arr::path($data, 'fic.data.firmas.dir.nombre') ?></th>
				</tr>
				<tr>
					<td>Cargo</td>
					<td colspan="2"><?= Arr::path($data, 'fic.data.firmas.dir.cargo') ?></td>
				</tr>
				<tr>
					<td>Fecha y Firma</td>
					<td><p class="text-center">______________________</p></td>
					<td><?= Arr::path($data, 'fic.data.firmas.dir.date_local') ?></td>
				</tr>
			</tbody>
		</table>
		
	</section>
</aside>

<style>
	.success {
		background-color: #ddd;
	}
	table {
		width: 100%;
	}
	.tobjetivo {
		width: 20%;
	}
	.tresultado {
		width: 20%;
	}
	.tladoderecho {
		width: 60%;
	}
	.text-center {
		text-align: center;
	}
	.text-right {
		text-align: right;
	}
	.td-total {
		width: 14%;
	}
	.tactores_inner {
		width: 20%;
	}
	.tejecucion_inner {
		width: 10%;
	}
	.small {
		font-size: 8px;
	}
</style>
