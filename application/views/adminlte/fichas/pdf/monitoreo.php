<style>
	.thindicadores{
		width: 20%;
	}
	.thmedios {
		width: 65%;
		font-weight:normal;
	}
	.tobjetivo {
		width: 15%;
	}
	table{
		width:100%;
	}
	.text-center {
		text-align: center;
	}
</style>
<section class="content-header">
	<h5 class="text-center">
		<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
	</h5>
	<h4 class="text-center">
		Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
	</h4>
	<h5 class="text-center">
		<?= $objeto['even_nombre'] ?>
	</h5>

	<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado()/* OR $action != 'finish'*/): ?>
	<h3 class="text-center">(RESULTADO PARCIAL)</h3>
	<?php endif ?>
	
</section>
<?php //debug($dimensiones) ?>
<table border="1" class="table">
	<thead>
		<tr class="success" nobr="true">
			<th class="text-center tobjetivo">O.E.</th>
			<th class="text-center thindicadores" >Indicadores</th>
			<th class="text-center thmedios">Medios de Verificación</th>
		</tr>
	</thead>
	<tbody>
		<?php // Dimensiones (objetivos) ?>
		<?php $oe = 0 ?>
		<?php $re = 0 ?>
		<?php $d = $r = 1 ?>
		<?php foreach ($dimensiones as $dim): ?>
			
			<?php foreach ($dim['resultados'] as $i => $res): ?>
				<tr>
					<?php if ($i == 0): ?>
						<td rowspan="<?= count($dim['resultados']) ?>" class="tobjetivo">
							<br><?= 'O.E.'.$d . '<br>'
                                    . Arr::path($dim['data'], 'dime_codigo', '') . ' ' .
                                    Arr::path($dim['data'], 'objetivo', '') ?>
						</td>
					<?php endif ?>
					<td colspan="2" style="width:85%">
						<br><?= 'R.' . $r . '<br>' . Arr::path($res['data'], 'fact_codigo', '') . ' '.
                            Arr::path($res['data'], 'resultado', '') ?>
						<br><br>
						<?php foreach ($res['indicadores'] as $ind): ?>
							<table border="1" class="table"> 
								<tbody>
									<tr>
										<td class="text-center" style="width:22%"><?= $ind['data']['descripcion'] ?: '--' ?></td>
										<td style="width:78%">
											<strong>Medios de Verificación:</strong><br>
											<?php foreach ($ind['data']['medios'] as $med): ?>
												<?php if($med['conc_id'] == 'otro'): ?>
													<?= 'Otro: '.$med['otro'] ?>
												<?php elseif($med['conc_id'] AND array_key_exists($med['conc_id'], $medios)): ?>
													<?= $medios[$med['conc_id']]['display']  ?><br>
												<?php endif ?><br>
											<?php endforeach ?>
										</td>
									</tr>
								</tbody>
							</table><br>
						<?php endforeach ?>	
					</td>
				</tr>
				<?php $r++ ?>
			<?php endforeach ?>
				<?php $d++ ?>
		<?php endforeach ?>	
	</tbody>
</table>
<h5 class="text-left">
	Nombre y firma del Presidente del Comité de Calidad
</h5>
<table border="1" class="table table-bordered" cellpadding="5">
	<tbody>
		<tr>
			<th>Apellidos y Nombres</th>
			<th colspan="2"><?= Arr::path($data, 'fic.data.firmas.com.nombre') ?></th>
		</tr>
		<tr>
			<td>Cargo</td>
			<td colspan="2"><?= Arr::path($data, 'fic.data.firmas.com.cargo') ?></td>
		</tr>
		<tr>
			<td>Fecha y Firma</td>
			<td><p class="text-center">______________________</p></td>
			<td><?= Arr::path($data, 'fic.data.firmas.com.date_local') ?></td>
		</tr>
	</tbody>
</table>
		
<h5 class="text-left">
	Nombre y firma del Director de la Escuela Universitaria o Director General del Instituto
</h5>
<table border="1" class="table table-bordered" cellpadding="5">
	<tbody>
		<tr>
			<th>Apellidos y Nombres</th>
			<th colspan="2"><?= Arr::path($data, 'fic.data.firmas.dir.nombre') ?></th>
		</tr>
		<tr>
			<td>Cargo</td>
			<td colspan="2"><?= Arr::path($data, 'fic.data.firmas.dir.cargo') ?></td>
		</tr>
		<tr>
			<td>Fecha y Firma</td>
			<td><p class="text-center">______________________</p></td>
			<td><?= Arr::path($data, 'fic.data.firmas.dir.date_local') ?></td>
		</tr>
	</tbody>
</table>		

<style>
	.success {
		background-color: #ddd;
	}
	.table {
		/*border: 1px solid black;*/
		padding: 5px;
	}
	.tresultado {
		font-weight:normal;
		text-align: justify;
	}
	.tladoderecho {
		width: 94%;
	}
	.table-centered > thead > tr > th {
		text-align: center;
	}
	.table-centered > tbody > tr > td {
		padding: 0;
	}
</style>
