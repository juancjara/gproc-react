<aside class="right-side">
	
	<section class="content-header">
		<h5 class="text-center">
			<?= "Comité de Calidad de ". $objeto['subtitle'] ?>
		</h5>
		<h4 class="text-center">
			Plan de Mejora - <?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>
		</h4>
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
		
		<?php if ( ! $oAcreditadoEtapaFicha->is_finalizado()/* OR $action != 'finish'*/): ?>
		<h3 class="text-center">(RESULTADO PARCIAL)</h3>
		<?php endif ?>
		
	</section>
	
	<section class="content">
		
		<div class="pull-right table-totales" id="table-total-wrapper">
			<table class="table" border="1" cellpadding="2">
				<thead>
					<tr class="success">
						<th></th>
						<?php foreach ($grupos_header as $grupo): ?>
						<th class="text-right"><?= ($grupo['grup_id'] == 'total') ? 'Total' : $grupo['grup_short'] ?></th>
						<?php endforeach ?>
						<th class="text-right">%</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach(['fec', 'propio', 'plan'] as $tipo): ?>
					<tr>
						<th>Total <?= strtoupper($tipo) ?></th>
						<?php foreach ($grupos_header as $grupo): ?>
						<td class="text-right">
							<?= Arr::path($data2, "totales.$tipo.{$grupo['grup_id']}") ?></td>
						<?php endforeach ?>
						<td class="text-right success"><?= Arr::path($data2, "porcentajes.$tipo") ?></td>
					</tr>
					<?php endforeach ?>
					<!--tr>
						<td colspan="9" class="text-right">
							(*) Corresponde a la Actividad de supervisión y monitoreo (1.5% del financiamiento FEC)</td>
					</tr-->
				</tbody>
			</table>
		</div>
		<br>

		<table border="1" class="table table-bordered" cellpadding="5">
			<thead>
				<tr class="success" nobr="true">
					<th>O.E.</th>
					<th>R.</th>
					
					<th>A.</th>
					<th>A. Desc</th>
					
					<th>Subactividad</th>
					<th>S. Desc</th>
					
					<th>Grupo</th>
					<th>Gr. Desc</th>
					
					<th>Gasto</th>
					<th>Ga. Desc</th>
					
					<th>Tipo</th>
					<th>T. Desc</th>
					
					<th>Item</th>
					<th>I. Desc</th>
					
					<th>Unidad de medida</th>
					<th>Costo unitario</th>
					<th>Cantidad</th>
					<th>Subtotal</th>
					<th>Financiamiento</th>
				</tr>
			</thead>
			<tbody>
				<?php // Dimensiones (objetivos) ?>
				<?php $oe = 0; ?>
				<?php $re = 0; ?>
				<?php foreach ($dimensiones as $val1 => $row1): ?>
					
					<?php if (Arr::path($data, "dimensiones|{$row1['dime_codigo']}|hide", NULL, '|')) continue ?>
					
					<?php ++$oe ?>
						
					<?php // Factores (resultados) ?>
					<?php foreach ($row1['data'] as $val2 => $row2): ?>
						
						<?php if (Arr::path($data, "factores|{$row2['fact_codigo']}|hide", NULL, '|')) continue ?>
						
						<?php ++$re ?>

						<?php // Actividades  ?>
							
							<?php foreach (Arr::path($data2, "actividades|{$row2['fact_codigo']}", array(), "|") as $i => $actividad): ?>
							
									<?php foreach (Arr::path($actividad, 'subactividades', array()) as $j => $subactividad): ?>
									
									<tr>
										<td>O.E.<?= $oe ?></td>
										<td>R.<?= $re ?></td>
										
										<td>A.<?= $re ?>.<?= $i + 1 ?></td>
										<td><?= Arr::path($data, "actividades|{$row2['fact_codigo']}|{$i}|miactividad", NULL, '|') ?></td>
										
										<td><?= ($j + 1) ?>.</td>
										<td><?= Arr::path($subactividad, 'descripcion') ?></td>
										
										<td><?= Arr::path($subactividad, 'grupo') ?></td>
										<td><?= Arr::path(TreeArray::factory($aGrupo, ['grup_id'])->get(), 
											Arr::path($subactividad, 'grupo').'.grup_short') ?></td>
										
										<td><?= Arr::path($subactividad, 'gasto') ?></td>
										<td><span><?= Arr::path($gastos, Arr::path($subactividad, 'gasto')) ?></span>
											<em><?= Arr::path($subactividad, 'gastoOtro') ?></em></td>
										
										<?php if (Arr::path($subactividad, 'grupo') != 1): ?>
											<td><?= Arr::path($subactividad, 'tipo') ?></td>
											<td><span><?= Arr::path($tipos, Arr::path($subactividad, 'tipo')) ?></span>
												<em><?= Arr::path($subactividad, 'tipoOtro') ?></em></td>
										<?php else: ?>
											<td>
												<strong>Costos:</strong><br>
												<?php foreach (Arr::path($subactividad, 'costosRRHH', array()) as $key => $val): ?>
													<em><?= Arr::path($tipos, $key) ?>: </em>
													<span><?= $val ?></span><br>
												<?php endforeach ?>
											</td>
											<td></td>
										<?php endif ?>
										
										<?php if (Arr::path($subactividad, 'grupo') != 1): ?>
											<td><?= Arr::path($subactividad, 'item') ?></td>
											<td><span><?= Arr::path($items, Arr::path($subactividad, 'item')) ?></span>
												<em><?= Arr::path($subactividad, 'itemOtro') ?></em></td>
										<?php else: ?>
											<td>
												<strong>Cantidades:</strong><br>
												<?php foreach (Arr::path($subactividad, 'cantidadesRRHH', array()) as $key => $val): ?>
													<em><?= Arr::path($tipos, $key) ?>: </em>
													<span><?= $val ?></span><br>
												<?php endforeach ?>
											</td>
											<td></td>
										<?php endif ?>
										
										<td><?= Arr::path($subactividad, 'unidad') ?></td>
										<td><?= Arr::path($subactividad, 'costounitario') ?></td>
										<td><?= Arr::path($subactividad, 'cantidad') ?></td>
										<td><?= Arr::path($subactividad, 'subtotal') ?></td>
										<td><?= $financiamiento[Arr::path($subactividad, 'financiamiento', '')] ?></td>
									</tr>
									
									<?php endforeach ?>
									
									<!--tr>
										<td>summary</td>
									</tr-->
								<!--tfoot>
									<tr>
										<th colspan="9">
											<strong>TOTALES POR ACTIVIDAD</strong><br>
											<table border="1" cellpadding="0">
												<tbody>
													<tr class="success text-center">
														<?php foreach ($grupos as $grupo): ?>
														<td><?= $grupo['grup_short'] ?></td>
														<?php endforeach ?>
													</tr>
													<tr class="success text-center">
														<?php foreach ($grupos as $grupo): ?>
														<td><?= Arr::path($actividad, "totales.{$grupo['grup_id']}") ?></td>
														<?php endforeach ?>
													</tr>
												</tbody>
											</table>
										</th>
									</tr>
								</tfoot-->
							
							<?php endforeach ?>
							
					<?php endforeach ?>

				<?php endforeach ?>

			</tbody>
		</table>
		
	</section>
</aside>

<?php //die() ?>

<style>
	.success {
		background-color: #ddd;
	}
	.text-center {
		text-align: center;
	}
	.text-right {
		text-align: right;
	}
	table {
		width: 100%;
	}
	.table-actividad {
		margin-bottom: 20px;
	}
	.th-objetivo {
		width: 8%;
	}
	.th-resultado {
		width: 8%;
	}
	.th-actividades,
	.td-actividades {
		width: 84%;
	}
	
	.tr-subactividad {
		font-size: 9px;
	}
	
	.td-descripcion {
		width: 32%;
	}
	.td-gasto, 
	.td-tipo, 
	.td-item {
		width: 10%;
	}
	.td-unidad, 
	.td-costounitario, 
	.td-cantidad, 
	.td-subtotal{
		width: 8%;
	}
	.td-financia {
		width: 6%;
	}
	
</style>
