<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

$assets = array(
	'css' => array(
		'default' => array(
			'boostrap'			=> '/adminlte/css/bootstrap.min.css',
			'fa'				=> '/adminlte/css/font-awesome.min.css',
			'ionicons'			=> '/adminlte/css/ionicons.min.css',
			'fonts'				=> '/admin/css/fonts.css',
			'adminlte'			=> '/adminlte/css/AdminLTE.css',
			'datetimepicker'	=> '/admin/css/datetimepicker.min.css',
			'admin'				=> '/admin/css/admin.css',
		),
	),
	'js' => array(
		'default' => array(
			'jquery'			=> '/admin/js/jquery.min.js',
			//'draggable'			=> '/saes_lte/js/plugins/jquery-sortable.js',
			'jquery-ui'			=> '/gproc_lte/js/plugins/jquery-ui/jquery-ui.min.js',
			'bootstrap'			=> '/adminlte/js/bootstrap.min.js',
			'adminlte'			=> '/adminlte/js/AdminLTE/app.js',
			'datetimepicker'	=> '/admin/js/datetimepicker.min.js',
			'admin'				=> '/admin/js/admin.js',
		),
	),
);

$theme = Theme::instance()->assets($assets);

$theme->template = Theme_View::factory('template/layout', 
	array('body_class' => NULL));

$theme->template->messages = Theme_View::factory('template/messages');

$theme
	->css('page', 'gproc', '/gproc_lte/css/common.css')
	->js('page', 'gproc', '/gproc_lte/js/common.js');

$theme->buttons(array(
		'edit' => array(
			'data-title' => 'Editar',
			'btn_class' => 'btn btn-sm btn-flat btn-success',
			'icon_class' => 'fa fa-pencil',
		),
		'delete' => array(
			'data-title' => 'Eliminar',
			'btn_class' => 'btn btn-sm btn-flat btn-danger',
			'icon_class' => 'fa fa-trash-o',
		),
		'new' => array(
			'title' => 'Nuevo',
			'btn_class' => 'btn btn-flat btn-primary',
			'icon_class' => 'fa fa-plus',
		),
		'excel' => array(
			'title' => 'Excel',
			'btn_class' => 'btn btn-flat btn-default btn-excel',
			'icon_class' => 'fa fa-file-text',
		),
		'csv' => array(
			'title' => 'CSV',
			'btn_class' => 'btn btn-flat btn-default',
			'icon_class' => 'fa fa-file-text-o',
		),
		'pdf' => array(
			'title' => 'PDF',
			'btn_class' => 'btn btn-flat btn-default',
			'icon_class' => 'fa fa-download',
		),
		'print' => array(
			'title' => 'Imprimir',
			'btn_class' => 'btn btn-flat btn-default btn-print',
			'icon_class' => 'fa fa-print',
		),
		'cancel' => array(
			'title' => 'Cancelar',
			'btn_class' => 'btn btn-flat btn-default',
			'icon_class' => 'fa fa-ban',
		),
	));
