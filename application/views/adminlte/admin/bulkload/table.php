<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Carga masiva
			<small>Arbol de estándares</small>
		</h1>
		
		
		
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<h4 class="box-title">Subir archivo CSV</h4>
			</div>
			
			<div class="box-body">
				
				<!--div class="alert alert-info alert-dismissable">
					<i class="fa fa-info"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Importante!</b>
					__
				</div-->
				
				<form role="form" class="form-horizontal" method="post" enctype="multipart/form-data">
					
					<div class="form-group">
						<label for="type_catalogo" class="col-sm-2 control-label">Tipo Catálogo</label>
						<div class="col-sm-6">
							<select class="form-control" name="type_catalogo" id="type_catalogo">
								<option value="">--</option> 
								<?php foreach (Model_Catalogo::$tipo_catalogo as $i => $tipo): ?>
								<option value="<?= $i ?>"><?= $tipo ?></option>
								<?php endforeach ?>
							</select>
						</div>	
					</div>
					
					<div class="form-group ">
						<label for="fact_codigo" class="col-sm-2 control-label">Archivo .csv</label>
						<div class="col-sm-6">
							<input type="file" class="form-control " name="file" id="file" placeholder="File" required>
							<p class="help-block">
								El archivo debe contener cabeceras con campos según el tipo de institución:
							</p>
						</div>
					</div>
					
					<div class="form-group">
						<label for="cata-tipo" class="col-sm-2 control-label">Template Catálogo</label>
						<div class="col-sm-6">
							<a id="template-catalogo" target="_blank" href="<?= Model_Catalogo::FOLDER_TEMPLATE.Model_Catalogo::TEMPLATE_CATALOGO ?>"><img src="/media/adminlte/img/thumbs-excel.png" alt=""></a>
							<p class="help-block">
								<strong>Descargar</strong>
							</p>
						</div>
					</div>	

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-flat btn-primary">Subir</button>
							<a href="/admin" class="btn btn-flat btn-default">Volver</a>
						</div>
					</div>
				</form>
				
			</div>
		</div>

		
		
	</section><!-- /.content -->
</aside><!-- /.right-side -->
