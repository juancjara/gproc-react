<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Datos generales</h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form role="form" class="form-horizontal" method="post">
				<div class="box-body">
					<?php if ($form->errors): ?>
						<div class="row">
							<div class="col-sm-6 col-sm-offset-2">
								<div class="callout callout-danger fade in text-left" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
									<h4>Error!</h4>

									<?php foreach ($form->errors as $error): ?>
										<p><?= $error ?></p>
									<?php endforeach ?>

								</div>
							</div>
						</div>
					<?php endif ?>

					<?php foreach ($form->controls as $control): ?>
						<?= $control ?>
					<?php endforeach ?>

					<input type="hidden" name="referrer" value="<?= $form->back_url ?>" >


				</div><!-- /.box-body -->

				<div class="box-footer">
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-flat btn-primary">Guardar</button>
							<a href="<?= $form->back_url ?>" class="btn btn-flat btn-default">Cancelar</a>
						</div>
					</div>
				</div>
			</form>
		</div>
		
		
	</section><!-- /.content -->
</aside><!-- /.right-side -->
