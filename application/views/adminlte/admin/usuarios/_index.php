<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		<?php //debug($table); ?>
		<?= Breadcrumb::build() ?>
		
	</section>

	<div class="list-buttons">
		<a href="/admin/usuarios/new" class="btn btn-flat btn-primary">
			<i class="fa fa-plus"></i> Nuevo</a>
	</div>
	
	<!-- Main content -->
	<section class="content">

		<div class="row">
			<div class="col-sm-7">
				<?= $table->searches ?>
			</div>
			<div class="col-sm-5">
				<?= $table->filters ?>
			</div>
		</div>
		
		<div class="box">
			<div class="box-header pagination-top">
				<?= $table->pagination ?>
			</div><!-- /.box-header -->
			
			<div class="box-body box-responsive">
				
				<table class="table table-bordered table-hover _dataTable">
					<thead>
						<tr>
							<?php foreach ($table->thead as $th): ?>
								<?php echo $th ?>
							<?php endforeach ?>
							<th class="th-actions"></th>
						</tr>
					</thead>

					<tbody>
						<?php foreach ($table->tbody as $id => $row): ?>
							<tr>
								<?php foreach ($row as $td): ?>
									<?php echo $td ?>
								<?php endforeach ?>
								<td class="td-actions">
									<a href="/admin/usuarios/edit/<?= $id ?>" class="btn btn-sm btn-flat btn-success" data-title="Editar"><i class="fa fa-pencil"></i></a>
									<a href="/admin/usuarios/delete/<?= $id ?>" class="btn btn-sm btn-flat btn-danger" data-title="Eliminar"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div><!-- /.box-body -->
			
			<div class="box-footer pagination-bottom">
				<?= $table->pagination ?>
			</div>
			
		</div><!-- /.box -->

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>