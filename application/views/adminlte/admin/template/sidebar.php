<aside class="left-side sidebar-offcanvas">                
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<img src="/media/gproc_lte/img/logo.png" class="img-responsive img-logo">
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->

		<ul class="sidebar-menu">
			<li>
				<a href="/admin/usuarios">
					<i class="fa fa-dashboard"></i> 
					<span>Usuarios</span>
				</a>
			</li>
			<li>
				<a href="/admin/evento">
					<i class="fa fa-dashboard"></i> 
					<span>Eventos</span>
				</a>
			</li>
			<li>
				<a href="/admin/eventoEtapa">
					<i class="fa fa-dashboard"></i> 
					<span>Etapas</span>
				</a>
			</li>
			<li>
				<a href="/admin/eventoEtapaFicha">
					<i class="fa fa-dashboard"></i> 
					<span>Fichas</span>
				</a>
			</li>
			<li>
				<a href="/admin/eventoNivel">
					<i class="fa fa-dashboard"></i> 
					<span>Niveles</span>
				</a>
			</li>
			<li class="treeview">
				<a href="#">	
					<i class="fa fa-list-ul"></i>
					<span>Catálogo</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li><a href="/admin/Catalogo">Ver Catálogo</a></li>
					<li><a href="/admin/Bulkload">Cargar Catálogo</a></li>
				</ul>
			</li>
			<li>
				<a href="/evaluador/verEventos">
					<i class="fa fa-dashboard"></i> 
					<span>Ver Eventos</span>
				</a>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>
