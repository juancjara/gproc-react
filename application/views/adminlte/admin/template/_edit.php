<aside class="right-side">                
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $title ?>
            <small><?= $subtitle ?></small>
        </h1>
        
        <?= Breadcrumb::build() ?>
        
    </section>

    <!-- Main content -->
    <section class="content">
        
        <?= $form ?>
    </section><!-- /.content -->
</aside><!-- /.right-side -->