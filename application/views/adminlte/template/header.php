<header class="header regular">
	<a href="/" class="logo">
		<img src="/media/gproc_lte/img/logo-white-o.png" class="icon">
		<!-- Add the class icon to your logo image or logo icon to add the margining -->
		<strong>{ GPROC }</strong>
		<!--img src="/media/saes_lte/img/logo-white.png" class="img-responsive"-->
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<!--a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a-->
		<?php
			$objeto = Session::instance()->get('objeto');
		?>
		<p class="navbar-text navbar-left hidden-xs" title="<?= $welcome ?>"><?= $welcome ?></p>
		
		<div class="navbar-right">
			<ul class="nav navbar-nav">
				
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i>
						<span><?= $objeto['username'] ?> <i class="caret"></i></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header bg-light-blue">
							<p>
								<!--Evento actual: <?//=  $oEventoActual->oEvento->even_nombre ?: 'Ninguno' ?>-->
								Evento actual: <?= $objeto['even_nombre']?>
								<small>Fecha: <?= date('d/m/Y') ?></small>
							</p>
						</li>
						<!-- Menu Body -->
						
						<!--<li class="user-body">
							<div class="col-xs-6 text-left">
								
								<a href="/events" class="btn btn-default">Mis eventos</a>
								
							</div>
						</li>-->
						
						<?php if ($auth->logged_in('operador')): ?>
							<!--<li>
								<a href="/admin"><i class="fa fa-dashboard"></i> Volver al Admin</a>
							</li>
							<li class="divider"></li>-->
						<?php endif ?>
						
						<?php if ($auth->logged_in('regular') OR $auth->logged_in('operador')): ?>
							<!--<li>
								<a href="/dashboard/assessment"><i class="fa fa-check-square-o"></i> Mis autoevaluaciones</a>
							</li>
							<li>
								<a href="/reports/statistics"><i class="fa fa-bar-chart-o"></i> Ver estadísticas</a>
							</li>
							<li>
								<a href="#"><i class="fa fa-print"></i> Imprimir</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="/file/uploads/Manual de Usuario SAES.pdf"><i class="fa fa-exclamation-circle"></i> Manual de usuario</a>
							</li>-->
						<?php endif ?>
						
						<!-- Menu Footer-->
						<li class="user-footer">
							
							<?php if ($auth->logged_in('regular')): ?>
								<!--<div class="pull-left">
									<a href="/profile" class="btn btn-default btn-flat">Perfil</a>
								</div>-->
							<?php endif ?>
							
							<div class="pull-right">
								<a href="/welcome/salir" class="btn btn-default btn-flat">Salir</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
