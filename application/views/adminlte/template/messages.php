<?php if ($message = Session::instance()->get_once('error')): ?>
	<div class="alert-wrapper">
		<div class="alert alert-danger fade in text-center" role="alert">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<strong><i class="fa fa-exclamation-triangle"></i></strong> <?= $message ?>
		</div>
	</div>
<?php endif ?>

<?php if ($info = Session::instance()->get_once('info')): ?>
	<div class="alert-wrapper">
		<div class="alert alert-success fade in text-center" role="alert">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<strong>Info:</strong> <?= $info ?>
		</div>
	</div>
<?php endif ?>
