<aside class="left-side sidebar-offcanvas">                
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<img src="/media/gproc_lte/img/logo.png" class="img-responsive img-logo">
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->

		<ul class="sidebar-menu">
			
			<?php //if (ACL::instance()->check_role(Model_Role::ROLE_EVALUADOR)): ?>
			<?php if (ACL::instance()->allowed('evaluador')): ?>
				<li><a href="/evaluador/verEventos">
					<i class="fa fa-dashboard"></i> Ver eventos</a></li>
			<?php endif ?>
			<?php //else: ?>
				<li>
					<a href="/eventos<?= URL::query() ?>">
						<i class="fa fa-list-ul"></i> 
						<span>Eventos</span>
					</a>
				</li>
			<?php //endif ?>
			
			<?php if (Request::current()->query('acev_id')): ?>
				<li>
					<a href="/etapas<?= URL::query() ?>">
						<i class="fa fa-list"></i> 
						<span>Etapas</span>
					</a>
				</li>

				<li>
					<a href="/expresionInteres<?= URL::query() ?>">
						<i class="fa fa-list-alt"></i> 
						<span>Expresión de Interés</span>
					</a>
				</li>

				<li>
					<a href="/documentacionBasica<?= URL::query() ?>">
						<i class="fa fa-file"></i> 
						<span>Documentación Básica</span>
					</a>
				</li>

				<li class="treeview">
					<a href="#">
						<i class="fa fa-suitcase"></i>
						<span>Planes de Mejora</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="/planificacion<?= URL::query() ?>"><i class="fa fa-angle-double-right"></i>Planificación</a></li>
						<li><a href="/presupuesto<?= URL::query() ?>"><i class="fa fa-angle-double-right"></i>Presupuesto Detallado</a></li>
						<li><a href="/cronograma<?= URL::query() ?>"><i class="fa fa-angle-double-right"></i>Cronograma</a></li>
						<li><a href="/monitoreo<?= URL::query() ?>"><i class="fa fa-angle-double-right"></i>Monitoreo y Evaluación</a></li>
						<li><a href="/efectos<?= URL::query() ?>"><i class="fa fa-angle-double-right"></i>Efectos Esperados</a></li>

						<?php //if (ACL::instance()->check_role(Model_Role::ROLE_REGULAR)): ?>
						<li><a href="/cerrarPlanMejora/documentos<?= URL::query() ?>"><i class="fa fa-angle-double-right"></i>Cerrar Plan de Mejora</a></li>
						<?php //endif ?>
					</ul>
				</li>

				<li>
					<a href="/documentacionComplementaria<?= URL::query() ?>">
						<i class="fa fa-file-text"></i> 
						<span>Documentación Complementaria</span>
					</a>
				</li>
			<?php endif ?>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>
