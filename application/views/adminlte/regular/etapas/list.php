<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h4 class="text-center">
			<?= "Bienvenidos, Sres. del Comité de Calidad de ". $objeto['subtitle']." de ".$objeto['inst_region'] ?>
		</h4>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-sm-5 col-lg-4">
				<div class="etapa-convocatoria" >
					<h3><?= $objeto['even_nombre'] ?></h3>
					<br>
						<?= substr($oEvento->even_descripcion, 0, 900)."..." ?>
					<div class="etapa_convocatoria_espacio"></div>
					<br>
					<div>
						<span class="span_pdf">
							  * Bases del evento:
						</span>
						<a href="<?= $oEvento->even_url_pdf ?> " target="_blank">
							<img src="/media/gproc_lte/img/pdf.png" class="img-responsive" width="30px" height="30px">
						</a>

					</div>
				</div>
			</div>

			<div class="col-sm-7 col-lg-8">
				<h3 class="text-center">
					 Etapas de <?= $objeto['even_nombre'];?>
				</h3>
				<?= Breadcrumb::build() ?>
			<!-- Main content -->
				<table class="table table-hover ">
					<thead>
						<tr class="info">
							<th>Etapa</th>
							<th>Vigencia</th>
							<th>Estado</th>
							<!--<th>Finalizado</th>-->
						</tr>
					</thead>
					<tbody>
						<?php foreach ($aAcreditadoEtapa as $oAcreditadoEtapa): ?>
						<tr>
							<td>
								<a class="table-link" href="<?= $oAcreditadoEtapa->oEventoEtapa->evet_path.URL::query() ?>">
									<?= $oAcreditadoEtapa->oEventoEtapa->evet_nombre ?></a>
								
								<div>
								<?php if ( ! $oAcreditadoEtapa->is_pendiente()): ?>
									<?php foreach (Arr::get($fichas, $oAcreditadoEtapa->evet_code, []) as $oAcreditadoEtapaFicha): ?>

										<a class="btn btn-flat btn-sm btn-default" target="_blank" 
											href="<?= $oAcreditadoEtapaFicha->get_path() ?>" 
											title="<?= $oAcreditadoEtapaFicha->oEventoEtapaFicha->evef_nombre ?>">
											<i class="fa fa-file-text-o"></i>
										</a>

									<?php endforeach ?>
								<?php endif ?>
								</div>
								
							</td>

							<td>
								<a class="table-link" href="<?= $oAcreditadoEtapa->oEventoEtapa->evet_path ?>">
									<?= date('d/m', strtotime($oAcreditadoEtapa->oEventoEtapa->evet_fecha_inicio)) ?> - 
									<?= date('d/m', strtotime($oAcreditadoEtapa->oEventoEtapa->evet_fecha_fin)) ?>
								</a>
							</td>

							<td>
								<div>
									<a href="<?= $oAcreditadoEtapa->oEventoEtapa->evet_path ?>">
										<label class="label label-<?= $oAcreditadoEtapa->class_estado() ?>">
											<?= $oAcreditadoEtapa->estado() ?>
											<?= ($oAcreditadoEtapa->oEventoEtapa->evet_code == 'AUT') ? $objeto['auto_porcentaje'].'%' : '' ?>
										</label>
									</a>
									
									<?php /*if ($oAcreditadoEtapa->oEventoEtapa->evet_code != 'RPM'): ?>
										<?php if ($oAcreditadoEtapa->acet_estado == Model_AcreditadoEtapa::ESTADO_OBSERVADO): ?>
										<span class="_pull-right">
											<a href="/file/<?= $oAcreditadoEtapa->get_documento_observacion() ?>" 
												target="_blank" class="btn btn-xs btn-flat btn-default">
												<i class="fa fa-file-text"></i></a>
										</span>
										<?php endif ?>
									<?php else:*/ ?>
										
									<table class="table table-condensed table-bordered"><tbody>
										<?php foreach ($oAcreditadoEtapa->aAcreditadoNivelEtapa->with('oEventoNivel')->find_all() 
											as $oAcreditadoNivelEtapa): ?>
											<tr>
												<td>
												<?= $oAcreditadoNivelEtapa->oEventoNivel->evni_nombre ?>: 
												</td>
												
												<td class="text-right">
													<label class="label label-<?= $oAcreditadoNivelEtapa->class_estado() ?>">
														<?= $oAcreditadoNivelEtapa->estado() ?>
													</label>

													<?php if ($oAcreditadoNivelEtapa->is_observado()): ?>
													<span class="_pull-right">
														<a href="/file/<?= $oAcreditadoNivelEtapa->get_documento_observacion() ?>" 
															target="_blank" class="btn btn-xs btn-flat btn-default">
															<i class="fa fa-file-text"></i></a>
													</span>
													<?php endif ?>
												</td>
												
											</tr>
										<?php endforeach ?>
									</tbody></table>
										
									<?php //endif ?>
									
								</div>
							</td>

						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				<br>
			</div>
		</div>
	</section>
</aside><!-- /.right-side -->
