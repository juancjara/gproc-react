<aside class="right-side _strech">
	
	<section class="content-header">
		<h5 class="text-center">
			Bienvenidos, Sres. del Comité de Calidad de <?= $objeto['subtitle'] ?></h5>
		<h4 class="text-center">
			<?= $oEventoEtapa->evet_nombre ?>
		</h4>
		<h5 class="text-center">
			<?= $objeto['even_nombre'] ?>
		</h5>
	</section>
	
	<!-- Main content -->
	<section class="content">
		
		<?php foreach ($errors as $error): ?>
		<div class="callout callout-danger">
			<p><?= $error ?></p>
		</div>
		<?php endforeach ?>

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
