<!--<aside class="right-side strech">-->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h4 class="text-center">
			<?= "Bienvenidos, Sres. del Comité de Calidad de ". $objeto['subtitle']." de ".$objeto['inst_region'] ?>
		</h4>
	</section>
	<section class="content">
		<div class="box">
			<div class="box-body">
				<table class="table table-hover ">
					<thead>
						<tr class="info">
							<th>Evento (Porcentaje mínimo)</th>
							<th>Puntaje obtenido</th>
							<th></th>
							<!--<th>Finalizado</th>-->
						</tr>
					</thead>
					<tbody>
						<?php foreach ($aAcreditadoEvento as $oAcreditadoEvento): ?>
							<?php $evento = $eventos[$oAcreditadoEvento->acev_id] ?>
							<?php $oAutoevaluacion = $autoevaluaciones[$oAcreditadoEvento->acev_id] ?>
							<tr>
								<td>
									<?= $evento['even_nombre'] ?> (<?= $evento['even_per_minimo'] ?>)
								</td>
								<td>
									<?= $oAutoevaluacion->data['auto_porcentaje'] ?>
								</td>
								<td>
									<?php if ($oAutoevaluacion->has_access($evento)): ?>
										<a class="btn btn-flat btn-sm btn-default" 
											href="<?= '/etapas'
											.URL::query(['acev_id' => $oAcreditadoEvento->acev_id]) ?>">
											Entrar</a>
									<?php else: ?>
										Usted no alcanzó el porcentaje mínimo
									<?php endif ?>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				<br>
			</div>
		</div>
	</section>
</aside><!-- /.right-side -->
