<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Developed by Sergio Melendez -->

	<title><?= __($site_title) ?></title>

	<?= $styles ?>

</head>

<body>

	<div id="wrapper">

		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			
			<?= $header ?>
			
			<?= $navbar ?>
			
		</nav>
		
		<?= $messages ?>
		
		<?= $content ?>
		
		<?= $footer ?>

	</div>
	<!-- /#wrapper -->

	<?= $scripts ?>

</body>

</html>
