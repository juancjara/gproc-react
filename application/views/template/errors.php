<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>GPROC - PROCALIDAD</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

		<link type="text/css" href="/media/adminlte/css/bootstrap.min.css" rel="stylesheet" />
		<link type="text/css" href="/media/adminlte/css/font-awesome.min.css" rel="stylesheet" />
		<link type="text/css" href="/media/adminlte/css/ionicons.min.css" rel="stylesheet" />
		<link type="text/css" href="/media/admin/css/fonts.css" rel="stylesheet" />
		<link type="text/css" href="/media/adminlte/css/AdminLTE.css" rel="stylesheet" />
		<link type="text/css" href="/media/admin/css/datetimepicker.min.css" rel="stylesheet" />
		<link type="text/css" href="/media/admin/css/admin.css" rel="stylesheet" />
		<link type="text/css" href="/media/gproc_lte/css/gproc.css" rel="stylesheet" />
	</head>
	<body class="skin-blue">
		<header class="header regular">
			<a href="/" class="logo">
				<img src="/media/gproc_lte/img/logo-white-o.png" class="icon">
				<strong>GProC</strong>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
			</nav>
		</header>

		<div class="wrapper row-offcanvas row-offcanvas-left">
			<aside class="right-side strech">
				<section class="content">

					<div class="box">
						<div class="box-body box-responsive">
					
							<?php if ($code == 404): ?>
								<h1>404</h1>
								<h2>La página que ha solicitado no existe</h2>
								<hr>
								<p class="help-block">Si considera que se trata de un error, 
									por favor póngase en contacto con un administrador</p>
							<?php elseif ($code == 403): ?>
								<h1>403</h1>
								<h2>No tiene autorización para acceder al recurso solicitado</h2>
								<hr>
								<p class="help-block">Si considera que se trata de un error, 
									por favor póngase en contacto con un administrador</p>
							<?php else: ?>
								<h1>500</h1>
								<h2>Ha ocurrido un error</h2>
								<hr>
								<p class="help-block">Si el problema persiste,
									por favor póngase en contacto con un administrador</p>
							<?php endif ?>
							
						</div>
					</div>

				</section><!-- /.content -->
			</aside><!-- /.right-side -->

			<div class="text-center">
				<hr>
				Jr. Rousseau N° 465 - San Borja. Central (01) 605-8960<br>
				<a href="mailto:procalidad@procalidad.gob.pe">procalidad@procalidad.gob.pe</a><br>
				<!--a href="http://procalidad.gob.pe" target="_blank">ProCalidad</a> - 
				Desarrollado por <a href="http://sergiomelendez.info" target="_blank">Sergio Meléndez</a> © Copyright 2014-->
			</div><br>

		</div><!-- ./wrapper -->

	</body>
</html>

<style>
	.box {
		width: 500px;
		margin: 100px auto 0;
		text-align: center;
	}
</style>
