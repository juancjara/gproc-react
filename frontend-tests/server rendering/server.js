import path from 'path';
import express from 'express';
import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import counterApp from './reducers';
import App from './containers/App';
import { renderToString } from 'react-dom/server';

const app =  new express();
const port = 3000;

app.use('/static', express.static(__dirname + '/static'));
app.get('/', handleRender);

function handleRender(req, res) {
  // Create a new Redux store instance
  const store = configureStore({ counter: 34 });

  // Render the component to a string
  const html = renderToString(
      <Provider store={store}>
        <App />
      </Provider>
  );

  // Grab the initial state from our Redux store
  const initialState = store.getState();

  // Send the rendered page back to the client
  res.send(renderFullPage(html, initialState));
}

function renderFullPage(html, initialState) {
  return `
    <html>
      <head>
        <title>Redux Universal Example</title>
      </head>
      <body>
        <div id="root">${html}</div>
        <script>
          window.__INITIAL_STATE__ = ${JSON.stringify(initialState)}
        </script>
        <script src="/static/bundle.js"></script>
      </body>
    </html>
    `;
}

app.listen(port, function(error) {
  if (error) {
    console.error(error);
  } else {
    console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port);
  }
});

