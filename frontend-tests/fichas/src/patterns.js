export default {
  all: '.*',
  numbers: '^[0-9]*$',
  decimalNumbers: '^[0-9]+(\\.[0-9][0-9]?)?$',
  validNumber: '^([0-9]+|N\/A|S\/D)$',
  numberLength(length) {
    return `^[0-9]{${length}}$`;
  },
  email: "[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?"
};
