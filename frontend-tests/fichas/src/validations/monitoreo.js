import _ from 'lodash';

import {
  createNoIndicadoresError,
  createMissingMediosError,
  createNoMediosError,
} from '../errors/monitoreo';

export const factoresValidation = state => {
  const indicadores = state.get('ind');

  const groupedMediosMissing = state.get('medios')
    .filter(medio => medio.get('count') === 0)
    .groupBy(medio => medio.get('fact_codigo'));

  const errors = state.get('fac')
    .map(factor => {
      const factorError = [];

      const hasNoIndicadores = !indicadores
        .filter(ind => ind.get('parent_id') === factor.get('id')).size;

      if (hasNoIndicadores) {
        factorError.push(createNoIndicadoresError({
          factorId: factor.get('id')
        }));
      }

      const fact_codigo = factor.get('fact_codigo');
      if (groupedMediosMissing.has(fact_codigo)) {
        factorError.push(createMissingMediosError({
          factorId: factor.get('id'),
          missing: groupedMediosMissing.get(fact_codigo).size
        }));
      }

      return factorError;
    });

  return errors;
};

export const indicadoresValidation = state =>
  state.get('ind')
  .map(ind =>
       ind.getIn(['data', 'medios']).size ?
       []: createNoMediosError({ indicadorId: ind.get('dbId') })
  );
