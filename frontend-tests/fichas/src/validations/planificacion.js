import { estandaresGroup }
  from '../containers/planificacion/tablePlanificacion/actividad/Estandar';

import { findIndexBy } from '../utils';

import {
  createMissingEstandaresError,
  createNoActividadesError,
} from '../errors/planificacion';

const getNames = e => `${e.get('text')}`;

export const dimensionesValidation = (
  state
) => _.times(state.get('dim').size, () => []);

export const actividadesValidation = (
  state
) => _.times(state.get('act').size, () => []);

const notHideAndHasNoActividades = ({
  factor,
  fact_codigo,
  actividades
}) => !factor.getIn(['data', 'hide']) &&
  (!actividades.has(fact_codigo) || actividades.get(fact_codigo).size === 0);

export const factoresValidation = (
  state
) => {
  const missingEstandares = state.get('estandares')
    .filter(estandar => estandar.get('tipo') === estandaresGroup[0].key)
    .filter(estandar => estandar.get('count') === 0);

  const groupedMissing = missingEstandares
    .groupBy(estandar => estandar.get('fact_codigo'));

  const actividades = state.get('act')
    .groupBy(act => act.getIn(['data', 'fact_codigo']));

  const errores = state.get('fac')
    .map(factor => {
      const fact_codigo = factor.getIn(['data', 'fact_codigo']);
      let errors = [];
      if (groupedMissing.has(fact_codigo)) {
        errors.push(
          createMissingEstandaresError({
            factorId: fact_codigo,
            missing: groupedMissing.get(fact_codigo).size
          })
        );
      }

      if (notHideAndHasNoActividades({ factor, fact_codigo, actividades })) {
        errors.push(
          createNoActividadesError({ factorId: fact_codigo })
        );
      }

      return errors;
    });

  return errores.toJS();
};
