import _ from 'lodash';

import { toNumber } from '../utils';

import {
  createNoSubActividadesError,
  createCustomDetalleTableError,
  createMissingFileSubActividadError,
  createCapacitacionExternaDiasError,
  createProfesoreVisitantesDiasError,
} from '../errors/presupuesto';

import {
  getRowPerc,
} from '../containers/planificacion/DetalleTable';

import { getTableData } from '../containers/presupuesto/DetalleTable';

export const actividadesValidation = state => {
  const subActividadesGroupedByAct = state.get('sub')
    .groupBy(s => s.get('parent_id'));

  return state.get('act')
    .map(act => {
      const dbId = act.get('dbId');
      if (!subActividadesGroupedByAct.has(dbId) ||
          !subActividadesGroupedByAct.get(dbId).size) {
        return [
          createNoSubActividadesError({ actividadDbId: dbId })
        ];
      }
      return [];
    });
};

const validateProfesoreVisitantes = subData => {
  const niveles = subData.get('niveles');

  return niveles.has(1) && niveles.get(1) === 'SP' &&
    toNumber(subData.get('num_dias') > 15);
};

const validateCapacitacionExterna = subData => {
  const niveles = subData.get('niveles');

  return niveles.has(0) && niveles.get(0) === 'R' &&
    niveles.has(1) && niveles.get(1) === 'RC' &&
    niveles.has(3) && niveles.get(3).slice(-1) === 'I' &&
    subData.get('inscripcion') > 0 && toNumber(subData.get('num_dias')) > 5;
};

export const subactividadesValidation = state =>
  state.get('sub')
  .map(sub => {
    const errors = [];
    if (sub.getIn(['data', 'todo_seleccionado']) &&
        !sub.getIn(['data', 'file'])) {
      errors.push(createMissingFileSubActividadError({
        subactividadId: sub.get('dbId'),
      }));
    }

    const capacitacionExternaDiasGreatherThan5 =
      validateCapacitacionExterna(sub.get('data'));
    if (capacitacionExternaDiasGreatherThan5) {
      errors.push(createCapacitacionExternaDiasError({
        subactividadId: sub.get('dbId'),
      }));
    }

    const profesoresVisitantesDiasGreatherThan15 =
      validateProfesoreVisitantes(sub.get('data'));
    if (profesoresVisitantesDiasGreatherThan15) {
      errors.push(createProfesoreVisitantesDiasError({
        subactividadId: sub.get('dbId'),
      }));
    }

    return errors;
  });

const percToNumber = str =>
  typeof str === 'string' ?
    toNumber(str.replace('%', '')) :
    str;

const percLessThan = (n1, n2) => percToNumber(n1) <= percToNumber(n2);

export const detalleTableValidation = state => {
  const totales = getTableData(state.get('sub'));
  const percFec = getRowPerc(totales.get('fec'));

  const fecNotLessThan = !percLessThan(totales.getIn(['fec', 'porc']), 95);
  const fecPasantiasNoLessThan = !percLessThan(percFec.get('RP'), 10);
  const fecRRHHNoLessThan = !percLessThan(percFec.get('tot_R'), 40);
  const fecConsultoriasNoLessThan = !percLessThan(percFec.get('SC'), 30);
  const fecBienesNoLessThan = !percLessThan(percFec.get('tot_B'), 60);

  const errors = [
    {
      msg: 'no puede ser mayor que 95%',
      show: fecNotLessThan,
    },
    {
      msg: 'para Pasantías no puede ser mayor que 10%',
      show: fecPasantiasNoLessThan,
    },
    {
      msg: 'para Formación de RRHH no puede ser mayor que 40%',
      show: fecRRHHNoLessThan,
    },
    {
      msg: 'para Consultorías no puede ser mayor que 30%',
      show: fecConsultoriasNoLessThan,
    },
    {
      msg: 'para Equipamiento y obras menores no puede ser mayor que 60%',
      show: fecBienesNoLessThan,
    },
  ];

  return errors
    .filter(({ show }) => show)
    .map(({ msg }) =>
      createCustomDetalleTableError(`El % del financiamiento FEC ${msg}.`));
};
