import _ from 'lodash';

import {
  createServiciosError,
  createPasantiasError,
} from '../errors/cronograma';

import { getContext } from '../context';
import { findIndexBy } from '../utils';

export const cronogramaValidation = state =>
  state.get('cro')
    .map((cro, i) => {
      const subActividadIndex = findIndexBy(
        getContext().subActividades,
        cro.get('parent_id'),
        s => s.id
      );

      const errors = [];

      const isServicios = getContext()
        .subActividades[subActividadIndex].data.niveles[0] === 'S';
      const startNot3Q = cro.getIn(['data', 'start']) < 2;
      if (isServicios && startNot3Q) {
        errors.push(createServiciosError({
          subActividadId: getContext() .subActividades[subActividadIndex].id,
        }));
      }

      const isPasantias = getContext()
        .subActividades[subActividadIndex].data.niveles[1] === 'RP';

      const startLessThan5Q = cro.getIn(['data', 'start']) < 4;
      if (isPasantias && startLessThan5Q) {
        errors.push(createPasantiasError({
          subActividadId: getContext() .subActividades[subActividadIndex].id,
        }));
      }

      return errors;
    });
