import { getContext } from '../context';
import {
  VALIDATION_COMITE_ERROR,
  VALIDATION_CUERPO_ACADEMICO_ERROR,
  VALIDATION_TOTAL_NOMBRADOS_ERROR,
} from '../messages/expresionInteres';
import {
  ERROR_CUERPO_ACADEMICO_TOTAL,
  ERROR_CUERPO_ACADEMICO_DOCENTE_TOTAL,
  ERROR_COMITE_NO_MEMBERS,
  createSedeError,
  createCarreraNameError,
} from '../errors/expresionInteres';

import {
  cleanText,
} from '../utils';

const notValidTotalNombrados = (
  cuerpoAcademico
) => {
  const gradosCondicion = cuerpoAcademico.get('grados_condicion');
  const docentesCategoria = cuerpoAcademico.get('docentes_categoria');
  return gradosCondicion.getIn(['tt', 'tt']) !==
    docentesCategoria.getIn(['tt', 'tt']);
};

const notValidTotalesCuerpoAcademico = (
  cuerpoAcademico
) => {
  const gradosCondicion = cuerpoAcademico.get('grados_condicion');
  const gradosDedicacion = cuerpoAcademico.get('grados_dedicacion');
  return gradosCondicion
    .some((row, key) => row.get('tt') !== gradosDedicacion.getIn([key, 'tt']));
};

export const fichaValidation = (
 state
) => {
  let errors = [];
  return errors;
};

export const carreraValidation = (
  carreraState
) => {
  let errors = [];
  const comite = carreraState.getIn(['data', 'comite']);

  if (getContext().institucional && !comite.size) {
    errors.push(ERROR_COMITE_NO_MEMBERS);
  }

  if (notValidTotalesCuerpoAcademico(
    carreraState.getIn(['data', 'cuerpo_academico'])
  )) {
    errors.push(ERROR_CUERPO_ACADEMICO_TOTAL);
  }

  if (getContext().universidad &&
      notValidTotalNombrados(
        carreraState.getIn(['data', 'cuerpo_academico'])
      )) {
    errors.push(ERROR_CUERPO_ACADEMICO_DOCENTE_TOTAL);
  }

  return errors;
};

export const institucionalValidation = (
  state
) => {
  let errors = [];

  const comite = state.getIn(['ins', 'data', 'comite']);
  if (!getContext().institucional && !comite.size) {
    errors.push(ERROR_COMITE_NO_MEMBERS);
  }

  const sedes = state.getIn(['ins', 'data', 'sedes']);
  errors = errors.concat(
    sedes
      .filter(sede => sede.get('carreras').size === 0)
      .map(sede => createSedeError(sede.get('nombre_sede')))
      .toJS()
  );
  if (!getContext().institucional) {
    const bannedName = state.getIn(['car', 0, 'data', 'nombre']);

    const cleanBannedName = cleanText(bannedName);
    const matchingCarreras = sedes.getIn([0, 'carreras'])
      .filter(carrera =>
        cleanText(carrera.get('nombre')) === cleanBannedName
      );

    if (matchingCarreras.size) {
      errors = errors
        .concat(createCarreraNameError(matchingCarreras.getIn([0, 'nombre'])));
    }
  }

  if (notValidTotalesCuerpoAcademico(
    state.getIn(['ins', 'data', 'cuerpo_academico'])
  )) {
    errors.push(ERROR_CUERPO_ACADEMICO_TOTAL);
  }

  if (getContext().universidad &&
      notValidTotalNombrados(
        state.getIn(['ins', 'data', 'cuerpo_academico'])
      )) {
    errors.push(ERROR_CUERPO_ACADEMICO_DOCENTE_TOTAL);
  }

  return errors;
};
