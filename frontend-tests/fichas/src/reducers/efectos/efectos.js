import actions from '../../constants/efectos';
const {
  EFECTO_UPDATE,
} = actions;

import efecto from './efecto';

export default (
  state,
  action
) => {
  switch (action.type) {
    case EFECTO_UPDATE:
      return state
        .setIn(
          [action.efectoIndex, 'data'],
          efecto(state.getIn([action.efectoIndex, 'data']), action)
        )
        .setIn([action.efectoIndex, 'status', 'hasChanged'], true);

    default:
      return state;
  }
};
