import { fromJS } from 'immutable';

import actions from '../../constants/efectos';
const {
  EFECTO_UPDATE,
} = actions;

const initialState = fromJS({
  descripcion: ''
});

export default (
  state,
  action
) => {

  if (!state) {
    return initialState;
  }

  switch (action.type) {
    case EFECTO_UPDATE:
      return state.
        set(action.prop, action.value);

    default:
      return state;
  }
};

