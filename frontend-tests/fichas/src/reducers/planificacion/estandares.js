import actions from '../../constants/planificacion';
const {
  ESTANDAR_DECREMENT,
  ESTANDAR_INCREMENT,
} = actions;

import { findIndexBy } from '../../utils';

const idSelector = e => e.get('esta_id');

export default (
  state,
  action
) => {
  let index;

  switch (action.type) {

    case ESTANDAR_DECREMENT:
      index = findIndexBy(state, action.estandar, idSelector);
      if (index === -1) {
        return state;
      }
      return state
        .updateIn([index, 'count'], c => c - 1);

    case ESTANDAR_INCREMENT:
      index = findIndexBy(state, action.estandar, idSelector);
      if (index === -1) {
        return state;
      }
      return state
        .updateIn([index, 'count'], c => c + 1);

    default:
      return state;
  }
};
