import { fromJS } from 'immutable';

import actions from '../../constants/planificacion';
const {
  ACTIVIDAD_ADD,
  ACTIVIDAD_REMOVE,
  ACTIVIDAD_UPDATE,

  ACTIVIDAD_ESTANDAR_ADD,
  ACTIVIDAD_ESTANDAR_REMOVE,
  ACTIVIDAD_ESTANDAR_UPDATE,
} = actions;

import status from '../status';
import actividad from './actividad/actividad';

import * as ResumenTable from '../../containers/planificacion/ResumenTable';
import * as DetalleTable from '../../containers/planificacion/DetalleTable';

export default (
  state,
  action
) => {
  switch (action.type) {
    case ACTIVIDAD_UPDATE:
    case ACTIVIDAD_ESTANDAR_ADD:
    case ACTIVIDAD_ESTANDAR_REMOVE:
    case ACTIVIDAD_ESTANDAR_UPDATE:
      return state
        .setIn(
          [action.actividadIndex, 'data'],
          actividad(state.getIn([action.actividadIndex, 'data']), action)
        )
        .setIn([action.actividadIndex, 'status', 'hasChanged'], true);

    case ACTIVIDAD_ADD:
      const newActividad = fromJS({
        dbId: action.dbId || null,
        parent_id: action.factorDbId,
        data: actividad(undefined, action),
        status: status(undefined, {}),
        "pasantias": '0',
        detalleTotales: {
          ...DetalleTable.createBaseStructure(),
        },
      });
      return state.insert(action.index, newActividad);

    case ACTIVIDAD_REMOVE:
      return state.delete(action.index);

    default:
      return state;
  }
};
