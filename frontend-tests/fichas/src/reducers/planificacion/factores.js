import { fromJS } from 'immutable';

import actions from '../../constants/planificacion';
const {
  FACTOR_UPDATE,
  FACTOR_TOGGLE_HIDE,
} = actions;

const getInitialState = ({
  cod,
}) => fromJS({
  fact_codigo: cod,
  resultado: '',
  hide: false,
});

export default (
  state,
  action
) => {
  if (!state) {
    return getInitialState(action);
  }

  switch (action.type) {
    case FACTOR_UPDATE:
      return state
        .setIn([action.index, 'data', action.prop], action.value)
        .setIn([action.index, 'status', 'hasChanged'], true);

    case FACTOR_TOGGLE_HIDE:
      return state
        .updateIn([action.index, 'data', 'hide'], (hide) => !hide)
        .setIn([action.index, 'status', 'hasChanged'], true);

    default:
      return state;
  }
};
