import actions from '../../../constants/planificacion';
const {
  ACTIVIDAD_UPDATE,

  ACTIVIDAD_ESTANDAR_ADD,
  ACTIVIDAD_ESTANDAR_REMOVE,
  ACTIVIDAD_ESTANDAR_UPDATE,
} = actions;

import { fromJS } from 'immutable';

import estandares from './actividadEstandares';

const getInitialState = ({
  id,
  fact_codigo,
}) => fromJS({
  id,
  fact_codigo,
  miactividad: '',
  actor_cargo: '',
  estandares: [],
});

export default (
  state,
  action
) => {
  if (!state) {
    return getInitialState(action);
  }

  switch (action.type) {
    case ACTIVIDAD_UPDATE:
      return state.set(action.prop, action.value);

    case ACTIVIDAD_ESTANDAR_ADD:
    case ACTIVIDAD_ESTANDAR_REMOVE:
    case ACTIVIDAD_ESTANDAR_UPDATE:
      return state.set(
        'estandares',
        estandares(state.get('estandares'), action)
      );

    default:
      return state;
  }
};
