import { Map } from 'immutable';

import actions from '../../../constants/planificacion';
const {
  ACTIVIDAD_ESTANDAR_ADD,
  ACTIVIDAD_ESTANDAR_REMOVE,
  ACTIVIDAD_ESTANDAR_UPDATE,
} = actions;

export default (
  state,
  action
) => {
  switch (action.type) {

    case ACTIVIDAD_ESTANDAR_ADD:
      return state.push(Map({
        id: action.id,
        esta_id: -1,
      }));

    case ACTIVIDAD_ESTANDAR_REMOVE:
      return state.delete(action.estandarIndex);

    case ACTIVIDAD_ESTANDAR_UPDATE:
      return state.setIn([action.estandarIndex, 'esta_id'], action.estandar);

    default:
      return state;
  }
};
