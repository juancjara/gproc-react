import { fromJS } from 'immutable';

import actions from '../../constants/planificacion';
const {
  FICHA_FIRMA_UPDATE,
} = actions;

import status from '../status';

const initialState = fromJS({
  data: {
    firmas: {
      com: {
        nombre: '',
        cargo: '',
        date: '',
        date_local: ''
      },
      dir: {
        nombre: '',
        cargo: '',
        date: '',
        date_local: ''
      }
    }
  },
});

export default (
  state = initialState,
  action
) => {
  switch (action.type) {
    case FICHA_FIRMA_UPDATE:
      return state
        .setIn(['data', 'firmas', action.firmaType, action.prop], action.value)
        .setIn(['status', 'hasChanged'], true);

    default:
      return state;
  }
}
