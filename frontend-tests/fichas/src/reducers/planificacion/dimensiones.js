import { fromJS } from 'immutable';

import actions from '../../constants/planificacion';

const {
  DIMENSION_UPDATE,
} = actions;

const getInitialState = ({
  cod
}) => fromJS({
  dime_codigo: cod,
  objetivo: '',
});

export default (
  state,
  action
) => {
  if (!state) {
    return getInitialState(action)
  }

  switch (action.type) {
    case DIMENSION_UPDATE:
      return state
        .setIn([action.index, 'data', action.prop], action.value)
        .setIn([action.index, 'status', 'hasChanged'], true);

    default:
      return state;
  }
};
