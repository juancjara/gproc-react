import { fromJS } from 'immutable';

import actions from '../../../constants/presupuesto';
const {
  SUBACTIVIDAD_ADD_NEXT_SELECT,
  SUBACTIVIDAD_UPDATE,
  SUBACTIVIDAD_LAST_LEVEL_SELECTED,
  SUBACTIVIDAD_LEVEL_SELECT,
} = actions;

const initialState = fromJS({
  niveles: [null],
  file: '',
  descripcion: '',
	otro: '',
  viaticos: '',
  pasajes: '',
	costo_unitario: '',
  unidad: '',
	cantidad: '',
  costo: '',
	total: '',
	financiamiento: '',
  todo_seleccionado: false,
});

export default (
  state = initialState,
  action
) => {
  switch (action.type) {

    case SUBACTIVIDAD_ADD_NEXT_SELECT:
      return state.setIn(['data', 'niveles', action.level + 1], null);

    case SUBACTIVIDAD_LAST_LEVEL_SELECTED:
      return _.reduce(
        action.fields,
        (state, {prop, value}) => state.setIn(['data', prop], value),
        state
      );

    case SUBACTIVIDAD_LEVEL_SELECT:
      return state
        .setIn(['data', 'todo_seleccionado'], action.isLastChild)
        .setIn(['data', 'niveles', action.level], action.value)
        .updateIn(
          ['data', 'niveles'],
          niveles => niveles.slice(0, action.level + 1)
        );

    case SUBACTIVIDAD_UPDATE:
      return state
        .setIn(['data', action.prop], action.value);

    default:
      return state;
  }
};
