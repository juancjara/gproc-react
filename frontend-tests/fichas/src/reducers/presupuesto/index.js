import { fromJS } from 'immutable';

import actions from '../../constants/presupuesto';
const {
  SUBACTIVIDAD_ADD,
  SUBACTIVIDAD_REMOVE,
  SUBACTIVIDAD_UPDATE,

  SUBACTIVIDAD_ADD_NEXT_SELECT,
  SUBACTIVIDAD_LAST_LEVEL_SELECTED,
  SUBACTIVIDAD_LEVEL_SELECT,
} = actions;

import actionsPlanificacion from '../../constants/planificacion';
const {
  FICHA_FIRMA_UPDATE,
} = actionsPlanificacion;

import nodoActions from '../../constants/status';
const {
  NODO_VALIDATION_ERROR,
  NODO_FETCHING,
  NODO_STORED,
  NODO_SERVER_ERROR,
  NODO_SUCCESS,
  UPDATE_REQUEST_TYPE,
} = nodoActions;

import ficha from '../planificacion/ficha';
import status from '../status';
import subactividades from './subactividades';

export default (
  state,
  action
) => {
  let newState = state;

  switch (action.type) {
    case UPDATE_REQUEST_TYPE:
      return newState.set('requestType', action.request);

    case NODO_STORED:
      newState = newState
        .setIn([...action.path, 'dbId'], action.dbId);
    case NODO_SUCCESS:
    case NODO_VALIDATION_ERROR:
    case NODO_FETCHING:
    case NODO_SERVER_ERROR:
      return newState
        .setIn(
          [...action.path, 'status'],
          status(state.getIn([...action.path, 'status']), action)
        );

    case FICHA_FIRMA_UPDATE:
      return state
        .set(
          'fic',
          ficha(state.get('fic'), action)
        );

    case SUBACTIVIDAD_ADD_NEXT_SELECT:
    case SUBACTIVIDAD_ADD:
    case SUBACTIVIDAD_REMOVE:
    case SUBACTIVIDAD_UPDATE:
    case SUBACTIVIDAD_LAST_LEVEL_SELECTED:
    case SUBACTIVIDAD_LEVEL_SELECT:
      return newState
        .set(
          'sub',
          subactividades(state.get('sub'), action)
        );

    default:
      return state;
  }
}
