import { fromJS } from 'immutable';

import actions from '../../constants/presupuesto';
const {
  SUBACTIVIDAD_ADD,
  SUBACTIVIDAD_REMOVE,
  SUBACTIVIDAD_UPDATE,

  SUBACTIVIDAD_ADD_NEXT_SELECT,
  SUBACTIVIDAD_LAST_LEVEL_SELECTED,
  SUBACTIVIDAD_LEVEL_SELECT,
} = actions;

import status from '../status';
import subactividad from './subactividad/subactividad';

export default (
  state,
  action
) => {
  switch (action.type) {

    case SUBACTIVIDAD_ADD_NEXT_SELECT:
    case SUBACTIVIDAD_UPDATE:
    case SUBACTIVIDAD_LAST_LEVEL_SELECTED:
    case SUBACTIVIDAD_LEVEL_SELECT:
      return state
        .set(
          action.index,
          subactividad(state.get(action.index), action)
        )
        .setIn([action.index, 'status', 'hasChanged'], true);

    case SUBACTIVIDAD_ADD:
      const newSubActividad = fromJS({
        dbId: action.dbId || null,
        parent_id: action.actividadDbId,
        data: subactividad(undefined, action),
        status: status(undefined, {}),
      });

      return state
        .insert(action.index, newSubActividad);

    case SUBACTIVIDAD_REMOVE:
      return state.delete(action.index);

    default:
      return state;
  }
};
