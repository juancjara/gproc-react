import actions from '../../constants/monitoreo';
const {
  MEDIO_DECREMENT,
  MEDIO_INCREMENT,
} = actions;

import { findIndexBy } from '../../utils';

const idSelector = e => e.get('id');

export default (
  state,
  action
) => {
  let index;

  switch (action.type) {
    case MEDIO_DECREMENT:
      index = findIndexBy(state, action.id, idSelector);
      if (index === -1) {
        return state;
      }
      return state
        .updateIn([index, 'count'], c => c - 1);

    case MEDIO_INCREMENT:
      index = findIndexBy(state, action.id, idSelector);
      if (index === -1) {
        return state;
      }
      return state
        .updateIn([index, 'count'], c => c + 1);

    default:
      return state;
  }
};
