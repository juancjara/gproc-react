import { fromJS } from 'immutable';

import actionsPlanificacion from '../../constants/planificacion';
const {
  FICHA_FIRMA_UPDATE,
} = actionsPlanificacion;

import actionsMonitoreo from '../../constants/monitoreo';
const {
  INDICADOR_ADD,
  INDICADOR_REMOVE,
  INDICADOR_UPDATE,

  INDICADOR_MEDIO_ADD,
  INDICADOR_MEDIO_REMOVE,
  INDICADOR_MEDIO_UPDATE,
  INDICADOR_MEDIO_OTRO_UPDATE,

  MEDIO_DECREMENT,
  MEDIO_INCREMENT,
} = actionsMonitoreo;

import nodoActions from '../../constants/status';
const {
  NODO_VALIDATION_ERROR,
  NODO_FETCHING,
  NODO_STORED,
  NODO_SERVER_ERROR,
  NODO_SUCCESS,
  UPDATE_REQUEST_TYPE,
} = nodoActions;

import ficha from '../planificacion/ficha';
import status from '../status';
import indicadores from './indicadores';
import medios from './medios';

export default (
  state,
  action
) => {
  let newState = state;

  switch(action.type) {
    case UPDATE_REQUEST_TYPE:
      return newState.set('requestType', action.request);

    case NODO_STORED:
      newState = newState
        .setIn([...action.path, 'dbId'], action.dbId);
    case NODO_SUCCESS:
    case NODO_VALIDATION_ERROR:
    case NODO_FETCHING:
    case NODO_SERVER_ERROR:
      return newState
        .setIn(
          [...action.path, 'status'],
          status(state.getIn([...action.path, 'status']), action)
        );

    case FICHA_FIRMA_UPDATE:
      return state
        .set(
          'fic',
          ficha(state.get('fic'), action)
        );

    case INDICADOR_ADD:
    case INDICADOR_REMOVE:
    case INDICADOR_UPDATE:
    case INDICADOR_MEDIO_ADD:
    case INDICADOR_MEDIO_REMOVE:
    case INDICADOR_MEDIO_UPDATE:
    case INDICADOR_MEDIO_OTRO_UPDATE:
      return state
        .set(
          'ind',
          indicadores(state.get('ind'), action)
        );

    case MEDIO_DECREMENT:
    case MEDIO_INCREMENT:
      return state
        .set(
          'medios',
          medios(state.get('medios'), action)
        );

    default:
      return state;
  }
};
