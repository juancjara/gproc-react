import { fromJS } from 'immutable';

import actions from '../../constants/monitoreo';
const {
  INDICADOR_ADD,
  INDICADOR_REMOVE,
  INDICADOR_UPDATE,

  INDICADOR_MEDIO_ADD,
  INDICADOR_MEDIO_REMOVE,
  INDICADOR_MEDIO_UPDATE,
  INDICADOR_MEDIO_OTRO_UPDATE,

  MEDIO_DECREMENT,
  MEDIO_INCREMENT,
} = actions;

import  medios from './medios';
import indicador from './indicador/indicador';
import status from '../status';

export default (
  state,
  action
) => {
  switch (action.type) {
    case INDICADOR_ADD:
      const newIndicador = fromJS({
        dbId: action.dbId || null,
        parent_id: action.factorDbId,
        data: indicador(undefined, {}),
        status: status(undefined, {}),
      });
      return state.push(newIndicador);

    case INDICADOR_REMOVE:
      return state.delete(action.index);

    case INDICADOR_MEDIO_ADD:
    case INDICADOR_MEDIO_REMOVE:
    case INDICADOR_MEDIO_UPDATE:
    case INDICADOR_MEDIO_OTRO_UPDATE:
    case INDICADOR_UPDATE:
      return state.setIn(
        [action.indicadorIndex, 'data'],
        indicador(state.getIn([action.indicadorIndex, 'data']), action)
      )
      .setIn([action.indicadorIndex, 'status', 'hasChanged'], true);

    case MEDIO_DECREMENT:
    case MEDIO_INCREMENT:
      return state
        .set(
          'medios',
          medios(state.get('medios'), action)
        );

    default:
      return state;
  }
};
