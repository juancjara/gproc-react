import { fromJS } from 'immutable';

import actions from '../../../constants/monitoreo';
const {
  INDICADOR_UPDATE,

  INDICADOR_MEDIO_ADD,
  INDICADOR_MEDIO_REMOVE,
  INDICADOR_MEDIO_UPDATE,
  INDICADOR_MEDIO_OTRO_UPDATE,
} = actions;

import indicadoresMedios from './indicadorMedios';

const initialState = fromJS({
  descripcion: '',
  medios: [],
});

export default (
  state = initialState,
  action
) => {
  switch (action.type) {
    case INDICADOR_UPDATE:
      return state.set(action.prop, action.value);


    case INDICADOR_MEDIO_ADD:
    case INDICADOR_MEDIO_REMOVE:
    case INDICADOR_MEDIO_UPDATE:
    case INDICADOR_MEDIO_OTRO_UPDATE:
      return state.set(
        'medios',
        indicadoresMedios(state.get('medios'), action)
      );

    default:
      return state;
  }
};
