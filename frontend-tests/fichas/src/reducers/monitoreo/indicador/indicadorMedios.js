import { Map } from 'immutable';

import actions from '../../../constants/monitoreo';
const {
  INDICADOR_MEDIO_ADD,
  INDICADOR_MEDIO_REMOVE,
  INDICADOR_MEDIO_UPDATE,
  INDICADOR_MEDIO_OTRO_UPDATE,
} = actions;

export default (
  state,
  action
) => {
  switch (action.type) {
    case INDICADOR_MEDIO_ADD:
      return state.push(Map({
        id: action.id,
        conc_id: '',
        otro: '',
      }));

    case INDICADOR_MEDIO_UPDATE:
      return state.setIn(
        [action.indicadorMedioIndex, 'conc_id'],
        action.conc_id
      );

    case INDICADOR_MEDIO_OTRO_UPDATE:
    console.log(action);
    console.log('here');
      return state.setIn(
        [action.indicadorMedioIndex, 'otro'],
        action.text
      );

    case INDICADOR_MEDIO_REMOVE:
      return state.delete(action.indicadorMedioIndex);

    default:
      return state;
  }
}
