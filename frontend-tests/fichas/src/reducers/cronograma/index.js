import { fromJS } from 'immutable';

import actionsPlanificacion from '../../constants/planificacion';
const {
  FICHA_FIRMA_UPDATE,
} = actionsPlanificacion;

import nodoActions from '../../constants/status';
const {
  NODO_VALIDATION_ERROR,
  NODO_FETCHING,
  NODO_STORED,
  NODO_SERVER_ERROR,
  NODO_SUCCESS,
  UPDATE_REQUEST_TYPE,
} = nodoActions;

import actionsCronograma from '../../constants/cronograma';
const {
  CRONOGRAMA_UPDATE,
} = actionsCronograma;

import cronograma from './cronograma';
import ficha from '../planificacion/ficha';
import status from '../status';

export default (
  state,
  action
) => {
  let newState = state;

  switch(action.type) {
    case UPDATE_REQUEST_TYPE:
      return newState.set('requestType', action.request);

    case NODO_STORED:
      newState = newState
        .setIn([...action.path, 'dbId'], action.dbId);
    case NODO_SUCCESS:
    case NODO_VALIDATION_ERROR:
    case NODO_FETCHING:
    case NODO_SERVER_ERROR:
      return newState
        .setIn(
          [...action.path, 'status'],
          status(state.getIn([...action.path, 'status']), action)
        );

    case CRONOGRAMA_UPDATE:
      return state
        .set(
          'cro',
          cronograma(state.get('cro'), action)
        );

    case FICHA_FIRMA_UPDATE:
      return state
        .set(
          'fic',
          ficha(state.get('fic'), action)
        );

    default:
      return state;
  }
};
