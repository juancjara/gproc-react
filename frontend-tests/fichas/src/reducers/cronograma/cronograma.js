import { fromJS } from 'immutable';

import actions from '../../constants/cronograma';
const {
  CRONOGRAMA_UPDATE,
} = actions;

const initialStateItem = fromJS({
  start: 0,
  duration: 1,
});

export default (
  state,
  action
) => {
  if (!state) return initialStateItem;

  switch (action.type) {
    case CRONOGRAMA_UPDATE:
      return state
        .setIn(
          [action.cronogramaIndex, 'data', action.prop],
          action.value
        )
        .setIn([action.cronogramaIndex, 'status', 'hasChanged'], true);

    default:
      return state;
  }
};
