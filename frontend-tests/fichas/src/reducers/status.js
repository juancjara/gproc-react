import { fromJS } from 'immutable';

import statusActions from '../constants/status';
const {
  NODO_VALIDATION_ERROR,
  NODO_STORED,
  NODO_FETCHING,
  NODO_SERVER_ERROR,
  NODO_SUCCESS,
} = statusActions;

const initialState = fromJS({
  status: 'STORED',
  hasChanged: false,
  errors: [],
});

export default (
  state = initialState,
  action
) => {
  switch (action.type) {

    case NODO_VALIDATION_ERROR:
      return state
        .set('errors', action.errors);

    case NODO_FETCHING:
      return state
        .set('status', 'FETCHING')
        .set('hasChanged', false);

    case NODO_SUCCESS:
      return state
        .set('status', 'STORED');

    case NODO_STORED:
      return state
        .set('status', 'STORED')
        .set('dbId', action.dbId);

    case NODO_SERVER_ERROR:
      return state
        .set('status', 'SERVER_ERROR')
        .set('hasChanged', true);

    default:
      return state;
  }
};
