import { Map, fromJS } from 'immutable';

import actions from '../../constants/expresionInteres';
const {
  FICHA_UPDATE,
  FICHA_UPDATE_INST,
  FICHA_UPDATE_CARR,
} = actions;

const initialState = fromJS({
  tifi_id: '',
  datos: {
    inst: {
      nombre: '',
      cargo: '',
      direccion: '',
      fijo: '',
      celular: '',
      email: '',
      emailalt: '',
      date: '',
      date_local: '',
    },
    carr: {
      nombre: '',
      cargo: '',
      direccion: '',
      fijo: '',
      celular: '',
      email: '',
      emailalt: '',
      date: '',
      date_local: '',
    },
  },
});

export default (
  state = initialState,
  action
) => {
  switch (action.type) {
    case FICHA_UPDATE:
      return state.set(action.prop, action.value);

    case FICHA_UPDATE_INST:
      return state.setIn(['datos', 'inst', action.prop], action.value);

    case FICHA_UPDATE_CARR:
      return state.setIn(['datos', 'carr', action.prop], action.value);

    default:
      return state;
  }
};
