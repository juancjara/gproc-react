import { Map, fromJS } from 'immutable';

import actions from '../../constants/expresionInteres';
const {
  INSTITUCIONAL_UPDATE,

  INSTITUCIONAL_COMITE_MEMBER_ADD,
  INSTITUCIONAL_COMITE_MEMBER_UPDATE,
  INSTITUCIONAL_COMITE_MEMBER_REMOVE,

  INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM,
  INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_COL_SUM,

  INSTITUCIONAL_TOTAL_ESTUDIANTES_UPDATE,

  INSTITUCIONAL_SEDE_ADD,
  INSTITUCIONAL_SEDE_UPDATE,
  INSTITUCIONAL_SEDE_REMOVE,
  INSTITUCIONAL_SEDE_CARRERA_ADD,
  INSTITUCIONAL_SEDE_CARRERA_UPDATE,
  INSTITUCIONAL_SEDE_CARRERA_REMOVE,
} = actions;

import comite from '../expresionInteres/institucional/comite';
import cuerpoAcademico
  from '../expresionInteres/institucional/cuerpoAcademico';
import totalEstudiantes
  from '../expresionInteres/institucional/totalEstudiantes';
import sedes from '../expresionInteres/institucional/sedes';

const initialState = fromJS({
  "comite": comite(undefined, {}),
  "ruc": "",
  "sedes": sedes(undefined, {}),
  "cuerpo_academico": cuerpoAcademico(undefined, {}),
  "total_estudiantes": totalEstudiantes(undefined, {}),
});

export default (
  state = initialState,
  action
) => {
  switch (action.type) {
    case INSTITUCIONAL_UPDATE:
      return state.set(action.prop, action.value);

    case INSTITUCIONAL_COMITE_MEMBER_ADD:
    case INSTITUCIONAL_COMITE_MEMBER_UPDATE:
    case INSTITUCIONAL_COMITE_MEMBER_REMOVE:
      return state.set('comite', comite(state.get('comite'), action));

    case INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM:
    case INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_COL_SUM:
      return state.setIn(
        ['cuerpo_academico', action.table],
        cuerpoAcademico(state.getIn(['cuerpo_academico', action.table]), action)
      );

    case INSTITUCIONAL_TOTAL_ESTUDIANTES_UPDATE:
      return state.set(
        'total_estudiantes',
        totalEstudiantes(state.get('total_estudiantes'), action)
      );

    case INSTITUCIONAL_SEDE_ADD:
    case INSTITUCIONAL_SEDE_UPDATE:
    case INSTITUCIONAL_SEDE_REMOVE:
      return state.set('sedes', sedes(state.get('sedes'), action));

    case INSTITUCIONAL_SEDE_CARRERA_ADD:
    case INSTITUCIONAL_SEDE_CARRERA_UPDATE:
    case INSTITUCIONAL_SEDE_CARRERA_REMOVE:
      return state
        .set('sedes', sedes(state.get('sedes'), action))
        .set(
          'total_estudiantes',
          totalEstudiantes(state.get('total_estudiantes'), action)
        );

    default:
      return state;
  }
};
