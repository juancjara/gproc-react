import { fromJS, List } from 'immutable';

import actions from '../../constants/expresionInteres';
const {
  CARRERA_FILL_EMPTY,

  CARRERA_ADD,
  CARRERA_REMOVE,
  CARRERA_UPDATE,

  CARRERA_COMITE_MEMBER_ADD,
  CARRERA_COMITE_MEMBER_REMOVE,
  CARRERA_COMITE_MEMBER_UPDATE,

  CARRERA_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM,
  CARRERA_CUERPO_ACADEMICO_UPDATE_COL_SUM,

  CARRERA_TOTAL_ESTUDIANTES_UPDATE,

  CARRERA_PUBLICACIONES_UPDATE,
  CARRERA_ACTIVIDADES_SERVICIOS_UPDATE,

  CARRERA_VINCULOS_ADD,
  CARRERA_VINCULOS_UPDATE,
  CARRERA_VINCULOS_REMOVE,

  CARRERA_FINANCIAMIENTO_UTILIZACION_UPDATE,
} = actions;

import { findIndexById } from '../../utils';
import carrera from './carrera/carrera';
import status from '../status';

const initialState = List();

export default (
  state = initialState,
  action
) => {

  let index;
  switch (action.type) {
    case CARRERA_ADD:
      const { carreraId: id } = action;
      const newCarrera = carrera(undefined, action);

      index = state.findLastIndex(row => {
        return row.getIn(['data', 'id']) < id;
      });

      return state.splice(index + 1, 0, newCarrera);

    case CARRERA_REMOVE:
      index = findIndexById(state, action.carreraId);
      //if (index === -1) return state;
      return state.delete(index);

    case CARRERA_FILL_EMPTY:
    case CARRERA_UPDATE:
    case CARRERA_COMITE_MEMBER_ADD:
    case CARRERA_COMITE_MEMBER_REMOVE:
    case CARRERA_COMITE_MEMBER_UPDATE:
    case CARRERA_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM:
    case CARRERA_CUERPO_ACADEMICO_UPDATE_COL_SUM:
    case CARRERA_TOTAL_ESTUDIANTES_UPDATE:
    case CARRERA_PUBLICACIONES_UPDATE:
    case CARRERA_ACTIVIDADES_SERVICIOS_UPDATE:
    case CARRERA_VINCULOS_ADD:
    case CARRERA_VINCULOS_UPDATE:
    case CARRERA_VINCULOS_REMOVE:
    case CARRERA_FINANCIAMIENTO_UTILIZACION_UPDATE:
      index = findIndexById(state, action.carreraId);
      //if (index === -1) return state;
      return state
        .setIn(
          [index, 'data'],
          carrera(state.getIn([index, 'data']), action)
        )
        .setIn(
          [index, 'status', 'hasChanged'],
          true
        );

    default:
      return state;
  }
};
