import { Map } from 'immutable';

import statusActions from '../constants/status';
const {
  NODO_VALIDATION_ERROR,
  NODO_STORED,
  NODO_FETCHING,
  NODO_SERVER_ERROR,
} = statusActions;

export default (
  state = Map(),
  action
) => {
  switch (action.type) {
    case NODO_VALIDATION_ERROR:
      return state
        .set('errors'], action.errors);

    case NODO_FETCHING:
      return state
        .set('status'], 'FETCHING')
        .set('hasChanged'], false);

    case NODO_STORED:
      return state
        .set('status', 'STORED')
        .set('dbId', action.dbId);

    case NODO_SERVER_ERROR:
      return state.set('status', 'SERVER_ERROR');

    default:
      return state;
  }
};
