import { fromJS } from 'immutable';

import actions from '../../../constants/expresionInteres';
const {
  INSTITUCIONAL_TOTAL_ESTUDIANTES_UPDATE,
  INSTITUCIONAL_SEDE_CARRERA_ADD,
  INSTITUCIONAL_SEDE_CARRERA_UPDATE,
  INSTITUCIONAL_SEDE_CARRERA_REMOVE,
} = actions;

import totalEstudiantesTable from './totalEstudiantesTable';

const initialState = fromJS({
  p_admision: totalEstudiantesTable(undefined, {}),
  m_semestre: totalEstudiantesTable(undefined, {}),
  egresados: totalEstudiantesTable(undefined, {}),
  titulados: totalEstudiantesTable(undefined, {}),
});

export default (
  state = initialState,
  action
) => {
  switch (action.type) {
    case INSTITUCIONAL_TOTAL_ESTUDIANTES_UPDATE:
      return state.set(
        action.table,
        totalEstudiantesTable(state.get(action.table), action)
      );

    case INSTITUCIONAL_SEDE_CARRERA_ADD:
    case INSTITUCIONAL_SEDE_CARRERA_REMOVE:
    case INSTITUCIONAL_SEDE_CARRERA_UPDATE:
      const mutable = state.asMutable();
      mutable.forEach((v, k) => {
        mutable.set(k, totalEstudiantesTable(v, action));
      });
      return mutable.asImmutable();

    default:
      return state;
  }
};
