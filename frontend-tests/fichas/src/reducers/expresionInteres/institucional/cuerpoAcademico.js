import _ from 'lodash';
import { fromJS } from 'immutable';

import { toNumber } from '../../../utils';

import actions from '../../../constants/expresionInteres';
const {
  INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM,
  INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_COL_SUM,
  CARRERA_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM,
  CARRERA_CUERPO_ACADEMICO_UPDATE_COL_SUM,
} = actions;

const updateTotal = (
  prevTotal,
  newVal,
  prevValue
) =>
  (toNumber(prevTotal) + toNumber(newVal) - toNumber(prevValue)).toString();

const initialState = fromJS({
  grados_dedicacion: {"pt":{"ex":"","tc":"","tp":"","tt":""},"pe":{"ex":"","tc":"","tp":"","tt":""},"bc":{"ex":"","tc":"","tp":"","tt":""},"lt":{"ex":"","tc":"","tp":"","tt":""},"mm":{"ex":"","tc":"","tp":"","tt":""},"dr":{"ex":"","tc":"","tp":"","tt":""},"tt":{"ex":"","tc":"","tp":"","tt":""}},
  grados_condicion: {"pt":{"no":"","co":"","tt":""},"pe":{"no":"","co":"","tt":""},"bc":{"no":"","co":"","tt":""},"lt":{"no":"","co":"","tt":""},"mm":{"no":"","co":"","tt":""},"dr":{"no":"","co":"","tt":""},"tt":{"no":"","co":"","tt":""}},
  docentes_categoria: {"pr":{"tt":""},"as":{"tt":""},"au":{"tt":""},"tt":{"tt":""}},
});

export default (
  state = initialState,
  action
) => {
  let prevValue;

  switch (action.type) {
    case INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM:
    case CARRERA_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM:
      prevValue = state.getIn([action.row, action.col]);

      return state.asMutable()
        // total col
        .updateIn(
          ['tt', action.col],
          _.partial(updateTotal, _, action.value, prevValue) // (val) => (newVal)
        )
        // total row
        .updateIn(
          [action.row, 'tt'],
          _.partial(updateTotal, _, action.value, prevValue)
        )
        // total sum
        .updateIn(
          ['tt', 'tt'],
          _.partial(updateTotal, _, action.value, prevValue)
        )
        // cell
        .setIn(
          [action.row, action.col],
          action.value.toString()
        )
        .asImmutable();

    case INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_COL_SUM:
    case CARRERA_CUERPO_ACADEMICO_UPDATE_COL_SUM:
      prevValue = state.getIn([action.row, action.col]);

      return state.asMutable()
        // total col
        .updateIn(
          ['tt', action.col],
          _.partial(updateTotal, _, action.value, prevValue)
        )
        // cell
        .setIn(
          [action.row, action.col],
          action.value.toString()
        )
        .asImmutable();

    default:
      return state;
  }
};
