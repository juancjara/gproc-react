import { List } from 'immutable';

import actions from '../../../constants/expresionInteres';
const {
  INSTITUCIONAL_SEDE_ADD,
  INSTITUCIONAL_SEDE_UPDATE,
  INSTITUCIONAL_SEDE_REMOVE,
  INSTITUCIONAL_SEDE_CARRERA_ADD,
  INSTITUCIONAL_SEDE_CARRERA_UPDATE,
  INSTITUCIONAL_SEDE_CARRERA_REMOVE,
} = actions;

import sede from './sede';
import { findIndexBy } from '../../../utils';

const initialState =
  List().push(sede(undefined, {}));

export default (
  state = initialState,
  action
) => {
  switch (action.type) {
    case INSTITUCIONAL_SEDE_ADD:
      return state.push(sede(undefined, action));

    case INSTITUCIONAL_SEDE_REMOVE:
      return state
        .delete(action.sedeIndex)
        .map((sede, i) => {
          return i < action.sedeIndex ?
            sede :
            sede.set('nombre_sede', `Filial ${i}`);
        });

    case INSTITUCIONAL_SEDE_UPDATE:
    case INSTITUCIONAL_SEDE_CARRERA_ADD:
    case INSTITUCIONAL_SEDE_CARRERA_UPDATE:
    case INSTITUCIONAL_SEDE_CARRERA_REMOVE:
      return state.set(
        action.sedeIndex,
        sede(state.get(action.sedeIndex), action)
      );

    default:
      return state;
  }
};
