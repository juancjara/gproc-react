import {
  Map,
} from 'immutable';

import actions from '../../../constants/expresionInteres';
const {
  INSTITUCIONAL_SEDE_CARRERA_ADD,
  INSTITUCIONAL_SEDE_CARRERA_UPDATE,
  INSTITUCIONAL_SEDE_CARRERA_REMOVE,
} = actions;

import { findIndexBy } from '../../../utils';

const findIndexById = (
  state,
  id
) => findIndexBy(state, id, (e) => e.get('id'));

export default (
  state = {},
  action
) => {
  let index;

  switch (action.type) {
    case INSTITUCIONAL_SEDE_CARRERA_ADD:
      const { nombre, area, carreraId: id } = action;
      return state.push(Map({
        nombre,
        area,
        id,
      }));

    case INSTITUCIONAL_SEDE_CARRERA_UPDATE:
      index = findIndexById(state, action.carreraId);
      return state.setIn(
        [index, action.prop],
        action.value
      );

    case INSTITUCIONAL_SEDE_CARRERA_REMOVE:
      index = findIndexById(state, action.carreraId);
      return state.delete(index);

    default:
      return state;
  }
};
