import { fromJS, List } from 'immutable';

import actions from '../../../constants/expresionInteres';
const {
  INSTITUCIONAL_TOTAL_ESTUDIANTES_UPDATE,
  INSTITUCIONAL_SEDE_CARRERA_ADD,
  INSTITUCIONAL_SEDE_CARRERA_UPDATE,
  INSTITUCIONAL_SEDE_CARRERA_REMOVE,
} = actions;

import { findIndexBy } from '../../../utils';

const initialRow = {
  "x2015": {
    "m": "",
    "f": ""
  },
  "x2014": {
    "m": "",
    "f": ""
  },
  "x2013": {
    "m": "",
    "f": ""
  },
  "x2012": {
    "m": "",
    "f": ""
  },
  "x2011": {
    "m": "",
    "f": ""
  },
};

export default (
  state = List(),
  action
) => {
  let index;

  switch (action.type) {
    case INSTITUCIONAL_TOTAL_ESTUDIANTES_UPDATE:
      index = findIndexBy(state, action.row, e => e.get('id'));
      return state.setIn([index, action.col, action.gender], action.value);

    case INSTITUCIONAL_SEDE_CARRERA_ADD:
      const newRow = fromJS({
        id: action.carreraId,
        nombre: `${action.sedeName}: ${action.nombre}`,
        ...initialRow,
      });

      index = state.findLastIndex(row => {
        return row.get('id') < action.carreraId;
      });

      return state.splice(index + 1, 0, newRow);

    case INSTITUCIONAL_SEDE_CARRERA_UPDATE:
      if (action.prop === 'nombre') {
        index = findIndexBy(state, action.carreraId, e => e.get('id'));
        //if (index === -1) return state;
        return state.setIn(
          [index, action.prop],
          `${action.sedeName}: ${action.value}`
        );
      }
      return state;

    case INSTITUCIONAL_SEDE_CARRERA_REMOVE:
      index = findIndexBy(state, action.carreraId, e => e.get('id'));
      //if (index === -1) return state;
      return state.delete(index);

    default:
      return state;
  }
};
