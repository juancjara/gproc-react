import { Map, fromJS, List } from 'immutable';

import { findIndexBy } from '../../../utils';
//import { getContext } from '../../../context';

import actions from '../../../constants/expresionInteres';
const {
  INSTITUCIONAL_COMITE_MEMBER_ADD,
  INSTITUCIONAL_COMITE_MEMBER_REMOVE,
  INSTITUCIONAL_COMITE_MEMBER_UPDATE,
  CARRERA_COMITE_MEMBER_ADD,
  CARRERA_COMITE_MEMBER_REMOVE,
  CARRERA_COMITE_MEMBER_UPDATE,
} = actions;

const findIndexById = (
  state,
  id
) => findIndexBy(state, id, (e) => e.get('id'));

const initialState = /*() =>
  getContext().institucional ?
    fromJS(getContext().aPersona) :*/
    List();

export default (
  state = initialState,
  action
) => {
  let index;

  switch (action.type) {
    case INSTITUCIONAL_COMITE_MEMBER_ADD:
    case CARRERA_COMITE_MEMBER_ADD:
      return state.push(Map({
        id: action.id,
        pers_nombres: '',
        pers_apellidos: '',
        pers_cargo: '',
        pers_correo: '',
        pers_telefono: '',
      }));

    case INSTITUCIONAL_COMITE_MEMBER_UPDATE:
    case CARRERA_COMITE_MEMBER_UPDATE:
      index = findIndexById(state, action.row);
      return state.setIn(
        [index, action.col],
        action.value
      );

    case INSTITUCIONAL_COMITE_MEMBER_REMOVE:
    case CARRERA_COMITE_MEMBER_REMOVE:
      index = findIndexById(state, action.index);
      return state.delete(index);

    default:
      return state;
  }
};
