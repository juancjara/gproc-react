import {
  Map,
  fromJS,
} from 'immutable';

import actions from '../../../constants/expresionInteres';
const {
  INSTITUCIONAL_SEDE_ADD,
  INSTITUCIONAL_SEDE_UPDATE,
  INSTITUCIONAL_SEDE_CARRERA_ADD,
  INSTITUCIONAL_SEDE_CARRERA_UPDATE,
  INSTITUCIONAL_SEDE_CARRERA_REMOVE,

  CARRERA_UPDATE,
} = actions;

import sedeCarreras from './sedeCarreras';

const getInitialState = ({
  name = 'Sede Principal',
  sedeId = 0,
}) =>
  fromJS({
    id: sedeId,
    nombre_sede: name,
    direc: '',
    dist: '',
    prov: '',
    region: '',
    carreras: [],
  });

export default (
  state,
  action
) => {
  if (!state) return getInitialState(action);

  switch (action.type) {
    case INSTITUCIONAL_SEDE_UPDATE:
      return state.set(action.prop, action.value);

    case INSTITUCIONAL_SEDE_CARRERA_ADD:
    case INSTITUCIONAL_SEDE_CARRERA_UPDATE:
    case INSTITUCIONAL_SEDE_CARRERA_REMOVE:
    case CARRERA_UPDATE:
      return state.set(
        'carreras',
        sedeCarreras(state.get('carreras'), action)
      );

    default:
      return state;
  }
};
