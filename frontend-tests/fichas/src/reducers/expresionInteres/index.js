import { fromJS } from 'immutable';

import actions from '../../constants/expresionInteres';
const {
  FICHA_UPDATE,
  FICHA_UPDATE_INST,
  FICHA_UPDATE_CARR,

  INSTITUCIONAL_UPDATE,
  INSTITUCIONAL_COMITE_MEMBER_ADD,
  INSTITUCIONAL_COMITE_MEMBER_UPDATE,
  INSTITUCIONAL_COMITE_MEMBER_REMOVE,
  INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM,
  INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_COL_SUM,
  INSTITUCIONAL_TOTAL_ESTUDIANTES_UPDATE,
  INSTITUCIONAL_SEDE_ADD,
  INSTITUCIONAL_SEDE_UPDATE,
  INSTITUCIONAL_SEDE_REMOVE,
  INSTITUCIONAL_SEDE_CARRERA_ADD,
  INSTITUCIONAL_SEDE_CARRERA_UPDATE,
  INSTITUCIONAL_SEDE_CARRERA_REMOVE,

  CARRERA_FILL_EMPTY,

  CARRERA_ADD,
  CARRERA_REMOVE,
  CARRERA_UPDATE,

  CARRERA_COMITE_MEMBER_ADD,
  CARRERA_COMITE_MEMBER_REMOVE,
  CARRERA_COMITE_MEMBER_UPDATE,

  CARRERA_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM,
  CARRERA_CUERPO_ACADEMICO_UPDATE_COL_SUM,

  CARRERA_TOTAL_ESTUDIANTES_UPDATE,

  CARRERA_PUBLICACIONES_UPDATE,
  CARRERA_ACTIVIDADES_SERVICIOS_UPDATE,

  CARRERA_VINCULOS_ADD,
  CARRERA_VINCULOS_UPDATE,
  CARRERA_VINCULOS_REMOVE,

  CARRERA_FINANCIAMIENTO_UTILIZACION_UPDATE,
} = actions;

import nodoActions from '../../constants/status';
const {
  NODO_VALIDATION_ERROR,
  NODO_FETCHING,
  NODO_STORED,
  NODO_SERVER_ERROR,

  UPDATE_REQUEST_TYPE,
} = nodoActions;

import ins from './institucional';
import fic from './ficha';
import carreras from './carreras';
import status from '../status';
import { combineImmutableReducers } from '../../utils';

const initialState = fromJS({
  fic: {
    dbId: null,
    data: fic(undefined, {}),
    status: status(undefined, {}),
  },
  ins: {
    dbId: null,
    data: ins(undefined, {}),
    status: status(undefined, {}),
  },
  car: carreras(undefined, {}),
});

export default (
  state = initialState,
  action
) => {
  let newState = state;

  switch (action.type) {
    case UPDATE_REQUEST_TYPE:
      return newState.set('requestType', action.request);

    case NODO_STORED:
      newState = newState
        .setIn([...action.path, 'dbId'], action.dbId);
    case NODO_VALIDATION_ERROR:
    case NODO_FETCHING:
    case NODO_SERVER_ERROR:
      return newState
        .setIn(
          [...action.path, 'status'],
          status(state.getIn([...action.path, 'status']), action)
        );

    case FICHA_UPDATE:
    case FICHA_UPDATE_INST:
    case FICHA_UPDATE_CARR:
      return state
        .setIn(
          ['fic', 'data'],
          fic(state.getIn(['fic', 'data']), action)
        )
        .setIn(
          ['fic', 'status', 'hasChanged'],
          true
        );

    case INSTITUCIONAL_SEDE_CARRERA_ADD:
    case INSTITUCIONAL_SEDE_CARRERA_REMOVE:
    case INSTITUCIONAL_UPDATE:
    case INSTITUCIONAL_COMITE_MEMBER_ADD:
    case INSTITUCIONAL_COMITE_MEMBER_UPDATE:
    case INSTITUCIONAL_COMITE_MEMBER_REMOVE:
    case INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM:
    case INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_COL_SUM:
    case INSTITUCIONAL_TOTAL_ESTUDIANTES_UPDATE:
    case INSTITUCIONAL_SEDE_ADD:
    case INSTITUCIONAL_SEDE_UPDATE:
    case INSTITUCIONAL_SEDE_REMOVE:
    case INSTITUCIONAL_SEDE_CARRERA_UPDATE:
      return state
        .setIn(
          ['ins', 'data'],
          ins(state.getIn(['ins', 'data']), action)
        )
        .setIn(
          ['ins', 'status', 'hasChanged'],
          true
        );

    case CARRERA_FILL_EMPTY:
    case CARRERA_ADD:
    case CARRERA_REMOVE:
    case CARRERA_UPDATE:
    case CARRERA_COMITE_MEMBER_ADD:
    case CARRERA_COMITE_MEMBER_REMOVE:
    case CARRERA_COMITE_MEMBER_UPDATE:
    case CARRERA_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM:
    case CARRERA_CUERPO_ACADEMICO_UPDATE_COL_SUM:
    case CARRERA_TOTAL_ESTUDIANTES_UPDATE:
    case CARRERA_PUBLICACIONES_UPDATE:
    case CARRERA_ACTIVIDADES_SERVICIOS_UPDATE:
    case CARRERA_VINCULOS_ADD:
    case CARRERA_VINCULOS_UPDATE:
    case CARRERA_VINCULOS_REMOVE:
    case CARRERA_FINANCIAMIENTO_UTILIZACION_UPDATE:
      return state
        .set('car', carreras(state.get('car'), action));

    default:
      return state;
  }
};
