import actions from '../../../constants/expresionInteres';
const {
  CARRERA_TOTAL_ESTUDIANTES_UPDATE,
} = actions;

export default (
  state = [],
  action
) => {
  switch (action.type) {
    case CARRERA_TOTAL_ESTUDIANTES_UPDATE:
      return state.setIn(
        [action.row, action.col, action.gender],
        action.value
      );

    default:
      return state;
  }
};
