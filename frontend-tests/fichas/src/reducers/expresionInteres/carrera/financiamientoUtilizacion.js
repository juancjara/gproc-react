import _ from 'lodash';
import {
  fromJS,
} from 'immutable';

import { rowHeadersYears } from '../../../constants/expresionInteres';
import {
  toNumber,
  getKeys,
  fillTableEmptyCellsWithNA,
} from '../../../utils';

import actions from '../../../constants/expresionInteres';
const {
  CARRERA_FILL_EMPTY,
  CARRERA_FINANCIAMIENTO_UTILIZACION_UPDATE,
} = actions;

const updateTotal = (
  prevTotal,
  newVal,
  prevValue
) =>
  (toNumber(prevTotal) + toNumber(newVal) - toNumber(prevValue)).toString();

const getPercentage = (
  value,
  total
) => (toNumber(value) / (toNumber(total) || 1) * 100).toFixed(2).toString();

export default (
  state = {},
  action
) => {
  switch (action.type) {
    case CARRERA_FINANCIAMIENTO_UTILIZACION_UPDATE:
      const mutable = state.toJS();
      const prevValue = mutable[action.row][action.col];
      const percentageCol = action.col.replace(/.$/, 'p');
      const newTotal = updateTotal(
        mutable.tt[action.col],
        action.value,
        prevValue
      );
      const rowIds = _.keysIn(mutable)
        .filter(k => k !== 'tt');

      mutable[action.row][action.col] = action.value;
      rowIds.forEach(rowId => {
        mutable[rowId][percentageCol] =
          getPercentage(mutable[rowId][action.col], newTotal);
      });

      mutable.tt[action.col] = newTotal;
      mutable.tt[percentageCol] = '100.00';

      return fromJS(mutable);

    case CARRERA_FILL_EMPTY:
      const rows = state
        .map((v, k) => k)
        .toJS();

      const rowKeys = _(rows).keys().dropRight().value();
      const colKeys = getKeys(_.take(rowHeadersYears, 2))
        .map(v => v + 'n');

      return fillTableEmptyCellsWithNA({ table: state, rowKeys, colKeys});

    default:
      return state;
  }
};
