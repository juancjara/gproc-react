import { Map, List } from 'immutable';

import { findIndexBy, fillTableEmptyCellsWithNA } from '../../../utils';

import actions from '../../../constants/expresionInteres';
const {
  CARRERA_FILL_EMPTY,
  CARRERA_VINCULOS_ADD,
  CARRERA_VINCULOS_UPDATE,
  CARRERA_VINCULOS_REMOVE,
} = actions;

const findIndexById = (
  state,
  id
) => findIndexBy(state, id, (e) => e.get('id'));

export default (
  state = List(),
  action
) => {
  let index;

  switch (action.type) {
    case CARRERA_VINCULOS_ADD:
      return state.push(Map({
        id: action.id,
        ic: '',
        dv: '',
      }));

    case CARRERA_VINCULOS_UPDATE:
      index = findIndexById(state, action.row);
      return state.setIn([index, action.prop], action.value);

    case CARRERA_VINCULOS_REMOVE:
      index = findIndexById(state, action.row);
      return state.delete(index);

    case CARRERA_FILL_EMPTY:
      const rowKeys = state.map((m, i) => i);
      const colKeys = ['ic', 'dv'];
      return fillTableEmptyCellsWithNA({ table: state, rowKeys, colKeys });

    default:
      return state;
  }
};
