import { fromJS } from 'immutable';

import actions from '../../../constants/expresionInteres';
const {

  CARRERA_FILL_EMPTY,
  CARRERA_UPDATE,

  CARRERA_COMITE_MEMBER_ADD,
  CARRERA_COMITE_MEMBER_REMOVE,
  CARRERA_COMITE_MEMBER_UPDATE,

  CARRERA_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM,
  CARRERA_CUERPO_ACADEMICO_UPDATE_COL_SUM,

  CARRERA_TOTAL_ESTUDIANTES_UPDATE,

  CARRERA_PUBLICACIONES_UPDATE,
  CARRERA_ACTIVIDADES_SERVICIOS_UPDATE,

  CARRERA_VINCULOS_ADD,
  CARRERA_VINCULOS_UPDATE,
  CARRERA_VINCULOS_REMOVE,

  CARRERA_FINANCIAMIENTO_UTILIZACION_UPDATE,
} = actions;

import comite from '../institucional/comite';
import cuerpoAcademico from '../institucional/cuerpoAcademico';
import totalEstudiantes from './totalEstudiantes';
import publicacion from './publicacion';
import actividadesServicios from './actividadesServicios';
import vinculos from './vinculos';
import financiamientoUtilizacion from './financiamientoUtilizacion';
import status from '../../status';

const getInitialState = ({
  dbId = null,
  carreraId = 0,
}) => fromJS({
  dbId,
  data: {
    nombre: '',
    area: '',
    id: carreraId,
    comite: comite(undefined, {}),
    estudiantes: {"2015":{"pa":{"m":"","f":""},"ms":{"m":"","f":""},"eg":{"m":"","f":""},"ti":{"m":"","f":""}},"2011":{"pa":{"m":"","f":""},"ms":{"m":"","f":""},"eg":{"m":"","f":""},"ti":{"m":"","f":""}},"2012":{"pa":{"m":"","f":""},"ms":{"m":"","f":""},"eg":{"m":"","f":""},"ti":{"m":"","f":""}},"2013":{"pa":{"m":"","f":""},"ms":{"m":"","f":""},"eg":{"m":"","f":""},"ti":{"m":"","f":""}},"2014":{"pa":{"m":"","f":""},"ms":{"m":"","f":""},"eg":{"m":"","f":""},"ti":{"m":"","f":""}}},
    cuerpo_academico: cuerpoAcademico(undefined, {}),
    inves: publicacion(undefined, {}),
    publi: publicacion(undefined, {}),
    paten: publicacion(undefined, {}),
    actividades: actividadesServicios(undefined, {}),
    servicios: actividadesServicios(undefined, {}),
    vinculos: vinculos(undefined, {}),
    origen_fondos: {"pao":{"2015n":"","2015p":"","2014n":"","2014p":""},"rdr":{"2015n":"","2015p":"","2014n":"","2014p":""},"itd":{"2015n":"","2015p":"","2014n":"","2014p":""},"otr":{"2015n":"","2015p":"","2014n":"","2014p":""},"tt":{"2015n":"","2015p":"","2014n":"","2014p":""}},
    destino_fondos: {"s1":{"2015n":"","2015p":"","2014n":"","2014p":""},"s2":{"2015n":"","2015p":"","2014n":"","2014p":""},"bc":{"2015n":"","2015p":"","2014n":"","2014p":""},"ob":{"2015n":"","2015p":"","2014n":"","2014p":""},"ou":{"2015n":"","2015p":"","2014n":"","2014p":""},"tt":{"2015n":"","2015p":"","2014n":"","2014p":""}},
  },
  status: status(undefined, {}),
});

export default (
  state,
  action
) => {
  if (!state) {
    return getInitialState(action);
  }

  switch (action.type) {
    case CARRERA_UPDATE:
      return state.set(action.prop, action.value);

    case CARRERA_COMITE_MEMBER_ADD:
    case CARRERA_COMITE_MEMBER_REMOVE:
    case CARRERA_COMITE_MEMBER_UPDATE:
      return state.set(
        'comite',
        comite(state.get('comite'), action)
      );

    case CARRERA_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM:
    case CARRERA_CUERPO_ACADEMICO_UPDATE_COL_SUM:
      return state.setIn(
        ['cuerpo_academico', action.table],
        cuerpoAcademico(state.getIn(['cuerpo_academico', action.table]), action)
      );

    case CARRERA_TOTAL_ESTUDIANTES_UPDATE:
      return state.set(
        'estudiantes',
        totalEstudiantes(state.get('estudiantes'), action)
      );

    case CARRERA_PUBLICACIONES_UPDATE:
      return state.set(
        action.table,
        publicacion(state.get(action.table), action)
      );

    case CARRERA_ACTIVIDADES_SERVICIOS_UPDATE:
      return state.set(
        action.table,
        actividadesServicios(state.get(action.table), action)
      );

    case CARRERA_VINCULOS_ADD:
    case CARRERA_VINCULOS_UPDATE:
    case CARRERA_VINCULOS_REMOVE:
      return state.set(
        'vinculos',
        vinculos(state.get('vinculos'), action)
      );

    case CARRERA_FINANCIAMIENTO_UTILIZACION_UPDATE:
      return state.set(
        action.table,
        financiamientoUtilizacion(state.get(action.table), action)
      );

    case CARRERA_FILL_EMPTY:
    return state
      .set('destino_fondos', financiamientoUtilizacion(state.get('destino_fondos'), action))
      .set('origen_fondos', financiamientoUtilizacion(state.get('origen_fondos'), action))
      .set('actividades', actividadesServicios(state.get('actividades'), action))
      .set('servicios', actividadesServicios(state.get('servicios'), action))
      .set('vinculos', vinculos(state.get('vinculos'), action))
      .set('inves', publicacion(state.get('inves'), action))
      .set('publi', publicacion(state.get('publi'), action))
      .set('paten', publicacion(state.get('paten'), action));

    default:
      return state;
  }
};
