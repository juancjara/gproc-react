import { fromJS } from 'immutable';

import { fillObjEmptyFieldsWithNA } from '../../../utils';

import actions from '../../../constants/expresionInteres';
const {
  CARRERA_FILL_EMPTY,
  CARRERA_ACTIVIDADES_SERVICIOS_UPDATE,
} = actions;

const initialState = fromJS({
  "2011": "",
  "2012": "",
  "2013": "",
  "2014": "",
  "2015": ""
});

export default (
  state = initialState,
  action
) => {
  switch (action.type) {
    case CARRERA_ACTIVIDADES_SERVICIOS_UPDATE:
      return state.set(action.row, action.value);

    case CARRERA_FILL_EMPTY:
      return fillObjEmptyFieldsWithNA(state);

    default:
      return state;
  }
};
