import { fromJS } from 'immutable';
import _ from 'lodash';

import actions from '../../../constants/expresionInteres';
const {
  CARRERA_FILL_EMPTY,
  CARRERA_PUBLICACIONES_UPDATE,
} = actions;

import { rowHeadersYears } from '../../../constants/expresionInteres';
import { headers }
  from '../../../components/expresionInteres/carrera/Publicaciones';

import { fillTableEmptyCellsWithNA, getKeys } from '../../../utils/index';

const initialState = fromJS({"2015":{"ti":"","ac":"","in":""},"2011":{"ti":"","ac":"","in":""},"2012":{"ti":"","ac":"","in":""},"2013":{"ti":"","ac":"","in":""},"2014":{"ti":"","ac":"","in":""}});


export default (
  state = initialState,
  action
) => {
  switch (action.type) {
    case CARRERA_PUBLICACIONES_UPDATE:
      return state.setIn([action.row, action.col], action.value);

    case CARRERA_FILL_EMPTY:
      const rowKeys = getKeys(rowHeadersYears);
      const colKeys = _(headers).pluck('key').drop().value();
      return fillTableEmptyCellsWithNA({table: state, rowKeys, colKeys });

    default:
      return state;
  }
};
