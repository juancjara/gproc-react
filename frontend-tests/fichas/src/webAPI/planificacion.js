import addToQueue from '../utils/queue';

const baseURL = '/ApiPlanificacion/';

export const APIaddActividad = (
  data,
  onSuccess,
  onError
) =>
  addToQueue({
    url: baseURL + 'addActividad' + location.search,
    data,
    onSuccess,
    onError,
  });

export const APIremoveActividad = (
  data,
  onSuccess,
  onError
) =>
  addToQueue({
    url: baseURL + 'removeActividad' + location.search,
    data,
    onSuccess,
    onError,
  });

export const APIstoreNodos = (
  data,
  onSuccess,
  onError
) =>
  addToQueue({
    url: baseURL + 'storeNodos' + location.search,
    data,
    onSuccess,
    onError,
  });

  export const APIfinish = () => {
    //location.href = '/expresionInteres/upload' + location.search;

    const form = document.createElement('form');
    form.action = '/planificacion/finish' + location.search;
    form.method = 'post';
    document.querySelector('body').appendChild(form);
    form.submit();
  };
