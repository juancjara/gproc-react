import addToQueue from '../utils/queue';

export const APIstoreNodo = (
  data,
  onSuccess,
  onError
) =>
  addToQueue({
    url: '/ApiExpInteres/storeNodo' + location.search,
    data,
    onSuccess,
    onError,
  });

export const APIaddCarrera = (
  data,
  onSuccess,
  onError
) =>
  addToQueue({
    url: '/ApiExpInteres/addCarrera' + location.search,
    data,
    onSuccess,
    onError,
  });

export const APIremoveCarrera = (
  data,
  onSuccess,
  onError
) => {
  addToQueue({
    url: '/ApiExpInteres/removeCarrera' + location.search,
    data,
    onSuccess,
    onError,
  });
};

export const APIremoveSede = (
  data,
  onSuccess,
  onError
) => {
  addToQueue({
    url: '/ApiExpInteres/removeSede' + location.search,
    data,
    onSuccess,
    onError,
  });
};

export const APIfinish = () => {
  //location.href = '/expresionInteres/upload' + location.search;

  const form = document.createElement('form');
  form.action = '/expresionInteres/upload' + location.search;
  form.method = 'post';
  document.querySelector('body').appendChild(form);
  form.submit();
};
