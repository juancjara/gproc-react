import addToQueue from '../utils/queue';

const baseURL = '/ApiPresupuesto/';

export const APIuploadFile = baseURL + 'uploadFile' + location.search;

export const APIaddSubActividad = (
  data,
  onSuccess,
  onError
) =>
  addToQueue({
    url: baseURL + 'addSubActividad' + location.search,
    data,
    onSuccess,
    onError,
  });

export const APIremoveSubActividad = (
  data,
  onSuccess,
  onError
) =>
  addToQueue({
    url: baseURL + 'removeSubActividad' + location.search,
    data,
    onSuccess,
    onError,
  });

export const APIstoreNodos = (
  data,
  onSuccess,
  onError
) =>
  addToQueue({
    url: baseURL + 'storeNodos' + location.search,
    data,
    onSuccess,
    onError,
  });

  export const APIfinish = () => {
    //location.href = '/presupuesto/upload' + location.search;

    const form = document.createElement('form');
    form.action = '/presupuesto/finish' + location.search;
    form.method = 'post';
    document.querySelector('body').appendChild(form);
    form.submit();
  };
