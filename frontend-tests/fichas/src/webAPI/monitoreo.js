import addToQueue from '../utils/queue';

const baseURL = '/ApiMonitoreo/';

export const APIaddIndicador = (
  data,
  onSuccess,
  onError
) =>
  addToQueue({
    url: baseURL + 'addIndicador' + location.search,
    data,
    onSuccess,
    onError,
  });

export const APIremoveIndicador = (
  data,
  onSuccess,
  onError
) =>
  addToQueue({
    url: baseURL + 'removeIndicador' + location.search,
    data,
    onSuccess,
    onError,
  });

export const APIstoreNodos = (
  data,
  onSuccess,
  onError
) =>
  addToQueue({
    url: baseURL + 'storeNodos' + location.search,
    data,
    onSuccess,
    onError,
  });

export const APIfinish = () => {
  const form = document.createElement('form');
  form.action = '/monitoreo/finish' + location.search;
  form.method = 'post';
  document.querySelector('body').appendChild(form);
  form.submit();
};
