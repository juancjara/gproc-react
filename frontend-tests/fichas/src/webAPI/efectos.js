import {
  createAPI,
  createAPIfinish,
} from './common';

const baseURL = '/ApiEfectos/';

export const APIstoreNodos = createAPI(baseURL + 'storeNodos');

export const APIfinish = createAPIfinish('efectos/finish');
