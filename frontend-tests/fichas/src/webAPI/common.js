import addToQueue from '../utils/queue';

export const createAPI = url =>
  (data, onSuccess, onError) =>
    addToQueue({
      url: url + location.search,
      data,
      onSuccess,
      onError,
    });

export const createAPIfinish = url =>
  () => {
    const form = document.createElement('form');
    form.action = url + location.search;
    form.method = 'post';
    document.querySelector('body').appendChild(form);
    form.submit();
  };
