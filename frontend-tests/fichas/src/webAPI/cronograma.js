import {
  createAPI,
  createAPIfinish,
} from './common';

const baseURL = '/ApiCronograma/';

export const APIstoreNodos = createAPI(baseURL + 'storeNodos');

export const APIfinish = createAPIfinish('cronograma/finish');
