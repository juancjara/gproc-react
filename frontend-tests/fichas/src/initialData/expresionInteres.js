import _ from 'lodash';
import {
  fromJS,
  List,
  Map,
} from 'immutable';

import rootReducer from '../reducers/expresionInteres';
import carrera from '../reducers/expresionInteres/carrera/carrera';

export const createBaseStructure = () => rootReducer(undefined, {});

const nodes = ['fic', 'ins', 'car'];

export const anyMissingNode = (nodes, data) =>
  _.any(nodes, key => (!data.has(key) || !data.get(key)));

export const fillMissingNodes = (nodes, data) =>
  nodes
    .reduce((acc, key) =>
      (acc.has(key) && acc.get(key)) ?
        acc :
        acc.set(key, rootReducer(undefined, {}).get(key))
      , data);

export const shouldAddOneCarrera = (context, data) =>
  !context.institucional && !data.get('car').size;

export const addCarrera = () => List().push(carrera(undefined, {}));

const applyCommonContext = (
  context,
  data
) =>
  data
    .setIn(['fic', 'data', 'moda_nombre'], context.moda_nombre)
    .setIn(['ins', 'data', 'inst_nombre'], context.inst_nombre)
    .setIn(['ins', 'data', 'inst_gestion'], context.moda_nombre);

const applyContextWhenEditable = (
  context,
  data
) =>
  data
    .setIn(['fic', 'data', 'datos', 'inst', 'date'], context.date)
    .setIn(['fic', 'data', 'datos', 'inst', 'date_local'], context.date_local)
    .setIn(['fic', 'data', 'datos', 'carr', 'date'], context.date)
    .setIn(['fic', 'data', 'datos', 'carr', 'date_local'], context.date_local);

const applyContextInstitucional = (
  context,
  initialData
) => {
  let data = initialData;
  data = data
    .deleteIn(['fic', 'data', 'datos', 'carr']);

  if (context.editable) {
    data = data
      .setIn(
        ['ins', 'data', 'comite'],
        fromJS(context.aPersona)
      );
  }

  return data;
};

const applyContextNoInstitucional = (
  context,
  initialData
) => {
  let data = initialData;
  if (context.editable) {
    data = data.setIn(
      ['car', 0, 'data', 'comite'],
      fromJS(context.aPersona)
    );
  }
  data = data.setIn(['car', 0, 'data', 'nombre'], context.carr_nombre);
  return data;
};

const applyContextNoUniversidad = (
  context,
  data
) =>
  data
    .deleteIn(['ins', 'data', 'cuerpo_academico', 'docentes_categoria'])
    .setIn(['ins', 'data', 'inst_subtipo'], context.inst_subtipo || '-')

export default (
  initialData,
  context
) => {
  let data = fromJS(initialData);

  if (!data) {
    data = createBaseStructure();
  }

  if (anyMissingNode(nodes, data)) {
    data = fillMissingNodes(nodes, data);
  }

  if (shouldAddOneCarrera(context, data)) {
    data = data.set('car', addCarrera());
  }

  data = applyCommonContext(context, data);

  if (context.editable) {
    data = applyContextWhenEditable(context, data);
  }

  if (context.institucional) {
    data = applyContextInstitucional(context, data);
  }

  if (!context.institucional) {
    data = applyContextNoInstitucional(context, data);
  }

  if (!context.universidad) {
    data = applyContextNoUniversidad(context, data);
  }

  return data;
};
