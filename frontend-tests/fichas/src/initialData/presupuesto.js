import { fromJS } from 'immutable';

import { anyMissingNode, fillMissingNodes } from './planificacion';
import status from '../reducers/status';
import fic from '../reducers/planificacion/ficha';

export const applyDateContext = (
  context,
  data
) =>
  data
  .setIn(['fic', 'data', 'firmas', 'com', 'date'], context.date)
  .setIn(['fic', 'data', 'firmas', 'com', 'date_local'], context.date_local)
  .setIn(['fic', 'data', 'firmas', 'dir', 'date'], context.date)
  .setIn(['fic', 'data', 'firmas', 'dir', 'date_local'], context.date_local);

const addDetalleTableStatus = data =>
  data
    .setIn(['totales', 'status'], status(undefined, {}));

const fillDataWithActividades = (data, context) => {
  const actividades = fromJS(context.act)
    .map(act => act.set('status', status(undefined, {})));
  return data.set('act', actividades);
};

const nodes = ['fic'];
const reducers = { fic };

export default (initialData, context) => {
  let data = fromJS(initialData);

  if (anyMissingNode(nodes, data)) {
    data = fillMissingNodes(nodes, data, reducers);
  }

  if (context.editable) {
    data = applyDateContext(context, data);
  }

  data = addDetalleTableStatus(data);

  data = fillDataWithActividades(data, context);

  console.log(data.toJS());
  return data;
};
