import { fromJS } from 'immutable';

import { applyDateContext } from './presupuesto';
import {
  anyMissingNode,
  fillMissingNodes,
} from './planificacion';
import status from '../reducers/status';

import fic from '../reducers/planificacion/ficha';
import indicador from '../reducers/monitoreo/indicador/indicador';

import {
  findIndexBy,
} from '../utils';

const nodes = ['fic'];
const reducers = { fic };

export const anyMissingDataIndicadores = data =>
  _.any(
    _.times(data.get('ind').size),
    idx => !data.hasIn(['ind', idx, 'data']) ||
      !data.getIn(['ind', idx, 'data'])
  );

export const fillMissingDataIndicadores = data =>
  _.reduce(
    _.times(data.get('ind').size),
    (acc, idx) =>
      (acc.hasIn(['ind', idx, 'data']) && acc.getIn(['ind', idx, 'data'])) ?
      acc:
      acc
      .setIn(['ind', idx, 'data'], indicador(undefined, {}))
      .setIn(['ind', idx, 'status'], status(undefined, {})),
    data
  );

export const removeMissingIndicadores = (
  context,
  data
) => {
  const indicadores = data.get('ind')
    .map(ind =>
      ind.updateIn(
        ['data', 'medios'],
        medios => medios
          .filter(m => {
            const index = findIndexBy(
              context.medios,
              m.get('conc_id'),
              medio => medio.conc_id
            );
            return m.get('conc_id') === 'otro' || index > - 1;
          })
      )
    );

  return data.set('ind', indicadores);
};

export const fillDataWithFactores = (
  context,
  data
) => {
  const factores = fromJS(context.factores)
    .map(factor => factor.set('status', status(undefined, {})));

  return data.set('fac', factores);
};

const getCountByMedio = (
  groupedUsedMedios,
  conc_id
) =>
  groupedUsedMedios.has(conc_id) ?
    groupedUsedMedios.get(conc_id).size:
    0;

const extractMediosFromContext = (
  context,
  data
) => {
  const groupedUsedMedios = data.get('ind')
    .flatMap(ind => ind.getIn(['data', 'medios']))
    .groupBy(medio => medio.get('conc_id'));

  const medios = _.map(
    context.medios,
    (medio, i) => ({
      id: medio.conc_id,
      count: getCountByMedio(groupedUsedMedios, medio.conc_id),
        ...medio,
    })
  );

  return data.set('medios', fromJS(medios));
};

export default (initialData, context) => {
  let data = fromJS(initialData);

  if (anyMissingNode(nodes, data)) {
    data = fillMissingNodes(nodes, data, reducers);
  }

  if (anyMissingDataIndicadores(data)) {
    data = fillMissingDataIndicadores(data);
  }

  if (context.editable) {
    data = applyDateContext(context, data);
  }

  data = fillDataWithFactores(context, data);

  data = removeMissingIndicadores(context, data);

  data = extractMediosFromContext(context, data);


  console.log(data.toJS());

  return data;
};
