import { fromJS } from 'immutable';

import { applyDateContext } from './presupuesto';
import { fillDataWithFactores } from './monitoreo';

import {
  anyMissingNode,
  fillMissingNodes,
} from './planificacion';

import status from '../reducers/status';
import fic from '../reducers/planificacion/ficha';
import efecto from '../reducers/efectos/efecto';

const nodes = ['fic'];
const reducers = { fic };

export const anyMissingDataEfectos = data =>
  _.any(
    _.times(data.get('efe').size),
    idx => !data.hasIn(['efe', idx, 'data']) ||
      !data.getIn(['efe', idx, 'data'])
  );

export const fillMissingDataEfectos = data =>
  _.reduce(
    _.times(data.get('efe').size),
    (acc, idx) =>
      (acc.hasIn(['efe', idx, 'data']) && acc.getIn(['efe', idx, 'data'])) ?
      acc:
      acc
      .setIn(['efe', idx, 'data'], efecto(undefined, {}))
      .setIn(['efe', idx, 'status'], status(undefined, {})),
    data
  );


export default (initialData, context) => {
  let data = fromJS(initialData);

  if (anyMissingNode(nodes, data)) {
    data = fillMissingNodes(nodes, data, reducers);
  }

  if (anyMissingDataEfectos(data)) {
    data = fillMissingDataEfectos(data);
  }

  if (context.editable) {
    data = applyDateContext(context, data);
  }

  data = fillDataWithFactores(context, data);

  console.log(data.toJS());
  return data;
};
