import { fromJS } from 'immutable';

import { applyDateContext } from './presupuesto';

import fic from '../reducers/planificacion/ficha';
import cronograma from '../reducers/cronograma/cronograma';
import status from '../reducers/status';

import {
  anyMissingNode,
  fillMissingNodes,
} from './planificacion';

const nodes = ['fic'];
const reducers = { fic };

export const anyMissingDataCronograma = data =>
  _.any(
    _.times(data.get('cro').size),
    idx => !data.hasIn(['cro', idx, 'data']) ||
      !data.getIn(['cro', idx, 'data'])
  );

export const fillMissingDataCronograma = data =>
  _.reduce(
    _.times(data.get('cro').size),
    (acc, idx) =>
      (acc.hasIn(['cro', idx, 'data']) && acc.getIn(['cro', idx, 'data'])) ?
      acc:
      acc
      .setIn(['cro', idx, 'data'], cronograma(undefined, {}))
      .setIn(['cro', idx, 'status'], status(undefined, {})),
    data
  );


export default (
  initialData,
  context
) => {
  let data = fromJS(initialData);

  if (anyMissingNode(nodes, data)) {
    data = fillMissingNodes(nodes, data, reducers);
  }

  if (anyMissingDataCronograma(data)) {
    data = fillMissingDataCronograma(data);
  }

  if (context.editable) {
    data = applyDateContext(context, data);
  }

  console.log(data.toJS());

  return data;
};
