import _ from 'lodash';
import {
  fromJS,
  List,
  Map,
} from 'immutable';

import rootReducer from '../reducers/planificacion';
import fic from '../reducers/planificacion/ficha';
import dim from '../reducers/planificacion/dimensiones';
import fac from '../reducers/planificacion/factores';
import status from '../reducers/status';

export const createBaseStructure = () => rootReducer(undefined, {});

const nodes = ['fic'];
const reducers = { fic };

const hasNotKey = (data, key) => !data.has(key) || !data.get(key);

export const anyMissingNode = (nodes, data) =>
  _.any(nodes, key => !data.hasIn([key, 'data']) || !data.getIn([key, 'data']));

export const fillMissingNodes = (nodes, data, reducers) =>
  nodes
    .reduce((acc, key) =>
      (acc.hasIn([key, 'data']) && acc.getIn([key, 'data'])) ?
        acc :
        acc
            .setIn([key, 'data'], reducers[key](undefined, {}).get('data'))
            .setIn([key, 'status'], status(undefined, {}))
      , data);

export const fillMissingFields = (data, reducer, initialValues) =>
  data.map((d, idx) => {
    let temp = d;
    if (hasNotKey(temp, 'data')) {
      temp = temp
        .set(
          'data',
          reducer(undefined, { cod: initialValues[idx] })
        );
    }
    if (hasNotKey(temp, 'status')) {
      temp = temp
        .set(
          'status',
          status(undefined, {})
        );
    }
    return temp;
  });

const fillMissingFactorFields = (data, context) =>
  data.set(
    'fac',
    fillMissingFields(
      data.get('fac'),
      fac,
      _.pluck(context.factores, 'fact_codigo')
    )
  );

const fillMissingDimensionFields = (data, context) =>
  data.set(
    'dim',
    fillMissingFields(
      data.get('dim'),
      dim,
      _.pluck(context.dimensiones, 'dime_codigo')
    )
  );

export const anyMissingNodeData = data =>
  data.filter(d => !d.has('data') || !d.get('data')).size > 0;

export const anyMissingDimensionData = data =>
  anyMissingNodeData(data.get('dim'));

export const anyMissingFactorData = data =>
  anyMissingNodeData(data.get('fac'));

export const applyContextWhenEditable = (
  context,
  data
) =>
  data
  .setIn(['fic', 'data', 'firmas', 'com', 'date'], context.date)
  .setIn(['fic', 'data', 'firmas', 'com', 'date_local'], context.date_local)
  .setIn(['fic', 'data', 'firmas', 'dir', 'date'], context.date)
  .setIn(['fic', 'data', 'firmas', 'dir', 'date_local'], context.date_local);

const getCountByEstandar = (
  groupedUsedEstandares,
  esta_id
) =>
  groupedUsedEstandares.has(esta_id) ?
    groupedUsedEstandares.get(esta_id).size:
    0;

const extractEstandaresFromContext = (
  data,
  context
) => {
  const groupedUsedEstandares = data.get('act')
    .flatMap(a => a.getIn(['data', 'estandares']))
          .groupBy(estandar => estandar.get('esta_id'));

  const estandares = _.map(
    context.estandares,
    (estandar, i) => ({
      index: i,
      count: getCountByEstandar(groupedUsedEstandares, estandar.esta_id),
      ...estandar,
    })
  );
  return data.set('estandares', fromJS(estandares));
};

export default (
  initialData,
  context
) => {
  let data = fromJS(initialData);

  if (!data) {
    data = createBaseStructure();
  }

  if (anyMissingNode(nodes, data)) {
    data = fillMissingNodes(nodes, data, reducers);
  }

  if (anyMissingDimensionData(data)) {
    data = fillMissingDimensionFields(data, context);
  }

  if (anyMissingFactorData(data)) {
    data = fillMissingFactorFields(data, context);
  }

  if (context.canEdit) {
    data = applyContextWhenEditable(context, data);
  }

  data = extractEstandaresFromContext(data, context);

  console.log(data.toJS());
  return data;
};
