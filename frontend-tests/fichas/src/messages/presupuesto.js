export const ADD_SUBACTIVIDAD_OK = 'Subactividad creada.';

export const REMOVE_SUBACTIVIDAD_OK = 'Subactividad eliminada.';

export const CONFIRM_REMOVE_SUBACTIVIDAD = '¿Está seguro que desea eliminar la subactividad?';
