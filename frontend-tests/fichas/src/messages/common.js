export const ERROR_NUMERIC_FIELD = 'Valor número o S/D o N/A';
export const ERROR_STRING_NA_SD_FIELD = 'Campo requerido o S/D o N/A';

export const ERROR_EMAIL = "Debe ingresar un email válido";
export const ERROR_TELEFONO = "Debe ingresar un teléfono válido";

export const WARNING_CARRERA_BANNED_NAME = "Diferente al comité de calidad actual";

export const ERROR_ALERT_ON_SAVE = 'Ocurrió un error al guardar. Intente guardar nuevamente.';

export const ERROR_MAX_INACTIVE_TIME_REACHED = 'Usted estuvo inactivo por cerca de una hora y su sesión puede haber expirado. Por favor recargue su navegador.';

export const SAVE_WITH_ERRORS = 'Se guardó la información pero existen errores en la ficha.';
export const ALREADY_SAVED_WITH_ERRORS = 'La información ya ha sido guardada pero existen errores en la ficha.';

export const WARNING_SESSION_WILL_EXPIRE = 'La sesión terminará pronto. Por favor guarde su información.';

export const SAVE_SUCCESS = 'La información se guardó correctamente';
export const ALREADY_SAVED_SUCCESS = 'La información ya ha sido guardada';
