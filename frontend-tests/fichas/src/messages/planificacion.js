export const ADD_ACTIVIDAD_OK = 'Actividad creada.';

export const REMOVE_ACTIVIDAD_OK = 'Actividad eliminada.';

export const CONFIRM_REMOVE_ACTIVIDAD = '¿Está seguro que desea eliminar la actividad?';
