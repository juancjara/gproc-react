export const REQUEST_ADD_INDICADOR_OK = 'Indicador agregado';

export const REQUEST_REMOVE_INDICADOR_OK = 'Indicador elminado';

export const CONFIRM_REMOVE_INDICADOR = '¿Está seguro que desea eliminar el indicador?';
