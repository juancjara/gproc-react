export const CONFIRM_REMOVE_SEDE = 'Remover sede y todas sus carreras?';
export const CONFIRM_REMOVE_CARRERA = 'Remover carrera?';
export const ERROR_REMOVE_FIRST_SEDE = 'No puede remover la sede principal';
export const WARNING_FILL_DATA = 'A PARTIR DE AQUÍ SOLO ES NECESARIO COMPLETAR LA INFORMACIÓN SI ESTÁ SOLICITANDO FINANCIAMIENTO PARA ESTA CARRERA O ESPECIALIDAD.';

export const REQUEST_ADD_CARRERA_OK = 'Carrera agregada';
export const REQUEST_REMOVE_CARRERA_OK = 'Carrera eliminada';
export const REQUEST_REMOVE_SEDE_OK = 'Sede eliminada';
