import keyMirror from 'keymirror';

const actions = keyMirror({
  SUBACTIVIDAD_ADD: null,
  SUBACTIVIDAD_REMOVE: null,
  SUBACTIVIDAD_UPDATE: null,

  SUBACTIVIDAD_ADD_NEXT_SELECT: null,
  SUBACTIVIDAD_LAST_LEVEL_SELECTED: null,
  SUBACTIVIDAD_LEVEL_SELECT: null,

  REQUEST_ADD_SUBACTIVIDAD: null,
  REQUEST_REMOVE_SUBACTIVIDAD: null,
});

export default actions;
