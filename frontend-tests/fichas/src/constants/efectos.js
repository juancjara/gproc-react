import keyMirror from 'keymirror';

const actions = keyMirror({
  EFECTO_UPDATE: null,
});

export default actions;
