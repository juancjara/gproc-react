import keyMirror from 'keymirror';

const actions = keyMirror({
  CRONOGRAMA_UPDATE: null,
});

export default actions;
