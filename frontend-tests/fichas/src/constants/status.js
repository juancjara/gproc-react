import keyMirror from 'keymirror';

const actions = keyMirror({
  NODO_VALIDATION_ERROR: null,
  NODO_FETCHING: null,
  NODO_STORED: null,
  NODO_SERVER_ERROR: null,
  NODO_SUCCESS: null,

  SERVER_ERROR: null,
  FETCHING: null,
  STORED: null,

  STORED_OK: null,
  STORED_CHANGED: null,

  REQUEST_SAVE_ALL: null,
  UPDATE_REQUEST_TYPE: null,
  UNAUTHORIZED_ERROR: null,
});

export default actions;
