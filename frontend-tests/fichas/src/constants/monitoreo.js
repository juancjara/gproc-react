import keyMirror from 'keymirror';

const actions = keyMirror({
  INDICADOR_ADD: null,
  INDICADOR_REMOVE: null,
  INDICADOR_UPDATE: null,

  INDICADOR_MEDIO_ADD: null,
  INDICADOR_MEDIO_REMOVE: null,
  INDICADOR_MEDIO_UPDATE: null,
  INDICADOR_MEDIO_OTRO_UPDATE: null,

  MEDIO_DECREMENT: null,
  MEDIO_INCREMENT: null,

  REQUEST_ADD_INDICADOR: null,
  REQUEST_REMOVE_INDICADOR: null,
});

export default actions;
