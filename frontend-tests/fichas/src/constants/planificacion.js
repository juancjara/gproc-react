import keyMirror from 'keymirror';

const actions = keyMirror({
  FICHA_FIRMA_UPDATE: null,

  DIMENSION_UPDATE: null,

  FACTOR_UPDATE: null,
  FACTOR_TOGGLE_HIDE: null,

  ACTIVIDAD_ADD: null,
  ACTIVIDAD_REMOVE: null,
  ACTIVIDAD_UPDATE: null,

  ACTIVIDAD_ESTANDAR_ADD: null,
  ACTIVIDAD_ESTANDAR_REMOVE: null,
  ACTIVIDAD_ESTANDAR_UPDATE: null,

  ESTANDAR_DECREMENT: null,
  ESTANDAR_INCREMENT: null,

  REQUEST_ADD_ACTIVIDAD: null,
  REQUEST_REMOVE_ACTIVIDAD: null,
  REQUEST_SAVE_ALL: null,
});

export default actions;
