import { pluralize } from '../utils';

export const createMissingEstandaresError = ({
  factorId,
  missing,
}) => ({
  key: factorId,
  message: `${pluralize('Falta', missing)} seleccionar ${missing}
    ${pluralize('estándar', missing)} ${pluralize('obligatorio', missing)}.`,
});

export const createNoActividadesError = ({
  factorId,
}) => ({
  key: factorId,
  message: 'Los resultados no ocultos deben tener al menos una actividad.Recuerda que solo puedes ocultar los resultados que tengan solo estándares opcionales.',
});
