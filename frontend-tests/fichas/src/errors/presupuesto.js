import {
  DETALLE_TABLE,
} from '../titles/presupuesto';

export const createNoSubActividadesError = ({
  actividadDbId,
}) => ({
  key: actividadDbId,
  message: 'La actividade debe tener al menos una subactividad',
});

export const createCustomDetalleTableError = message => ({
  key: DETALLE_TABLE,
  message,
});

export const createMissingFileSubActividadError = ({
  subactividadId,
}) => ({
  key: subactividadId,
  message: 'Debe subir el archivo de especificación',
});

export const createCapacitacionExternaDiasError = ({
  subactividadId,
}) => ({
  key: subactividadId,
  message: 'El número de días de capacitación externa no puede ser mayor a 5.',
});

export const createProfesoreVisitantesDiasError = ({
  subactividadId,
}) => ({
  key: subactividadId,
  message: 'El número de días de profesores visitantes no puede ser mayor a 15.',
});
