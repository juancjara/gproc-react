import { pluralize } from '../utils';

export const createMissingMediosError = ({
  factorId,
  missing,
}) => ({
  key: factorId,
  message: `${pluralize('Falta', missing)} seleccionar ${missing}
    ${pluralize('medio', missing)} ${pluralize('obligatorio', missing)}.`,
});

export const createNoIndicadoresError = ({
  factorId,
}) => ({
  key: factorId,
  message: 'El resultado debe tener al menos un indicador',
});

export const createNoMediosError = ({
  indicadorId,
}) => ({
  key: indicadorId,
  message: 'El indicador debe tener al menos un medio de verificación',
});
