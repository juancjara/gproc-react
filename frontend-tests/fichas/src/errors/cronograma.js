export const createServiciosError = ({
  subActividadId,
}) => ({
  key: subActividadId,
  message: 'Las subactividades de servicio deben iniciar a partir de la tercera quincena.'
});

export const createPasantiasError = ({
  subActividadId,
}) => ({
  key: subActividadId,
  message: 'Las subactividades de pasantias deben iniciar a partir de la quinta quincena.'
});
