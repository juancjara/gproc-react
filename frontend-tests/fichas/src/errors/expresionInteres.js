import _ from 'lodash';

import {
  CUERPO_ACADEMICO,
  COMITE,
  SEDE,
} from '../titles/expresionInteres';

export const ERROR_CUERPO_ACADEMICO_TOTAL = {
 key: CUERPO_ACADEMICO,
  message: 'Los totales por Dedicación y Condición deben ser iguales.'
};

export const ERROR_CUERPO_ACADEMICO_DOCENTE_TOTAL = {
  key: CUERPO_ACADEMICO,
  message: 'El total de condición debe ser igual al total en categoría docente'
};

export const ERROR_COMITE_NO_MEMBERS = {
  key: COMITE,
  message: 'El comité debe tener por lo menos un miembro.'
};

export const createSedeError = (nombre) => ({
  key: SEDE,
  message: `La sede: ${nombre} debe tener por lo menos una carrera`
});

export const createCarreraNameError = (name) => ({
  key: SEDE,
  message: `El nombre de la carrera ${name} no puede ser utilizado.`
})
