import React, { Component, createElement } from 'react';
import { connect } from 'react-redux';

import Alert from '../../components/common/Alert';
import StatusBadges from '../../components/common/status/StatusBadges';
import NodoButton from '../../components/common/status/NodoButton';

import * as messages from '../../messages/common';
import actions from '../../constants/status';
const {
  STORED_CHANGED,
  SERVER_ERROR,
  FETCHING,
  STORED_OK,
  UNAUTHORIZED_ERROR,
} = actions;

const nextSaveTime = 15 * 60 * 1000;
const maxInactiveTime = 50 * 60 * 1000;

const nothingToSave = (pastProps, nextProps) =>
  pastProps.status === nextProps.status && nextProps.status === STORED_OK;

const saveFinished = (pastProps, nextProps) =>
  pastProps.status === FETCHING &&
        nextProps.status !== FETCHING;

const shouldScheduleInactiveAlert = (pastProps, nextProps) =>
  pastProps.status === FETCHING &&
    (nextProps.status === STORED_OK || nextProps.status === STORED_CHANGED);

const shouldScheduleAutoSave = (pastProps, nextProps) =>
  (pastProps.status === STORED_CHANGED && nextProps.status === STORED_CHANGED) ||
  (pastProps.status === STORED_OK && nextProps.status === STORED_CHANGED) ||
  (pastProps.status === FETCHING && nextProps.status === STORED_CHANGED);

const shouldClearIntervals = (pastProps, nextProps) =>
  pastProps.status === FETCHING &&
    pastProps.status !== nextProps.status;

const getNothingToSaveAlertData = (nextProps) => {
  const alertData = nextProps.getCustomAlertMessage(nextProps);
  if (alertData) {
    return alertData;
  }

  if (nextProps.secciones.length) {
    return {
      alertMessage: messages.ALREADY_SAVED_WITH_ERRORS,
      type: 'danger',
    };
  }
  return {
    alertMessage: messages.ALREADY_SAVED_SUCCESS,
    type: 'success',
  };
};

const messageReconnect = () => (
  <div>
    {messages.ERROR_ALERT_ON_SAVE}
    <br />
    Click
    {' '}
    <a href="#" onClick={(e) => {
        e.preventDefault();
        window.open('/?close=1', 500, 500);
      }}>aquí</a>
    {' '}
    para volver a iniciar sesión
  </div>
);

const getSaveAlertData = (nextProps) => {
  const alertData = nextProps.getCustomAlertMessage(nextProps);
  if (alertData) {
    return alertData;
  }

  if (nextProps.status === SERVER_ERROR ) {
    if (nextProps.requestType === UNAUTHORIZED_ERROR) {
      return {
        alertMessage: createElement(messageReconnect),
        type: 'danger',
      };
    }
    else {
      return {
        alertMessage: messages.ERROR_ALERT_ON_SAVE,
        type: 'danger',
      };
    }
  }

  const saveOk = nextProps.status === STORED_OK ||
          nextProps.status === STORED_CHANGED;
  if (saveOk && nextProps.secciones.length) {
    return {
      alertMessage: messages.SAVE_WITH_ERRORS,
      type:'danger',
    };
  }

  return {
    alertMessage: messages.SAVE_SUCCESS,
    type: 'success',
  };
};

// const connectSystemStatus = (
//   params
// ) => (wrappedComponent) => {
  class HOCSystemStatus extends Component {
    constructor(props) {
      super(props);
      this.state = {
        alertMessage: '',
        type: '',
      };

      this.downloadPDF = this.downloadPDF.bind(this);
      this.onFinish = this.onFinish.bind(this);
      this.onCloseAlert = this.onCloseAlert.bind(this);
      this.saveScheduled = undefined;
      this.intervalAFK = undefined;
    }

    onCloseAlert(e) {
      e.stopPropagation();
      this.setState({ alertMessage: '' });
    }

    componentDidMount() {
      if (this.props.dummyComponent) {
        return;
      }

      $(window).bind('beforeunload', () =>
		    this.props.status !== STORED_OK &&
        this.props.status !== SERVER_ERROR ?
          'Existen datos que no han sido guardados.' :
          undefined
	    );

      this.recreateIntervalAFK();
    }

    recreateIntervalAFK() {
      // console.log('create afk interval');
      clearTimeout(this.intervalAFK);
      this.intervalAFK = setTimeout(
        () => {
          // console.log('max afk time reached');
          this.intervalAFK = undefined;
          this.setState({
            alertMessage: messages.WARNING_SESSION_WILL_EXPIRE,
            type: 'danger',
          });
        },
        maxInactiveTime
      );
    }

    componentWillReceiveProps(nextProps) {
      if (this.props.dummyComponent) {
        return;
      }

      if (nothingToSave(this.props, nextProps)) {
        return this.setState(
          getNothingToSaveAlertData(nextProps, this.onCloseAlert));
      }

      if (saveFinished(this.props, nextProps)) {
        this.setState(getSaveAlertData(nextProps));
      }
      if (shouldClearIntervals(this.props, nextProps)) {
        // console.log('clear intervals');
        clearTimeout(this.intervalAFK);
        clearTimeout(this.saveScheduled);
      }

      if (shouldScheduleAutoSave(this.props, nextProps)) {
        // console.log('saving scheduled');
        clearTimeout(this.saveScheduled);
        this.saveScheduled = setTimeout(
          () => {
            // console.log('saving scheduled alert');
            this.props.onSaveAll();
            this.saveSchedule = undefined;
          },
          nextSaveTime
        );
      }

      if (shouldScheduleInactiveAlert(this.props, nextProps)) {
        this.recreateIntervalAFK();
      }
    }

    downloadPDF() {
      const url = this.props.urlPDF + location.search;
      console.log(url);
      $('iframe').attr('src', url);
    }


    onFinish() {
      this.props.onFinish(this.props.status);
    }

    render() {
      const {
        status,
        onSaveAll,
        secciones,
        dummyComponent,
      } = this.props;

      const isDisabled = status === FETCHING;
      return (
      <div className="well system-status">
        {
          !dummyComponent && this.state.alertMessage !== '' &&
            (
              <Alert
                type={this.state.type}
                message={this.state.alertMessage}
                onClose={this.onCloseAlert}
              />
            )
        }
        <button type="button"
          className="btn btn-flat btn-github"
          title="Descargar vista previa"
          onClick={this.downloadPDF}>
				  <i className="fa fa-download"></i> PDF
				</button>{" "}
        <NodoButton
          type={'success'}
          text={'Guardar sin terminar'}
          onClick={onSaveAll}
          disabled={isDisabled}
        />{" "}
        <NodoButton
          type={'primary'}
          text={'Finalizar ficha'}
          onClick={this.onFinish}
          disabled={isDisabled}
        />{" "}

        {secciones.length ? (
          <span>
            Existen errores en las siguientes secciones: {" "}
            {secciones.map(
              (seccion, i) =>
                (<span key={i}>
                  <small className="label label-default">{seccion}</small>
                  {" "}
                </span>)
            )}
          </span>
        ) : undefined }

        <div className="pull-right">
          Estado de la ficha: {" "}
          <StatusBadges status={status} />
        </div>
      </div>
      )
    }
  };

//   return HOCSystemStatus;
// };

export default (mapStateToProps, mapDispatchToProps) =>
  connect(mapStateToProps, mapDispatchToProps)(HOCSystemStatus);
