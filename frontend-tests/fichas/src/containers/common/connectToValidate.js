import React, { Component, createElement } from 'react';
import { validateField } from '../../utils/index';

const connectToValidate = (
  params
) => {
  return (wrappedComponent) => {
    class HOCValidate extends Component {
      constructor (props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.setRef = this.setRef.bind(this);

        this.state = {
          value: props.value || '',
          msg: "",
        };
      }

      setRef (e) {
        this.el = e;
      }

      componentDidMount() {
        if (this.el.tagName === 'TEXTAREA') {
          this.el.pattern = this.el.getAttribute('data-pattern');
        }
        this.setState({ msg: validateField(this.el) });
        this.$el = $(this.el);
        this.$el.tooltip({
          'title': this.$el.data('error-msg'),
          'trigger': 'manual'
        });
      }

      componentWillReceiveProps(nextProps) {
        if (nextProps.value === 'N/A') {
          this.setState({
            value: 'N/A',
            msg: '',
          });
        }
      }

      onChange (e) {
        this.setState({ value: e.target.value });
        if (this.props.instantUpdate) {
          this.onBlur(e);
        }
      }

      onBlur (e) {
        const target = e.target;
        const msg = validateField(target);
        const $el = this.$el;

        this.setState({ msg });

        if (msg.length) {
          if (target.value !== '' || target.required) {
            $el.tooltip('show');
				    setTimeout(() => $el.tooltip('hide'), 2000);
          }
        }

        if (target.value !== this.props.value) {
          if (target.name) {
            this.props.onChange(e);
          } else {
            this.props.onChange(target.value);
          }
        }
      }

      render () {
        const attr = {
          ...this.props,
          required: this.props.required,
          pattern: this.props.pattern,
          name: this.props.name,
          className: this.props.className,
          value: this.state.value,
          msg: this.state.msg,
          onChange: this.onChange,
          onBlur: this.onBlur,
          setRef: this.setRef,
          placeholder: //this.props.required ?
            this.props.placeholder, //|| "* campo requerido" :
            //null,
        };

        return createElement(wrappedComponent, {...attr});
      }
    };

    return HOCValidate;
  };
};

export default connectToValidate;
