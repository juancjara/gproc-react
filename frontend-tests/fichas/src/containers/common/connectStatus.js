import React, { PropTypes, Component, createElement } from 'react';
import ImmutablePropTypes  from 'react-immutable-proptypes';
import { connect } from 'react-redux';

import { summaryStatus } from '../../utils/status';
import { storeNodo } from '../../actions/aNodoStatus';
import {
  validateInlineErrors,
  validateField,
} from '../../utils/';

const propTypes = {
  status: ImmutablePropTypes.contains({
    status: PropTypes.string.isRequired,
    hasChanged: PropTypes.bool.isRequired,
    errors: PropTypes.isRequired,
  }),
  path: PropTypes.array.isRequired,
  storeNodo: PropTypes.func.isRequired,
};

export const connectStatus = (
  params
) => {
  return (wrappedComponent) => {

    class HOCStatus extends Component {
      constructor(props) {
        super(props);
        this.state = {
          inlineErrors: '',
        };
        this.onSave = this.onSave.bind(this);
      }

      componentDidMount() {
        this.root = document.getElementById(this.props.path.join(''));
      }

      onSave() {
        if ( ! validateInlineErrors(this.root)) {
          this.setState({
            inlineErrors: 'Algunos campos tienen errores',
          });
        } else {
          this.setState({ inlineErrors: '' });
        }

        this.props.storeNodo(this.props.path);
      }

      render() {

        const props = {
          ...this.props.status.toJS(),
          rootId: this.props.path.join(''),
          inlineErrors: this.state.inlineErrors,
          onSave: this.onSave,
          summaryStatus: summaryStatus([this.props.status.toJS()]),
          index: this.props.index,
          ...params,
        };

        return createElement(wrappedComponent, {...props});
      }
    }
    HOCStatus.propTypes = propTypes;

    return HOCStatus;
  };
};

const createMapStateToProps = ({
  path,
}) => (state) =>({
  status: state.getIn([...path, 'status']),
  path,
});

const createMapDispatchToProps = ({
  fnValidation,
  webAPI,
}) => (dispatch) => ({
    storeNodo: (path) => dispatch(storeNodo({ fnValidation, webAPI, path })),
  });

export const connectStatusStore = ({
  path,
  webAPI,
  fnValidation
}) => {
  const mapStateToProps = createMapStateToProps({ path });
  const mapDispatchToProps = createMapDispatchToProps({ fnValidation, webAPI });

  return (nodoComponent) => {
    const nodoConnectedStatus = connectStatus()(nodoComponent);
    return connect(mapStateToProps, mapDispatchToProps)(nodoConnectedStatus);
  };
};

export default connectStatusStore;
