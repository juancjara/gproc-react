import React, { Component } from 'react';

import StatusBadges from '../../components/common/status/StatusBadges';

export default class TabNav extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    e.stopPropagation();
    e.preventDefault();
    this.props.onClick(this.props.index);
  }

  render() {
    const {
      active,
      status,
      editable,
      text,
      onClick,
    } = this.props;

    return (
      <li
        className={active ? 'active': ''}
        onClick={this.onClick}
        role="presentation"
      >
        <a href="">
          {text} {' '}
        {
          editable &&
            <StatusBadges status={status} />
        }
        </a>
      </li>
    );
  }
}
