import React, { Component } from 'react';

import TableHeader from '../../components/common/table/TableHeader';
import TableFooter from '../../components/common/table/TableFooter';
import TableBodyPresupuesto
  from '../presupuesto/tablePresupuesto/TableBodyPresupuesto';
import RowMonitoreo
  from './tableMonitoreo/RowMonitoreo';

import { getContext } from '../../context';

const headers = [
  { display : 'O.E.', customClass: 'tdimensiones' },
  { display : 'Indicadores', customClass: 'tindicadores' },
  { display: 'Medios de Verificación', customClass: 'tmedios' },
];

const TableMonitoreoNodo = () => (
  <table className="table table-bordered table-hover">
    <TableHeader
      classTh={'text-center success'}
      headers={headers}
    />
    <TableBodyPresupuesto RowComp={RowMonitoreo} />
    <TableFooter
      classTh={'text-center success'}
      headers={headers}
    />
  </table>
);

export default TableMonitoreoNodo;
