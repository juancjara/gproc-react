import React from 'react';

import FichaNodo from '../planificacion/FichaNodo';
import TableMonitoreoNodo from './TableMonitoreoNodo';
import SystemStatus from './SystemStatus';

import { getContext } from '../../context';

const App = () => {
  const editable = getContext().editable;
  return (
    <div className="form-horizontal ficha">
      {
        editable &&
          <SystemStatus />
      }
      <TableMonitoreoNodo />
      <FichaNodo />
      {
        editable &&
          <SystemStatus dummyComponent />
      }
    </div>
  );
};

export default App;
