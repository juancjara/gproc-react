import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import FactorNodo from './FactorNodo';

import TdPresupuesto from '../../../components/presupuesto/TdPresupuesto';

import { mapStateToProps }
  from '../../presupuesto/tablePresupuesto/RowPresupuesto';

class RowMonitoreo extends Component {
  shouldComponentUpdate(nextProps) {
    return false;
  }

  render() {
    const {
      firstRow,
      dimensionData,
      factorData,
    } = this.props;

    return (
      <tr>
        {
          firstRow && (
              <TdPresupuesto
	        show
                {...dimensionData}
              />
          )
        }
        <FactorNodo {...factorData}/>
      </tr>
    );
  }
}
// const RowMonitoreo = ({
// }) => (

// );

export default connect(mapStateToProps)(RowMonitoreo);
