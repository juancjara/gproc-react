import React from 'react';
import { connect } from 'react-redux';

import {
  removeIndicador,
} from '../../../../actions/monitoreo/aIndicadores';

import {
  updateIndicador,
} from '../../../../actions/monitoreo/indicador/aIndicador';

import BoxError from '../../../../components/common/BoxError';
import StatusBadges from '../../../../components/common/status/StatusBadges';
import Medios from './Medios';

import DisplayOrArea from '../../../../components/common/form/DisplayOrArea';
import RemoveButton from '../../../../components/common/RemoveButton';

import patterns from '../../../../patterns';
import {
  isDisabledMonitoreo,
  getBoxErrors,
} from '../../../../utils';
import { getContext } from '../../../../context';

const Indicador = ({
  name,
  descripcion,
  index,
  factorIndex,
  removeIndicador,
  editable,
  isDisabled,
  rootId,
  summaryStatus,
  onChange,
  errors,
}) => (
  <div id={rootId} className="well-indicador">
    <RemoveButton
      extraClass={'pull-right'}
      size={'sm'}
      onClick={removeIndicador}
      editable={editable}
      title={'Remove indicador'}
      disabled={isDisabled}
    />
    {
      editable &&
        <StatusBadges status={summaryStatus} />
    }
    {name}
    <BoxError
      errors={errors}
    />
    <table className="table table-bordered">
      <tbody>
        <tr>
          <td className="tindicadores">
            <DisplayOrArea
              rows="5"
              value={descripcion}
              name={'descripcion'}
              pattern={patterns.all}
              onChange={onChange}
              editable={editable}
              required
            />
          </td>
          <td className="tmedios" style={{paddingLeft: 10, verticalAlign: 'top'}}>
            <Medios
              indicadorIndex={index}
              factorIndex={factorIndex}
            />
          </td>
        </tr>
      </tbody>
    </table>
    <hr />
  </div>
);

const mapStateToProps = (
  state,
  {
    index,
    visibleIndex,
    factorIndex,
  }
) => ({
  name: `I.${factorIndex + 1}.${visibleIndex + 1}`,
  editable: getContext().editable,
  isDisabled: isDisabledMonitoreo(state),
  descripcion: state.getIn(['ind', index, 'data', 'descripcion']),
  errors: getBoxErrors(
    state.getIn(['ind', index, 'dbId']),
    state.getIn(['ind', index, 'status']).toJS().errors
  ),
});

const mapDispatchToProps = (
  dispatch,
  {
    index,
    factorIndex,
  }
) => ({
  onChange: ({ target }) =>
    dispatch(updateIndicador(index, target.name, target.value)),
  removeIndicador: () => dispatch(removeIndicador(index, factorIndex)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Indicador);
