import React from 'react';
import { connect } from 'react-redux';
import { Map } from 'immutable';

import Medio from './Medio';

import AddButton from '../../../../components/common/AddButton';

import { getContext } from '../../../../context';
import {
  findIndexBy,
  isDisabledMonitoreo,
} from '../../../../utils';

import {
  addMedio,
} from '../../../../actions/monitoreo/indicador/aIndicadorMedios';

const Medios = ({
  medios,
  indicadorIndex,
  factorIndex,
  editable,
  isDisabled,
  addMedio,
}) => (
  <div>
    {
      editable && (
        <div>
          Seleccione Medios de Verificación que se relacionan con el Resultado:
        </div>
      )
    }
    <table>
    <tbody>
    {
      medios.map((medio, i) =>
        editable ?
          (
            <Medio
              key={medio.get('id')}
              selected={medio.get('conc_id')}
              indicadorIndex={indicadorIndex}
              factorIndex={factorIndex}
              indicadorMedioIndex={i}
            />
          ) :
          (
            <tr key={i}>
              <td>
                {medio.get('display')}
                {
                  medio.get('id') === 'otro' &&
                    ` : ${medio.get('otro')}`
                }
              </td>
            </tr>
          )

      )
    }
    </tbody>
    </table>
    <AddButton
      size={'xs'}
      onClick={addMedio}
      editable={editable}
      disabled={isDisabled}
      text={''}
    />
  </div>
);

const noMoreMediosToChooseFrom = ({
  state,
  indicadorIndex,
  factorIndex,
}) => {
  const fact_codigo = getContext().factores[factorIndex].fact_codigo;

  const countMediosUsed = state
    .getIn(['ind', indicadorIndex, 'data', 'medios'])
    .filter(m => m.get('conc_id') !== 'otro')
    .size;

  const countFactorMedios = state
    .getIn(['medios'])
    .filter(m => m.get('fact_codigo') === fact_codigo)
    .size;

  return countMediosUsed === countFactorMedios;
};

const buildMedios = ({
  mediosData,
  mediosState,
}) => mediosData
  .map(medio => {
    if (medio.get('conc_id') === 'otro') {
      return Map({ id: 'otro', display: 'Otro', otro: medio.get('otro') });
    }

    const index = findIndexBy(
      mediosState,
      medio.get('conc_id'),
      e => e.get('conc_id')
    );
    if (index === -1) return Map({ display: 'No seleccionado'});
    return mediosState.get(index);
  })

const getMedios = ({
  mediosData,
  state,
  editable,
}) =>
  editable ?
    mediosData:
    buildMedios({mediosData, mediosState: state.get('medios')})

const mapStateToProps = (
  state,
  {
    indicadorIndex,
    factorIndex,
  }
) => ({
  // medios: state.getIn(['ind', indicadorIndex, 'data', 'medios']),
  medios: getMedios({
    mediosData: state.getIn(['ind', indicadorIndex, 'data', 'medios']),
    state,
    editable: getContext().editable,
  }),
  editable: getContext().editable,
  isDisabled: isDisabledMonitoreo(state),
});


const mapDispatchToProps = (
  dispatch,
  { indicadorIndex }
) => ({
  addMedio: () => dispatch(addMedio(indicadorIndex)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Medios);
