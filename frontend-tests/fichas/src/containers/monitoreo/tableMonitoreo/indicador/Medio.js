import React from 'react';
import { connect } from 'react-redux';

import { getContext } from '../../../../context';

import DisplayOrInput from '../../../../components/common/form/DisplayOrInput';
import RemoveButton from '../../../../components/common/RemoveButton';
import SelectField from '../../../../components/common/form/SelectField';

import {
  removeMedio,
  selectMedio,
  updateOtroMedio,
} from '../../../../actions/monitoreo/indicador/aIndicadorMedios';

import patterns from '../../../../patterns';

const Medio = ({
  mediosOptions,
  selected,
  otro,
  selectMedio,
  removeMedio,
  updateMedioOtro,
  editable,
}) => (
  <tr>
    <td className="td-medio">
      <SelectField
        required
        value={selected}
        options={mediosOptions.concat({id: 'otro', name: 'Otro'})}
        onChange={selectMedio}
      />
      {
        selected ==='otro' &&
        (
          <DisplayOrInput
            name="otro"
            value={otro}
            editable={editable}
            onChange={updateMedioOtro}
            required
            patterns={patterns.all}
          />
        )
      }
    </td>
    <td>
      <RemoveButton
        size={'sm'}
        onClick={removeMedio}
        title="Remover medio"
        editable={editable}
      />
    </td>
  </tr>
);

const getMediosOptions = ({
  state,
  factorIndex,
  indicadorIndex,
  indicadorMedioIndex,
}) => {
  const fact_codigo = getContext().factores[factorIndex].fact_codigo;
  const mediosUsedGrouped = state
    .getIn(['ind', indicadorIndex, 'data', 'medios'])
    .filter((m, i) => m.get('conc_id').length && i !== indicadorMedioIndex)
    .groupBy(medio => medio.get('conc_id'));

  const mediosOptions = state.get('medios')
    .filter(m =>
      m.get('fact_codigo') === fact_codigo &&
            !mediosUsedGrouped.has(m.get('id'))
    )
    .map(m => m.set('name', `(${m.get('count')}) ${m.get('display')}`))
    .toJS();

  return mediosOptions;
};

const mapStateToProps = (
  state,
  {
    indicadorIndex,
    indicadorMedioIndex,
    factorIndex,
  }
) => ({
  mediosOptions: getMediosOptions({ state, factorIndex, indicadorIndex,
                                    indicadorMedioIndex }),
  selected: state.getIn(['ind', indicadorIndex, 'data', 'medios',
                         indicadorMedioIndex, 'conc_id']),
  otro: state.getIn(['ind', indicadorIndex, 'data', 'medios',
                         indicadorMedioIndex, 'otro']),
  editable: getContext().editable,
});

const mapDispatchToProps = (
  dispatch,
  {
    indicadorIndex,
    indicadorMedioIndex,
  }
) => ({
  removeMedio: () =>
    dispatch(removeMedio(indicadorIndex, indicadorMedioIndex)),
  selectMedio: ({ target }) =>
    dispatch(selectMedio(indicadorIndex, indicadorMedioIndex, target.value)),
  updateMedioOtro: ({ target }) =>
    dispatch(updateOtroMedio(indicadorIndex, indicadorMedioIndex, target.value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Medio);
