import React from 'react';
import { connect } from 'react-redux';

import Indicador from './Indicador';
import { connectStatus } from '../../../common/connectStatus';

const IndicadorNodo = (
  props
) => {
  const path = ['ind', props.index];

  const IndicadorConnected = connect(
    state => ({
      path,
      status: state.getIn(path.concat(['status'])),
    }),
    dispatch => ({
      storeNodo: () => {},
    })
  )(connectStatus(props)(Indicador));

  return <IndicadorConnected />;
};

export default IndicadorNodo;
