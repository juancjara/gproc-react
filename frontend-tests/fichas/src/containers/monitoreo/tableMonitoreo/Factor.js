import React from 'react';
import { connect } from 'react-redux';

import {
  addIndicador
} from '../../../actions/monitoreo/aIndicadores';

import Indicadores from './Indicadores';

import BoxError from '../../../components/common/BoxError';
import StatusBadges from '../../../components/common/status/StatusBadges';
import AddButton from '../../../components/common/AddButton';

import {
  isDisabledMonitoreo,
  getBoxErrors,
} from '../../../utils';

import { getContext } from '../../../context';

const Factor = ({
  index,
  title,
  name,
  addIndicador,
  editable,
  isDisabled,
  summaryStatus,
  errors,
}) => (
  <td colSpan="2">
    <div>
      {
        editable &&
          <StatusBadges status={summaryStatus} />
      }
      <strong>{name}</strong>{" "}{title}
    </div>
    <BoxError
      errors={errors}
    />
    <Indicadores
      factorIndex={index}
    />
    <br />
    <AddButton
      onClick={addIndicador}
      editable={editable}
      text={'Agregar indicador'}
      size={'sm'}
      disabled={isDisabled}
    />
  </td>
);


const mapStateToProps = (
  state,
  { index }
) => ({
  editable: getContext().editable,
  isDisabled: isDisabledMonitoreo(state),
  errors: getBoxErrors(
    state.getIn(['fac', index, 'id']),
    state.getIn(['fac', index, 'status']).toJS().errors
  )
});

const mapDispatchToProps = (
  dispatch,
  { index }
) => ({
  addIndicador: () => dispatch(addIndicador(index)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Factor);
