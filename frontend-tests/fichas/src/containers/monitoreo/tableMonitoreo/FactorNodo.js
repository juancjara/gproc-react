import React from 'react';
import { connect } from 'react-redux';

import Factor from './Factor';
import { connectStatus } from '../../common/connectStatus';

const FactorNodo = (
  props
) => {
  const path = ['fac', props.index];

  const FactorConnected = connect(
    state => ({
      path,
      status: state.getIn(path.concat(['status'])),
    }),
    dispatch => ({
      storeNodo: () => {},
    })
  )(connectStatus(props)(Factor));

  return <FactorConnected />;
};

export default FactorNodo;
