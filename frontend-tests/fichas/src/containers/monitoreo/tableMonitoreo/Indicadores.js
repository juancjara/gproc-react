import React, { Component } from 'react';
import { connect } from 'react-redux';

import IndicadorNodo from './indicador/IndicadorNodo';
import { getContext} from '../../../context';

import { addIndexImmutable } from '../../../utils';

class Indicadores extends Component {
  shouldComponentUpdate(nextProps) {
    return !_.isEqual(
      this.props.indicadores.toJS().map(i => i.status.status),
      nextProps.indicadores.toJS().map(i => i.status.status)
    );
  }

  render() {
    const {
      factorIndex,
      indicadores,
    } = this.props;

    return (
      <div>
        {
          indicadores.map((indicador, i) => (
            <IndicadorNodo
              key={indicador.get('dbId')}
              index={indicador.get('index')}
              visibleIndex={i}
              factorIndex={factorIndex}
            />
          ))
        }
      </div>
    );
  }
}

const getIndicadores = (
  state,
  factorDbId
) => state.get('ind')
  .map(addIndexImmutable)
  .filter(ind => ind.get('parent_id') === factorDbId);

const mapStateToProps = (
  state,
  { factorIndex }
) => ({
  indicadores: getIndicadores(state, getContext().factores[factorIndex].id)
});

export default connect(mapStateToProps)(Indicadores);
