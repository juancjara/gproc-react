import React from 'react';
import { connect } from 'react-redux';

import ProgressBarComp
  from '../../components/planificacion/ProgressBarComp';

const ProgressBar = ({
  title,
  percentage,
}) => (
  <ProgressBarComp
    title={title}
    percentage={percentage}
  />
);

const mapStateToProps = (
  state
) => {

  const totalMedios = state.get('medios').size;
  const totalUsedMedios = state.get('medios')
    .filter(medio => medio.get('count') > 0)
    .size;

  const percentage =  totalUsedMedios/
    (totalMedios || 1) * 100;
  const percentageFormated = `${percentage.toFixed(2)}%`;

  return {
    title: `${totalUsedMedios} procesados de
            ${totalMedios} medios de verificación
            (${percentageFormated})`,
    percentage: percentageFormated,
  };
};

export default connect(mapStateToProps)(ProgressBar);
