import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import reactDOM from 'react-dom';

import { getContext } from '../../context';

import connectSystemStatus from '../common/connectSystemStatus';
import * as messages from '../../messages/monitoreo';

import {
  finishFicha,
  showErrorsAndSaveAllNodes,
} from '../../actions/monitoreo/aSystem';

import { summaryStatus } from '../../utils/status';

import actions from '../../constants/status';
const {
  STORED_CHANGED,
  SERVER_ERROR,
  FETCHING,
  STORED_OK,
  UNAUTHORIZED_ERROR,
} = actions;

import constantsPresupuesto from '../../constants/monitoreo';
const {
  REQUEST_ADD_INDICADOR,
  REQUEST_REMOVE_INDICADOR,
} = constantsPresupuesto;

import { getSeccionesError } from '../planificacion/SystemStatus';

const getCustomAlertMessage = ({ requestType, status }) => {
  const saveOK = status === STORED_OK || status === STORED_CHANGED;

  if (saveOK && requestType === REQUEST_ADD_INDICADOR) {
    return {
      alertMessage: messages.REQUEST_ADD_INDICADOR_OK,
      type: 'success',
    };
  }

  if (saveOK && requestType === REQUEST_REMOVE_INDICADOR) {
    return {
      alertMessage: messages.REQUEST_REMOVE_INDICADOR_OK,
      type: 'success',
    };
  }

  return null;
};

// @TODO: refactor this
const flattenStatus = state => {

  const indGroupedByFactor = state.get('ind')
    .groupBy(ind => ind.get('parent_id'));

  const indicadoresStatus = [];
  const factoresStatus = [];

  _.forEach(getContext().factores, (f, factorIndex) => {
    factoresStatus.push({
      status: state.getIn(['fac', factorIndex, 'status']).toJS(),
      name: `R.${factorIndex + 1}`,
    });

    if (indGroupedByFactor.has(f.id)) {
      indGroupedByFactor.get(f.id)
        .forEach((ind, indIndex) => {
          indicadoresStatus.push({
            status: ind.get('status').toJS(),
            name: `I.${factorIndex + 1}.${indIndex + 1}`
          });
        });
    }
  });

  return [
    ...factoresStatus,
    ...indicadoresStatus,
    {
      name: 'Datos del responsable',
      status: state.getIn(['fic', 'status']).toJS()
    },
  ];
};

const mapStateToProps = (
  state
) => {
  const flatStatus = flattenStatus(state);
  const secciones = getSeccionesError(flatStatus);
  const statuses = _.pluck(flatStatus, 'status');

  return {
    status: summaryStatus(statuses, secciones.length),
    secciones,
    requestType: state.get('requestType'),
    urlPDF: '/monitoreo/pdf',
    getCustomAlertMessage,
  };
};

const mapDispatchToProps = (
  dispatch
) => ({
  onSaveAll: () => dispatch(showErrorsAndSaveAllNodes()),
  onFinish: status => dispatch(finishFicha(status)),
});

export default connectSystemStatus(mapStateToProps, mapDispatchToProps);
