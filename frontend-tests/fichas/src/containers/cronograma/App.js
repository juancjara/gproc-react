import React from 'react';

import FichaNodo from '../planificacion/FichaNodo';
import TableCronogramaNodo from './TableCronogramaNodo';
import { getContext } from '../../context';
import SystemStatus from './SystemStatus';

const App = () => {
  const editable = getContext().editable;

  return (
      <div className="form-horizontal ficha" style={{overflow: 'hidden'}}>
      {
        editable &&
          <SystemStatus />
      }
      <TableCronogramaNodo />
      <FichaNodo />
      {
        editable &&
          <SystemStatus dummyComponent />
      }
    </div>
  );
};

export default App;
