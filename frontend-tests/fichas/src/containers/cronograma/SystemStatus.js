import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { getContext } from '../../context';
import {
  getActividades,
  groupSubactividadesByAct,
} from './tableCronograma/TableBodyCronograma';

import connectSystemStatus from '../common/connectSystemStatus';

import {
  finishFicha,
  showErrorsAndSaveAllNodes,
} from '../../actions/cronograma/aSystem';

import { summaryStatus } from '../../utils/status';

import actions from '../../constants/status';
const {
  STORED_CHANGED,
  SERVER_ERROR,
  FETCHING,
  STORED_OK,
  UNAUTHORIZED_ERROR,
} = actions;

import { getSeccionesError } from '../planificacion/SystemStatus';

const getCustomAlertMessage = () => null;

// @TODO: refactor this
const flattenStatus = state => {

  const croGroupedBySub = state.get('cro')
    .groupBy(cro => cro.get('parent_id'));

  const cronogramaStatus = [];

  const visibleActividades = getActividades(
    getContext().actividades,
    groupSubactividadesByAct(getContext().subActividades)
  );

  _.forEach(visibleActividades, (act, actIndex) => {
    const subActividadesFiltered = _.filter(
      getContext().subActividades,
      sub => sub.parent_id === act.id
    );

    _.forEach(
      subActividadesFiltered,
      (sub, subIndex) => {
        cronogramaStatus.push({
          status: croGroupedBySub.getIn([sub.id, 0, 'status']).toJS(),
          name: `Sub .${ actIndex + 1 }.${ subIndex + 1}`,
        });
      }
    );
  });

  return [
    ...cronogramaStatus,
    {
      name: 'Datos del responsable',
      status: state.getIn(['fic', 'status']).toJS()
    },
  ];
};

const mapStateToProps = (
  state
) => {
  const flatStatus = flattenStatus(state);
  const secciones = getSeccionesError(flatStatus);
  const statuses = _.pluck(flatStatus, 'status');

  return {
    status: summaryStatus(statuses, secciones.length),
    secciones,
    requestType: state.get('requestType'),
    urlPDF: '/cronograma/pdf',
    getCustomAlertMessage,
  };
};

const mapDispatchToProps = (
  dispatch
) => ({
  onSaveAll: () => dispatch(showErrorsAndSaveAllNodes()),
  onFinish: status => dispatch(finishFicha(status)),
});

export default connectSystemStatus(mapStateToProps, mapDispatchToProps);
