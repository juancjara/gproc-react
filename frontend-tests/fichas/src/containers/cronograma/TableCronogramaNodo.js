import React from 'react';

import TableHeader from '../../components/common/table/TableHeader';
import TableFooter from '../../components/common/table/TableFooter';
import TableBodyCronograma
  from './tableCronograma/TableBodyCronograma';

import { getContext } from '../../context';

export const getColWidth = _.memoize(
  () => 864/getContext().quincenas
);

const getHeaders = _.memoize(() => [
  { display : 'Actividades', customClass: '' },
  { display : 'Subactividades', customClass: '' },
    ..._.times(getContext().quincenas)
    .map(idx => ({
      display : `Q${idx + 1}`,
      style: {
        width: getColWidth(),
        minWidth: getColWidth(),
        WebkitBoxSizing: 'border-box',
        MozBoxSizing: 'border-box',
        boxSizing: 'border-box',
      }
    }))
  ,
]);

const TableCronogramaNodo = () => (
  <div className="table-responsive">
    <table className="table table-bordered table-hover">
      <TableHeader
        classTh={'text-center success'}
        headers={getHeaders()}
      />
      <TableBodyCronograma />
      <TableFooter
        classTh={'text-center success'}
        headers={getHeaders()}
      />
    </table>
  </div>
);

export default TableCronogramaNodo;
