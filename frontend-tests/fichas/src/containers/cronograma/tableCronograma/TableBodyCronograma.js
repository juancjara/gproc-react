import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { getContext } from '../../../context';
import { flatMap } from '../../../utils';

import TdPresupuesto from '../../../components/presupuesto/TdPresupuesto';
import RowCronograma from './RowCronograma';

const TableBodyCronograma = ({
  actividades,
  subActividadesGrouped,
}) => (
  <tbody>
    {
      flatMap(actividades, (act, actIndex) => {
          if (subActividadesGrouped.hasOwnProperty(act.id)) {
            return _.map(subActividadesGrouped[act.id], (s, i) => (
              <RowCronograma
                key={s.id}
                firstRow={i === 0}
                actividadIndex={act.index}
                actividadVisibleIndex={`${act.tempIndex}.${act.name}`}
                subActividadIndex={s.index}
                subActividadVisibleIndex={i}
              />
            ))
          } else {
            return (<tr key={act.id}>
              <TdPresupuesto
                index={1}
                size={1}
                name={`A.${act.tempIndex}.${act.name}`}
              />
              <td>Sin subactividades</td>
              {
                _.times(getContext().quincenas - 1)
                  .map(idx => <td key={idx}></td>)
              }
            </tr>
            )
          }
        }
      )
    }
  </tbody>
);

// @TODO refactor this
export const getActividades = (
  actividades,
  subActividadesGrouped
) => {
  const rawActividades = _(actividades)
    .map((act, index) => ({
        ...act,
      index,
    }))
    .sort('parent_id')
  // .filter(act => subActividadesGrouped.hasOwnProperty(act.id))
    .value();

  if (!rawActividades.length) {
    return rawActividades;
  }

  let temp = 0;
  let pastFactDbId = '';
  let actIndex = 0;

  return _(rawActividades)
    .map(act => {
      if (act.parent_id !== pastFactDbId) {
        pastFactDbId = act.parent_id;
        actIndex = 0;
        temp++;
      }
      else {
        actIndex++;
      }

      return {
        tempIndex: temp,
        name: actIndex + 1,
        ...act,
      };
    })
    .value();
}
export const groupSubactividadesByAct = subActividades =>
  _(subActividades)
    .map((sub, index) => ({
      ...sub,
      index,
    }))
    .groupBy(sub => sub.parent_id)
    .value();

const mapStateToProps = () => {
  const subActividadesGrouped = groupSubactividadesByAct(getContext().subActividades);
  return {
    actividades: getActividades(getContext().actividades,
                                subActividadesGrouped),
    subActividadesGrouped,
  };
};

export default connect(mapStateToProps)(TableBodyCronograma);
