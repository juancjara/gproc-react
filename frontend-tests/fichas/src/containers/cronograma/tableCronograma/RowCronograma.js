import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import _ from 'lodash';

import BoxError from '../../../components/common/BoxError';

import { getContext } from '../../../context';
import {
  getIn,
  toNumber,
  getBoxErrors,
} from '../../../utils';

import {
  getColWidth,
} from '../TableCronogramaNodo';

import {
  updateCronograma,
} from '../../../actions/cronograma/aCronograma';

import TdPresupuesto from '../../../components/presupuesto/TdPresupuesto';

class RowCronograma extends Component {
  componentDidMount() {
    if (!getContext().editable) {
      return;
    }

    const gridWidth = getColWidth();

    const index = this.props.cronogramaIndex;
    const containment = `#wrapper${index}`;
    $(`#time${index}`)
      .draggable({
        axis: 'x',
        containment,
        grid: [gridWidth],
        stop: (e, ui) => {
          this.props.update(index, 'start', Math.floor(ui.position.left / gridWidth));
        }
      })
      .resizable({
        containment,
        grid: gridWidth,
        handles: 'e',
        stop: (e, ui) => {
          this.props.update(index, 'duration', Math.floor(ui.size.width / gridWidth) + 1);
        }
      });
  }

  render() {
    const {
      firstRow,
      actividadData,
      subActividadData,
      quincenas,
      duration,
      start,
    } = this.props;

    const gridWidth = +getColWidth();
    const wrapperStyle = {
      position: 'absolute',
      height: '39px',
      width: gridWidth * quincenas - 10,
      top: 0,
      left: 5,
    };

    const timeWidth = gridWidth - 10;
    const left = timeWidth * start + start * 10 + 5;
    const timeStyle = {
      cursor: getContext().editable? 'move': 'auto',
      zIndex: 10,
      top: 2,
      bottom: 0,
      left,
      WebkitBoxSizing: 'border-box',
      MozBoxSizing: 'border-box',
      boxSizing: 'border-box',
      position: 'absolute',
      minWidth: timeWidth * duration + (duration - 1) * 10,
      background: '#ddd',
      border: '1px solid #777',
      borderRadius: 5,
    };

    return (
      <tr>
        {
          firstRow && (
              <TdPresupuesto
            {...actividadData}
              />
          )
        }
        <TdPresupuesto
      {...subActividadData}
        />
        <td style={{position: 'relative'}}>
          <div
            style={wrapperStyle}
            id={`wrapper${subActividadData.index}`}
          />
        <div style={timeStyle} id={`time${subActividadData.index}`}></div>
        </td>
        {
          _.times(quincenas - 1)
            .map(idx => <td key={idx}></td>)
        }

      </tr>
    );
  }
};

const getSizeActividad = _.memoize(
  actividadIndex =>
    _.filter(
      getContext().subActividades,
      s => s.parent_id ===
        getContext().actividades[actividadIndex].id
    ).length
);

const mapStateToProps = (
  state,
  {
    actividadIndex,
    actividadVisibleIndex,
    subActividadIndex,
    subActividadVisibleIndex,
  }
) => {
  const subActividadId = getContext().subActividades[subActividadIndex].id;

  const cronogramaIndex = state.get('cro')
          .map((c, index) => Map({
            index,
            parent_id: c.get('parent_id')
          }))
          .filter(c => c.get('parent_id') === subActividadId)
          .getIn([0, 'index']);

  const errorsSub = getBoxErrors(
    subActividadId,
    state.getIn(['cro', cronogramaIndex, 'status']) .toJS().errors
  );

  return {
    actividadData: {
      index: subActividadIndex,
      size: getSizeActividad(actividadIndex),
      title: getIn(getContext().actividades[actividadIndex],
                   'data.miactividad', ''),
      name: `A.${actividadVisibleIndex}`,
    },
    subActividadData: {
      index: subActividadIndex,
      title: getIn(getContext().subActividades[subActividadIndex],
                   'data.descripcion', ''),
      comp: <BoxError errors={errorsSub} />,
      name: `Sub.${actividadVisibleIndex}.${subActividadVisibleIndex + 1}`,
    },
    quincenas: getContext().quincenas,
    duration: state.getIn(['cro', cronogramaIndex, 'data', 'duration']),
    start: state.getIn(['cro', cronogramaIndex, 'data', 'start']),
    cronogramaIndex,
  };
}

const mapDispatchToProps = (
  dispatch,
  {
    subActividadIndex,
  }
) => {
  return {
    update: (cronogramaIndex, prop, value) =>
      dispatch(updateCronograma(cronogramaIndex, prop, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RowCronograma);
