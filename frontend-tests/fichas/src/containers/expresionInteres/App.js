import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Map } from 'immutable';

import { summaryStatus } from '../../utils/status';
import { getContext } from '../../context';
import SystemStatus from './SystemStatus';
import FichaNodo from './FichaNodo';
import DatosInstitucionalesNodo from './DatosInstitucionalesNodo';
import CarreraNodo from './CarreraNodo';
import NavTab from '../common/NavTab';
import { findIndexBy } from '../../utils';
import {
  showErrorsAndSaveAllNodes,
} from '../../actions/expresionInteres/aSystem';

class CarrerasComp extends Component {
  componentDidMount () {
    // console.log('carreras_mounted');
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.currentTab !== nextProps.currentTab ||
      this.props.carrerasId.length !== nextProps.carrerasId.length;
  }

  render () {
    console.log('carreras_render');
    const {
      carrerasId,
      currentTab,
    } = this.props;

    const carreras = carrerasId
      .map((carreraId, i) => (
        <div
          className={(currentTab - 2) === i ? '' : 'hidden'}
          key = { carreraId || i }
        >
          <CarreraNodo
            carreraId={carreraId}
            index={i}
          />
        </div>
      ));

    return <div>{carreras}</div>;
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    this.state = {
      currentTab: 0,
    };
  }

  onClick(currentTab) {
    this.setState({ currentTab });
  }

  // componentDidMount() {
  //   setInterval(
  //     () => {
  //       this.props.onSaveAll();
  //     },
  //     900000
  //   );
  // }

  render() {
    const {
      status,
      carreras,
      tabs,
      editable
    } = this.props;

    const currentTab = this.state.currentTab;
    const carrerasId = carreras
      .map(carrera => carrera.getIn(['data', 'id']))
      .toJS();

    const carrerasComp = /*carrerasId
      .map((carreraId, i) => (
        <div
          className={(currentTab - 2) === i || 'hidden'}
          key = { carreraId || i }
        >
          <CarreraNodo
            carreraId={carreraId}
            index={i}
          />
        </div>
      ));*/
      <CarrerasComp currentTab={currentTab} carrerasId={carrerasId} />

    return (
      <div className="form-horizontal ficha">
        {
          getContext().editable &&
            <SystemStatus />
        }
        <div className="nav-tabs-custom">
          <ul className="nav nav-tabs nav-justified">
          {
            tabs.map(({ title, status }, i) => (
              <NavTab
                key={i}
                index={i}
                active={currentTab === i}
                onClick={this.onClick}
                text={title}
                status={status}
                editable={editable}
              />
            ))
          }
          </ul>

          <div className="tab-content">
            <div className={currentTab === 0 || 'hidden'}>
              <FichaNodo />
            </div>
            <div className={currentTab === 1 || 'hidden'}>
              <DatosInstitucionalesNodo />
            </div>
            {carrerasComp}
          </div>
        </div>
        {
          getContext().editable &&
            <SystemStatus dummyComponent/>
        }
      </div>
    );
  }
};

const formatCarreraTabNoInstitucional = (
  state
) => [{
  title: state.getIn(['car', 0, 'data', 'nombre']),
  status: summaryStatus([state.getIn(['car', 0, 'status']).toJS()]),
}];

const formatCarreraTabInstitucional = (
  state
) =>
  state.get('car')
    .map(carrera => Map({
      id: carrera.getIn(['data', 'id']),
      status: carrera.get('status'),
    }))
    .toJS()
    .map(_.partial(
      formatCarrera,
      state.getIn(['ins', 'data', 'total_estudiantes', 'p_admision']),
      _
    ));


const formatCarrera = (
  carreras,
  {
    status,
    id,
  }
) => {

  const index = findIndexBy(
    carreras,
    id,
    e => e.get('id')
  );
  return {
    title: carreras.getIn([index, 'nombre']),
    status: summaryStatus([status]),
  };
};

const formatTabs = (
  state
) => {
  const carrerasTabs =
    getContext().institucional ?
    formatCarreraTabInstitucional(state) :
    formatCarreraTabNoInstitucional(state);

  const tabs = [
    {
      title: 'Datos del responsable',
      status: summaryStatus([state.getIn(['fic', 'status']).toJS()]),
    },
    {
      title: 'Datos institucionales',
      status: summaryStatus([state.getIn(['ins', 'status']).toJS()]),
    },
  ].concat(carrerasTabs);

  return tabs;
};

const mapStateToProps = (state) => ({
  carreras: state.getIn(['car']),
  tabs: formatTabs(state),
  editable: getContext().editable,
});

const mapDispatchToProps = (dispatch) => ({
  onSaveAll: () => dispatch(showErrorsAndSaveAllNodes()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
