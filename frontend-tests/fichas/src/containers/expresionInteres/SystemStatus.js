import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { fromJS } from 'immutable';

import { getContext } from '../../context';
import { summaryStatus } from '../../utils/status';
import connectSystemStatus from '../common/connectSystemStatus';
import NodoButton from '../../components/common/status/NodoButton';
import StatusBadges from '../../components/common/status/StatusBadges';
import {
  finishFicha,
  showErrorsAndSaveAllNodes
} from '../../actions/expresionInteres/aSystem';

import * as messagesExpInteres from '../../messages/expresionInteres';
import * as messages from '../../messages/common';

import actions from '../../constants/status';
const {
  STORED_CHANGED,
  SERVER_ERROR,
  STORED_OK,
} = actions;

import constantsExpInteres from '../../constants/expresionInteres';
const {
  REQUEST_ADD_CARRERA,
  REQUEST_REMOVE_CARRERA,
  REQUEST_REMOVE_SEDE,
} = constantsExpInteres;

const getCustomAlertMessage = ({ requestType, status }) => {
  const saveOK = status === STORED_OK || status === STORED_CHANGED;
  if (saveOK && requestType === REQUEST_ADD_CARRERA) {
    return {
      alertMessage: messagesExpInteres.REQUEST_ADD_CARRERA_OK,
      type: 'success',
    };
  };

  if (saveOK && requestType === REQUEST_REMOVE_CARRERA) {
    return {
      alertMessage: messagesExpInteres.REQUEST_REMOVE_CARRERA_OK,
      type: 'success',
    };
  };

  if (saveOK && requestType === REQUEST_REMOVE_SEDE) {
    return {
      alertMessage: messagesExpInteres.REQUEST_REMOVE_SEDE_OK,
      type: 'success',
    };
  };

  return null;
};

const flattenStatus = (state) => {
  const ficStatus = {
    name: 'Datos del responsable',
    status: state.getIn(['fic', 'status']).toJS(),
  };
  const insStatus = {
    name: 'Datos institucionales',
    status: state.getIn(['ins', 'status']).toJS()
  };

  let totalEstudiantesRows = fromJS([{
    nombre: state.getIn(['car', 0, 'data', 'nombre'])
  }]);
  if (getContext().institucional) {
    totalEstudiantesRows = state
      .getIn(['ins', 'data', 'total_estudiantes', 'p_admision']);
  }

  const carrerasStatus = state
    .get('car').map((carrera, i) => ({
      name: totalEstudiantesRows.getIn([i, 'nombre']),
      status: carrera.get('status').toJS()
    }));

  return [ficStatus, insStatus, ...carrerasStatus];
};

const getSeccionesError = (state) =>
  _(state)
     .map(({ name, status }) => {
       if (status.errors.length) {
         return `${name}`;
       }
       return '';
     })
    .filter(error => error.length)
    .value();

const mapStateToProps = (
  state
) => {
  const flatStatus = flattenStatus(state);
  const secciones = getSeccionesError(flatStatus);
  const statuses = _(flatStatus).pluck('status').value();
  return {
    status: summaryStatus(statuses, secciones.length),
    secciones,
    requestType: state.get('requestType'),
    urlPDF: '/expresionInteres/pdf',
    getCustomAlertMessage,
  };
};

const mapDispatchToProps = (
  dispatch
) => ({
  onSaveAll: () => dispatch(showErrorsAndSaveAllNodes()),
  onFinish: (status) => dispatch(finishFicha(status)),
});

export default connectSystemStatus(mapStateToProps, mapDispatchToProps);
