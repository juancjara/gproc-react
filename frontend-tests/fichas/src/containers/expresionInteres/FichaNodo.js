import React from 'react';
import { connect } from 'react-redux';

import Modalidad from './ficha/Modalidad';
import ActionButtons from '../../components/common/status/ActionButtons';
import StatusHeader from '../../components/expresionInteres/StatusHeader';
import connectStatusStore from '../common/connectStatus';
import DatosFirmas from '../../components/expresionInteres/DatosFirma';
import { APIstoreNodo } from '../../webAPI/expresionInteres';
import { context } from '../../context';
import SectionTitle from '../../components/expresionInteres/common/SectionTitle';

import {
  fichaUpdate,
  fichaUpdateInst,
  fichaUpdateCarr,
} from '../../actions/expresionInteres/aFicha';

const mapStateToProps = (
  state
) => ({
  instData: state.getIn(['fic', 'data', 'datos', 'inst']),
  carrData: state.getIn(['fic', 'data', 'datos', 'carr']),
  isInstitucional: context.institucional,
});

const mapDispatchToProps = (
  dispatch
) => ({
  onUpdateInst: (e) => dispatch(fichaUpdateInst(e.target.name, e.target.value)),
  onUpdateCarr: (e) => dispatch(fichaUpdateCarr(e.target.name, e.target.value)),
});

const Firmas = connect(mapStateToProps, mapDispatchToProps)(DatosFirmas);

// <Modalidad />
const FichaNodo = (
  props
) => (
  <div id={props.rootId}>
    <div className="pull-left">
      {
        context.editable &&
          <ActionButtons {...props} />
      }
    </div>

    <SectionTitle
      title={'Sección Inicial - Datos del responsable'}
      subtitle={'Esta sección debe ser llenada antes, durante o después de llenar las otras secciones, Datos institucionales, y Presentación de la(s) carrera(s)'}
    />

    {
      context.editable &&
        <StatusHeader {...props} />
    }

    <hr/>
    <Firmas editable={context.editable} />

    {
      context.editable &&
        <ActionButtons {...props} />
    }
  </div>
);

export default connectStatusStore({
  path: ['fic'],
  webAPI: APIstoreNodo,
})(FichaNodo);
