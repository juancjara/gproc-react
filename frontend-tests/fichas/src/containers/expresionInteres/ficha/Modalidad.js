import React, { Component } from 'react';
import { connect } from 'react-redux';

import BoxHeader from '../../../components/common/BoxHeader';
import StaticField from '../../../components/common/form/StaticField';
import RadioField from '../../../components/common/form/RadioField';

import { fichaUpdate } from '../../../actions/expresionInteres/aFicha';
import { context } from '../../../context';

const titleModalidad = 'Modalidad y Categoría de Financiamiento - Miembros del Comité de Calidad Institucional';

const Modalidad = ({
  tifi_id,
  onFichaUpdate,
  modaNombre
}) => (
  <div className='box'>
    <BoxHeader
     title={titleModalidad}
     number={0}
    />

    <div className='box-body'>
      <StaticField
        label={'Modalidad'}
        value={modaNombre}
      />

      <RadioField
        editable={context.editable}
        name={'tifi_id'}
        selected={tifi_id}
        label={'Categoría financiamiento'}
        options={context.aTipoFinanciamiento}
        onChange={onFichaUpdate}
        required
      />

    </div>

  </div>
);

const mapStateToProps = (
  state
) => ({
  tifi_id: state.getIn(['fic', 'data', 'tifi_id']),
  modaNombre: state.getIn(['fic', 'data', 'moda_nombre']),
});

const mapDispatchToProps = (
  dispatch
) => ({
  onFichaUpdate: (e) => dispatch(fichaUpdate(e.target.name, e.target.value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Modalidad);
