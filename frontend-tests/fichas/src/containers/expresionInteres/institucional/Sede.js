import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import BoxHeader from '../../../components/common/BoxHeader';
import EditableField from '../../../components/common/form/EditableField';
import RemoveButton from '../../../components/common/RemoveButton';
import SedeCarreras from './SedeCarreras';

import { isDisabledInstitucional } from '../../../utils';
import { context } from '../../../context';

import {
  updateSede,
  removeSede,
} from '../../../actions/expresionInteres/institucional/aSedes';

const fields = [
  {
    label: 'Dirección *',
    key: 'direc',
  },
  {
    label: 'Distrito *',
    key: 'dist',
  },
  {
    label: 'Provincia *',
    key: 'prov',
  },
  {
    label: 'Región *',
    key: 'region',
  },
];

const Sede = ({
  sede,
  sedeIndex,
  onChange,
  onRemove,
  isDisabled,
}) => (
  <div className="col-sm-6 col-lg-4 filial_item">
    <div className="box">
      <BoxHeader title={sede.get('nombre_sede')}>
        <div className="box-tools pull-right">
          <RemoveButton
           onClick={onRemove}
           title={'Remover filial'}
           editable={context.editable}
           disabled={isDisabled}
          />
        </div>
      </BoxHeader>


      <div className="box-body">
        {fields.map(({ label, key }) => (
          <EditableField
            editable={context.editable}
            required
            key={key}
            label={label}
            value={sede.get(key)}
            name={key}
            onChange={onChange}
            classLabel={'col-xs-4'}
            classWrapInput={'col-xs-8'}
            classWrapDisplay={'col-xs-8'}
          />
        ))}

        <SedeCarreras
          sedeIndex={sedeIndex}
          sedeName={sede.get('nombre_sede')}
        />
      </div>
    </div>
  </div>
);

const mapStateToProps = (
  state,
  { sedeIndex }
) => ({
  sede: state.getIn(['ins', 'data', 'sedes', sedeIndex]),
  sedeIndex,
  isDisabled: isDisabledInstitucional(state, context.institucional),
});

const mapDispatchToProps = (
  dispatch,
  { sedeIndex }
) => ({
  onChange: (e) => dispatch(updateSede(sedeIndex, e.target.name, e.target.value)),
  onRemove: () => dispatch(removeSede(sedeIndex, context.institucional)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Sede);
