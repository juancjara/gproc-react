import React, { Component } from 'react';
import { connect } from 'react-redux';

import BoxHeader from '../../../components/common/BoxHeader';
import StaticField from '../../../components/common/form/StaticField';
import Table from '../../../components/common/table/Table';
import AddButton from '../../../components/common/AddButton';
import { context, getContext } from '../../../context';
import { isDisabledInstitucional } from '../../../utils';
import Sede from './Sede';
import * as errors from '../../../errors/expresionInteres';

import {
  addSede,
} from '../../../actions/expresionInteres/institucional/aSedes';

const Sedes = ({
  sedes,
  addSede,
  isDisabled,
  bannedName,
}) => (
  <div className='box'>
    <BoxHeader
      title={'Sedes de la Institución y Carreras por área de conocimiento'}
      number={3}
    >
      <div className="box-tools pull-right">
        <AddButton
          editable={context.editable}
          title={'Agregar filial'}
          text={'Agregar filial'}
          onClick={addSede}
          disabled={isDisabled}
        />
      </div>

    </BoxHeader>

    <div className="box-body">
      <div className="row filial_list">
        {
          bannedName &&
            (
              <p>
                {`No debe crear la carrera ${bannedName} que es tema de esta convocatoria en la Sede Principal.`}
              </p>
            )
        }
        {
          sedes.map((sede, i) => (
            <Sede
              key={sede.get('id')}
              sedeIndex={i}
            />
          ))
        }
      </div>
    </div>
  </div>
);

const mapStateToProps = (
  state
) => ({
  sedes: state.getIn(['ins', 'data', 'sedes']),
  isDisabled: isDisabledInstitucional(state, context.institucional),
  bannedName: !getContext().institucional ?
    state.getIn(['car', 0, 'data', 'nombre']):
    undefined
});

const mapDispatchToProps = (
  dispatch
) => ({
  addSede: () => dispatch(addSede(context.institucional)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Sedes);
