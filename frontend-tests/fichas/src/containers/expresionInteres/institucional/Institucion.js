import React, { Component } from 'react';
import { connect } from 'react-redux';

import BoxHeader from '../../../components/common/BoxHeader';
import StaticField from '../../../components/common/form/StaticField';
import EditableField from '../../../components/common/form/EditableField';
import { context } from '../../../context';
import patterns from '../../../patterns';

import { institucionalUpdate }
  from '../../../actions/expresionInteres/aInstitucional';

const Institucion = ({
  ruc,
  codMod,
  nombre,
  subtipo,
  gestion,
  onChange,
}) => (
  <div className="box">
    <BoxHeader
      number={2}
      title={'Institución'}
    />
    <div className="box-body">
      <StaticField
        label={'Nombre de la institución'}
        value={nombre}
      />
      <EditableField
        editable={context.editable}
        label={'RUC *'}
        value={ruc}
        name={'ruc'}
        pattern={patterns.numberLength(11)}
        errorMsg={'Campo debe tener 11 numeros'}
        onChange={onChange}
        required
      />
      <StaticField
       label={'Tipo de gestión'}
       value={gestion}
      />
      {
        !context.universidad &&
          <StaticField
            label={'Tipo'}
            value={subtipo}
          />
      }

      {
        !context.universidad &&
          <EditableField
            editable={context.editable}
            label={'Código Modular *'}
            name={'cod_mod'}
            value={codMod}
            onChange={onChange}
            required
          />
      }
    </div>
  </div>
);

const mapStateToProps = (
  state
) => ({
  ruc: state.getIn(['ins', 'data', 'ruc']),
  codMod: state.getIn(['ins', 'data', 'cod_mod']),
  nombre: state.getIn(['ins', 'data', 'inst_nombre']),
  gestion: state.getIn(['ins', 'data', 'inst_gestion']),
  subtipo: state.getIn(['ins', 'data', 'inst_subtipo']),
});

const mapDispatchToProps = (
  dispatch
) => ({
  onChange: (e) => dispatch(institucionalUpdate(e.target.name,
                                                e.target.value))
});

export default connect(mapStateToProps, mapDispatchToProps)(Institucion);
