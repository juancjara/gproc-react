import React, { Component } from 'react';
import { connect } from 'react-redux';

import patterns from '../../../patterns';
import EditableField from '../../../components/common/form/EditableField';
import Table from '../../../components/common/table/Table';
import AddButton from '../../../components/common/AddButton';
import * as messages from '../../../messages/common';

import { context, getContext } from '../../../context';
import { isDisabledInstitucional } from '../../../utils';

import {
  addSedeCarrera,
  removeSedeCarrera,
  updateCarrera,
} from '../../../actions/expresionInteres/institucional/aSedeCarreras';

const headers = [
  {key: 'nombre', display: 'Nombres *'},
  {key: 'area', display: 'Área académica *'},
];

const SedeCarreras = ({
  carreras,
  addCarrera,
  onChange,
  removeCarrera,
  isDisabled,
  sedeIndex,
}) => (
  <div>
    <legend>Carreras</legend>
    <Table
      className={"table table-condensed"}
      editable={context.editable}
      headers={headers}
      body={carreras}
      onChange={onChange}
      canBeRemoved={!isDisabled}
      onRemove={removeCarrera}
      pattern={patterns.all}
    />
    <AddButton
      editable={context.editable}
      text={'Agregar carrera'}
      onClick={addCarrera}
      disabled={isDisabled}
    />
    {
      !getContext().institucional  && sedeIndex === 0 && (
        <small className="text-red">
          {messages.WARNING_CARRERA_BANNED_NAME}
        </small>
      )
    }
  </div>
);

const mapStateToProps = (
  state,
  { sedeIndex }
) => ({
  carreras: state.getIn(['ins', 'data', 'sedes', sedeIndex, 'carreras']),
  isDisabled: isDisabledInstitucional(state, context.institucional),
  sedeIndex,
});

const mapDispatchToProps = (
  dispatch,
  {
    sedeIndex,
    sedeName,
  }
) => ({
  onChange: (r, c, v) => dispatch(updateCarrera(sedeIndex, sedeName, r, c, v)),
  addCarrera: () => dispatch(addSedeCarrera(sedeIndex, context.institucional)),
  removeCarrera: (carreraId) => dispatch(removeSedeCarrera(sedeIndex, carreraId, context.institucional)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SedeCarreras);
