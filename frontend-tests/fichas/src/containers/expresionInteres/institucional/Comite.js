import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fromJS } from 'immutable';

import BoxHeader from '../../../components/common/BoxHeader';
import Comite from '../../../components/expresionInteres/common/Comite';

import { context } from '../../../context';

import {
  addMember,
  removeMember,
  updateMember,
} from '../../../actions/expresionInteres/institucional/aComiteInstitucional';

const InstitucionalComite = ({
  comite,
  onComiteUpdate,
  addMember,
  removeMember,
  editable,
}) => (
  <div className='box'>
    <BoxHeader
      title={'Miembros del Comité de Calidad Institucional'}
      number={1}
    />

    

    <Comite
      editable={editable}
      comite={comite}
      onComiteUpdate={onComiteUpdate}
      canBeRemoved={editable}
      removeMember={removeMember}
      addMember={addMember}
    >
      {
        !context.universidad &&
          <h5>
            Para el caso de los ISE, solo pueden presentar Planes de Mejora para 
            el conjunto de las carreras pedagógicas o para cada una de las
            carreras tecnológicas.
          </h5>
      }
    </Comite>
  </div>
);

const mapStateToProps = (
  state
) => ({
  comite: state.getIn(['ins', 'data', 'comite']),
  editable: !context.institucional && context.editable,
});

const mapDispatchToProps = (
  dispatch
) => ({
  onComiteUpdate: (r, c, v) => dispatch(updateMember(r, c, v)),
  addMember: () => dispatch(addMember()),
  removeMember: (row) => {dispatch(removeMember(row))},
});

export default connect(mapStateToProps, mapDispatchToProps)(InstitucionalComite);
