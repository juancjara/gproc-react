import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import BoxHeader from '../../../components/common/BoxHeader';
import CuerpoAcademicoDedicacion
  from '../../../components/expresionInteres/CuerpoAcademicoDedicacion';
import CuerpoAcademicoCondicion
  from '../../../components/expresionInteres/CuerpoAcademicoCondicion';
import CuerpoAcademicoDocente
from '../../../components/expresionInteres/CuerpoAcademicoDocente';
import { context } from '../../../context';

import {
  updateRowColSum,
  updateColSum,
} from '../../../actions/expresionInteres/institucional/aCuerpoAcademico';
import * as errors from '../../../errors/expresionInteres';

const title = 'Cuerpo académico de la Institución del año en curso (detallar en números)';


export const classErrorTotales = 'error-totales';
export const classErrorTotalUniversidad = 'error-total-universidad';
export const getClassCondicion = (
  totalesError,
  totalErrorUniversidad
) =>
  (totalesError ? classErrorTotales: '') + ' ' +
          (totalErrorUniversidad? classErrorTotalUniversidad: '');

const CuerpoAcademico = ({
  cuerpoAcademico,
  totalesError,
  totalErrorUniversidad,
  onChangeDedicacion,
  onChangeCondicion,
  onChangeDocentes,
}) => (
  <div
    className={`${context.editable ? 'cuerpo_academico': null} box`}
  >
    <BoxHeader
      title={title}
      subtitle={'(registrar el mayor grado y/o título alcanzado por cada docente)'}
      number={4}
    />

    <div className="box-body">
      <div className="row">
        <p>
         {errors.ERROR_CUERPO_ACADEMICO_TOTAL.message}
         {' '}
         {
           context.universidad ?
             errors.ERROR_CUERPO_ACADEMICO_DOCENTE_TOTAL.message:
             ''
         }
        </p>
        <CuerpoAcademicoDedicacion
          classTable={totalesError ? classErrorTotales: ''}
          data={cuerpoAcademico.get('grados_dedicacion')}
          onChange={onChangeDedicacion}
          editable={context.editable}
        />

        <CuerpoAcademicoCondicion
  classTable={getClassCondicion(totalesError, totalErrorUniversidad)}
          data={cuerpoAcademico.get('grados_condicion')}
          onChange={onChangeCondicion}
          editable={context.editable}
        />
      </div>
      {
        context.universidad &&
          <div className="row">
            <CuerpoAcademicoDocente
              classTable={totalErrorUniversidad ? classErrorTotalUniversidad: ''}
              data={cuerpoAcademico.get('docentes_categoria')}
              onChange={onChangeDocentes}
              editable={context.editable}
            />
          </div>
      }
    </div>
  </div>
);

const mapStateToProps = (
  state,
  {
    totalesError,
    totalErrorUniversidad,
  }
) => ({
  cuerpoAcademico: state.getIn(['ins', 'data', 'cuerpo_academico']),
  totalesError,
  totalErrorUniversidad
});

const mapDispatchToProps = (
  dispatch
) => ({
  onChangeDedicacion: (r, c, v) => dispatch(updateRowColSum('grados_dedicacion',
                                                            r, c, v)),
  onChangeCondicion: (r, c, v) => dispatch(updateRowColSum('grados_condicion',
                                                            r, c, v)),
  onChangeDocentes: (r, c, v) => dispatch(updateColSum('docentes_categoria',
                                                       r, c, v)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CuerpoAcademico);
