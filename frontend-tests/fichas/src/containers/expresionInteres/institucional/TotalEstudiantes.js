import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import BoxHeader from '../../../components/common/BoxHeader';
import StaticField from '../../../components/common/form/StaticField';
import TotalEstudiantesTable
  from '../../../components/expresionInteres/TotalEstudiantesTable';
import { context } from '../../../context';

import {
  updateTotalEstudiantes
} from '../../../actions/expresionInteres/institucional/aTotalEstudiantes';


const title = 'Total de estudiantes de la Institución por Carrera (detallar en números)';

const tables = [
  {
    key: 'p_admision',
    title: 'Postulantes x Admisión',
  },
  {
    key: 'm_semestre',
    title: 'Matriculados x semestre',
  },
  {
    key: 'egresados',
    title: 'Egresados',
  },
  {
    key: 'titulados',
    title: 'Titulados',
  },
];

const TotalEstudiantes = ({
  totalEstudiantes,
  onChange,
}) => (
  <div className='box total_estudiantes'>
    <BoxHeader
      title={title}
      number={5}
    />
    <div className="box-body">
      <div className="row">
        {
          _.map(tables, ({key, title}) => (
            <TotalEstudiantesTable
              editable={context.editable}
              key={key}
              data={totalEstudiantes.get(key)}
              title={title}
              onChange={onChange[key]}
            />
          ))
        }
      </div>
    </div>
    
  </div>
);

const mapStateToProps = (
  state
) => ({
  totalEstudiantes: state.getIn(['ins', 'data', 'total_estudiantes'])
});

const createOnChangeFn = (dispatch, tableId) =>
  (r, c, v) => {
    const keys = c.split('/');
    dispatch(updateTotalEstudiantes(tableId, r, keys[0], keys[1], v));
  };

const mapDispatchToProps = (
  dispatch
) => ({
  onChange: {
    p_admision: createOnChangeFn(dispatch, 'p_admision'),
    m_semestre: createOnChangeFn(dispatch, 'm_semestre'),
    egresados: createOnChangeFn(dispatch, 'egresados'),
    titulados: createOnChangeFn(dispatch, 'titulados'),
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(TotalEstudiantes);
