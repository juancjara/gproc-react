import React from 'react';
import { connect } from 'react-redux';

import { context } from '../../../context';

import { actividadesServiciosUpdate}
  from '../../../actions/expresionInteres/carrera/aActividadesServicios';
import ActividadesServicios
  from '../../../components/expresionInteres/carrera/ActividadesServicios';

const title = 'Servicios de Bienestar de la Carrera';

const Servicios = ({
  servicios,
  onChange,
}) => (
  <ActividadesServicios
    number={7}
    title={title}
    subtitle={'En caso de tener más de una por año, separar con punto y coma (;) en cada campo'}
    body={servicios}
    editable={context.editable}
    header={'Nombre del Servicio'}
    onChange={onChange}
  />
);

const mapStateToProps = (
  state,
  { index }
) => ({
  servicios: state.getIn(['car', index, 'data', 'servicios']),
});

const mapDispatchToProps = (
  dispatch,
  { carreraId }
) => ({
  onChange: (r, c, v) =>
    dispatch(actividadesServiciosUpdate(carreraId, 'servicios', r, v))
});

export default connect(mapStateToProps, mapDispatchToProps)(Servicios);
