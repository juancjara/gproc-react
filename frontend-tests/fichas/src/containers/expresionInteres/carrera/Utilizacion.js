import React from 'react';
import { connect } from 'react-redux';

import BoxHeader from '../../../components/common/BoxHeader';
import TableDic from '../../../components/common/table/TableDic';
import { context } from'../../../context';

import {
  financiamientoUtilizacionUpdate
} from '../../../actions/expresionInteres/carrera/aFinanciamiento';
import * as messages from '../../../messages/common';

const title = 'Utilización de fondos en la Carrera';

const headers = [
  { key: 'label', display: 'Utilización de los fondos' },
  { key: '2015n', display: '2015' },
  { key: '2015p', display: '%' },
  { key: '2014n', display: '2014' },
  { key: '2014p', display: '%' },
];

const rowHeaders = [
  { s1: 'Salarios profesores/investigadores' },
  { s2: 'Salarios personal técnico y administrativo' },
  { bc: 'Bienes de capital' },
  { ob: 'Obras' },
  { ou: 'Otros usos' },
  { tt: 'TOTAL' },
];

const Utilizacion = ({
  destinoFondos,
  onChange,
}) =>(
  <div className="col-sm-6">
    <div className="box">
      <BoxHeader
        number={10}
        title={title}
        subtitle={'(miles de nuevos soles)'}
      />
      <div className="box-body">
        <TableDic
          className={'table table-condensed table-bordered table-centered'}
          editable={context.editable}
          headers={headers}
          rowHeaders={rowHeaders}
          body={destinoFondos}
          newColKey={'label'}
          headerCol= {['label']}
          readOnlyCol= {['label', 'tt', '2015p', '2014p']}
          readOnlyRow= {['tt']}
          onChange={onChange}
          errorMsg={messages.ERROR_NUMERIC_FIELD}
        />
      </div>
    </div>
  </div>
);

const mapStateToProps = (
  state,
  { index }
) => ({
  destinoFondos: state.getIn(['car', index, 'data', 'destino_fondos']),
});

const mapDispatchToProps = (
  dispatch,
  { carreraId }
) => ({
  onChange: (r, c, v) =>
    dispatch(financiamientoUtilizacionUpdate(carreraId, 'destino_fondos',
                                             r, c, v)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Utilizacion);
