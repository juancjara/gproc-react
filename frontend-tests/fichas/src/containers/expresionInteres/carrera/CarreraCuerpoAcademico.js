import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import BoxHeader from '../../../components/common/BoxHeader';
import CuerpoAcademicoDedicacion
  from '../../../components/expresionInteres/CuerpoAcademicoDedicacion';
import CuerpoAcademicoCondicion
  from '../../../components/expresionInteres/CuerpoAcademicoCondicion';
import CuerpoAcademicoDocente
from '../../../components/expresionInteres/CuerpoAcademicoDocente';

import { context } from '../../../context';
import {
  classErrorTotales,
  classErrorTotalUniversidad,
  getClassCondicion,
} from '../institucional/CuerpoAcademico';
import * as errors from '../../../errors/expresionInteres';

import {
  updateRowColSum,
  updateColSum,
} from '../../../actions/expresionInteres/carrera/aCarreraCuerpoAcademico';

const title = 'Cuerpo académico de la Carrera del año en curso';
const subtitle = '(consignar en números; registrar el mayor grado y/o título alcanzado por cada docente)';

const CarreraCuerpoAcademico = ({
  cuerpoAcademico,
  onChangeDedicacion,
  onChangeCondicion,
  onChangeDocentes,
  totalesError,
  totalErrorUniversidad,
}) => (
  <div className="box">
    <BoxHeader
      title={title}
      number={4}
      subtitle={subtitle}
    />

    <div className="box-body">
      <div className="row">

        <p>
          {errors.ERROR_CUERPO_ACADEMICO_TOTAL.message}
          {' '}

          {
            context.universidad ?
              errors.ERROR_CUERPO_ACADEMICO_DOCENTE_TOTAL.message:
              ''
          }
        </p>
        <CuerpoAcademicoDedicacion
          classTable={totalesError ? classErrorTotales: ''}
          data={cuerpoAcademico.get('grados_dedicacion')}
          onChange={onChangeDedicacion}
          editable={context.editable}
        />

        <CuerpoAcademicoCondicion
          classTable={getClassCondicion(totalesError, totalErrorUniversidad)}
          data={cuerpoAcademico.get('grados_condicion')}
          onChange={onChangeCondicion}
          editable={context.editable}
        />
        {
          context.universidad &&
          <div className="row">
            <CuerpoAcademicoDocente
              classTable={totalErrorUniversidad ? classErrorTotalUniversidad: ''}
              data={cuerpoAcademico.get('docentes_categoria')}
              onChange={onChangeDocentes}
              editable={context.editable}
            />
          </div>
        }
      </div>
    </div>
  </div>
);

const mapStateToProps = (
  state,
  {
    index,
    totalError,
    totalesErrorUniversidad,
  }
) => ({
  cuerpoAcademico: state.getIn(['car', index, 'data', 'cuerpo_academico']),
  totalError,
  totalesErrorUniversidad,
});

const mapDispatchToProps = (
  dispatch,
  { carreraId }
) => ({
  onChangeDedicacion: (r, c, v) => dispatch(updateRowColSum(carreraId, 'grados_dedicacion',
                                                            r, c, v)),
  onChangeCondicion: (r, c, v) => dispatch(updateRowColSum(carreraId, 'grados_condicion',
                                                            r, c, v)),
  onChangeDocentes: (r, c, v) => dispatch(updateColSum(carreraId, 'docentes_categoria',
                                                       r, c, v)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CarreraCuerpoAcademico);
