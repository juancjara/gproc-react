import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fromJS } from 'immutable';

import BoxHeader from '../../../components/common/BoxHeader';
import StaticField from '../../../components/common/form/StaticField';
import EditableField from '../../../components/common/form/EditableField';
import Comite from '../../../components/expresionInteres/common/Comite';
import { context } from '../../../context';

import {
  addMember,
  removeMember,
  updateMember,
} from '../../../actions/expresionInteres/carrera/aComite';

const CarreraComite = ({
  comite,
  removeMember,
  addMember,
  updateMember,
  editable,
}) =>(
  <div className="box">
    <BoxHeader
      number={2}
      title={'Miembros del Comité de Calidad de la Carrera'}
    />
    <Comite
      editable={editable}
      comite={comite}
      addMember={addMember}
      canBeRemoved={editable}
      removeMember={removeMember}
      onComiteUpdate={updateMember}
    />
  </div>
);

const mapStateToProps = (
  state,
  { index }
) => ({
  comite: state.getIn(['car', index, 'data', 'comite']),
  editable: context.editable && context.institucional
});

const mapDispatchToProps = (
  dispatch,
  { carreraId }
) => ({
  addMember: () => dispatch(addMember(carreraId)),
  updateMember: (r, c, v) => dispatch(updateMember(carreraId, r, c, v)),
  removeMember: (row) => dispatch(removeMember(carreraId, row)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CarreraComite);
