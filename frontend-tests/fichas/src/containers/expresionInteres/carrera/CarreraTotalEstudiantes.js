import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { formatTableDicDimensionFrom3To2 } from '../../../utils';
import { context } from '../../../context';

import BoxHeader from '../../../components/common/BoxHeader';
import CustomTableHeader
  from '../../../components/common/table/CustomTableHeader';
import TableBodyDic
  from '../../../components/common/table/TableBodyDic';
import { totalEstudiantesUpdate }
  from '../../../actions/expresionInteres/carrera/aTotalEstudiantes';
import * as messages from '../../../messages/common';
import { rowHeadersYears } from '../../../constants/expresionInteres';

const headers = [
  {key: 'nombre', display: 'Nombre'},
  {key: 'pa/m', display: 'M'},
  {key: 'pa/f', display: 'F'},
  {key: 'ms/m', display: 'M'},
  {key: 'ms/f', display: 'F'},
  {key: 'eg/m', display: 'M'},
  {key: 'eg/f', display: 'F'},
  {key: 'ti/m', display: 'M'},
  {key: 'ti/f', display: 'F'},
];

const customHeaders = [
  {
    display: headers[0].display,
    attr: {
      rowSpan: 2,
      className: 'col-xs-1',
    },
  },
  {
    display: 'Postulantes x Admisión',
    attr: {
      colSpan: 2,
    },
  },
  {
    display: 'Matriculados x Semestre',
    attr: {
      colSpan: 2,
    },
  },
  {
    display: 'Egresados',
    attr: {
      colSpan: 2,
    },
  },
  {
    display: 'Titulados',
    attr: {
      colSpan: 2,
    },
  },
];

class CarreraTotalEstudiantes extends React.Component {

  componentDidMount() {
    //console.log('total estudiantes mount');
  }

  render() {
    const {
      estudiantes,
      onChange,
    } = this.props;

    return (
        <div className="box total_estudiantes">
        <BoxHeader
          number={3}
          title={'Estudiantes de la Carrera (consignar en números)'}
        />

        <div className="box-body">
        <table className="table table-bordered table-condensed table-centered">
        <CustomTableHeader
          customHeaders={customHeaders}
          simpleHeaders={_.drop(headers)}
        />
        <TableBodyDic
          body={formatTableDicDimensionFrom3To2(estudiantes)}
          editable={context.editable}
          headers={headers}
          rowHeaders={rowHeadersYears}
          newColKey={'nombre'}
          headerCol={['nombre']}
          readOnlyCol={['nombre']}
          onChange={onChange}
          errorMsg={messages.ERROR_NUMERIC_FIELD}
        />
        </table>
        </div>
        </div>
    );
  }
}

const mapStateToProps = (
  state,
  { index }
) => ({
  estudiantes: state.getIn(['car', index, 'data', 'estudiantes']),
});

const mapDispatchToProps = (
  dispatch,
  { carreraId }
) => ({
  onChange: (r, c, v) => {
    const keys = c.split('/');
    dispatch(totalEstudiantesUpdate(carreraId, r, keys[0], keys[1], v));
  }
});


export default connect(mapStateToProps, mapDispatchToProps)(CarreraTotalEstudiantes);
