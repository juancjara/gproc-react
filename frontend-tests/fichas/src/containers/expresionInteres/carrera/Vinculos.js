import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fromJS } from 'immutable';

import BoxHeader from '../../../components/common/BoxHeader';
import Table from '../../../components/common/table/Table';
import AddButton from '../../../components/common/AddButton';
import { context } from '../../../context';
import patterns from '../../../patterns';
import * as messages from '../../../messages/common';

import {
  vinculosAdd,
  vinculosUpdate,
  vinculosRemove,
} from '../../../actions/expresionInteres/carrera/aVinculos';

const title = 'Vínculos interinstitucionales vigentes y activos involucrados con la Carrera';

const headers = /*fromJS*/([
  { key: 'ic', display: 'Institución contraparte' },
  { key: 'dv', display: 'Detalle del vínculo' },
]);

class Vinculos extends Component {
  render () {
    const {
      vinculos,
      onAdd,
      onRemove,
      onUpdate,
    } = this.props;

    return (
      <div className="box">
        <BoxHeader
          number={8}
          title={title}
        />
        <div className="box-body">
          <div className="row">
            <div className="col-sm-6">
              <Table
                className={"table table-condensed"}
                editable={context.editable}
                body={vinculos}
                headers={headers}
                canBeRemoved={context.editable}
                onRemove={onRemove}
                onChange={onUpdate}
                pattern={patterns.all}
                errorMsg={messages.ERROR_STRING_NA_SD_FIELD}
              />
              <AddButton
                onClick={onAdd}
                editable={context.editable}
                title={"Agregar vínculo"}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (
  state,
  { index }
) => ({
  vinculos: state.getIn(['car', index, 'data', 'vinculos']),
});

const mapDispatchToProps = (
  dispatch,
  { carreraId }
) => ({
  onAdd: () => dispatch(vinculosAdd(carreraId)),
  onRemove: (r) => dispatch(vinculosRemove(carreraId, r)),
  onUpdate: (r, c, v) => dispatch(vinculosUpdate(carreraId, r, c, v)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Vinculos);
