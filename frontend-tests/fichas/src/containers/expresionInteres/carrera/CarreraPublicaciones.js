import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import ButtonEditable from '../../../components/common/form/ButtonEditable';
import BoxError from '../../../components/common/BoxError';
import BoxHeader from '../../../components/common/BoxHeader';
import Investigaciones
  from '../../../components/expresionInteres/carrera/Investigaciones';
import Publicaciones
  from '../../../components/expresionInteres/carrera/Publicaciones';
import Patentes
from '../../../components/expresionInteres/carrera/Patentes';
import { context, getContext } from '../../../context';

import * as messages from '../../../messages/common';

import {
  publicacionesUpdate,
} from '../../../actions/expresionInteres/carrera/aPublicaciones';
import {
  fillBellowEmptyItems,
} from '../../../actions/expresionInteres/aCarreras';

const title = ' Investigaciones concluidas, publicaciones y patentes de la Carrera';
import { WARNING_FILL_DATA } from '../../../messages/expresionInteres';

const CarreraPublicaciones = ({
  investigaciones,
  publicaciones,
  patentes,
  onChangeInvestigaciones,
  onChangePublicaciones,
  onChangePatentes,
  fillEmpty,
  showWarningAutoFill
}) => (
  <div>
    {
      showWarningAutoFill && (
        <BoxError
        color="bg-navy"
          errors={[WARNING_FILL_DATA]}>
          <ButtonEditable
            editable
        className="btn btn-flat btn-danger btn-md"
            onClick={fillEmpty}
            text="Si no es así pulsee aquí"
          />
        </BoxError>
      )
    }

  <div className="box">
    <BoxHeader
      title={title}
      subtitle={'En caso de tener más de una por año, separar con punto y coma (;) en cada campo'}
      number={5}
    />

    <div className="box-body">
      <div className="row">
        <div className="col-md-6">
          <Investigaciones
            data={investigaciones}
            onChange={onChangeInvestigaciones}
            editable={context.editable}
            errorMsg={messages.ERROR_STRING_NA_SD_FIELD}
          />
        </div>

        <div className="col-md-6">
          <Publicaciones
            data={publicaciones}
            onChange={onChangePublicaciones}
            editable={context.editable}
            errorMsg={messages.ERROR_STRING_NA_SD_FIELD}
          />
        </div>

        <div className="col-md-6">
          <Patentes
            data={patentes}
            onChange={onChangePatentes}
            editable={context.editable}
            errorMsg={messages.ERROR_STRING_NA_SD_FIELD}
          />
        </div>
      </div>

    </div>

  </div>
    </div>
);

const shouldShowWarningAutoFill = context =>
  context.institucional && !context.universidad;

const mapStateToProps = (
  state,
  { index }
) => ({
  investigaciones: state.getIn(['car', index, 'data', 'inves']),
  publicaciones: state.getIn(['car', index, 'data', 'publi']),
  patentes: state.getIn(['car', index, 'data', 'paten']),
  showWarningAutoFill: shouldShowWarningAutoFill(getContext()),
});

const mapDispatchToProps = (
  dispatch,
  { carreraId }
) => ({
  onChangeInvestigaciones: (r, c, v) => dispatch(
    publicacionesUpdate(carreraId, 'inves', r, c, v)
  ),
  onChangePublicaciones: (r, c, v) => dispatch(
    publicacionesUpdate(carreraId, 'publi', r, c, v)
  ),
  onChangePatentes: (r, c, v) => dispatch(
    publicacionesUpdate(carreraId, 'paten', r, c, v)
  ),
  fillEmpty: () => {
    console.log('fill empty');
    dispatch(fillBellowEmptyItems());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(CarreraPublicaciones);
