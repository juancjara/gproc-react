import React from 'react';
import { connect } from 'react-redux';

import BoxHeader from '../../../components/common/BoxHeader';
import TableDic from '../../../components/common/table/TableDic';
import { context } from '../../../context';

import {
  financiamientoUtilizacionUpdate
} from '../../../actions/expresionInteres/carrera/aFinanciamiento';
import * as messages from '../../../messages/common';

const title = 'Financiamiento de la Carrera';

const headers = [
  { key: 'label', display: 'Origen de los fondos' },
  { key: '2015n', display: '2015' },
  { key: '2015p', display: '%' },
  { key: '2014n', display: '2014' },
  { key: '2014p', display: '%' },
];

const rowHeaders = [
  { pao: 'Presupuesto Anual ordinario (Tesoro Público)' },
  { rdr: 'Recursos Directamente Recaudados' },
  { itd: 'Ingresos por transferencias y donaciones' },
  { otr: 'Otros (especificar)' },
  { tt: 'TOTAL' },
];

const Financiamiento = ({
  origenFondos,
  onChange,
}) =>(
  <div className="col-sm-6">
    <div className="box">
      <BoxHeader
        number={9}
        title={title}
        subtitle={'(miles de nuevos soles)'}
      />
      <div className="box-body">
        <TableDic
          className={'table table-condensed table-bordered table-centered'}
          editable={context.editable}
          headers={headers}
          rowHeaders={rowHeaders}
          body={origenFondos}
          newColKey={'label'}
          headerCol= {['label']}
          readOnlyCol= {['label', 'tt', '2015p', '2014p']}
          readOnlyRow= {['tt']}
          onChange={onChange}
          errorMsg={messages.ERROR_NUMERIC_FIELD}
        />
      </div>
    </div>
  </div>
);

const mapStateToProps = (
  state,
  { index }
) => ({
  origenFondos: state.getIn(['car', index, 'data', 'origen_fondos']),
});

const mapDispatchToProps = (
  dispatch,
  { carreraId }
) => ({
  onChange: (r, c, v) =>
    dispatch(financiamientoUtilizacionUpdate(carreraId, 'origen_fondos',
                                             r, c, v)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Financiamiento);
