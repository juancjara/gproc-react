import React from 'react';
import { connect } from 'react-redux';

import BoxHeader from '../../../components/common/BoxHeader';
import StaticField from '../../../components/common/form/StaticField';
import EditableField from '../../../components/common/form/EditableField';
import { context } from '../../../context';
import { findIndexBy } from '../../../utils';

import { carreraAreaUpdate }
  from '../../../actions/expresionInteres/carrera/aCarrera';

const AreaAcademica = ({
  nombre,
  area,
  carreraUpdate,
}) =>(
  <div className="box">
    <BoxHeader
      number={1}
      title={'Carrera / Área Académica'}
    />
    <div className="box-body">
      <StaticField
        label={'Carrera *'}
        value={nombre}
      />
      <EditableField
        editable={context.editable && !context.institucional}
        label={'Área Académica *'}
        value={area}
        name={'area'}
        onChange={carreraUpdate}
        required
      />
    </div>
  </div>
);

const getCarreraProp = (
  state,
  carreraId,
  prop
) => {
  const sedeId = Math.floor(carreraId / 1000);
  const sedeIndex = findIndexBy(state.getIn(['ins', 'data', 'sedes']), sedeId, e => e.get('id'));

  const carreras = state.getIn(
    ['ins', 'data', 'sedes', sedeIndex, 'carreras']
  );
  const carIndex = findIndexBy(carreras, carreraId, (e) => e.get('id'));

  return carreras.getIn([carIndex, prop]);
};

const getArea = (
  state,
  carreraId
) => {
  if (context.institucional) {
    return getCarreraProp(state, carreraId, 'area');
  }
  return state.getIn(['car', 0, 'data', 'area']);
};

const getNombre = (
  state,
  carreraId
) => {
  if (context.editable && context.institucional) {
    return getCarreraProp(state, carreraId, 'nombre');
  }
  return state.getIn(['car', 0, 'data', 'nombre']);
};

const mapStateToProps = (
  state,
  {
    index,
    carreraId
  }
) => ({
  nombre: getNombre(state, carreraId),
  area: getArea(state, carreraId),
  editable: context.institucional && context.editable,
});

const mapDispatchToProps = (
  dispatch,
  {
    carreraId,
    index,
  }
) => ({
  carreraUpdate: (e) => dispatch(carreraAreaUpdate(carreraId, e.target.value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AreaAcademica);
