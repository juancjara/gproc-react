import React from 'react';
import { connect } from 'react-redux';

import { context } from '../../../context';

import { actividadesServiciosUpdate}
  from '../../../actions/expresionInteres/carrera/aActividadesServicios';
import ActividadesServicios
  from '../../../components/expresionInteres/carrera/ActividadesServicios';

import * as messages from '../../../messages/common';

const title = 'Actividades de Extensión y Responsabilidad Social de la Carrera';

const Actividades = ({
  actividades,
  onChange,
}) => (
  <ActividadesServicios
    number={6}
    title={title}
    subtitle = {'En caso de tener más de una por año, separar con punto y coma (;) en cada campo'}
    body={actividades}
    editable={context.editable}
    header={'Nombre de la actividad'}
    onChange={onChange}
    errorMsg={messages.ERROR_STRING_NA_SD_FIELD}
  />
);

const mapStateToProps = (
  state,
  { index }
) => ({
  actividades: state.getIn(['car', index, 'data', 'actividades']),
});

const mapDispatchToProps = (
  dispatch,
  { carreraId }
) => ({
  onChange: (r, c, v) =>
    dispatch(actividadesServiciosUpdate(carreraId, 'actividades', r, v))
});

export default connect(mapStateToProps, mapDispatchToProps)(Actividades);
