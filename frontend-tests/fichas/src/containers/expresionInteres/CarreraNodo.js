import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { context } from '../../context';
import SectionTitle from '../../components/expresionInteres/common/SectionTitle';
import ActionButtons from '../../components/common/status/ActionButtons';
import StatusHeader from '../../components/expresionInteres/StatusHeader';
import connectStatusStore from '../common/connectStatus';
import { connectStatus } from '../common/connectStatus';
import BoxError from '../../components/common/BoxError';

import AreaAcademica from './carrera/AreaAcademica';
import CarreraComite from './carrera/CarreraComite';
import CarreraTotalEstudiantes from './carrera/CarreraTotalEstudiantes';
import CarreraCuerpoAcademico from './carrera/CarreraCuerpoAcademico';
import CarreraPublicaciones from './carrera/CarreraPublicaciones';
import Actividades from './carrera/Actividades';
import Servicios from './carrera/Servicios';
import Vinculos from './carrera/Vinculos';
import Financiamiento from './carrera/Financiamiento';
import Utilizacion from './carrera/Utilizacion';
import { APIstoreNodo } from '../../webAPI/expresionInteres';
import { storeNodo } from '../../actions/aNodoStatus';
import * as titles from '../../titles/expresionInteres';
import { getBoxErrors, errorExists } from '../../utils';
import {
  ERROR_CUERPO_ACADEMICO_TOTAL,
  ERROR_CUERPO_ACADEMICO_DOCENTE_TOTAL,
} from '../../errors/expresionInteres';

class Carrera extends Component {

  componentDidMount () {
    console.log('carrera_mount');
  }

  render () {
    const {
      carreraId,
      index,
      rootId,
      ...props,
    } = this.props;

    return (
      <div id={rootId}>
        <div className="pull-left">
          {
            context.editable &&
              <ActionButtons {...props} />
          }
        </div>

        <SectionTitle
          title={`Sección B - Presentación de la${ context.institucional? 's': '' } Carrera${ context.institucional ? 's': '' }`}
          subtitle={context.institucional ? 'En el caso de los Planes de Mejora Institucional, se registrará la información correspondiente por cada una de las Carreras ofrecidas': ''}
        />

        {
          context.editable &&
            <StatusHeader {...props} />
        }
        <hr/>
        <AreaAcademica
          carreraId={carreraId}
          index={index}
        />
        <CarreraComite
          carreraId={carreraId}
          index={index}
        />
        <BoxError
          errors={getBoxErrors(titles.COMITE, props.errors)}
        />
        <CarreraTotalEstudiantes
          carreraId={carreraId}
          index={index}
        />
        <CarreraCuerpoAcademico
          totalesError={errorExists(ERROR_CUERPO_ACADEMICO_TOTAL, props.errors)}
          totalErrorUniversidad={errorExists(ERROR_CUERPO_ACADEMICO_DOCENTE_TOTAL, props.errors)}
          carreraId={carreraId}
          index={index}
        />
        <BoxError
          errors={getBoxErrors(titles.CUERPO_ACADEMICO, props.errors)}
        />
        <CarreraPublicaciones
          carreraId={carreraId}
          index={index}
        />
        <div className="row">

          <Actividades
            carreraId={carreraId}
            index={index}
          />
          <Servicios
            carreraId={carreraId}
            index={index}
          />

        </div>
        <Vinculos
          carreraId={carreraId}
          index={index}
        />
        <div className="row">

          <Financiamiento
            carreraId={carreraId}
            index={index}
          />
          <Utilizacion
            carreraId={carreraId}
            index={index}
          />

        </div>

        {
          context.editable &&
            <ActionButtons {...props} />
        }
      </div>
    );
  }
};

const CarreraNodo = ({
  carreraId,
  index
}) => {
  const CarreraConnected = connect(
    (state) => ({
      path: ['car', index],
      status: state.getIn(['car', index, 'status']),
    }),
    (dispatch) => ({
      storeNodo: () => dispatch(storeNodo({
        webAPI: APIstoreNodo,
        path: ['car', index]
      }))
    })
  )(connectStatus({carreraId, index})(Carrera));

  return <CarreraConnected />;
};

export default CarreraNodo;
