import React from 'react';

import SectionTitle from '../../components/expresionInteres/common/SectionTitle';
import Comite from './institucional/Comite';
import Institucion from './institucional/Institucion';
import Sedes from './institucional/Sedes';
import CuerpoAcademico from './institucional/CuerpoAcademico';
import TotalEstudiantes from './institucional/TotalEstudiantes';

import * as titles from '../../titles/expresionInteres';
import { context } from '../../context';
import BoxError from '../../components/common/BoxError';
import ActionButtons from '../../components/common/status/ActionButtons';
import StatusHeader from '../../components/expresionInteres/StatusHeader';
import connectStatusStore from '../common/connectStatus';
import { institucionalValidation } from '../../validations/expresionInteres';
import { APIstoreNodo } from '../../webAPI/expresionInteres';
import { getBoxErrors, errorExists } from '../../utils';
import {
  ERROR_CUERPO_ACADEMICO_TOTAL,
  ERROR_CUERPO_ACADEMICO_DOCENTE_TOTAL,
} from '../../errors/expresionInteres';

const DatosInstitucionalesNodo = (
  props
) => (
  <div id={props.rootId}>
    <div className="pull-left">
      {
        context.editable &&
          <ActionButtons {...props} />
      }
    </div>

    <SectionTitle
      title={'Sección A - Datos Institucionales'}
      subtitle={'Esta sección es aplicable para todas y cada una de las categorías de Planes de Mejora'}
    />
    {
      context.editable &&
        <StatusHeader {...props} />
    }
    <hr/>

    <Comite />
    <BoxError
      errors={getBoxErrors(titles.COMITE, props.errors)}
    />
    <Institucion />

    <Sedes />
    <BoxError
      errors={getBoxErrors(titles.SEDE, props.errors)}
    />
    <CuerpoAcademico
      totalesError={errorExists(ERROR_CUERPO_ACADEMICO_TOTAL, props.errors)}
  totalErrorUniversidad={errorExists(ERROR_CUERPO_ACADEMICO_DOCENTE_TOTAL, props.errors)}
    />
    <BoxError
      errors={getBoxErrors(titles.CUERPO_ACADEMICO, props.errors)}
    />
    <TotalEstudiantes />

    {
      context.editable &&
        <ActionButtons {...props} />
    }
  </div>
);

export default connectStatusStore({
  path: ['ins'],
  webAPI: APIstoreNodo,
  fnValidation: institucionalValidation,
})(DatosInstitucionalesNodo);
