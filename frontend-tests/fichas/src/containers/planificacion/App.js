import React from 'react';
import { connect } from 'react-redux';

import Alert from '../../components/common/Alert';
import ResumenTable from './ResumenTable';
import DetalleTable from './DetalleTable';
import TablePlanificationNodo from './TablePlanificacionNodo';
import FichaNodo from './FichaNodo';
import { getContext } from '../../context';
import SystemStatus from './SystemStatus';

const App = () => (
  <div className="form-horizontal ficha">
    {
      (getContext().canEdit || getContext().canAddOrRemove) &&
      <SystemStatus />

    }
    <DetalleTable />
    <TablePlanificationNodo />
    <FichaNodo />
    {
      (getContext().canEdit || getContext().canAddOrRemove) &&
       <SystemStatus dummyComponent />

    }
  </div>
);

export default connect()(App);
