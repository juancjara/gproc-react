import React, { Component, createElement } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import ReactDOM from 'react-dom';

import connectSystemStatus from '../common/connectSystemStatus';
import * as messagesPlanificacion from '../../messages/planificacion';
import { groupByDimension } from './tablePlanificacion/TableBodyPlanificacion';
import { getContext } from '../../context';
import { summaryStatus } from '../../utils/status';
import StatusBadges from '../../components/common/status/StatusBadges';
import {
  finishFicha,
  showErrorsAndSaveAllNodes
} from '../../actions/planificacion/aSystem';

import actions from '../../constants/status';
const {
  STORED_CHANGED,
  SERVER_ERROR,
  FETCHING,
  STORED_OK,
  UNAUTHORIZED_ERROR,
} = actions;

import constantsPlanificacion from '../../constants/planificacion';
const {
  REQUEST_ADD_ACTIVIDAD,
  REQUEST_REMOVE_ACTIVIDAD,
  REQUEST_SAVE_ALL,
} = constantsPlanificacion;

const getCustomAlertMessage  = ({ requestType , status }) => {
  const saveOK = status === STORED_OK || status === STORED_CHANGED;
  if (saveOK && requestType === REQUEST_ADD_ACTIVIDAD) {
    return {
      alertMessage: messagesPlanificacion.ADD_ACTIVIDAD_OK,
      type: 'success',
    };
  };

  if ( saveOK && requestType === REQUEST_REMOVE_ACTIVIDAD) {
      return {
        alertMessage: messagesPlanificacion.REMOVE_ACTIVIDAD_OK,
        type: 'success',
      };
  };

  return null;
};

const getStatus = ({
  nodoState,
  nameTemplate,
  pathSelector,
}) =>
  nodoState.map(nodo => ({
    name: nameTemplate + nodo.getIn(pathSelector),
    status: nodo.get('status').toJS(),
    dbId: nodo.get('dbId'),
  }));

// @TODO refactor visibleIndex, and more stuff
const flattenStatus = (state) => {
  const factoresGrouped = groupByDimension(state.get('fac'));

  let visibleIndex = 0;
  const dimensionesStatus = getStatus({
    nodoState: state.get('dim'),
    pathSelector: ['data', 'dime_codigo'],
  })
    .map(nodo => ({
      ...nodo,
      name: 'O.E. ' + (
          factoresGrouped.get(nodo.dbId)
            .filter(f => !f.get('hide'))
            .size === 0 ? -1: ++visibleIndex
        ),
    }));

  visibleIndex = 0;
  const factoresStatus = state.get('fac')
    .map(f => {
      const factIdx = f.getIn(['data', 'hide']) ? -1: ++visibleIndex;
      return {
        dbId: f.get('dbId'),
        status: f.get('status').toJS(),
        visibleIndex: factIdx,
        fact_codigo: f.getIn(['data', 'fact_codigo']),
        name: 'R. ' + factIdx,
      }
    }).toJS();

  let actividadesStatus = [];
  const actGroupedByFactor = state.get('act')
    .groupBy(act => act.getIn(['data', 'fact_codigo']));

  _.forEach(factoresStatus, f => {
    if (actGroupedByFactor.has(f.fact_codigo)) {
      actGroupedByFactor.get(f.fact_codigo)
        .forEach((act, i) => {
          actividadesStatus.push({
            status: act.get('status').toJS(),
            name: `A.${f.visibleIndex}.${i + 1}`,
          });
        }
      );
    }
  });

  return [
    ...dimensionesStatus,
    ...factoresStatus,
    ...actividadesStatus,
    {
      name: 'Datos del responsable',
      status: state.getIn(['fic', 'status']).toJS()
    }
  ];
};

export const getSeccionesError = (state) =>
  _(state)
     .map(({ name, status }) => {
       if (status.errors.length) {
         return `${name}`;
       }
       return '';
     })
    .filter(error => error.length)
    .value();

const mapStateToProps = (
  state
) => {
  const flatStatus = flattenStatus(state);
  const secciones = getSeccionesError(flatStatus);
  const statuses = _(flatStatus).pluck('status').value();
  return {
    status: summaryStatus(statuses, secciones.length),
    secciones,
    requestType: state.get('requestType'),
    urlPDF: '/planificacion/pdf',
    getCustomAlertMessage,
  };
};

const mapDispatchToProps = (
  dispatch
) => ({
  onSaveAll: () => dispatch(showErrorsAndSaveAllNodes()),
  onFinish: (status) => dispatch(finishFicha(status)),
});

export default connectSystemStatus(mapStateToProps, mapDispatchToProps);
