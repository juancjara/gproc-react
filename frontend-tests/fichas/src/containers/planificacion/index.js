import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import { fromJS } from 'immutable';

import App from './App';
import ProgressBar from './ProgressBar';
import { setContext } from '../../context';
import configureStore from '../../store/planificacion';

import getInitialData from '../../initialData/planificacion';

const context = window.__CONTEXT__;
const serverRender = window.__SERVER_RENDER__;
let initialState = window.__INITIAL_STATE__;

if ( ! serverRender) {
  initialState = getInitialData(initialState, context);
}

setContext(context);

const store = configureStore(initialState);

if (context.canEdit) {
  ReactDOM.render(
    <Provider store={store} >
      <ProgressBar />
    </Provider>,
    document.getElementById('root-progress')
  );
}

ReactDOM.render(
  <Provider store={store} >
    <App />
  </Provider>,
  document.getElementById('root')
);
