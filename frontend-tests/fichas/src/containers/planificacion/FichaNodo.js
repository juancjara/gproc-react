import React from 'react';

import connectStatusStore from '../common/connectStatus';
import { getContext } from '../../context';
import DatosFirmaNodo from './DatosFirmaNodo';

const FichaNodo = ({
  summaryStatus,
  ...props,
}) => (
  <div id={props.rootId}>
    <DatosFirmaNodo
      status={summaryStatus}
      editable={getContext().canEdit || getContext().editable}
    />
  </div>
);

export default connectStatusStore({
  path: ['fic'],
  webAPI: () => {console.log('store')}
})(FichaNodo);
