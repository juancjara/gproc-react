import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Map, is } from 'immutable';
import _ from 'lodash';

import BoxError from '../../../components/common/BoxError';
import ActividadNodo from './actividad/ActividadNodo';
import {
  addActividad,
} from '../../../actions/planificacion/aActividades';

import {
  toggleHideFactor,
} from '../../../actions/planificacion/aFactores';

import {
  isDisabledPlanificacionAddActividad,
} from '../../../utils';

import { getBoxErrors } from '../../../utils';
import { getContext } from '../../../context';
import AddButton from '../../../components/common/AddButton';
import ButtonEditable from '../../../components/common/form/ButtonEditable';

class Actividades extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    const otherPropsChanged = !_.isEqual(
      _.omit(nextProps, ['actividades']),
      _.omit(this.props, ['actividades'])
    );

    const actividadesNotDeepEqual = !_.isEqual(
      this.props.actividades.toJS(),
      nextProps.actividades.toJS()
    );

    return otherPropsChanged || actividadesNotDeepEqual;
  }

  getToggleButtonClass() {
    return 'btn btn-flat btn-sm btn-' +
      (this.props.hideFactor ?
        'success' :
        'danger');
  }

  getToggleButtonText() {
    return this.props.hideFactor ?
      'Reestabler resultado':
      'Eliminar resultado';
  }

  render() {
    const {
      actividades,
      factorIndex,
      factorVisibleIndex,
      addActividad,
      toggleHideFactor,
      hideFactor,
      factorDbId,
      factorCanBeHide,
      isDisabled,
      errors,
    } = this.props;

  const noActividades = !actividades.size;

  return (
    <td>
      <BoxError
        errors={errors}
      />
      {
        factorCanBeHide && (
          <ButtonEditable
            className={this.getToggleButtonClass()}
            icon= {`${hideFactor ? 'undo': 'times'}`}
            onClick={toggleHideFactor}
            text={this.getToggleButtonText()}
            editable={getContext().canEdit}
            disabled={isDisabled}
          />
        )
      }
      { !hideFactor && (
        <div>
          {
          actividades.map((actividad, i) => (
            <ActividadNodo
              key={actividad.get('id')}
              factorIndex={factorIndex}
              factorVisibleIndex={factorVisibleIndex}
              actividadIndex={actividad.get('index')}
              iterationIndex={i}
              factorDbId={factorDbId}
            />
          ))
          }
          {
            noActividades &&
              <AddButton
                onClick={addActividad}
                editable={getContext().canAddOrRemove}
                text={' Agregar actividad'}
                size={'sm'}
                disabled={isDisabled}
              />
          }
          </div>
        )
      }
    </td>
    );
  }
}


const getActividades = (
  state,
  factorDbId
) =>
  state.get('act')
    .map((actividad, i) => Map({
      parent_id: actividad.get('parent_id'),
      id: actividad.getIn(['data', 'id']),
      index: i,
    }))
    .filter(actividad => actividad.get('parent_id') === factorDbId);

const mapStateToProps = (
  state,
  {
    factorDbId,
    factorIndex,
    factorVisibleIndex,
    hideFactor,
    factorCanBeHide,
  }
) => ({
  factorDbId,
  actividades: getActividades(state, factorDbId),
  factorIndex,
  hideFactor,
  isDisabled: isDisabledPlanificacionAddActividad(state.get('fac')),
  factorVisibleIndex,
  errors: getBoxErrors(
    state.getIn(['fac', factorIndex, 'data', 'fact_codigo']),
    state.getIn(['fac', factorIndex, 'status']).toJS().errors
  ),
});

const mapDispatchToProps = (
  dispatch,
  {
    factorDbId,
    factorIndex,
  }
) => ({
  addActividad: () => dispatch(addActividad(0, factorDbId)),
  toggleHideFactor: () => dispatch(toggleHideFactor(factorIndex)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Actividades);
