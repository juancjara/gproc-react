import React from 'react';
import { Map } from 'immutable';
import { connect } from 'react-redux';

import RowPlanificacion from './RowPlanificacion';

const TableBodyPlanificacion = ({
  dimensiones,
  factoresGrouped,
}) => (
  <tbody>
  {
    dimensiones
      .flatMap((dimension, dimensionIndex) =>
        factoresGrouped.get(dimension.get('dbId'))
          .map((factor, i) => (
            <RowPlanificacion
              key={`${dimensionIndex}.${i}`}
              firstRow={i === 0}
              factorDbId={factor.get('dbId')}
              dimensionVisibleIndex={dimension.get('visibleIndex')}
              dimensionIndex={dimensionIndex}
              factorIndex={factor.get('index')}
              factorVisibleIndex={factor.get('visibleIndex')}
            />
           )
        ))
  }
  </tbody>
);

export const groupByDimension = (
  factores
) => {
  let visibleIndex = 0;
  return factores
    .map((factor, i) => Map({
      dbId: factor.get('dbId'),
      dimension: factor.get('parent_id'),
      index: i,
      visibleIndex: factor.getIn(['data', 'hide']) ? -1: ++visibleIndex,
      hide: factor.getIn(['data', 'hide'])
    }))
    .groupBy(factor => factor.get('dimension'));
}
const getDimensiones = (dimensiones, factoresGrouped) => {
  let visibleIndex = 0;
  return dimensiones
    .map(dimension =>
      dimension.set(
        'visibleIndex',
        factoresGrouped.get(dimension.get('dbId'))
          .filter(f => !f.get('hide'))
          .size === 0 ? -1: ++visibleIndex
      )
    );
};

const mapStateToProps = (
  state
) => {
  const factoresGrouped = groupByDimension(state.get('fac'));

  return {
    dimensiones: getDimensiones(state.get('dim'), factoresGrouped),
    factoresGrouped,
  };
};

export default connect(mapStateToProps)(TableBodyPlanificacion);
