import React , { Component } from 'react';
import { connect } from 'react-redux';

import Actividad from './Actividad';
import { connectStatus } from '../../../common/connectStatus';

const ActividadNodo = (
  props
) => {
  const index = props.actividadIndex;
  const path = ['act', index];

  const ActividadConnected = connect(
    (state) => ({
      path,
      status: state.getIn(path.concat(['status']))
    }),
    (dispatch) => ({
      storeNodo: () => () => {console.log('store')},
    })
  )(connectStatus(props)(Actividad));

  return <ActividadConnected />
};

export default ActividadNodo;
