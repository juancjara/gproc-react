import React from 'react';
import { connect } from 'react-redux';
import { Map } from 'immutable';

import AddButton from '../../../../components/common/AddButton';

import Estandar from './Estandar';
import { getContext } from '../../../../context';
import { findIndexBy } from '../../../../utils';

import {
  selectEstandar,
  addEstandar,
} from '../../../../actions/planificacion/actividad/aActividadesEstandares';

const Estandares = ({
  fact_codigo,
  estandares,
  usedEstandares,
  updateEstandar,
  addEstandar,
  cannotAdd,
  actividadIndex,
}) => (
  <tr>
    <td colSpan="6">
      {
        getContext().canEdit && 
          <span>Seleccione Estándares con los que se relaciona la actividad:</span>
      }
      <table>
        <tbody>
          {
            estandares.map((estandar, i) =>
              getContext().canEdit ?
                (
                  <Estandar
                    key={estandar.get('id')}
                    selected={estandar.get('esta_id')}
                    actividadEstandarIndex={i}
                    fact_codigo={fact_codigo}
                    usedEstandares={usedEstandares}
                    updateEstandar={updateEstandar}
                    actividadIndex={actividadIndex}
                    editable={getContext.canEdit}
                  />
                ) :
                <tr key={i}><td>{estandar.get('display')}</td></tr>
            )
          }
        </tbody>
      </table>
      <AddButton
        size={'xs'}
        onClick={addEstandar}
        editable={getContext().canAddOrRemove}
        disabled={cannotAdd}
        title="Agregar estándar"
      />
    </td>
  </tr>
);

const estandaresInFactor = ({
  state,
  fact_codigo,
}) => state
  .get('estandares')
  .filter(estandar => estandar.get('fact_codigo') === fact_codigo);

const getDisplayEstandares = ({
  estandares,
  estandaresState,
}) => estandares
  .map(estandar => {
    console.log(estandar.toJS());
    const index = findIndexBy(
      estandaresState,
      estandar.get('esta_id'),
      e => e.get('esta_id')
    );
    if (index === -1) return Map({display: 'No seleccionado'});
    return estandaresState.get(index);
  })

const getEstandares = ({
  estandares,
  state,
}) => getContext().canEdit ?
  estandares :
  getDisplayEstandares({ estandares, estandaresState: state.get('estandares') });

const mapStateToProps = (
  state,
  {
    actividadIndex,
    fact_codigo,
  }
) => {
  const estandares = state
    .getIn(['act', actividadIndex, 'data', 'estandares']);
  return {
    estandares: getEstandares({ estandares, state }),
    fact_codigo,
    usedEstandares: estandares.map(estandar => estandar.get('esta_id')),
    cannotAdd: estandares.size === estandaresInFactor({ state, fact_codigo }).size,
    actividadIndex,
  };
};

const mapDispatchToProps = (
  dispatch,
  { actividadIndex }
) => ({
  updateEstandar: (estandarIndex, value) =>
    dispatch(selectEstandar(actividadIndex, estandarIndex, value)),
    addEstandar: () => dispatch(addEstandar(actividadIndex)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Estandares);
