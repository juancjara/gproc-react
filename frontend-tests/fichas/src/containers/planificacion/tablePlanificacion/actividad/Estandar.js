
import React, { Component } from 'react';
import { connect } from 'react-redux';

import RemoveButton from '../../../../components/common/RemoveButton';

import {
  removeEstandar,
} from '../../../../actions/planificacion/actividad/aActividadesEstandares';

import SelectEstandar
  from '../../../../components/planificacion/actividad/SelectEstandar';
import { getContext } from '../../../../context';

export const estandaresGroup = [
  {
    key: 'Obligatorios',
    display: '-Obligatorios',
  },
  {
    key: 'Opcionales',
    display: '-Opcionales',
  },
];

const Estandar = ({
  selected,
  fact_codigo,
  estandaresFactor,
  onChange,
  removeEstandar,
  editable,
}) => (
  <tr>
    <td>
      <SelectEstandar
        selected={selected}
        onChange={onChange}
        estandaresGroup={estandaresGroup}
        estandaresFactor={estandaresFactor}
        fact_codigo={fact_codigo}
      />
    </td>
    <td>
      <RemoveButton
        size={'sm'}
        onClick={removeEstandar}
        title={'Remover estándar'}
        editable={getContext().canAddOrRemove}
      />
    </td>
  </tr>
);

const groupByFactorFilteredBy = (
  state,
  filterBy
) => state
  .get('estandares')
  .filter(filterBy)
  .groupBy(estandar => estandar.get('tipo'));

const filterNotUsedAndFactor = (
  fact_codigo,
  selected,
  usedEstandares,
  estandar
) => estandar.get('fact_codigo') === fact_codigo &&
  usedEstandares
    .filter(used =>
      estandar.get('esta_id') === used && used !== selected
    )
    .size === 0;

const mapStateToProps = (
  state,
  {
    fact_codigo,
    usedEstandares,
    selected,
  }
) => ({
  selected,
  fact_codigo,
  estandaresFactor: groupByFactorFilteredBy(
    state,
    _.partial(filterNotUsedAndFactor, fact_codigo, selected,
              usedEstandares, _)
  )
});

const mapDispatchToProps = (
  dispatch,
  {
    actividadEstandarIndex,
    updateEstandar,
    actividadIndex,
  }
) => ({
  onChange: ({ target }) => updateEstandar(actividadEstandarIndex, target.value),
  removeEstandar: () =>
    dispatch(removeEstandar(actividadIndex, actividadEstandarIndex))
});


export default connect(mapStateToProps, mapDispatchToProps)(Estandar);
