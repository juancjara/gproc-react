import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { fromJS, Map } from 'immutable';

import AddButton from '../../../../components/common/AddButton';
import ActividadFirstRow
  from '../../../../components/planificacion/actividad/ActividadFirstRow';
import DisplayCell from '../../../../components/common/table/DisplayCell';
import Row from '../../../../components/common/table/Row';
import RemoveButton from '../../../../components/common/RemoveButton';

import Estandares from './Estandares';

import {
  updateActividad,
} from '../../../../actions/planificacion/actividad/aActividad';

import {
  addActividad,
  removeActividad,
} from '../../../../actions/planificacion/aActividades';
import {
  isDisabledPlanificacionAddActividad,
} from '../../../../utils';

import patterns from '../../../../patterns';
import { getContext } from '../../../../context';

import {
  getFieldTitles,
} from '../../ResumenTable';

import {
  getTitleId,
  updateTotals,
} from '../../DetalleTable';

const Actividad = ({
  addActividad,
  title,
  headers,
  miactividad,
  fact_codigo,
  onUpdate,
  summaryRow,
  actividadIndex,
  onActorUpdate,
  isDisabled,
  removeActividad,
  rootId,
  summaryStatus,
  ...status,
}) => (
  <div id={rootId}>
    <table className="table table-condensed table-bordered table-resumen">
      <tbody>
        <ActividadFirstRow
          status={summaryStatus}
          editable={getContext().canEdit}
          title={title}
          miactividad={miactividad}
          onChange={onUpdate}
          onRemove={removeActividad}
          editableBtn={getContext().canAddOrRemove}
          disabledBtn={isDisabled}
        />
        <tr>
        {
          headers.map(({ display }, i) => (
            <DisplayCell
              className="text-center"
              key={i}
              value={display}
              isReadOnly
            />
          ))
        }
        </tr>
        <Row
          headers={headers}
          readOnlyCol={_(headers).pluck('key').dropRight().value()}
          indexRow={1}
          onChange={onActorUpdate}
          canBeRemoved={false}
          row={summaryRow}
          editable={getContext().canEdit}
          pattern={patterns.all}
          className="row-resumen"
        />
        <Estandares
          fact_codigo={fact_codigo}
          actividadIndex={actividadIndex}
        />
      </tbody>
    </table>

    <AddButton
      onClick={addActividad}
      editable={getContext().canAddOrRemove}
      text={' Agregar actividad'}
      size={'sm'}
      disabled={isDisabled}
    />
  </div>
);

const getHeaders = _.memoize(
  () =>
    getFieldTitles()
    .map((title, i) => ({
      key: getTitleId(i),
      display: title.display
    }))
    .concat([
      {key: 'total', display: 'Total actividad'},
      {key: 'actor_cargo', display: 'Responsable de la actividad'},
    ])
);

const formatSummaryRow = ({
  state,
  index
}) => {
  const totalesPlan = state.getIn(['act', index, 'detalleTotales', 'plan']).toJS();
  return fromJS(updateTotals(totalesPlan)).merge(Map({
    actor_cargo: state.getIn(['act', index, 'data', 'actor_cargo'])
  }));
}

const mapStateToProps = (
  state,
  {
    actividadIndex,
    factorIndex,
    factorVisibleIndex,
    iterationIndex,
  }
) => ({
  title: `A.${factorVisibleIndex}.${iterationIndex + 1}`,
  fact_codigo: state.getIn(['fac', factorIndex, 'data', 'fact_codigo']),
  miactividad: state.getIn(['act', actividadIndex, 'data', 'miactividad']),
  summaryRow: formatSummaryRow({ state, index: actividadIndex }),
  actividadIndex,
  headers: getHeaders(),
  isDisabled: isDisabledPlanificacionAddActividad(state.get('fac')),
});

const mapDispatchToProps = (
  dispatch,
  {
    actividadIndex,
    factorDbId,
  }
) => ({
  onUpdate: ({ target }) =>
    dispatch(updateActividad(actividadIndex, target.name, target.value)),
  onActorUpdate: (r, c, v) => dispatch(updateActividad(actividadIndex, c, v )),
  removeActividad: () => dispatch(removeActividad(actividadIndex)),
  addActividad: () => dispatch(addActividad(actividadIndex + 1, factorDbId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Actividad);
