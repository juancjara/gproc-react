import React from 'react';
import { connect } from 'react-redux';

import StatusBadges from '../../../components/common/status/StatusBadges';
import { getContext } from '../../../context';
import Factor from './Factor';
import RowHeader from '../../../components/planificacion/RowHeader';

import { updateDimension } from '../../../actions/planificacion/aDimensiones';

const Dimension = ({
  objetivo,
  onUpdate,
  title,
  rowSpan,
  allFactorsHide,
  visibleIndex,
  rootId,
  summaryStatus,
  ...status,
}) => (
    <td rowSpan={rowSpan} id={rootId}>
    {
      getContext().canEdit &&
        <StatusBadges status={summaryStatus} />
    }
    <RowHeader
      editable={getContext().canEdit}
      title={`${title}`}
      cod={`O.E.${visibleIndex}`}
      onChange={onUpdate}
      value={objetivo}
      name={'objetivo'}
      hideEdit={allFactorsHide}
    />
  </td>
);

const factoresInDimension = ({
  state,
  index,
}) => {
  const dimensionDbId = state.getIn(['dim', index, 'dbId']);
  return state.get('fac')
    .map((factor, i) => ({
      index: i,
      parent_id: factor.get('parent_id'),
      hide: factor.getIn(['data', 'hide']),
    }))
    .filter(({ parent_id }) => parent_id === dimensionDbId)
};

const mapStateToProps = (
  state,
  {
    index,
    visibleIndex,
  }
) => {
  const factores = factoresInDimension({ state, index });

  return {
    objetivo: state.getIn(['dim', index, 'data', 'objetivo']),
    rowSpan: factores.size,
    title: getContext().dimensiones[index].display,
    allFactorsHide: factores.filter(({ hide }) => !hide).size === 0,
  }
};

const mapDispatchToProps = (
  dispatch,
  { index }
) => ({
  onUpdate: ({ target }) =>
    dispatch(updateDimension(index, target.name, target.value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dimension);
