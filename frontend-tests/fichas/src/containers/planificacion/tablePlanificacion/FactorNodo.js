import React , { Component } from 'react';
import { connect } from 'react-redux';

import Factor from './Factor';
import { connectStatus } from '../../common/connectStatus';

const FactorNodo = (
  props
) => {
  const index = props.index
  const path = ['fac', index];

  const FactorConnected = connect(
    (state) => ({
      path,
      status: state.getIn(path.concat(['status']))
    }),
    (dispatch) => ({
      storeNodo: () => () => {console.log('store')},
    })
  )(connectStatus(props)(Factor));

  return <FactorConnected />
};

export default FactorNodo;
