import React from 'react';
import { connect } from 'react-redux';

import DimensionNodo from './DimensionNodo';
import FactorNodo from './FactorNodo';
import Actividades from './Actividades';

import { estandaresGroup } from './actividad/Estandar';

const RowPlanificacion = ({
  dimensionIndex,
  dimensionVisibleIndex,
  factorIndex,
  factorVisibleIndex,
  factorDbId,
  firstRow,
  hideFactor,
  factorCanBeHide,
}) => (
  <tr>
    {
      firstRow &&
        (<DimensionNodo
          index={dimensionIndex}
          visibleIndex={dimensionVisibleIndex}
         />)
    }
    <FactorNodo
      visibleIndex={factorVisibleIndex}
      index={factorIndex}
      dimensionIndex={dimensionIndex}
      hideFactor={hideFactor}
    />
    <Actividades
      factorIndex={factorIndex}
      factorVisibleIndex={factorVisibleIndex}
      factorDbId={factorDbId}
      hideFactor={hideFactor}
      factorCanBeHide={factorCanBeHide}
    />
  </tr>
);

const canHideFactor = ({
  state,
  factorIndex,
}) => {
  const fact_codigo = state.getIn(['fac', factorIndex, 'data', 'fact_codigo']);
  const typeObligatorio = estandaresGroup[1].key;

  return state.get('estandares')
    .filter(estandar => estandar.get('fact_codigo') === fact_codigo)
    .filter(estandar => estandar.get('tipo') !== typeObligatorio)
    .size === 0;
}

const mapStateToProps = (
  state,
  {
    dimensionIndex,
    dimensionVisibleIndex,
    factorVisibleIndex,
    factorIndex,
    factorDbId,
    firstRow,
  }
) => ({
  hideFactor: state.getIn(['fac', factorIndex, 'data', 'hide']),
  dimensionIndex,
  dimensionVisibleIndex,
  factorVisibleIndex,
  factorIndex,
  factorDbId,
  firstRow,
  factorCanBeHide: canHideFactor({ state, factorIndex }),
})

export default connect(mapStateToProps)(RowPlanificacion);
