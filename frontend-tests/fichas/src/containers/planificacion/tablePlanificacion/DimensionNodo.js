import React , { Component } from 'react';
import { connect } from 'react-redux';

import Dimension from './Dimension';
import { connectStatus } from '../../common/connectStatus';

const DimensionNodo = (
  props
) => {
  const index = props.index;
  const path = ['dim', index];

  const DimensionConnected = connect(
    (state) => ({
      path,
      status: state.getIn(path.concat(['status']))
    }),
    (dispatch) => ({
      storeNodo: () => () => {console.log('store')},
    })
  )(connectStatus(props)(Dimension));

  return <DimensionConnected />
};

export default DimensionNodo;
