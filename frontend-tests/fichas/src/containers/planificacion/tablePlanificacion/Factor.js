import React from 'react';
import { connect } from 'react-redux';

import StatusBadges from '../../../components/common/status/StatusBadges';
import RowHeader from '../../../components/planificacion/RowHeader';

import TextAreaField from '../../../components/common/form/TextAreaField';

import { updateFactor } from '../../../actions/planificacion/aFactores';
import { getContext } from '../../../context';

const Factor = ({
  resultado,
  title,
  visibleIndex,
  dimensionIndex,
  onUpdate,
  hideFactor,
  rootId,
  summaryStatus,
  ...status,
}) => (
  <td id={rootId}>
    {
      getContext().canEdit &&
        <StatusBadges status={summaryStatus} />
    }
    <RowHeader
      title={title}
      cod={`R.${visibleIndex}`}
      onChange={onUpdate}
      value={resultado}
      name={'resultado'}
      hideEdit={hideFactor}
      editable={getContext().canEdit}
    />
  </td>
);

const mapStateToProps = (
  state,
  {
    index,
    visibleIndex,
    dimensionIndex,
    hideFactor,
  }
) => ({
  resultado: state.getIn(['fac', index, 'data', 'resultado']),
  title: getContext().factores[index].display,
  hideFactor,
  visibleIndex,
});

const mapDispatchToProps = (
  dispatch,
  { index }
) => ({
  onUpdate: ({ target }) =>
    dispatch(updateFactor(index, target.name, target.value)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Factor);
