import React from 'react';
import { connect } from 'react-redux';

import {
  updateFirma,
} from '../../actions/planificacion/aFicha';

import DatosFirmas from '../../components/common/fichas/DatosFirmas';

const DatosFirmaNodo = ({
  data,
  onChange,
  editable,
  status,
}) => (
  <DatosFirmas
    status={status}
    data={data}
    onChange={onChange}
    editable={editable}
  />
);

const mapStateToProps = (
  state,
  {
    editable,
    summaryStatus,
  }
) => ({
  data: state.getIn(['fic', 'data', 'firmas']),
  summaryStatus,
  editable,
});

const mapDispatchToProps = (
  dispatch
) => ({
  onChange: (firmaType, { target }) =>
    dispatch(updateFirma(firmaType, target.name, target.value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DatosFirmaNodo);
