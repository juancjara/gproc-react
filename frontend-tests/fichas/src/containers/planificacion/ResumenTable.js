import React from 'react';
import { connect } from 'react-redux';
import { fromJS, Map } from 'immutable';
import _ from 'lodash';

import SummaryTableComp
  from '../../components/planificacion/SummaryTableComp';
import SummaryTableCellPercentage
  from '../../components/planificacion/SummaryTableCellPercentage';
import { getContext } from '../../context';
import { toNumber } from '../../utils';

export const extraCols =  [
  {
    display: '',
    key: 'label',
  },
  {
    key: 'total',
    display: 'Total',
  },
  {
    key: 'porc',
    display: '%',
  }
];

export const getFieldTitles = _.memoize(
  () =>
    [
      ...getContext().catalogo_titles
      .map(({ id, short_name }) => ({
        key: id,
        display: short_name,
      }))
    ]
);

export const getTitles = _.memoize(() =>
  [
    extraCols[0],
    ...getFieldTitles(),
    extraCols[1],
    extraCols[2],
  ]
);

export const getKeysFields = _.memoize(() =>
  getTitles()
    .slice(1, -2)
    .map(({ key }) => key)
);

const getRowKeys = _.memoize(() =>
  _(rowHeaders)
    .map(_.keys)
    .flatten()
    .value()
);

export const rowHeaders = [
  { fec: 'Total FEC'},
  { propio: 'Total PROPIO'},
  { plan: 'Total PLAN'},
];

const ResumenTable = ({
  data,
  percRows,
  totalPasantias,
  percPasantias,
}) => (
  <SummaryTableComp
    headers={getTitles()}
    data={fromJS(data)}
    percRows={percRows}
    rowHeaders={rowHeaders}
    totalPasantias={totalPasantias}
    percPasantias={percPasantias}
  />
);

const isVisisble = (factor) => !factor.getIn(['data', 'hide']);

export const getVisibleActividades = (
  state
) => {
  const factoresVisibles = state.get('fac')
    .filter(isVisisble)
    .reduce(
      (acc, factor) => acc.set(factor.get('dbId'), true),
      Map()
    );

  return state.get('act')
    .filter(actividad => factoresVisibles.has(actividad.get('parent_id')));
};

export const sumToString = (prev, next) =>
  (Number(prev) + Number(next)).toFixed(2).toString();

const mergeTotales = (acc, totales) =>
  acc.mergeDeepWith(sumToString, totales);

const sumValues = (rawRow) => {
  return _(rawRow.toJS())
    .pick(getKeysFields())
    .values()
    .sum()
    .toFixed(2).toString();
};

const setTotalesPerRow = ({
  totales,
  rowKeys,
  totalKey,
}) =>
  _.reduce(
    rowKeys,
    (acc, key) => acc.setIn(
      [key, totalKey],
      sumValues(acc.get(key))
    ),
    totales
  );

const getSumTotales = ({
  totales,
  rowKeys,
  totalKey,
}) =>
  _.reduce(
    rowKeys,
    (acc, key) => acc + toNumber(totales.getIn([key, totalKey])),
    0
  );

export const getPerc = (
  value,
  total,
  decimals
) => {
  if (decimals === null || decimals === undefined) {
    decimals = 2;
  }

  return (toNumber(value) / (toNumber(total) || 1) * 100)
    .toFixed(decimals).toString();
}


const setPercentagePerRow = ({
  totales,
  rowKeys,
  percKey,
  totalKey,
  sumTotales,
}) =>
  _.reduce(
    rowKeys,
    (acc, key) => acc.setIn(
      [key, percKey],
      `${getPerc(totales.getIn([key, totalKey]), sumTotales)}%`
    ),
    totales
  );

export const createBaseStructure = () => {
  const lengthKeysFields = getKeysFields().length;

  const cleanRowResumenTable = _.zipObject(
    getKeysFields(),
    _.times(lengthKeysFields, () => '0')
  );

  return fromJS(_.zipObject(
    getRowKeys(),
    _.times(3, () => cleanRowResumenTable)
  ));
};

const getTableFormated = (
  actividades
) => {
  let totales;

  if (actividades.size) {
    totales = actividades
      .asMutable()
      .map(actividad => actividad.get('detalleTotales'))
      .map()
      .reduce(mergeTotales , Map());
  } else {
    totales = createBaseStructure().asMutable();
  }

  const rowKeys = getRowKeys();

  const totalKey = 'total';
  const percKey = 'porc';

  totales = setTotalesPerRow({ totales, rowKeys, totalKey });

  const sumTotales = getSumTotales({
    totales,
    rowKeys: _.dropRight(rowKeys),
    totalKey
  });

  totales = setPercentagePerRow({
    totales,
    rowKeys,
    totalKey,
    percKey,
    sumTotales
  });

  return totales.asImmutable();
};

export const createCompCellPerc = perc => (
  <SummaryTableCellPercentage
    perc={perc}
  />
);

const getRowPerc = (row) => {
  const total = row.get('total');
  return _.reduce(
    getKeysFields(),
    (acc, key) =>
      acc.set(key, createCompCellPerc(row.get(key), total)
    ),
    Map()
  );
};

export const getTotalPasantias = actividades =>
  actividades
    .map(actividad => actividad.getIn(['pasantias']))
    .reduce(sumToString, 0);

const mapStateToProps = (
  state
) => {
  const visibleActividades = getVisibleActividades(state);
  const summaryTable = getTableFormated(visibleActividades);
  const totalPasantias = getTotalPasantias(visibleActividades);

  return {
    data: summaryTable,
    percRows: Map({ fec: getRowPerc(summaryTable.get('fec')) }),
    totalPasantias,
    percPasantias: getPerc(
      totalPasantias,
      summaryTable.getIn(['fec', 'total']),
      1
    )
  };
};

export default connect(mapStateToProps)(ResumenTable);
