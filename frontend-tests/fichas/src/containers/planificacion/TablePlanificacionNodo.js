import React, { Component } from 'react';

import TableHeader from '../../components/common/table/TableHeader';
import TableFooter from '../../components/common/table/TableFooter';
import TableBodyPlanificacion
  from './tablePlanificacion/TableBodyPlanificacion';
import { getContext } from '../../context';

const headers = [
  { display : 'Objetivo Específico (O.E.)', customClass: 'tobjetivo' },
  { display : 'Resultados o Productos (R.)', customClass: 'tresultado' },
  { display : 'Actividades (A.)', customClass: 'tladoderecho' },
];

const TablePlanificacionNodo = () => (
  <table className="table table-bordered table-hover">
    <TableHeader
      classTh={'text-center success'}
      headers={headers}
    />
    <TableBodyPlanificacion />
    <TableFooter
      classTh={'text-center success'}
      headers={headers}
    />
  </table>
);

export default TablePlanificacionNodo;
