import React from 'react';
import { connect } from 'react-redux';
import { fromJS, Map } from 'immutable';
import _ from 'lodash';

import PasantiasFooter from '../../components/planificacion/PasantiasFooter';
import CustomTableHeader from '../../components/common/table/CustomTableHeader';
import TableBodyDic from '../../components/common/table/TableBodyDic';
import SummaryTableComp
  from '../../components/planificacion/SummaryTableComp';
import SummaryTableCellPercentage
  from '../../components/planificacion/SummaryTableCellPercentage';
import { getContext } from '../../context';
import { toNumber } from '../../utils';
import {
  rowHeaders,
  getVisibleActividades,
  sumToString,
  getPerc,
  createCompCellPerc,
  extraCols,
  getTotalPasantias,
} from './ResumenTable';

const getGroupedLevel2Catalogo = _.memoize(
  () => {
    const grouped = _(getContext().catalogo_subtitles)
      .map(obj => ({
        key: obj.id,
        display: obj.short_name,
          ...obj,
      }))
      .groupBy('parent_id')
      .value();
    return getContext().catalogo_titles
      .map(({ id }) => _.values(grouped[id]));
  }
);

const addTotalColLevel2 = (group, i) => {
  const parent = getContext().catalogo_titles[i];
  return group.concat({
    display: `Total ${parent.short_name}`,
    key: `tot_${parent.id}`
  });
};

export const getTitles = _.memoize(() =>
  [
    extraCols[0],
    ..._(getGroupedLevel2Catalogo())
      .map(addTotalColLevel2)
      .flatten()
      .value(),
    extraCols[1],
    extraCols[2],
  ]
);

export const getCustomHeaders = _.memoize(() =>
  [
    {
      display: 'Participación',
      attr: {
        colSpan : 1,
        rowSpan : 2,
        style: { verticalAlign: 'middle' },
        className: 'text-center success',
      },
    },
    ...getContext().catalogo_titles
      .map(({ name, id }) => ({
        display: name,
        attr: {
          colSpan : getContext().catalogo_subtitles
            .filter(sub => sub.parent_id === id)
            .length + 1,
          className: 'text-center success'
        },
      })),
    {
      display: 'Total plan',
      attr: {colSpan : 2, className: 'text-center success' },
    },
  ]
);

const getFieldKeys = () =>
  _(getGroupedLevel2Catalogo())
    .flatten()
    .pluck('key')
    .value();

const getFieldWithTotalKeys = _.memoize(() =>
  _(getTitles())
    .drop()
    .dropRight(2)
    .pluck('key')
    .value()
);

export const getReadOnlyCols = _.memoize(() =>
  [
    'label',
    ..._.map(getContext().catalogo_titles, ({ id }) => `tot_${id}`),
    'total',
    'porc'
  ]
);

const getRowKeys = _.memoize(() =>
  _(rowHeaders)
    .map(_.keys)
    .flatten()
    .value()
);

const DetalleTable = ({
  percRows,
  data,
  // totalPasantias,
  // percPasantias,
}) => (
  <table id="table-detalle" className="table table-bordered table-condensed">
    <CustomTableHeader
      customHeaders={getCustomHeaders()}
      simpleHeaders={_.drop(getTitles())}
      classSimpleThs="success text-center"
    />
    <TableBodyDic
      editable={false}
      headers={getTitles()}
      rowHeaders={rowHeaders}
      body={fromJS(data)}
      newColKey={'label'}
      readOnlyCol={getReadOnlyCols()}
      extraCellComponents={percRows}
    />
    {
      /*
      <PasantiasFooter
        totalPasantias={totalPasantias}
        percPasantias={percPasantias}
      />
       */
    }
  </table>
);

export const sumFields = (
  fields,
  obj
) => _(obj)
  .pick(fields)
  .values()
  .sum()
  .toFixed(2).toString();

export const getTitleId = (i) => `tot_${getContext().catalogo_titles[i].id}`;

export const updateTotals = row => {
  _.forEach(getGroupedLevel2Catalogo(), (a, i) => {
      row[getTitleId(i)] =
        sumFields(_.pluck(a ,'key'), row);
    });

  const total = _.reduce(
    getGroupedLevel2Catalogo(),
    (acc, _, i) => sumToString(acc, row[getTitleId(i)]),
    '0.00'
  );
  row.total = total;

  return row;
};

const updateTotalPerc = (row, total) =>
  getPerc(row['total'], total) + '%';

const mergeTotales = (acc, act) =>
  _.reduce(getRowKeys(), (carry, k) => {
    carry[k] = _.merge(act[k], carry[k], sumToString);
    return carry;
  }, acc);

export const createBaseStructure = () => {
  const cleanRow = _.zipObject(
    getFieldKeys(),
    _.times(getFieldKeys().length, () => '0.00')
  );

  return _.zipObject(
    getRowKeys(),
    _.times(3, () => _.cloneDeep(cleanRow))
  );
};

const getTotales = visibleActividades => {
  if (visibleActividades.size) {
    return visibleActividades
      .map(a => a.get('detalleTotales'))
      .toJS()
      .reduce(mergeTotales, {});
  }

  return createBaseStructure();
};

const getTotalPlan = partialTotals => {
  const totales = _.cloneDeep(partialTotals);
  _.forEach(
    _.keys(totales.plan),
    key => totales.plan[key] = sumToString(totales.fec[key], totales.propio[key])
  );
  return totales;
};

// const formatTable = visibleActividades => {
export const formatTable = structureTotales => {
  const totalPercKey = 'porc';
  //const totales = structureTotales;

  const withTotals =_.reduce(getRowKeys(), (obj, k) => {
    obj[k] = updateTotals(structureTotales[k]);
    return obj;
  }, {});

  const total = sumToString(withTotals.fec.total, withTotals.propio.total);

  const withTotalPlan = getTotalPlan(withTotals);

  const withTotalPerc = _.reduce(
    getRowKeys(),
    (acc, k) => {
      acc[k] = _.assign(
        withTotalPlan[k],
        {[totalPercKey]: updateTotalPerc(withTotalPlan[k], total)}
      );
      return acc;
    }, {});

  return fromJS(withTotalPerc);
};

export const getRowPerc = row => {
  const total = row.get('total');
  const keyFields = getFieldWithTotalKeys();

  return _.reduce(
    keyFields,
    (acc, key) =>
      acc.set(key, getPerc(row.get(key), total)),
    Map()
  );
};

export const getRowPercComp = row => {
  const rowWithPerc = getRowPerc(row);
  return _.reduce(
    getFieldWithTotalKeys(),
    (acc, key) =>
      acc.set(key, createCompCellPerc(rowWithPerc.get(key))),
    Map()
  );
};

const mapStateToProps = (
  state
) => {
  const visibleActividades = getVisibleActividades(state);
  const totales = getTotales(visibleActividades);
  const detailTable = formatTable(totales);
  console.log(detailTable.toJS());
  const percRows = getRowPercComp(detailTable.get('fec'));
  // const totalPasantias = getTotalPasantias(visibleActividades);

  return {
    data: detailTable,
    percRows: Map({ fec: percRows }),
    // totalPasantias,
    // percPasantias: getPerc(
    //   totalPasantias,
    //   detailTable.getIn(['fec', 'total']),
    //   1
    // )
  };
};

export default connect(mapStateToProps)(DetalleTable);
