import React from 'react';
import { connect } from 'react-redux';

import ProgressBarComp
  from '../../components/planificacion/ProgressBarComp';

const ProgressBar = ({
  title,
  percentage,
}) => (
  <ProgressBarComp
    title={title}
    percentage={percentage}
  />
);

const mapStateToProps = (
  state
) => {
  const estandaresObligatorios = state.get('estandares')
    .filter(estandar => estandar.get('tipo') === 'Obligatorios');

  const totalEstandaresObligatorios = estandaresObligatorios.size;

  const totalUsedEstandaresObligatorios = estandaresObligatorios
    .filter(estandar => estandar.get('count') > 0)
    .size;

  const percentage = totalUsedEstandaresObligatorios /
    (totalEstandaresObligatorios || 1) * 100;
  const percentageFormated = `${percentage.toFixed(2)}%`;

  return {
    title: `${totalUsedEstandaresObligatorios} procesados de
            ${totalEstandaresObligatorios} estándares por atender
            (${percentageFormated})`,
    percentage: percentageFormated,
  };
};

export default connect(mapStateToProps)(ProgressBar);
