import React, { Component } from 'react';

import TableHeader from '../../components/common/table/TableHeader';
import TableFooter from '../../components/common/table/TableFooter';
import TableBodyPresupuesto
  from './tablePresupuesto/TableBodyPresupuesto';
import RowPresupuesto from './tablePresupuesto/RowPresupuesto';

import { getContext } from '../../context';

const headers = [
  { display : 'O.E.', customClass: 'tobjetivo' },
  { display : 'R.', customClass: 'tresultado' },
  { display : 'SubActividades ', customClass: 'tladoderecho' },
];

const TablePresupuestoNodo = () => (
  <table className="table table-bordered table-hover">
    <TableHeader
      classTh={'text-center success'}
      headers={headers}
    />
    <TableBodyPresupuesto RowComp={RowPresupuesto}/>
    <TableFooter
      classTh={'text-center success'}
      headers={headers}
    />
  </table>
);

export default TablePresupuestoNodo;
