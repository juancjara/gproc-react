import React from 'react';
import { connect } from 'react-redux';
import { fromJS, Map } from 'immutable';

import BoxError from '../../components/common/BoxError';
import SummaryTableComp
from '../../components/planificacion/SummaryTableComp';
import SummaryTableCellPercentage
from '../../components/planificacion/SummaryTableCellPercentage';
import { getContext } from '../../context';
import {
  toNumber,
  getBoxErrors,
} from '../../utils';

import PasantiasFooter from '../../components/planificacion/PasantiasFooter';
import CustomTableHeader from '../../components/common/table/CustomTableHeader';
import TableBodyDic from '../../components/common/table/TableBodyDic';

import { PASANTIAS_ID } from '../../actions/presupuesto/subActividad/aSubActividad';

import {
  DETALLE_TABLE,
} from '../../titles/presupuesto';

import {
  extraCols,
  rowHeaders,
  sumToString,
  getPerc,
} from '../planificacion/ResumenTable';

import {
  getCustomHeaders,
  getReadOnlyCols,
  createBaseStructure,
  formatTable,
  getRowPercComp,
  getTitles,
} from '../planificacion/DetalleTable';

import { otroID }
  from './tablePresupuesto/actividad/subActividad/CatalogoPicker';

const DetalleTable = ({
  percRows,
  data,
  // totalPasantias,
  // percPasantias,
  otros,
  errors,
}) => (
  <div>
  <table id="table-detalle" className="table table-bordered table-condensed">
    <CustomTableHeader
      customHeaders={getCustomHeaders()}
      simpleHeaders={_.drop(getTitles())}
      classSimpleThs="success text-center"
    />
    <TableBodyDic
      editable={false}
      headers={getTitles()}
      rowHeaders={rowHeaders}
      body={fromJS(data)}
      newColKey={'label'}
      readOnlyCol={getReadOnlyCols()}
      extraCellComponents={percRows}
    />
    {
      <tfoot>
        <tr>
          <td colSpan="6" className="text-left">
            <strong>Tipo de Cambio: 3.46</strong>
          </td>
          <td colSpan="6" className="text-right">
            <strong>{otros}</strong>
          </td>
        </tr>
      </tfoot>
    }
    {
      /*
       <PasantiasFooter
       totalPasantias={totalPasantias}
       percPasantias={percPasantias}
       />
      */
    }
    </table>
    {
      getContext().editable &&
        <BoxError
          errors={getBoxErrors(DETALLE_TABLE, errors)}
        />
    }
 </div>
);

export const getTotales = subActividades => {
  let totales = createBaseStructure();
  if (subActividades.size) {
    _.forEach(_.keys(totales), rowKey => {
      _.keys(totales[rowKey])
        .forEach(colKey => {
          const subActFiltered = subActividades
            .filter(sub =>
              sub.hasIn(['data', 'niveles', 1]) &&
                sub.getIn(['data', 'niveles', 1]) === colKey &&
                sub.getIn(['data', 'financiamiento']).toLowerCase() === rowKey
            );
          if (subActFiltered.size) {
            totales[rowKey][colKey] = subActFiltered.
              reduce(
                (acc, elem) =>
                  sumToString(acc, elem.getIn(['data', 'total'])),
                '0'
              );
          }
        });
      });
  }
  return totales;
};

export const getOtrosAmount = state =>
  state.get('sub')
  .filter(
    sub => sub.getIn(['data', 'todo_seleccionado']) &&
      sub.getIn(['data', 'niveles']).last() === otroID &&
      sub.getIn(['data', 'financiamiento']).toLowerCase() === 'fec'
  )
    .reduce(
      (acc, sub) => sumToString(acc, sub.getIn(['data', 'total'])),
      '0.00'
    );

const getTotalPasantias = subActividades =>
  subActividades
     .filter(sub =>
       sub.hasIn(['data', 'niveles', 1]) &&
         sub.getIn(['data', 'niveles', 1]) === PASANTIAS_ID &&
         sub.getIn(['data', 'financiamiento']).toLowerCase() === 'fec'
     )
    .reduce(
      (acc, sub) => sumToString(acc, sub.getIn(['data', 'total'])),
      '0.00'
    );

export const getTableData = subActividades =>
  formatTable(getTotales(subActividades));

const mapStateToProps = state => {
  const detailTable = getTableData(state.get('sub'));
  const percRows = getRowPercComp(detailTable.get('fec'));
  const totalOtros = getOtrosAmount(state);
  const percOtrosFEC = getPerc(totalOtros, detailTable.getIn(['fec', 'total']));
  const errors = state.getIn(['totales', 'status']).toJS().errors;

  return {
    data: detailTable,
    percRows: Map({ fec: percRows }),
    otros: `Otros FEC: S/. ${totalOtros} (${percOtrosFEC}%)`,
    errors,
    // totalPasantias,
    // percPasantias: getPerc(
    //   totalPasantias,
    //   detailTable.getIn(['fec', 'total']),
    //   1
    // ),
  };
};

export default connect(mapStateToProps)(DetalleTable);
