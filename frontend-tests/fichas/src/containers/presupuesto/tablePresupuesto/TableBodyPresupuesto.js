import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { getContext } from '../../../context';
import { flatMap } from '../../../utils';

const TableBodyPresupuesto = ({
  dimensiones,
  factoresGrouped,
  RowComp
}) => (
  <tbody>
    {
      flatMap(dimensiones, (d, dimIndex) =>
        _.map(factoresGrouped[d.dime_codigo], (f, i) => (
          <RowComp
            key={`${d.index}${i}`}
            firstRow={i === 0}
            dimensionIndex={d.index}
            dimensionVisibleIndex={dimIndex}
            factorIndex={f.index}
            factorVisibleIndex={i}
          />
        ))
      )
    }
  </tbody>
);

const getDimensiones = (
  dimensiones,
  factoresGrouped
) => {
  return _(dimensiones)
    .map((dimension, index) => ({
      ...dimension,
      index,
    }))
    .filter(dimension => {
      if (factoresGrouped.hasOwnProperty(dimension.dime_codigo)) {
        return factoresGrouped[dimension.dime_codigo].length;
      } else {
        return false;
      }
    })
    .value();
};

const groupByDimension = factores => {
  return _(factores)
    .map((factor, index) => ({
      ...factor,
      index,
    }))
    .groupBy(f => f.dime_codigo)
    .value();
};

const mapStateToProps = state => {
  const factoresGrouped = groupByDimension(getContext().factores);

  return {
    dimensiones: getDimensiones(getContext().dimensiones, factoresGrouped),
    factoresGrouped,
  };
}

export default connect(mapStateToProps)(TableBodyPresupuesto);
