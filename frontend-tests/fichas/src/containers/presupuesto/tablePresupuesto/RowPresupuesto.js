import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

// import Dimension from './Dimension';
import TdPresupuesto from '../../../components/presupuesto/TdPresupuesto';
import Actividades from './Actividades';

import { hasIn } from '../../../utils';
import { getContext } from '../../../context';

const RowPresupuesto = ({
  firstRow,
  dimensionData,
  factorData
}) => (
  <tr>
    {
      firstRow && (
        <TdPresupuesto
	  show
          {...dimensionData}
        />
      )
    }
    <TdPresupuesto
      show
      {...factorData}
    />
    <Actividades factorIndex={factorData.index} />
  </tr>
);

const getSizeDimension = _.memoize(
  (dimensionIndex, factores) =>
    _.filter(
      factores,
      f => f.dime_codigo ===
        getContext().dimensiones[dimensionIndex].dime_codigo
    ).length
);

export const mapStateToProps = (
  state,
  {
    firstRow,
    dimensionIndex,
    dimensionVisibleIndex,
    factorIndex,
    factorVisibleIndex,
  }
) => {
  const dimension = getContext().dimensiones[dimensionIndex];
  const factor = getContext().factores[factorIndex];
  return {
    firstRow,
    dimensionData: {
      index: dimensionIndex,
      size: getSizeDimension(dimensionIndex, getContext().factores),
      title: hasIn(dimension, 'data.objetivo') ? dimension.data.objetivo: '',
      name: `O.E.${dimensionVisibleIndex + 1 }`,
    },
    factorData: {
      index: factorIndex,
      title: hasIn(factor, 'data.resultado') ? factor.data.resultado: '',
      name: `R.${factorIndex + 1 }`,
    }
  };
};

export default connect(mapStateToProps)(RowPresupuesto);
