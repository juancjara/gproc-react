import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Map } from 'immutable';

import { getContext } from '../../../../context';
import AddButton from '../../../../components/common/AddButton';
import SubActividadNodo from './subActividad/SubActividadNodo';

import { isDisabledPresupuesto } from '../../../../utils/';

import {
  addSubActividad,
} from '../../../../actions/presupuesto/aSubActividades';

class SubActividades extends Component {
  shouldComponentUpdate(nextProps) {
    return this.props.subActividades.size !== nextProps.subActividades.size;
  }

  render() {
    const {
      subActividades,
      actividadIndex,
      addSubActividad,
      actividadVisibleIndex,
      factorIndex,
      isDisabled,
    } = this.props;

    const zeroSubActividades = !subActividades.size;

    return (
      <div className="box-body">
        {
          subActividades.map((sub, i) =>
            <SubActividadNodo
              key={sub.get('id')}
              subActividadIndex={sub.get('index')}
              actividadIndex={actividadIndex}
              name={`Sub.${factorIndex + 1}.${actividadVisibleIndex + 1}
                    .${i + 1}`}
              iterationIndex={i}
            />
          )
        }
        {
          zeroSubActividades &&
            <AddButton
              onClick={addSubActividad}
              editable={getContext().editable}
              text={'Agregar SubActividad'}
              disabled={isDisabled}
            />
        }
      </div>
    );
  }
};

const getSubActividades = (
  state,
  actividadIndex
) => {
  // const actividadId = state.getIn(['act', actividadIndex, 'dbId']);
  const actividadId = getContext().act[actividadIndex].dbId;
  return state
    .get('sub')
    .map((sub, index) => Map({
      id: sub.get('dbId'),
      index,
      parent_id: sub.get('parent_id'),
    }))
    .filter(sub => sub.get('parent_id') === actividadId);
};

const mapStateToProps = (
  state,
  {
    actividadIndex,
    actividadVisibleIndex,
    factorIndex,
  }
) => ({
  subActividades: getSubActividades(state, actividadIndex),
  actividadIndex,
  actividadVisibleIndex,
  factorIndex,
  isDisabled: isDisabledPresupuesto(state),
});

const mapDispatchToProps = (
  dispatch,
  { actividadIndex }
) => ({
  addSubActividad: () => dispatch(addSubActividad(0, actividadIndex)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SubActividades);
