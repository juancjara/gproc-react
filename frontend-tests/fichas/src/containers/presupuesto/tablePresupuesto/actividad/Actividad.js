import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { fromJS } from 'immutable';

import Row from '../../../../components/common/table/Row';
import TableHeader from '../../../../components/common/table/TableHeader';
import StatusBadges from '../../../../components/common/status/StatusBadges';
import BoxError from '../../../../components/common/BoxError';

import { getContext } from '../../../../context';
import SubActividades from './SubActividades';

import {
  getBoxErrors,
} from '../../../../utils';

import {
  getFieldTitles,
} from '../../../../containers/planificacion/ResumenTable';

import {
  getTitleId,
  updateTotals
} from '../../../../containers/planificacion/DetalleTable';

import {
  getTableData,
} from '../../DetalleTable';

const Actividad = ({
  factorIndex,
  index,
  name,
  title,
  actividadVisibleIndex,
  headers,
  summaryStatus,
  summaryRow,
  errors,
}) => (
  <div className="box box-default">
    <div className="box-header bg-success">
      <div className="box-title">
        {
          getContext().editable &&
            <StatusBadges status={summaryStatus} />
        }
        {title}
        <div>{name}</div>
      </div>
    </div>
	<BoxError
      errors={errors}
    />
    <SubActividades
      actividadIndex={index}
      actividadVisibleIndex={actividadVisibleIndex}
      factorIndex={factorIndex}
    />
  <div className="box-footer">
      <table className="table table-bordered bg-success">
        <TableHeader
          headers={headers}
        />
        <tbody>
          <Row
            editable={false}
            headers={headers}
            indexRow={1}
            row={fromJS(summaryRow)}
            canBeRemoved={false}
          />
        </tbody>
      </table>
    </div>
  </div>
);

const getHeaders = _.memoize(
  () =>
    getFieldTitles()
    .map((title, i) => ({
      key: getTitleId(i),
      display: title.display
    }))
    .concat([
      {key: 'total', display: 'Total Actividad'},
    ])
);

const getSummaryRow = (state, index) => {
  const dbIdActividad = getContext().act[index].dbId;
  const data = {};
  const subActividades = state.get('sub')
    .filter(sub =>
      sub.get('parent_id') === dbIdActividad &&
        sub.getIn(['data', 'niveles', 0]) &&
        sub.getIn(['data', 'financiamiento'])
    );

  const rawTotalesPlan = getTableData(subActividades)
          .get('plan').toJS();

  const totales = _.pick(
    rawTotalesPlan,
    _.keys(rawTotalesPlan).filter(k => k.search('tot') > -1)
  );
  return totales;
};

const mapStateToProps = (
  state,
  {
    index,
    factorIndex,
    visibleIndex,
  }
) => ({
  factorIndex,
  factorCodigo: getContext().factores[factorIndex].fact_codigo,
  index,
  title: `A.${factorIndex + 1}.${visibleIndex + 1}`,
  name: getContext().act[index].data.miactividad,
  actividadVisibleIndex: visibleIndex,
  headers: getHeaders(),
  summaryRow: getSummaryRow(state, index),
  errors: getBoxErrors(
    state.getIn(['act', index, 'id']),
    state.getIn(['act', index, 'status']).toJS().errors
  ),
  // name: state.getIn(['act', index, 'data', 'miactividad']),
});

export default connect(mapStateToProps)(Actividad);
