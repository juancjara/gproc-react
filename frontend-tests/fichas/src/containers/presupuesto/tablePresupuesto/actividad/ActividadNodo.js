import React from 'react';
import { connect } from 'react-redux';

import Actividad from './Actividad';
import { connectStatus } from '../../../common/connectStatus';

const ActividadNodo = (
  props
) => {
  const path = ['act', props.index];

  const ActividadConnected = connect(
    state => ({
      path,
      status: state.getIn(path.concat(['status'])),
    }),
    dispatch => ({
      storeNodo: () => {},
    })
  )(connectStatus(props)(Actividad));

  return <ActividadConnected />;
};

export default ActividadNodo;
