import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getContext } from '../../../../../context';
import DisplayOrSelect from '../../../../../components/common/form/DisplayOrSelect';

import {
  updateSubActividadField,
} from '../../../../../actions/presupuesto/subActividad/aSubActividad';

const options = [
  { id: 'propio', name: 'Propio'},
  { id: 'fec', name: 'FEC'},
];

class TipoPago extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      tipoPago,
      updateField,
      editable,
    } = this.props;
    return (
      <td>
        <small>Tipo de financiamiento</small>
        <DisplayOrSelect
          editable={editable}
          value={tipoPago}
          name="financiamiento"
          onChange={updateField}
          options={options}
          required
        />
      </td>
    );
  }
};

const mapStateToProps = (
  state,
  { subActividadIndex }
) => ({
  tipoPago: state.getIn(['sub', subActividadIndex, 'data', 'financiamiento']),
  editable: getContext().editable,
});

const mapDispatchToProps = (
  dispatch,
  { subActividadIndex }
) => ({
  updateField: ({ target }) =>
    dispatch(updateSubActividadField(subActividadIndex, target.name, target.value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TipoPago);
