import React from 'react';
import { connect } from 'react-redux';

import EditableField
  from '../../../../../components/common/form/EditableField';
import StaticField
  from '../../../../../components/common/form/StaticField';
import DisplayOrInput
  from '../../../../../components/common/form/DisplayOrInput';
import { otroID } from './CatalogoPicker';

import FileUpload from './FileUpload';

import { getContext } from '../../../../../context';
import patterns from '../../../../../patterns';

import {
  updateSubActividadField
} from '../../../../../actions/presupuesto/subActividad/aSubActividad';

const RRHHfields = ({
  data,
  updateField,
  updateFile,
  capacitacion,
  otro,
  editable,
  catalogoLastLevel,
  subactividadId,
  profesoresVisitantes,
  profesoresVisitantesLocal,
}) => (
  <td>
    <EditableField
      editable={otro && editable}
      label="Pasajes:"
      smallLabel
      value={data.get('pasajes')}
      name="pasajes"
      onChange={updateField}
      required
      pattern={patterns.decimalNumbers}
    />
    <StaticField
      label={`Viáticos${capacitacion || profesoresVisitantes ? '+ honorarios': ''}:`}
  smallLabel
  value={data.get('viaticos')}
    />
    <EditableField
      editable={editable}
      value={data.get('num_dias')}
      name="num_dias"
      placeholder="N° dias"
      label="N° dias"
      smallLabel
      onChange={updateField}
      required
      pattern={patterns.numbers}
    />
    {
      capacitacion &&
      <EditableField
        editable={editable}
        value={data.get('inscripcion')}
        name="inscripcion"
        placeholder="Inscripción"
        label="Inscripción"
        smallLabel
        onChange={updateField}
        required
        pattern={patterns.decimalNumbers}
      />
    }
    <FileUpload
      onUpdate={updateFile}
      file={data.get('file')}
      catalogoLastLevel={catalogoLastLevel}
      subactividadId={subactividadId}
    />
 </td>
);

const mapStateToProps = (
  state,
  { subActividadIndex }
) => ({
  data: state.getIn(['sub', subActividadIndex, 'data']),
  capacitacion: state.getIn(['sub', subActividadIndex, 'data',
                             'niveles', 1]) === 'RC',
  otro: state.getIn(['sub', subActividadIndex, 'data',
                     'niveles']).last() === otroID,
  editable: getContext().editable,
  catalogoLastLevel: state.getIn(['sub', subActividadIndex, 'data', 'niveles']).last(),
  subactividadId: state.getIn(['sub', subActividadIndex, 'dbId']),
  profesoresVisitantes: state.getIn(['sub', subActividadIndex, 'data',
                                     'niveles', 1]) === 'SP',
  profesoresVisitantesLocal: state.getIn(['sub', subActividadIndex, 'data',
                                     'niveles', 2]) === 'SPL',
});

const mapDispatchToProps = (
  dispatch,
  { subActividadIndex }
) => ({
  updateField: ({ target }) =>
    dispatch(updateSubActividadField(subActividadIndex, target.name, target.value)),
  updateFile: filePath =>
    dispatch(updateSubActividadField(subActividadIndex, 'file', filePath)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RRHHfields);
