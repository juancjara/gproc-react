import React, { Component } from 'react';

import { getContext } from '../../../../../context';
import { APIuploadFile } from '../../../../../webAPI/presupuesto';

class FileUpload extends Component {
  constructor(props) {
    super(props);
    this.state = { enableUpload: this.isEnableUpload(props) };

    this.setRef = this.setRef.bind(this);
    this.onFilesAdded = this.onFilesAdded.bind(this);
    this.onFileUploaded = this.onFileUploaded.bind(this);
    this.onError = this.onError.bind(this);
  }

  isEnableUpload({ catalogoLastLevel }) {
    const selected = getContext().catalogo
      .filter(item => item.id === catalogoLastLevel);
    if (!selected.length) {
      return true;
    }
    return selected[0].path ? false : true;
  }

  componentWillReceiveProps(nextProps) {
    const enableUpload = this.isEnableUpload(nextProps);
    this.setUpPlugin(enableUpload);
    this.setState({ enableUpload });
  }

  setUpPlugin(enable) {
    if (enable) {
      this.activatePlugin();
    } else {
      this.removePlugin();
    }
  }

  onFilesAdded() {
    this.uploader.start();
  }

  onFileUploaded(a, b, c) {
    //console.log(a, b, c);
    //console.log('response', c.response);
    this.props.onUpdate(JSON.parse(c.response).filename);
  }

  onError(a, b, c) {
    console.log('error', a, b, c);
  }

  setRef(e) {
    this.button = e;
  }

  activatePlugin() {
    if (this.uploader) return;
    console.log('activando');
    this.settings = {
      url: APIuploadFile,
      browse_button: this.button,
      multi_selection: false,
      multipart_params: {
        identifier: this.props.subactividadId,
      },
    };

    this.uploader = new plupload.Uploader(this.settings);
    this.uploader.bind('FilesAdded', this.onFilesAdded);
    this.uploader.bind('FileUploaded', this.onFileUploaded);
    this.uploader.bind('Error', this.onError);
    this.uploader.init();

  }

  removePlugin () {
    if (!this.uploader) {
      return;
    }

    this.uploader.unbind('FilesAdded', this.onFilesAdded);
    this.uploader.unbind('FileUploaded', this.onFileUploaded);
    this.uploader.unbind('Error', this.onError);
    this.uploader.destroy();
    this.uploader = null;
  }

  componentDidMount() {
    if (this.state.enableUpload) {
      this.activatePlugin();
    }
  }

  componentWillUnmount() {
    this.removePlugin();
  }

  render() {
    const {
      file,
    } = this.props;

    const uploadClass = this.state.enableUpload ? '' : 'disabled';
    const downloadClass = file ? '' : 'disabled';

    return (
      <div className="input-group-btn">
        {
          getContext().editable &&
            (
              <span>
                <button
                  ref={this.setRef}
                  className={`btn bnt-default btn-flat ${uploadClass}`}
                >
                <i className='fa fa-upload' />
                </button>
              </span>
            )
        }
				<a href={"/file/" + file}
          className={`btn btn-default btn-flat btn_file_view ${downloadClass}`}
          target="_blank"
          title={"Ver documento de especificación técnica"}
        >
				  <i className="fa fa-file-text full" />
				  <i className="fa fa-file-text-o empty" />
				</a>
			</div>
    );
  }
}

export default FileUpload;
