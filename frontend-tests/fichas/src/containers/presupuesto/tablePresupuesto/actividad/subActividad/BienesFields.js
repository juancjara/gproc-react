import React from 'react';
import { connect } from 'react-redux';

import EditableField
  from '../../../../../components/common/form/EditableField';
import StaticField
  from '../../../../../components/common/form/StaticField';
import DisplayOrInput
  from '../../../../../components/common/form/DisplayOrInput';
import { otroID } from './CatalogoPicker';

import FileUpload from './FileUpload';

import { getContext } from '../../../../../context';
import patterns from '../../../../../patterns';

import {
  updateSubActividadField
} from '../../../../../actions/presupuesto/subActividad/aSubActividad';

const BienesFields = ({
  data,
  obrasMenores,
  editable,
  updateField,
  updateFile,
  catalogoLastLevel,
  subactividadId,
}) => (
  <td>
    <EditableField
      editable={obrasMenores && editable}
      label="Costo"
      smallLabel
      value={data.get('costo')}
      name="costo"
      onChange={updateField}
      required
      pattern={patterns.decimalNumbers}
    />
    <FileUpload
      onUpdate={updateFile}
      catalogoLastLevel={catalogoLastLevel}
      file={data.get('file')}
      subactividadId={subactividadId}
    />
  </td>
);

const mapStateToProps = (
  state,
  { subActividadIndex }
) => ({
  data: state.getIn(['sub', subActividadIndex, 'data']),
  obrasMenores: state.getIn(['sub', subActividadIndex, 'data',
                             'niveles', 1]) === 'B2' || state.getIn(['sub', subActividadIndex, 'data', 'niveles']).last() === otroID,
  editable: getContext().editable,
  catalogoLastLevel: state.getIn(['sub', subActividadIndex, 'data', 'niveles']).last(),
  subactividadId: state.getIn(['sub', subActividadIndex, 'dbId']),
});

const mapDispatchToProps = (
  dispatch,
  { subActividadIndex }
) => ({
  updateField: ({ target }) =>
    dispatch(updateSubActividadField(subActividadIndex, target.name, target.value)),
  updateFile: filePath =>
    dispatch(updateSubActividadField(subActividadIndex, 'file', filePath)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BienesFields);
