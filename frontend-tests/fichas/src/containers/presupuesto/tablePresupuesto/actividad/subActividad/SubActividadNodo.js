import React from 'react';
import { connect } from 'react-redux';

import SubActividad from './SubActividad';
import { connectStatus } from '../../../../common/connectStatus';

const SubActividadNodo = (
  props
) => {
  const path  = ['sub', props.subActividadIndex];

  const SubActividadConnected = connect(
    state => ({
      path,
      status: state.getIn(path.concat(['status'])),
    }),
    dispatch => ({
      storeNodo: () => {},
    })
  )(connectStatus(props)(SubActividad));

  return <SubActividadConnected />
};

export default SubActividadNodo;
