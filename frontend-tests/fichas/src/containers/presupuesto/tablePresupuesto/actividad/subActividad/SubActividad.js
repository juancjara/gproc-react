import React from 'react';
import { connect } from 'react-redux';

import SubActividadData from './SubActividadData';
import CatalogoPicker from './CatalogoPicker';
import RRHHfields from './RRHHfields';
import BienesFields from './BienesFields';
import ServiciosFields from './ServiciosFields';
import TotalFields from './TotalFields';
import TipoPago from './TipoPago';
import StatusBadges from '../../../../../components/common/status/StatusBadges';
import BoxError from '../../../../../components/common/BoxError';

import {
  isDisabledPresupuesto,
  getBoxErrors,
} from '../../../../../utils';
import { getContext } from '../../../../../context';
import AddButton from '../../../../../components/common/AddButton';
import RemoveButton from '../../../../../components/common/RemoveButton';

import {
  addSubActividad,
  removeSubActividad,
} from '../../../../../actions/presupuesto/aSubActividades';

const SubActividad = ({
  dbId,
  addSubActividad,
  factCodigo,
  subActividadIndex,
  allSelected,
  firstLevel,
  editable,
  removeSubActividad,
  name,
  rootId,
  summaryStatus,
  isDisabled,
  errors,
}) => {
  let compCustomFields = undefined;
  if (firstLevel === 'R') {
    compCustomFields = <RRHHfields subActividadIndex={subActividadIndex} />;
  } else if (firstLevel === 'B') {
    compCustomFields = <BienesFields subActividadIndex={subActividadIndex} />;
  } else if (firstLevel === 'S') {
    compCustomFields = <ServiciosFields subActividadIndex={subActividadIndex} />;
  }
  return(
    <div id={rootId} className="well-subactividad">
      <BoxError
        errors={errors}
      />
      {
        getContext().editable &&
          <StatusBadges status={summaryStatus} />
      }
      <span>{name}</span>
      <RemoveButton
        extraClass="pull-right"
        size="sm"
        onClick={removeSubActividad}
        editable={editable}
        title="Remover subactividad"
        disabled={isDisabled}
      />
      <table className="table table-bordered table-subactividad">
        <tbody>
          <tr>
            <SubActividadData
              subActividadIndex={subActividadIndex}
            />
            <CatalogoPicker
              factCodigo={factCodigo}
              subActividadIndex={subActividadIndex}
            />
            {
              allSelected ?
                compCustomFields :
                <td />
            }
            {
              allSelected ?
                <TotalFields
                  subActividadIndex={subActividadIndex}
                /> :
                <td />
            }
            {
              allSelected ?
                <TipoPago
                  subActividadIndex={subActividadIndex}
                /> :
                <td />
            }
          </tr>
        </tbody>
      </table>
      <AddButton
        onClick={addSubActividad}
        editable={editable}
        text={'Agregar SubActividad'}
        disabled={isDisabled}
      />
    </div>
  );
};

const mapStateToProps = (
  state,
  { subActividadIndex }
) => ({
  dbId: state.getIn(['sub', subActividadIndex, 'dbId']),
  subActividadIndex,
  allSelected: state
    .getIn(['sub', subActividadIndex, 'data', 'todo_seleccionado']),
  firstLevel: state.getIn(['sub', subActividadIndex, 'data', 'niveles', 0]),
  editable: getContext().editable,
  isDisabled: isDisabledPresupuesto(state),
  errors: getBoxErrors(
    state.getIn(['sub', subActividadIndex, 'dbId']),
    state.getIn(['sub', subActividadIndex, 'status']).toJS().errors
  ),
});

const mapDispatchToProps = (
  dispatch,
  {
    actividadIndex,
    subActividadIndex,
    name,
  }
) => ({
  addSubActividad: () =>
    dispatch(addSubActividad(subActividadIndex + 1, actividadIndex)),
  removeSubActividad: () =>
    dispatch(removeSubActividad(subActividadIndex, actividadIndex)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SubActividad);
