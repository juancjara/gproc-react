import React from 'react';
import { connect } from 'react-redux';

import StaticField
  from '../../../../../components/common/form/StaticField';
import EditableField
  from '../../../../../components/common/form/EditableField';
import  DisplayOrInput
  from '../../../../../components/common/form/DisplayOrInput';
import { otroID } from './CatalogoPicker';

import FileUpload from './FileUpload';

import { getContext } from '../../../../../context';
import patterns from '../../../../../patterns';

import {
  updateSubActividadField
} from '../../../../../actions/presupuesto/subActividad/aSubActividad';

const ServiciosFields = ({
  data,
  otro,
  profesoresVisitantes,
  editable,
  updateField,
  updateFile,
  catalogoLastLevel,
  subactividadId,
}) => (
  <td>
    {
      profesoresVisitantes &&
        <EditableField
          editable={otro && editable}
          label="Pasajes:"
          smallLabel
          value={data.get('pasajes')}
          name="pasajes"
          onChange={updateField}
          required
          pattern={patterns.decimalNumbers}
        />
    }
    <EditableField
      editable={otro && editable}
      label={profesoresVisitantes ? 'Viáticos:': 'Costo'}
      smallLabel
      value={data.get('costo')}
      name="costo"
      onChange={updateField}
      required
      pattern={patterns.decimalNumbers}
    />
    {
      profesoresVisitantes &&
        <EditableField
          value={data.get('num_dias')}
          name="num_dias"
          placeholder="N° días"
          label="N° días"
          smallLabel
          onChange={updateField}
          required
          pattern={patterns.numbers}
          editable={editable}
        />
    }
    <FileUpload
      catalogoLastLevel={catalogoLastLevel}
      onUpdate={updateFile}
      file={data.get('file')}
      subactividadId={subactividadId}
    />
  </td>
);

const mapStateToProps = (
  state,
  { subActividadIndex }
) => ({
  data: state.getIn(['sub', subActividadIndex, 'data']),
  profesoresVisitantes: state.getIn(['sub', subActividadIndex, 'data',
                             'niveles', 1]) === 'SP',
  otro: state.getIn(['sub', subActividadIndex, 'data',
                     'niveles']).last() === otroID,
  editable: getContext().editable,
  catalogoLastLevel: state.getIn(['sub', subActividadIndex, 'data', 'niveles']).last(),
  subactividadId: state.getIn(['sub', subActividadIndex, 'dbId']),
});

const mapDispatchToProps = (
  dispatch,
  { subActividadIndex }
) => ({
  updateField: ({ target }) =>
    dispatch(updateSubActividadField(subActividadIndex, target.name, target.value)),
  updateFile: filePath =>
    dispatch(updateSubActividadField(subActividadIndex, 'file', filePath)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ServiciosFields);
