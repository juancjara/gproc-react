import React from 'react';
import { connect } from 'react-redux';

import DisplayOrArea from '../../../../../components/common/form/DisplayOrArea';
import { updateSubActividadField } from '../../../../../actions/presupuesto/subActividad/aSubActividad';
import patterns from '../../../../../patterns';
import { getContext } from '../../../../../context';

const SubActividadData = ({
  descripcion,
  updateField,
  title,
}) => (
  <td>
    <small>Descripción</small>
    <DisplayOrArea
      rows="5"
      value={descripcion}
      name={'descripcion'}
      pattern={patterns.all}
      onChange={updateField}
      editable={getContext().editable}
      required
    />
  </td>
);

const mapStateToProps = (
  state,
  { subActividadIndex }
) => ({
  descripcion: state.getIn(['sub', subActividadIndex, 'data', 'descripcion']),
});

const mapDispatchToProps = (
  dispatch,
  { subActividadIndex }
) => ({
  updateField: ({ target }) =>
    dispatch(updateSubActividadField(subActividadIndex, target.name, target.value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SubActividadData);
