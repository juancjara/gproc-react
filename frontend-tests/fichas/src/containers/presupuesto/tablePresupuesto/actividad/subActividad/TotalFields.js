import React from 'react';
import { connect } from 'react-redux';

import EditableField
  from '../../../../../components/common/form/EditableField';
import StaticField
  from '../../../../../components/common/form/StaticField';
import DisplayOrInput
  from '../../../../../components/common/form/DisplayOrInput';

import patterns from '../../../../../patterns';
import { getContext } from '../../../../../context';

import {
  updateSubActividadField
} from '../../../../../actions/presupuesto/subActividad/aSubActividad';

const TotalFields = ({
  data,
  updateField,
  editableCantidad,
  editable,
}) => (
  <td>
    <StaticField
      label="CU"
      smallLabel
      value={data.get('costo_unitario')}
    />
    <EditableField
      smallLabel
      editable={editableCantidad && editable}
      label="Cantidad"
      value={data.get('cantidad')}
      name="cantidad"
      onChange={updateField}
      required
      pattern={patterns.numbers}
    />
    <StaticField
      smallLabel
      value={data.get('unidad')}
      label="Unidad"
    />
    <StaticField
      smallLabel
      value={data.get('total')}
      label="TOTAL"
    />
 </td>
);

const isEditableCantidad = niveles =>
  niveles.get(0) !== 'S' && niveles.get(1) !== 'B2';

const mapStateToProps = (
  state,
  { subActividadIndex }
) => ({
  data: state.getIn(['sub', subActividadIndex, 'data']),
  editableCantidad: isEditableCantidad(state.getIn(['sub', subActividadIndex, 'data', 'niveles'])),
  editable: getContext().editable,
});

const mapDispatchToProps = (
  dispatch,
  { subActividadIndex }
) => ({
  updateField: ({ target }) =>
    dispatch(updateSubActividadField(subActividadIndex, target.name, target.value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TotalFields);
