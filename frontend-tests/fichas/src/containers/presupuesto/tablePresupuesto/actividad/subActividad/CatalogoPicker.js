import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import uuid from 'node-uuid';

import DisplayOrSelect
  from '../../../../../components/common/form/DisplayOrSelect';
import DisplayOrInput
  from '../../../../../components/common/form/DisplayOrInput';
import BoxError
  from '../../../../../components/common/BoxError';

import patterns from '../../../../../patterns';
import { getContext } from '../../../../../context';

import {
  selectLevel,
  updateSubActividadField,
} from '../../../../../actions/presupuesto/subActividad/aSubActividad';

export const otroID = 'otro';

// @TODO need refactor, maybe memoize get options
// refactor initial state
const shouldAddOtros = elementSelected =>
  elementSelected.name !== 'Nacional' &&
  elementSelected.id !== 'SP' &&
  elementSelected.id !== 'B2';

class CatalogoPicker extends Component {
  constructor(props) {
    super(props);
    const levelsData = [props.nivel1];
    const niveles = props.data.get('niveles');

    for (var i = 0, len = niveles.size - 1; i < len; i++) {
      this.getOptions(levelsData, i, niveles.get(i));
    }

    this.state = { levelsData };
    this.onChange = this.onChange.bind(this);
  }

  getOptions(levelsData, nivel, value) {
    if (!value) return;
    const elementSelected = levelsData[nivel]
      .filter(item => item.id === value)[0];
    const isLastChild = elementSelected && !elementSelected.children;
    const childrenArr = elementSelected.children.split(',');
    levelsData[nivel + 1] = getContext().catalogo
      .filter(({ id }) => _.indexOf(childrenArr, id) > -1);
    if (!levelsData[nivel + 1][0].children && shouldAddOtros(elementSelected)) {
      levelsData[nivel + 1].push({id: otroID , name: 'Otro'});
    }
    levelsData = levelsData.slice(0, nivel + 2);
  }

  onChange(nivel, value) {
    let { levelsData } = this.state;
    const elementSelected = levelsData[nivel]
      .filter(item => item.id === value)[0];

    const isLastChild = elementSelected && !elementSelected.children;

    let lastChildData = null;
    if (value && isLastChild && elementSelected.data) {
      lastChildData = JSON.parse(elementSelected.data);
    }

    if (value && isLastChild && elementSelected.id === otroID) {
      lastChildData = {
        costo: '',
        unidad: JSON.parse(levelsData[nivel][0].data).unidad,
      };
    }

    if (isLastChild && elementSelected && elementSelected.path) {
      lastChildData.path = elementSelected.path;
    }
    this.props.selectLevel(isLastChild, nivel, value, lastChildData);
    if (isLastChild || !value) {
      return;
    }

    const childrenArr = elementSelected.children.split(',');
    levelsData[nivel + 1] = getContext().catalogo
      .filter(({ id }) => _.indexOf(childrenArr, id) > -1);
    if (!levelsData[nivel + 1][0].children && shouldAddOtros(elementSelected)) {
         levelsData[nivel + 1].push({
           id: otroID ,
           name: 'Otro'
         });
    }
    levelsData = levelsData.slice(0, nivel + 2);

    this.setState({ levelsData });
  }

  getValue(value, level) {
    if (!value) return '';
    if (getContext().editable) return value;

    const options = this.state.levelsData[level]
      .filter(({ id }) => id === value);
    if (options.length) {
      return options[0].name;
    }
    return '';
  }

  getDefaultOption(actualLevel) {
    if (actualLevel > 1 && this.props.data.getIn(['niveles', 1]) === 'SP') {
      return 'Seleccione origen';
    }
    if (this.props.data.getIn(['niveles', 0]) === 'R' && actualLevel > 1) {
      return 'Seleccione origen/destino';
    }
    return 'Seleccione';
  }

  render() {
    const {
      levelsData,
    } = this.state;

    const {
      data,
      updateField,
    } = this.props;

    const levelsSelected = data.get('niveles');

    return (
      <td>
        <small>Catálogo</small>
        {
          _.times(levelsData.length).map(level =>
            (level === 0 || levelsSelected.get(level - 1)) && (
              <div key={level}>
                <DisplayOrSelect
                  required
                  defaultOption={this.getDefaultOption(level)}
                  name="catalogo"
                  editable={getContext().editable}
                  value={this.getValue(levelsSelected.get(level), level) || ''}
                  onChange={e => this.onChange(level, e.target.value)}
                  options={levelsData[level]}
                />
                {
                   levelsSelected.get(level) === otroID &&
                     <DisplayOrInput
                       editable={getContext().editable}
                       name="otro"
                       pattern={patterns.all}
                       value={data.get('otro')}
                       onChange={updateField}
                       required
                     />
                }
                {
                  getContext().editable &&
                  levelsSelected.get(level) === otroID &&
                    <BoxError
                      errors={['Recuerde que si selecciona "Otro" deberá sustentar el gasto']}
                    />
                }
              </div>
            )
          )
        }
      </td>
    );
  }
}

const mapStateToProps = (
  state,
  {
    subActividadIndex,
  }
) => ({
  nivel1: getContext().catalogo.filter(c => c.parent_id === null),
  data: state.getIn(['sub', subActividadIndex, 'data']),
});

const mapDispatchToProps = (
  dispatch,
  { subActividadIndex }
) => ({
  selectLevel: (isLastChild, level, value, lastChildData) =>
    dispatch(selectLevel(subActividadIndex, isLastChild, level, value, lastChildData)),
  updateField: ({ target }) =>
    dispatch(updateSubActividadField(subActividadIndex, target.name, target.value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CatalogoPicker);
