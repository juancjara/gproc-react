import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Map } from 'immutable';

import ActividadNodo from './actividad/ActividadNodo';

import { getContext } from '../../../context';

class Actividades extends Component {
  shouldComponentUpdate(nextProps) {
    //return this.props.subActividades !== nextProps.subActividades;
    //console.log(this.props.subActividades, nextProps.subActividades);
    return true;
  }

  render() {
    const {
      actividades,
      factorIndex,
    } = this.props;

    return (
      <td>
      {
        actividades.map((act, i) => (
          <ActividadNodo
            key={act.index}
            index={act.index}
            factorIndex={factorIndex}
            visibleIndex={i}
          />
        ))
      }
      </td>
    );
  }
}

const getActividades = (
  state,
  fact_codigo
) =>
  getContext().act
    .map((actividad, index) => ({
      index,
      fact_codigo: actividad.data.fact_codigo,
    }))
    .filter(actividad => actividad.fact_codigo === fact_codigo);

const mapStateToProps = (
  state,
  { factorIndex }
) => ({
  actividades: getActividades(
    state,
    getContext().factores[factorIndex].fact_codigo
  ),
  factorIndex,
  subActividades: state.get('sub').size,
});

export default connect(mapStateToProps)(Actividades);
