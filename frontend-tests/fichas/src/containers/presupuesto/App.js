import React from 'react';

import DetalleTable from './DetalleTable';
import FichaNodo from '../planificacion/FichaNodo';
import TablePresupuestoNodo from './TablePresupuestoNodo';
import SystemStatus from './SystemStatus';

import { getContext } from '../../context';

const App = () => {
  const editable = getContext().editable;

  return (
    <div className="form-horizontal ficha">
      {
        editable &&
          <SystemStatus />
      }
      <DetalleTable />
      <h4>
        <a href="/media/adminlte/template/bienes_procalidad.xlsx"
          target="_blank">
          Descargar catálogo de bienes y servicios{" "}
          <i className="fa fa-file"></i>
        </a>
      </h4>
      <TablePresupuestoNodo />
      <FichaNodo />
      {
        editable &&
          <SystemStatus dummyComponent />
      }
    </div>
  );
};

export default App;
