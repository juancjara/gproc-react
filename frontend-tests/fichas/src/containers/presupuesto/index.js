import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import { fromJS } from 'immutable';

import App from './App';
import { setContext } from '../../context';
import configureStore from '../../store/presupuesto';

import getInitialData from '../../initialData/presupuesto';

const context = window.__CONTEXT__;
const serverRender = window.__SERVER_RENDER__;
let initialState = window.__INITIAL_STATE__;

if ( ! serverRender) {
  initialState = getInitialData(initialState, context);
} else {
  initialState = fromJS(initialState);
}

setContext(context);

const store = configureStore(initialState);

ReactDOM.render(
  <Provider store={store} >
    <App />
  </Provider>,
  document.getElementById('root')
);
