import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import reactDOM from 'react-dom';

import { getContext } from '../../context';

import connectSystemStatus from '../common/connectSystemStatus';
import * as messagesPresupuesto from '../../messages/presupuesto';

import {
  finishFicha,
  showErrorsAndSaveAllNodes,
} from '../../actions/presupuesto/aSystem';

import { summaryStatus } from '../../utils/status';

import actions from '../../constants/status';
const {
  STORED_CHANGED,
  SERVER_ERROR,
  FETCHING,
  STORED_OK,
  UNAUTHORIZED_ERROR,
} = actions;

import constantsPresupuesto from '../../constants/presupuesto';
const {
  REQUEST_ADD_SUBACTIVIDAD,
  REQUEST_REMOVE_SUBACTIVIDAD,
} = constantsPresupuesto;

import { getSeccionesError } from '../planificacion/SystemStatus';

const getCustomAlertMessage = ({ requestType, status }) => {
  const saveOK = status === STORED_OK || status === STORED_CHANGED;

  if (saveOK && requestType === REQUEST_ADD_SUBACTIVIDAD) {
    return {
      alertMessage: messagesPresupuesto.ADD_SUBACTIVIDAD_OK,
      type: 'success',
    };
  }

  if (saveOK && requestType === REQUEST_REMOVE_SUBACTIVIDAD) {
    return {
      alertMessage: messagesPresupuesto.REMOVE_SUBACTIVIDAD_OK,
      type: 'success',
    };
  }

  return null;
};


// @TODO: refactor this
const flattenStatus = state => {
  const factoresWithIndexes = _.map(
    getContext().factores,
    (f, index) => ({
      ...f,
      index,
    })
  );

  const actGroupedByFactor = state.get('act')
    .groupBy(act => act.getIn(['data', 'fact_codigo']));

  const subActividadesGrouped = state.get('sub')
    .groupBy(sub => sub.get('parent_id'));

  const actividadesStatus = [];
  const subActividadesStatus = [];

  _.forEach(factoresWithIndexes, f => {
    if (actGroupedByFactor.has(f.fact_codigo)) {
      actGroupedByFactor.get(f.fact_codigo)
        .forEach((act, actIndex) => {
          actividadesStatus.push({
            status: act.get('status').toJS(),
            name: `A.${f.index + 1}.${actIndex + 1}`
          });

          if (subActividadesGrouped.has(act.get('dbId'))) {
            subActividadesGrouped.get(act.get('dbId'))
              .forEach((sub, i) => {
                subActividadesStatus.push({
                  status: sub.get('status').toJS(),
                  name: `Sub.${f.index + 1}.${actIndex + 1}.${i + 1}`,
                });
              });
          }
        });
    }
  });

  return [
    ...actividadesStatus,
    ...subActividadesStatus,
    {
      name: 'Totales',
      status: state.getIn(['totales', 'status']).toJS()
    },
    {
      name: 'Datos del responsable',
      status: state.getIn(['fic', 'status']).toJS()
    },
  ];
};

const mapStateToProps = (
  state
) => {
  const flatStatus = flattenStatus(state);
  const secciones = getSeccionesError(flatStatus);
  const statuses = _.pluck(flatStatus, 'status');

  return {
    status: summaryStatus(statuses, secciones.length),
    secciones,
    requestType: state.get('requestType'),
    urlPDF: '/presupuesto/pdf',
    getCustomAlertMessage,
  };
};

const mapDispatchToProps = (
  dispatch
) => ({
  onSaveAll: () => dispatch(showErrorsAndSaveAllNodes()),
  onFinish: status => dispatch(finishFicha(status)),
});

export default connectSystemStatus(mapStateToProps, mapDispatchToProps);
