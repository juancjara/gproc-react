import React from 'react';

import FichaNodo from '../planificacion/FichaNodo';
import TableEfectosNodo from './TableEfectosNodo';
import SystemStatus from './SystemStatus';

import { getContext } from '../../context';

const App = () => {
  const editable = getContext().editable;
  return (
    <div className="form-horizontal ficha">
      {
        editable &&
          <SystemStatus />
      }
      <TableEfectosNodo />
      <FichaNodo />
      {
        editable &&
          <SystemStatus dummyComponent />
      }
    </div>
  );
}

export default App;
