import React from 'react';

import TableHeader from '../../components/common/table/TableHeader';
import TableFooter from '../../components/common/table/TableFooter';
import TableBodyPresupuesto
  from '../presupuesto/tablePresupuesto/TableBodyPresupuesto';
import RowEfectos
  from './tableEfectos/RowEfectos';

import { getContext } from '../../context';

const headers = [
  { display : 'Objetivo Específico', customClass: '' },
  { display : 'Resultado', customClass: '' },
  { display : 'Efecto Esperado', customClass: '' },
];

const TableEfectosNodo = () => (
  <table className="table table-bordered table-hover">
    <TableHeader
      classTh={'text-center success'}
      headers={headers}
    />
    <TableBodyPresupuesto RowComp={RowEfectos} />
    <TableFooter
      classTh={'text-center success'}
      headers={headers}
    />
  </table>
);

export default TableEfectosNodo;
