import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import TdPresupuesto from '../../../components/presupuesto/TdPresupuesto';
import EfectoNodo from './EfectoNodo';

import { mapStateToProps }
  from '../../presupuesto/tablePresupuesto/RowPresupuesto';

class RowEfectos extends Component {
  shouldComponentUpdate(nextProps) {
    return false;
  }

  render() {
    const {
      firstRow,
      dimensionData,
      factorData,
    } = this.props;

    return (
      <tr>
        {
          firstRow && (
            <TdPresupuesto
	      show
              {...dimensionData}
            />
          )
        }
        <TdPresupuesto 
	  show
	  {...factorData} 
	/>
        <EfectoNodo
          factorIndex={factorData.index}
        />
      </tr>
    );
  }
};

export default connect(mapStateToProps)(RowEfectos);
