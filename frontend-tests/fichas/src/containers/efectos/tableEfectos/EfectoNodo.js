import React from 'react';
import { connect } from 'react-redux';

import Efecto from './Efecto';
import { connectStatus } from '../../common/connectStatus';

import { findIndexBy } from '../../../utils';
import { getContext } from '../../../context';

const EfectoNodo = (
  props
) => {
  const EfectoConnected = connect(
    state => {
      const factDbId = getContext().factores[props.factorIndex].id;
      const index = findIndexBy(
        state.get('efe'),
        factDbId,
        e => e.get('parent_id')
      );
      const path = ['efe', index];
      return {
        path,
        status: state.getIn(path.concat(['status'])),
        index,
      };
    },
    dispatch => ({
      storeNodo: () => {},
    })
  )(connectStatus(props)(Efecto));

  return <EfectoConnected />;
};

export default EfectoNodo;
