import React from 'react';
import { connect } from 'react-redux';

import { updateEfecto } from '../../../actions/efectos/aEfecto';

import StatusBadges from '../../../components/common/status/StatusBadges';
import DisplayOrArea from '../../../components/common/form/DisplayOrArea';

import patterns from '../../../patterns';
import { getContext } from '../../../context';

const Efecto = ({
  descripcion,
  onChange,
  name,
  editable,
  factorIndex,
  rootId,
  summaryStatus,
}) => (
  <td id={rootId}>
    {
      editable &&
        <StatusBadges status={summaryStatus} />
    }
    {name}
    <DisplayOrArea
      rows="5"
      value={descripcion}
      name={'descripcion'}
      pattern={patterns.all}
      onChange={onChange}
      editable={editable}
      required
    />
  </td>
);

const mapStateToProps = (
  state,
  {
    factorIndex,
    index,
  }
) => {
  return {
    name: `E.${factorIndex + 1}`,
    editable: getContext().editable,
    descripcion: state.getIn(['efe', index, 'data', 'descripcion']),
  };
};

const mapDispatchToProps = (
  dispatch,
  { index }
) => ({
  onChange: ({ target }) =>
    dispatch(updateEfecto(index, target.name, target.value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Efecto);

