import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { getContext } from '../../context';

import connectSystemStatus from '../common/connectSystemStatus';

import {
  finishFicha,
  showErrorsAndSaveAllNodes,
} from '../../actions/efectos/aSystem';

import { summaryStatus } from '../../utils/status';

import actions from '../../constants/status';
const {
  STORED_CHANGED,
  SERVER_ERROR,
  FETCHING,
  STORED_OK,
  UNAUTHORIZED_ERROR,
} = actions;

import constantsPresupuesto from '../../constants/monitoreo';
const {
  REQUEST_ADD_INDICADOR,
  REQUEST_REMOVE_INDICADOR,
} = constantsPresupuesto;

import { getSeccionesError } from '../planificacion/SystemStatus';

const getCustomAlertMessage = () => null;

// @TODO: refactor this
const flattenStatus = state => {

  const efeGroupedByFactor = state.get('efe')
    .groupBy(efe => efe.get('parent_id'));

  const efectosStatus = [];

  _.forEach(getContext().factores, (f, factorIndex) => {

    if (efeGroupedByFactor.has(f.id)) {
      efeGroupedByFactor.get(f.id)
        .forEach((efe, efeIndex) => {
          efectosStatus.push({
            status: efe.get('status').toJS(),
            name: `I.${factorIndex + 1}`
          });
        });
    }
  });

  return [
    ...efectosStatus,
    {
      name: 'Datos del responsable',
      status: state.getIn(['fic', 'status']).toJS()
    },
  ];
};

const mapStateToProps = (
  state
) => {
  const flatStatus = flattenStatus(state);
  const secciones = getSeccionesError(flatStatus);
  const statuses = _.pluck(flatStatus, 'status');

  return {
    status: summaryStatus(statuses, secciones.length),
    secciones,
    requestType: state.get('requestType'),
    urlPDF: '/efectos/pdf',
    getCustomAlertMessage,
  };
};

const mapDispatchToProps = (
  dispatch
) => ({
  onSaveAll: () => dispatch(showErrorsAndSaveAllNodes()),
  onFinish: status => dispatch(finishFicha(status)),
});

export default connectSystemStatus(mapStateToProps, mapDispatchToProps);
