import _ from 'lodash';

import statusActions from '../constants/status';
const {
  SERVER_ERROR,
  FETCHING,
  STORED,

  STORED_OK,
  STORED_CHANGED,
} = statusActions;

export const summaryStatus = (statuses) => {
  const arrStatus = _.map(statuses, ({status}) => status);

  if (_.indexOf(arrStatus, FETCHING) > -1) {
    return FETCHING;
  }

  if (_.indexOf(arrStatus, SERVER_ERROR) > -1) {
    return SERVER_ERROR;
  }

  if (_.every(arrStatus, s => s === STORED) &&
      _.every(statuses, ({ hasChanged }) => !hasChanged)) {
    return STORED_OK;
  }

  return STORED_CHANGED;
};
