import { Map, fromJS } from 'immutable';
import _ from 'lodash';

export const addIndexImmutable = (elem, i) =>
  elem.set('index', i);

export const getIn = (obj, path, defaultValue) => {
  if (!obj) return defaultValue;

  const keys = path.split('.');
  let tempObj = _.cloneDeep(obj);
  for (var i = 0, len = keys.length; i < len; i++) {
    if (tempObj !== null && tempObj.hasOwnProperty(keys[i])) {
      tempObj = tempObj[keys[i]];
    } else {
      return defaultValue;
    }
  }
  return tempObj;
};

export const hasIn = (obj, path) => {
  if (!obj) return false;

  const keys = path.split('.');
  let tempObj = _.cloneDeep(obj);
  for (var i = 0, len = keys.length; i < len; i++) {
    if (tempObj && tempObj.hasOwnProperty(keys[i])) {
      tempObj = tempObj[keys[i]];
    } else {
      return false;
    }
  }
  return true;
};

export const flatMap = _.compose(_.flatten, _.map);

const updateIfEmpty = v => (v === '' || v === undefined || v === null) ? 'N/A': v;

export const fillObjEmptyFieldsWithNA = (obj, ownKeys) => {
  let keys;
  if (ownKeys) {
    keys = ownKeys;
  } else {
    keys = _.keys(obj.toJS());
  }
  let mutable = obj.asMutable();
  keys.forEach(k => {
    mutable = mutable.update(k, updateIfEmpty);
  });
  return mutable.asImmutable();
};

export const fillTableEmptyCellsWithNA = ({
  table,
  rowKeys,
  colKeys,
}) => {
  let mutable = table.asMutable();
  rowKeys.forEach(rKey => {
    colKeys.forEach(cKey => {
      mutable = mutable.updateIn([rKey, cKey], updateIfEmpty);
    });
  });
  return mutable.asImmutable();
};

export const getKeys = rowHeaders =>
  _(rowHeaders)
    .map(_.keys)
    .flatten()
    .value();

const unicode = {
  'à': 'a',
  'á': 'a',
  'â': 'a',
  'á': 'a',
  'ã': 'a',
  'ä': 'a',
  'è': 'e',
  'é': 'e',
  'ê': 'e',
  'ë': 'e',
  'ì': 'i',
  'í': 'i',
  'î': 'i',
  'ï': 'i',
  'ò': 'o',
  'ó': 'o',
  'ô': 'o',
  'õ': 'o',
  'ö': 'o',
  'ù': 'u',
  'ú': 'u',
  'û': 'u',
  'ü': 'u',
  ' ': '',
};


export const cleanText = (myText) => {
  myText = myText || '';
  return Object.keys(unicode).reduce((newText, key) => {
    const reg = new RegExp(key, 'g');
    return newText.replace(reg, unicode[key]);
  }, myText.toLowerCase());
};

const words = {
  'Falta': 'n',
  'estándar': 'es',
  'obligatorio': 's',
  'medio': 's',
};

export const pluralize = (
  word,
  number
) => word + (number > 1 ? words[word]: '');

export const errorExists = (elem, errors) =>
  errors.filter(error => _.isEqual(elem, error)).length;

export const getBoxErrors = (key, errors) =>
  _(errors)
    .filter(error => error.hasOwnProperty('key') && error.key === key)
    .pluck('message')
    .value();

export const formatTableDicDimensionFrom3To2 = (table) => {
  const tableMutable = {};

  table.forEach((v, rowKey) => {
    tableMutable[rowKey] = {};

    v.forEach((col, colKey) => {
      col.forEach((value, k) => {
        tableMutable[rowKey][`${colKey}/${k}`] = value;
      });
    });
  });

  return fromJS(tableMutable);
};

const formatRowDimension = (acc, col, key) => {
  if (Map.isMap(col)) {
    return acc.merge(
      col.reduce(
        (genderAcc, value, k) =>
          genderAcc.set(`${key}/${k}`, value)
        , Map()
      )
    );
  } else {
    return acc.set(key, col);
  }
};

export const formatTableDimensionFrom3to2 = (table) => {
  return table.map((row) => {
    return row.reduce(formatRowDimension, Map());
  });
};

export const toNumber = (x) => +x || 0;

export const isDisabledInstitucional = (
  state,
  isInstitucional
) => isInstitucional &&
  state.getIn(['ins', 'status', 'status']) === 'FETCHING';

export const isDisabledPlanificacionAddActividad = (
  factores
) => factores
  .filter(f => f.getIn(['status', 'status']) === 'FETCHING')
  .size !== 0;

export const isDisabledPresupuesto = state =>
  state.get('act')
    .filter(nodo => nodo.getIn(['status', 'status']) === 'FETCHING')
    .size !== 0;

const anyNodoFetching = (state, key) =>
  state.get(key)
    .filter(nodo => nodo.getIn(['status', 'status']) === 'FETCHING')
    .size !== 0;

export const isDisabledMonitoreo = state =>
  anyNodoFetching(state, 'fac') || anyNodoFetching(state, 'ind')
  || state.getIn(['fic', 'status', 'status']) === 'FETCHING';

export const findIndexBy = (
  table,
  identifier,
  selector
) => table.findIndex(row => selector(row) === identifier);

export const findIndexById = (
  table,
  lookupId
) => findIndexBy(table, lookupId, (item) => item.getIn(['data', 'id']));

function mapValues(obj, fn) {
  return Object.keys(obj).reduce((result, key) => {
    return result.set(key, fn(obj[key], key));
  }, Map());
}

export const combineImmutableReducers = (
  reducers
) => {
  const initialState = Map();
  return (state = initialState, action) => mapValues(
    reducers,
    (reducer, key) => reducer(state.get(key), action)
  );
};

export const validateInlineErrors = (
  nodeHTML
) => {
  if (!nodeHTML) {
    return true;
  }
  const errors = [...nodeHTML.querySelectorAll('input, textarea, select')]
    .filter((el) => validateField(el) !== '');

  return errors.length === 0;
};

export const validateField = (
  {
    pattern,
    value,
    required
  },
  messages = {}
) => {
  const {
    msgRequired = "Campo requerido",
    msgPatternFailed = "No coincide con el formato solicitado",
  } = messages;

  if (required && value === "") {
    return msgRequired;
  }
  if ( ! required && value === "") {
    return '';
  }

  if (pattern && pattern.length && ! new RegExp(pattern).test(value)) {
    return msgPatternFailed;
  }

  return '';
};
