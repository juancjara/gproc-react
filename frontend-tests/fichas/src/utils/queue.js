import request from 'superagent';

const queue = [];
let working = false;

const isEmpty = (queue) => queue.length === 0;

const processNext = () => {
  if (isEmpty(queue)) {
    working = false;
    return;
  }

  working = true;
  const {
    url,
    method = 'POST',
    data = {},
    onSuccess,
    onError,
  } = queue.shift();

  let req;

  if (method === 'POST') {
    req = request
      .post(url)
      .send(data);
  } else if (method === 'PUT') {
    req = request
      .put(url)
      .send(data);
  }

  //setTimeout(() => {
    req
      .timeout(6000)
      .end((err, res) => {
        console.log(err, res);
        //console.log(err.statusText, err.timeout);
        //console.log(res && res.text);

        // Error status code !== 200 or timeout
        if (err) {
          onError({
            code: res && res.statusCode,
            message: res && res.statusText || 'Error de espera',
          });
        }

        // status code === 200 but response !== json
        else if ( ! res.body) {
          onError({message: 'Respuesta desconocida del servidor'});
        }

        // status code === 200 and known error
        else if (res.body.statusText === 'ERROR') {
          onError(res.body);
        }

        // success
        else {
          onSuccess(res.body);
        }

        processNext();
      });

  //}, 2000);
};

const addToQueue = (
	item
) => {
	queue.push(item);
	if ( ! working) processNext();
};

export default addToQueue;
