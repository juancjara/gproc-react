//import 'babel-polyfill';

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import { fromJS } from 'immutable';

//console.log(Array.from);

import rootReducer from './reducers/expresionInteres';
import App from './containers/expresionInteres/App';
import { setContext } from './context';
import configureStore from './store/expresionInteres';

import getInitialData from './initialData/expresionInteres';

const context = window.__CONTEXT__;
const serverRender = window.__SERVER_RENDER__;
let initialState = fromJS(window.__INITIAL_STATE__);

if ( ! serverRender) {
  initialState = getInitialData(initialState, context);
}

setContext(context);
const store = configureStore(initialState);

ReactDOM.render(
  <Provider store={store} >
    <App />
  </Provider>,
  document.getElementById('root')
);
