import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

export default (initialState, rootReducer) => {
  const createStoreWithMiddleware = applyMiddleware(
    thunk
  )(createStore);
  const store = createStoreWithMiddleware(rootReducer, initialState);

  return store;
};
