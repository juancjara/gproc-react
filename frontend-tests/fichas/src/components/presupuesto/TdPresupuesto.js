import React from 'react';

const TdPresupuesto = ({
  index,
  title,
  size = 1,
  name,
  comp,
  show
}) => (
  <td rowSpan={size}>
    <div title={title}>
      {name}
      {comp}
    </div>
    {
      show ? title: ''
    }
  </td>
);

export default TdPresupuesto;
