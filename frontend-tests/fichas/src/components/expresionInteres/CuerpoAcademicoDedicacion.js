import React from 'react';
import _ from 'lodash';

import { rowHeaders } from './CuerpoAcademicoCondicion';
import CuerpoAcademico from './common/CuerpoAcademico';

const headers = [
  { key: 'label', display: 'Grados y Títulos' },
  { key: 'ex', display: 'Exclusiva' },
  { key: 'tc', display: 'Tiempo Completo' },
  { key: 'tp', display: 'Tiempo Parcial' },
  { key: 'tt', display: 'Total' },
];
const customHeaders = [
  {
    display: headers[0].display,
    attr: {
      rowSpan: 2,
      className: 'col-xs-4',
    },
  },
  {
    display: 'Dedicación',
    attr: {
      colSpan: headers.length - 1,
    },
  },
];

const CuerpoAcademicoDedicacion = ({
  data,
  onChange,
  editable,
  classTable = '',
}) => (
  <CuerpoAcademico
    className={`col-lg-6 ${classTable}`}
    customHeaders={customHeaders}
    headers={headers}
    data={data}
    rowHeaders={rowHeaders}
    editable={editable}
    onChange={onChange}
  />
);

export default CuerpoAcademicoDedicacion;
