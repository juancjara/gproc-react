import React from 'react';
import _ from 'lodash';

import patterns from '../../../patterns';
import TableBodyDic from '../../common/table/TableBodyDic';
import CustomTableHeader from '../../common/table/CustomTableHeader';
import { rowHeadersYears } from '../../../constants/expresionInteres';

const headers = [
  { key: 'label', display: 'Año' },
  { key: 'ti', display: 'Nombre y N° de Registro' },
  { key: 'ac', display: 'Modalidad' },
  { key: 'in', display: 'Institución que la otorga' },
];

const customHeaders = [
  {
    display: headers[0].display,
    attr: {
      rowSpan: 2,
      className: 'col-xs-4',
    },
  },
  {
    display: 'Patentes',
    attr: {
      colSpan: headers.length - 1,
    },
  },
];

const Patentes = ({
  data,
  onChange,
  editable,
  errorMsg,
}) => (
  <table className="table table-bordered table-condensed table-centered">
    <CustomTableHeader
      customHeaders={customHeaders}
      simpleHeaders={_.drop(headers)}
    />
    <TableBodyDic
      editable={editable}
      headers={headers}
      rowHeaders={rowHeadersYears}
      body={data}
      newColKey={'label'}
      headerCol= {['label']}
      readOnlyCol={['label']}
      onChange={onChange}
      pattern={patterns.all}
      errorMsg={errorMsg}
    />
  </table>
);

export default Patentes;
