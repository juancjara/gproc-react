import React from 'react';
import _ from 'lodash';

import patterns from '../../../patterns';
import TableBodyDic from '../../common/table/TableBodyDic';
import CustomTableHeader from '../../common/table/CustomTableHeader';
import { rowHeadersYears } from '../../../constants/expresionInteres';

const headers = [
  { key: 'label', display: 'Año' },
  { key: 'ti', display: 'Título' },
  { key: 'ac', display: 'Área de conocimiento' },
  { key: 'in', display: 'Investigador(es)' },
];

const customHeaders = [
  {
    display: headers[0].display,
    attr: {
      rowSpan: 2,
      className: 'col-xs-4',
    },
  },
  {
    display: 'Investigaciones',
    attr: {
      colSpan: headers.length - 1,
    },
  },
];

const Investigaciones = ({
  data,
  onChange,
  editable,
  errorMsg,
}) => (
  <table className="table table-bordered table-condensed table-centered">
    <CustomTableHeader
      customHeaders={customHeaders}
      simpleHeaders={_.drop(headers)}
    />
    <TableBodyDic
      editable={editable}
      headers={headers}
      rowHeaders={rowHeadersYears}
      body={data}
      newColKey={'label'}
      readOnlyCol={['label']}
      headerCol= {['label']}
      onChange={onChange}
      pattern={patterns.all}
      errorMsg={errorMsg}
    />
  </table>
);

export default Investigaciones;
