import React, { Component } from 'react';
import { Map, fromJS } from 'immutable';

import patterns from '../../../patterns';
import BoxHeader from '../../common/BoxHeader';
import { rowHeadersYears } from '../../../constants/expresionInteres';

import TableDic
  from '../../common/table/TableDic';

const headers = [
  { key: 'label', display: '' },
  { key: 'data', display: '' },
];

const ActividadesServicios = ({
  title,
  subtitle,
  number,
  body,
  editable,
  header,
  onChange,
  errorMsg,
}) => {
  headers[1].display = header;
  return (
    <div className="col-sm-6">
      <div className="box">
        <BoxHeader
          number={number}
          title={title}
          subtitle={subtitle}
        />
        <div className="box-body">
          <TableDic
            className={"table table-bordered table-condensed table-centered"}
            editable={editable}
            body={body.map(v => Map({ data: v }))}
            rowHeaders={rowHeadersYears}
            headers={headers}
            newColKey={'label'}
            readOnlyCol={['label']}
            headerCol={['label']}
            onChange={onChange}
            pattern={patterns.all}
            errorMsg={errorMsg}
          />
        </div>
      </div>
    </div>
  );
};

export default ActividadesServicios;
