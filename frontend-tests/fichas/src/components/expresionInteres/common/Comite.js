import React from 'react';

import patterns from '../../../patterns';
import Table from '../../common/table/Table';
import AddButton from '../../common/AddButton';
import * as messages from '../../../messages/common';

const headers = [
  { key: 'pers_nombres', display: 'Nombres *' },
  { key: 'pers_apellidos', display: 'Apellidos *' },
  { key: 'pers_cargo', display: 'Cargo *' },
  { key: 'pers_correo', display: 'Correo *' },
  { key: 'pers_telefono', display: 'Teléfono *' },
];

const Comite = ({
  comite,
  onComiteUpdate,
  removeMember,
  editable,
  canBeRemoved,
  addMember,
  children,
}) => (
  <div className="box-body box-responsive">
    {children}
    <Table
      className="table table-condensed"
      editable={editable}
      body={comite}
      headers={headers}
      onChange={onComiteUpdate}
      canBeRemoved={canBeRemoved}
      onRemove={removeMember}
      pattern={patterns.all}
      specificPatterns={[
        {
          key: 'pers_correo',
          pattern: patterns.email,
          errorMsg: messages.ERROR_EMAIL
        },
        {
          key: 'pers_telefono',
          pattern: patterns.numbers,
          errorMsg: messages.ERROR_TELEFONO
        }
      ]}
    />

    <AddButton
      title={'Agregar miembro'}
      editable={editable}
      onClick={addMember}
    />

  </div>
);

export default Comite;
