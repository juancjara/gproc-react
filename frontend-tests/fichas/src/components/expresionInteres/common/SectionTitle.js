import React from 'react';

const SectionTitle = ({
  title,
  subtitle,
  children,
}) => (
  <div>
    <h4 className="text-center" >
      {title}
      {children}
    </h4>
		<h4>
      <small>
        {subtitle}
      </small>
    </h4>
  </div>
);

export default SectionTitle;
