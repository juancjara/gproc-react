import React from 'react';
import _ from 'lodash';

import TableBodyDic from '../../common/table/TableBodyDic';
import CustomTableHeader from '../../common/table/CustomTableHeader';
import * as messages from '../../../messages/common';

const CuerpoAcademico = ({
  headers,
  rowHeaders,
  customHeaders,
  data,
  onChange,
  editable,
  className,
}) => (
  <div className={className}>
    <table className="table table-bordered table-condensed table-centered table_condicion">
      <CustomTableHeader
        customHeaders={customHeaders}
        simpleHeaders={_.drop(headers)}
      />
      <TableBodyDic
        labelTh
        editable={editable}
        headers={headers}
        rowHeaders={rowHeaders}
        body={data}
        newColKey={'label'}
        headerCol= {['label']}
        readOnlyCol= {['tt', 'label']}
        readOnlyRow= {['tt']}
        onChange={onChange}
        errorMsg={messages.ERROR_NUMERIC_FIELD}
      />
    </table>
  </div>
);

export default CuerpoAcademico;
