import React from 'react';

import TableDic from '../common/table/TableDic';

const headers = [
  { key: 'label', display: 'Categoría docente' },
  { key: 'tt', display: 'Total' },
];
const rowHeaders = [
  { pr: 'Principal' },
  { as: 'Asociado' },
  { au: 'Auxiliar' },
  { tt: 'Total' },
];

const CuerpoAcademicoDocente = ({
  data,
  onChange,
  editable,
  classTable,
}) => (
  <div className="col-sm-4">
    <TableDic
      className={`table table-bordered table-condensed table-centered docentes ${classTable}`}
      editable={editable}
      headers={headers}
      body={data}
      rowHeaders={rowHeaders}
      newColKey = {'label'}
      headerCol={['label']}
      readOnlyRow={['tt']}
      onChange={onChange}
    />
  </div>
);

export default CuerpoAcademicoDocente;
