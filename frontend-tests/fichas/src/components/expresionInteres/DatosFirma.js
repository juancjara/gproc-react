import React from 'react';
import _ from 'lodash';

import BoxHeader from '../common/BoxHeader';
import EditableField from '../common/form/EditableField';
import InputField from '../common/form/InputField';
import DisplayField from '../common/form/DisplayField';
import DisplayOrInput from '../common/form/DisplayOrInput';

import patterns from '../../patterns';

const fields = [
  {
    key: 'nombre',
    label: 'Apellidos y nombres *',
  },
  {
    key: 'cargo',
    label: 'Cargo *',
  },
  {
    key: 'direccion',
    label: 'Dirección institucional',
  },
];

export const DatosFirma = ({
  title,
  data,
  onChange,
  editable,
}) => {
  let EditableComp = DisplayField;
  if (editable) {
    EditableComp = InputField;
  }

  return (
    <div className="box">
      <BoxHeader title={title} />

      <div className="box-body">
        {_.map(fields, ({ key, label }) => (
          <EditableField
            editable={editable}
            label={label}
            key={key}
            name={key}
            value={data.get(key)}
            onChange={onChange}
            required
          />
        ))}

        <div className="form-group">
          <label className="col-xs-3 col-sm-2 control-label">
            Teléfonos
          </label>

            <DisplayOrInput
              classWrapper="col-xs-4 col-sm-3"
              name={'fijo'}
              value={data.get('fijo')}
              editable={editable}
              onChange={onChange}
              pattern={patterns.numbers}
              placeholder={'Fijo'}
            />

          <DisplayOrInput
            classWrapper="col-xs-4 col-sm-3"
            name={'celular'}
            value={data.get('celular')}
            editable={editable}
            onChange={onChange}
            pattern={patterns.numbers}
            placeholder={'Celular'}
          />

        </div>

        <div className="form-group">
          <label className="col-xs-3 col-sm-2 control-label">
            Email *
          </label>

            <DisplayOrInput
              classWrapper="col-xs-4 col-sm-3"
              name={'email'}
              value={data.get('email')}
              editable={editable}
              onChange={onChange}
              pattern={patterns.email}
              placeholder={'Institucional'}
              required
            />

            <DisplayOrInput

              classWrapper="col-xs-4 col-sm-3"
              name={'emailalt'}
              value={data.get('emailalt')}
              editable={editable}
              onChange={onChange}
              pattern={patterns.email}
              placeholder={'Alternativo'}
            />
        </div>

        <div className="form-group">
          <label className="col-xs-3 col-sm-2 control-label">
            Fecha y firma
          </label>
          <div className="col-xs-4 col-sm-3">
            <br/>
            <br/>
            <br/>
            __________________________
          </div>
          <div className="col-xs-5 col-sm-3">
            <br/>
            <br/>
            <br/>
            <p className="form-control-static text-right">
              {data.get('date_local')}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export const DatosFirmas = ({
  instData,
  carrData,
  onUpdateInst,
  onUpdateCarr,
  editable,
  isInstitucional,
}) => (
  <div>
    <DatosFirma data={instData}
      editable={editable}
      title={'Datos y firma de la autoridad Institucional'}
      onChange={onUpdateInst}
    />
    {!isInstitucional &&
       <DatosFirma data={carrData}
         editable={editable}
         title={'Datos y firma de la autoridad de la Carrera'}
         onChange={onUpdateCarr}
       />
    }
  </div>
);

export default DatosFirmas;
