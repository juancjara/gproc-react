import React from 'react';
import _ from 'lodash';

import { formatTableDimensionFrom3to2 } from '../../utils';
import TableBody from '../common/table/TableBody';
import CustomTableHeader from '../common/table/CustomTableHeader';
import * as messages from '../../messages/common';

const headers = [
  { key: 'nombre', display: 'Carrera' },
  { key: 'x2015/m', display: 'M' },
  { key: 'x2015/f', display: 'F' },
  { key: 'x2014/m', display: 'M' },
  { key: 'x2014/f', display: 'F' },
  { key: 'x2013/m', display: 'M' },
  { key: 'x2013/f', display: 'F' },
  { key: 'x2012/m', display: 'M' },
  { key: 'x2012/f', display: 'F' },
  { key: 'x2011/m', display: 'M' },
  { key: 'x2011/f', display: 'F' },
];

const getCustomHeaders = (
  title
)  => [
  {
    display: headers[0].display,
    attr: {
      rowSpan: 3,
      className: 'col-xs-4',
    },
  },
  {
    display: title,
    attr: {
      colSpan: headers.length - 1,
    },
  },
];

const extraHeaders = [
  {display:  '2015', attr: {colSpan: 2}},
  {display:  '2014', attr: {colSpan: 2}},
  {display:  '2013', attr: {colSpan: 2}},
  {display:  '2012', attr: {colSpan: 2}},
  {display:  '2011', attr: {colSpan: 2}},
];

const TotalEstudiantesTable = ({
  title,
  data,
  onChange,
  editable,
}) => (
  <div className="col-sm-6">
    <table className="table table-bordered table-condensed table-centered table_carreras">
      <CustomTableHeader
        customHeaders={getCustomHeaders(title)}
        extraHeaders={extraHeaders}
        simpleHeaders={_.drop(headers)}
      />
      <TableBody
        editable={editable}
        headers={headers}
        body={formatTableDimensionFrom3to2(data)}
        readOnlyCol={['nombre']}
        onChange={onChange}
        errorMsg={messages.ERROR_NUMERIC_FIELD}
      />
    </table>
  </div>

);

export default TotalEstudiantesTable;
