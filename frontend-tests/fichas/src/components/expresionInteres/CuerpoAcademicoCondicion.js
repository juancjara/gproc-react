import React from 'react';
import _ from 'lodash';

import CuerpoAcademico from './common/CuerpoAcademico';

const headers = [
  { key: 'label', display: 'Grados y Títulos' },
  { key: 'no', display: 'Nombrado' },
  { key: 'co', display: 'Contratado' },
  { key: 'tt', display: 'Total' },
];

const customHeaders = [
  {
    display: headers[0].display,
    attr: {
      rowSpan: 2,
      className: 'col-xs-4',
    },
  },
  {
    display: 'Condición',
    attr: {
      colSpan: headers.length - 1,
    },
  },
];

export const rowHeaders = [
  { pt: 'Profesional Técnico' },
  { pe: 'Profesional Técnico con mención especializada' },
  { bc: 'Bachiler' },
  { lt: 'Licenciado / Titulado' },
  { mm: 'Magister / Master' },
  { dr: 'Doctor' },
  { tt: 'Total' },
];

const CuerpoAcademicoCondicion = ({
  data,
  onChange,
  editable,
  classTable = '',
}) => (
  <CuerpoAcademico
    rowHeaders={rowHeaders}
    customHeaders={customHeaders}
    headers={headers}
    editable={editable}
    data={data}
    onChange={onChange}
    className={`col-lg-6 ${classTable}`}
  />
);

export default CuerpoAcademicoCondicion;
