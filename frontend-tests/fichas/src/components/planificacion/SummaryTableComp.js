import React from 'react';

import SummaryTableCellPercentage from './SummaryTableCellPercentage';
import TableHeader from '../common/table/TableHeader';
import TableBodyDic from '../common/table/TableBodyDic';

const SummaryTableComp = ({
  data,
  percRows,
  headers,
  rowHeaders,
  totalPasantias,
  percPasantias,
}) => (
  <div className="pull-right table-totales" id="table-total-wrapper">
    <table className="table table-bordered table-condensed table-total">
      <TableHeader
        headers={headers}
        classTh={'text-center success'}
      />
      <TableBodyDic
        editable={false}
        rowHeaders={rowHeaders}
        headers={headers}
        body={data}
        newColKey={'label'}
        readOnlyCol={['label']}
        extraCellComponents={percRows}
      />
      <tfoot>
        <tr>
          <td colSpan="8" className="text-right">
            {`Total pasantías FEC: ${totalPasantias}`}
            <SummaryTableCellPercentage
              perc={percPasantias}
            />
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
);

export default SummaryTableComp;
