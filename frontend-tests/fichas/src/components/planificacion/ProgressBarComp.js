import React from 'react';

const ProgressBarComp = ({
  title,
  percentage,
}) => (
  <div className="pull-right">
		<h4 className="text-right title_resumen">
      {title}
    </h4>
		<div
      className="progress xs progress-striped active bar_progress"
      _data-toggle="tooltip"
	    title=""
      data-placement="bottom"
    >
		  <div
        className="progress-bar progress-bar-success bar_procesados"
        role="progressbar"
	      data-toggle="tooltip"
        title=""
        _data-placement="bottom"
        style={{width: percentage}}
      />
		</div>
	</div>
);

export default ProgressBarComp;
