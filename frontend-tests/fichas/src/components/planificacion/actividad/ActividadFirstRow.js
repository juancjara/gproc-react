import React from 'react';

import RemoveButton from '../../common/RemoveButton';
import StatusBadges from '../../common/status/StatusBadges';
import DisplayOrArea from '../../common/form/DisplayOrArea';

import patterns from '../../../patterns';

const ActividadFirstRow = ({
  miactividad,
  status,
  title,
  onChange,
  editable,
  editableBtn,
  onRemove,
  disabledBtn
}) => (
  <tr>
    <td colSpan="6">
      <div>
        {
          editable &&
            <StatusBadges status={status} />
        }
        <RemoveButton
          extraClass={'pull-right'}
          size={'sm'}
          onClick={onRemove}
          editable={editableBtn}
          title={'Remover actividad'}
          disabled={disabledBtn}
        />
      </div>
      <div className="strong">{title}</div>
      <DisplayOrArea
        rows="5"
        classWrapper=""
        value={miactividad}
        name={'miactividad'}
        pattern={patterns.all}
        onChange={onChange}
        editable={editable}
        required
      />
    </td>
  </tr>
);

export default ActividadFirstRow;
