import React from 'react';

const OptionsEstandares = ({
  groupTitle,
  estandares,
}) => (
    <optgroup label={groupTitle}>
    {
      estandares.map(estandar => (
        <option
          key={estandar.get('esta_id')}
          value={estandar.get('esta_id')}
        >
          {`(${estandar.get('count')}) ${estandar.get('display')}`}
        </option>
      ))
    }
  </optgroup>
);

export default OptionsEstandares;
