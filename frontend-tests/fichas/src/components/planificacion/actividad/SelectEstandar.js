import React, { Component } from 'react';
import connectToValidate from '../../../containers/common/connectToValidate';

import OptionsEstandares from './OptionsEstandares';

class SelectEstandar extends Component{
  constructor(props) {
    super(props);
    this.setRef = this.setRef.bind(this);
    this.onChange = this.onChange.bind(this);
    this.state = {
      msg: "",
    };
  }

  setRef(e) {
    this.el = e;
    this.$el = $(this.el);
  }

  componentDidMount() {
    let msg = '';
    const target = this.el;
    if (!target.value && target.value === "") {
      msg = 'error';
    }
    this.setState({ msg });
    this.$el.tooltip({
      'title': this.$el.data('error-msg'),
      'trigger': 'manual'
    });
  }

  onChange(e) {
    let msg = '';
    const $el = this.$el;

    if (!e.target.value && e.target.value === "") {
      msg = 'error';
    }
    if (msg.length) {
      $el.tooltip('show');
			setTimeout(() => $el.tooltip('hide'), 2000);
    }

    this.setState({ msg });

    this.props.onChange(e);
  }

  render() {
    const {
      selected,
      estandaresGroup,
      estandaresFactor,
      fact_codigo,
    } = this.props;

    return(
      <div className={this.state.msg ? 'error_cell' : ''}>
        <select
         className="form-control"
         value={selected}
         onChange={this.onChange}
         required
         ref={this.setRef}
         data-error-msg="Debe seleccionar un estándar"
        >
        <option value=''>[Seleccione estandar]</option>
        <optgroup
          label={`Estándares del factor ${fact_codigo}`}
        />
        {
          estandaresGroup.map(({ key, display }, i) =>
            estandaresFactor.has(key) &&
              (
                <OptionsEstandares key={key}
                  groupTitle={display}
                  estandares={estandaresFactor.get(key)}
                />
              )
          )
        }
        </select>
      </div>
    );
  }
}
export default SelectEstandar;
