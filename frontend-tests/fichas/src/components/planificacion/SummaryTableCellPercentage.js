import React from 'react';

const SummaryTableCellPercentage = ({
  perc
}) => (
  <small className="text-primary">
    {`(${perc}%)`}
  </small>
);

export default SummaryTableCellPercentage;
