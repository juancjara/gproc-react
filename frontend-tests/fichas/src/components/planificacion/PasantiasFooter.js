import React from 'react';

import SummaryTableCellPercentage from './SummaryTableCellPercentage';

const PasantiasFooter = ({
  totalPasantias,
  percPasantias,
}) => (
  <tfoot>
    <tr>
      <td colSpan="20" className="text-right">
      {`Total pasantías FEC: ${totalPasantias}`}
        <SummaryTableCellPercentage
          perc={percPasantias}
        />
      </td>
    </tr>
  </tfoot>

);

export default PasantiasFooter;
