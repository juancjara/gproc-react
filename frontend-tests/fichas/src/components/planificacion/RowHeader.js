import React from 'react';

import DisplayOrArea from '../common/form/DisplayOrArea';
import patterns from '../../patterns';

const RowHeader = ({
  title,
  cod,
  onChange,
  value,
  name,
  editable,
  hideEdit,
}) => (
  <div>
    <small>{title}</small>
    <br />
    {
      !hideEdit && (
        <div>
          <label>{cod}</label>
          <DisplayOrArea
            classWrapper=""
            value={value}
            name={name}
            onChange={onChange}
            pattern={patterns.all}
            editable={editable}
            rows="15"
            required
          />
        </div>
      )
    }
  </div>
);

export default RowHeader;
