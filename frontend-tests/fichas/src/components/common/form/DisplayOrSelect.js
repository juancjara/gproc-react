import React from 'react';

import DisplayField from './DisplayField';
import SelectField from './SelectField';

const DisplayOrSelect = ({
  editable,
  value,
  onChange,
  name,
  options,
  defaultOption,
  ...props,
}) => (
  editable ?
    <SelectField
      {...props}
      defaultOption={defaultOption}
      value={value}
      name={name}
      options={options}
      onChange={onChange}
      instantUpdate
    /> :
    <DisplayField value={value} />
);

export default DisplayOrSelect;
