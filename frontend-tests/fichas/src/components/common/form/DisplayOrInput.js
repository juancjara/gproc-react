import React from 'react';

import DisplayField from './DisplayField';
import InputField from './InputField';

const DisplayOrInput = ({
  editable,
  value,
  onChange,
  name,
  classWrapper,
  ...props,
}) => (
  editable ?
    <InputField
      value={value}
      name={name}
      onChange={onChange}
      classWrapper={classWrapper}
      {...props}
    /> :
    <DisplayField
      value={value}
      classWrapper={classWrapper}
    />
);

export default DisplayOrInput;
