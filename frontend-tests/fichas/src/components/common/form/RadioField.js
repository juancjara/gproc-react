import React from 'react';
import _ from 'lodash';

import DisplayField from './DisplayField';

const RadioField = ({
  name,
  label,
  options,
  selected,
  onChange,
  editable,
}) => {
  let comp;

  if (editable) {
    comp = _.map(options, ({ id, text }) => (
      <div className="radio" key={id}>
        <label>
          <input
            value={id}
            name={name}
            type="radio"
            checked={selected === id}
            onChange={onChange}
          />
          {text}
        </label>
      </div>
    ));
  } else {
    const filtered = _.filter(options, ({ id }) => id === selected).pop();
    const value = filtered && filtered.text;
    comp = <DisplayField value={value} />;
  }

  return (
    <div className="form-group">
      <label className="col-xs-3 col-sm-2 control-label">
        {label}
      </label>
      <div className="col-xs-9">
        {comp}
      </div>
    </div>
  );
};

export default RadioField;
