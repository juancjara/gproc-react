import React, { PropTypes } from 'react';

import DisplayField from './DisplayField';

const propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};

const StaticField = ({
  label,
  value,
  classLabel = "col-xs-3 col-sm-2 control-label",
  classWrapDisplay,
  smallLabel,
}) => (
  <div className="form-group">
    {
      smallLabel ?
        <small className="col-xs-12">{label}</small> :
        <label className={classLabel}>
          {label}
        </label>
    }
    <DisplayField
      classWrapper={smallLabel ? 'col-xs-12' : classWrapDisplay}
      value={value}
    />
  </div>
);

StaticField.propTypes = propTypes;

export default StaticField;
