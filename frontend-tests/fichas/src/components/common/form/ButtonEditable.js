import React, { PropTypes } from 'react';

const propTypes = {
  editable: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  className: PropTypes.string,
};

const ButtonEditable = ({
  editable,
  onClick,
  className = '',
  text,
  children,
  icon,
  ...attrs
}) => (
  editable ?
    <button onClick={onClick} className={className} {...attrs}>
      {icon && (<i className={`fa fa-${icon}`}></i>)}
      {text}
      {children}
    </button> :
    <div />
);

ButtonEditable.propTypes = propTypes;

export default ButtonEditable;
