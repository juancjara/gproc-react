import React from 'react';

import DisplayField from './DisplayField';
import TextAreaField from './TextAreaField';

const DisplayOrArea = ({
  editable,
  value,
  onChange,
  name,
  classWrapper,
  ...props,
}) => (
  editable ?
    <TextAreaField
      {...props}
      value={value}
      name={name}
      onChange={onChange}
    /> :
    <DisplayField
      classWrapper={classWrapper}
      value={value}
    />
);

export default DisplayOrArea;
