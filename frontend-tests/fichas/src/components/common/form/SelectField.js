import React from 'react';
import connectToValidate from '../../../containers/common/connectToValidate';

import _ from 'lodash';

const SelectField = ({
  onChange,
  msg = '',
  value,
  classWrapper = '',
  name,
  defaultOption="Seleccione",
  options,
  setRef,
  ...props,
}) => (
  <div
    className={`${ !value.length ? 'error_cell' : '' } ${classWrapper}`} >
    <select
      {...props}
      className="form-control"
      ref={setRef}
      name={name}
      value={value}
      onChange={onChange}
    >
    <option value="">{defaultOption}</option>
    {
      _.map(options, ({ id, name }) => (
        <option
          key={id}
          value={id}
        >
          {name}
        </option>
      ))
    }
    </select>
  </div>
);

export default SelectField;
