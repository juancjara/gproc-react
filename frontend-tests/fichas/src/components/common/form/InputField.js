import React from 'react';
import connectToValidate from '../../../containers/common/connectToValidate';

const InputField = ({
  onChange,
  msg,
  onBlur,
  errorMsg,
  setRef,
  classWrapper,
  ...props,
}) => (
  <div
    className={`${ msg.length ? 'error_cell' : '' } ${classWrapper}`}
  >
    <input
      {...props}
      ref={setRef}
      className={'form-control'}
      data-error-msg={ errorMsg || 'Campo requerido' }
      type="text"
      onChange={onChange}
      onBlur={onBlur}
    />
  </div>
);

export default connectToValidate()(InputField);
