import React from 'react';

const DisplayField = ({
  value,
  classWrapper = 'col-xs-9',
}) => (
  <div className={classWrapper}>
    <p className="form-control-static">
      {value}
    </p>
  </div>
);

export default DisplayField;
