import React from 'react';
import connectToValidate from '../../../containers/common/connectToValidate';

const TextAreaInput = ({
  onChange,
  msg,
  onBlur,
  errorMsg,
  setRef,
  pattern,
  ...props,
}) => (
  <div
    className={`${ msg.length ? 'error_cell' : '' }`}
    data-error-msg={ props.errorMsg || 'Campo requerido' }
  >
    <textarea
      {...props}
      data-pattern={pattern}
      ref={setRef}
      data-error-msg={ errorMsg || 'Campo requerido' }
      className="form-control"
      onChange={onChange}
      onBlur={onBlur}
    />
  </div>
);

export default connectToValidate()(TextAreaInput);
