import React from 'react';

import InputField from './InputField';
import DisplayField from './DisplayField';

const EditableField = ({
  label,
  onChange,
  editable,
  className = "form-group",
  classLabel = 'col-xs-3 col-sm-2',
  classWrapInput = 'col-xs-9 col-sm-3',
  smallLabel,
  ...props,
}) => (
  <div className={className}>
    {smallLabel ?
      <small className="col-xs-12">{label}</small> :
      <label className={`${classLabel} control-label`}>
        {label}
      </label>
    }
    {
      editable ?
        <div className={smallLabel ? 'col-xs-12' : classWrapInput}>
          <InputField
            {...props}
            onChange={onChange}
          />
        </div> :
        <DisplayField
          value={props.value}
          classWrapper={smallLabel ? 'col-xs-12' : undefined}
        />
    }
 </div>
);

export default EditableField;
