import React from 'react';

const RemoveButton = ({
  onClick,
  text = '',
  title = '',
  editable,
  size='xs',
  extraClass,
  ...extraAttrs,
}) => (
  editable ?
    <button
      type="button"
      className={`btn btn-flat btn-danger btn-${size} ${extraClass}`}
      {...extraAttrs}
      title={title}
      onClick={onClick}
    >
      <i className="fa fa-times"></i>
      {text}
    </button> :
    <div />
);

export default RemoveButton;
