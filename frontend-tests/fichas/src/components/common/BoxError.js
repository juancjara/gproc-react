import React from 'react';

const BoxError = ({
  errors,
  children,
  color='alert-danger'
}) => (
  errors.length ? (
    <div className={`alert ${color}`} >
      {errors.map((error, i) => (<p key={i}>{error}</p>))}
      {children}
    </div>
  ) : <div />
);

export default BoxError;
