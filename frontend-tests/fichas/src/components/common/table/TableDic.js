import React from 'react';
import TableHeader from './TableHeader';
import TableBodyDic from './TableBodyDic';

const TableDic = ({
  headers,
  rowHeaders,
  newColKey,
  body,
  headerCol,
  readOnlyCol,
  readOnlyRow,
  onChange,
  editable,
  className = '',
  pattern,
  errorMsg,
}) => (
  <table className={className}>
    <TableHeader headers={headers} />
    <TableBodyDic
      editable={editable}
      rowHeaders={rowHeaders}
      newColKey={newColKey}
      headers={headers}
      body={body}
      headerCol={headerCol}
      readOnlyCol={readOnlyCol}
      readOnlyRow={readOnlyRow}
      onChange={onChange}
      pattern={pattern}
      errorMsg={errorMsg}
    />
  </table>
);

export default TableDic;
