import React from 'react';
import _ from 'lodash';

const CustomTableHeader = ({
  customHeaders,
  extraHeaders,
  simpleHeaders,
  classSimpleThs,
}) => {
  // @TODO: refactor extraheaders
  const customThs = _.map(customHeaders, ({ display, attr }, i) => (
    <th key={i} {...attr}>{ display }</th>
  ));
  const extraThs = _.map(extraHeaders, ({ display, attr }, i) => (
    <th key={i} {...attr}>{ display }</th>
  ));

  const simpleThs = _.map(simpleHeaders, ({ display }, i) => (
    <th key={i} className={classSimpleThs}>{ display }</th>
  ));

  const ths = [
    customThs,
    simpleThs
  ];

  if (extraThs.length) {
    ths.splice(1, 0, extraThs);
  }

  return (
    <thead>
      {
        ths.map((headers, i) => (
            <tr key={i} >{headers}</tr>
        ))
      }
    </thead>
  );
};

export default CustomTableHeader;
