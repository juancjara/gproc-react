import React, { Component } from 'react';
import _ from 'lodash';
import { List } from 'immutable';

class TableFooter extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    let {
      headers,
      classTh,
    } = this.props;

    if (List.isList(headers)) { headers = headers.toJS(); }

    const ths = _.map(headers, ({ display }, i) => (
        <th key={i} className={classTh}>{display}</th>
    ));

    return (
      <tfoot>
        <tr>
        {ths}
        </tr>
      </tfoot>
    );
  }
}

export default TableFooter;
