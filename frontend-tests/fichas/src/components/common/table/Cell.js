import React, { Component, PropTypes } from 'react';
import _ from 'lodash';

import EditCell from './EditCell';
import DisplayCell from './DisplayCell';

const propTypes = {
  row: PropTypes.any.isRequired,
  col: PropTypes.any.isRequired,
  editable: PropTypes.bool.isRequired,
  value: PropTypes.any.isRequired,
  extraAttr: PropTypes.object,
};

class Cell extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(value) {
    const {
      row,
      col,
      onChange,
    } = this.props;
    onChange(row, col, value);
  }

  render() {
    const {
      editable,
      value,
      pattern,
      extraAttr = {},
      labelTh,
      errorMsg,
      extraCellComp,
    } = this.props;

    const isReadOnly = _.has(extraAttr, 'readOnly');
    const isEditable = editable && !isReadOnly;

    return isEditable ?
      <EditCell
        errorMsg={errorMsg}
        onChange={this.handleChange}
        pattern={pattern}
        value={value}
        attr={extraAttr}
      /> :
      <DisplayCell
        extraCellComp={extraCellComp}
        isReadOnly={isReadOnly}
        value={value}
        attr={extraAttr}
      />;
  }
}

Cell.propTypes = propTypes;

export default Cell;
