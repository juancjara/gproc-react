import React from 'react';
import _ from 'lodash';

import patterns from '../../../patterns';
import Cell from './Cell';
import RemoveButton from '../RemoveButton';

const Row = ({
  readOnlyCol,
  readOnlyRow,
  headerCol = [],
  headers,
  indexRow,
  onChange,
  canBeRemoved,
  onRemove,
  row,
  editable,
  pattern = patterns.validNumber,
  errorMsg,
  specificPatterns = [],
  extraCellComponents,
  className = '',
}) => {
  const tds = _.map(headers, ({ key }, i) => {
    let cellEditable = editable;
    let extraAttr = {};

    if (_.indexOf(readOnlyCol, key) > -1) {
      extraAttr = { ...extraAttr, readOnly: true };
    }
    if (_.indexOf(readOnlyRow, indexRow) > -1) {
      extraAttr = { ...extraAttr, readOnly: true };
    }
    if (_.indexOf(headerCol, key) > -1) {
      cellEditable = false;
    }

    let actualPattern = pattern;
    let actualErrorMsg = errorMsg;

    const foundPattern = specificPatterns
      .filter(sp => sp.key === key);

    if (foundPattern.length > 0) {
      actualPattern = foundPattern[0].pattern;
      actualErrorMsg = foundPattern[0].errorMsg;
    }
    extraAttr.pattern = actualPattern;

    let extraCellComp;
    if (extraCellComponents) {
      extraCellComp = extraCellComponents.get(key);
    }

    return (
      <Cell
        key={i}
        pattern={actualPattern}
        editable={cellEditable}
        value={row.get(key)}
        extraAttr={extraAttr}
        row={indexRow}
        col={key}
        onChange={onChange}
        errorMsg={actualErrorMsg}
        extraCellComp={extraCellComp}
      />
    );
  });

  let removeBtn;
  if (editable && canBeRemoved) {
    removeBtn = (
      <td className="td-actions">
        <RemoveButton
          editable
          onClick={() => onRemove(indexRow)}
        />
      </td>
    );
  }

  return (
    <tr className={className}>
      {tds}
      {removeBtn}
    </tr>
  );
};

export default Row;
