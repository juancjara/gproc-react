import React, { Component } from 'react';
import _ from 'lodash';
import { List } from 'immutable';

class TableHeader extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    let {
      headers,
      classTh,
    } = this.props;

    if (List.isList(headers)) { headers = headers.toJS(); }

    const ths = _.map(headers, ({ display, customClass = '', style }, i) => (
        <th key={i}
          className={`${classTh} ${customClass}`}
          style={style}
        >
          {display}
        </th>
    ));

    return (
      <thead>
        <tr>
          {ths}
        </tr>
     </thead>
    );
  }
}

export default TableHeader;
