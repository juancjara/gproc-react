import React, { Component } from 'react';
import TableHeader from './TableHeader';
import TableBody from './TableBody';

class Table extends Component {
  componentDidMount() {
  }
  render () {
    const {
      body,
      headers,
      onChange,
      readOnlyCol = [],
      readOnlyRow = [],
      canBeRemoved = false,
      onRemove,
      editable,
      className = '',
      pattern,
      errorMsg,
      specificPatterns,
    } = this.props;

    return (
      <table className={className}>
        <TableHeader headers={headers} />
        {<TableBody
          editable={editable}
          body={body}
          headers={headers}
          onChange={onChange}
          readOnlyCol={readOnlyCol}
          readOnlyRow={readOnlyRow}
          canBeRemoved={canBeRemoved}
          onRemove={onRemove}
          pattern={pattern}
          errorMsg={errorMsg}
          specificPatterns={specificPatterns}
        />}
      </table>
    );
  }
};

export default Table;
