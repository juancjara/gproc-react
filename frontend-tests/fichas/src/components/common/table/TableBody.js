import React from 'react';
import Row from './Row';

const TableBody = ({
  body,
  headers,
  onChange,
  readOnlyCol = [],
  readOnlyRow = [],
  canBeRemoved,
  onRemove,
  editable,
  pattern,
  errorMsg,
  specificPatterns,
}) => {
  const rows = body.map((row, i) => {
    const id = row.has('id') ? row.get('id') : i;
    return (
      <Row
        editable={editable}
        key={id}
        indexRow={id}
        row={row}
        headers={headers}
        onChange={onChange}
        readOnlyCol={readOnlyCol}
        readOnlyRow={readOnlyRow}
        canBeRemoved={canBeRemoved}
        onRemove={onRemove}
        pattern={pattern}
        errorMsg={errorMsg}
        specificPatterns={specificPatterns}
      />
    );
  });

  return (
    <tbody>
      {rows}
    </tbody>
  );
};

export default TableBody;
