import React from 'react';

const DisplayCell = ({
  attr,
  value,
  isReadOnly,
  extraCellComp,
  className=""
}) => (
  isReadOnly ?
    <th className={className}>
      <div
        {...attr}
      >
        {value}
      </div>
    {extraCellComp}
    </th> :
    <td>
      <div
        {...attr}
      >
        {value}
      </div>

    {extraCellComp}
    </td>
);

export default DisplayCell;
