import React, { Component, PropTypes } from 'react';
import { validateField } from '../../../utils';
import connectToValidate from '../../../containers/common/connectToValidate';

const propTypes = {
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
  attr: PropTypes.object,
  errorMsg: PropTypes.string,
};

const EditCell = ({
  onChange,
  msg,
  onBlur,
  errorMsg,
  setRef,
  value,
  ...props,
}) => (
 <td className={msg ? 'error_cell' : ''}>
   <input
     {...props}
     value={value}
     onChange={onChange}
     onBlur={onBlur}
     ref={setRef}
     data-error-msg={ errorMsg || 'Campo requerido' }
     className="form-control"
     type="text"
     required
   />
 </td>
);

EditCell.propTypes = propTypes;

export default  connectToValidate()(EditCell);
