import React from 'react';
import _ from 'lodash';
import {
  fromJS,
  Map,
} from 'immutable';

import Row from './Row';

const TableBodyDic = ({
  headers,
  rowHeaders,
  newColKey,
  body,
  readOnlyCol = [],
  readOnlyRow = [],
  headerCol = [],
  onChange,
  editable,
  pattern,
  errorMsg,
  extraCellComponents = Map(),
}) => {
  const rowKeys = _(rowHeaders).map(_.keysIn).flatten().value();
  const tableMutable = {};
  let table = Map();
  let trs;

  if (body.size) {
    _.forEach(rowKeys, (key, i) => {
      tableMutable[key] =
        body.get(key).merge(Map({ [newColKey]: rowHeaders[i][key] }));
    });

    table = fromJS(tableMutable);
    trs = _.map(rowKeys, (k, i) => (
      <Row
        editable={editable}
        extraCellComponents={extraCellComponents.get(k)}
        row={table.get(k)}
        indexRow={k}
        key={i}
        headers={headers}
        headerCol={headerCol}
        readOnlyCol={readOnlyCol}
        readOnlyRow={readOnlyRow}
        onChange = {onChange}
        pattern={pattern}
        errorMsg={errorMsg}
      />
    ));
  }

  return (
    <tbody>{trs}</tbody>
  );
};

export default TableBodyDic;
