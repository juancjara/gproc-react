import React, { PropTypes } from 'react';

const propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  number: PropTypes.number,
};

const BoxHeader = ({
  title,
  subtitle,
  number = -1,
  children,
}) => (
  <div className="box-header">
    <h4 className="box-title">
      {number > -1 &&
        <span className="number">{number}.</span>
      }
      {title}
      <small className="text-red">{' '}{subtitle}</small>
    </h4>
    {children}
  </div>
);

BoxHeader.propTypes = propTypes;

export default BoxHeader;
