import React from 'react';

const NodoButton = ({
  text,
  type = 'success',
  icon,
  onClick,
  disabled,
}) => (
  <button
    type="button"
    className={`btn btn-flat btn-${type}`}
    onClick={onClick}
    disabled={disabled}
  >
    <i className={`fa fa-${icon}`}></i>
    {text}
  </button>
);

export default NodoButton;
