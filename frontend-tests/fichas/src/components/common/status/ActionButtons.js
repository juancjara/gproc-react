import React from 'react';

import NodoButton from './NodoButton';

const ActionButtons = ({
  status,
  onSave,
}) => (
  <div>
    <NodoButton
      text={'Guardar sección'}
      type={'success'}
      disabled={status === 'FETCHING'}
      onClick={onSave}
    />
  </div>
);

export default ActionButtons;
