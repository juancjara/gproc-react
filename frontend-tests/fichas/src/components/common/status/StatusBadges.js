import React from 'react';

import statusConstants from '../../../constants/status';
const {
  STORED_OK,
  STORED_CHANGED,
  FETCHING,
  SERVER_ERROR,
} = statusConstants;


const StatusBadges = ({
  status,
}) => {
  let attr = {
    background: null,
    icon: null,
    title: null,
  };

  switch (status) {
    case STORED_OK:
      attr = {
        background: 'green',
        icon: 'fa-save',
        title: 'Guardado',
      };
    break;

    case STORED_CHANGED:
      attr = {
        background: 'yellow',
        title: 'Pendiente',
        icon: 'fa-exclamation',
      };
      break;

    case FETCHING:
      attr = {
        background: 'yellow',
        title: 'Guardando...',
        icon: 'fa-spin fa-spinner',
      };
      break;

    case SERVER_ERROR:
      attr = {
        background: 'red',
        title: 'Error al guardar',
        icon: 'fa-exclamation-triangle',
      };
  }

  return (
    <label className={`badge bg-${attr.background}`}>
      <i title={attr.title} className={`fa ${attr.icon}`}></i>
    </label>
  );
};

export default StatusBadges;
