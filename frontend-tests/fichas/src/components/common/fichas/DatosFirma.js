import React, { Component } from 'react';
import _ from 'lodash';

import EditableField from '../form/EditableField';
import BoxHeader from '../BoxHeader';

const fields = [
  {
    key: 'nombre',
    label: 'Apellidos y nombres *',
  },
  {
    key: 'cargo',
    label: 'Cargo *',
  },
];

class DatosFirma extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    this.props.onChange(this.props.firmaType, e);
  }

  render() {
    const {
      firmaType,
      title,
      data,
      editable,
    } = this.props;

    return (
      <div className="box">
        <BoxHeader title={title} />
        <div className="box-body">
        {
          _.map(fields, ({ key, label }) => (
            <EditableField
              editable={editable}
              label={label}
              key={key}
              name={key}
              value={data.get(key)}
              onChange={this.onChange}
              required
            />
          ))
        }
          <div className="form-group">
            <label className="col-xs-3 col-sm-2 control-label">
              Fecha y firma
            </label>
            <div className="col-xs-4 col-sm-3">
              <br/>
              <br/>
              <br/>
              __________________________
            </div>
            <div className="col-xs-5 col-sm-3">
              <br/>
              <br/>
              <br/>
              <p className="form-control-static text-right">
                {data.get('date_local')}
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default DatosFirma;
