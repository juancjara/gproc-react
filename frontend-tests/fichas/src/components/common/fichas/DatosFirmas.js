import React, { Component } from 'react';
import _ from 'lodash';

import StatusBadges from '../../common/status/StatusBadges';
import SectionTitle from '../../expresionInteres/common/SectionTitle';
import EditableField from '../../common/form/EditableField';
import DatosFirma from './DatosFirma';

const types = [
  {
    firmaType: 'com',
    title: 'Nombre y firma del Presidente del Comité de Calidad'
  },
  {
    firmaType: 'dir',
    title: 'Nombre y firma del Director de la Escuela Universitaria o Director General del Instituto'
  },
];

class DatosFirmas extends Component {
  render() {
    const {
      data,
      onChange,
      editable,
      status,
    } = this.props;

    return (
      <div className="firmas">
        <SectionTitle
          title={'Datos del responsable'}
        >
        {
          editable &&
            <StatusBadges status={status} />
        }
        </SectionTitle>
        {
          _.map(types, ({ firmaType, title }) => (
            <DatosFirma
              key={firmaType}
              title={title}
              firmaType={firmaType}
              data={data.get(firmaType)}
              onChange={onChange}
              editable={editable}
            />
          ))
        }
      </div>
    );
  }
}

export default DatosFirmas;
