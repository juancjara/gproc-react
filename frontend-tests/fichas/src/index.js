//import 'babel-polyfill';

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import { fromJS } from 'immutable';

//console.log(Array.from);

import rootReducer from './reducers/expresionInteres';
import App from './containers/expresionInteres/App';
import { setContext } from './context';
import configureStore from './store/expresionInteres';

import getInitialData from './initialData/expresionInteres';

/*
const data = JSON.parse(document.getElementById('inputData').value) || undefined;
const context = JSON.parse(document.getElementById('inputContext').value);
setContext(context);
//*/

/*
import data from '../tests/data.v2';
const context = {"editable":true, "carr_nombre": "EDUCACION PRIMARIA","aTipoFinanciamiento":[{"id":"2","text":"Plan de Mejora de Carreras"},{"id":"4","text":"Evaluaci\u00f3n Externa de Carrera"}],"institucional":false,"universidad":false,"aPersona":[{"pers_id":"911","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Wildo","pers_apellidos":"Condor\u00ed","pers_cargo":"Presidente","pers_telefono":"996500961","pers_correo":"widowilliam@hotmail.com","pers_tipo":"contacto","acre_id":"220"},{"pers_id":"912","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Nilton","pers_apellidos":"Mayta","pers_cargo":"Secretario","pers_telefono":"951771202","pers_correo":"nicemaja78@hotmail.com","pers_tipo":"comision","acre_id":"220"},{"pers_id":"913","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Ofelia","pers_apellidos":"Mamani","pers_cargo":"Docente","pers_telefono":"951565014","pers_correo":"sumayaofely@hotmail.com","pers_tipo":"comision","acre_id":"220"}],"moda_nombre":"En proceso de acreditaci\u00f3n","inst_nombre":"UNIVERSIDAD NACIONAL DEL ALTIPLANO","inst_gestion":"PUBLICA", "aPersona":[{"pers_id":"911","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Wildo","pers_apellidos":"Condor\u00ed","pers_cargo":"Presidente","pers_telefono":"996500961","pers_correo":"widowilliam@hotmail.com","pers_tipo":"contacto","acre_id":"220"},{"pers_id":"912","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Nilton","pers_apellidos":"Mayta","pers_cargo":"Secretario","pers_telefono":"951771202","pers_correo":"nicemaja78@hotmail.com","pers_tipo":"comision","acre_id":"220"},{"pers_id":"913","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Ofelia","pers_apellidos":"Mamani","pers_cargo":"Docente","pers_telefono":"951565014","pers_correo":"sumayaofely@hotmail.com","pers_tipo":"comision","acre_id":"220"}]};
setContext(context);
//*/

const context = window.__CONTEXT__;
const serverRender = window.__SERVER_RENDER__;
let initialState = fromJS(window.__INITIAL_STATE__);

if ( ! serverRender) {
  initialState = getInitialData(initialState, context);
}

setContext(context);
const store = configureStore(initialState);

ReactDOM.render(
  <Provider store={store} >
    <App />
  </Provider>,
  document.getElementById('root')
);


/*const data = {
  id: 123,
  table: [
    {foo: '1', bar: '10'},
    {foo: '2', bar: '20'},
  ]
};

const Table = (
  { table }
) => {console.log('render'); return (
  <table>
    <tbody>
      {table.map((r, i) => (
        <tr key={i}>
          <td>{r.foo}</td>
          <td>{r.bar}</td>
        </tr>
      ))}
    </tbody>
  </table>
);};

class App extends Component {
  constructor (props) {
    super(props);
    this.state = data;
  }

  render () {
    return (
      <div>
        <p>{this.state.id}</p>
        <Table table={this.state.table} />
      </div>
    );
  }

}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
*/
