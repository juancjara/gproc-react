import actions from '../../constants/planificacion';
const {
  FICHA_FIRMA_UPDATE,
} = actions;

export const updateFirma = (
  firmaType,
  prop,
  value
) => ({
  type: FICHA_FIRMA_UPDATE,
  firmaType,
  prop,
  value,
});
