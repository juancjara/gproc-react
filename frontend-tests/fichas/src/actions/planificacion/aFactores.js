import actions from '../../constants/planificacion';
const {
  FACTOR_UPDATE,
  FACTOR_TOGGLE_HIDE,
} = actions;

export const updateFactor = (
  index,
  prop,
  value
) => ({
  type: FACTOR_UPDATE,
  index,
  prop,
  value,
});

export const toggleHideFactor = (
  index
) => ({
  type: FACTOR_TOGGLE_HIDE,
  index,
});
