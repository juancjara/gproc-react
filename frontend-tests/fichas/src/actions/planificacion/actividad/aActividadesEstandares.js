import uuid from 'node-uuid';

import actions from '../../../constants/planificacion';
const {
  ACTIVIDAD_ESTANDAR_ADD,
  ACTIVIDAD_ESTANDAR_REMOVE,
  ACTIVIDAD_ESTANDAR_UPDATE,
} = actions;

import {
  estandarDecrement,
  estandarIncrement,
} from '../aEstandares';

export const addEstandar = (
  actividadIndex
) => ({
  type: ACTIVIDAD_ESTANDAR_ADD,
  actividadIndex,
  id: uuid.v4(),
});

export const removeEstandar = (
  actividadIndex,
  estandarIndex
) => (dispatch, getState) => {
  const lastEstandar = getState()
    .getIn(['act', actividadIndex, 'data', 'estandares',
            estandarIndex, 'esta_id']);

  dispatch(estandarDecrement(lastEstandar));
  dispatch(_removeEstandar(actividadIndex, estandarIndex));
};

export const _removeEstandar = (
  actividadIndex,
  estandarIndex
) => ({
  type: ACTIVIDAD_ESTANDAR_REMOVE,
  actividadIndex,
  estandarIndex,
});

export const _selectEstandar = (
  actividadIndex,
  estandarIndex,
  estandar
) => ({
  type: ACTIVIDAD_ESTANDAR_UPDATE,
  actividadIndex,
  estandarIndex,
  estandar,
});

export const selectEstandar = (
  actividadIndex,
  estandarIndex,
  estandar
) => (dispatch, getState) => {
  const lastEstandar = getState()
    .getIn(['act', actividadIndex, 'data', 'estandares',
            estandarIndex, 'esta_id']);

  dispatch(estandarDecrement(lastEstandar));
  dispatch(estandarIncrement(estandar));
  dispatch(_selectEstandar(actividadIndex, estandarIndex, estandar));
};
