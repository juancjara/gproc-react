import actions from '../../../constants/planificacion';
const {
  ACTIVIDAD_UPDATE,
} = actions;

export const updateActividad = (
  actividadIndex,
  prop,
  value
) => ({
  type: ACTIVIDAD_UPDATE,
  actividadIndex,
  prop,
  value,
});
