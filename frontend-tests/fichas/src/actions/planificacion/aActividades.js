import uuid from 'node-uuid';
import _ from 'lodash';

import * as messages from '../../messages/planificacion';

import constantsPlanificacion from '../../constants/planificacion';
const {
  ACTIVIDAD_ADD,
  ACTIVIDAD_REMOVE,

  REQUEST_ADD_ACTIVIDAD,
  REQUEST_REMOVE_ACTIVIDAD,
} = constantsPlanificacion;

import constantsStatus from '../../constants/status';
const {
  UNAUTHORIZED_ERROR,
} = constantsStatus;

import { findIndexBy } from '../../utils';

import actividades from '../../reducers/planificacion/actividades';

import {
  APIaddActividad,
  APIremoveActividad,
} from '../../webAPI/planificacion';

import {
  nodoServerError,
  nodoFetching,
  nodoSuccess,

  updateRequestType,
} from '../aNodoStatus';

import { estandarDecrement } from './aEstandares';

// @TODO remove id, dbId is enough
export const _addActividad = (
  index,
  factorDbId,
  fact_codigo
) => ({
  type: ACTIVIDAD_ADD,
  index,
  id: uuid.v4(),
  factorDbId,
  fact_codigo,
});

const onSuccessAddActividad = ({
  dispatch,
  actionAddActividad,
  path,
  index,
}, { ids }) => {
  console.log('dbIds', ids, index);
  dispatch(nodoSuccess(path));

  actionAddActividad.dbId = ids[index].dbId;
  dispatch(actionAddActividad);
};

const onErrorAddActividad = ({
  path,
  dispatch,
}, { code, message }) => {
  if (code === 401) {
    dispatch(updateRequestType(UNAUTHORIZED_ERROR));
  }
  dispatch(nodoServerError(path, message));
};

export const addActividad = (
  index,
  factorDbId
) => (dispatch, getState) => {

  console.log('act', index);
  let dataActividadesOptimistic = getState()
    .get('act');

  const factorIndex = findIndexBy(
    getState().get('fac'),
    factorDbId,
    (factor) => factor.get('dbId')
  );

  const fact_codigo = getState().getIn(['fac', factorIndex, 'data', 'fact_codigo']);
  const actionAddActividad = _addActividad(index, factorDbId, fact_codigo);
  const path = ['act', index];

  dispatch(updateRequestType(REQUEST_ADD_ACTIVIDAD));

  dataActividadesOptimistic = actividades(
    dataActividadesOptimistic,
    actionAddActividad
  );
  const payload = dataActividadesOptimistic

    .filter(a => a.get('parent_id') === factorDbId)
    .toJS()
    .map((act, i) => ({
      orden: i,
      path: ['act'],
      ..._.pick(act, ['data', 'parent_id', 'dbId']),
    }));

  payload.push({
    path: ['fac', fact_codigo],
    ..._.pick(getState().getIn(['fac', factorIndex]).toJS(), ['data', 'parent_id', 'dbId']),
  });

  console.log('payload', payload);
  const indexNewCar = payload
          .map((act, i) => ({
            dbId: act.dbId,
            index: i
          }))
          .filter(act => act.dbId === null)[0].index;
  console.log(indexNewCar);

  const dumbPath = ['fac', factorIndex];
  dispatch(nodoFetching(dumbPath));

  APIaddActividad(
    payload,
    _.partial(
      onSuccessAddActividad,
      { dispatch, actionAddActividad, path: dumbPath, index: indexNewCar },
      _
    ),
    _.partial(onErrorAddActividad, { dispatch, path: dumbPath }, _)
  );

};

export const _removeActividad = (
  index
) => ({
  type: ACTIVIDAD_REMOVE,
  index,
});

const onSuccessRemoveActividad = ({
  dispatch,
  index,
  getState,
  path,
}, data) => {
  console.log('remove here');
  dispatch(nodoSuccess(path));
  const estandares = getState()
    .getIn(['act', index, 'data', 'estandares'])
    .map(e => e.get('esta_id'));

  estandares
    .forEach(estandar => dispatch(estandarDecrement(estandar)));

  dispatch(_removeActividad(index));
};

const onErrorRemoveActividad = ({
  dispatch,
  paths,
}, { code, message }) => {
  if (code === 401) {
    dispatch(updateRequestType(UNAUTHORIZED_ERROR));
  }
  paths
    .forEach(path => dispatch(nodoServerError(path, message)));
}

export const removeActividad = (
  index
) => (dispatch, getState) => {

  if (!confirm(messages.CONFIRM_REMOVE_ACTIVIDAD)) {
    return;
  }
  const path = ['act', index];

  const factorDbId = getState().getIn([...path, 'parent_id']);
  const factorIndex = findIndexBy(
    getState().get('fac'),
    factorDbId,
    (factor) => factor.get('dbId')
  );
  const dumbPath = ['fac', factorIndex];

  dispatch(updateRequestType(REQUEST_REMOVE_ACTIVIDAD));

  dispatch(nodoFetching(path));
  dispatch(nodoFetching(dumbPath));

  const payload = {
    dbId: getState().getIn([...path, 'dbId']),
    factor: {
      path: ['fac', getState().getIn(['fac', factorIndex, 'data', 'fact_codigo'])],
      ..._.pick(getState().getIn(['fac', factorIndex]).toJS(), ['data', 'parent_id', 'dbId']),
    }
  };
  console.log('remove', payload);

  APIremoveActividad(
    payload,
    _.partial(
      onSuccessRemoveActividad,
      { dispatch, path: dumbPath, index, getState },
      _
    ),
    _.partial(
      onErrorRemoveActividad,
      { dispatch, paths: [dumbPath, path] },
      _
    )
  );
};
