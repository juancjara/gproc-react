import { fromJS } from 'immutable';

import {
  estandaresValidation,
} from '../../validations/planificacion';

import {
  validateInlineErrors,
} from '../../utils';

import {
  APIstoreNodo,
  APIfinish,
} from '../../webAPI/planificacion';

import {
  storeNodos,
  notValidated,

  updateRequestType,
} from '../../actions/aNodoStatus';

import {
  getContext,
} from '../../context';

import { APIstoreNodos } from '../../webAPI/planificacion';

import constants from '../../constants/status';
const {
  STORED_OK,

} = constants;

import constantsPlanificacion from '../../constants/planificacion';
const {
  REQUEST_SAVE_ALL,
} = constantsPlanificacion;

import {
  dimensionesValidation,
  factoresValidation,
  actividadesValidation,
} from '../../validations/planificacion';

const keyPaths = ['dim', 'fac', 'act'];

const getPaths = (state) =>
  [
    ['fic'],
    ..._(keyPaths)
      .map(k => state.get(k).map((_, i) => [k, i]).toJS())
      .flatten()
      .value()
  ];

export const displayErrors = ({
  dispatch,
  paths,
  errors,
}) =>
  errors.forEach((error, i) =>
    dispatch(notValidated(paths[i], error)));

const hasErrorAndDisplay = ({
  dispatch,
  state,
}) => {
  const paths = getPaths(state);

  let validInlineFichas = paths
    .map(p =>validateInlineErrors(document.getElementById(p.join(''))));

  const errorMsg = 'Existen campos que no cumplen con los valores permitidos';
  const inlineErrors = validInlineFichas
    .map(isValid => {
      if (!isValid) {
        return [errorMsg];
      }
      return [];
    });

  const validationErrors = [
    [],
    ...dimensionesValidation(state),
    ...factoresValidation(state),
    ...actividadesValidation(state),
  ];

  const mixedErrors = inlineErrors
   .map((errors, i) => errors.concat(validationErrors[i]));

  displayErrors({ dispatch, paths, errors: mixedErrors });

  const cannotFinish = _.filter(mixedErrors, errors => errors.length).length;
  return cannotFinish;
};

export const finishFicha = (status) =>
  (dispatch, getState) => {

    const cannotFinish = hasErrorAndDisplay({
      dispatch,
      state: getState()
    });

    if (status !== STORED_OK) {
      return saveAllNodes({
        dispatch,
        state: getState(),
      });
    }

    if (cannotFinish) {
      return;
    }
    
    APIfinish();
  };


const saveAllNodes = ({
  dispatch,
  state,
}) => {
  const paths = getPaths(state);
  const pathsToSave = paths
    .filter(path =>
      state.getIn([...path, 'status', 'status']) !== 'FETCHING' &&
      state.getIn([...path, 'status', 'hasChanged'])
    );

  if (!pathsToSave.length) return;

  // @TODO REFACTOR
  const extraPaths = pathsToSave
    .map(path => {
      const elem = state.getIn([...path, 'data']);
      if (path[0] === 'dim') {
        return ['dim', elem.get('dime_codigo')];
      }
      if (path[0] === 'fac') {
        return ['fac', elem.get('fact_codigo')];
      }
      return path;
    });

  dispatch(updateRequestType(REQUEST_SAVE_ALL));
  dispatch(storeNodos({ paths: pathsToSave, extraPaths,
                        webAPI: APIstoreNodos }));
};

export const showErrorsAndSaveAllNodes = () =>
  (dispatch, getState) => {
    hasErrorAndDisplay({
      dispatch,
      state: getState()
    });

    saveAllNodes({
      dispatch,
      state: getState(),
    });
  };
