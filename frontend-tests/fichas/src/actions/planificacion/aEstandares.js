import actions from '../../constants/planificacion';
const {
  ESTANDAR_DECREMENT,
  ESTANDAR_INCREMENT,
} = actions;

export const estandarDecrement = (
  estandar
) => ({
  type: ESTANDAR_DECREMENT,
  estandar,
});

export const estandarIncrement = (
  estandar
) => ({
  type: ESTANDAR_INCREMENT,
  estandar,
});
