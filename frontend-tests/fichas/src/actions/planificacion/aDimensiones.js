import actions from '../../constants/planificacion';
const {
  DIMENSION_UPDATE,
} = actions;

export const updateDimension = (
  index,
  prop,
  value
) => ({
  type: DIMENSION_UPDATE,
  index,
  prop,
  value,
});
