import actions from '../../constants/cronograma';
const {
  CRONOGRAMA_UPDATE,
} = actions;

export const updateCronograma = (
  cronogramaIndex,
  prop,
  value
) => ({
  type: CRONOGRAMA_UPDATE,
  cronogramaIndex,
  prop,
  value
});
