import { fromJS } from 'immutable';

import constantsPlanificacion from '../../constants/planificacion';
const {
  REQUEST_SAVE_ALL,
} = constantsPlanificacion;

import {
  displayErrors,
} from '../planificacion/aSystem';

import {
  toNumber,
  validateInlineErrors,
} from '../../utils';

import {
  APIstoreNodos,
  APIfinish,
} from '../../webAPI/presupuesto';

import constants from '../../constants/status';
const {
  STORED_OK,
} = constants;

import {
  getOtrosAmount,
} from '../../containers/presupuesto/DetalleTable';

import {
  actividadesValidation,
  subactividadesValidation,
  detalleTableValidation,
} from '../../validations/presupuesto';

import {
  storeNodos,
  updateRequestType
} from '../../actions/aNodoStatus';

const keyPaths = ['act', 'sub'];

const getPaths = (state, keyPaths) =>
  [
    ['fic'],
    ['totales'],
    ..._(keyPaths)
      .map(k => state.get(k).map((_, i) => [k, i]).toJS())
      .flatten()
      .value()
  ];

const hasErrorAndDisplay= ({
  dispatch,
  state,
}) => {
  const paths = getPaths(state, keyPaths);

  //no inline erros on actividades, cause no rootid
  let validInlineFichas = paths
        .map(p =>validateInlineErrors(document.getElementById(p.join(''))));

  const errorMsg = 'Existen campos que no cumplen con los valores permitidos';
  const inlineErrors = validInlineFichas
    .map(isValid => {
      if (!isValid) {
        return [errorMsg];
      }
      return [];
    });

  const validationErrors = [
    [],
    detalleTableValidation(state),
    ...actividadesValidation(state),
    ...subactividadesValidation(state),
  ];

  const mixedErrors = inlineErrors
    .map((errors, i) => errors.concat(validationErrors[i]));
  console.log(mixedErrors);

  displayErrors({ dispatch, paths, errors: mixedErrors });

  const cannotFinish = _.filter(mixedErrors, errors => errors.length).length;
  return cannotFinish;

};

export const finishFicha = status =>
  (dispatch, getState) => {
    const cannotFinish = hasErrorAndDisplay({
      dispatch,
      state: getState()
    });

    if (status !== STORED_OK) {
      return saveAllNodes({
        dispatch,
        state: getState(),
      });
    }

    if (cannotFinish) {
      return;
    }

    const otrosAmount = getOtrosAmount(getState());
    const otrosWarning = `El monto que tiene acumulado en otros FEC S/. ${otrosAmount}
deberá ser sustentado ante un comité para poder aprobar el plan de mejora`;

    if (toNumber(otrosAmount) === 0 ||
        (otrosAmount > 0 && confirm(otrosWarning))) {
      console.log('to finish');
      APIfinish();
    }
  };

export const shouldSaveNode = (
  path,
  state
) => state.getIn([...path, 'status', 'status']) !== 'FETCHING' &&
  state.getIn([...path, 'status', 'hasChanged']);

const saveAllNodes = ({
  dispatch,
  state,
}) => {
  const paths = getPaths(state, ['sub']);
  const pathsToSave = paths
    .filter(path =>
      state.getIn([...path, 'status', 'status']) !== 'FETCHING' &&
        state.getIn([...path, 'status', 'hasChanged'])
    );

  if (!pathsToSave.length) return;

  dispatch(updateRequestType(REQUEST_SAVE_ALL));
  dispatch(storeNodos({ paths: pathsToSave, webAPI: APIstoreNodos }));
};

export const showErrorsAndSaveAllNodes = () =>
  (dispatch, getState) => {
    hasErrorAndDisplay({
      dispatch,
      state: getState()
    });

    saveAllNodes({
      dispatch,
      state: getState(),
    });
  };
