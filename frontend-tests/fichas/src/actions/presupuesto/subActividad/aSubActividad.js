import { toNumber } from '../../../utils/';

import { getContext } from '../../../context';

import constantsPresupuesto from '../../../constants/presupuesto';
const {
  SUBACTIVIDAD_UPDATE,

  SUBACTIVIDAD_ADD_NEXT_SELECT,
  SUBACTIVIDAD_LAST_LEVEL_SELECTED,
  SUBACTIVIDAD_LEVEL_SELECT,
} = constantsPresupuesto;

const selectLastLevel = (
  index,
  fields
) => ({
  type: SUBACTIVIDAD_LAST_LEVEL_SELECTED,
  index,
  fields,
});

const addNext = (
  index,
  level
) => ({
  type: SUBACTIVIDAD_ADD_NEXT_SELECT,
  index,
  level,
});

const _selectLevel = (
  index,
  isLastChild,
  level,
  value
) => ({
  type: SUBACTIVIDAD_LEVEL_SELECT,
  index,
  isLastChild,
  level,
  value,
});

const findById = (arr, lookup) =>
  _.filter(
    arr,
    item => item.id === lookup
  )[0];

const findByLowerName = (arr, lookup) =>
  _.filter(
    arr,
    item => item.name.toLowerCase() === lookup
  )[0];

export const selectLevel = (
  index,
  isLastChild,
  level,
  value,
  lastChildData
) => (dispatch, getState) => {
  dispatch(_selectLevel(index, isLastChild, level, value));

  if (!isLastChild) {
    dispatch(addNext(index, level));
    dispatch(_updateSubActividadField(index, 'financiamiento', ''));
    return;
  }

  let fields = [
    {
      prop: 'otro',
      value: '',
    },
    {
      prop: 'cantidad',
      value: '1',
    },
    {
      prop: 'unidad',
      value: lastChildData.unidad,
    },
    {
      prop: 'financiamiento',
      value: '',
    },
    {
      prop: 'costo_unitario',
      value: '',
    },
    {
      prop: 'total',
      value: '',
    },
    {
      prop: 'file',
      value: lastChildData.path,
    },
  ];
  const niveles = getState().getIn(['sub', index, 'data', 'niveles']);

  let pasajes = '';
  let num_dias = '1';
  let inscripcion = '';
  let costo = '';
  let viaticos = '';

  if (niveles.get(0) === 'R' && niveles.get(2) !== 'SPL'
      && niveles.get(2) !== 'RPL') {
    const idTipoViaje = niveles.get(3);
    const nameTipoViaje = findById(
      getContext().catalogo,
      idTipoViaje
    ).name.toLowerCase();

    let idViaticos;
    if (nameTipoViaje === 'nacional') {
      idViaticos = niveles.last();
    } else {
      idViaticos = niveles.get(4);
    }

    const nameToLookup = findById(
      getContext().catalogo,
      idViaticos
    ).name.toLowerCase();

    const viaticoItem = findByLowerName(
      getContext().viaticos,
      nameToLookup
    );
    viaticos = JSON.parse(viaticoItem.data).costo;
  }

  if (niveles.get(2) === 'RPL') {
    viaticos = '0';
  }

  if (niveles.get(0) === 'R' || niveles.get(1) === 'SP') {
    pasajes = lastChildData.costo;
  }


  if (niveles.get(0) === 'S' || niveles.get(0) === 'B') {
    costo = lastChildData.costo;
  }


  if (niveles.get(1) === 'SP') {
    if (niveles.get(2) === 'SPL' || niveles.get(3).slice(-1) === 'N') {
      viaticos = '865';
    }
    else {
      viaticos = '1557';
    }
  }



  fields = fields.concat([
    {
      prop: 'pasajes',
      value: pasajes,
    },
    {
      prop: 'num_dias',
      value: num_dias,
    },
    {
      prop: 'inscripcion',
      value: inscripcion,
    },
    {
      prop: 'costo',
      value: costo,
    },
    {
      prop: 'viaticos',
      value: viaticos,
    },
  ]);

  dispatch(selectLastLevel(index, fields));

  calculateTotales({index, dispatch, getState});
};

export const _updateSubActividadField = (
  index,
  prop,
  value
) => ({
  type: SUBACTIVIDAD_UPDATE,
  index,
  prop,
  value,
});

export const PASANTIAS_ID = 'RP';

const calculateTotales = ({
  index,
  dispatch,
  getState,
}) => {

  const subActividadData = getState().getIn(['sub', index, 'data']);
  const niveles = subActividadData.get('niveles');

  if (niveles.get(0) === 'R') {
    let {
      pasajes,
      viaticos,
      num_dias,
      inscripcion,
      cantidad,
    } = subActividadData.toJS();

    if (niveles.get(1) === PASANTIAS_ID) {
      inscripcion = 0;
    }
    let costoUnitario = toNumber(pasajes) +
      toNumber(viaticos) * toNumber(num_dias) + toNumber(inscripcion);
    dispatch(_updateSubActividadField(index, 'costo_unitario', costoUnitario.toFixed(2).toString()));

    let total = costoUnitario * toNumber(cantidad);
    dispatch(_updateSubActividadField(index, 'total', total.toFixed(2).toString()));
  }

  if (niveles.get(0) === 'B') {
    let {
      costo,
      cantidad,
    } = subActividadData.toJS();
    let costoUnitario = toNumber(costo);
    dispatch(_updateSubActividadField(index, 'costo_unitario', costoUnitario.toFixed(2).toString()));

    let total = toNumber(costo) * toNumber(cantidad);
    dispatch(_updateSubActividadField(index, 'total', total.toFixed(2).toString()));
  }

  if (niveles.get(0) === 'S') {
    let {
      num_dias,
      cantidad,
      costo,
      pasajes,
    } = subActividadData.toJS();
    if (niveles.get(1) !== 'SP') {
      num_dias = 1;
      pasajes = 0;
    }
    let costoUnitario = toNumber(pasajes) + toNumber(costo) * toNumber(num_dias);
    dispatch(_updateSubActividadField(index, 'costo_unitario', costoUnitario.toFixed(2).toString()));

    let total = costoUnitario * toNumber(cantidad);
    dispatch(_updateSubActividadField(index, 'total', total.toFixed(2).toString()));
  }
};

export const updateSubActividadField = (
  index,
  prop,
  value
) => (dispatch, getState) => {
  dispatch(_updateSubActividadField(index, prop, value));
  calculateTotales({index, dispatch, getState});
};
