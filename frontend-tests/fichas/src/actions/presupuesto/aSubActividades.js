import _ from 'lodash';

import { getContext } from '../../context';
import subactivades from '../../reducers/presupuesto/subactividades';

import * as messages from '../../messages/presupuesto';

import constantsPresupuesto from '../../constants/presupuesto';
const {
  SUBACTIVIDAD_ADD,
  SUBACTIVIDAD_REMOVE,

  REQUEST_REMOVE_SUBACTIVIDAD,
  REQUEST_ADD_SUBACTIVIDAD,
} = constantsPresupuesto;

import { findIndexBy } from '../../utils';

import constantsStatus from '../../constants/status';
const {
  UNAUTHORIZED_ERROR,
} = constantsStatus;

import {
  nodoServerError,
  nodoFetching,
  nodoSuccess,

  updateRequestType,
} from '../aNodoStatus';

import {
  APIaddSubActividad,
  APIremoveSubActividad,
} from '../../webAPI/presupuesto';

// @TODO update dbId
export const _addSubActividad = (
  index,
  actividadIndex,
  actividadDbId,
  dbId
) => ({
  type: SUBACTIVIDAD_ADD,
  index,
  actividadIndex,
  actividadDbId,
  dbId,
});

const onSuccessAddSubActividad = ({
  dispatch,
  actionAddSubActividad,
  path,
  index
}, { ids }) => {
  console.log('dbIds', ids, index);
  dispatch(nodoSuccess(path));

  actionAddSubActividad.dbId = ids[index].dbId;
  dispatch(actionAddSubActividad);
};

//refactor this function, is the same with planificacion
const onErrorAddSubActividad = ({
  path,
  dispatch,
}, { code, message }) => {
  if (code === 401) {
    dispatch(updateRequestType(UNAUTHORIZED_ERROR));
  }

  dispatch(nodoServerError(path, message));
};

export const addSubActividad = (
  index,
  actividadIndex
) =>
  (dispatch, getState) => {
    // const actividadDbId = getState().getIn(['act', actividadIndex, 'dbId']);
    const actividadDbId = getContext().act[actividadIndex].dbId;

    const actionAddSubActividad = _addSubActividad(index, actividadIndex,
                                                   actividadDbId);
    dispatch(updateRequestType(REQUEST_ADD_SUBACTIVIDAD));

    let subActividadesOptimistic = subactivades(
      getState().get('sub'),
      actionAddSubActividad
    );

    const payload = subActividadesOptimistic
      .filter(s => s.get('parent_id') === actividadDbId)
      .toJS()
      .map((sub, orden) => ({
        orden,
        path: ['sub'],
        ..._.pick(sub, ['data', 'parent_id', 'dbId'])
      }));

    console.log(payload);
    const indexNewSubActividad = findIndexBy(
      payload,
      null,
      s => s.dbId
    );
    console.log(indexNewSubActividad);

    const dumbPath = ['act', actividadIndex];
    dispatch(nodoFetching(dumbPath));

    APIaddSubActividad(
      payload,
      _.partial(
        onSuccessAddSubActividad,
        { dispatch, actionAddSubActividad, path: dumbPath, index: indexNewSubActividad},
        _
      ),
      _.partial(
        onErrorAddSubActividad,
        {dispatch, path: dumbPath}, _
      )
    );

  };

const _removeSubActividad = (
  index
) => ({
  type: SUBACTIVIDAD_REMOVE,
  index,
});

const onSuccessRemoveSubActividad = ({
  dispatch,
  path,
  index,
  getState,
},  data) => {
  dispatch(_removeSubActividad(index));
  dispatch(nodoSuccess(path));
};

const onErrorRemoveSubActividad = ({
  dispatch,
  paths,
}, { code, message }) => {
  if (code === 401) {
    dispatch(updateRequestType(UNAUTHORIZED_ERROR));
  }
  paths
    .forEach(path => dispatch(nodoServerError(path, message)));
};

export const removeSubActividad = (
  index,
  actividadIndex
) =>
  (dispatch, getState) => {
    if (!confirm(messages.CONFIRM_REMOVE_SUBACTIVIDAD)) {
      return;
    }

    const dumbPath = ['act', actividadIndex];
    const path = ['sub', index];

    dispatch(updateRequestType(REQUEST_REMOVE_SUBACTIVIDAD));

    dispatch(nodoFetching(dumbPath));
    dispatch(nodoFetching(path));

    const payload = {
      dbId: getState().getIn([...path, 'dbId']),
    };

    APIremoveSubActividad(
      payload,
      _.partial(
        onSuccessRemoveSubActividad,
        { dispatch, path: dumbPath, index, getState },
        _
      ),
      _.partial(
        onErrorRemoveSubActividad,
        { dispatch, paths: [dumbPath, path ]},
        _
      )
    );
  };
