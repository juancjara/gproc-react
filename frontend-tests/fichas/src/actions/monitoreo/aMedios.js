import actions from '../../constants/monitoreo';
const {
  MEDIO_INCREMENT,
  MEDIO_DECREMENT,
} = actions;

export const medioIncrement = (
  id
) => ({
  type: MEDIO_INCREMENT,
  id,
});

export const medioDecrement = (
  id
) => ({
  type: MEDIO_DECREMENT,
  id,
});
