import uuid from 'node-uuid';

import actions from '../../../constants/monitoreo';
const {
  INDICADOR_MEDIO_ADD,
  INDICADOR_MEDIO_REMOVE,
  INDICADOR_MEDIO_UPDATE,
  INDICADOR_MEDIO_OTRO_UPDATE,
} = actions;

import {
  medioDecrement,
  medioIncrement,
} from '../aMedios';

export const updateOtroMedio = (
  indicadorIndex,
  indicadorMedioIndex,
  text
) => ({
  type: INDICADOR_MEDIO_OTRO_UPDATE,
  indicadorIndex,
  indicadorMedioIndex,
  text,
});

export const _addMedio = (
  indicadorIndex
) => ({
  type: INDICADOR_MEDIO_ADD,
  indicadorIndex,
  id: uuid.v4(),
});

export const addMedio = (
  indicadorIndex
) =>
  (dispatch, getState) => {
    dispatch(_addMedio(indicadorIndex));
  };

export const _removeMedio = (
  indicadorIndex,
  indicadorMedioIndex
) => ({
  type: INDICADOR_MEDIO_REMOVE,
  indicadorIndex,
  indicadorMedioIndex,
});

export const removeMedio = (
  indicadorIndex,
  indicadorMedioIndex
) =>
  (dispatch, getState) => {
    const pastMedioId = getState()
      .getIn(['ind', indicadorIndex, 'data', 'medios',
              indicadorMedioIndex, 'conc_id']);

    dispatch(medioDecrement(pastMedioId));
    dispatch(_removeMedio(indicadorIndex, indicadorMedioIndex));
  };

// @TODO
//could use updateIndicador action, using prop as an array path
export const _selectMedio = (
  indicadorIndex,
  indicadorMedioIndex,
  conc_id
) => ({
  type: INDICADOR_MEDIO_UPDATE,
  indicadorIndex,
  indicadorMedioIndex,
  conc_id,
});

export const selectMedio = (
  indicadorIndex,
  indicadorMedioIndex,
  value
) =>
  (dispatch, getState) => {
    const pastMedioId = getState()
      .getIn(['ind', indicadorIndex, 'data', 'medios',
              indicadorMedioIndex, 'conc_id']);

    dispatch(medioDecrement(pastMedioId));
    dispatch(medioIncrement(value));
    dispatch(_selectMedio(indicadorIndex, indicadorMedioIndex, value));
};
