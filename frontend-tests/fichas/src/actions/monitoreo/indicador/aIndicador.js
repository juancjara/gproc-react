import actions from '../../../constants/monitoreo';
const {
  INDICADOR_UPDATE,
} = actions;

export const updateIndicador = (
  indicadorIndex,
  prop,
  value
) => ({
  type: INDICADOR_UPDATE,
  indicadorIndex,
  prop,
  value,
});
