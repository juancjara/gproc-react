
import constantsMonitoreo from '../../constants/monitoreo';
const {
  INDICADOR_ADD,
  INDICADOR_REMOVE,

  REQUEST_ADD_INDICADOR,
  REQUEST_REMOVE_INDICADOR,
} = constantsMonitoreo;

import * as messages from '../../messages/monitoreo';

import {
  APIaddIndicador,
  APIremoveIndicador,
} from '../../webAPI/monitoreo';

import constantsStatus from '../../constants/status';
const {
  UNAUTHORIZED_ERROR,
} = constantsStatus;

import indicador from '../../reducers/monitoreo/indicador/indicador';
import { getContext } from '../../context';

import {
  nodoServerError,
  nodoFetching,
  nodoSuccess,

  updateRequestType,
} from '../aNodoStatus';

export const _addIndicador = (
  dbId,
  factorDbId
) => ({
  type: INDICADOR_ADD,
  factorDbId,
  dbId,
});

const onSuccessAddIndicador = ({
  dispatch,
  actionAddIndicador,
  path,
}, { dbId }) => {
  console.log('dbId', dbId);
  dispatch(nodoSuccess(path));

  actionAddIndicador.dbId = dbId;
  dispatch(actionAddIndicador);
};

const onErrorAddIndicador = ({
  path,
  dispatch,
}, { code, message }) => {
  if (code === 401) {
    dispatch(updateRequestType(UNAUTHORIZED_ERROR));
  }

  dispatch(nodoServerError(path, message));
};

export const addIndicador = (
  factorIndex
) =>
  (dispatch, getState) => {
    const factorDbId = getState().getIn(['fac', factorIndex, 'id']);
    const actionAddIndicador = _addIndicador('', factorDbId);

    const dataIndicadorOptimistic = indicador(
      undefined,
      actionAddIndicador
    );

    const payload = {
      parent_id: factorDbId,
      data: dataIndicadorOptimistic.toJS(),
      dbId: null,
      path: ['ind']
    };
    console.log('payload', payload);

    dispatch(updateRequestType(REQUEST_ADD_INDICADOR));

    const dumbPath = ['fac', factorIndex];
    dispatch(nodoFetching(dumbPath));

    APIaddIndicador(
      payload,
      _.partial(
        onSuccessAddIndicador,
        { dispatch, actionAddIndicador, path: dumbPath },
        _
      ),
      _.partial(
        onErrorAddIndicador,
        { dispatch, path: dumbPath },
        _
      )
    );
  };

const _removeIndicador = (
  index
) => ({
  type: INDICADOR_REMOVE,
  index,
});

const onSuccessRemoveIndicador = ({
  dispatch,
  path,
  index,
}, data) => {
  dispatch(_removeIndicador(index))
  dispatch(nodoSuccess(path));
};

const onErrorRemoveIndicador = ({
  dispatch,
  paths,
}, { code, message }) => {
  if (code === 401) {
    dispatch(updateRequestType(UNAUTHORIZED_ERROR));
  }
  paths
    .forEach(path => dispatch(nodoServerError(path, message)));
};

export const removeIndicador = (
  index,
  factorIndex
) =>
  (dispatch, getState) => {
    if (!confirm(messages.CONFIRM_REMOVE_INDICADOR)) {
      return;
    }

    const dumbPath = ['fac', factorIndex];
    const path = ['ind', index];

    dispatch(updateRequestType(REQUEST_REMOVE_INDICADOR));

    dispatch(nodoFetching(dumbPath));
    dispatch(nodoFetching(path));

    const payload = {
      dbId: getState().getIn([...path, 'dbId']),
    };

    APIremoveIndicador(
      payload,
      _.partial(
        onSuccessRemoveIndicador,
        { dispatch, path: dumbPath, index },
        _
      ),
      _.partial(
        onErrorRemoveIndicador,
        { dispatch, paths: [dumbPath, path] },
        _
      )
    );
  };
