import actions from '../../constants/expresionInteres';
const {
  INSTITUCIONAL_UPDATE,
} = actions;

export const institucionalUpdate = (
  prop,
  value
) => ({
  type: INSTITUCIONAL_UPDATE,
  prop,
  value,
});
