import actions from '../../constants/expresionInteres';

const {
  FICHA_UPDATE,
  FICHA_UPDATE_INST,
  FICHA_UPDATE_CARR,
} = actions;

export const fichaUpdate = (
  prop,
  value
) => ({
  type: FICHA_UPDATE,
  prop,
  value,
});

export const fichaUpdateInst = (
  prop,
  value
) => ({
  type: FICHA_UPDATE_INST,
  prop,
  value,
});

export const fichaUpdateCarr = (
  prop,
  value
) => ({
  type: FICHA_UPDATE_CARR,
  prop,
  value,
});
