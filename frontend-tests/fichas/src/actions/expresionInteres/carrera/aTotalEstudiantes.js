import actions from '../../../constants/expresionInteres';
const {
  CARRERA_TOTAL_ESTUDIANTES_UPDATE,
} = actions;

export const totalEstudiantesUpdate = (
  carreraId,
  row,
  col,
  gender,
  value
) => ({
  type: CARRERA_TOTAL_ESTUDIANTES_UPDATE,
  carreraId,
  row,
  col,
  gender,
  value,
});
