import uuid from 'node-uuid';

import actions from '../../../constants/expresionInteres';
const {
  CARRERA_VINCULOS_ADD,
  CARRERA_VINCULOS_UPDATE,
  CARRERA_VINCULOS_REMOVE,
} = actions;

export const vinculosAdd = (
  carreraId
) => ({
  type: CARRERA_VINCULOS_ADD,
  carreraId,
  id: uuid.v4(),
});

export const vinculosUpdate = (
  carreraId,
  row,
  prop,
  value
) => ({
  type: CARRERA_VINCULOS_UPDATE,
  carreraId,
  row,
  prop,
  value,
});

export const vinculosRemove = (
  carreraId,
  row
) => ({
  type: CARRERA_VINCULOS_REMOVE,
  carreraId,
  row,
});
