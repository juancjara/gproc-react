import actions from '../../../constants/expresionInteres';
const {
  CARRERA_FINANCIAMIENTO_UTILIZACION_UPDATE,
} = actions;

export const financiamientoUtilizacionUpdate = (
  carreraId,
  table,
  row,
  col,
  value
) => ({
  type: CARRERA_FINANCIAMIENTO_UTILIZACION_UPDATE,
  carreraId,
  table,
  row,
  col,
  value,
});
