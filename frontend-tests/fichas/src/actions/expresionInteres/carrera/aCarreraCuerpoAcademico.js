import actions from '../../../constants/expresionInteres';
const {
  CARRERA_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM,
  CARRERA_CUERPO_ACADEMICO_UPDATE_COL_SUM,
} = actions;

export const updateRowColSum = (
  carreraId,
  table,
  row,
  col,
  value
) => ({
  type: CARRERA_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM,
  carreraId,
  table,
  row,
  col,
  value,
});

export const updateColSum = (
  carreraId,
  table,
  row,
  col,
  value
) => ({
  type: CARRERA_CUERPO_ACADEMICO_UPDATE_COL_SUM,
  carreraId,
  table,
  row,
  col,
  value,
});
