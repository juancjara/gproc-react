import actions from '../../../constants/expresionInteres';
const {
  CARRERA_UPDATE,
} = actions;

export const carreraAreaUpdate = (
  carreraId,
  value
) => ({
  type: CARRERA_UPDATE,
  carreraId,
  prop: 'area',
  value,
});
