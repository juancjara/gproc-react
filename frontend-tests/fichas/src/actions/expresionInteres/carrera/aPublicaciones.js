import actions from '../../../constants/expresionInteres';
const {
  CARRERA_PUBLICACIONES_UPDATE,
} = actions;

export const publicacionesUpdate = (
  carreraId,
  table,
  row,
  col,
  value
) => ({
  type: CARRERA_PUBLICACIONES_UPDATE,
  table,
  carreraId,
  row,
  col,
  value,
});
