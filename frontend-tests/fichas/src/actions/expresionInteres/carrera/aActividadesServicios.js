import actions from '../../../constants/expresionInteres';
const {
  CARRERA_ACTIVIDADES_SERVICIOS_UPDATE,
} = actions;

export const actividadesServiciosUpdate = (
  carreraId,
  table,
  row,
  value
) => ({
  type: CARRERA_ACTIVIDADES_SERVICIOS_UPDATE,
  carreraId,
  table,
  row,
  value,
});
