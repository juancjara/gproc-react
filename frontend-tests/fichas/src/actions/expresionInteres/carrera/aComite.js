import uuid from 'node-uuid';

import actions from '../../../constants/expresionInteres';
const {
  CARRERA_COMITE_MEMBER_ADD,
  CARRERA_COMITE_MEMBER_REMOVE,
  CARRERA_COMITE_MEMBER_UPDATE,
} = actions;

export const addMember = (
  carreraId
) => ({
  type: CARRERA_COMITE_MEMBER_ADD,
  carreraId,
  id: uuid.v4(),
});

export const removeMember = (
  carreraId,
  index
) => ({
  type: CARRERA_COMITE_MEMBER_REMOVE,
  carreraId,
  index,
});

export const updateMember = (
  carreraId,
  row,
  col,
  value
) => ({
  type: CARRERA_COMITE_MEMBER_UPDATE,
  carreraId,
  row,
  col,
  value,
});
