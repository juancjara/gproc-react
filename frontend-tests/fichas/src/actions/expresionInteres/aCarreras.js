import actions from '../../constants/expresionInteres';
const {
  CARRERA_ADD,
  CARRERA_REMOVE,
  CARRERA_FILL_EMPTY,

  REQUEST_ADD_CARRERA,
} = actions;

import constantsStatus from '../../constants/status';
const {
  UNAUTHORIZED_ERROR,
} = constantsStatus;

import {
  findIndexById,
} from '../../utils';

import {
  APIaddCarrera,
} from '../../webAPI/expresionInteres';

import {
  addCarrera,
} from '../expresionInteres/institucional/aSedeCarreras';

import {
  nodoStored,
  nodoFetching,
  nodoServerError,
  updateRequestType,
} from '../aNodoStatus';

import institucional from '../../reducers/expresionInteres/institucional';
import carrera from '../../reducers/expresionInteres/carrera/carrera';

export const carrerasAddCarrera = (
  dbId,
  carreraId,
  nombre = '',
  area = ''
) => ({
  type: CARRERA_ADD,
  dbId,
  carreraId,
  nombre,
  area,
});

export const carrerasRemoveCarrera = (
  carreraId
) => ({
  type: CARRERA_REMOVE,
  carreraId,
});

const onSuccess = ({
  dispatch,
  pathIns,
  sedeName,
  sedeIndex,
  carreraId,
},
{
  data,
}) => {
  console.log('addcarera rs', data[0].dbId, data[1].dbId);
  dispatch(addCarrera(sedeIndex, sedeName, carreraId));
  dispatch(carrerasAddCarrera(data[1].dbId, carreraId));

  dispatch(nodoStored(pathIns, data[0].dbId));
};

const onError = ({
  dispatch,
  pathIns,
}, { message, code}) => {
  if (code === 401) {
    dispatch(updateRequestType(UNAUTHORIZED_ERROR));
  }
  dispatch(nodoServerError(pathIns, message));
};

export const addCarreraInDB = ({
  getState,
  dispatch,
  sedeName,
  carreraId,
  indexCarrera,
  sedeIndex,
}) => {
  const pathIns = ['ins'];
  //const pathCar = ['car', indexCarrera];
  const pathCar = ['car', carreraId];

  const dataInstitucional = getState().getIn(['ins', 'data']);

  const dataInstitucionalOptimistic = institucional(
    dataInstitucional,
    addCarrera(sedeIndex, sedeName, carreraId)
  );

  const dataCarreraOptimistic = carrera(
    undefined,
    { carreraId }
  )
  .get('data');

  const payload = [
    {
      data: dataInstitucionalOptimistic.toJS(),
      dbId: getState().getIn(['ins', 'dbId']),
      path: pathIns,
    },
    {
      data: dataCarreraOptimistic.toJS(),
      dbId: null,
      path: pathCar,
    },
  ];

  dispatch(updateRequestType(REQUEST_ADD_CARRERA));
  dispatch(nodoFetching(pathIns));

  APIaddCarrera(
    payload,
    _.partial(onSuccess, { dispatch, pathIns, sedeName, sedeIndex, carreraId }, _),
    _.partial(onError, { dispatch, pathIns }, _)
  );
};

export const fillBellowEmptyItems = () => ({
  type: CARRERA_FILL_EMPTY,
});
