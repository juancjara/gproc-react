import * as messages from '../../../messages/expresionInteres';
import _ from 'lodash';

import actions from '../../../constants/expresionInteres';
const {
  INSTITUCIONAL_SEDE_CARRERA_ADD,
  INSTITUCIONAL_SEDE_CARRERA_REMOVE,
  INSTITUCIONAL_SEDE_CARRERA_UPDATE,

  REQUEST_REMOVE_CARRERA,
} = actions;

import constantsStatus from '../../../constants/status';
const {
  UNAUTHORIZED_ERROR,
} = constantsStatus;

import {
  addCarreraInDB,
  carrerasRemoveCarrera,
} from '../aCarreras';

import {
  APIremoveCarrera,
} from '../../../webAPI/expresionInteres';

import {
  nodoStored,
  nodoFetching,
  nodoServerError,
  updateRequestType,
} from '../../aNodoStatus';

import {
  findIndexById,
  findIndexBy,
} from '../../../utils';

import institucional from '../../../reducers/expresionInteres/institucional';

const getNextCarreraId = (
  getState,
  sedeIndex
) => {
  const carreras = getState()
    .getIn(['ins', 'data', 'sedes', sedeIndex, 'carreras']);

  return carreras.size ?
    carreras.last().get('id') + 1 :
    getState().getIn(['ins', 'data', 'sedes', sedeIndex, 'id']) * 1000;
};

// =============================================================================

export const addCarrera = (
  sedeIndex,
  sedeName,
  carreraId,
  nombre = '',
  area = ''
) => ({
  type: INSTITUCIONAL_SEDE_CARRERA_ADD,
  sedeIndex,
  sedeName,
  carreraId,
  nombre,
  area,
});

export const addSedeCarrera = (
  sedeIndex,
  isInstitucional,
  carrera,
  area
) => {
  return (dispatch, getState) => {
    const carreraId = getNextCarreraId(getState, sedeIndex);
    const sedeName = getState()
      .getIn(
        ['ins', 'data', 'sedes', sedeIndex, 'nombre_sede']
      );

    if (isInstitucional) {
      const indexCarrera = getState()
        .getIn(
          ['ins', 'data', 'sedes', sedeIndex, 'carreras']
        )
        .size;

      return addCarreraInDB({
        getState,
        dispatch,
        sedeName,
        sedeIndex,
        carreraId,
        indexCarrera,
      });
    }

    dispatch(addCarrera(sedeIndex, sedeName, carreraId, carrera, area));
  };
};

// =============================================================================

export const removeSedeCarrera = (
  sedeIndex,
  carreraId,
  isInstitucional
) => {
  return (dispatch, getState) => {
    if (isInstitucional && confirm(messages.CONFIRM_REMOVE_CARRERA)) {
      return removeCarreraInDB({ getState, dispatch, sedeIndex, carreraId });
    }

    if ( ! isInstitucional) {
      dispatch(removeCarrera(sedeIndex, carreraId));
    }
  };
};

const onSuccess = ({
  pathIns,
  sedeIndex,
  carreraId,
  dispatch,
}, { dbId }) => {
  console.log('remove carrera db', dbId);
  dispatch(nodoStored(pathIns, dbId));

  dispatch(removeCarrera(sedeIndex, carreraId));
  dispatch(carrerasRemoveCarrera(carreraId));
};

const onError = ({
  pathIns,
  pathCarrera,
  dispatch,
}, { message, code }) => {
  if (code === 401) {
    dispatch(updateRequestType(UNAUTHORIZED_ERROR));
  }

  dispatch(nodoServerError(pathIns, message));
  dispatch(nodoServerError(pathCarrera, message));
};

export const removeCarreraInDB = ({
  getState,
  dispatch,
  sedeIndex,
  carreraId,
}) => {
  const index = findIndexById(getState().get('car'), carreraId);

  const pathIns = ['ins'];
  const pathCarrera = ['car', index];


  dispatch(updateRequestType(REQUEST_REMOVE_CARRERA));
  dispatch(nodoFetching(pathIns));
  dispatch(nodoFetching(pathCarrera));

  const dataInstitucional = getState().getIn(['ins', 'data']);

  const dataInstitucionalOptimistic = institucional(
    dataInstitucional,
    removeCarrera(sedeIndex, carreraId)
  );

  const payload = {
    ins: {
      data: dataInstitucionalOptimistic.toJS(),
      dbId: getState().getIn(['ins', 'dbId']),
      path: pathIns,
    },
    carreraId: getState().getIn(['car', index, 'dbId']),
  };

  APIremoveCarrera(
    payload,
    _.partial(onSuccess, { dispatch, pathIns, carreraId, sedeIndex }, _),
    _.partial(onError, { dispatch, pathIns, pathCarrera }, _)
  );
};

export const removeCarrera = (
  sedeIndex,
  carreraId
) => ({
  type: INSTITUCIONAL_SEDE_CARRERA_REMOVE,
  sedeIndex,
  carreraId,
});

export const updateCarrera = (
  sedeIndex,
  sedeName,
  carreraId,
  prop,
  value
) => ({
  type: INSTITUCIONAL_SEDE_CARRERA_UPDATE,
  sedeIndex,
  sedeName,
  carreraId,
  prop,
  value,
});

// =============================================================================
