import actions from '../../../constants/expresionInteres';
const {
  INSTITUCIONAL_TOTAL_ESTUDIANTES_UPDATE,
} = actions;

export const updateTotalEstudiantes = (
  table,
  row,
  col,
  gender,
  value
) => ({
  type: INSTITUCIONAL_TOTAL_ESTUDIANTES_UPDATE,
  table,
  row,
  col,
  gender,
  value,
});
