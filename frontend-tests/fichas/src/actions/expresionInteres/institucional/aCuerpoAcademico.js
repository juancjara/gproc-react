import actions from '../../../constants/expresionInteres';
const {
  INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM,
  INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_COL_SUM,
} = actions;

export const updateRowColSum = (
  table,
  row,
  col,
  value
) => ({
  type: INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_ROW_COL_SUM,
  table,
  row,
  col,
  value,
});

export const updateColSum = (
  table,
  row,
  col,
  value
) => ({
  type: INSTITUCIONAL_CUERPO_ACADEMICO_UPDATE_COL_SUM,
  table,
  row,
  col,
  value,
});
