import uuid from 'node-uuid';

import actions from '../../../constants/expresionInteres';
const {
  INSTITUCIONAL_COMITE_MEMBER_ADD,
  INSTITUCIONAL_COMITE_MEMBER_REMOVE,
  INSTITUCIONAL_COMITE_MEMBER_UPDATE,
} = actions;

export const addMember = (
) => ({
  type: INSTITUCIONAL_COMITE_MEMBER_ADD,
  id: uuid.v4(),
});

export const removeMember = (
  index
) => ({
  type: INSTITUCIONAL_COMITE_MEMBER_REMOVE,
  index,
});

export const updateMember = (
  row,
  col,
  value
) => ({
  type: INSTITUCIONAL_COMITE_MEMBER_UPDATE,
  row,
  col,
  value,
});
