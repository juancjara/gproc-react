import _ from 'lodash';

import * as messages from '../../../messages/expresionInteres';

import actions from '../../../constants/expresionInteres';
const {
  INSTITUCIONAL_SEDE_ADD,
  INSTITUCIONAL_SEDE_REMOVE,
  INSTITUCIONAL_SEDE_UPDATE,

  REQUEST_REMOVE_SEDE,
} = actions;

import constantsStatus from '../../../constants/status';
const {
  UNAUTHORIZED_ERROR,
} = constantsStatus;

import {
  findIndexById,
} from '../../../utils';

import {
  nodoStored,
  nodoFetching,
  nodoServerError,
  updateRequestType,
} from '../../aNodoStatus';

import {
  addCarreraInDB,
  carrerasRemoveCarrera,
} from '../aCarreras';

import {
  addCarrera,
  removeCarrera,
} from './aSedeCarreras';

import {
  APIremoveSede,
} from '../../../webAPI/expresionInteres';

import institucional from '../../../reducers/expresionInteres/institucional';

// =============================================================================

export const _addSede = (
  sedeId,
  name
) => ({
  type: INSTITUCIONAL_SEDE_ADD,
  sedeId,
  name,
});

export const addSede = (
  isInstitucional
) => {
  return (dispatch, getState) => {
    const lastSede = getState().getIn(['ins', 'data', 'sedes']).last();
    const sedesSize = getState().getIn(['ins', 'data', 'sedes']).size;
    const sedeId = lastSede.get('id') + 1;

    const sedeName = `Filial ${sedeId}`;

    dispatch(_addSede(sedeId, sedeName));

    if (isInstitucional) {
      return addCarreraInDB(
        { getState, dispatch, sedeName, carreraId: sedeId * 1000,
          indexCarrera: 0 , sedeIndex: sedesSize});
    }
    dispatch(addCarrera(sedesSize, sedeName, sedeId * 1000));
  }
};

// =============================================================================

export const _removeSede = (
  sedeIndex
) => ({
  type: INSTITUCIONAL_SEDE_REMOVE,
  sedeIndex,
});

const onSuccess = ({
  pathIns,
  dispatch,
  ids,
  sedeIndex,
}, { dbId }) => {
  console.log('remove all carreras', dbId);
  dispatch(nodoStored(pathIns, dbId));

  ids.forEach(id =>
    dispatch(removeCarrera(sedeIndex, id)));
  ids.forEach(id =>
    dispatch(carrerasRemoveCarrera(id)));

  dispatch(_removeSede(sedeIndex));
};

const onError = ({
  dispatch,
  pathIns,
  carrerasIndexes,
}, { message, code }) => {
  if (code === 401) {
    dispatch(updateRequestType(UNAUTHORIZED_ERROR));
  }
  dispatch(nodoServerError(pathIns, message));

  carrerasIndexes
    .forEach(index =>
      dispatch(nodoServerError(['car', index], message)));
};

const removeAllCarrerasInDB = ({
  getState,
  dispatch,
  sedeIndex,
  ids,
}) => {
  const pathIns = ['ins'];
  let carrerasSize = getState()
    .getIn(['ins', 'data', 'sedes', sedeIndex, 'carreras']).size;

  const carreras = getState().get('car');
  const carrerasIndexes = ids
    .map(_.partial(findIndexById, carreras, _));

  dispatch(updateRequestType(REQUEST_REMOVE_SEDE));
  dispatch(nodoFetching(pathIns));

  carrerasIndexes
    .forEach(index => dispatch(nodoFetching(['car', index])));

  const dbIdsCarrera = carrerasIndexes
    .map(index => getState().getIn(['car', index, 'dbId']));

  const dataInstitucional = getState().getIn(['ins', 'data']);

  let dataInstitucionalOptimistic = dataInstitucional;
  ids.forEach(id => {
    dataInstitucionalOptimistic = institucional(
      dataInstitucionalOptimistic,
      removeCarrera(sedeIndex, id)
    );
  });

  dataInstitucionalOptimistic = institucional(
    dataInstitucionalOptimistic,
    _removeSede(sedeIndex)
  );

  const payload = {
    ins: {
      data: dataInstitucionalOptimistic.toJS(),
      dbId: getState().getIn(['ins', 'dbId']),
      path: pathIns,
    },
    ids: dbIdsCarrera,
  };

  console.log(payload);

  APIremoveSede(
    payload,
    _.partial(onSuccess, { dispatch, ids, sedeIndex, pathIns }, _),
    _.partial(onError, { dispatch, pathIns, carrerasIndexes }, _)
  );
};

const isFirstSede = (sedeIndex) => sedeIndex === 0;

export const removeSede = (
  sedeIndex,
  isInstitucional
) => {
  return (dispatch, getState) => {
    if (isFirstSede(sedeIndex)) {
      return alert(messages.ERROR_REMOVE_FIRST_SEDE);
    }

    const carreras = getState()
      .getIn(['ins', 'data', 'sedes', sedeIndex, 'carreras']);

    const idsToRemove = carreras
      .map((row) => row.get('id'));

    if (isInstitucional && confirm(messages.CONFIRM_REMOVE_SEDE)) {
      return removeAllCarrerasInDB({
        getState,
        dispatch,
        sedeIndex,
        ids: idsToRemove.toJS(),
      });
    }

    if ( ! isInstitucional) {
      idsToRemove.forEach(id => dispatch(removeCarrera(sedeIndex, id)));
      dispatch(_removeSede(sedeIndex));
    }
  };
};

export const updateSede = (
  sedeIndex,
  prop,
  value
) => ({
  type: INSTITUCIONAL_SEDE_UPDATE,
  sedeIndex,
  prop,
  value,
});
