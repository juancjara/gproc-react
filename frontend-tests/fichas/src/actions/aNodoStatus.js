import _ from 'lodash';

import actions from '../constants/status';
const {
  NODO_VALIDATION_ERROR,
  NODO_STORED,
  NODO_FETCHING,
  NODO_SERVER_ERROR,
  NODO_SUCCESS,

  REQUEST_SAVE_ALL,
  UPDATE_REQUEST_TYPE,
  UNAUTHORIZED_ERROR,
} = actions;

export const notValidated = (
  path,
  errors
) => ({
  type: NODO_VALIDATION_ERROR,
  path,
  errors,
});

export const updateRequestType = (
  request
) => ({
  type: UPDATE_REQUEST_TYPE,
  request,
});

export const nodoFetching = (
  path
) => ({
  type: NODO_FETCHING,
  path,
});

export const nodoSuccess = (
  path
) => ({
  type: NODO_SUCCESS,
  path,
});

export const nodoStored = (
  path,
  dbId
) => ({
  type: NODO_STORED,
  path,
  dbId,
});

export const nodoServerError = (
  path,
  error
) => ({
  type: NODO_SERVER_ERROR,
  path,
  error,
});

const defaultValidation = () => [];

const onSuccessStoreNodos = ({
  paths,
  dispatch,
}, { data }) => {
  console.log('stores nodos', data);
  _.forEach(paths, path => dispatch(nodoSuccess(path)));
};

const onErrorStoreNodos = ({
  paths,
  dispatch,
}, { code, message }) => {
  if (code === 401) {
    dispatch(updateRequestType(UNAUTHORIZED_ERROR));
  }
  paths.forEach((path, i) =>
    dispatch(nodoServerError(path, message)));
};

export const storeNodos = ({
  paths,
  extraPaths = paths,
  webAPI,
}) => (dispatch, getState) => {
  const payload = _.map(paths, (path, i) => ({
      dbId: getState().getIn(path).get('dbId'),
      data: getState().getIn(path).get('data').toJS(),
      path: extraPaths[i],
      parent_id: getState().getIn(path).get('parent_id'),
    }));

  _.forEach(paths, path => dispatch(nodoFetching(path)));

  webAPI(
    payload,
    _.partial(onSuccessStoreNodos, { paths, dispatch }, _),
    _.partial(onErrorStoreNodos, { paths, dispatch }, _)
  );
};

const onSuccess = (
  path,
  dispatch,
  { dbId }
) => dispatch(nodoStored(path, dbId));

const onError = (
  path,
  dispatch,
  { code, message }
) => {
  if (code === 401) {
    dispatch(updateRequestType(UNAUTHORIZED_ERROR));
  }
  dispatch(nodoServerError(path, message));
};

export const storeNodo = ({
  path,
  webAPI,
}) => {
  return (dispatch, getState) => {

    const nodoState = getState().getIn(path);
    const data = {
      dbId: nodoState.get('dbId'),
      data: nodoState.get('data').toJS(),
      path,
    };

    dispatch(updateRequestType(REQUEST_SAVE_ALL));
    dispatch(nodoFetching(path));

    webAPI(
      data,
      _.partial(onSuccess, path, dispatch, _),
      _.partial(onError, path, dispatch, _)
    );
  };
};
