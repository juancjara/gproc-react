import actions from '../../constants/efectos';
const {
  EFECTO_UPDATE,
} = actions;

export const updateEfecto = (
  efectoIndex,
  prop,
  value
) => ({
  type: EFECTO_UPDATE,
  efectoIndex,
  prop,
  value,
});
