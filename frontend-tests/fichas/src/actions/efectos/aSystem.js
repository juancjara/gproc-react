import _ from 'lodash';

import constantsPlanificacion from '../../constants/planificacion';
const {
  REQUEST_SAVE_ALL,
} = constantsPlanificacion;

import {
  displayErrors,
} from '../planificacion/aSystem';

import {
  shouldSaveNode,
} from '../presupuesto/aSystem';

import {
  validateInlineErrors,
} from '../../utils';

import {
  APIstoreNodos,
  APIfinish,
} from '../../webAPI/efectos';

import {
  storeNodos,
  updateRequestType
} from '../../actions/aNodoStatus';

import constants from '../../constants/status';
const {
  STORED_OK,
} = constants;


const keyPaths = ['fac', 'efe'];

const getPaths = (state, keyPaths) =>
  [
    ['fic'],
      ..._(keyPaths)
      .map(k => state.get(k).map((_, i) => [k, i]).toJS())
      .flatten()
      .value()
    ];

const hasErrorAndDisplay = ({
  dispatch,
  state,
}) => {
  const paths = getPaths(state, ['efe']);

  const validInlineNodos = paths
    .map(p => validateInlineErrors(document.getElementById(p.join(''))));

  const errorMsg = 'Existen campos que no cumplen con los valores permitidos';
  const inlineErrors = validInlineNodos
    .map(isValid => {
      if (!isValid) {
        return [errorMsg];
      }
      return [];
    });

  displayErrors({ dispatch, paths, errors: inlineErrors });

  const cannotFinish = _.filter(inlineErrors, errors => errors.length).length;
  return cannotFinish;
};


const saveAllNodes = ({
  dispatch,
  state,
}) => {
  const paths = getPaths(state, ['efe']);
  const pathsToSave = paths
          .filter(_.partial(shouldSaveNode, _, state));

  if (!pathsToSave.length) return;

  dispatch(updateRequestType(REQUEST_SAVE_ALL));
  dispatch(storeNodos({ paths: pathsToSave, webAPI: APIstoreNodos }));
};

export const finishFicha = status =>
  (dispatch, getState) => {
    const cannotFinish = hasErrorAndDisplay({
      dispatch,
      state: getState()
    });

    if (status !== STORED_OK) {
      return saveAllNodes({
        dispatch,
        state: getState(),
      });
    }

    if (cannotFinish) {
      return;
    }
    console.log('to finish');
    APIfinish();
  };

export const showErrorsAndSaveAllNodes = () =>
  (dispatch, getState) => {
    hasErrorAndDisplay({
      dispatch,
      state: getState(),
    });

    saveAllNodes({
      dispatch,
      state: getState(),
    });
  };
