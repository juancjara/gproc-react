var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('../webpack.config');

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
  proxy: {
    '/ApiPlanificacion/*': {
      target: 'http://localhost:5000',
    },
    '/ApiExpInteres/*': {
      target: 'http://localhost:5000',
    },
    '/ApiPresupuesto/*': {
      target: 'http://localhost:5000',
    },
    '/ApiMonitoreo/*': {
      target: 'http://localhost:5000',
    },
    '/ApiEfectos/*': {
      target: 'http://localhost:5000',
    },
    '/ApiCronograma/*': {
      target: 'http://localhost:5000',
    }
  }
}).listen(3000, 'localhost', function (err, result) {
  if (err) {
    console.log(err);
  }

  console.log('Listening at localhost:3000');
});
