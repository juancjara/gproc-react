var path = require('path');
var webpack = require('webpack');

var entries = [
  'webpack-dev-server/client?http://localhost:3000',
  'webpack/hot/only-dev-server',
];

module.exports = {
  devtool: 'eval',
  entry: {
    expresionInteres: entries.concat('./src/containers/expresionInteres/index'),
    planificacion: entries.concat('./src/containers/planificacion/index'),
    presupuesto: entries.concat('./src/containers/presupuesto/index'),
    monitoreo: entries.concat('./src/containers/monitoreo/index'),
    efectos: entries.concat('./src/containers/efectos/index'),
    efectos: entries.concat('./src/containers/efectos/index'),
    cronograma: entries.concat('./src/containers/cronograma/index'),
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['react-hot', 'babel'],
      include: path.join(__dirname, 'src')
    }]
  }
};
