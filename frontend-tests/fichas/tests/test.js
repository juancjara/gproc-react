//import data from './data.v2';
var data = require('./data.v2');
//import _ from 'lodash';

//const res = {};

function isObject(nodo) {
  return typeof nodo === 'object' && ! Array.isArray(nodo);
}

function nullify(nodo) {

  if (isObject(nodo)) {
    Object.keys(nodo).forEach(function (key) {
      nodo[key] = nullify(nodo[key]);
    })
    return nodo;
  }
  else if (Array.isArray(nodo)) {
    return [];
    /*nodo = nodo.map(function (el) {
      return nullify(el);
    });*/
  }
  else {
    return "";
  }
}

console.log(JSON.stringify(nullify(data)));
