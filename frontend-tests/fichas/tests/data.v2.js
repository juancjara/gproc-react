(function (root) {
  var data = {
    "fic": {
      "dbId": 123,
      "data": {
        "tifi_id": "2",
        "datos": {
          "inst": {
            "nombre": "enriquez salas porfirio",
            "cargo": "rector",
            "direccion": "av. el ejercito 1320",
            "fijo": "051 369377",
            "celular": "951669165",
            "email": "rflorez@hotmail.com",
            "emailalt": "rflorez@hotmail.com",
            "date": "2015-08-17",
            "date_local": "17 de agosto del 2015"
          },
          "carr": {
            "nombre": "sanga catunta raul",
            "cargo": "director",
            "direccion": "av. residencial 141",
            "fijo": "051 363607",
            "celular": "950896540",
            "email": "raulsanga_123@hotmail.com",
            "emailalt": "raulsanga1@hotmail.com",
            "date": "2015-08-17",
            "date_local": "17 de agosto del 2015"
          }
        }
      },
      status: {
        "status": 'STORED',
        "errors": [],
        "hasChanged": false,
      },
    },
    "ins": {
      status: {
        "status": 'STORED',
        "errors": [],
        "hasChanged": false,
      },
      "dbId": 125,
      "data": {
        "comite": [
          {
            "id": "0217de02-cb1d-47e7-afed-dbfa4731f541",
            "pers_nombres": "yony",
            "pers_apellidos": "pino vanegas",
            "pers_cargo": "jefe de la oficina universitaria de acreditación",
            "pers_correo": "yopinov@hotmail.com",
            "pers_telefono": "951633904"
          }
        ],
        "cod_mod": "-",
        "ruc": "20145496170",
        "sedes": [
          {
            "id": 0,
            "nombre_sede": "Sede principal",
            "direc": "av. sesquicentenario nº 1150",
            "dist": "puno",
            "prov": "puno",
            "region": "puno",
            "carreras": [
              {
                "id": 0,
                "nombre": "educación secundaria con 4 especialidades",
                "area": "sociales"
              },
              {
                "id": 1,
                "nombre": "educación primaria",
                "area": "sociales"
              },
              {
                "id": 3,
                "nombre": "educación inicial",
                "area": "sociales"
              },
            ]
          }
        ],
        "cuerpo_academico": {
          "grados_dedicacion": {
            "pt": {
              "ex": "0",
              "tc": "0",
              "tp": "0",
              "tt": "0"
            },
            "pe": {
              "ex": "0",
              "tc": "0",
              "tp": "0",
              "tt": "0"
            },
            "bc": {
              "ex": "0",
              "tc": "0",
              "tp": "0",
              "tt": "0"
            },
            "lt": {
              "ex": "112",
              "tc": "212",
              "tp": "181",
              "tt": "505"
            },
            "mm": {
              "ex": "217",
              "tc": "152",
              "tp": "86",
              "tt": "455"
            },
            "dr": {
              "ex": "74",
              "tc": "63",
              "tp": "9",
              "tt": "146"
            },
            "tt": {
              "ex": "403",
              "tc": "427",
              "tp": "276",
              "tt": "1106"
            }
          },
          "grados_condicion": {
            "pt": {
              "no": "0",
              "co": "0",
              "tt": "0"
            },
            "pe": {
              "no": "0",
              "co": "0",
              "tt": "0"
            },
            "bc": {
              "no": "0",
              "co": "0",
              "tt": "0"
            },
            "lt": {
              "no": "337",
              "co": "168",
              "tt": "505"
            },
            "mm": {
              "no": "352",
              "co": "103",
              "tt": "455"
            },
            "dr": {
              "no": "135",
              "co": "11",
              "tt": "146"
            },
            "tt": {
              "no": "824",
              "co": "282",
              "tt": "1106"
            }
          },
          "docentes_categoria": {
            "pr": {
              "tt": "135"
            },
            "as": {
              "tt": "352"
            },
            "au": {
              "tt": "337"
            },
            "tt": {
              "tt": "824"
            }
          }
        },
        "total_estudiantes": {
          "p_admision": [
            {
              "id": 0,
              "nombre": "Sede principal: educación secundaria con 4 especialidades",
              "x2014": {
                "m": "8",
                "f": "2"
              },
              "x2013": {
                "m": "8",
                "f": "2"
              },
              "x2012": {
                "m": "8",
                "f": "2"
              },
              "x2011": {
                "m": "8",
                "f": "2"
              },
              "x2015": {
                "m": "8",
                "f": "2"
              }
            },
            {
              "id": 1,
              "nombre": "Sede principal: educación primaria",
              "x2014": {
                "m": "8",
                "f": "2"
              },
              "x2013": {
                "m": "8",
                "f": "2"
              },
              "x2012": {
                "m": "8",
                "f": "2"
              },
              "x2011": {
                "m": "8",
                "f": "2"
              },
              "x2015": {
                "m": "8",
                "f": "2"
              }
            },
            {
              "id": 3,
              "nombre": "Sede principal: educación inicial",
              "x2014": {
                "m": "8",
                "f": "2"
              },
              "x2013": {
                "m": "8",
                "f": "2"
              },
              "x2012": {
                "m": "8",
                "f": "2"
              },
              "x2011": {
                "m": "8",
                "f": "2"
              },
              "x2015": {
                "m": "8",
                "f": "2"
              }
           },
                    ],
          "m_semestre": [
            {
              "id": 0,
              "nombre": "Sede principal: educación secundaria con 4 especialidades",
              "x2014": {
                "m": "8",
                "f": "2"
              },
              "x2013": {
                "m": "8",
                "f": "2"
              },
              "x2012": {
                "m": "8",
                "f": "2"
              },
              "x2011": {
                "m": "8",
                "f": "2"
              },
              "x2015": {
                "m": "8",
                "f": "2"
              }
            },
            {
              "id": 1,
              "nombre": "Sede principal: educación primaria",
              "x2014": {
                "m": "8",
                "f": "2"
              },
              "x2013": {
                "m": "8",
                "f": "2"
              },
              "x2012": {
                "m": "8",
                "f": "2"
              },
              "x2011": {
                "m": "8",
                "f": "2"
              },
              "x2015": {
                "m": "8",
                "f": "2"
              }
            },
            {
              "id": 3,
              "nombre": "Sede principal: educación inicial",
              "x2014": {
                "m": "8",
                "f": "2"
              },
              "x2013": {
                "m": "8",
                "f": "2"
              },
              "x2012": {
                "m": "8",
                "f": "2"
              },
              "x2011": {
                "m": "8",
                "f": "2"
              },
              "x2015": {
                "m": "8",
                "f": "2"
              }
           },
                    ],
          "egresados": [
            {
              "id": 0,
              "nombre": "Sede principal: educación secundaria con 4 especialidades",
              "x2014": {
                "m": "8",
                "f": "2"
              },
              "x2013": {
                "m": "8",
                "f": "2"
              },
              "x2012": {
                "m": "8",
                "f": "2"
              },
              "x2011": {
                "m": "8",
                "f": "2"
              },
              "x2015": {
                "m": "8",
                "f": "2"
              }
            },
            {
              "id": 1,
              "nombre": "Sede principal: educación primaria",
              "x2014": {
                "m": "8",
                "f": "2"
              },
              "x2013": {
                "m": "8",
                "f": "2"
              },
              "x2012": {
                "m": "8",
                "f": "2"
              },
              "x2011": {
                "m": "8",
                "f": "2"
              },
              "x2015": {
                "m": "8",
                "f": "2"
              }
            },
            {
              "id": 3,
              "nombre": "Sede principal: educación inicial",
              "x2014": {
                "m": "8",
                "f": "2"
              },
              "x2013": {
                "m": "8",
                "f": "2"
              },
              "x2012": {
                "m": "8",
                "f": "2"
              },
              "x2011": {
                "m": "8",
                "f": "2"
              },
              "x2015": {
                "m": "8",
                "f": "2"
              }
           },
                    ],
          "titulados": [
            {
              "id": 0,
              "nombre": "Sede principal: educación secundaria con 4 especialidades",
              "x2014": {
                "m": "8",
                "f": "2"
              },
              "x2013": {
                "m": "8",
                "f": "2"
              },
              "x2012": {
                "m": "8",
                "f": "2"
              },
              "x2011": {
                "m": "8",
                "f": "2"
              },
              "x2015": {
                "m": "8",
                "f": "2"
              }
            },
            {
              "id": 1,
              "nombre": "Sede principal: educación primaria",
              "x2014": {
                "m": "8",
                "f": "2"
              },
              "x2013": {
                "m": "8",
                "f": "2"
              },
              "x2012": {
                "m": "8",
                "f": "2"
              },
              "x2011": {
                "m": "8",
                "f": "2"
              },
              "x2015": {
                "m": "8",
                "f": "2"
              }
            },
            {
              "id": 3,
              "nombre": "Sede principal: educación inicial",
              "x2014": {
                "m": "8",
                "f": "2"
              },
              "x2013": {
                "m": "8",
                "f": "2"
              },
              "x2012": {
                "m": "8",
                "f": "2"
              },
              "x2011": {
                "m": "8",
                "f": "2"
              },
              "x2015": {
                "m": "8",
                "f": "2"
              }
           },
                    ]
        }
      }
    },
    "car": [
      {
        dbId: 5,
        status : {
          "status": 'STORED',
          "errors": [],
          "hasChanged": false,
        },
        data: {
        "id": 0,
        "nombre": "educaciÓn secundaria  con 4 especialidades",
        "area": "sociales",
        "comite": [
          {
            "id": "02e03-cb1d-47e7-afed-dbfa4731f541",
            "pers_nombres": "wildo",
            "pers_apellidos": "condorí",
            "pers_cargo": "presidente",
            "pers_correo": "widowilliam@hotmail.com",
            "pers_telefono": "996500961"
          },
          {
            "id": "0204-cb1d-47e7-afed-dbfa4731f541",
            "pers_nombres": "nilton",
            "pers_apellidos": "mayta",
            "pers_cargo": "secretario",
            "pers_correo": "nicemaja78@hotmail.com",
            "pers_telefono": "951771202"
          },
          {
            "id": "02e05-cb1d-47e7-afed-dbfa4731f541",
            "pers_nombres": "ofelia",
            "pers_apellidos": "mamani",
            "pers_cargo": "docente",
            "pers_correo": "sumayaofely@hotmail.com",
            "pers_telefono": "951565014"
          }
        ],
        "estudiantes": {
          "2015": {
            "pa": {
              "m": "6",
              "f": "6"
            },
            "ms": {
              "m": "6",
              "f": "6"
            },
            "eg": {
              "m": "6",
              "f": "6"
            },
            "ti": {
              "m": "6",
              "f": "6"
            }
          },
          "2011": {
            "pa": {
              "m": "8",
              "f": "9"
            },
            "ms": {
              "m": "1",
              "f": "76"
            },
            "eg": {
              "m": "6",
              "f": "6"
            },
            "ti": {
              "m": "6",
              "f": "6"
            }
          },
          "2012": {
            "pa": {
              "m": "8",
              "f": "2"
            },
            "ms": {
              "m": "6",
              "f": "3"
            },
            "eg": {
              "m": "7",
              "f": "7"
            },
            "ti": {
              "m": "8",
              "f": "8"
            }
          },
          "2013": {
            "pa": {
              "m": "1",
              "f": "1"
            },
            "ms": {
              "m": "1",
              "f": "1"
            },
            "eg": {
              "m": "1",
              "f": "123"
            },
            "ti": {
              "m": "8",
              "f": "8"
            }
          },
          "2014": {
            "pa": {
              "m": "2",
              "f": "3"
            },
            "ms": {
              "m": "4",
              "f": "5"
            },
            "eg": {
              "m": "2",
              "f": "1"
            },
            "ti": {
              "m": "2",
              "f": "3"
            }
          }
        },
        "cuerpo_academico": {
          "grados_dedicacion": {
          "pt": {
            "ex": "0",
            "tc": "0",
            "tp": "1",
            "tt": "1"
          },
          "pe": {
            "ex": "0",
            "tc": "1",
            "tp": "0",
            "tt": "1"
          },
          "bc": {
            "ex": "1",
            "tc": "0",
            "tp": "1",
            "tt": "2"
          },
          "lt": {
            "ex": "0",
            "tc": "0",
            "tp": "2",
            "tt": "2"
          },
          "mm": {
            "ex": "0",
            "tc": "1",
            "tp": "6",
            "tt": "7"
          },
          "dr": {
            "ex": "2",
            "tc": "0",
            "tp": "9",
            "tt": "11"
          },
          "tt": {
            "ex": "3",
            "tc": "2",
            "tp": "19",
            "tt": "24"
          }
        },
        "grados_condicion": {
          "pt": {
            "no": "0",
            "co": "1",
            "tt": "1"
          },
          "pe": {
            "no": "1",
            "co": "0",
            "tt": "1"
          },
          "bc": {
            "no": "0",
            "co": "2",
            "tt": "2"
          },
          "lt": {
            "no": "2",
            "co": "0",
            "tt": "2"
          },
          "mm": {
            "no": "3",
            "co": "4",
            "tt": "7"
          },
          "dr": {
            "no": "5",
            "co": "6",
            "tt": "11"
          },
          "tt": {
            "no": "11",
            "co": "13",
            "tt": "24"
          }
        },
        "docentes_categoria": {
          "pr": {
            "tt": "0"
          },
          "as": {
            "tt": "2"
          },
          "au": {
            "tt": "2"
          },
          "tt": {
            "tt": "4"
          }
        }
        },
        "inves": {
          "2015": {
            "ti": "asdf",
            "ac": "123",
            "in": "sadf"
          },
          "2011": {
            "ti": "asdf",
            "ac": "123",
            "in": "sadf"
          },
          "2012": {
            "ti": "asdf",
            "ac": "123",
            "in": "sadf"
          },
          "2013": {
            "ti": "asdf",
            "ac": "123",
            "in": "sadf"
          },
          "2014": {
            "ti": "asdf",
            "ac": "123",
            "in": "sadf"
          }
        },
        "publi": {
          "2015": {
            "ti": "qwr",
            "ac": "wu",
            "in": "sfda"
          },
          "2011": {
            "ti": "wri",
            "ac": "qwh",
            "in": "qwery"
          },
          "2012": {
            "ti": "asdfoi",
            "ac": "wer12",
            "in": "eui"
          },
          "2013": {
            "ti": "asdf",
            "ac": "qweui",
            "in": "sfdh"
          },
          "2014": {
            "ti": "123hjhi",
            "ac": "uiuoishdjfh",
            "in": "sdfh"
          }
        },
        "paten": {
          "2015": {
            "ti": "fd",
            "ac": "wwe",
            "in": "wwe"
          },
          "2011": {
            "ti": "efs",
            "ac": "ew",
            "in": "rfd"
          },
          "2012": {
            "ti": "ewry",
            "ac": "rwfd",
            "in": "w"
          },
          "2013": {
            "ti": "wehf",
            "ac": "eru",
            "in": "erh"
          },
          "2014": {
            "ti": "yuir",
            "ac": "wer",
            "in": "ewryh"
          }
        },
        "actividades": {
          "2011": "n123",
          "2012": "n341",
          "2013": "n1212",
          "2014": "n2",
          "2015": "n1"
        },
        "servicios": {
          "2011": "12k3jl",
          "2012": "123jlkjl",
          "2013": "213",
          "2014": "123jsd",
          "2015": "care1"
        },
        "vinculos": [
          {
            "id": "b77b3677-ac15-4097-ae3d-c5ade8eb22b9",
            "ic": "ins",
            "dv": "de"
          }
        ],
        "origen_fondos": {
          "pao": {
            "2015n": "100",
            "2015p": "0.08",
            "2014n": "200",
            "2014p": "37.52"
          },
          "rdr": {
            "2015n": "100",
            "2015p": "0.08",
            "2014n": "200",
            "2014p": "37.52"
          },
          "itd": {
            "2015n": "123123",
            "2015p": "99.83",
            "2014n": "123",
            "2014p": "23.08"
          },
          "otr": {
            "2015n": "10",
            "2015p": "0.01",
            "2014n": "10",
            "2014p": "1.88"
          },
          "tt": {
            "2015n": "123333",
            "2015p": "100.00",
            "2014n": "533",
            "2014p": "100.00"
          }
        },
        "destino_fondos": {
          "s1": {
            "2015n": "12",
            "2015p": "41.38",
            "2014n": "2",
            "2014p": "12.50"
          },
          "s2": {
            "2015n": "3",
            "2015p": "10.34",
            "2014n": "4",
            "2014p": "25.00"
          },
          "bc": {
            "2015n": "5",
            "2015p": "17.24",
            "2014n": "6",
            "2014p": "37.50"
          },
          "ob": {
            "2015n": "7",
            "2015p": "24.14",
            "2014n": "1",
            "2014p": "6.25"
          },
          "ou": {
            "2015n": "2",
            "2015p": "6.90",
            "2014n": "3",
            "2014p": "18.75"
          },
          "tt": {
            "2015n": "29",
            "2015p": "100.00",
            "2014n": "16",
            "2014p": "100.00"
          }
        }
        }
      },
    ],
  };

  if (typeof module !== 'undefined' && module.exports) {
    module.exports = data;
  } else {
    root.__DATA__ = data;
  }
})(this);
