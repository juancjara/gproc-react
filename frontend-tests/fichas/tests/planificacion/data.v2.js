(function (root) {
  var data =
   {
    dim: [
      {
        dbId: 123,
        data: {
          dimeId: '1',
          objetivo: 'Optimizar ..',
        },
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        }
      },
      {
        dbId: 124,
        data: {
          dimeId: '1',
          objetivo: 'Mejorar ..',
        },
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        }
      }
    ],
    fac: [
      {
        dbId: 125,
        parentId: 123,
        data: {
          fact_codigo: '1.1',
          resultado: 'EL IESPP cuenta con presupuesto articulado ...',
          hide: false,
        },
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        }
      },
      {
        dbId: 126,
        parentId: 123,
        data: {
          fact_codigo: '1.3',
          resultado: 'EL IESPP cuenta con mecanismos ...',
          hide: false,
        },
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        }
      },
      {
        dbId: 127,
        parentId: 124,
        data: {
          fact_codigo: '2.3',
          resultado: 'Las practicas evaluativas en las sessiones de aprendizaje osn premanents ...',
          hide: false,
        },
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        }
      },
      {
        dbId: 147,
        parentId: 124,
        data: {
          fact_codigo: '2.4',
          resultado: 'Las practicas evaluativas en la ...',
          hide: false,
        },
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        }
      }
    ],
  act: [
    {
      dbId: 128,
      parentId: 125,
      data: {
        fact_codigo: '1.1',
        id: 'asdasd-asdasdsa-asdasd',
        miactividad: 'Taller de teatro ...',
        actor_cargo: 'Jefe de unidad academica',
        estandares: [
          {
            id: 0,
            esta_id: '3368'
          },
        ],
      },
      totales: {
        "fec":{
          "B":"359.05",
          "R":"220",
          "S":"1463",
        },
        "propio":{
          "B":"49",
          "R":"33",
          "S":"18",
        },
        "plan":{
          "B":"2118.05",
          "R":"26",
          "S":"133",
        },
      },
      "pasantias": '80',
      detalleTotales: {
        "fec": {
          "B1": "82",
          "B2": "12",
          "R1": "123",
          "R2": "123",
          "S1": "123",
          "S2": "123",
        },
        "propio": {
          "B1": "12",
          "B2": "12",
          "R1": "123",
          "R2": "123",
          "S1": "123",
          "S2": "123",
        },
        "plan": {
          "B1": "12",
          "B2": "12",
          "R1": "123",
          "R2": "123",
          "S1": "123",
          "S2": "123",
        }
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      },
    },
    {
      dbId: 129,
      parentId: 125,
      data: {
        fact_codigo: '1.1',
        id: '1sdasd-sdfasdasdsa-asdasd',
        miactividad: 'otra actividad',
        actor_cargo: 'Jefe de unidad academica',
        estandares: [
        ]
      },
      totales: {
        "fec":{
          "B":"65",
          "R":"28",
          "S":"13",
        },
        "propio":{
          "B":"40",
          "R":"80",
          "S":"18",
        },
        "plan":{
          "B":"20",
          "R":"26",
          "S":"13",
        },
      },
      "pasantias": '12',
      detalleTotales: {
        "fec": {
          "B1": "12",
          "B2": "12",
          "R1": "123",
          "R2": "123",
          "S1": "123",
          "S2": "123",
        },
        "propio": {
          "B1": "12",
          "B2": "12",
          "R1": "123",
          "R2": "123",
          "S1": "123",
          "S2": "123",
        },
        "plan": {
          "B1": "12",
          "B2": "12",
          "R1": "123",
          "R2": "123",
          "S1": "123",
          "S2": "123",
        }
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      }
    },
    {
      dbId: 130,
      parentId: 126,
      data: {
        fact_codigo: '1.3',
        id: 'asdasadsfd-sdfasdasdsa-asdasd',
        miactividad: 'adsf',
        actor_cargo: 'Jefe de unidad academica',
        estandares: [
        ]
      },
      totales: {
        "fec":{
          "B":"659",
          "R":"228",
          "S":"131",
        },
        "propio":{
          "B":"49",
          "R":"33",
          "S":"18",
        },
        "plan":{
          "B":"10",
          "R":"26",
          "S":"13",
        },
      },
      "pasantias": '10',
      detalleTotales: {
        "fec": {
          "B1": "12",
          "B2": "2",
          "R1": "123",
          "R2": "123",
          "S1": "423",
          "S2": "123",
        },
        "propio": {
          "B1": "12",
          "B2": "12",
          "R1": "123",
          "R2": "123",
          "S1": "123",
          "S2": "123",
        },
        "plan": {
          "B1": "1850",
          "B2": "12",
          "R1": "123",
          "R2": "123",
          "S1": "123",
          "S2": "123",
        }
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      }
    }
  ],
  fic: {
    dbId: 129,
    data: {
      firmas: {
        com: {
          nombre: 'Garrage Navarro simon',
          cargo: 'Jefe de acreditacion',
          date: '2016-02-11',
          date_local: '11 de febrero del 2016'
        },
        dir: {
          nombre: 'Yris Olivia Curay Ochoa',
          cargo: 'Directora General',
          date: '2016-02-11',
          date_local: '11 de febrero del 2016'
        }
      }
    },
    status: {
      errors: [],
      status: 'STORED',
      hasChanged: false,
    }
  },
  estandares: [
    {
      index: 0,
      esta_id: '3368',
      tipo: 'Obligatorios',
      fact_codigo: '1.1',
      display: '1.1.3. Coherencia presupuestal',
      count: 1
    },
    {
      index: 1,
      esta_id: '3380',
      tipo: 'Obligatorios',
      fact_codigo: '2.3',
      display: '1.4.1. Implementacion del prespuesto',
      count: 0
    },
    {
      index: 2,
      esta_id: '3381',
      tipo: 'Obligatorios',
      fact_codigo: '1.1',
      display: '1.1.1. algo',
      count: 0
    },
    {
      index: 3,
      esta_id: '3382',
      tipo: 'Opcionales',
      fact_codigo: '1.3',
      display: '1.4.2. algo mas',
      count: 0
    }
    ],
  };

  if (typeof module !== 'undefined' && module.exports) {
    module.exports = data;
  } else {
    root.__DATA__ = data;
  }
})(this);

