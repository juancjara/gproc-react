import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import factores from '../../src/reducers/planificacion/factores';
import {
  updateFactor,
} from '../../src/actions/planificacion/aFactores';

import data from './data.v2';
const initialState = data.fac;

describe('factores', () => {
  it('update field', () => {
    const expected = _.cloneDeep(initialState);
    expected[2].data.resultado = 'test 2';
    expected[2].status.hasChanged = true;

    const result = factores(
      fromJS(initialState),
      updateFactor(2, 'resultado', 'test 2')
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
