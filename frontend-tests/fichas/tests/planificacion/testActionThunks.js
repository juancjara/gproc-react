import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import {
  fromJS,
} from 'immutable';

import data from './data.v2';

import {
  removeActividad,
} from '../../src/actions/planificacion/aActividades';

import {
  selectEstandar,
} from '../../src/actions/planificacion/actividad/aActividadesEstandares';

import actions from '../../src/constants/planificacion';
const {
  ACTIVIDAD_ESTANDAR_UPDATE,
  ACTIVIDAD_REMOVE,
  ESTANDAR_DECREMENT,
  ESTANDAR_INCREMENT,
} = actions;

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('action creator thunks', () => {
  it.skip('remove sede and estandares', (done) => {
    const expectedActions = [
      {
        type: ESTANDAR_DECREMENT,
        estandar: 3435,
      },
      {
        type: ESTANDAR_DECREMENT,
        estandar: 3419,
      },
      {
        type: ACTIVIDAD_REMOVE,
        index: 0,
      },
    ];

    const store = mockStore(fromJS(data), expectedActions, done);
    store.dispatch(removeActividad(0));
  });

  it.skip('select estandar', (done) => {
    const expectedActions = [
      {
        type: ESTANDAR_DECREMENT,
        estandar: 3435,
      },
      {
        type: ESTANDAR_INCREMENT,
        estandar: 1234,
      },
      {
        type: ACTIVIDAD_ESTANDAR_UPDATE,
        actividadIndex: 0,
        estandarIndex: 0,
        estandar: 1234,
      },
    ];

    const store = mockStore(fromJS(data), expectedActions, done);
    store.dispatch(selectEstandar(0, 0, 1234));
  });
});
