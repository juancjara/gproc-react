export default {
  dimensiones: [
    {
      dbId: 123,
      data: {
        dimeId: '1',
        objetivo: 'Optimizar ..',
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      }
    },
    {
      dbId: 124,
      data: {
        dimeId: '1',
        objetivo: 'Mejorar ..',
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      }
    }
  ],
  factores: [
    {
      dbId: 125,
      data: {
        factId: '1.1',
        parentId: 123,
        resultado: 'EL IESPP cuenta con presupuesto articulado ...',
        hide: '',
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      }
    },
    {
      dbId: 126,
      data: {
        factId: '1.3',
        parentId: 123,
        resultado: 'EL IESPP cuenta con mecanismos ...',
        hide: '',
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      }
    },
    {
      dbId: 127,
      data: {
        factId: '2.3',
        parentId: 124,
        resultado: 'Las practicas evaluativas en las sessiones de aprendizaje osn premanents ...',
        hide: '',
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      }
    }
  ],
  actividades: [
    {
      dbId: 128,
      data: {
        id: 'asdasd-asdasdsa-asdasd',
        parentId: '1.1',
        miactividad: 'Taller de teatro ...',
        actor_cargo: 'Jefe de unidad academica',
        estandares: [
          {
            id: 0,
            estandar: 3435
          },
          {
            id: 1,
            estandar: 3419
          },
        ]
      }
    }
  ],
  fic: {
    com: {
      nombre: 'Garrage Navarro simon',
      cargo: 'Jefe de acreditacion',
      date: '2016-02-11',
      date_local: '11 de febrero del 2016'
    },
    dir: {
      nombre: 'Yris Olivia Curay Ochoa',
      cargo: 'Directora General',
      date: '2016-02-11',
      date_local: '11 de febrero del 2016'
    }
  },
  estandares: {
    tipo: 'Obligatorios',
    children: [
      {
        dimension_id: 682,
        children: [
          {
            id: 3368,
            text: '1.1.3. Coherencia presupuestal',
            count: 0
          },
          {
            id: 3379,
            text: '1.3.6. Aplicacion de competencias adquiridas',
            count: 0
          },
          {
            id: 3380,
            text: '1.4.1. Implementacion del prespuesto',
            count: 0
          }
        ]
      }
    ]
  }
};
