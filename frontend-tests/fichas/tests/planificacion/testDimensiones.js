import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import dimensiones from '../../src/reducers/planificacion/dimensiones';
import {
  updateDimension,
} from '../../src/actions/planificacion/aDimensiones';

import data from './data.v2';
const initialState = data.dim;

describe('dimensiones', () => {
  it('update field', () => {
    const expected = _.cloneDeep(initialState);
    expected[1].data.objetivo = 'test';
    expected[1].status.hasChanged = true;

    const result = dimensiones(
      fromJS(initialState),
      updateDimension(1, 'objetivo', 'test')
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
