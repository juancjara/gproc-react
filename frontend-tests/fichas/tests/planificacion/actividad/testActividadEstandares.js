import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import actividadEstandares
  from '../../../src/reducers/planificacion/actividad/actividadEstandares';
import {
  addEstandar,
  _removeEstandar,
  _selectEstandar,
} from '../../../src/actions/planificacion/actividad/aActividadesEstandares';

import data from '../data.v2';

describe('Actividad estandares', () => {
  let initialState;
  let expected;

  beforeEach(() => {
    initialState = _.cloneDeep(data.act[0].data.estandares);
    expected = _.cloneDeep(initialState);
  });

  it('add estandar', () => {
    expected.push({
      id: null,
      esta_id: -1,
    });

    const result = actividadEstandares(
      fromJS(initialState),
      addEstandar(0)
    );

    assert.equal(result.toJS().length, expected.length);
    assert.equal(result.toJS().pop().esta_id, expected.pop().esta_id)
  });

  it('remove estandar', () => {
    expected.shift();

    const result = actividadEstandares(
      fromJS(initialState),
      _removeEstandar(0, 0)
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('update estandar', () => {
    expected[0].esta_id = 1234;

    const result = actividadEstandares(
      fromJS(initialState),
      _selectEstandar(0, 0, 1234)
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
