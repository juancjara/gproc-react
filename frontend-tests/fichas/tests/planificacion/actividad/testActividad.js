import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import actividad from '../../../src/reducers/planificacion/actividad/actividad';
import {
  updateActividad,
} from '../../../src/actions/planificacion/actividad/aActividad';

import data from '../data.v2';

describe('actividad', () => {
  let initialState;
  let expected;

  beforeEach(() => {
    initialState = _.cloneDeep(data.act);
    expected = _.cloneDeep(initialState);
  });

  it('update field', () => {
    expected = expected[0].data;
    expected.miactividad = 'testerino';

    const result = actividad(
      fromJS(initialState[0].data),
      updateActividad(0, 'miactividad', 'testerino')
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
