(function (root) {
  var data =
    {
      dim: [
        {
          dbId: 123,
          data: null,
          status: {
            errors: [],
            status: 'STORED',
            hasChanged: false,
          }
        },
        {
          dbId: 124,
          data: null,
          status: {
            errors: [],
            status: 'STORED',
            hasChanged: false,
          }
        }
      ],
      fac: [
        {
          dbId: 125,
          data: null,
          parent_id: 123,
        },
        {
          dbId: 126,
          data: null,
          parent_id: 123,
        },
        {
          dbId: 127,
          data: null,
          parent_id: 124,
        },
        {
          dbId: 147,
          data: null,
          parent_id: 124,
        }
      ],
      act: [
        {
dbId: 150,
      parent_id: 125,
      data: {
        fact_codigo: '1.1',
        id: 'asdasadsfd-sdfasdasdsa-asdasd',
        miactividad: 'adsf',
        actor_cargo: 'Jefe de unidad academica',
        estandares: [
        ]
      },
      "pasantias": '10',
      detalleTotales: {
        "fec": {
          "B1": "2",
          "B2": "2",
          "R1": "123",
          "R2": "123",
          "S1": "423",
          "S2": "123",
        },
        "propio": {
          "B1": "12",
          "B2": "12",
          "R1": "123",
          "R2": "123",
          "S1": "123",
          "S2": "123",
        },
        "plan": {
          "B1": "1850",
          "B2": "12",
          "R1": "123",
          "R2": "123",
          "S1": "123",
          "S2": "123",
        }
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      }
    }
      ],
      fic: {
        dbId: 'gg',
        data: null,
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        }
      },
      requestType: '',
  };

  if (typeof module !== 'undefined' && module.exports) {
    module.exports = data;
  } else {
    root.__DATA__ = data;
  }
})(window);
