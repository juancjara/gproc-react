import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import status from '../../src/reducers/status';
import actividades from '../../src/reducers/planificacion/actividades';
import {
  _addActividad,
  _removeActividad,
} from '../../src/actions/planificacion/aActividades';

import {
  updateActividad,
} from '../../src/actions/planificacion/actividad/aActividad';

import {
  addEstandar,
  _removeEstandar,
  _selectEstandar,
} from '../../src/actions/planificacion/actividad/aActividadesEstandares';

import data from './data.v2';

import { setContext } from '../../src/context';
setContext({"catalogo_titles":[{"id":"B","name":"B__ asdf","short_name":"B__","parent_id":null,"data":null},{"id":"R","name":"R__ asdf","short_name":"R__","parent_id":null,"data":null},{"id":"S","name":"S__ asf","short_name":"S__","parent_id":null,"data":null}],"catalogo_subtitles":[{"id":"B1","name":"B1__","short_name":"B1__","parent_id":"B","data":null},{"id":"B2","name":"B2__","short_name":"B2__","parent_id":"B","data":null},{"id":"R1","name":"R1__","short_name":"R1__","parent_id":"R","data":null},{"id":"R2","name":"R2__","short_name":"R2__","parent_id":"R","data":null},{"id":"S1","name":"S1__","short_name":"S1__","parent_id":"S","data":null},{"id":"S2","name":"S2__","short_name":"S2__","parent_id":"S","data":null}],"estandares":[{"display":"3.    El desarrollo del plan estratégico se evalúa anualmente","esta_id":"4055","tipo":"Obligatorios","fact_codigo":"1.3"},{"display":"4.    Más del 75% de estudiantes, docentes y administrativos conoce el plan estratégico","esta_id":"4056","tipo":"Obligatorios","fact_codigo":"1.3"},{"display":"5.    El plan estratégico tiene políticas orientadas al aseguramiento de la calidad en la carrera profesional","esta_id":"4057","tipo":"Obligatorios","fact_codigo":"1.1"},{"display":"6.    La Universidad tiene normas sobre organización y funciones y la Unidad Académica el manual correspondiente para su aplicación","esta_id":"4058","tipo":"Obligatorios","fact_codigo":"1.1"},{"display":"7.    Las actividades académicas y administrativas están coordinadas para asegurar el desarrollo del proyecto educativo","esta_id":"4059","tipo":"Obligatorios","fact_codigo":"1.1"},{"display":"8.    La Unidad Académica tiene un sistema de gestión de la calidad implementado","esta_id":"4060","tipo":"Obligatorios","fact_codigo":"1.1"},{"display":"9.    La Unidad Académica tiene un programa implementado que contribuye a internalizar la cultura organizacional en los estudiantes, docentes y administrativos de la carrera profesional","esta_id":"4061","tipo":"Obligatorios","fact_codigo":"1.1"},{"display":"10. La Unidad Académica tiene un sistema implementado de información y comunicación","esta_id":"4062","tipo":"Obligatorios","fact_codigo":"1.3"},{"display":"11. El plan operativo de la carrera de Economía es elaborado con la participación de representantes de los docentes, estudiantes, egresados y de otros grupos de interés","esta_id":"4063","tipo":"Obligatorios","fact_codigo":"1.3"},{"display":"12. El desarrollo del plan operativo se evalúa para determinar las acciones correctivas correspondientes","esta_id":"4064","tipo":"Obligatorios","fact_codigo":"1.1"},{"display":"13. Más del 75% de estudiantes, docentes y administrativos conoce el plan operativo","esta_id":"4065","tipo":"Obligatorios","fact_codigo":"1.3"},{"display":"15.    Se justifica la existencia de la carrera profesional en base a un estudio de la demanda social","esta_id":"4067","tipo":"Obligatorios","fact_codigo":"2.3"},{"display":"16.    Los perfiles del ingresante y del egresado guardan coherencia con los lineamientos del proyecto educativo","esta_id":"4068","tipo":"Obligatorios","fact_codigo":"2.4"},{"display":"20.    El plan de estudios tiene un número de horas teóricas y prácticas que asegura el logro del perfil del egresado","esta_id":"4072","tipo":"Obligatorios","fact_codigo":"2.3"},{"display":"21.    El plan de estudios tiene una secuencia de asignaturas, o cursos, que fortalece el proceso enseñanza-aprendizaje","esta_id":"4073","tipo":"Obligatorios","fact_codigo":"2.4"},{"display":"22.    El plan de estudios vincula los procesos de enseñanza-aprendizaje con los procesos de investigación, extensión universitaria y proyección social","esta_id":"4074","tipo":"Obligatorios","fact_codigo":"2.3"}],"factores":[{"display":"1. Planificación, Organización, Dirección y Control","dime_codigo":"1","fact_codigo":"1.1"},{"display":"2. Enseñanza - aprendizaje","dime_codigo":"1","fact_codigo":"1.3"},{"display":"3. asdfjklasdjlfj","dime_codigo":"2","fact_codigo":"2.3"},{"display":"4. asdf","dime_codigo":"2","fact_codigo":"2.4"}],"dimensiones":[{"display":"1. Gestión de la Carrera","dime_codigo":"1"},{"display":"2. Formación Profesional","dime_codigo":"2"}],"canEdit":true,"canAddOrRemove":true,"date":"2016-03-14","date_local":"14 de Marzo del 2016"});

describe('actividades', () => {
  let initialState;
  let expected;

  beforeEach(() => {
    initialState = _.cloneDeep(data.act);
    expected = _.cloneDeep(initialState);
  });

  it('add actividad', () => {
    const id = '123';
    const factorId = '4321';

    const actionAddActividad = _addActividad(id, factorId)

    expected.push({
      dbId: undefined,
      data: {
        id: actionAddActividad.id,
        fact_codigo: factorId,
        miactividad: '',
        actor_cargo: '',
        estandares: [],
      },
      parentId: id,
      status: status(undefined, {}).toJS(),
      "detalleTotales": {
        "fec": {
          "B1": "0",
          "B2": "0",
          "R1": "0",
          "R2": "0",
          "S1": "0",
          "S2": "0",
        },
        "plan": {
          "B1": "0",
          "B2": "0",
          "R1": "0",
          "R2": "0",
          "S1": "0",
          "S2": "0",
        },
        "propio": {
          "B1": "0",
          "B2": "0",
          "R1": "0",
          "R2": "0",
          "S1": "0",
          "S2": "0",
        },
      },
      pasantias: "0",
      "totales": {
        "fec": {
          "B": "0",
          "R": "0",
          "S": "0",
        },
        "plan": {
          "B": "0",
          "R": "0",
          "S": "0",
        },
        "propio": {
          "B": "0",
          "R": "0",
          "S": "0",
        },
      },
    });

    const result = actividades(
      fromJS(initialState),
      actionAddActividad
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('remove actividad', () => {
    expected.shift();

    const result = actividades(
      fromJS(initialState),
      _removeActividad(0)
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('update actividad', () => {
    expected[0].data.miactividad = 'test';
    expected[0].status.hasChanged = true;

    const result = actividades(
      fromJS(initialState),
      updateActividad(0, 'miactividad', 'test')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('add estandar', () => {
    const estandaresLength = expected[0].data.estandares.length;
    expected[0].data.estandares.push({
      id: null,
      esta_id: -1,
    });
    expected[0].status.hasChanged = true;

    let result = actividades(
      fromJS(initialState),
      addEstandar(0)
    );

    result = result.toJS();
    result[0].data.estandares[estandaresLength].id = null;

    assert.deepEqual(result, expected);
  });

  it('remove estandar', () => {
    expected[0].data.estandares.shift();
    expected[0].status.hasChanged = true;

    const result = actividades(
      fromJS(initialState),
      _removeEstandar(0, 0)
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('update estandar', () => {
    expected[0].data.estandares[0].esta_id = 123;
    expected[0].status.hasChanged = true;

    const result = actividades(
      fromJS(initialState),
      _selectEstandar(0, 0, 123)
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
