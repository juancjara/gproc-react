import uuid from 'node-uuid';
import express from 'express';
import bodyParser from 'body-parser';
import _ from 'lodash';

const app = new express();
const port = 5000;

app.use(bodyParser.json());

const timeout = 2000;

app.post('/ApiCronograma/storeNodos', (req, res) => {
  console.log(req.params, req.body);
  console.log(JSON.stringify(req.body));
  // return res.send(502);
  // return res.send(401);
  const dbIds = [];
  for (var i = 0; i < req.body.length; i++) {
    dbIds.push({ dbId: i });
  }
  setTimeout(() => {
    res.send({
      statusText: 'OK',
      ids: dbIds,
    });
  }, timeout);
});

app.post('/ApiEfectos/storeNodos', (req, res) => {
  console.log(req.params, req.body);
  console.log(JSON.stringify(req.body));
  // return res.send(502);
  // return res.send(401);
  const dbIds = [];
  for (var i = 0; i < req.body.length; i++) {
    dbIds.push({ dbId: i });
  }
  setTimeout(() => {
    res.send({
      statusText: 'OK',
      ids: dbIds,
    });
  }, timeout);
});

app.post('/ApiMonitoreo/addIndicador', (req, res) => {
  console.log(req.params, req.body);
  console.log(JSON.stringify(req.body));

  setTimeout(() => {
    res.send({
      statusText: 'OK',
      dbId: uuid.v4(),
    });
  }, timeout);
});

app.post('/ApiMonitoreo/removeIndicador', (req, res) => {
  console.log(req.params, req.body);
  console.log(JSON.stringify(req.body));
  setTimeout(() => {
    res.send({
      statusText: 'OK',
    });
  }, timeout);
});

app.post('/ApiMonitoreo/storeNodos', (req, res) => {
  console.log(req.params, req.body);
  console.log(JSON.stringify(req.body));
  // res.send(502);
  // res.send(401);
  const dbIds = [];
  for (var i = 0; i < req.body.length; i++) {
    dbIds.push({ dbId: i });
  }
  setTimeout(() => {
    res.send({
      statusText: 'OK',
      ids: dbIds,
    });
  }, timeout);
});

app.post('/ApiPresupuesto/storeNodos', (req, res) => {
  console.log(req.params, req.body);
  console.log(JSON.stringify(req.body));
  // res.send(502);
  // res.send(401);
  const dbIds = [];
  for (var i = 0; i < req.body.length; i++) {
    dbIds.push({ dbId: i });
  }
  setTimeout(() => {
    res.send({
      statusText: 'OK',
      ids: dbIds,
    });
  }, timeout);
});

app.post('/ApiPresupuesto/addSubActividad', (req, res) => {
  console.log(req.params, req.body);
  console.log(JSON.stringify(req.body));
  // res.send(502);
  // res.send(401);
  const dbIds = [];
  for (var i = 0; i < req.body.length; i++) {
    dbIds.push({
      dbId: req.body[i].dbId || _.random(100, 9999)
    });
    // if (req.body[i].dbId === null) {
    //   dbIds.push({ dbId: req.body[i].dbId})
    // }
    // dbIds.push({ dbId: req.body[] });
  }
  setTimeout(() => {
    res.send({
      statusText: 'OK',
      ids: dbIds,
    });
  }, timeout);
});

app.post('/ApiPresupuesto/removeSubActividad', (req, res) => {
  console.log(req.params, req.body);
  // res.send(401);
  setTimeout(() => {
    res.send({
      statusText: 'OK',
    });
  }, timeout);
});



app.post('/ApiPlanificacion/storeNodos', (req, res) => {
  console.log('here');
  console.log(req.params, req.body);
  console.log(JSON.stringify(req.body));
  // res.send(502);
  // res.send(401);
  const dbIds = [];
  for (var i = 0; i < req.body.length; i++) {
    dbIds.push({ dbId: i });
  }
  setTimeout(() => {
    res.send({
      statusText: 'OK',
      ids: dbIds,
    });
  }, timeout);
});

app.post('/ApiPlanificacion/addActividad', (req, res) => {
  console.log(req.params, req.body);
  console.log(JSON.stringify(req.body));
  // res.send(502);
  // res.send(401);
  const dbIds = [];
  for (var i = 0; i < req.body.length; i++) {
    dbIds.push({ dbId: i });
  }
  setTimeout(() => {
    res.send({
      statusText: 'OK',
      ids: dbIds,
    });
  }, timeout);
});

app.post('/ApiPlanificacion/removeActividad', (req, res) => {
  console.log(req.params, req.body);
  // res.send(401);
  setTimeout(() => {
    res.send({ dbId: 1234});
  }, timeout);
});

app.post('/ApiExpInteres/addCarrera', (req, res) => {
  console.log(req.params, req.body);
  // res.send(401);
  res.send({
    data: [
      {dbId: 0},
      {dbId: 1},
    ]
  });
});

app.post('/ApiExpInteres/removeCarrera', (req, res) => {
  console.log(req.params, req.body);
  setTimeout(() => {
    res.send({
      dbId: 0,
    });
  }, timeout);
});

app.post('/ApiExpInteres/removeSede', (req, res) => {
  console.log(req.params, req.body);
  setTimeout(() => {
    res.send({
      dbId: 0,
    });
  }, timeout);
});

app.post('/ApiExpInteres/storeNodo', (req, res) => {
  console.log(req.params, req.body);
  setTimeout(() => {
    res.send({
      dbId: 0,
    });
  }, timeout);
});

app.listen(port, (err) => {
  if (err) return console.log(err);
  return console.log('listening on %s port', port);
});
