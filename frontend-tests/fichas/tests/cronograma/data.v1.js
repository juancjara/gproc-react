(function (root) {
  var data = {
    cro: [
      {
        dbId: '1',
        parent_id: '199',
        // data: {
        //   start: 0,
        //   duration: 1,
        // },
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      },
      {
        dbId: '2',
        parent_id: '200',
        data: {
          start: 2,
          duration: 8,
        },
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      },
      {
        dbId: '3',
        parent_id: '201',
        data: {
          start: 1,
          duration: 5,
        },
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      },
    ],
    fic: {
      dbId: 129,
      data: {
        firmas: {
          com: {
            nombre: 'Garrage Navarro simon',
            cargo: 'Jefe de acreditacion',
            date: '2016-02-11',
            date_local: '11 de febrero del 2016'
          },
          dir: {
            nombre: 'Yris Olivia Curay Ochoa',
            cargo: 'Directora General',
            date: '2016-02-11',
            date_local: '11 de febrero del 2016'
          }
        }
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      },
    },
  };

  if (typeof module !== 'undefined' && module.exports) {
    module.exports = data;
  } else {
    root.__DATA__ = data;
  }
})(this);
