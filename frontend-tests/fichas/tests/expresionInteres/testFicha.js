import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import ficha from '../../src/reducers/expresionInteres/ficha';

import {
  fichaUpdate,
  fichaUpdateInst,
  fichaUpdateCarr,
} from '../../src/actions/expresionInteres/aFicha';

import { fic } from '../data.v2';
const initialState = fic.data;

describe('Ficha: update fields', () => {
  it('update field', () => {
    const expected = _.cloneDeep(initialState);
    expected.tifi_id = '1';

    const result = ficha(
      fromJS(initialState),
      fichaUpdate('tifi_id', '1')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('update field inst', () => {
    const expected = _.cloneDeep(initialState);
    expected.datos.inst.nombre = 'Martin';

    const result = ficha(
      fromJS(initialState),
      fichaUpdateInst('nombre', 'Martin')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('update field carr', () => {
    const expected = _.cloneDeep(initialState);
    expected.datos.carr.cargo = 'Secretario';

    const result = ficha(
      fromJS(initialState),
      fichaUpdateCarr('cargo', 'Secretario')
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
