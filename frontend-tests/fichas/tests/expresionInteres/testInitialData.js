import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import carrera from '../../src/reducers/expresionInteres/carrera/carrera';

import {
  createBaseStructure,
  anyMissingNode,
  fillMissingNodes,
  shouldAddOneCarrera,
  addCarrera,
} from '../../src/initialData/expresionInteres';

describe('initial data', () => {
  let context;

  beforeEach(() => {
    context = {"editable":true, "carr_nombre": "EDUCACION PRIMARIA","aTipoFinanciamiento":[{"id":"2","text":"Plan de Mejora de Carreras"},{"id":"4","text":"Evaluaci\u00f3n Externa de Carrera"}],"institucional":true,"universidad":true,"aPersona":[{"pers_id":"911","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Wildo","pers_apellidos":"Condor\u00ed","pers_cargo":"Presidente","pers_telefono":"996500961","pers_correo":"widowilliam@hotmail.com","pers_tipo":"contacto","acre_id":"220"},{"pers_id":"912","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Nilton","pers_apellidos":"Mayta","pers_cargo":"Secretario","pers_telefono":"951771202","pers_correo":"nicemaja78@hotmail.com","pers_tipo":"comision","acre_id":"220"},{"pers_id":"913","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Ofelia","pers_apellidos":"Mamani","pers_cargo":"Docente","pers_telefono":"951565014","pers_correo":"sumayaofely@hotmail.com","pers_tipo":"comision","acre_id":"220"}],"moda_nombre":"En proceso de acreditaci\u00f3n","inst_nombre":"UNIVERSIDAD NACIONAL DEL ALTIPLANO","inst_gestion":"PUBLICA", "aPersona":[{"pers_id":"911","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Wildo","pers_apellidos":"Condor\u00ed","pers_cargo":"Presidente","pers_telefono":"996500961","pers_correo":"widowilliam@hotmail.com","pers_tipo":"contacto","acre_id":"220"},{"pers_id":"912","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Nilton","pers_apellidos":"Mayta","pers_cargo":"Secretario","pers_telefono":"951771202","pers_correo":"nicemaja78@hotmail.com","pers_tipo":"comision","acre_id":"220"},{"pers_id":"913","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Ofelia","pers_apellidos":"Mamani","pers_cargo":"Docente","pers_telefono":"951565014","pers_correo":"sumayaofely@hotmail.com","pers_tipo":"comision","acre_id":"220"}]};
  });

  it('data undefined using only root reducer', () => {
    const data = createBaseStructure(undefined, context);
    let expected = {"fic":{"dbId":null,"data":{"tifi_id":"","datos":{"inst":{"fijo":"","nombre":"","emailalt":"","date":"","direccion":"","celular":"","date_local":"","email":"","cargo":""},"carr":{"fijo":"","nombre":"","emailalt":"","date":"","direccion":"","celular":"","date_local":"","email":"","cargo":""}}},"status":{"status":"STORED","hasChanged":false,"errors":[]}},"ins":{"dbId":null,"data":{"comite":[],"ruc":"","sedes":[{"id": 0, "nombre_sede":"Sede Principal","direc":"","dist":"","prov":"","region":"","carreras":[]}],"cuerpo_academico":{"grados_dedicacion":{"pt":{"ex":"","tc":"","tp":"","tt":""},"pe":{"ex":"","tc":"","tp":"","tt":""},"bc":{"ex":"","tc":"","tp":"","tt":""},"lt":{"ex":"","tc":"","tp":"","tt":""},"mm":{"ex":"","tc":"","tp":"","tt":""},"dr":{"ex":"","tc":"","tp":"","tt":""},"tt":{"ex":"","tc":"","tp":"","tt":""}},"grados_condicion":{"pt":{"no":"","co":"","tt":""},"pe":{"no":"","co":"","tt":""},"bc":{"no":"","co":"","tt":""},"lt":{"no":"","co":"","tt":""},"mm":{"no":"","co":"","tt":""},"dr":{"no":"","co":"","tt":""},"tt":{"no":"","co":"","tt":""}},"docentes_categoria":{"pr":{"tt":""},"as":{"tt":""},"au":{"tt":""},"tt":{"tt":""}}},"total_estudiantes":{"p_admision":[],"m_semestre":[],"egresados":[],"titulados":[]}},"status":{"status":"STORED","hasChanged":false,"errors":[]}},"car":[]};

    assert.deepEqual(data.toJS(), expected);
  });

  it('any node missing', () => {
    const expected = true;
    const res = anyMissingNode(['fic', 'ins', 'car'],
                               fromJS({ fic: '',ins: ''}));

    assert.equal(res, expected);
  });

  it('fill missing node: ficha', () => {
    const expected = {
      fic: null,
      car: {},
      ins: {},
    };

    const res = fillMissingNodes(['fic', 'ins', 'car'],
                                 fromJS(expected));

     expected.fic = {"dbId":null,"data":{"tifi_id":"","datos":{"inst":{"fijo":"","nombre":"","emailalt":"","date":"","direccion":"","celular":"","date_local":"","email":"","cargo":""},"carr":{"fijo":"","nombre":"","emailalt":"","date":"","direccion":"","celular":"","date_local":"","email":"","cargo":""}}},"status":{"status":"STORED","hasChanged":false,"errors":[]}};

    assert.deepEqual(res.toJS(), expected);
  });

  it('should add one carrera', () => {
    context.institucional = false;
    const expected = {
      fic: {},
      ins: {},
      car: {},
    };
    const res = shouldAddOneCarrera(context, fromJS(expected));

    assert.equal(res, true);
  });

  it ('add carrera', () => {
    const res = addCarrera(fromJS(), context);
    const expected = [carrera(undefined, {}).toJS()];

    assert.deepEqual(res.toJS(), expected);
  });
});
