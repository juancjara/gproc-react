import {
  fromJS,
} from 'immutable';
import thunk from 'redux-thunk';

import data from '../data.v2';

import {
  addSede,
  removeSede,
} from '../../src/actions/expresionInteres/institucional/aSedes';

import {
  addSedeCarrera,
} from '../../src/actions/expresionInteres/institucional/aSedeCarreras.js';

import actions from '../../src/constants/expresionInteres';
const {
  INSTITUCIONAL_SEDE_ADD,
  INSTITUCIONAL_SEDE_REMOVE,
  INSTITUCIONAL_SEDE_CARRERA_ADD,
  INSTITUCIONAL_SEDE_CARRERA_REMOVE,
} = actions;

import configureStore from 'redux-mock-store';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('actions creators thunks', () => {
  it('add sede and carrera, institucional = false', (done) => {
    const expectedActions = [
      {
        type: INSTITUCIONAL_SEDE_ADD,
        sedeId: 1,
        name: 'Filial 1',
      },
      {
        type: INSTITUCIONAL_SEDE_CARRERA_ADD,
        sedeIndex: 1,
        sedeName: 'Filial 1',
        carreraId: 1000,
        nombre: '',
        area: '',
      },
    ];
    const store = mockStore(fromJS(data), expectedActions, done);
    store.dispatch(addSede(false));
  });

  it.skip('add sede and carrera, institucional = true', (done) => {
    const expectedActions = [
      {
        type: INSTITUCIONAL_SEDE_ADD,
        name: 'Filial 1',
      },
      {
        type: INSTITUCIONAL_SEDE_CARRERA_ADD,
        sedeId: 1,
        sedeName: 'Filial 1',
        carreraId: 1000,
        nombre: '',
        area: '',
      },
    ];
    const store = mockStore(fromJS(data), expectedActions, done);
    store.dispatch(addSede(true));
  });

  it('add carrera institucional false', (done) => {
    const expectedActions = [
      {
        type: INSTITUCIONAL_SEDE_CARRERA_ADD,
        sedeIndex: 0,
        sedeName: 'Sede principal',
        carreraId: 4,
        nombre: 'test',
        area: 'area',
      },
    ];
    const store = mockStore(fromJS(data), expectedActions, done);
    store.dispatch(addSedeCarrera(0, false, 'test', 'area'));
  });

  it.skip('remove sede and carreras institucional = false', (done) => {
    const expectedActions = [];
    for (let i = 0; i < 3; i++) {
      expectedActions.push({
        type: INSTITUCIONAL_SEDE_CARRERA_REMOVE,
        sedeId: 0,
        carreraId: i,
      });
    }

    expectedActions.push({
      type: INSTITUCIONAL_SEDE_REMOVE,
      id: 0,
    });

    const store = mockStore(fromJS(data), expectedActions, done);
    store.dispatch(removeSede(0, false));
  });
});
