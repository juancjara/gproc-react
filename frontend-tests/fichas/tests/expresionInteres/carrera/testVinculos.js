import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import vinculos from '../../../src/reducers/expresionInteres/carrera/vinculos';

import {
  vinculosAdd,
  vinculosUpdate,
  vinculosRemove,
} from '../../../src/actions/expresionInteres/carrera/aVinculos';

import { car as initialState } from '../../data.v2';

describe('Carrera: vinculos interinstitucionales', () => {
  it('vinculos add', () => {
    const result = vinculos(
      fromJS(initialState[0].data.vinculos),
      vinculosAdd(0)
    );

    const newRow = {
      id: result.last().get('id'),
      ic: '',
      dv: '',
    };

    const expected = _.cloneDeep(initialState[0].data.vinculos);
    expected.push(newRow);

    assert.deepEqual(result.toJS(), expected);
  });

  it('vinculos update', () => {
    const expected = _.cloneDeep(initialState[0].data.vinculos);
    expected[0].ic = 'new value';

    const result = vinculos(
      fromJS(initialState[0].data.vinculos),
      vinculosUpdate(0, 0, 'ic', 'new value')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('vinculos update', () => {
    const expected = _.cloneDeep(initialState[0].data.vinculos);
    expected.pop();

    const result = vinculos(
      fromJS(initialState[0].data.vinculos),
      vinculosRemove(0, 0)
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
