import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import publicacion from '../../../src/reducers/expresionInteres/carrera/publicacion';

import {
  publicacionesUpdate,
} from '../../../src/actions/expresionInteres/carrera/aPublicaciones';

import { car as initialState } from '../../data.v2';

describe('Carrera investigaciones, publicaciones y patentes', () => {
  it('update field publicacion', () => {
    const expected = _.cloneDeep(initialState[0].data.publi);
    expected['2014'].ti = 'test';

    const result = publicacion(
      fromJS(initialState[0].data.publi),
      publicacionesUpdate(0, 'publi', '2014', 'ti', 'test')
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
