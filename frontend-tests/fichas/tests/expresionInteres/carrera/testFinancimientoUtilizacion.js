import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import financimientoUtilizacion
  from '../../../src/reducers/expresionInteres/carrera/financiamientoUtilizacion';

import {
  financiamientoUtilizacionUpdate,
} from '../../../src/actions/expresionInteres/carrera/aFinanciamiento';

import { car } from '../../data.v2';

describe('Carrera financiamiento utilizacion', () => {
  let pureState;
  let expected;

  beforeEach(() => {
    expected = _.cloneDeep(car[0].data);
    pureState = fromJS(car[0].data);
  });

  it('origin fondos update', () => {
    expected.origen_fondos.pao['2014n'] = '100';
    expected.origen_fondos.pao['2014p'] = '23.09';
    expected.origen_fondos.rdr['2014p'] = '46.19';
    expected.origen_fondos.itd['2014p'] = '28.41';
    expected.origen_fondos.otr['2014p'] = '2.31';
    expected.origen_fondos.tt['2014n'] = '433';

    const result = financimientoUtilizacion(
      pureState.get('origen_fondos'),
      financiamientoUtilizacionUpdate(0, 'origen_fondos', 'pao', '2014n', '100')
    );

    assert.deepEqual(result.toJS(), expected.origen_fondos);
  });

  it('destino fondos update', () => {
    expected.destino_fondos.s2['2015n'] = '20';
    expected.destino_fondos.tt['2015n'] = '46';
    expected.destino_fondos.s1['2015p'] = '26.09';
    expected.destino_fondos.s2['2015p'] = '43.48';
    expected.destino_fondos.bc['2015p'] = '10.87';
    expected.destino_fondos.ob['2015p'] = '15.22';
    expected.destino_fondos.ou['2015p'] = '4.35';

    const result = financimientoUtilizacion(
      pureState.get('destino_fondos'),
      financiamientoUtilizacionUpdate(0, 'destino_fondos', 's2', '2015n', '20')
    );

    assert.deepEqual(result.toJS(), expected.destino_fondos);
  });
});
