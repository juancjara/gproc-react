import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import actividadesServicios
from '../../../src/reducers/expresionInteres/carrera/actividadesServicios';

import {
  actividadesServiciosUpdate,
} from '../../../src/actions/expresionInteres/carrera/aActividadesServicios';

import { car } from '../../data.v2';

describe('Carrera actividades y servicios', () => {
  let pureState;
  let expected;

  beforeEach(() => {
    expected = _.cloneDeep(car[0].data);
    pureState = fromJS(car[0].data);
  });

  it('actividades update', () => {
    expected.actividades['2014'] = 'test';

    const result = actividadesServicios(
      fromJS(pureState.get('actividades')),
      actividadesServiciosUpdate(0, 'actividades', '2014', 'test')
    );

    assert.deepEqual(result.toJS(), expected.actividades);
  });

  it('servicios update', () => {
    expected.servicios['2014'] = 'servicio';

    const result = actividadesServicios(
      fromJS(pureState.get('servicios')),
      actividadesServiciosUpdate(0, 'servicios', '2014', 'servicio')
    );

    assert.deepEqual(result.toJS(), expected.servicios);
  });
});
