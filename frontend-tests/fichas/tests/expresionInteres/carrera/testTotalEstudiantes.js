import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import totalEstudiantes
  from '../../../src/reducers/expresionInteres/carrera/totalEstudiantes';

import {
  totalEstudiantesUpdate,
} from '../../../src/actions/expresionInteres/carrera/aTotalEstudiantes.js';

import { car as initialState } from '../../data.v2';

describe('Carrera total estudiantes', () => {
  it('total estudiantes update', () => {
    const expected = _.cloneDeep(initialState[0].data.estudiantes);
    expected['2014'].pa.M = '5';

    const result = totalEstudiantes(
      fromJS(initialState[0].data.estudiantes),
      totalEstudiantesUpdate(0, '2014', 'pa', 'M', '5')
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
