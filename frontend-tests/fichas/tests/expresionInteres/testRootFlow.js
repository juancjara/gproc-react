import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import initialState from '../data.v2';

import rootReducer from '../../src/reducers/expresionInteres';

import {
  addSede,
  removeSede,
} from '../../src/actions/expresionInteres/institucional/aSedes';

import {
  addSedeCarrera,
} from '../../src/actions/expresionInteres/institucional/aSedeCarreras';

// @TODO: add more tests
describe('institucional reducers flow', () => {
  let data;

  before(() => {
    data = _.cloneDeep(initialState);
  });

  it.skip('add sede and carrera THUNK', () => {
    const sedeName = `Filial ${data.ins.data.sedes.length}`;
    const carreraId = data.ins.data.sedes.length * 1000;
    const expected = _.cloneDeep(data);
    expected.ins.status.hasChanged = true;
    expected.ins.data.sedes.push({
      nombre_sede: sedeName,
      direc: '',
      dist: '',
      prov: '',
      region: '',
      carreras: [{
        id: carreraId,
        nombre: '',
        area: '',
      }],
    });
    expected.car.push({
      data: {
        id: carreraId,
        nombre: '',
        area: '',
      },
      dbId: undefined,
      status: {
        errors: [],
        hasChanged: false,
        status: "STORED",
      },
    });
    const newTotalEstudiantesRow = {
      id: carreraId,
      nombre: `${sedeName}: `,
      x2010: '',
      x2011: '',
      x2012: '',
      x2013: '',
      x2014: '',
    };
    expected.ins.data.total_estudiantes.p_admision.push(newTotalEstudiantesRow);
    expected.ins.data.total_estudiantes.m_semestre.push(newTotalEstudiantesRow);
    expected.ins.data.total_estudiantes.egresados.push(newTotalEstudiantesRow);
    expected.ins.data.total_estudiantes.titulados.push(newTotalEstudiantesRow);

    const createStoreWithMiddleware = applyMiddleware(
      thunk
    )(createStore);

    const store = createStoreWithMiddleware(rootReducer, fromJS(data));
    store.dispatch(addSede());
    assert.deepEqual(store.getState().toJS(), expected);
  });

  it.skip('remove sede and carreras THUNK', () => {
    const expected = _.cloneDeep(data);
    expected.ins.status.hasChanged = true;
    expected.car = [];
    expected.ins.data.sedes = [];
    expected.ins.data.total_estudiantes.p_admision = [];
    expected.ins.data.total_estudiantes.m_semestre = [];
    expected.ins.data.total_estudiantes.egresados = [];
    expected.ins.data.total_estudiantes.titulados = [];

    const createStoreWithMiddleware = applyMiddleware(
      thunk
    )(createStore);

    const store = createStoreWithMiddleware(rootReducer, fromJS(data));
    store.dispatch(removeSede(0));

    assert.deepEqual(store.getState().toJS(), expected);
  });

  it.skip('add carrera THUNK', () => {
    const sedeName = 'Sede principal';
    const carreraId = 3;
    const expected = _.cloneDeep(data);
    expected.ins.data.sedes[0].carreras.push({
      id: carreraId,
      nombre: 'car',
      area: 'ar',
    });
    expected.ins.status.hasChanged = true;
    expected.car.push({
      dbId: undefined,
      data: {
        id: carreraId,
        nombre: 'car',
        area: 'ar',
      },
      status: {
        status: 'STORED',
        errors: [],
        hasChanged: false,
      }
    });

    const newTotalEstudiantesRow = {
      id: carreraId,
      nombre: `${sedeName}: car`,
      x2010: '',
      x2011: '',
      x2012: '',
      x2013: '',
      x2014: '',
    };
    expected.ins.data.total_estudiantes.p_admision.push(newTotalEstudiantesRow);
    expected.ins.data.total_estudiantes.m_semestre.push(newTotalEstudiantesRow);
    expected.ins.data.total_estudiantes.egresados.push(newTotalEstudiantesRow);
    expected.ins.data.total_estudiantes.titulados.push(newTotalEstudiantesRow);

    const createStoreWithMiddleware = applyMiddleware(
      thunk
    )(createStore);

    const store = createStoreWithMiddleware(rootReducer, fromJS(data));
    store.dispatch(addSedeCarrera(0, 'car', 'ar'));
    assert.deepEqual(store.getState().toJS(), expected);
  });
});
