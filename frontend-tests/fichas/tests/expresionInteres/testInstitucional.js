import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import data from '../data.v2';
const { ins: { data: initialState } } = data;

import institucional from '../../src/reducers/expresionInteres/institucional';

import {
  institucionalUpdate,
} from '../../src/actions/expresionInteres/aInstitucional';

import {
  addMember,
} from '../../src/actions/expresionInteres/institucional/aComiteInstitucional';

import {
  updateRowColSum,
  updateColSum,
} from '../../src/actions/expresionInteres/institucional/aCuerpoAcademico';

import {
  updateSede,
} from '../../src/actions/expresionInteres/institucional/aSedes';

import {
  updateTotalEstudiantes,
} from '../../src/actions/expresionInteres/institucional/aTotalEstudiantes';

describe('Institucional', () => {
  let expected;

  beforeEach(() => {
    expected = _.cloneDeep(initialState);
  });

  it('update field', () => {
    expected.ruc = '000000000';

    const result = institucional(
      fromJS(initialState),
      institucionalUpdate('ruc', '000000000')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('Comite: add comite member', () => {
    const result = institucional(fromJS(initialState), addMember());

    expected.comite.push({
      id: result.get('comite').last().get('id'),
      pers_nombres: '',
      pers_apellidos: '',
      pers_cargo: '',
      pers_correo: '',
      pers_telefono: '',
    });

    assert.deepEqual(result.toJS(), expected);
  });

  it('Cuerpo Academico: update grados dedicacion (row column total)', () => {
    expected.cuerpo_academico.grados_dedicacion.pt.ex = '1';
    expected.cuerpo_academico.grados_dedicacion.pt.tt = '1';
    expected.cuerpo_academico.grados_dedicacion.tt.ex = '404';
    expected.cuerpo_academico.grados_dedicacion.tt.tt = '1107';

    const result = institucional(
      fromJS(initialState),
      updateRowColSum('grados_dedicacion', 'pt', 'ex', 1)
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('CuerpoAcademico: update docentes categoria (row total)', () => {
    expected.cuerpo_academico.docentes_categoria.pr.tt = '200';
    expected.cuerpo_academico.docentes_categoria.tt.tt = '889';

    const result = institucional(
      fromJS(initialState),
      updateColSum('docentes_categoria', 'pr', 'tt', 200)
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('Sedes: update field sede', () => {
    expected.sedes[0].nombre_sede = 'name 1';
    const result = institucional(
      fromJS(initialState),
      updateSede(0, 'nombre_sede', 'name 1')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('Lista estudiantes: update cell postulantes x admision', () => {
    expected.total_estudiantes.p_admision[2].x2013.m = '100';
    const result = institucional(
      fromJS(initialState),
      updateTotalEstudiantes('p_admision', 3, 'x2013', 'm', '100')
    );
    assert.deepEqual(result.toJS(), expected);
  });
});
