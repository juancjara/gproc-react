import { assert } from 'chai';
import _ from 'lodash';
import {
  fromJS,
} from 'immutable';

import data from '../../data.v2';
const { ins: { data: { sedes: sedesRaw } } } = data;
import sedes
  from '../../../src/reducers/expresionInteres/institucional/sedes';
import {
  _addSede,
  _removeSede,
  updateSede,
} from '../../../src/actions/expresionInteres/institucional/aSedes';

describe('Sedes', () => {
  let initialState;

  beforeEach(() => {
    initialState = _.cloneDeep(sedesRaw);
  });

  it('add sede', () => {
    const newSede = {
      id: 1,
      nombre_sede: 'Sede Principal',
      direc: '',
      dist: '',
      prov: '',
      region: '',
      carreras: [],
    };
    const expected = _.cloneDeep(initialState);
    expected.push(newSede);
    const result = sedes(fromJS(initialState), _addSede(1, 'Sede Principal'));

    assert.deepEqual(result.toJS(), expected);
  });

  it('remove sede, TODO remove carreras', () => {
    const sedesData = initialState;
    sedesData.push({
      nombre_sede: '',
      direc: '',
      dist: '',
      prov: '',
      region: '',
      carreras: [
        {
          id: 101,
          nombre: '',
          area: '',
        },
      ],
    });
    const expected = _.cloneDeep(sedesData);
    expected.pop();
    const result = sedes(fromJS(sedesData), _removeSede(1));

    assert.deepEqual(result.toJS(), expected);
  });

  it('update field sede', () => {
    const expected = _.cloneDeep(initialState);
    expected[0].nombre_sede = 'name 1';
    const result = sedes(
      fromJS(initialState),
      updateSede(0, 'nombre_sede', 'name 1')
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
