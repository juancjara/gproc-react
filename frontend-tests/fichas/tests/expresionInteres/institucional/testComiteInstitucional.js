import { assert } from 'chai';
import {
  fromJS,
  List,
} from 'immutable';

import comiteInstitucional
  from '../../../src/reducers/expresionInteres/institucional/comite';
import {
  addMember,
  removeMember,
  updateMember,
} from '../../../src/actions/expresionInteres/institucional/aComiteInstitucional';

describe('comite institucional reducers', () => {
  it('add comite member', () => {
    const expected = [
      {
        pers_nombres: '',
        pers_apellidos: '',
        pers_cargo: '',
        pers_correo: '',
        pers_telefono: '',
      },
    ];
    const result = comiteInstitucional(List(), addMember());
    expected[0].id = result.toJS()[0].id;

    assert.deepEqual(result.toJS(), expected);
  });

  it('remove comite member', () => {
    const initialState = [
      {
        pers_nombres: '',
        pers_apellidos: '',
        pers_cargo: '',
        pers_correo: '',
        pers_telefono: '',
      },
    ];
    const expected = [];

    const result = comiteInstitucional(fromJS(initialState), removeMember(0));

    assert.deepEqual(result.toJS(), expected);
  });

  it('update member field', () => {
    const initialState = [
      {
        pers_cargo: 'none',
      },
      {
        pers_cargo: 'none2',
        pers_telefono: '',
      },
    ];
    const expected = initialState.slice();
    expected[1].pers_cargo = 'test';

    const result = comiteInstitucional(
      fromJS(initialState),
      updateMember(1, 'pers_cargo', 'test')
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
