import { assert } from 'chai';
import _ from 'lodash';
import {
  fromJS,
} from 'immutable';

import totalEstudiantesTable
  from '../../../src/reducers/expresionInteres/institucional/totalEstudiantesTable';

import {
  updateTotalEstudiantes,
} from '../../../src/actions/expresionInteres/institucional/aTotalEstudiantes';

import {
  addCarrera,
  removeCarrera,
  updateCarrera,
} from '../../../src/actions/expresionInteres/institucional/aSedeCarreras';

import { ins } from '../../data.v2';

function testTotalEstudiantes(initialState, tableId) {
  const table = initialState.total_estudiantes[tableId];
  const expected = _.cloneDeep(table);
  expected[2].x2013.m = '100';
  const result = totalEstudiantesTable(
    fromJS(table),
    updateTotalEstudiantes(tableId, 3, 'x2013', 'm', '100')
  );
  assert.deepEqual(result.toJS(), expected);
}

const years = {
  x2015: {
    m: '',
    f: '',
  },
  x2014: {
    m: '',
    f: '',
  },
  x2013: {
    m: '',
    f: '',
  },
  x2012: {
    m: '',
    f: '',
  },
  x2011: {
    m: '',
    f: '',
  }
};

describe('total estudiantes', () => {
  let initialState;

  beforeEach(() => {
    initialState = _.cloneDeep(ins.data);
  });

  it('update cell postulantes x admision', () => {
    testTotalEstudiantes(initialState, 'p_admision');
  });

  it('update cell matriculados x semestre', () => {
    testTotalEstudiantes(initialState, 'm_semestre');
  });

  it('update cell egresados', () => {
    testTotalEstudiantes(initialState, 'egresados');
  });

  it('update cell titulados', () => {
    testTotalEstudiantes(initialState, 'titulados');
  });

  it('add carrera inside', () => {
    const table = initialState.total_estudiantes.p_admision;
    table[table.length - 1].id = table.length + 2;
    const expected = _.cloneDeep(table);
    expected.push(expected[expected.length - 1]);
    expected[expected.length - 2] = _.assign({
      id: 2,
      nombre: 'sede: namerino',
    }, years);
    const result = totalEstudiantesTable(
      fromJS(table),
      addCarrera(0, 'sede', 2, 'namerino', '')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('add carrera at the end', () => {
    const table = initialState.total_estudiantes.p_admision;
    const expected = _.cloneDeep(table);
    expected.push(_.assign({
      id: 37,
      nombre: 'sede: namerino',
    }, years));
    const result = totalEstudiantesTable(
      fromJS(table),
      addCarrera(0, 'sede', 37, 'namerino', '')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('add carrera at the beginning', () => {
    const basicRow = {
      id: 37,
      nombre: '',
    };
    const table = [_.clone(basicRow), _.clone(basicRow)];
    table[1].id = 38;
    const expected = _.cloneDeep(table);
    expected.unshift(_.assign({
      id: 1,
      nombre: 'sede: namerino',
    }, years));
    const result = totalEstudiantesTable(
      fromJS(table),
      addCarrera(0, 'sede', 1, 'namerino', '')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('remove carrera ', () => {
    const basicRow = {
      id: 38,
      nombre: '',
    };
    const table = [basicRow, basicRow];
    table[0].id = 1;
    const expected = _.cloneDeep(table);
    expected.pop();
    const result = totalEstudiantesTable(
      fromJS(table),
      removeCarrera(1)
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('sede carrera update', () => {
    const table = initialState.total_estudiantes.p_admision;
    const carreraId = table.slice(-1)[0].id;
    const expected = _.cloneDeep(table);
    expected[expected.length - 1].nombre =
      `${expected.slice(-1)[0].nombre.split(':')[0]}: testerino`;
    const result = totalEstudiantesTable(
      fromJS(table),
      updateCarrera(0, 'Sede principal', carreraId, 'nombre', 'testerino')
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
