import { assert } from 'chai';
import _ from 'lodash';
import {
  fromJS,
} from 'immutable';

import sedeCarreras
  from '../../../src/reducers/expresionInteres/institucional/sedeCarreras';
import {
  addCarrera,
  removeCarrera,
  updateCarrera,
} from '../../../src/actions/expresionInteres/institucional/aSedeCarreras';
import rawData from '../../data.v2';

describe('sedeCarreras', () => {
  let initialState;
  let expected;

  beforeEach(() => {
    initialState = _.cloneDeep(rawData.ins.data.sedes);
    expected = _.cloneDeep(rawData.ins.data.sedes);
  });

  it('add carrera', () => {
    const idCarrera = initialState.length;
    const newCarrera = {
      id: idCarrera,
      nombre: 'EDUCACION PRIMARIA',
      area: 'SOCIALES',
    };
    expected.push(newCarrera);

    const result = sedeCarreras(
      fromJS(initialState),
      addCarrera(0, 'sede', idCarrera, 'EDUCACION PRIMARIA', 'SOCIALES')
    );
    assert.deepEqual(result.toJS(), expected);
  });

  it('remove carrera from sede', () => {
    const idCarrera = initialState.length;
    const newCarrera = {
      id: idCarrera,
      nombre: 'EDUCACION PRIMARIA',
      area: 'SOCIALES',
    };

    const result = sedeCarreras(
      fromJS(initialState.concat(newCarrera)),
      removeCarrera(0, idCarrera)
    );
    assert.deepEqual(result.toJS(), expected);
  });

  it('update field carrera name', () => {
    expected[expected.length - 1].nombre = 'test';

    const result = sedeCarreras(
      fromJS(initialState),
      updateCarrera(0, 'Sede principal', initialState.length - 1, 'nombre', 'test')
    );
    assert.deepEqual(result.toJS(), expected);
  });
});
