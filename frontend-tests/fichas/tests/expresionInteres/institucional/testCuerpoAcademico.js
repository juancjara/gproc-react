import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import cuerpoAcademico
  from '../../../src/reducers/expresionInteres/institucional/cuerpoAcademico';

import {
  updateRowColSum,
  updateColSum,
} from '../../../src/actions/expresionInteres/institucional/aCuerpoAcademico';

import { ins } from '../../data.v2';

const { data: { cuerpo_academico: initialState } } = ins;

describe('Cuerpo academico: update tables with row and/or column total', () => {
  it('update grados dedicacion (row column total)', () => {
    const expected = _.cloneDeep(initialState.grados_dedicacion);
    expected.pt.ex = '1';
    expected.pt.tt = '1';
    expected.tt.ex = '404';
    expected.tt.tt = '1107';

    const result = cuerpoAcademico(
      fromJS(initialState.grados_dedicacion),
      updateRowColSum('grados_dedicacion', 'pt', 'ex', 1)
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('update grados condicion (row column total)', () => {
    const expected = _.cloneDeep(initialState.grados_condicion);
    expected.pt.no = '1';
    expected.pt.tt = '1';
    expected.tt.no = '825';
    expected.tt.tt = '1107';

    const result = cuerpoAcademico(
      fromJS(initialState.grados_condicion),
      updateRowColSum('grados_condicion', 'pt', 'no', 1)
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('update docentes categoria (row total)', () => {
    const expected = _.cloneDeep(initialState.docentes_categoria);
    expected.pr.tt = '200';
    expected.tt.tt = '889';

    const result = cuerpoAcademico(
      fromJS(initialState.docentes_categoria),
      updateColSum('docentes_categoria', 'pr', 'tt', 200)
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
