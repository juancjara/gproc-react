import _ from 'lodash';
import { assert } from 'chai';
import {
  fromJS,
} from 'immutable';

import { car as initialState } from '../data.v2';


import carrera from '../../src/reducers/expresionInteres/carrera/carrera';
import carreras from '../../src/reducers/expresionInteres/carreras';
import status from '../../src/reducers/status';

import {
  carrerasAddCarrera,
  carrerasRemoveCarrera,
} from '../../src/actions/expresionInteres/aCarreras';

import {
  carreraAreaUpdate,
} from '../../src/actions/expresionInteres/carrera/aCarrera';

import {
  addMember,
  removeMember,
  updateMember,
} from '../../src/actions/expresionInteres/carrera/aComite';

import {
  totalEstudiantesUpdate,
} from '../../src/actions/expresionInteres/carrera/aTotalEstudiantes';

import {
  publicacionesUpdate,
} from '../../src/actions/expresionInteres/carrera/aPublicaciones';

import {
  actividadesServiciosUpdate,
} from '../../src/actions/expresionInteres/carrera/aActividadesServicios';

import {
  vinculosAdd,
  vinculosUpdate,
  vinculosRemove,
} from '../../src/actions/expresionInteres/carrera/aVinculos';

import {
  financiamientoUtilizacionUpdate,
} from '../../src/actions/expresionInteres/carrera/aFinanciamiento';

import {
  updateRowColSum,
  updateColSum,
} from '../../src/actions/expresionInteres/carrera/aCarreraCuerpoAcademico';


describe('Carreras', () => {
  let expected;
  let pureState;

  beforeEach(() => {
    expected = _.cloneDeep(initialState);
    pureState = fromJS(initialState);
  });

  it('Carrera add', () => {
    const newCarrera = carrera(undefined, {dbId: 5}).toJS();
    newCarrera.data.id = 6;
    expected.push(newCarrera);

    const result = carreras(
      pureState,
      carrerasAddCarrera(5, 6)
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('Carrera remove', () => {
    expected.shift();

    const result = carreras(
      pureState,
      carrerasRemoveCarrera(0, 0)
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('Carrera update', () => {
    expected[0].data.area = 'new area';
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      carreraAreaUpdate(0, 'new area')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('carrera comite add member', () => {
    const result = carreras(
      pureState,
      addMember(0)
    );

    expected[0].data.comite.push({
      id: result.getIn([0, 'data', 'comite']).last().get('id'),
      pers_nombres: '',
      pers_apellidos: '',
      pers_cargo: '',
      pers_correo: '',
      pers_telefono: '',
    });
    expected[0].status.hasChanged = true;


    assert.deepEqual(result.toJS(), expected);
  });

  it('carrera comite update member', () => {
    expected[0].data.comite[0].pers_nombres = 'name';
    expected[0].status.hasChanged = true;
    const id = expected[0].data.comite[0].id;

    const result = carreras(
      pureState,
      updateMember(0, id, 'pers_nombres', 'name')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('carrera comite remove member', () => {
    expected[0].data.comite.pop();
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      removeMember(0, 1)
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('carrera total estudiantes update', () => {
    expected[0].data.estudiantes['2014'].ms.F = '50';
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      totalEstudiantesUpdate(0, '2014', 'ms', 'F', '50')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('carrera cuerpo academico dedicacion update', () => {
    expected[0].data.cuerpo_academico.grados_dedicacion.pt.tc = '5';
    expected[0].data.cuerpo_academico.grados_dedicacion.tt.tc = '7';
    expected[0].data.cuerpo_academico.grados_dedicacion.pt.tt = '6';
    expected[0].data.cuerpo_academico.grados_dedicacion.tt.tt = '29';
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      updateRowColSum(0, 'grados_dedicacion', 'pt', 'tc', '5')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('carrera cuerpo academico condicion update', () => {
    expected[0].data.cuerpo_academico.grados_condicion.pe.co = '8';
    expected[0].data.cuerpo_academico.grados_condicion.tt.co = '21';
    expected[0].data.cuerpo_academico.grados_condicion.pe.tt = '9';
    expected[0].data.cuerpo_academico.grados_condicion.tt.tt = '32';
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      updateRowColSum(0, 'grados_condicion', 'pe', 'co', '8')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('carrera cuerpo academico docentes categoria update', () => {
    expected[0].data.cuerpo_academico.docentes_categoria.as.tt = '10';
    expected[0].data.cuerpo_academico.docentes_categoria.tt.tt = '12';
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      updateColSum(0, 'docentes_categoria', 'as', 'tt', '10')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('carrera investigaciones update', () => {
    expected[0].data.inves['2014'].ac = 'new value';
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      publicacionesUpdate(0, 'inves', '2014', 'ac', 'new value')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('carrera publicaciones update', () => {
    expected[0].data.publi['2011'].ac = 'new public';
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      publicacionesUpdate(0, 'publi', '2011', 'ac', 'new public')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('carrera patentes update', () => {
    expected[0].data.paten['2011'].ti = 'new paten';
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      publicacionesUpdate(0, 'paten', '2011', 'ti', 'new paten')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('carrera actividades update', () => {
    expected[0].data.actividades['2011'] = 'new value';
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      actividadesServiciosUpdate(0, 'actividades', '2011', 'new value')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('Carrera vinculos add', () => {
    const result = carreras(
      pureState,
      vinculosAdd(0)
    );

    expected[0].data.vinculos.push({
      id: result.getIn([0, 'data', 'vinculos']).last().get('id'),
      ic: '',
      dv: '',
    });
    expected[0].status.hasChanged = true;

    assert.deepEqual(result.toJS(), expected);
  });

  it('Carrera vinculos remove', () => {
    expected[0].data.vinculos.shift();
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      vinculosRemove(0, 0)
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('Carrera vinculos update', () => {
    expected[0].data.vinculos[0].ic = 'new ic';
    expected[0].status.hasChanged = true;
    const id = expected[0].data.vinculos[0].id;

    const result = carreras(
      pureState,
      vinculosUpdate(0, id, 'ic', 'new ic')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('Carrera origen fondos update', () => {
    expected[0].data.origen_fondos.pao['2014n'] = '200';
    expected[0].data.origen_fondos.pao['2014p'] = '37.52';
    expected[0].data.origen_fondos.itd['2014p'] = '23.08';
    expected[0].data.origen_fondos.tt['2014n'] = '533';
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      financiamientoUtilizacionUpdate(0, 'origen_fondos', 'pao', '2014n', '200')
    );

    assert.deepEqual(result.toJS(), expected);
  });

  it('carrera destino fondos update', () => {
    expected[0].data.destino_fondos.s2['2014n'] = '20';
    expected[0].data.destino_fondos.tt['2014n'] = '32';
    expected[0].data.destino_fondos.s1['2014p'] = '6.25';
    expected[0].data.destino_fondos.s2['2014p'] = '62.50';
    expected[0].data.destino_fondos.bc['2014p'] = '18.75';
    expected[0].data.destino_fondos.ob['2014p'] = '3.13';
    expected[0].data.destino_fondos.ou['2014p'] = '9.38';
    expected[0].status.hasChanged = true;

    const result = carreras(
      pureState,
      financiamientoUtilizacionUpdate(0, 'destino_fondos', 's2', '2014n', '20')
    );

    assert.deepEqual(result.toJS(), expected);
  });
});
