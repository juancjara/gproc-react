(function (root) {
  var data = {
    efe: [
      {
        dbId: 1,
        parent_id: '568',
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      },
      {
        dbId: 2,
        data: {
          descripcion: 'asdfjadlfjlj'
        },
        parent_id: '569',
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      },
      {
        dbId: 3,
        data: {
          descripcion: ''
        },
        parent_id: '570',
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      },
      {
        dbId: 4,
        data: {
          descripcion: ''
        },
        parent_id: '571',
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      },
      {
        dbId: 5,
        data: {
          descripcion: ''
        },
        parent_id: '572',
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      },
      {
        dbId: 6,
        data: {
          descripcion: ''
        },
        parent_id: '573',
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      },
      {
        dbId: 7,
        data: {
          descripcion: ''
        },
        parent_id: '574',
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      },
      {
        dbId: 8,
        data: {
          descripcion: ''
        },
        parent_id: '575',
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      },
    ],
    fic: {
      dbId: 129,
      data: {
        firmas: {
          com: {
            nombre: 'Garrage Navarro simon',
            cargo: 'Jefe de acreditacion',
            date: '2016-02-11',
            date_local: '11 de febrero del 2016'
          },
          dir: {
            nombre: 'Yris Olivia Curay Ochoa',
            cargo: 'Directora General',
            date: '2016-02-11',
            date_local: '11 de febrero del 2016'
          }
        }
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      },
    },
  };

  if (typeof module !== 'undefined' && module.exports) {
    module.exports = data;
  } else {
    root.__DATA__ = data;
  }
})(this);
