(function (root) {
  var data = {
    act: [ ],
    sub: [
	    {
		    dbId: 999,
		    parent_id: 589,
		    data: {
          // niveles: ["B", "B1", "BE.3", "BE.3.3", null/*'BE.3.3.1'*/],
          // niveles: [null],
          niveles: ["R", "RC", "RPA", "RPAI", "RPAI.3", "RPAI.3.11"],
          descripcion: 'asdfasjdfljadlfjl',
			    otro: 'asdf',
          viaticos: '12',
          pasajes: '1',
			    costo_unitario: '10',
			    unidad: '1',
			    cantidad: '2',
          costo: '123',
			    total: '3000',
          file: 'asdf',
			    financiamiento: 'propio',
          todo_seleccionado: true,
		    },
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        }
	    },
    ],
    fic: {
      dbId: 129,
      data: {
        firmas: {
          com: {
            nombre: 'Garrage Navarro simon',
            cargo: 'Jefe de acreditacion',
            date: '2016-02-11',
            date_local: '11 de febrero del 2016'
          },
          dir: {
            nombre: 'Yris Olivia Curay Ochoa',
            cargo: 'Directora General',
            date: '2016-02-11',
            date_local: '11 de febrero del 2016'
          }
        }
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      }
    },
  };

  if (typeof module !== 'undefined' && module.exports) {
    module.exports = data;
  } else {
    root.__DATA__ = data;
  }
})(this);

