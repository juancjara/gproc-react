// {
//   dbId: 1,
//   data: {
//     descripcion: 'asdf',
//     medios: [
//       {
//         id: 'asdfasdf',
//         conc_id: '12342',
//       }
//     ],
//   },
//   parent_id: 100,
//   status: {
//     errors: [],
//     status: 'STORED',
//     hasChanged: false,
//   },
// }

(function (root) {
  var data = {
    ind: [
      {
        dbId: 1,
        data: {
          descripcion: 'asdf',
          medios: [
            {
              id: 'asdfasdf',
              conc_id: '1632',
            },
            {
              id: 'aasdfasdf',
              conc_id: 'otro',
              otro: 'asdf test otro',
            }
          ],
        },
        parent_id: '568',
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      },
      {
        dbId: 2,
        data: {
          descripcion: 'asdf asdf',
          medios: [
            {
              id: '123',
              conc_id: '1634',
            },
            {
              id: '123',
              conc_id: '',
            },
            {
              id: '12314',
              conc_id: '1636',
              otro: 'asdf test otro',
            }
          ],
        },
        parent_id: '568',
        status: {
          errors: [],
          status: 'STORED',
          hasChanged: false,
        },
      }
    ],
    fic: {
      dbId: 129,
      data: {
        firmas: {
          com: {
            nombre: 'Garrage Navarro simon',
            cargo: 'Jefe de acreditacion',
            date: '2016-02-11',
            date_local: '11 de febrero del 2016'
          },
          dir: {
            nombre: 'Yris Olivia Curay Ochoa',
            cargo: 'Directora General',
            date: '2016-02-11',
            date_local: '11 de febrero del 2016'
          }
        }
      },
      status: {
        errors: [],
        status: 'STORED',
        hasChanged: false,
      },
    },
  };

  if (typeof module !== 'undefined' && module.exports) {
    module.exports = data;
  } else {
    root.__DATA__ = data;
  }
})(this);
// medios: [
//   {
//     count: 0,
//     "display": "asd - asd asd",
//     "id": "1632",
//     "fact_codigo": "1",
//   },
//   {
//     count: 0,
//     "display": "esta emdio verificacion",
//     "id": "1634",
//     "fact_codigo": "1",
//   },
//   {
//     count: 0,
//     "display": "medio asdf",
//     "id": "1635",
//     "fact_codigo": "1",
//   },
//   {
//     count: 0,
//     "display": "another medio",
//     "id": "1636",
//     "fact_codigo": "1",
//   },
// ],
