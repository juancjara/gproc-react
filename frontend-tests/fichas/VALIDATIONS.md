Definiciones
=====================

## Objeto estado de los nodos

```
1.	expresionInteres
1.1.	ficha >>
1.2.	institucional
1.2.1.	comite >>
1.2.2.	cuerpoAcademico >>
1.2.3.	sedes
1.2.3.1.	sede __
1.2.3.1.1.	sedeCarreras >>
1.2.4.	totalEstudiantes
1.2.4.1.	totalEstudiantesTable >>
1.3.	carreras
1.3.1.	carrera
1.3.1.1.	comite >>
1.3.1.2.	totalEstudiantes >>
1.3.1.3.	cuerpoAcademico >>
1.3.1.4.	financiamientoUtilizacion >>
1.3.1.5.	vinculos >>
1.3.1.6.	actividadesServicios >>
```

1.1.
...

1.2.1.
...

1.2.2.
int | S/D | N/A, required
each ded total == each con total
cat_doc only for institutes
cat_doc total == con-nom-total

1.2.3.1.
...

1.2.3.1.1.
...

1.2.4.1.
...

1.3.1.
...

1.3.1.1.
...

1.3.1.2.
...

1.3.1.3.
=1.2.2.

...
