import path from 'path';
import express from 'express';
import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { renderToString } from 'react-dom/server';
import { fromJS } from 'immutable';

import getInitialDataExpInteres from './src/initialData/expresionInteres';
import ExpresionInteres from './src/containers/expresionInteres/App';
import configureStoreExpInteres from './src/store/expresionInteres';
import { setContext } from './src/context';
import d from './tests/data.v2.js';

const app =  new express();
const port = 4000;

app.use('/dist', express.static(__dirname + '/dist'));

const renderPartialPage = ({
  initialState,
  context,
  html
}) => (
  `
    <div id="root">${html}</div>
    <script>
     window.__INITIAL_STATE__ = ${JSON.stringify(initialState)}
     window.__CONTEXT__ = ${JSON.stringify(context)}
    </script>
  `
);

app.get('/expresionInteres', (req, res) => {
  console.log('body', req.body);
  // const {
  //   data,
  //   context,
  // } = req.body;
  const context = {"editable":true, "carr_nombre": "EDUCACION PRIMARIA","aTipoFinanciamiento":[{"id":"2","text":"Plan de Mejora de Carreras"},{"id":"4","text":"Evaluaci\u00f3n Externa de Carrera"}],"institucional":true,"universidad":true,"aPersona":[{"pers_id":"911","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Wildo","pers_apellidos":"Condor\u00ed","pers_cargo":"Presidente","pers_telefono":"996500961","pers_correo":"widowilliam@hotmail.com","pers_tipo":"contacto","acre_id":"220"},{"pers_id":"912","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Nilton","pers_apellidos":"Mayta","pers_cargo":"Secretario","pers_telefono":"951771202","pers_correo":"nicemaja78@hotmail.com","pers_tipo":"comision","acre_id":"220"},{"pers_id":"913","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Ofelia","pers_apellidos":"Mamani","pers_cargo":"Docente","pers_telefono":"951565014","pers_correo":"sumayaofely@hotmail.com","pers_tipo":"comision","acre_id":"220"}],"moda_nombre":"En proceso de acreditaci\u00f3n","inst_nombre":"UNIVERSIDAD NACIONAL DEL ALTIPLANO","inst_gestion":"PUBLICA", "aPersona":[{"pers_id":"911","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Wildo","pers_apellidos":"Condor\u00ed","pers_cargo":"Presidente","pers_telefono":"996500961","pers_correo":"widowilliam@hotmail.com","pers_tipo":"contacto","acre_id":"220"},{"pers_id":"912","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Nilton","pers_apellidos":"Mayta","pers_cargo":"Secretario","pers_telefono":"951771202","pers_correo":"nicemaja78@hotmail.com","pers_tipo":"comision","acre_id":"220"},{"pers_id":"913","pers_fecha_reg":"2014-09-11 09:29:07","pers_fecha_act":"2014-09-11 09:29:07","pers_estado":"activo","pers_nombres":"Ofelia","pers_apellidos":"Mamani","pers_cargo":"Docente","pers_telefono":"951565014","pers_correo":"sumayaofely@hotmail.com","pers_tipo":"comision","acre_id":"220"}]};
  const data = d;

  setContext(context);

  const initialState = getInitialDataExpInteres(fromJS(data), context);
  const store = configureStoreExpInteres(initialState);

  const html = renderToString(
    <Provider store={store}>
      <ExpresionInteres />
    </Provider>
  );

  // return renderFullPage({
  //   html,
  //   context,
  //   initialState: initialState.toJS()
  // });
  res.send(renderFullPage({
    html,
    context,
    initialState: initialState.toJS()
  }));
});


function renderFullPage({html, context, initialState}) {
  return `
    <html>
      <head>
        <title>Redux Universal Example</title>
      </head>
      <body>
        <div id="root">${html}</div>
        <script>
          window.__INITIAL_STATE__ = ${JSON.stringify(initialState)}
          window.__CONTEXT__ = ${JSON.stringify(context)}
        </script>
        <script src="/dist/index.js"></script>
      </body>
    </html>
    `;
};

app.listen(port, function(error) {
  if (error) {
    console.error(error);
  } else {
    console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port);
  }
});

