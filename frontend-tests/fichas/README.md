Definiciones
=====================

## Objeto estado de los nodos

```
{
  fic: {
    status: STORED,
    hasChanged: true,
    errors: ['La suma debe dar 10'],
  }
  ins: {
    status: STORED,
    hasChanged: false,
    errors: [],
  }
  car0: {
    status: FETCHING,
    hasChanged: false,
    errors: [],
  },
  car1000: {
    status: ERROR,
    hasChanged: true,
    errors: [],
  }
}
```


## Al iniciar:

```
cureState() {
  // verificar consistencia de los nodos carreras con las carreras en el nodo institucional
}
```

## Data al enviar un nodo

`fetch(url, { nodo_state })`


## statuses:

STORED:   data guardada
FETCHING: data siendo enviada
ERROR:    data no guardada


## Ficha estados

```
onInit: {
  status: STORED
  hasChanged: false,
  errors: [],
}

onChange: {
  hasChanged: true,
}

onSubmit: {
  status: FETCHING,
  hasChanged: false,
}

```

```
onSubmit: (url, nodoId, data) => {
  fetch(url, data)
    ->then(_.partial(onReceive, nodoId, _))
}

onReceive: (nodoId, response) => {
  setState({
    nodoId: nodoId,
    status: STORED | ERROR,
  });
}
```

## Al abandonar / guardar ficha / terminar ficha:

```
doFetch() {
  each.node.status === ERROR: fetch (retry)
  each.node.hasChanged === true: fetch
}

onLeave() {
  doFetch()

  system.status === STORED ? leave : alert()
}

onFinish() {
  each.node.validate()
  global.validate()

  leave or finish
}
```


## System status

```
updateSystemState() {
  {
    ERROR: if any.status === ERROR
    FETCHING: if any.status === FETCHING
    STORED: if all.status === STORED && all.hasChanged === false
      CHANGED: any.hasChanged === true
  }
}
```


## timer

```
setTimeout(
  doFetch,
  n * 1000
)
```


## servidor:

```
```
